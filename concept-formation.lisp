; Concform.lsp a part of the EPAM VI program copyright 2002 by 
; Howard B. Richman.  This is the Feb. 24, 2002 version

;  Many of these parameters are from RULEX

; These global variables are used in a concept formation experiment
(defvar *try-conjunction-parameter* 100)
(defvar *speed-of-learning-parameter* 21)
(defvar *attributes-for-sampling*)
(defvar *find-category-routine*)
(defvar *study-routine*)
(defvar *compute-typicalities-routine*)
(defvar *results*)
(defvar *possible-values*)
(defvar *classifications*)
(defvar *default-category* 'positive)
(defvar *other-category* 'negative)
(defvar *categories*)
(defvar *other-category-parameter* 100); Determines the percentage of 
; the time that subjects will form an hypothesis about the *other-category*
(defvar *last-object*)
(defvar *last-category*)
(defvar *confidence* '(possible likely very-likely))
(defvar *binary-values*)
(defvar *learning-array*)
(defvar *typicality-array*)
(defvar *net-descriptions*)
(defvar *errors-by-case*)
(defvar *latency-results*)
(defvar *profile-results*)
(defvar *recognition-results*)
(defvar *pattern-results*)
(defvar *hamming-results*)
(defvar *dimension-of-rule*) ; dimension specified by instructions in the 
;rules and exceptions strategy
(defvar *recognition-test-routine*) ; routine used to perform a recognition
; test in a concept-formation-experiment.  The routine used can vary by strategy.

(defvar *transfer-array*) ; an array which shows the number of errors made by the subject
                ; for each case when the *phase-of-experiment* is 'transfer.
(defvar *recognition-test-array*) ; an array which holds the results 
                                  ; of a recognition test done during the 
                                  ; transfer stage if *do-recognition-test*
                                  ; is true and it is the transfer phase.
(defvar *errors-per-block-array*) ; an array which shows the number of 
                ; errors per block.  For example, the first cell or 0 cell
                ; has a list showing the number of errors on the first trial
                ; of each run of the experiment.  


(defvar *a*) ; members of category a for experimenter routine
(defvar *b*) ; members of category b
(defvar *maximum-learning-trials*) ; This is the number of learning trials
                                     ; that will occur during each learning
                                     ; sequence.
(defvar *phase-of-experiment*)
(defvar *stage-of-experiment*) ; used as part of the rules-and-exceptions
; strategy to keep track for the subject whether he is studying the rule
; or studying the exception.  The two values are 'rule-learning and 
; 'exception-learning.
(defvar *cases*)
(defvar *transfer-set*)
(defvar *output2* nil) ; This holds the output of the recognition test routine
(defvar *do-prelearning-phase* nil)

; These parameters correspond precisely to the RULEX parameters of the 
; published model.  A noticing order is put together during each call 
; to hypothesis formation by sampling from these weights, thus they 
; reflect a likelihood to be added to an hypothesis.
(defvar *w1* 1) ; In simulations simulated by both EPAM and RULEX, these
(defvar *w2* 1) ;     weights determine EPAM's noticing order as well!
(defvar *w3* 1) ;    
(defvar *w4* 1) ;
(defvar *w5* 1) ; 
(defvar *w6* 1) ; 
(defvar *w7* 1) ; 
(defvar *list-of-possible-tests*) ; This variable should be instantiated 
; whenever the stimuli are created for a concept formation experiment.
(defun create-noticing-order-from-rulex-weights ()
   (let ((tests *list-of-possible-tests*)
         (weights (list *w1* *w2* *w3* *w4* *w5* *w6* *w7*)))
      (create-noticing-order-from-weights tests weights)))

(defun create-noticing-order-from-weights (tests weights)
   (cond ((null (cdr tests)) tests)
         ('else
          (let* ((total-weight (loop repeat (length tests)
                                 for weight in weights 
                                 sum weight))
                 (random-number (random total-weight)))
             (loop for test in tests
               for weight in weights
               for n from 1
               sum weight into cumulative-weight
               if (< random-number cumulative-weight)
               return (cons test 
                        (create-noticing-order-from-weights
                         (remove-nth-element-from n tests)
                         (remove-nth-element-from n weights))))))))

   

(defun remove-nth-element-from (n lis)
  (loop for element in lis 
        for i from 1 
        if (not (= n i))
        collect element))
        



(defun find-subject-response (object)
   (when *print-strategy-updates*
      (format t "~%Finding subject response to ~a~%" object))
  (let (response)
     ; About 250 msec of fixed time to respond in a categorization experiment
     (time-tally-to-make-a-decision)
     (cond
      ((setf response (apply *find-category-routine* (list object))))
      ((setf response (guess *categories*))
       (when *print-strategy-updates*
          (format t "Guessing response~%"))))
     response))

(defun guess (categories)
  (time-tally-to-guess-a-response)
  (nth (random (length categories)) categories))

(defun can-create-hypothesis? (object &optional dimensions)
   "Create a simple hypothesis by comparing current item to previous item.  
Hypothesis is that current item shares a characteristic with previous item 
and is therefore the same classification.  If all characteristics different 
pick an attribute that differs and choose it for hypothesis.  Make
prediction based upon new hypothesis.  Last object is storied in 
global variable *last-object*, last-category is stored in global variable
*last-category*"
   (when *last-object* 
      (when (and *print-strategy-updates*
                 (or (null *last-category*)
                     (eql *last-category* #\_)))
         (format t "Not creating hypothesis because last-category is no longer in imagery store~%"
           ))
     (when *last-category*
        (create-hypothesis object *last-object* *last-category* nil dimensions))))
 
 (defun create-hypothesis (object last-object category 
                            &optional association dimensions initial-confidence)
    (when (not initial-confidence)
       (setf initial-confidence 'possible))
    (setf (get 'visual 'imagery-store) nil)
    (when *print-strategy-updates*
       (format t "Creating hypothesis by comparing:~%object=~a~%& last-object=~a~%" 
         object last-object))
    (when (not association) (setf association 'category))
    (let* ((noticing-order (or dimensions
                               (create-noticing-order-from-rulex-weights)))
           (image (get-image-of (get-chunk-for object)))
           )
       (when (not (member 'is-a noticing-order))
          (setf noticing-order (cons 'is-a noticing-order))
       )
       (loop for attribute in (create-noticing-order-from image)
         for value = (test-value-finder image attribute)
         do (visualize-value-into-sketch-pad 'stimulus value attribute))
       (when *print-strategy-updates* 
          (format t "Noticing-order = ~a~%" noticing-order))
       (loop for attribute in noticing-order
         for value1 = (specify (test-value-finder object attribute))
         for value2 = (specify (test-value-finder last-object attribute))
         for value3 = (specify (test-value-finder image attribute))
         with count = 0
         if (= count 2)
         do 'nil ; Already testing sufficient attributes so stop.
         else if (and (listp attribute) ; compound-test
                      (> count 0))
         do 'nil ; do not put a compound on hypothesis if there is already a test there
         else if (and (listp attribute)
                      (loop for test in attribute
                        for value = (specify (test-value-finder image test))
                        if (and value
                                (not (eql value #\_)))
                        ; here one part of the compound test is on the image
                        return t))
         do 'nil
         else if value3
         do 'nil
         else if (and value1
                      (not (eql value1 #\_))
                      (equal value1 value2))
         do (visualize-value-into-sketch-pad 'stimulus value1 attribute)
         (when (not (eql attribute 'is-a)) (incf count))
         (when (listp attribute) (incf count))
         finally 
         (return (cond ((> count 0)
                        (visualize-value-into-sketch-pad 'response category 1)
                        (visualize-value-into-sketch-pad 'stimulus initial-confidence 'confidence)
                        (when *print-strategy-updates*
                           (print-pad))
                        t)
                       ('else 
                        (when *print-strategy-updates* 
                           (format t "Can't create an hypothesis because no additional features.~%"))
                        (setf (get 'visual 'imagery-store) nil)
                        (cond ((null (find-associate 'category object))
                               (when *print-strategy-updates* 
                                  (format t "Associating object and category because no current association.~%"))
                               (associate 'category object category)
                               (when *print-strategy-updates*
                                  (print-net *net* 'everything)))
                              ('else nil))))))))
 

(defun form-best-association-hypothesis (object correct-response)
   (let ((image (get-image-of (get-chunk-for object))))
      (setf (get 'visual 'imagery-store) nil)
      (when *print-strategy-updates*
         (format t "Creating hypothesis to find best association~%"))
      (loop for attribute in (create-noticing-order-from image)
        for value = (test-value-finder image attribute)
        do (visualize-value-into-sketch-pad 'stimulus value attribute))
      (visualize-value-into-sketch-pad 'response correct-response 1)
      (visualize-value-into-sketch-pad 'stimulus 'very-likely 'confidence)
      (when *print-strategy-updates*
         (print-pad))
      t))
    
(defun is-there-an-hypothesis? ()
   (when (get 'visual 'imagery-store)
      (let ((image (get-value-in-imagery-store 'stimulus 'visual)))
         (and image (get-value image 'confidence)))))

; The hypothesis will be in the retrieval structure focus cell.
;   1. The first item on the ordered list will be the stimulus
;   2. The second item on the ordered list will be the response (in this case category)
;   3. The type will be 'category
;   4. The frequency of correctness will be stored as the value of 
;      Correct-count
;   5. The stimulus will only be studied and a link created if it is 
;      decided to cache it.
(defun does-object-match-hypothesis? (object)
   (when (get 'visual 'imagery-store)
      (let ((image (get-value-in-imagery-store 'stimulus 'visual))
            (category nil))
         (when (and image (get-value image 'confidence))
            (time-tally-to-predict-from-hypothesis))
         (when (and image 
                    (get-value image 'confidence)
                    (not (eql image #\_))
                    (not (difference-finder object image))
                    (eql (get-chunk-for object)
                      (get-chunk-for image))
                    )
            (setf category 
                  (test-value-finder (get-value-in-imagery-store 'response 'visual) 
                    1))
            (when (and category
                       (not (eql category #\_)))
               category)))))

(defun time-to-sort-in-net ()
   "This routine is called when there has just been a sort in the net
that has been called as part of a strategy that requires attention.
The minimum time is 250 msec.  Times greater than that are determined 
by the *sorting-clock* which should have been set to 0 before the 
sort occurred."
   (when (< *sorting-clock* 250) (setf *sorting-clock* 250))
   (when *print-time-updates*
      (format t "time-to-sort-in-net= ~a~%" *sorting-clock*))
   (setf *clock* (+ *clock* *sorting-clock*)))


(defun find-category-in-standard-condition (item &optional is-a categories)
   "When a list of dimensions is specified, only those dimensions will be
 considered when making an hypothesis.  For example, in the Rules and Exceptions
 condition of the Medin and Smith experiment, the only dimension to be
 considered when making the hypothesis was nose-length." 
   (when (not is-a) 
      (setf is-a (get-value item 'is-a)))
   (when (not categories)
      (setf categories *categories*))
   (setf *sorting-clock* 0)
   (let* ((object (put-value (copy-object item) is-a 'is-a))
          (category (find-associate 'category object))
          (hypothesis? (is-there-an-hypothesis?))
          result)
      (when *print-strategy-updates* 
         (format t "~%~a ~a is presented~%" *clock* object))
      (time-to-sort-in-net)
      (when *print-strategy-updates*
         (print-pad))
      (when hypothesis? 
            (time-tally-to-predict-from-hypothesis))
      (cond ((and hypothesis?
                  (setf result (does-object-match-hypothesis? object)))
             (when *print-strategy-updates*
                (format t "Object matches hypothesis, so making prediction=~a from hypothesis.~%" result))
             result)
            ((and hypothesis?
                  (eql (get-chunk-for object)
                    (get-chunk-for (get-value-in-imagery-store 'stimulus 'visual))))
             (setf result 
                   (car (remove (test-value-finder (get-value-in-imagery-store 'response 'visual) 1)
                          categories)))
             (time-tally-to-make-opposite-prediction-from-hypothesis)
             (when *print-strategy-updates*
                (format t "Object sorts to hypothesis without matching, so making predicting opposite.~%"))
             result)
            ('else
             (when *print-strategy-updates*
                (cond (hypothesis?
                       (format t "Object does not sort to same node as hypothesis, so making prediction from LTM.~%"))
                      ('else 
                       (format t "There is no hypothesis so making prediction from LTM.~%"))))
             category))))

(defun does-item-sort-to-top-node? (object)
   (let ((top-item (list nil)))
      (put-value top-item (get-value object 'is-a) 'is-a)
      (eql (get-chunk-for object) (get-chunk-for top-item))))

(defun predict-opposite-category-from-hypothesis ()
   (let ((category 
           (test-value-finder 
             (get-value-in-imagery-store 'response 'visual) 1)))
      (car (remove category *categories*))))

;   1. If object matched hypothesis then classify based upon the hypothesis
;      and the answer was correct, then increment the correct-count on the 
;      hypothesis and possibly cache the hypothesis if the correct count 
;      was high enough.  But if the answer was incorrect,
;      then either elaborate -- if the correct count is high enough -- 
;      or discard the hypothesis.
;   2. If correct and Classified based upon long-term, do nothing.  Could 
;      occassionally increment correct count if that would be helpful for 
;      typicality ratings.
;   3. If incorrect and classified based upon long-term, study the correct 
;      association so that the incorrect will be ambiguous in the future.


; Changes -- write routine -- Is item confused?

(defun study-in-standard-condition (item correct-response error?
                                     &optional is-a dimensions)
   (when (not is-a)
      (setf is-a (get-value item 'is-a)))
   (let* ((object (put-value (copy-object item) is-a 'is-a))
          (associate (find-associate 'category object))
          (confused? (is-image-confused? object)))
      (cond ((not (eql (get-value (get-image-of (get-chunk-for object)) 'is-a)
                    (get-value object 'is-a)))
             (study object)) ; this creates a net for this type of object
            ((does-object-match-hypothesis? object)
             (cond (error? 
                     (when *print-strategy-updates*
                        (format t "Deleting hypothesis because object match led to error.~%"))
                     (setf (get 'visual 'imagery-store) nil))
                   ('else (increment-count-or-cache-hypothesis))
                   ))
            ((and error?
                  (null confused?)
                  associate
                  (or (not (is-there-an-hypothesis?))
                      (not (eql (get-chunk-for object)
                             (get-chunk-for (get-value-in-imagery-store 'stimulus 'visual))))))
             (when *print-strategy-updates*
                (format t "Marking image as confused because it led to incorrect prediction.~%")
                )
             (mark-image-as-confused object)
             )
            ((and (not (is-there-an-hypothesis?))
                  (or confused? (null associate))
                  (get 'visual 'imagery-store)
                  (eql correct-response (find-last-response is-a))
                  (or (not (eql correct-response *other-category*))
                      (< (random 100) *other-category-parameter*))
                  )
             (when *print-strategy-updates*
                (format t "Creating hypothesis from stimulus.~%"))
             (create-hypothesis 
               object 
               (put-value (copy-object (get-value-in-imagery-store 'stimulus 'visual)) is-a 'is-a)
               correct-response 
               'category
               dimensions
               'possible
               ))
            ((and (eql is-a 'exception)
                  (not (is-there-an-hypothesis?))
                  (or confused? (null associate))
                  (get 'visual 'imagery-store)
                  (not (eql correct-response (find-last-response is-a)))
                  )
             (when *print-strategy-updates*
                (format t "Creating hypothesis from stimulus.~%"))
             (create-hypothesis 
               object 
               object
               correct-response 
               'category
               dimensions
               'possible
               ))
            ((and (is-there-an-hypothesis?)
                  (eql (get-chunk-for object)
                    (get-chunk-for (get-value-in-imagery-store 'stimulus 'visual)))
                  (eql correct-response 
                    (test-value-finder (get-value-in-imagery-store 'response 'visual) 1)))
             (when *print-strategy-updates*
                (format t "Correct response is on hypothesis, so checking whether can narrow it.~%"))
             (possibly-narrow-hypothesis object associate)))))


(defun mark-image-as-confused (object)
   (let ((image (get-image-of (get-chunk-or-letter-for object))))
      (put-value image 'confused 'confidence)
      (time-tally-to-build-detail)))

(defun is-image-confused? (object)
   (let ((image (get-image-of (get-chunk-or-letter-for object))))
      (eql (get-value image 'confidence)
        'confused)))

   
(defun put-copy-of-object-and-category-in-vis (object category)
   (setf (get 'visual 'imagery-store) nil)
   (when *print-strategy-updates*
       (format t "Putting copy of object and category in visual imagery store~%" 
         ))
   (loop for attribute in (create-noticing-order-from object)
     for value = (test-value-finder object attribute)
     do (visualize-value-into-sketch-pad 'stimulus value attribute))
   (visualize-value-into-sketch-pad 'response category 1))
          
(defun possibly-narrow-hypothesis (object associate?)
   (let* ((hypothesis-object (get-value-in-imagery-store 'stimulus 'visual))
          (correct-response (test-value-finder (get-value-in-imagery-store 'response 'visual) 1))
          (tests-on-hypothesis 
            (find-test-that-is-on-hypothesis-object hypothesis-object)))
      (cond ((atom tests-on-hypothesis) 
             (cond (associate? nil)
                   ('else 
                    (when *print-strategy-updates*
                       (format t "Deleting hypothesis and associating node with response as part of possibly-narrow-hypothesis~%"))
                    (associate 'category object correct-response)
                    (when *print-strategy-updates*
                       (print-net *net* 'everything))
                    (setf (get 'visual 'imagery-store) nil))))
            ('else
             (let ((bad-tests
                    (loop for test in tests-on-hypothesis
                      for value1 = (test-value-extractor hypothesis-object 
                                     test)
                      for value2 = (test-value-extractor object test)
                      if (not (eql value1 value2))
                      collect test)))
                (cond ((and (car bad-tests)
                            (not (cdr bad-tests)))
                       (remove-value-from-sketch-pad 'stimulus (car bad-tests))
                       (when *print-strategy-updates*
                          (format t "Removing attribute = ~a from hypothesis~%"
                            (car bad-tests))
                          (print-pad)
                          t))
                      ('else nil)))
                ;;                      (associate? nil)
                ;      ('else
                 ;      (when *print-strategy-updates*
                  ;        (format t "Deleting hypothesis and associating node with response as part of possibly-narrow-hypothesis~%"))
                   ;    (associate 'category object correct-response)
                    ;   (setf (get 'visual 'imagery-store) nil))))
             ))))
                   

(defun is-correct-response-in-hypothesis? (correct-response type-of-object)
   (when (get 'visual 'imagery-store)
      (and 
           (eql 
             (test-value-finder (get-value-in-imagery-store 'stimulus 'visual) 'is-a) 
             type-of-object)
           (eql 
             (test-value-finder (get-value-in-imagery-store 'response 'visual) 1) 
             correct-response))))


(defun increment-count-or-cache-hypothesis ()
   "If a maximum of 1 is specified, the routine will cache a 
possible hypothesis.  If a maximum of 2 is specified, the routine will
cache a likely hypothesis." 
    (let* ((hypothesis-object (get-value-in-imagery-store 'stimulus 'visual)
             )
          (confidence (get-value hypothesis-object 'confidence)))
      (cond ((eql confidence 'potential)
             (visualize-value-into-sketch-pad 'stimulus 'possible 'confidence)
             nil)
            ((eql confidence 'possible) 
             (visualize-value-into-sketch-pad 'stimulus 'likely 'confidence)
             nil)
            ((eql confidence 'likely)
             (visualize-value-into-sketch-pad 'stimulus 'very-likely 'confidence)
             nil)
            ('else
             (cache-hypothesis-and-its-opposite)
             t))))

(defun cache-hypothesis-and-its-opposite (&optional association)
   (when *print-strategy-updates* 
      (format t "Caching Hypothesis!~%")
      (print-pad))
   (let* ((object (get-value-in-imagery-store 'stimulus 'visual))
          (category (test-value-finder (get-value-in-imagery-store 'response 'visual) 1))
          (test (find-test-that-is-on-hypothesis-object object))
          (node (get-chunk-for object))
          opposite-object)
      (when (not association) (setf association 'category))
      (when (and (listp test)
                 (member 'is-a test))
         (study object)
         (setf test (remove 'is-a test))
         (when (null (cdr test)) (setf test (car test))))
      (setf opposite-object (create-opposite-of object test))
      (when test
         (when (listp test) ; This prevents the creation of a test for (color shape) at a node that already has a test for (shape color)
            (loop for tst in (get node 'tests)
              for reverse-test = (reverse test)
              if (equal reverse-test tst)
              do (setf test tst))) 
         (loop while (study (test-value-finder object test)))
         (study object test)
         (loop while (study (test-value-finder opposite-object test)))
         (study opposite-object test))
      (associate association object category)
      (when *print-strategy-updates*
         (print-net *net* 'everything))
      (when *print-strategy-updates* (format t "Discarding hypothesis~%"))
      (setf (get 'visual 'imagery-store) nil)
   ))

    
(defun find-test-that-is-on-hypothesis-object (object)
   (let* ((image (get-image-of (get-chunk-for object)))
          (noticing-order (create-noticing-order-from object))
          (tests (loop for attribute in noticing-order
                   for value = (test-value-finder object attribute)
                   if (and (not (test-value-finder image attribute))
                           (not (eql value #\_)))
                   collect attribute)))
      (cond ((null (cdr tests)) (car tests))
            ('else tests))))

;----------------------------------------------------------------------
;          Routines specific to Martin-Caramazza experiments
;
; Routines for Martin & Caramazza's (1980) experiments from the 
; Journal of Experimental Psychology: General, Volume 109, 320-353.

(defun martin-caramazza-simulation ()
  "This routine performs a simulation of Medin & Smith's (1978) study of 
concept learning under strategic control.  Several global-parameters can 
change the conditions of this simulation:
   *a* lists the case numbers that will be considered to be category a
   *b* lists the case numbers that will be considered to be category b
   *inter-trial-interval* is the amount of time between trials
   *presentation-rate* is the amount of time in msec between presentation of the
                       response and presentation of the next stimulus
   *stimulus-duration-rate* this should be set very high for these 
                            experiments, perhaps a minute of longer, 
                            because the stimulus is kept in front of the
                            subject until the subject responds.
   *response-duration-rate* is the amount of time between when the response
                            is exposed with the stimulus before it is taken
                            away from view.  This number should be lower 
                            than the *presentation-rate*
   *number-of-sessions* determines the number of trials that will occur in 
                        each condition."
   (setf *speed-of-learning-parameter* 100)
   (setf *other-category-parameter* 100); this means that subjects will only
   (setf *overall-results* nil) ; will be used to hold results of Experiment 2
   (setf *latency-results* nil) ; will be used to hold results of Experiment 3
   (setf *errors-by-case* nil)
   (setf *profile-results* nil)
   (setf *do-prelearning-phase* nil)
   (setf *a* '(1 2 3 4 5 6 7 8 9 10 11 12)) ; these case numbers are harry
   (setf *b* '(13 14 15 16 17 18 19 20 21 22 23 24)) ; these case numbers are charlie
   (setf *default-category* 'harry)
   (setf *other-category* 'charlie)
   (setf *other-category-parameter* 50); this means that subjects will only
   ; form hypotheses about Charlie's 50% of the time when such hypotheses are
   ; possible.  They form hypotheses about Harry's 100% of the time.
   (setf *inter-trial-interval* 0)
   (setf *presentation-rate* 4000); This is the amount of time between output 
   ; presentation of the next response.
   (setf *response-duration-rate* 3000) ; This is the amount of time 
   ; that a response will be exposed upon the screen after the subject 
   ; outputs a response.
   (setf *stimulus-duration-rate* 60000) ; This is the maximum amount of time that 
   ; a stimulus will actually be on the screen.  It should be set very high.  
   ; actually the stimulus is exposed on the screen in these experiments until 
   ; the subject responds
   (setf *w1* 10.0) ; hair
   (setf *w2* 22.0) ; face-shape
   (setf *w3* 4.0) ; eyes
   (setf *w4* 1.0) ; eye-brows
   (setf *w5* 1.0) ; nose
   (setf *w6* 2.0) ; mouth
   (setf *w7* 6.0) ; ears
   (setf *number-of-sessions* 1000)
   (setf *maximum-learning-trials* 30) ; This is the number of learning trials
   ; that will occur during each learning
   ; sequence.
   (setf *exit-criteria-routine* 'martin-caramazza-exit-criteria)
   (loop for experiment in '(
                             make-martin-caramazza-cases-exp3
                             make-martin-caramazza-cases-exp2
                             )
     do (martin-caramazza experiment))
   (report-results-for-martin-caramazza-simulation)
   )

(defun martin-caramazza (experiment)
   "This routine sets the following variables which govern the subject routine 
 depending upon the condition:
          *find-category-routine*
          *study-routine*
 and also initialized the following variables which are used by the 
 experimenter routine to collect results:
      *learning-array* an array which shows the number of errors made by the 
                     subject for each case when the *phase-of-experiment* is 
                     'learning.  The the 0 cell in the array shows how many
                     subjects reached the criteria of a perfect trial.
      *transfer-array* an array which shows the number of errors made by the subject
                 for each case when the *phase-of-experiment* is 'transfer.
      *latency-array* an array which shows the number of errors made by the subject
                 for each case when the *phase-of-experiment* is 'latency."
   (setf *find-category-routine* 'find-category-in-standard-condition)
   (setf *study-routine* 'study-in-standard-condition)
   (setf *compute-typicalities-routine* 'standard-compute-typicalities)
   (setf *categories* '(harry charlie))
   (setf *binary-values* '(1 2))
   (setf *learning-array* (make-array 25 :initial-element nil)); array used to 
   ; hold errors during
   ; learning phase
   (setf *latency-array* (make-array 25 :initial-element nil)) ; array used to hold
   ; responses during
   ; transfer-phase
   (setf *typicality-array* (make-array 25 :initial-element nil))
   (setf *errors-per-block-array* (make-array *maximum-learning-trials* :initial-element nil)) ; errors during
   (setf *net-descriptions* nil)
   (cond ((eql experiment 'make-martin-caramazza-cases-exp2)
          (format t "~%Beginning Martin and Caramazza Experiment 2 - Defining Features Categories:~%"))
         ((eql experiment 'make-martin-caramazza-cases-exp3)
          (format t "~%Beginning Martin and Caramazza Exp. 3 - Family Resemblance Categories:~%")))
   (loop repeat *number-of-sessions*
     do (loop initially (setf (aref *learning-array* 0)
                              (cons 0 (aref *learning-array* 0)))
          for n from 1 to 24
          do (setf (aref *learning-array* n) 
                   (cons nil (aref *learning-array* n)))
          (setf (aref *latency-array* n) 
                (cons nil (aref *latency-array* n)))
          )
     (setf *rsf* nil)
     (martin-caramazza-experiment experiment)
     finally (cond ((eq experiment 'make-martin-caramazza-cases-exp3)
                    (report-martin-caramazza-errors 'classify-exp3-net))
                   ((eq experiment 'make-martin-caramazza-cases-exp2)
                    (report-martin-caramazza-errors 'classify-exp2-net)))))


(defun martin-caramazza-exit-criteria ()
  (cond ((eq *phase-of-experiment* 'learning)
         (or (>= *trial*  *maximum-learning-trials*)
             (>= *number-of-consecutive-perfect-trials* 2)))
        ((eq *phase-of-experiment* 'latency)
         (>= *trial* 5))))

(defun make-martin-caramazza-cases-exp2 ()
   (setf *list-of-possible-tests* '(hair face-shape eyes eye-brows
                                               nose mouth ears))
   (setf *binary-values* '((1 2) (2 1)))
   (setf *cases* (loop for case in '((1 1 2 1 1 1 1 1)  (2 1 1 2 1 1 1 1) 
                                     (3 1 1 1 2 1 1 1)  (4 1 2 2 2 1 1 1) 
                                     (5 1 1 2 2 1 1 1)  (6 1 1 2 2 1 1 2) 
                                     (7 1 2 1 1 1 2 1)  (8 1 1 1 2 1 2 1)
                                     (9 1 2 1 2 1 2 1) (10 1 1 1 1 1 2 1) 
                                     (11 1 2 1 2 1 2 2) (12 1 1 1 2 1 2 2)
                                     (13 2 2 2 2 1 2 2) (14 2 1 2 2 1 2 2) 
                                     (15 2 2 2 1 1 2 1) (16 2 1 2 1 1 2 2) 
                                     (17 2 2 2 1 2 2 2) (18 1 2 2 2 2 2 2)
                                     (19 2 1 2 1 2 2 2) (20 1 2 2 1 2 2 2) 
                                     (21 2 2 2 2 2 2 2) (22 1 1 2 1 2 2 2)
                                     (23 2 2 2 1 2 2 1) (24 1 1 2 1 2 2 1))
                   for case-number = (first case)
                   for hair = (second case)
                   for face-shape = (third case)
                   for eyes = (fourth case)
                   for eye-brows = (fifth case)
                   for nose = (sixth case)
                   for mouth = (seventh case)
                   for ears = (eighth case)
                   for ordered-alist =  (list (list 'hair hair)
                                          (list 'face-shape face-shape)
                                          (list 'eyes eyes)
                                          (list 'eye-brows eye-brows)
                                          (list 'nose nose)
                                          (list 'mouth mouth)
                                          (list 'ears ears))
                   collect (list (cons (list 'is-a 'mc) ; mc for martin-caramazza
                                   (cons (list 'case-number case-number) 
                                     ordered-alist))))))

(defun make-martin-caramazza-cases-exp3 ()
   (setf *list-of-possible-tests* '(hair face-shape eyes eye-brows
                                               nose mouth ears))
   (setf *binary-values* '((1 2) (2 1)))
   (setf *cases* (loop for case in '((1  1 2 1 1 1 1 1)  
                                     (2  1 2 2 2 2 1 1) 
                                     (3  1 2 1 1 1 2 1)  
                                     (4  1 1 1 2 2 1 1) 
                                     (5  2 2 1 2 2 1 1)
                                     (6  2 1 1 1 1 1 1) 
                                     (7  2 2 2 2 1 2 2)
                                     (8  2 2 2 1 1 2 1)
                                     (9  2 2 2 1 2 1 2)
                                     (10 1 2 2 2 2 1 2) 
                                     (11 2 2 1 2 2 1 2)
                                     (12 2 2 2 1 2 2 1)
                                     (13 2 1 2 1 1 1 1)
                                     (14 1 1 1 2 2 2 1) 
                                     (15 2 1 2 2 1 1 1)
                                     (16 1 1 2 2 1 1 2) 
                                     (17 1 2 1 2 1 2 2)
                                     (18 1 1 1 2 1 2 2)
                                     (19 2 1 2 2 1 2 2)
                                     (20 2 1 1 1 1 2 2) 
                                     (21 2 1 2 1 2 1 2)
                                     (22 1 2 1 1 2 2 2)
                                     (23 1 1 1 1 2 2 2)
                                     (24 1 1 2 1 2 2 1))
                      for case-number = (first case)
                      for hair = (second case)
                      for face-shape = (third case)
                      for eyes = (fourth case)
                      for eye-brows = (fifth case)
                      for nose = (sixth case)
                      for mouth = (seventh case)
                      for ears = (eighth case)
                      for alist =  (list (list 'hair hair)
                                         (list 'face-shape face-shape)
                                         (list 'eyes eyes)
                                         (list 'eye-brows eye-brows)
                                         (list 'nose nose)
                                         (list 'mouth mouth)
                                         (list 'ears ears))
                      collect (list (cons (list 'is-a 'mc) ; mc for martin-caramazza
                                          (cons (list 'case-number case-number) 
                                                alist))))))


(defun martin-caramazza-experiment (create-cases-routine)
  "This routine runs a single subject through a martin-caramazza experiment.
It is responsible for initializing all of the following variables.
      *sr-list* is the list of pairs of stimulus and response objects.
      *position* is the position.  If it is an even position then the next item
                 to place in the store is a stimulus.  If it is an odd position
                 then the next item to place in the store is the response.
                 if the position is the length of the sr-list then have reached
                 the end of the list and it is time to start over
      *trial* is the number of the trial, it gets incremented everytime the
              routine starts over.
      *starting-errors* is the number of total errors at the beginning of a 
                        trial if at the end of a trial it is equal to *errors*
                        then no errors have occurred on that trial.
      *output* is the response made by the subject routine
      *number-of-consecutive-perfect-trials* is the number of consecutive times 
                                             through the list without an error
                                             it should be 0 at beginning.
      *phase-of-experiment* may be 'prelearning 'learning 'transfer, 'latency, 'shepard and
                 so on.  The value of this variable determines what arrays are
                 filled with information by the experiment via the 
                 test-correctness-of-categorization-output routine and also
                 the exit criteria of the experiment via the 
                 exit-criteria routine.
      *consecutive-correct* must be set at zero at the beginning of a trial."
  (initialize-variables)
  (setf *experimenter* 'concept-formation-experimenter)
  (apply create-cases-routine nil)
  (setf *phase-of-experiment* 'learning)
  (setf *sr-list* (loop for case in (reorder-list-randomly *cases*)
                        for case-number = (get-value case 'case-number)
                        for correct-response = (cond ((member case-number *a*) 'harry)
                                                     ('else 'charlie))
                        collect (list case correct-response)))
  (zero-out-clocks)
  (setf *position* 0
        *trial* 0
        *starting-errors* 0
        *output* nil
        *number-of-consecutive-perfect-trials* 0
        *new-stimulus-flag* nil
        *errors* 0
        *last-object* nil
        *last-category* nil
        (get 'visual 'imagery-store) nil
        *consecutive-correct*  0)
  (concept-formation-strategy)
  (setf *net-descriptions* 
        (cons (parse-net-description (get (get *net* 'is-a) 'mc))
              *net-descriptions*))
  (when *print-imagery-stores*
    (print-net *net* 'everything)
    (format t "Group ~a net description = ~a~%" 
            (cond ((eq create-cases-routine 'make-martin-caramazza-cases-exp3)
                   (classify-exp3-net (car *net-descriptions*)))
                  ('else
                   (classify-exp2-net (car *net-descriptions*))))
            (car *net-descriptions*)))
  ; second phase of experiment starts here
   (setf *phase-of-experiment* 'latency)
   (when *print-experimenter-updates*
      (format t "~%~%Beginning Latency phase of Experiment:~%"))
  (setf *sr-list* (reorder-list-randomly *sr-list*))
  (zero-out-clocks)
  (setf *position* 0
        *trial* 0
        *starting-errors* 0
        *output* nil
        *number-of-consecutive-perfect-trials* 0
        *new-stimulus-flag* nil
        *errors* 0
        *last-object* nil
        *last-category* nil
        (get 'visual 'imagery-store) nil
        *consecutive-correct*  0)
  (concept-formation-strategy)
  (apply *compute-typicalities-routine* 
         (list (loop repeat 12
                     for case in *cases*
                     collect case)
               1
               'harry))
  (apply *compute-typicalities-routine*
         (list (loop for case in (nthcdr 12 *cases*)
                     collect case)
               13
               'charlie))
  )
  
(defun standard-compute-typicalities (cases array-index &optional category)
  "This routine computes typicalities based upon the idea that when several
members of the same category sort to the same node, those members are more 
typical than when different members of the same category sort to different 
nodes.  The typicality score is simply the number of items that sort to the 
same node."
  (identity category)
  (let ((alist nil))
     (loop for case in cases
       for object = (copy-object case)
       for chunk = (get-chunk-for object)
       for association = (assoc chunk alist)
       for current-number = (second association)
       if current-number
       do (rplaca (cdr association) (1+ current-number))
       else
       do (setf alist (cons (list chunk 1) alist)))
     (loop for case in cases
       for object = (copy-object case)
       for chunk = (get-chunk-for object)
       for association = (assoc chunk alist)
       for current-number = (or (second association) 0)
       for n from array-index
       do (setf (aref *typicality-array* n) 
                (cons current-number (aref *typicality-array* n))))))


(defun report-martin-caramazza-errors (net-classification-routine)
   (format t "~%Results of Martin-Caramazza-Experiment:~%")
   (loop with trials-to-criteria-array = (make-array (1+ *number-of-sessions*) :initial-element 0)
     for n from 1
     for response-list in (aref *learning-array* 1)
     for trials-to-criteria = (length response-list)
     do (setf (aref trials-to-criteria-array n) trials-to-criteria)
     finally (loop for n from 1 upto *number-of-sessions*
               for number in (aref *learning-array* 0)
               with cumulative-number = 0
               if (> number 1)
               do (incf cumulative-number)
               finally (progn (format t "~a subjects out of ~a reached criteria~%"
                                cumulative-number *number-of-sessions*)
                         (setf *errors-by-case* 
                               (append *errors-by-case*
                                 (list cumulative-number))))))
   (loop for which-ones in '(all group-1 group-2 unclassified)
     do (report-martin-caramazza-results net-classification-routine
         which-ones)))

(defun report-martin-caramazza-results (net-classification-routine which-ones)
   "Which-ones can have four values group-1 group-2 unclassified or all
if which-ones is 'all then the results for all subjects who met the criterion
are reported.  Otherwise just those classified as group-1, group-2, 
or unclassified are reported." 
   (format t "~%Results for ~a subjects:~%" which-ones)
   (let* ((average-latencies-array 
            (make-array 25 :initial-element 0))
          (average-tle-array 
            (make-array 25 :initial-element 0))
          (average-typicality-array 
           (make-array 25 :initial-element 0))
          (criterion-list (classify-nets-into-groups 
                           net-classification-routine
                           which-ones))
          (number-in-this-group (loop for criterion in criterion-list
                                  count criterion)))
      (format t "There were ~a subjects in this condition~%" 
        number-in-this-group)
      (setf *errors-by-case* 
            (append *errors-by-case* (list number-in-this-group)))
      (when (> number-in-this-group 0)
         (loop for case from 1 upto 12
           for average-tle = (loop with sum = 0
                               with sessions = 0
                               for response-list in (aref *learning-array* case)
                               for tle = (loop with last-error = 0 
                                           for response in response-list
                                           for n from 1
                                           if (not (eql response 'harry))
                                           do (setf last-error n)
                                           finally (return last-error))
                               for criterion in criterion-list
                               if criterion
                               do (incf sessions)
                               (setf sum (+ sum tle))
                               finally (return (* 1.0 (/ sum sessions))))
           do (setf (aref average-tle-array case) average-tle)
           (when *print-major-updates*
              (format t "For Harry~a, Trial Last Error = ~a~%" case average-tle))
           )
         (loop for case from 13 upto 24
           for average-tle = (loop with sum = 0
                               with sessions = 0
                               for response-list in (aref *learning-array* case)
                               for tle = (loop with last-error = 0 
                                           for response in response-list
                                           for n from 1
                                           if (not (eql response 'charlie))
                                           do (setf last-error n)
                                           finally (return last-error))
                               for criterion in criterion-list
                               if criterion
                               do (incf sessions)
                               (setf sum (+ sum tle))
                               finally (return (* 1.0 (/ sum sessions))))
           do (setf (aref average-tle-array case) average-tle)
           (when *print-major-updates* 
              (format t "For Charlie~a, Trial Last Error = ~a~%" 
                (- case 12)
                average-tle)))
         (loop for case from 1 upto 24
           for result-list = (loop for results in (aref *latency-array* case)
                               for criterion in criterion-list
                               if criterion
                               append results)
           for average-latency = (loop for latency in result-list 
                                   for n from 1
                                   sum latency into latencies
                                   finally (return (* 1.0 (/ latencies n))))
           do (setf (aref average-latencies-array case) average-latency)
           if (< case 13)
           do (when *print-major-updates* 
                 (format t "For Harry~a, reaction-time = ~a~%" case average-latency)
                 )
           else do (when *print-major-updates* 
                      (format t "For Charlie~a, reaction-time = ~a~%" 
                        (- case 12)
                        average-latency)))
         (loop for case from 1 upto 24
           for average-typicality = (loop for typicality 
                                      in (aref *typicality-array* case)
                                      with n = 0
                                      for criterion in criterion-list
                                      if criterion
                                      do (incf n)
                                      and sum typicality into typicalities
                                      finally (return (* 1.0 (/ typicalities
                                                                n))))
           do (setf (aref average-typicality-array case) average-typicality)
           if (< case 13)
           do (when *print-major-updates* 
                 (format t "For Harry~a, typicality = ~a~%" case average-typicality)
                 )
           else do (when *print-major-updates* 
                      (format t "For Charlie~a, typicality = ~a~%" 
                        (- case 12)
                        average-typicality)))
         (when (eq net-classification-routine 'classify-exp3-net)
            (tabulate-results-for-experiment3 average-latencies-array 
             which-ones
             average-tle-array 
             average-typicality-array))
         (when (eq net-classification-routine 'classify-exp2-net)
            (tabulate-results-for-experiment2 average-latencies-array 
             average-tle-array 
             average-typicality-array))
         )))


(defun tabulate-results-for-experiment2 (average-latencies-array 
                                          average-tle-array 
                                          average-typicality-array)
   (loop for case from 1 upto 12
     for average-latency = (aref average-latencies-array case)
     sum average-latency into latency-sum
     finally (progn (when *print-major-updates* (format t "Average latency for Harry's = ~a~%"
                                                  (/ latency-sum 12.0)))
               (setf *overall-results* 
                     (append *overall-results* (list (/ latency-sum 12000))))))
   (loop for case from 13 upto 24
     for average-latency = (aref average-latencies-array case)
     sum average-latency into latency-sum
     finally (progn (when *print-major-updates* (format t "Average latency for Charlie's = ~a~%"
                                                  (/ latency-sum 12.0)))
               (setf *overall-results* 
                     (append *overall-results* (list (/ latency-sum 12000))))))
   (loop for case from 1 upto 12
     for average-latency = (aref average-tle-array case)
     sum average-latency into latency-sum
     finally (progn (when *print-major-updates* (format t "Average TLE for Harry's = ~a~%"
                                                  (/ latency-sum 12.0)))
               (setf *overall-results* 
                     (append *overall-results* (list (/ latency-sum 12))))))
   (loop for case from 13 upto 24
     for average-latency = (aref average-tle-array case)
     sum average-latency into latency-sum
     finally (progn (when *print-major-updates* (format t "Average TLE for Charlie's = ~a~%"
                                                  (/ latency-sum 12.0)))
               (setf *overall-results* 
                     (append *overall-results* (list (/ latency-sum 12))))))
   (loop for case from 1 upto 12
     for average-latency = (aref average-typicality-array case)
     sum average-latency into latency-sum
     finally (progn (when *print-major-updates* (format t "Average typicality for Harry's = ~a~%"
                                                  (/ latency-sum 12.0)))
               (setf *overall-results* 
                     (append *overall-results* (list (/ latency-sum 12000))))))
   (loop for case from 13 upto 24
     for average-latency = (aref average-typicality-array case)
     sum average-latency into latency-sum
     finally (progn (when *print-major-updates* (format t "Average typicality for Charlie's = ~a~%"
                                                  (/ latency-sum 12.0)))
               (setf *overall-results* 
                     (append *overall-results* (list (/ latency-sum 12000))))))
   (loop for case from 1 upto 6
     for average-latency = (aref average-latencies-array case)
     sum average-latency into latency-sum
     finally (progn (when *print-major-updates* (format t "Average latency for Harry's having a smile = ~a~%"
                                                  (/ latency-sum 6.0)))
               (setf *overall-results* 
                     (append *overall-results* (list (/ latency-sum 6000))))))
   (loop for case from 7 upto 12
     for average-latency = (aref average-latencies-array case)
     sum average-latency into latency-sum
     finally (progn (when *print-major-updates* (format t "Average latency for Harry's having a frown = ~a~%"
                                                  (/ latency-sum 6.0)))
               (setf *overall-results* 
                     (append *overall-results* (list (/ latency-sum 6000))))))
   (loop for case from 1 upto 6
     for average-tle = (aref average-tle-array case)
     sum average-tle into tle-sum
     finally (progn (when *print-major-updates* (format t "Average tle for Harry's having a smile = ~a~%"
                                                  (/ tle-sum 6.0)))
               (setf *overall-results* 
                     (append *overall-results* (list (/ tle-sum 6))))))
   (loop for case from 7 upto 12
     for average-tle = (aref average-tle-array case)
     sum average-tle into tle-sum
     finally (progn (when *print-major-updates* (format t "Average tle for Harry's having a frown = ~a~%"
                                                  (/ tle-sum 6.0)))
               (setf *overall-results* 
                     (append *overall-results* (list (/ tle-sum 6))))))
   (loop for case from 1 upto 6
     for average-typicality = (aref average-typicality-array case)
     sum average-typicality into typicality-sum
     finally (progn (when *print-major-updates* (format t "Average typicality for Harry's having a smile = ~a~%"
                                                  (/ typicality-sum 6.0)))
               (setf *overall-results* 
                     (append *overall-results* (list (/ typicality-sum 6))))))
   (loop for case from 7 upto 12
     for average-typicality = (aref average-typicality-array case)
     sum average-typicality into typicality-sum
     finally (progn (when *print-major-updates* (format t "Average typicality for Harry's having a frown = ~a~%"
                                                  (/ typicality-sum 6.0)))
               (setf *overall-results* 
                     (append *overall-results* (list (/ typicality-sum 6))))))
   (loop for case from 13 upto 16
     for average-latency = (aref average-latencies-array case)
     sum average-latency into latency-sum
     finally (progn (when *print-major-updates* (format t "Average latency for Charlie's having a round nose = ~a~%"
                                                  (/ latency-sum 4.0)))
               (setf *overall-results* 
                     (append *overall-results* (list (/ latency-sum 4000))))))
   (loop for case from 17 upto 24
     for average-latency = (aref average-latencies-array case)
     sum average-latency into latency-sum
     finally (progn (when *print-major-updates* (format t "Average latency for Charlie's having a triangle nose = ~a~%"
                                                  (/ latency-sum 8.0)))
               (setf *overall-results* 
                     (append *overall-results* (list (/ latency-sum 8000))))))
   (loop for case from 13 upto 16
     for average-tle = (aref average-tle-array case)
     sum average-tle into tle-sum
     finally (progn (when *print-major-updates* (format t "Average tle for Charlie's having a round nose = ~a~%"
                                                  (/ tle-sum 4.0)))
               (setf *overall-results* 
                     (append *overall-results* (list (/ tle-sum 4))))))
   (loop for case from 17 upto 24
     for average-tle = (aref average-tle-array case)
     sum average-tle into tle-sum
     finally (progn (when *print-major-updates* (format t "Average tle for Charlie's having a triangle nose = ~a~%"
                                                  (/ tle-sum 8.0)))
               (setf *overall-results* 
                     (append *overall-results* (list (/ tle-sum 8))))))
   (loop for case from 13 upto 16
     for average-typicality = (aref average-typicality-array case)
     sum average-typicality into typicality-sum
     finally (progn (when *print-major-updates* (format t "Average typicality for Charlie's having a round nose = ~a~%"
                                                  (/ typicality-sum 4.0)))
               (setf *overall-results* 
                     (append *overall-results* (list (/ typicality-sum 4))))))
   (loop for case from 17 upto 24
     for average-typicality = (aref average-typicality-array case)
     sum average-typicality into typicality-sum
     finally (progn (when *print-major-updates* (format t "Average typicality for Charlie's having a triangle nose = ~a~%"
                                                  (/ typicality-sum 8.0)))
               (setf *overall-results* 
                     (append *overall-results* (list (/ typicality-sum 8))))))
   (loop for case in '(18 20 22 24)
     for average-latency = (aref average-latencies-array case)
     sum average-latency into latency-sum
     finally (progn (when *print-major-updates* (format t "Average latency for Charlie's having a triangle nose &  hair= ~a~%"
                                                  (/ latency-sum 4.0)))
               (setf *overall-results* 
                     (append *overall-results* (list (/ latency-sum 4000))))))
   (loop for case in '(17 19 21 23)
     for average-latency = (aref average-latencies-array case)
     sum average-latency into latency-sum
     finally (progn (when *print-major-updates* (format t "Average latency for Charlie's having a triangle nose & no hair= ~a~%"
                                                  (/ latency-sum 4.0)))
               (setf *overall-results* 
                     (append *overall-results* (list (/ latency-sum 4000))))))
   (loop for case in '(18 20 22 24)
     for average-tle = (aref average-tle-array case)
     sum average-tle into tle-sum
     finally (progn (when *print-major-updates* (format t "Average tle for Charlie's having a triangle nose &  hair= ~a~%"
                                                  (/ tle-sum 4.0)))
               (setf *overall-results* 
                     (append *overall-results* (list (/ tle-sum 4))))))
   (loop for case in '(17 19 21 23)
     for average-tle = (aref average-tle-array case)
     sum average-tle into tle-sum
     finally (progn (when *print-major-updates* (format t "Average tle for Charlie's having a triangle nose & no hair= ~a~%"
                                                  (/ tle-sum 4.0)))
               (setf *overall-results* 
                     (append *overall-results* (list (/ tle-sum 4))))))
   (loop for case in '(18 20 22 24)
     for average-typicality = (aref average-typicality-array case)
     sum average-typicality into typicality-sum
     finally (progn (when *print-major-updates* (format t "Average typicality for Charlie's having a triangle nose &  hair= ~a~%"
                                                  (/ typicality-sum 4.0)))
               (setf *overall-results* 
                     (append *overall-results* (list (/ typicality-sum 4))))))
   (loop for case in '(17 19 21 23)
     for average-typicality = (aref average-typicality-array case)
     sum average-typicality into typicality-sum
     finally (progn (when *print-major-updates* (format t "Average typicality for Charlie's having a triangle nose & no hair= ~a~%"
                                                  (/ typicality-sum 4.0)))
               (setf *overall-results* 
                     (append *overall-results* (list (/ typicality-sum 4))))))
   (loop for case in '(2 3 5 6 8 10 12 13 15 17 18 20 21 23)     
     for average-tle = (aref average-tle-array case)
     sum average-tle into tle-sum
     finally (progn (when *print-major-updates* (format t "Average tle for all faces having typical face shape= ~a~%"
                                                  (/ tle-sum 14.0)))
               (setf *overall-results* 
                     (append *overall-results* (list (/ tle-sum 14))))))
   (loop for case in '(1 4 7 9 11 14 16 19 22 24)     
     for average-tle = (aref average-tle-array case)
     sum average-tle into tle-sum
     finally (progn (when *print-major-updates* (format t "Average tle for all faces not having typical face shape= ~a~%"
                                                  (/ tle-sum 10.0)))
               (setf *overall-results* 
                     (append *overall-results* (list (/ tle-sum 10))))))
   (loop for case in '(13 14 15 16 18 20 22 24)
     for average-latency = (aref average-latencies-array case)
     sum average-latency into latency-sum
     finally (progn (when *print-major-updates* (format t "Average latency for Charlie's having a round nose or hair= ~a~%"
                                                  (/ latency-sum 8000.0)))
               (setf *overall-results* 
                     (append *overall-results* (list (/ latency-sum 8000))))))
   (loop for case in '(17 19 21 23)
     for average-latency = (aref average-latencies-array case)
     sum average-latency into latency-sum
     finally (progn (when *print-major-updates* (format t "Average latency for Charlie's having neither a round nose or hair= ~a~%"
                                                  (/ latency-sum 4000.0)))
               (setf *overall-results* 
                     (append *overall-results* (list (/ latency-sum 4000))))))
   )
   
   
   
   
    
(defun report-results-for-martin-caramazza-simulation ()
   (format t "Results for Experiment 2 of Martin & Caramazza (1980).~%")
   (format t "TLE means  Trial on which the Last Error occurred.~%")
   (format t "~%Data For All Subjects:~%")
   (format t "OBSERVED: No significant TLE difference between categories.~%")
   (format t "EPAM: Average TLE for Harrys = ~a, Average TLE for Charlies = ~a~%" (nth 2 *overall-results*) (nth 3 *overall-results*))
   (format t "OBSERVED: Significant TLE difference between Harrys having a smile and frown.~%")
   (format t "EPAM: TLE for Harry's with smile = ~a, TLE for Harry's with frown = ~a~%" (nth 8 *overall-results*) (nth 9 *overall-results*))
   (format t "OBSERVED: Significant TLE difference between Charlie's having a round nose and triangular nose.~%")
   (format t "EPAM: TLE for Charlie's with round nose = ~a, TLE for Charlie's with triangular nose = ~a~%" (nth 14 *overall-results*) (nth 15 *overall-results*))
   (format t "OBSERVED: Significant TLE difference between Charlie's having triangular nose and hair and Charlie's having a triangular nose but no hair.~%")
   (format t "EPAM: TLE for Charlie's having triangular nose and hair = ~a, Charlie's having triangular nose and no hair = ~a~%" (nth 20 *overall-results*) (nth 21 *overall-results*))
   (format t "OBSERVED: Marginal TLE difference between faces with typical shape and those without.~%")
   (format t "EPAM: TLE for faces with typical shape = ~a, TLE for faces without typical shape = ~a~%" (nth 24 *overall-results*) (nth 25 *overall-results*))
   
   (format t "~%Data for Group 1 Subjects:~%")
   (format t "OBSERVED: 6 of the 12 subjects were in this group.~%")
   (format t "EPAM: ~a of the ~a subjects who reached criterion were in this group.~%" 
     (nth 7 *errors-by-case*) (nth 6 *errors-by-case*))
   (format t "OBSERVED: Longer reaction times for Charlie's either with round noses or with hair when compared to other Charlies.~%")
   (format t "EPAM: Average reaction time for Charlie's with either round noses or hair = ~5,3f seconds, average reaction time for other Charlies = ~5,3f seconds~%" 
     (nth 54 *overall-results*) (nth 55 *overall-results*))
   (format t "OBSERVED: Unexpected difference: Smiling Harry's classified faster than frowning Harry's.~%")
   (format t "EPAM: Average reaction time for smiling Harrys = ~5,3f seconds, reaction time for frowning Harrys = ~5,3f seconds~%" 
     (nth 34 *overall-results*) (nth 35 *overall-results*))
   (format t "OBSERVED: From subjects' comments it appeared that subjects first classified on the basis of either nose or hair.~%")
   (format t "EPAM: Top-test was hair for ~a subjects, nose for ~a subjects, mouth for ~a subjects, eyes for ~a subjects~%" 
     (nth 60 *profile-results*) (nth 61 *profile-results*) (nth 62 *profile-results*) (nth 63 *profile-results*))

   (format t "~%Data for Group 2 Subjects:~%")
   (format t "OBSERVED: 5 of the 12 subjects were in this group.~%")
   (format t "EPAM: ~a of the ~a subjects who reached criterion were in this group.~%" 
     (nth 8 *errors-by-case*) (nth 6 *errors-by-case*))
   (format t "OBSERVED: Average reaction time for Harrys = 0.680 seconds, reaction time for Charlies = 0.870 seconds.~%")
   (format t "EPAM: Average reaction time for Harry's = ~5,3f seconds, reaction time for Charlies = ~5,3f seconds.~%" 
     (nth 56 *overall-results*) (nth 57 *overall-results*))
   (format t "OBSERVED: Trend: Average reaction time for smiling Harrys = 0.630 seconds, reaction time for frowning Harrys = 0.709 seconds.~%")
   (format t "EPAM: Average reaction time for smiling Harrys = ~5,3f seconds, reaction time for frowning Harrys = ~5,3f seconds~%" 
     (nth 63 *overall-results*) (nth 62 *overall-results*))
   (format t "OBSERVED: From subjects' comments it appeared that subjects learned a conjunction of round nose and hair, the defining features of the Harry category, and categorizing Charlies by default.  They used a combination of features to determine membership in the Harry category.~%")
   (format t "EPAM: Top-test was nose-hair for ~a subjects, eyes-mouth for ~a subjects.~%" 
     (nth 77 *profile-results*) (nth 76 *profile-results*))
   )
   
(defun tabulate-results-for-experiment3 (average-latencies-array 
                                          which-ones
                                          &optional 
                                          average-tle-array 
                                          average-typicality-array)
   (identity average-tle-array)
   (identity average-typicality-array)
   (when (or *print-major-updates*
             (eql which-ones 'group-1))
      (format t "~%Results for Figure 7:~%")
      (format t "Human data comes from Martin and Caramazza, 1980, Journal of Experimental Psychology:General, Vol 109, pp 320-353~%")
      (format t "for human subjects in Group 1.  All latency times are expressed in msec.~%") 
      (format t "Group of Faces         People    EPAM6~%")
      (format t "Harry 5,7,8,9,11,12      789     ~5,0f~%"
        (loop for case in '(5 7 8 9 11 12)
          sum (aref average-latencies-array case) into latency-average
          finally (return (/ latency-average 6.0))))
      (format t "Harry 1,2,10             996     ~5,0f~%"
        (loop for case in '(1 2 10)
          sum (aref average-latencies-array case) into latency-average
          finally (return (/ latency-average 3.0))))
      (format t "Harry 3                 1257     ~5,0f~%"
        (/ (aref average-latencies-array 3) 1.0))
      (format t "Charlie 5,10            1322     ~5,0f~%"
        (loop for case in '(17 22)
          sum (aref average-latencies-array case) into latency-average
          finally (return (/ latency-average 2.0))))
      (format t "Charlie 4,6,7,8,9,11     731     ~5,0f~%"
        (loop for case in '(16 18 19 20 21 23)
          sum (aref average-latencies-array case) into latency-average
          finally (return (/ latency-average 6.0))))
      (format t "Charlie 1,3,12           939     ~5,0f~%"
        (loop for case in '(13 15 24)
          sum (aref average-latencies-array case) into latency-average
          finally (return (/ latency-average 3.0))))
      (format t "Charlie 2               1226     ~5,0f~%"
        (/ (aref average-latencies-array 14) 1.0))
      (format t "Harry 4,6               1276     ~5,0f~%"
        (loop for case in '(4 6)
          sum (aref average-latencies-array case) into latency-average
          finally (return (/ latency-average 2.0)))))
   (when (or *print-major-updates*
             (eql which-ones 'group-2))
      (format t "~%Results for Figure 8:~%")
      (format t "Human data comes from Martin and Caramazza, 1980, Journal of Experimental Psychology:General, Vol 109, pp 320-353~%")
      (format t "for human subjects in Group 2.  All latency times are expressed in msec.~%") 
      (format t "Group of Faces                People    EPAM6~%")
      (format t "Harry 1,2,3,5,8,9,10,11,12      819     ~5,0f~%"
        (loop for case in '(1 2 3 5 8 9 10 11 12)
          sum (aref average-latencies-array case) into latency-average
          finally (return (/ latency-average 9.0))))
      (format t "Harry 7                         993     ~5,0f~%"
        (/ (aref average-latencies-array 7) 1.0))
      (format t "Charlie 5,10                   1271     ~5,0f~%"               
        (loop for case in '(17 22)
          sum (aref average-latencies-array case) into latency-average
          finally (return (/ latency-average 2.0))))
      (format t "Charlie 2,4,6,7,8,9,11,12       867     ~5,0f~%"
        (loop for case in '(14 16 18 19 20 21 23 24)
          sum (aref average-latencies-array case) into latency-average
          finally (return (/ latency-average 8.0))))
      (format t "Charlie 1,3                    1006     ~5,0f~%"
        (loop for case in '(13 15)
          sum (aref average-latencies-array case) into latency-average
          finally (return (/ latency-average 2.0))))
      (format t "Harry 4,6                      1117     ~5,0f~%"
        (loop for case in '(4 6)
          sum (aref average-latencies-array case) into latency-average
          finally (return (/ latency-average 2.0))))
      ))

(defun classify-nets-into-groups (net-classification-routine which-ones)
  "This routine returns a list of t's and nils.  The result is t if the 
subject met the criterion of two consecutive perfect trials and was in the 
group specified by which-ones.  The result is nil otherwise.  Which-ones can
be either group-1 group-2, unclassified, or all."
  (loop with net-profiles = nil
    with top-test-is-hair = 0
    with top-test-is-nose = 0
    with top-test-is-mouth = 0
    with top-test-is-eyes = 0
    with top-test-is-eyes-mouth = 0
    with top-test-is-nose-hair = 0
    with top-test-is-other = 0
    with top-test-is-face-shape-top-left-is-hair = 0
    with top-test-is-face-shape-top-left-is-ears = 0
    with top-test-is-face-shape-top-left-is-mouth = 0
    with top-test-is-face-shape-top-left-is-ears-mouth = 0
    with top-test-is-face-shape-top-left-is-other = 0
    for hit = nil
    for criterion in (aref *learning-array* 0)
    for net-profile in *net-descriptions*
    for net-classification = (apply net-classification-routine
                               (list net-profile))
    for criterion-met? = (and (> criterion 1)
                              (cond ((eq which-ones 'all) t)
                                    ((eq which-ones 'group-1) 
                                     (= net-classification 1))
                                    ((eq which-ones 'group-2)
                                     (= net-classification 2))
                                    ((eq which-ones 'unclassified)
                                     (= net-classification 0))))
    for top-test = (when (listp net-profile) (car net-profile))
    for top-left = (when (and (listp net-profile)
                               (listp (second net-profile)))
                       (caadr net-profile))
    collect criterion-met? into result
    if criterion-met?
    do (setf hit (loop for pair in net-profiles
                   if (equal net-profile (first pair))
                   do (rplaca (cdr pair) (1+ (second pair)))
                   and return t))
    (when (not hit) 
       (setf net-profiles
             (cons (list net-profile 1) net-profiles)))
    (cond ((eql top-test 'hair) (incf top-test-is-hair))
          ((eql top-test 'nose) (incf top-test-is-nose))
          ((eql top-test 'mouth) (incf top-test-is-mouth))
          ((eql top-test 'eyes) (incf top-test-is-eyes))
          ((equal top-test '(eyes mouth)) (incf top-test-is-eyes-mouth))
          ((equal top-test '(mouth eyes)) (incf top-test-is-eyes-mouth))
          ((equal top-test '(nose hair)) (incf top-test-is-nose-hair))
          ((equal top-test '(hair nose)) (incf top-test-is-nose-hair))
          ((not (eql top-test 'face-shape)) (incf top-test-is-other))
          ((eql top-left 'hair) (incf top-test-is-face-shape-top-left-is-hair))
          ((eql top-left 'ears) (incf top-test-is-face-shape-top-left-is-ears))
          ((eql top-left 'mouth) (incf top-test-is-face-shape-top-left-is-mouth))
          ((equal top-left '(ears mouth)) (incf top-test-is-face-shape-top-left-is-ears-mouth))
          ((equal top-left '(mouth ears)) (incf top-test-is-face-shape-top-left-is-ears-mouth))
          ('else (incf top-test-is-face-shape-top-left-is-other)))
    finally (return (progn 
                      (setf *profile-results* 
                            (append *profile-results* 
                              (list top-test-is-hair
                                top-test-is-nose 
                                top-test-is-mouth
                                top-test-is-eyes
                                top-test-is-eyes-mouth
                                top-test-is-nose-hair
                                top-test-is-face-shape-top-left-is-hair
                                top-test-is-face-shape-top-left-is-ears
                                top-test-is-face-shape-top-left-is-mouth
                                top-test-is-face-shape-top-left-is-ears-mouth
                                top-test-is-face-shape-top-left-is-other
                                top-test-is-other
                                )))
                      (format t "Net-profiles for ~a subjects:~%" which-ones)
                      (loop for profile in (reverse net-profiles)
                        do (when *print-major-updates* (format t "~a of ~a~%"
                                                         (second profile) (first profile))))
                      (format t "top-test-is-hair = ~a~%" top-test-is-hair)
                      (format t "top-test-is-nose = ~a~%" top-test-is-nose)
                      (format t "top-test-is-mouth = ~a~%" top-test-is-mouth)
                      (format t "top-test-is-eyes = ~a~%" top-test-is-eyes)
                      (format t "top-test-is-eyes-mouth = ~a~%" top-test-is-eyes-mouth)
                      (format t "top-test-is-nose-hair = ~a~%" top-test-is-nose-hair)
                      (format t "top-test-is-face-shape-top-left-is-hair = ~a~%" top-test-is-face-shape-top-left-is-hair)
                      (format t "top-test-is-face-shape-top-left-is-ears = ~a~%" top-test-is-face-shape-top-left-is-ears)
                      (format t "top-test-is-face-shape-top-left-is-mouth = ~a~%" top-test-is-face-shape-top-left-is-mouth)
                      (format t "top-test-is-face-shape-top-left-is-ears-mouth = ~a~%" top-test-is-face-shape-top-left-is-ears-mouth)
                      (format t "top-test-is-face-shape-top-left-is-other = ~a~%" top-test-is-face-shape-top-left-is-other)
                      (format t "top-test-is-other = ~a~%" top-test-is-other)
                      (format t "net-classification-routine = ~a, which-ones = ~a~%" net-classification-routine which-ones)
                      result
                      ))))




(defun parse-net-description (net)
  "This routine parses a discrimination net which only has
the values 1 and 2, (1 1) or (2 2) for its attributes.  When there 
is a list of tests, only the top test is included."
   (when net 
      (let* ((test (first (get net 'tests)))
             (branching-node (when test (get net test)))
             (node1 (when branching-node (get branching-node 1)))
             (node2 (when branching-node (get branching-node 2))))
         (when (listp test) ; compound tests for example 
            ; if test is (eyes mouth) value may be (1 1) or (2 2).  In  
            ; this experiment values of (1 2) or (2 1) are expected to 
            ; be rare and so will not be parsed but instead will have
            ; node1 branches to nil
            (loop for remainder on (symbol-plist branching-node)
              for attribute = (first remainder)
              for value = (second remainder)
              for image = (get-image-of attribute)
              if (eql attribute 'parent) do 'nil
              else if (not image) do 'nil
              else if (not (eql 'compound-value (get-value image 'is-a)))
              do 'nil
              else if (equal (cdr image) '(1 1))
              do (setf node1 value)
              else if (equal (cdr image) '(2 2))
              do (setf node2 value)))
         (cond ((null test) t)
               ('else 
                (list test 
                  (parse-net-description node2)
                  (parse-net-description node1)))))))
 

(defun classify-exp3-net (net-description)
  (cond ((atom net-description) 0)
        ('else
         (let ((top-test (car net-description))
               (top-left (when (listp (second net-description))
                           (car (second net-description)))))
           (cond ((eql top-test 'hair) 1)
                 ((equal top-test '(face-shape hair)) 1)
                 ((equal top-test '(hair face-shape)) 1)
                 ((not (eq top-test 'face-shape)) 0)
                 ((eq top-left 'hair) 1)
                 ('else 2))))))


(defun classify-exp2-net (net-description)
  (cond ((atom net-description) 0)
        ('else 
         (let ((top-test (car net-description)))
           (cond ((eql top-test 'hair) 1)
                 ((eql top-test 'nose) 1)
                 ((equal top-test '(nose hair)) 2)
                 ((equal top-test '(hair nose)) 2)
                 ('else 0))))))


(defun concept-formation-strategy ()
  "This is the subject's strategy when pictures are presented and are left in
the visual-sensory-store for a while even after the subject has responded.  In 
this strategy the subject
  1. At the beginning of each trial waits for stimulus 
      If experimenter says done, then exit.
  2. Responds to stimulus in sensory store using find-category-routine.
  3. Waits for next item.  
      If experimenter says done, then exit.
      If no response and instead another stimulus presented, goto 2
  4. Study using study-category-routine
  5. Copy the visual image of the stimulus into the imagery store
       Copy the visual image of the response into the imagery store
       Add a feature to the visual image of the stimulus in the imagery store.
       goto 1 to wait for the next stimulus."
   (wait-for-next-item) 
   (loop with subject-response
     if (memory-store-search (get 'visual 'sensory-store) 'signal-to-respond)
     return t  ; experimenter says done so exit
     do (setf subject-response (find-category-routine))
     (setf *output* subject-response) ; this outputs to experimenter routine
     (when (and (eql *phase-of-experiment* 'transfer)
                *do-recognition-test*)
        (setf *output2* (do-recognition-test-in-concept-formation-experiment))) 
     (advance-the-timer 10) ; This makes sure that the latency of response gets measured accurately
     (wait-for-next-item)
     if (memory-store-search (get 'visual 'sensory-store) 'signal-to-respond)
     return t
     if (get-object-from-store 'response 'visual 'sensory-store)
     do (study-category-routine subject-response)
     (wait-for-item)
     ;if (eql *phase-of-experiment* 'tranfer)
     ;do (setf (get 'visual 'imagery-store) nil)
     ))



(defun get-object-from-store (section modality type-of-store)
  (loop for item in (get modality type-of-store)
        if (eq (get-value item 'group) section)
        return (specify item)))

(defun wait-for-next-item ()
  (wait-for-item)
  (setf *new-stimulus-flag* nil))

(defun find-category-routine ()
   (let ((object (get-object-from-store 'stimulus 'visual 'sensory-store)))
      (when (or (eql *phase-of-experiment* 'transfer)
                (eql *phase-of-experiment* 'latency))
         (when (get 'visual 'imagery-store)
            (setf (get 'visual 'imagery-store) nil)))
      (find-subject-response object)))

(defun study-category-routine (subjects-response)
   (let* ((do-it? (< (random 100) *speed-of-learning-parameter*))
          (object (get-object-from-store 'stimulus 'visual 'sensory-store))
          (correct-response 
            (specify (get-object-from-store 'response 'visual 'sensory-store)))
          (error? (not (eql correct-response subjects-response))))
      (cond
       (do-it? 
         (wait-until-learning-available)
         (apply *study-routine* (list object correct-response error?)))
       ('else
        (when *print-strategy-updates*
           (format t "Not studying because of speed-of-learning-parameter~%"))))
      (when (not (is-there-an-hypothesis?))
         (put-copy-of-object-and-category-in-vis object correct-response))))

; -----------------------------------------------------------------

; Experimenter routines
          
(defun concept-formation-experimenter ()
  "This is a concept-formation experimenter routine which presents both stimuli
and responses each trial.  The difference between this and a paired associate
experimenter is that responding to the stimulus is self paced.  The experimenter
presents the stimulus and leaves it exposed until the subject responds and after
the response, leaves the stimulus and correct-response exposed together for 
an amount of time determined by the *response-duration* rate and the 
*presentation-rate*.  Note *clock* is reset to zero whenever the 
subject responds.  Routine exits by placing 'signal-to-respond in sensory store
when the exit-criteria-routine exits with t.   
  1.  The following variables are used: 
      *clock* is the amount of time that has elapsed so far since it was reset
              at zero.  The clock is advanced by subject routines.  This 
              experimenter is called whenever the clock is advanced.
      *sr-list* is the list of pairs of stimulus and response objects.
      *position* is the position.  If it is an even position then the next item
                 to place in the store is a stimulus.  If it is an odd position
                 then the next item to place in the store is the response.
                 if the position is the length of the sr-list then have reached
                 the end of the list and it is time to start over
      *trial* is the number of the trial, it gets incremented everytime the
              routine starts over.
      *inter-trial-interval* is the amount of time between trials
      *starting-errors* is the number of total errors at the beginning of a 
                        trial if at the end of a trial it is equal to *errors*
                        then no errors have occurred on that trial.
      *presentation-rate* is the amount of time between presentation of the
                          response and presentation of the next stimulus
      *stimulus-duration-rate* this should be set very high for these 
                               experiments, perhaps a minute of longer, 
                               because the stimulus is kept in front of the
                               subject until the subject responds.
      *response-duration-rate* is the amount of time between when the response
                               is exposed with the stimulus before it is taken
                               away from view.  This number should be lower 
                               than the *presentation-rate* 
      *output* is the response made by the subject routine
      *number-of-consecutive-perfect-trials* is the number of consecutive times 
                                             through the list without an error
                                             it should be 0 at beginning.

      *learning-array* an array which lists the responses made by the 
                     subject by case when the *phase-of-experiment* is 
                     'learning.  The the 0 cell in the array indicates the 
                     largest number of consecutive perfect trials that the
                     subject had during learning.
      *transfer-array* an array which lists the responses made by the subject
                 by case when the *phase-of-experiment* is 'transfer.
      *latency-array* an array which shows the reaction-time of the subject
                 for each case when the *phase-of-experiment* is 'latency.
      *errors-per-block-array* an array which holds the number of errors for 
                               each trial 
      *phase-of-experiment* may be 'learning 'transfer, 'latency, 'shepard and
                 so on.  The value of this variable determines what arrays are
                 filled with information by the experiment via the 
                 test-correctness-of-categorization-output routine and also
                 the exit criteria of the experiment via the 
                 exit-criteria routine.
      *consecutive-correct* must be set at zero at the beginning of a trial.
"
  (cond ((= *position* (* 2 (length *sr-list*))) ; Note: end of list is next
         (when (> *clock* (+ *inter-trial-interval* *presentation-rate*))
            (when (and (eql *phase-of-experiment* 'learning)
                       (< *trial* *maximum-learning-trials*))
               (setf (aref *errors-per-block-array* *trial*) 
                     (cons (- *errors* *starting-errors*)
                       (aref *errors-per-block-array* *trial*))))
            (cond ((= *errors* *starting-errors*)
                   (incf *number-of-consecutive-perfect-trials*)
                   (when (and (eql *phase-of-experiment* 'learning)
                              (< (car (aref *learning-array* 0))
                                 *number-of-consecutive-perfect-trials*))
                      (setf (aref *learning-array* 0)
                            (cons *number-of-consecutive-perfect-trials*
                              (cdr (aref *learning-array* 0))))))
                  ('else 
                   (setf *number-of-consecutive-perfect-trials* 0)))
            (incf *trial*)
            (cond ((exit-criteria-routine)
                   (put-control-symbol-in-sensory-store 'signal-to-respond 
                    'visual))
                  ('else (when *print-experimenter-updates* 
                            (format t 
                              "               *** BEGINNING TRIAL #~a ***~%" 
                              (1+ *trial*)))
                   (setf *starting-errors* *errors*
                         *position* 0
                         *sr-list* (reorder-list-randomly *sr-list*))))))
        ((exit-criteria-routine)
         (put-control-symbol-in-sensory-store 'signal-to-respond 'visual))
        ((= (mod *position* 2) 0) ; Note: stimulus is next
         (when (> *clock* *response-duration-rate*)
            (zero-out-except-epam-clocks) ;set to 0 for calculating latency
            (put-stimulus-in-sensory-store 'visual 0)
            (setf *output* nil)
            (incf *position*)))
        ('else ; Note: response is next
         (when *output*
            (test-correctness-of-categorization-output)
            (zero-out-except-epam-clocks) ; set to 0 for latency timer
            (setf *output* nil)
           (when (and (not (eql *phase-of-experiment* 'transfer))
                      (not (eql *phase-of-experiment* 'latency)))
              (put-category-in-sensory-store 'visual))
            (incf *position*)))))

(defun exit-criteria-routine ()
   "The exit criteria depends upon the value of the variable 
*phase-of-the-experiment* and is based upon such factors as:
*number-of-consecutive-perfect-trials* - times through whole list without error
*trial* - the number of trials that have been completed.
*consecutive-correct* - the number of items correct consecutively."
  (apply *exit-criteria-routine* nil))

(defun test-correctness-of-categorization-output ()
  "This routine checks to see if the subject's response was correct.  If not 
it increments *errors* and updates the *learning-array*.  It exits with whether
or not the response was correct."
   (let* ((correct-response (second (nth (/ (1- *position*) 2) *sr-list*)))
          (case (first (nth (/ (1- *position*) 2) *sr-list*)))
          (case-number (get-value case 'case-number)))
      (when *print-experimenter-updates*
         (format t 
           "For case = ~a, correct-response = ~a, subject's response = ~a, reaction-time = ~a~%"
           case correct-response *output* *clock*))
      (cond ((eq *phase-of-experiment* 'latency)
             (when case-number 
                (setf (aref *latency-array* case-number)
                      (cons (append (car (aref *latency-array* case-number))
                              (list *clock*))
                        (cdr (aref *latency-array* case-number))))
                (when *print-experimenter-updates*
                   (format t "Latency of response = ~a ms.~%" *clock*))))
            ((eq *phase-of-experiment* 'learning)
             (when case-number 
                (setf (aref *learning-array* case-number)
                      (cons (append (car (aref *learning-array* case-number))
                              (list *output*))
                        (cdr (aref *learning-array* case-number))))
                (incf *consecutive-correct*); note - if error this will be reduced to zero.
                (when (not (eql *output* correct-response))
                   (incf *errors*)
                   (setf *consecutive-correct* 0))
                (when *print-experimenter-updates*
                   (format t "                 *** ERRORS SO FAR = ~a ***~%" *errors*))
                ))
            ((eq *phase-of-experiment* 'transfer)
             (when case-number
                (setf (aref *transfer-array* case-number)
                      (append (aref *transfer-array* case-number) (list *output*)))
                (when *do-recognition-test*
                   (setf (aref *recognition-test-array* case-number)
                         (append (aref *recognition-test-array* case-number)
                           (list *output2*))))
                (when *print-experimenter-updates*
                   (format t "Latency of response = ~a ms.~%" *clock*))
                ))
            )
      (eql correct-response *output*)))

(defun put-category-in-sensory-store (modality)
  "This routine puts the category in the sensory store and makes the 
duration of both the stimulus and the category equal to the 
*response-duration-rate*."
  (let ((duration-rate (+ (get modality 'sensory-persistence)
                          *response-duration-rate*))
        (stimulus (copy-object (first (nth (/ (1- *position*) 2) *sr-list*))))
        (category (list nil (second (nth (/ (1- *position*) 2) *sr-list*)))))
    (put-value stimulus 'stimulus 'group)
    (put-value category 'response 'group)
    (put-value stimulus duration-rate 'time)
    (put-value category duration-rate 'time)
    (setf (get modality 'sensory-store) (list stimulus category)
          *new-stimulus-flag* t
          (get modality 'new-stimulus) t)))


   
;------------------------------------------------------------------------
;             Routines Specific to Medin & Smith Simulation

(defun medin-and-smith-simulation ()
   "This routine performs a simulation of Medin & Smith's (1978) study of 
 concept learning under strategic control.  It also replicates Medin and
 Schaffer's (1978) experiment 3. Several global-parameters can 
 change the conditions of this simulation:
   *a* lists the case numbers that will be considered to be category a
   *b* lists the case numbers that will be considered to be category b
   *inter-trial-interval* is the amount of time between trials
   *presentation-rate* is the amount of time in msec between presentation of the
                       response and presentation of the next stimulus
   *stimulus-duration-rate* this should be set very high for these 
                            experiments, perhaps a minute of longer, 
                            because the stimulus is kept in front of the
                            subject until the subject responds.
   *response-duration-rate* is the amount of time between when the response
                            is exposed with the stimulus before it is taken
                            away from view.  This number should be lower 
                            than the *presentation-rate*
   *number-of-sessions* determines the number of trials that will occur in 
                        each condition."
   (setf *speed-of-learning-parameter* 40)
   (setf *other-category-parameter* 100)
   (setf *w1* 4) ; eye-height
   (setf *w2* 1) ; eye-spacing
   (setf *w3* .5) ; nose-length
   (setf *w4* 3.5) ; mouth-height
   (setf *other-category-parameter* 100)
   (setf *errors-by-case* nil)
   (setf *overall-results* nil)
   (setf *latency-results* nil)
   (setf *recognition-results* nil)
   (setf *do-recognition-test* nil)
   (setf *do-prelearning-phase* nil)
   (setf *dimension-of-rule* 'nl) ; this is the dimension for the rule in 
                                  ; the rules-plus-exceptions-condition
   (setf *a* '(4 7 15 13 5)) ; these case numbers are a's
   (setf *b* '(12 2 14 10)) ; these case numbers are b's
   (setf *inter-trial-interval* 0)
   (setf *presentation-rate* 3000)
   (setf *response-duration-rate* 2000)
   (setf *stimulus-duration-rate* 60000)
   (setf *number-of-sessions* 1000)
   (setf *maximum-learning-trials* 32) 
   (setf *exit-criteria-routine* 'medin-and-smith-exit-criteria)
   (setf *default-category* #\A)
   (setf *other-category* #\B)
   (loop for condition in '(
                            standard 
                            rules 
                            prototype
                            )
     do (medin-and-smith condition)
     (when *print-major-updates* (print-net *net* 'everything))
     (report-medin-and-smith-errors))
   (report-medin-and-smith-results))


(defun report-medin-and-smith-results ()
   (format t "~%Mean Number of Errors for Each Face during Initial Learning as~%")
   (format t "a Function of Instructions. Human data is from Medin and Smith, 1981,~%")
   (format t "EPAM V data is from Gobet et. al, 1997.~%~%")
   (format t "-----------------------------------------------------------------------~%")
   (format t "                                    Instruction~%")
   (format t "             ----------------------------------------------------------~%")
   (format t "                  Standard       Rules & Exceptions       Prototype~%")
   (format t "             ------------------  ------------------  ------------------~%")
   (format t "Face Number  People Epam5 Epam6  People Epam5 Epam6  People EPAM5 EPAM6~%")
   (format t "-----------------------------------------------------------------------~&")
   (format t " 4  (1112)     4.5   9.0 ~5,1f     3.9   6.5 ~5,1f     7.7  11.2 ~5,1f~%"
     (nth 0 *errors-by-case*) (nth 9 *errors-by-case*) (nth 18 *errors-by-case*))
   (format t " 5  (1212)     8.2  10.8 ~5,1f     5.9   8.5 ~5,1f     9.2  11.9 ~5,1f~%"
     (nth 1 *errors-by-case*) (nth 10 *errors-by-case*) (nth 19 *errors-by-case*))
   (format t " 7  (1211)     4.2   6.6 ~5,1f     3.3   4.0 ~5,1f     6.7   9.9 ~5,1f~%"
     (nth 2 *errors-by-case*) (nth 11 *errors-by-case*) (nth 20 *errors-by-case*))
   (format t "13  (1121)    11.9  11.3 ~5,1f    10.7  18.8 ~5,1f    13.7  12.8 ~5,1f~%"
     (nth 3 *errors-by-case*) (nth 12 *errors-by-case*) (nth 21 *errors-by-case*))
   (format t "15  (2111)     2.8   5.6 ~5,1f     2.8   4.5 ~5,1f     4.9   8.6 ~5,1f~%"
     (nth 4 *errors-by-case*) (nth 13 *errors-by-case*) (nth 22 *errors-by-case*))
   (format t " 2  (1122)    12.9  14.0 ~5,1f    13.8  18.5 ~5,1f    10.3  16.0 ~5,1f~%"
     (nth 5 *errors-by-case*) (nth 14 *errors-by-case*) (nth 23 *errors-by-case*))
   (format t "10  (2112)     4.4   6.6 ~5,1f     3.8   4.9 ~5,1f     4.2  10.4 ~5,1f~%"
     (nth 6 *errors-by-case*) (nth 15 *errors-by-case*) (nth 24 *errors-by-case*))
   (format t "12  (2221)    15.2  12.8 ~5,1f     6.3   7.7 ~5,1f    17.4  15.1 ~5,1f~%"
     (nth 7 *errors-by-case*) (nth 17 *errors-by-case*) (nth 25 *errors-by-case*))
   (format t "14  (2222)     6.6   8.4 ~5,1f     6.8   5.3 ~5,1f     8.7  12.4 ~5,1f~%"
     (nth 8 *errors-by-case*) (nth 17 *errors-by-case*) (nth 26 *errors-by-case*))
   (format t "M              7.9   9.5 ~5,1f     6.3   8.7 ~5,1f     9.2  12.0 ~5,1f~%"
     (/ (loop for n from 0 upto 8 sum (nth n *errors-by-case*)) 9)
     (/ (loop for n from 9 upto 17 sum (nth n *errors-by-case*)) 9)
     (/ (loop for n from 18 upto 26 sum (nth n *errors-by-case*)) 9))
   (format t "~%Observed and Predicted Proporations of Correct Categorization for~%")
   (format t "Each Face during Transfer. Human data is from Medin and Smith, 1981,~%")
   (format t "EPAM V data is from Gobet et. al, 1997.~%~%")
   (format t "-----------------------------------------------------------------------~%")
   (format t "                                    Instruction~%")
   (format t "             ----------------------------------------------------------~%")
   (format t "                  Standard       Rules & Exceptions       Prototype~%")
   (format t "             ------------------  ------------------  ------------------~%")
   (format t "Face Number  People EPAM5 EPAM6  People EPAM5 EPAM6  People EPAM5 EPAM6~%")
   (format t "-----------------------------------------------------------------------~&")
   (format t "Old faces~%")
   (format t " 4A  (1112)   0.97  0.83 ~5,2f    0.89  0.87 ~5,2f    0.77  0.75 ~5,2f~%"
     (nth 0 *overall-results*) (nth 16 *overall-results*) (nth 32 *overall-results*))
   (format t " 7A  (1212)   0.97  0.96 ~5,2f    0.94  0.98 ~5,2f    0.97  0.85 ~5,2f~%"
     (nth 1 *overall-results*) (nth 17 *overall-results*) (nth 33 *overall-results*))
   (format t "15A  (1211)   0.92  0.96 ~5,2f    0.94  0.94 ~5,2f    0.88  0.90 ~5,2f~%"
     (nth 2 *overall-results*) (nth 18 *overall-results*) (nth 34 *overall-results*))
   (format t "13A  (1121)   0.81  0.78 ~5,2f    0.72  0.64 ~5,2f    0.70  0.68 ~5,2f~%"
     (nth 3 *overall-results*) (nth 19 *overall-results*) (nth 35 *overall-results*))
   (format t " 5A  (2111)   0.72  0.76 ~5,2f    0.78  0.77 ~5,2f    0.60  0.68 ~5,2f~%"
     (nth 4 *overall-results*) (nth 20 *overall-results*) (nth 36 *overall-results*))
   (format t "12B  (1122)   0.67  0.71 ~5,2f    0.73  0.80 ~5,2f    0.45  0.57 ~5,2f~%"
     (nth 5 *overall-results*) (nth 21 *overall-results*) (nth 37 *overall-results*))
   (format t " 2B  (2112)   0.72  0.68 ~5,2f    0.70  0.67 ~5,2f    0.72  0.57 ~5,2f~%"
     (nth 6 *overall-results*) (nth 22 *overall-results*) (nth 38 *overall-results*))
   (format t "14B  (2221)   0.97  0.89 ~5,2f    0.91  0.94 ~5,2f    0.83  0.75 ~5,2f~%"
     (nth 7 *overall-results*) (nth 23 *overall-results*) (nth 39 *overall-results*))
   (format t "10B  (2222)   0.95  0.96 ~5,2f    0.95  0.96 ~5,2f    0.87  0.89 ~5,2f~%"
     (nth 8 *overall-results*) (nth 24 *overall-results*) (nth 40 *overall-results*))
   (format t "M             0.86  0.84 ~5,2f    0.84  0.84 ~5,2f    0.77  0.74 ~5,2f~%~%"
     (/ (loop for n from 0 upto 8 sum (nth n *overall-results*)) 9)
     (/ (loop for n from 16 upto 24 sum (nth n *overall-results*)) 9)
     (/ (loop for n from 32 upto 40 sum (nth n *overall-results*)) 9))
   (format t "New faces~%")
   (format t " 1A  (1221)   0.72  0.54 ~5,2f    0.45  0.26 ~5,2f    0.73  0.56 ~5,2f~%"
     (nth 9 *overall-results*) (nth 25 *overall-results*) (nth 41 *overall-results*))
   (format t " 6A  (1111)   0.98  0.93 ~5,2f    0.88  0.74 ~5,2f    0.87  0.88 ~5,2f~%"
     (nth 10 *overall-results*) (nth 26 *overall-results*) (nth 42 *overall-results*))
   (format t " 9A  (2121)   0.27  0.52 ~5,2f    0.08  0.33 ~5,2f    0.28  0.50 ~5,2f~%"
     (nth 11 *overall-results*) (nth 27 *overall-results*) (nth 43 *overall-results*))
   (format t "11A  (2211)   0.39  0.58 ~5,2f    0.75  0.88 ~5,2f    0.52  0.61 ~5,2f~%"
     (nth 12 *overall-results*) (nth 28 *overall-results*) (nth 44 *overall-results*))
   (format t " 3B  (1222)   0.44  0.63 ~5,2f    0.80  0.91 ~5,2f    0.35  0.55 ~5,2f~%"
     (nth 13 *overall-results*) (nth 29 *overall-results*) (nth 45 *overall-results*))
   (format t " 8B  (2212)   0.77  0.55 ~5,2f    0.42  0.28 ~5,2f    0.78  0.53 ~5,2f~%"
     (nth 14 *overall-results*) (nth 30 *overall-results*) (nth 46 *overall-results*))
   (format t "16B  (2122)   0.91  0.83 ~5,2f    0.88  0.66 ~5,2f    0.88  0.74 ~5,2f~%"
     (nth 15 *overall-results*) (nth 31 *overall-results*) (nth 47 *overall-results*))
   (loop with sum-of-squares = 0
     for n from 0 upto 16
     for people in '(.97 .97 .92 .81 .72 
                     .67 .72 .97 .95 
                     .72 .98 .27 .39 .44 .77 .91)
     for epam = (nth n *overall-results*)
     for diff = (expt (- people epam) 2)
     do (setf sum-of-squares (+ sum-of-squares diff))
     finally (format t "Sum of squares for EPAM = ~a~%" sum-of-squares))
   (format t "~%Mean Reaction Times for Correct Responses for Each Old Face During Speeded~%")
   (format t "Classificastion as a Function of Instructions. Human data is from Smith, 1981,~%")
   (format t "EPAM V data is from Gobet et. al, 1997.~%~%")
   (format t "-----------------------------------------------------------------------~%")
   (format t "                                    Instruction~%")
   (format t "             ----------------------------------------------------------~%")
   (format t "                  Standard       Rules & Exceptions       Prototype~%")
   (format t "             ------------------  ------------------  ------------------~%")
   (format t "Face Values  People EPAM5 EPAM6  People EPAM5 EPAM6  People EPAM5 EPAM6~%")
   (format t "-----------------------------------------------------------------------~&")
   (format t " 4   (1112)   1.11  0.96 ~5,2f    1.27  1.23 ~5,2f    1.92  1.65 ~5,2f~%"
     (nth 0 *latency-results*) (nth 9 *latency-results*) (nth 18 *latency-results*))
   (format t " 5   (1212)   1.34  1.02 ~5,2f    1.61  1.33 ~5,2f    2.13  1.68 ~5,2f~%"
     (nth 1 *latency-results*) (nth 10 *latency-results*) (nth 19 *latency-results*))
   (format t " 7   (1211)   1.08  0.70 ~5,2f    1.21  0.93 ~5,2f    1.69  1.39 ~5,2f~%"
     (nth 2 *latency-results*) (nth 11 *latency-results*) (nth 20 *latency-results*))
   (format t "13   (1121)   1.27  1.03 ~5,2f    1.87  1.44 ~5,2f    2.12  1.64 ~5,2f~%"
     (nth 3 *latency-results*) (nth 12 *latency-results*) (nth 21 *latency-results*))
   (format t "15   (2111)   1.07  0.64 ~5,2f    1.31  0.95 ~5,2f    1.54  1.22 ~5,2f~%"
     (nth 4 *latency-results*) (nth 13 *latency-results*) (nth 22 *latency-results*))
   (format t " 2   (1122)   1.30  1.14 ~5,2f    1.97  1.53 ~5,2f    1.91  1.78 ~5,2f~%"
     (nth 5 *latency-results*) (nth 14 *latency-results*) (nth 23 *latency-results*))
   (format t "10   (2112)   1.08  0.65 ~5,2f    1.42  0.96 ~5,2f    1.64  1.25 ~5,2f~%"
     (nth 6 *latency-results*) (nth 15 *latency-results*) (nth 24 *latency-results*))
   (format t "12   (2221)   1.13  1.10 ~5,2f    1.58  1.30 ~5,2f    2.29  1.75 ~5,2f~%"
     (nth 7 *latency-results*) (nth 17 *latency-results*) (nth 25 *latency-results*))
   (format t "14   (2222)   1.19  0.77 ~5,2f    1.34  0.95 ~5,2f    1.85  1.44 ~5,2f~%"
     (nth 8 *latency-results*) (nth 17 *latency-results*) (nth 26 *latency-results*))
   (format t "M             1.17  0.89 ~5,2f    1.51  1.18 ~5,2f    1.90  1.53 ~5,2f~%"
     (/ (loop for n from 0 upto 8 sum (nth n *latency-results*)) 9)
    (/ (loop for n from 9 upto 17 sum (nth n *latency-results*)) 9)
     (/ (loop for n from 18 upto 26 sum (nth n *latency-results*)) 9))
     )
 

(defun medin-and-smith (condition)
"This routine sets the following variables which govern the subject routine 
depending upon the condition:
          *find-category-routine*
          *study-routine*
          *recognition-test-routine* 
and also initialized the following variables which are used by the 
experimenter routine to collect results:
      *learning-array* an array which shows the number of errors made by the 
                     subject for each case when the *phase-of-experiment* is 
                     'learning.  The the 0 cell in the array shows how many
                     subjects reached the criteria of a perfect trial.
      *transfer-array* an array which shows the number of errors made by the subject
                 for each case when the *phase-of-experiment* is 'transfer.
      *latency-array* an array which shows the number of errors made by the subject
                 for each case when the *phase-of-experiment* is 'latency.
      *errors-per-block-array* an array which shows the number of errors made
                 for each trial.  It is used in the Palmeri-and-nosofsky
                 experiment 1 in order to determine which subjects had 
                 less than 6 errors in the last four blocks."
   (cond ((eql condition 'standard)
          (format t "~%~%Standard instructions:~%")
          (setf *find-category-routine* 'find-category-in-standard-condition)
          (setf *study-routine* 'study-in-standard-condition)
          (setf *recognition-test-routine* 'recognition-test-in-standard-condition)
          )
         ((eql condition 'rules)
          (format t "~%~%Rules plus Exceptions instructions:~%")
          (setf *find-category-routine* 'find-category-in-rules-condition)
          (setf *study-routine* 'study-in-rules-condition)
          (setf *recognition-test-routine* 'recognition-test-in-rules-condition)
          )
         ((eql condition 'prototype)
          (format t "~%~%Prototype instructions:~%")
          (setf *find-category-routine* 'find-category-in-prototype-condition)
          (setf *study-routine* 'study-in-prototype-condition)
          (setf *recognition-test-routine* 'recognition-test-in-prototype-condition))
         )
   (setf *categories* '(#\A #\B))
   (setf *binary-values* '(0 1))
   (setf *learning-array* (make-array 17 :initial-element nil)) ; this holds
   ; responses during learning stage indexed by case number
   (setf *latency-array* (make-array 17 :initial-element nil)) ; reaction times array
   (setf *transfer-array* (make-array 17 :initial-element nil)) ; errors during
   (setf *recognition-test-array* (make-array 17 :initial-element nil)) ; errors during
   (setf *errors-per-block-array* (make-array *maximum-learning-trials* :initial-element nil)) ; errors during
   (loop repeat *number-of-sessions*
     do (loop initially (setf (aref *learning-array* 0)
                              (cons 0 (aref *learning-array* 0)))
          for n from 1 to 16
          do (setf (aref *learning-array* n) 
                   (cons nil (aref *learning-array* n))))
     (medin-and-smith-experiment)
     (when *print-major-updates*
        (format t "-------------------------------------~%")
        (format t "Generalization = ~a~%" (find-pattern-string-of-last-run))
        (print-pad)
        (print-net *net* 'everything)
     )))

(defun medin-and-smith-exit-criteria ()
  (cond ((and (eq *phase-of-experiment* 'learning)
              (>= *number-of-consecutive-perfect-trials* 1))
         t)
        ((and (eq *phase-of-experiment* 'learning)
              (>= *trial*  *maximum-learning-trials*))
         t)
        ((and (eq *phase-of-experiment* 'transfer)
              (>= *trial* 2))
         t)
        ((and (eq *phase-of-experiment* 'latency)
              (>= *trial* (round (/ *maximum-learning-trials* 2))))
         t)
        ('else nil)))



(defun medin-and-smith-experiment ()
  "This routine runs a single subject through a medin-and-smith experiment.
It is responsible for initializing all of the following variables.
      *sr-list* is the list of pairs of stimulus and response objects.
      *position* is the position.  If it is an even position then the next item
                 to place in the store is a stimulus.  If it is an odd position
                 then the next item to place in the store is the response.
                 if the position is the length of the sr-list then have reached
                 the end of the list and it is time to start over
      *trial* is the number of the trial, it gets incremented everytime the
              routine starts over.
      *starting-errors* is the number of total errors at the beginning of a 
                        trial if at the end of a trial it is equal to *errors*
                        then no errors have occurred on that trial.
      *output* is the response made by the subject routine
      *number-of-consecutive-perfect-trials* is the number of consecutive times 
                                             through the list without an error
                                             it should be 0 at beginning.
      *phase-of-experiment* may be 'learning 'transfer, 'latency, 'shepard and
                 so on.  The value of this variable determines what arrays are
                 filled with information by the experiment via the 
                 test-correctness-of-categorization-output routine and also
                 the exit criteria of the experiment via the 
                 exit-criteria routine.
      *consecutive-correct* must be set at zero at the beginning of a trial."
  (initialize-variables)
  (setf *experimenter* 'concept-formation-experimenter)
  (make-medin-and-smith-cases)
  (when (eql *study-routine* 'study-in-rules-condition)
     (setf *stage-of-experiment* 'rule-learning)) ; This is necessary
   ; so that the rules-and-exceptions strategy knows when to switch 
   ; from rule-learning to exception learning.
; first phase-of-experiment starts here
   (when *do-prelearning-phase*
      (let ((speed-of-learning-parameter *speed-of-learning-parameter*))
         (setf *speed-of-learning-parameter* 100)
         (setf *phase-of-experiment* 'prelearning)
         (setf *sr-list* (loop for case in (reorder-list-randomly *cases*)
                           for case-number = (get-value case 'case-number)
                           for correct-response = (cond ((member case-number *a*) #\A)
                                                        ('else #\B))
                           if (and (not (eql case-number 5))
                                   (not (eql case-number 12)))
                           collect (list case correct-response)))
         (zero-out-clocks)
         (setf *position* 0
               *trial* 0
               *starting-errors* 0
               *output* nil
               *number-of-consecutive-perfect-trials* 0
               *new-stimulus-flag* nil
               *errors* 0
               *last-object* nil
               *last-category* nil
               (get 'visual 'imagery-store) nil
               *consecutive-correct*  0)
         (concept-formation-strategy)
         (setf *speed-of-learning-parameter* speed-of-learning-parameter)))
   (setf *phase-of-experiment* 'learning)
   (setf *sr-list* (loop for case in (reorder-list-randomly *cases*)
                     for case-number = (get-value case 'case-number)
                     for correct-response = (cond ((member case-number *a*) #\A)
                                                  ('else #\B))
                     collect (list case correct-response)))
   (zero-out-clocks)
   (setf *position* 0
         *trial* 0
         *starting-errors* 0
         *output* nil
         *number-of-consecutive-perfect-trials* 0
         *new-stimulus-flag* nil
         *errors* 0
         *last-object* nil
         *last-category* nil
         *consecutive-correct*  0)
   (when (not *do-prelearning-phase*)
      (setf (get 'visual 'imagery-store) nil))
   (concept-formation-strategy)
   ; second phase of experiment starts here
   (setf *phase-of-experiment* 'transfer)
   ;(print-net *net* 'everything)
   ;(break)
   (setf *sr-list* (loop for case in (reorder-list-randomly 
                                       (append *cases*
                                         *transfer-set*))
                     for case-number = (get-value case 'case-number)
                     for correct-response = (cond ((member case-number *a*) #\A)
                                                  ('else #\B))
                     collect (list case correct-response)))
   (zero-out-clocks)
   (setf *position* 0
         *trial* 0
         *starting-errors* 0
         *output* nil
         *number-of-consecutive-perfect-trials* 0
         *new-stimulus-flag* nil
         *errors* 0
         *consecutive-correct*  0)
   (concept-formation-strategy)
   ; third phase of experiment starts here
   (setf *phase-of-experiment* 'latency)
   (setf *sr-list* (loop for case in (reorder-list-randomly 
                                       (append *cases*
                                         *transfer-set*))
                     for case-number = (get-value case 'case-number)
                     for correct-response = (cond ((member case-number *a*) #\A)
                                                  ('else #\B))
                     collect (list case correct-response)))
   (zero-out-clocks)
   (setf *position* 0
         *trial* 0
         *starting-errors* 0
         *output* nil
         *number-of-consecutive-perfect-trials* 0
         *new-stimulus-flag* nil
         *errors* 0
         *consecutive-correct*  0)
   (concept-formation-strategy))

(defun report-medin-and-smith-errors ()
   (format t "There were ~a subjects who had perfect runs.~%"
     (loop for result in (aref *learning-array* 0)
       count (> result 0)))
   (loop for case in '(4 5 7 13 15)
     for result-list = (loop for results in (aref *learning-array* case)
                         append results)
     for number-not-a = (loop for item in result-list
                          count (not (eql item #\A)))
     for errors = (* 1.0 (/ number-not-a *number-of-sessions*))
     do (when *print-major-updates* 
           (format t "For case = ~a, errors = ~a~%" case errors))
     (setf *errors-by-case* (append *errors-by-case* (list errors)))
     )
   (loop for case in '(2 10 12 14)
     for result-list = (loop for results in (aref *learning-array* case)
                         append results)
     for number-not-b = (loop for item in result-list
                          count (not (eql item #\B)))
     for errors = (* 1.0 (/ number-not-b *number-of-sessions*))
     do (when *print-major-updates* 
           (format t "For case = ~a, errors = ~a~%" case errors))
     (setf *errors-by-case* (append *errors-by-case* (list errors)))
     )
  (loop for case in '(4 7 15 13 5)
    for number-of-a = (loop for item in (aref *transfer-array* case)
                        count (eql item #\A))
    for correct = (/ number-of-a (* 2.0 *number-of-sessions*))
    do (when *print-major-updates* 
          (format t "For case = ~a, Number of report a's = ~a~%" case correct)
          )
    (setf *overall-results* (append *overall-results* (list correct)))
    )
  (loop for case in '(12 2 14 10)
    for number-of-b = (loop for item in (aref *transfer-array* case)
                        count (eql item #\B))
    for correct = (/ number-of-b (* 2.0 *number-of-sessions*))
    do (when *print-major-updates* 
          (format t "For case = ~a, Number of report b's = ~a~%" case correct))
    (setf *overall-results* (append *overall-results* (list correct)))
    )
   (loop for case in '(1 6 9 11)
     for number-of-a = (loop for item in (aref *transfer-array* case)
                         count (eql item #\A))
     for correct = (/ number-of-a (* 2.0 *number-of-sessions*))
     do (when *print-major-updates* 
           (format t "For transfer case = ~a, Number of report a's = ~a~%"
             case correct))
     (setf *overall-results* (append *overall-results* (list correct)))
    )
  (loop for case in '(3 8 16)
        for number-of-b = (loop for item in (aref *transfer-array* case)
                                count (eql item #\B))
        for correct = (/ number-of-b (* 2.0 *number-of-sessions*))
        do (when *print-major-updates* 
              (format t "For transfer case = ~a, Number of report b's = ~a~%"
                   case correct))
    (setf *overall-results* (append *overall-results* (list correct)))
    )
  (loop for case in '(4 5 7 13 15 2 10 12 14)
        for result-list = (loop for results in (aref *latency-array* case)
                                append results)
        for average-latency = (loop for latency in result-list 
                                    for n from 1
                                    sum latency into latencies
                                    finally (return (* .001 (/ latencies n))))
        do (when *print-major-updates* 
              (format t "For case = ~a, reaction-time = ~a~%" case average-latency))
    (setf *latency-results* (append *latency-results* (list average-latency)))
    )
   )

(defun make-medin-and-smith-cases ()
   (setf *list-of-possible-tests* '(eh es nl mh))
   (setf *binary-values* '((1 0) (0 1)))
   (setf *cases* (loop for case in '((4 1 1 1 0) (7 1 0 1 0) (15 1 0 1 1) 
                                    (13 1 1 0 1) (5 0 1 1 1) (12 1 1 0 0) 
                                    (2 0 1 1 0) (14 0 0 0 1) (10 0 0 0 0))
                      for case-number = (first case)
                      for eh = (second case)
                      for es = (third case)
                      for nl = (fourth case)
                      for mh = (fifth case)
                      for alist =  (list (list 'eh eh)
                                         (list 'es es)
                                         (list 'nl nl)
                                         (list 'mh mh))
                      collect (list (cons (list 'is-a 'ms)
                                          (cons (list 'case-number case-number) 
                                                alist)))))
   (setf *transfer-set* (loop for case in '((1 1 0 0 1) (6 1 1 1 1) 
                                           (9 0 1 0 1) (11 0 0 1 1) 
                                           (3 1 0 0 0) (8 0 0 1 0) (16 0 1 0 0))
                             for case-number = (first case)
                             for eh = (second case)
                             for es = (third case)
                             for nl = (fourth case)
                             for mh = (fifth case)
                             collect (list (list (list 'case-number case-number)
                                                 (list 'is-a 'ms)
                                                 (list 'eh eh)
                                                 (list 'es es)
                                                 (list 'nl nl)
                                                 (list 'mh mh))))))

(defun find-category-in-rules-condition (case)
   (cond ((eql *stage-of-experiment* 'rule-learning)
             (find-category-in-standard-condition case))
         ('else
          (let (exception? rule)
             (setf *sorting-clock* 0)
             (setf rule (find-associate 'category case))
             (time-to-sort-in-net)
             (when (not rule)
                (format t "ERROR: No rule though in exceptions stage!!!~%")
                (break))
             (setf exception? 
                   (find-category-in-standard-condition case 'exception '(yes no)))
             (cond ((eql exception? 'yes) (car (remove rule *categories*)))
                   ((eql exception? 'no) rule)
                   ('else nil))))))
                    


   
(defun study-in-rules-condition (case correct-response error?)
   (cond ((eql *stage-of-experiment* 'rule-learning)
          (study-rule-in-rules-condition case 
           correct-response error? (list 'is-a *dimension-of-rule*)))
         ('else
          (study-exception-in-rules-condition case correct-response error?)
          )))

(defun study-rule-in-rules-condition (object correct-response error?
                                       dimensions)
   (cond ((does-object-match-hypothesis? object)
          (cond (error? 
                  (when *print-strategy-updates*
                     (format t "Deleting hypothesis because object match led to error.~%"))
                  (setf (get 'visual 'imagery-store) nil))
                ('else 
                 (when (increment-count-or-cache-hypothesis)
                    (study-opposite-object-with-other-category 
                     object correct-response)
                    (when *print-strategy-updates* 
                       (print-net *net* 'everything))
                    (setf (get 'visual 'imagery-store) nil)
                    (setf *stage-of-experiment* 'exception-learning)
                    ))))
         ((not error?) 
          nil)
         ((not (find-associate 'category object))
          (when *print-strategy-updates*
             (format t "Studying because no category associated with object.~%"))
          (associate 'category object correct-response)
          (when *print-strategy-updates*
             (print-net *net* 'everything)))
         ((not (is-there-an-hypothesis?))
          (when *print-strategy-updates*
             (format t "Creating hypothesis from stimulus.~%"))
          (create-hypothesis object object correct-response 'category dimensions 'likely))
         ))

(defun study-opposite-object-with-other-category (object correct-response)
   (let* ((opposite-object (create-opposite-of object *dimension-of-rule*))
          (opposite-category (car (remove correct-response *categories*)))
          (exception-object (put-value (copy-object object) 'exception 'is-a))
          (exception-opposite-object (put-value (copy-object opposite-object) 'exception 'is-a))
          )
      ; (study opposite-object *dimension-of-rule*)
      (associate 'category opposite-object opposite-category)
      (study exception-object)
      (study exception-object *dimension-of-rule*)
      (study exception-opposite-object *dimension-of-rule*)))

(defun create-opposite-of (object1 test)
   (let ((object (copy-object object1)))
      (cond ((null test) nil)
            ((atom test) 
             (let ((value (second (assoc (test-value-finder object1 test) *binary-values*))
                   ))
                (when (and value (not (eql value #\_))) 
                   (put-test-value-on object value test)
                   object)))
            ('else
             (loop for attribute in test
               for value1 = (test-value-extractor object1 attribute)
               for value2 = (second (assoc value1 *binary-values*))
               if (or (null value2) (eql value2 #\_))
               return nil
               else if t
               do (put-test-value-on object value2 attribute)
               finally (return object))))))
      
(defun study-exception-in-rules-condition (case correct-response error?)
   (when *print-strategy-updates* (format t "Trying to learn about exceptions.~%"))
   (let* ((rule (find-associate 'category case))
          (exception? (cond ((eql rule correct-response) 'no)
                            ('else 'yes))))
      (study-in-standard-condition case exception? error? 'exception)))

(defun find-last-response (is-a)
   (let ((last-object
          (get-value-in-imagery-store 'stimulus 'visual))
         (last-category
          (test-value-finder (get-value-in-imagery-store 'response 'visual) 1)))
      (cond ((null last-category) nil)
            ((eql is-a (get-value last-object 'is-a))
             last-category)
            ((eql is-a 'exception)
             (let ((rule (find-associate 'category last-object)))
                (cond ((eql last-category rule) 'no)
                      ('else 'yes))))
            ((eql is-a last-category) 'yes)
                ('else 'no))))




(defun find-category-in-prototype-condition (case)
   (let ((resulta (find-category-in-standard-condition case *default-category* '(yes no)))
         (resultb (find-category-in-standard-condition case *other-category* '(yes no))))
      (time-tally-to-make-a-decision) ; this extra 250 msec takes into account
      ; the time to make a decision in this more complicated condition.
      (cond ((and (eql resulta 'yes) (eql resultb 'yes))
             nil)
            ((eql resulta 'yes)
             *default-category*)
            ((eql resultb 'yes)
             *other-category*)
            ('else nil))))

(defun study-in-prototype-condition (case correct-response error?)
   (let* ((other-response (car (remove correct-response *categories*)))
          (errorc? (not (eql 'yes 
                          (find-category-in-standard-condition case correct-response '(yes no)))))
          (erroro? (not (eql 'no 
                          (find-category-in-standard-condition case other-response '(yes no))))))
      (identity error?) ; the purpose of this command is to prevent a compilation error message
      (cond ((is-there-an-hypothesis?)
             (when *print-strategy-updates*
                (format t "Studying category of hypothesis.~%"))
             (let ((hypothesis-isa (get-value (get-value-in-imagery-store 'stimulus 'visual) 'is-a)))
                (cond ((eql hypothesis-isa correct-response)
                       (study-in-standard-condition case 'yes errorc? correct-response))
                      ('else
                       (study-in-standard-condition case 'no erroro? other-response)))))
            (errorc? 
              (when *print-experimenter-updates*
                 (format t "Studying yes as member of own category.~%"))
              (study-in-standard-condition case 'yes t correct-response))
            (erroro?
             (when *print-strategy-updates*
                (format t 
                  "Studying no because member of wrong category.~%"))
             (study-in-standard-condition case 'no t other-response))
            )))

; ------------------------------------------------------------------------

(defun palmeri-and-nosofsky-experiment1-simulation ()
   "This experiment is from Journal of Experimental Psychology: Learning
Memory and Cognition, 1995, Vol 21, No. 3, 548-568.  It is very similar
to the Medin and Smith rules-plus-exception strategy simulation except that
the rule was along the first dimension, which we call 'eh in our simulations.
Also, a recognition test was performed at the end."
   (setf *speed-of-learning-parameter* 100)
   (setf *other-category-parameter* 50) ; this mainly reduces the chance of creating a "no" hypothesis
   (setf *do-prelearning-phase* t)
   (setf *errors-by-case* nil)
   (setf *overall-results* nil)
   (setf *latency-results* nil)
   (setf *recognition-results* nil)
   (setf *do-recognition-test* t)
   (setf *dimension-of-rule* 'eh)
   (setf *a* '(4 7 15 13 5)) ; these case numbers are a's
   (setf *b* '(12 2 14 10)) ; these case numbers are b's
   (setf *inter-trial-interval* 0)
   (setf *presentation-rate* 3000)
   (setf *response-duration-rate* 2000)
   (setf *stimulus-duration-rate* 60000)
   (setf *number-of-sessions* 1000)
   (setf *maximum-learning-trials* 14) ; This is actually 16 trials if you
                                       ; count the 2 pre-learning trials
   (setf *exit-criteria-routine* 'palmeri-and-nosofsky-exit-criteria)
   (setf *default-category* #\A)
   (setf *other-category* #\B)
   (setf *w1* .1) ; eye-height
   (setf *w2* .1) ; eye-spacing
   (setf *w3* .1) ; nose-length
   (setf *w4* .1) ; mouth-height
   (setf *other-category-parameter* 100)
   (medin-and-smith 'rules)
   (when *print-major-updates* (print-net *net* 'everything))
   (report-palmeri-and-nosofsky-experiment1-errors)
   (report-palmeri-and-nosofsky-experiment1-results)
   )

(defun report-palmeri-and-nosofsky-experiment1-errors ()
   (let ((met-criteria-list nil)
         number-that-met-criteria)
      (loop for last-trial in (aref *errors-per-block-array* (- *maximum-learning-trials* 1))
        for second-to-last-trial in (aref *errors-per-block-array* (- *maximum-learning-trials* 2))
        for third-to-last-trial in (aref *errors-per-block-array* (- *maximum-learning-trials* 3))
        for fourth-to-last-trial in (aref *errors-per-block-array* (- *maximum-learning-trials* 4))
        for sum-of-errors = (+ last-trial
                               second-to-last-trial
                               third-to-last-trial
                               fourth-to-last-trial)
        for met-criteria = (< sum-of-errors 6)
        do (setf met-criteria-list (cons met-criteria met-criteria-list)))
      (setf number-that-met-criteria
            (loop for met-criteria in met-criteria-list
              count met-criteria))
      (format t "~a subjects met the criteria of less than 6 errors in last 4 trials.~%"
        number-that-met-criteria)
      (when (> number-that-met-criteria 0)
         (loop for case in '(4 7 15 13 5 12 2 14 10 1 3 6 8 9 11 16)
           for number-of-a = (loop for remainder on (aref *transfer-array* case) by #'cdddr
                               for successful-subject in met-criteria-list
                               with result = 0
                               if successful-subject
                               do (loop repeat 3
                                    for item in remainder
                                    if (eql item #\A)
                                    do (incf result))
                               finally (return result))
           for correct = (/ number-of-a (* 3.0 number-that-met-criteria))
           do (when *print-major-updates* 
                 (format t "For case = ~a, Number of report a's = ~a~%" case correct)
                 )
           (setf *overall-results* (append *overall-results* (list correct)))
           )
         (loop for case in '(4 7 15 13 5 12 2 14 10 1 3 6 8 9 11 16)
           for result-list = (loop with latency-list = (car (aref *latency-array* case))
                               for successful-subject in met-criteria-list
                               for n from 0
                               for remainder = (nthcdr (* n 6) latency-list)
                               with result = nil
                               if successful-subject
                               do (loop repeat 6
                                    for item in remainder
                                    do (setf result (cons item result)))
                               finally (return result))
           for median-latency = (* .001 (find-median result-list))
           do (when *print-major-updates* 
                 (format t "For case = ~a, reaction-time = ~a~%" case median-latency))
           (setf *latency-results* (append *latency-results* (list median-latency)))
           )
         (loop for case in '(4 7 15 13 5 12 2 14 10 1 3 6 8 9 11 16)
           for number-of-a = (loop for remainder on (aref *recognition-test-array* case) by #'cdddr
                               for successful-subject in met-criteria-list
                               with result = 0
                               if successful-subject
                               do (loop repeat 3
                                    for item in remainder
                                    do (setf result (+ item result)))
                               finally (return result))
           for correct = (/ number-of-a (* 3.0 number-that-met-criteria))
           do (when *print-major-updates* 
                 (format t "For case = ~a, Proportion recognition = ~a~%" case correct)
                 )
           (setf *recognition-results* (append *recognition-results* (list correct)))
           )
         )))

(defun report-palmeri-and-nosofsky-experiment1-results ()
   (format t "~%Categorization Response Probabilities of reporting that item is~%")
   (format t "a member of Category A during the Transfer Condition.  The Human ~%")
   (format t "data and the RULEX data are from Palmeri & Nosofsky, 1995,~%~%")
   (format t "-------------------------------~%")
   (format t "Face Values  People RULEX EPAM6~%")
   (format t "-------------------------------~&")
   (format t "Category A~%")
   (format t " 4   (1112)   0.94  0.97 ~5,2f~%"
     (nth 0 *overall-results*))
   (format t " 7   (1212)   1.00  0.98 ~5,2f~%"
     (nth 1 *overall-results*))
   (format t "15   (1211)   0.97  0.99 ~5,2f~%"
     (nth 2 *overall-results*))
   (format t "13   (1121)   0.98  0.97 ~5,2f~%"
     (nth 3 *overall-results*))
   (format t " 5   (2111)   0.92  0.93 ~5,2f~%"
     (nth 4 *overall-results*))
   (format t "Category B~%")
   (format t "12   (1122)   0.13  0.13 ~5,2f~%"
     (nth 5 *overall-results*))
   (format t " 2   (2112)   0.06  0.03 ~5,2f~%"
     (nth 6 *overall-results*))
   (format t "14   (2221)   0.02  0.01 ~5,2f~%"
     (nth 7 *overall-results*))
   (format t "10   (2222)   0.02  0.01 ~5,2f~%"
     (nth 8 *overall-results*))
   (format t "Transfer Item~%")
   (format t " 1   (1221)   0.94  0.93 ~5,2f~%"
     (nth 9 *overall-results*))
   (format t " 3   (1222)   0.69  0.65 ~5,2f~%"
     (nth 10 *overall-results*))
   (format t " 6   (1111)   0.94  0.98 ~5,2f~%"
     (nth 11 *overall-results*))
   (format t " 8   (2212)   0.03  0.06 ~5,2f~%"
     (nth 12 *overall-results*))
   (format t " 9   (2121)   0.14  0.26 ~5,2f~%"
     (nth 13 *overall-results*))
   (format t "11   (2211)   0.32  0.30 ~5,2f~%"
     (nth 14 *overall-results*))
   (format t "16   (2122)   0.08  0.02 ~5,2f~%"
     (nth 15 *overall-results*))
   (loop with sum-of-squares = 0
     for n from 0 upto 16
     for people in '(.94 1 .97 .98 .92 .13 .06 .02 .02 .94 .69 .94 .03 .14 .32 .08)
     for epam = (nth n *overall-results*)
     for diff = (expt (- people epam) 2)
     do (setf sum-of-squares (+ sum-of-squares diff))
     finally (format t "Sum of squares for EPAM = ~a~%" sum-of-squares))

   (format t "----------------------------~%")
   (format t "~%Median speeded categorization response times in Palmeri~%")
   (format t "Nosofsky's Experiment 1.  The Human data is estimated from~%")
   (format t "Figure 2 on page 554 of Palmeri & Nosofsky, 1995,~%~%")
   (format t "----------------------------~%")
   (format t "Face Values  People    EPAM6~%")
   (format t "----------------------------~&")
   (format t "Category A~%")
   (format t " 4   (1112)   1.47    ~5,2f~%"
     (nth 0 *latency-results*))
   (format t " 7   (1212)   1.11    ~5,2f~%"
     (nth 1 *latency-results*))
   (format t "15   (1211)   1.28    ~5,2f~%"
     (nth 2 *latency-results*))
   (format t "13   (1121)   1.45    ~5,2f~%"
     (nth 3 *latency-results*))
   (format t " 5   (2111)   2.12    ~5,2f~%"
     (nth 4 *latency-results*))
   (format t "Category B~%")
   (format t "12   (1122)   2.24    ~5,2f~%"
     (nth 5 *latency-results*))
   (format t " 2   (2112)   1.50    ~5,2f~%"
     (nth 6 *latency-results*))
   (format t "14   (2221)   1.14    ~5,2f~%"
     (nth 7 *latency-results*))
   (format t "10   (2222)   1.11    ~5,2f~%"
     (nth 8 *latency-results*))
   (format t "Transfer Item~%")
   (format t " 1   (1221)   1.26    ~5,2f~%"
     (nth 9 *latency-results*))
   (format t " 3   (1222)   1.43    ~5,2f~%"
     (nth 10 *latency-results*))
   (format t " 6   (1111)   1.32    ~5,2f~%"
     (nth 11 *latency-results*))
   (format t " 8   (2212)   1.32    ~5,2f~%"
     (nth 12 *latency-results*))
   (format t " 9   (2121)   1.36    ~5,2f~%"
     (nth 13 *latency-results*))
   (format t "11   (2211)   1.47    ~5,2f~%"
     (nth 14 *latency-results*))
   (format t "16   (2122)   1.55    ~5,2f~%"
     (nth 15 *latency-results*))
   (format t "----------------------------~%")
   (format t "~%Old-new recognition probabilities observed and predicted~%")
   (format t "in Palmeri and Nosofsky's Experiment 1.  The Human data is~%")
   (format t "estimated from Figure 1 (A) on page 553 of Palmeri & Nosofsky, 1995,~%~%")
   (format t "-------------------------------~%")
   (format t "Face Values  People RULEX EPAM6~%")
   (format t "-------------------------------~&")
   (format t "Category A~%")
   (format t " 4   (1112)   0.79  0.74 ~5,2f~%"
     (nth 0 *recognition-results*))
   (format t " 7   (1212)   0.70  0.65 ~5,2f~%"
     (nth 1 *recognition-results*))
   (format t "15   (1211)   0.63  0.65 ~5,2f~%"
     (nth 2 *recognition-results*))
   (format t "13   (1121)   0.85  0.75 ~5,2f~%"
     (nth 3 *recognition-results*))
   (format t " 5   (2111)   0.94  0.93 ~5,2f~%"
     (nth 4 *recognition-results*))
   (format t "Category B~%")
   (format t "12   (1122)   0.99  1.00 ~5,2f~%"
     (nth 5 *recognition-results*))
   (format t " 2   (2112)   0.75  0.74 ~5,2f~%"
     (nth 6 *recognition-results*))
   (format t "14   (2221)   0.60  0.65 ~5,2f~%"
     (nth 7 *recognition-results*))
   (format t "10   (2222)   0.79  0.65 ~5,2f~%"
     (nth 8 *recognition-results*))
   (format t "Transfer Item~%")
   (format t " 1   (1221)   0.64  0.65 ~5,2f~%"
     (nth 9 *recognition-results*))
   (format t " 3   (1222)   0.72  0.72 ~5,2f~%"
     (nth 10 *recognition-results*))
   (format t " 6   (1111)   0.71  0.72 ~5,2f~%"
     (nth 11 *recognition-results*))
   (format t " 8   (2212)   0.56  0.65 ~5,2f~%"
     (nth 12 *recognition-results*))
   (format t " 9   (2121)   0.67  0.72 ~5,2f~%"
     (nth 13 *recognition-results*))
   (format t "11   (2211)   0.66  0.70 ~5,2f~%"
     (nth 14 *recognition-results*))
   (format t "16   (2122)   0.66  0.74 ~5,2f~%"
     (nth 15 *recognition-results*))
   (format t "Means~%")
   (format t "Exceptions    0.97  0.97 ~5,2f~%"
     (/ (+ (nth 4 *recognition-results*)
           (nth 5 *recognition-results*))
        2))
   (format t "Confusables   0.80  0.75 ~5,2f~%"
     (/ (+ (nth 3 *recognition-results*)
           (nth 6 *recognition-results*))
        2))
   (format t "Other Old     0.70  0.67 ~5,2f~%"
     (/ (+ (nth 0 *recognition-results*)
           (nth 1 *recognition-results*)
           (nth 2 *recognition-results*)
           (nth 7 *recognition-results*)
           (nth 8 *recognition-results*))
        5))
   (format t "New           0.66  0.70 ~5,2f~%"
     (/ (+ (nth 9 *recognition-results*)
           (nth 10 *recognition-results*)
           (nth 11 *recognition-results*)
           (nth 12 *recognition-results*)
           (nth 13 *recognition-results*)
           (nth 14 *recognition-results*)
           (nth 15 *recognition-results*))
        7))
   (format t "----------------------------~%")
   )

(defun do-recognition-test-in-concept-formation-experiment ()
   (let (object clock-holder result)
      (setf clock-holder *clock*)
      (setf object (get-object-from-store 'stimulus 'visual 'sensory-store))
      (setf result (apply *recognition-test-routine* (list object)))
      (when (> *clock* clock-holder)
         (format t "Warning: Clock advanced during recognition test.  This could throw off the results of latencies collected during transfer phase!~%")
         )
      (when *print-strategy-updates*
         (format t "Result of Recogniton Test = ~a~%" result))
      result))

(defun recognition-test-in-rules-condition (item)
   "When a list of dimensions is specified, only those dimensions will be
considered when making an hypothesis.  For example, in the Rules and Exceptions
condition of the Medin and Smith experiment, the only dimension to be
considered when making the hypothesis was nose-length." 
   (let* ((rule (car (find-associates 'category item)))
          (type-of-exception 'exception)
          (object (put-value (copy-object item) type-of-exception 'is-a))
          (chunk (get-chunk-for object)))
      (cond ((null rule) 0)
            ('else
             (loop for test in *list-of-possible-tests*
               with image = (get-image-of chunk)
               with n = 0
               if (get-value image test)
               do (incf n)
               finally (return n))))))
 
(defun recognition-test-in-standard-condition (object)
   "When a list of dimensions is specified, only those dimensions will be
considered when making an hypothesis.  For example, in the Rules and Exceptions
condition of the Medin and Smith experiment, the only dimension to be
considered when making the hypothesis was nose-length." 
   (loop for test in *list-of-possible-tests*
     with image = (get-image-of (get-chunk-for object))
     with n = 0
     if (get-value image test)
     do (incf n)
     finally (return n)))

 (defun palmeri-and-nosofsky-exit-criteria ()
  (cond ((and (eq *phase-of-experiment* 'prelearning)
              (>= *trial* 2))
         t)
        ((and (eq *phase-of-experiment* 'learning)
              (>= *trial*  *maximum-learning-trials*))
         t)
        ((and (eq *phase-of-experiment* 'transfer)
              (>= *trial* 3))
         t)
        ((and (eq *phase-of-experiment* 'latency)
              (>= *trial* 6))
         t)
        ('else nil)))

; -----------------------

(defun nosofsky-palmeri-and-mckinley-experiment1-simulation ()
   "This experiment is from Journal of Experimental Psychology: Learning
Memory and Cognition, 1995, Vol 21, No. 3, 548-568.  "
   (setf *speed-of-learning-parameter* 40)
   (setf *other-category-parameter* 100)
   (setf *do-prelearning-phase* nil)
   (setf *errors-by-case* nil)
   (setf *overall-results* nil)
   (setf *latency-results* nil)
   (setf *recognition-results* nil)
   (setf *pattern-results* nil)
   (setf *hamming-results* nil)
   (setf *do-recognition-test* nil)
   (setf *dimension-of-rule* 'eh)
   (setf *a* '(4 7 15 13 5)) ; these case numbers are a's
   (setf *b* '(12 2 14 10)) ; these case numbers are b's
   (setf *inter-trial-interval* 0)
   (setf *presentation-rate* 3000)
   (setf *response-duration-rate* 2000)
   (setf *stimulus-duration-rate* 60000)
   (setf *number-of-sessions* 10000)
   (setf *maximum-learning-trials* 16) 
   (setf *exit-criteria-routine* 'nosofsky-palmeri-and-mckinley-exit-criteria)
   (setf *default-category* #\A)
   (setf *other-category* #\B)
   (setf *w1* .1) ; eye-height
   (setf *w2* .1) ; eye-spacing
   (setf *w3* .1) ; nose-length
   (setf *w4* .1) ; mouth-height
   (setf *other-category-parameter* 100)
   (medin-and-smith 'standard)
   (when *print-major-updates* (print-net *net* 'everything))
   (report-nosofsky-palmeri-and-mckinley-experiment1-errors)
   (report-nosofsky-palmeri-and-mckinley-experiment1-results)
   )

(defun report-nosofsky-palmeri-and-mckinley-experiment1-errors ()
   (let ((patterns (make-array 128 :initial-element 0))
         (hammings (make-array 8 :initial-element 0))
         (pattern-holder (make-array *number-of-sessions* :initial-element 0))
         (hamming-holder (make-array *number-of-sessions* :initial-element 0))
         )
      (format t "There were ~a subjects who had perfect runs.~%"
        (loop for result in (aref *learning-array* 0)
          count (> result 0)))
      (loop for case in '(4 7 15 13 5 12 2 14 10 1 3 6 8 9 11 16)
        for number-of-a = (loop for remainder on (aref *transfer-array* case) by #'cdddr
                            with result = 0
                            do (loop repeat 3
                                 for item in remainder
                                 if (eql item #\A)
                                 do (incf result))
                            finally (return result))
        for correct = (/ number-of-a (* 3.0 *number-of-sessions*))
        do (when *print-major-updates* 
              (format t "For case = ~a, Number of report a's = ~a~%" case correct)
              )
        (setf *overall-results* (append *overall-results* (list correct)))
        )
      (loop for case in '(1 3 6 8 9 11 16)
        for exponent from 0
        for n = (expt 2 exponent)
        do (loop for errors-tail on (aref *transfer-array* case) by #'cdddr
             for response = (loop repeat 3
                              with result = 0
                              for item in errors-tail
                              if (eql item #\A)
                              do (incf result)
                              finally (return (cond ((> result 1) #\A)
                                                    ('else #\B))))
             for hamming = (cond ((eql (second errors-tail) (third errors-tail)) 0)
                                ('else 1))
             for session from 0
             if (= hamming 1)
             do (incf (aref hamming-holder session))
             if (eql response #\A)
             do (setf (aref pattern-holder session)
                      (+ n (aref pattern-holder session)))))
      (loop for n from 0 upto (1- *number-of-sessions*)
        for pattern = (aref pattern-holder n)
        do (incf (aref patterns pattern)))
      (loop for n from 0 upto (1- *number-of-sessions*)
        for hamming = (aref hamming-holder n)
        do (incf (aref hammings hamming)))
      ;; Now, report the results in terms of proportion of total patterns
      (when *print-major-updates* (format t "Results by pattern:~%"))
      (loop for hamming-number from 0 upto 7
        do (setf *hamming-results* 
                 (append *hamming-results* (list (aref hammings hamming-number)))))
      (loop for pattern-number in '(127 47 55 23 39 71 7 83 3 61 45 117 53 85 21 
                                    37 5 41 49 65 46 38 6 2 124 60 44 12 116 52 
                                    84 20 36 68 4 24)
        for pattern-string = (convert-pattern-number-to-string pattern-number 7)
        for number-of-instances = (* 1.0 (aref patterns pattern-number))
        for proportion = (/ number-of-instances *number-of-sessions*)
        sum number-of-instances into total-accounted-for
        do (when *print-major-updates* 
              (format t "For pattern = ~a, probability = ~a~%" 
                pattern-string proportion))
        (setf *pattern-results* (append *pattern-results* (list proportion)
                                  ))
        finally (progn                   
                  (when *print-major-updates* 
                     (format t "For pattern = other, probability = ~a~%"
                       (/ (- *number-of-sessions* total-accounted-for)
                          *number-of-sessions*)))
                  (setf *pattern-results*
                        (cons (/ (- *number-of-sessions* total-accounted-for)
                                 *number-of-sessions*)
                          *pattern-results*))))
      ))

(defun report-nosofsky-palmeri-and-mckinley-experiment1-results ()
   (format t "~%Categorization Response Probabilities of reporting that item is~%")
   (format t "a member of Category A during the Transfer Condition.  The Human ~%")
   (format t "data and the RULEX data are from Nosofsky, Palmeri & McKinley, 1994,~%~%")
   (format t "---------------------------------------~%")
   (format t "Face Values  People RULEX Context  EPAM~%")
   (format t "---------------------------------------~%")
   (format t "Category A~%")
   (format t " 4   (1112)   0.77   0.79   0.79  ~5,2f~%"
     (nth 0 *overall-results*))
   (format t " 7   (1212)   0.78   0.83   0.79  ~5,2f~%"
     (nth 1 *overall-results*))
   (format t "15   (1211)   0.83   0.88   0.77  ~5,2f~%"
     (nth 2 *overall-results*))
   (format t "13   (1121)   0.64   0.65   0.65  ~5,2f~%"
     (nth 3 *overall-results*))
   (format t " 5   (2111)   0.61   0.64   0.63  ~5,2f~%"
     (nth 4 *overall-results*))
   (format t "Category B~%")
   (format t "12   (1122)   0.39   0.45   0.40  ~5,2f~%"
     (nth 5 *overall-results*))
   (format t " 2   (2112)   0.41   0.44   0.40  ~5,2f~%"
     (nth 6 *overall-results*))
   (format t "14   (2221)   0.21   0.23   0.21  ~5,2f~%"
     (nth 7 *overall-results*))
   (format t "10   (2222)   0.15   0.16   0.19  ~5,2f~%"
     (nth 8 *overall-results*))
   (format t "Transfer Item~%")
   (format t " 1   (1221)   0.56   0.62   0.58  ~5,2f~%"
     (nth 9 *overall-results*))
   (format t " 3   (1222)   0.41   0.47   0.47  ~5,2f~%"
     (nth 10 *overall-results*))
   (format t " 6   (1111)   0.82   0.85   0.79  ~5,2f~%"
     (nth 11 *overall-results*))
   (format t " 8   (2212)   0.40   0.45   0.45  ~5,2f~%"
     (nth 12 *overall-results*))
   (format t " 9   (2121)   0.32   0.34   0.33  ~5,2f~%"
     (nth 13 *overall-results*))
   (format t "11   (2211)   0.53   0.61   0.56  ~5,2f~%"
     (nth 14 *overall-results*))
   (format t "16   (2122)   0.20   0.22   0.22  ~5,2f~%"
     (nth 15 *overall-results*))
   
   (loop with sum-of-squares = 0
     for n from 0 upto 16
     for people in '(.77 .78 .83 .64 .61 .39 .41 .21 .15 .56 .41 .82 .4 .32 .53 .2)
     for epam = (nth n *overall-results*)
     for diff = (expt (- people epam) 2)
     do (setf sum-of-squares (+ sum-of-squares diff))
     finally (format t "Sum of squares for EPAM = ~a~%" sum-of-squares))
   (format t "---------------------------------------~%")
   (format t "~%Generalization Pattern Probabilities during transfer stage~%")
   (format t "The Human data, context model data, and RULEX data are estimated from the~%") 
   (format t "Figures 9, 10, and 11 of Nosofsky, Palmeri & McKinley, 1994,~%~%")
   (format t "---------------------------------------~%")
   (format t "Pattern  People   Rulex Context    EPAM~%")
   (format t "AAAAAAA   0.009   0.003   0.003   ~5,3f~%" (nth 1 *pattern-results*))
   (format t "AAAABAB   0.013   0.012   0.033   ~5,3f~%" (nth 2 *pattern-results*))
   (format t "AAABAAB   0.013   0.011   0.012   ~5,3f~%" (nth 3 *pattern-results*))
   (format t "AAABABB   0.013   0.036   0.006   ~5,3f~%" (nth 4 *pattern-results*))
   (format t "AAABBAB   0.026   0.034   0.031   ~5,3f~%" (nth 5 *pattern-results*))
   (format t "AAABBBA   0.013   0.012   0.004   ~5,3f~%" (nth 6 *pattern-results*))
   (format t "AAABBBB   0.141   0.122   0.015   ~5,3f~%" (nth 7 *pattern-results*))
   (format t "AABBABA   0.009   0.004   0.000   ~5,3f~%" (nth 8 *pattern-results*))
   (format t "AABBBBB   0.013   0.013   0.001   ~5,3f~%" (nth 9 *pattern-results*))
   (format t "ABAAAAB   0.018   0.011   0.025   ~5,3f~%" (nth 10 *pattern-results*))
   (format t "ABAABAB   0.022   0.034   0.062   ~5,3f~%" (nth 11 *pattern-results*))
   (format t "ABABAAA   0.009   0.004   0.005   ~5,3f~%" (nth 12 *pattern-results*))
   (format t "ABABAAB   0.026   0.015   0.024   ~5,3f~%" (nth 13 *pattern-results*))
   (format t "ABABABA   0.009   0.005   0.003   ~5,3f~%" (nth 14 *pattern-results*))
   (format t "ABABABB   0.009   0.014   0.012   ~5,3f~%" (nth 15 *pattern-results*))
   (format t "ABABBAB   0.069   0.029   0.058   ~5,3f~%" (nth 16 *pattern-results*))
   (format t "ABABBBB   0.026   0.035   0.029   ~5,3f~%" (nth 17 *pattern-results*))
   (format t "ABBABAB   0.013   0.006   0.005   ~5,3f~%" (nth 18 *pattern-results*))
   (format t "ABBBAAB   0.009   0.003   0.001   ~5,3f~%" (nth 19 *pattern-results*))
   (format t "ABBBBBA   0.009   0.003   0.000   ~5,3f~%" (nth 20 *pattern-results*))
   (format t "BAAABAB   0.013   0.012   0.034   ~5,3f~%" (nth 21 *pattern-results*))
   (format t "BAABBAB   0.009   0.009   0.031   ~5,3f~%" (nth 22 *pattern-results*))
   (format t "BAABBBB   0.013   0.012   0.016   ~5,3f~%" (nth 23 *pattern-results*))
   (format t "BABBBBB   0.009   0.003   0.001   ~5,3f~%" (nth 24 *pattern-results*))
   (format t "BBAAAAA   0.013   0.007   0.005   ~5,3f~%" (nth 25 *pattern-results*))
   (format t "BBAAAAB   0.031   0.035   0.026   ~5,3f~%" (nth 26 *pattern-results*))
   (format t "BBAABAB   0.132   0.126   0.064   ~5,3f~%" (nth 27 *pattern-results*))
   (format t "BBAABBB   0.013   0.011   0.032   ~5,3f~%" (nth 28 *pattern-results*))
   (format t "BBABAAA   0.018   0.004   0.005   ~5,3f~%" (nth 29 *pattern-results*))
   (format t "BBABAAB   0.018   0.017   0.024   ~5,3f~%" (nth 30 *pattern-results*))
   (format t "BBABABA   0.009   0.004   0.003   ~5,3f~%" (nth 31 *pattern-results*))
   (format t "BBABABB   0.031   0.006   0.012   ~5,3f~%" (nth 32 *pattern-results*))
   (format t "BBABBAB   0.035   0.035   0.059   ~5,3f~%" (nth 33 *pattern-results*))
   (format t "BBABBBA   0.009   0.004   0.006   ~5,3f~%" (nth 34 *pattern-results*))
   (format t "BBABBBB   0.044   0.009   0.030   ~5,3f~%" (nth 35 *pattern-results*))
   (format t "BBBAABB   0.009   0.004   0.001   ~5,3f~%" (nth 36 *pattern-results*))
   (format t "---------------------------------------~%")
   (format t "---------------------------------------~%")
   (format t "~%Hamming distance probabilities between first~%")
   (format t "and second blocks during transfer stage~%")
   (format t "The Human data, RULEX data, and context model data~%")
   (format t "are estimated from Figure 12 of ~%") 
   (format t "Nosofsky, Palmeri & McKinley, 1994,~%~%")
   (format t "---------------------------------------~%")
   (format t "Hamming~%")
   (format t "Distance People Rulex Context  EPAM~%")
   (format t "    0     0.26   0.26   0.03  ~5,2f~%" (/ (nth 0 *hamming-results*) *number-of-sessions*))
   (format t "    1     0.25   0.31   0.14  ~5,2f~%" (/ (nth 1 *hamming-results*) *number-of-sessions*))
   (format t "    2     0.23   0.18   0.28  ~5,2f~%" (/ (nth 2 *hamming-results*) *number-of-sessions*))
   (format t "    3     0.16   0.11   0.29  ~5,2f~%" (/ (nth 3 *hamming-results*) *number-of-sessions*))
   (format t "    4     0.09   0.08   0.18  ~5,2f~%" (/ (nth 4 *hamming-results*) *number-of-sessions*))
   (format t "    5     0.01   0.05   0.06  ~5,2f~%" (/ (nth 5 *hamming-results*) *number-of-sessions*))
   (format t "    6     0.01   0.02   0.01  ~5,2f~%" (/ (nth 6 *hamming-results*) *number-of-sessions*))
   (format t "    7     0.00   0.00   0.00  ~5,2f~%" (/ (nth 7 *hamming-results*) *number-of-sessions*))
   (format t "---------------------------------------~%")
   )

(defun nosofsky-palmeri-and-mckinley-exit-criteria ()
  (cond ((and (eq *phase-of-experiment* 'learning)
              (>= *trial*  *maximum-learning-trials*))
         t)
        ((and (eq *phase-of-experiment* 'transfer)
              (>= *trial* 3))
         t)
        ((and (eq *phase-of-experiment* 'latency)
              (>= *trial* 1))
         t)
        ('else nil)))

(defun convert-pattern-number-to-string (n string-length)
  "Note the number can range from 0 to 255."
  (loop for n1 from 0 to (1- string-length)
        for n2 = (expt 2 n1)
        if (= 0 (logand n n2))
        collect #\b into result
        else if t
        collect #\a into result
        finally (return (coerce result 'string))))

(defun find-pattern-string-of-last-run ()
   (loop for case in '(1 3 6 8 9 11 16)
     for exponent from 0
     for n = (expt 2 exponent)
     for errors-tail = (reverse (aref *transfer-array* case))
     with pattern-holder = 0
     for response = (loop repeat 3
                      with result = 0
                      for item in errors-tail
                      if (eql item #\A)
                      do (incf result)
                      finally (return (cond ((> result 1) #\A)
                                            ('else #\B))))
     if (eql response #\A)
     do (setf pattern-holder (+ n pattern-holder))
     finally (return (convert-pattern-number-to-string pattern-holder 7))))




; ------------------------------------------

(defun shepard-hovland-jenkins-simulation ()
  "This routine performs a simulation of Medin & Smith's (1978) study of 
concept learning under strategic control.  Several global-parameters can 
change the conditions of this simulation:
   *a* lists the case numbers that will be considered to be category a
   *b* lists the case numbers that will be considered to be category b
   *inter-trial-interval* is the amount of time between trials
   *presentation-rate* is the amount of time in msec between presentation of the
                       response and presentation of the next stimulus
   *stimulus-duration-rate* this should be set very high for these 
                            experiments, perhaps a minute of longer, 
                            because the stimulus is kept in front of the
                            subject until the subject responds.
   *response-duration-rate* is the amount of time between when the response
                            is exposed with the stimulus before it is taken
                            away from view.  This number should be lower 
                            than the *presentation-rate*
   *number-of-sessions* determines the number of trials that will occur in 
                        each condition."
   (setf *speed-of-learning-parameter* 100)
   (setf *other-category-parameter* 100)
   (setf *do-prelearning-phase* nil)
   (setf *errors-by-case* nil)
   (setf *overall-results* nil)
   (setf *latency-results* nil)
   (setf *recognition-results* nil)
   (setf *pattern-results* nil)
   (setf *hamming-results* nil)
   (setf *do-recognition-test* nil)
   (setf *inter-trial-interval* 0)
   (setf *presentation-rate* 3000)
   (setf *response-duration-rate* 2000)
   (setf *stimulus-duration-rate* 60000)
   (setf *number-of-sessions* 1000)
   (setf *maximum-learning-trials* 25)
   (setf *exit-criteria-routine* 'shepard-hovland-jenkins-exit-criteria)
   (setf *w1* 1.0) 
   (setf *w2* 1.0) 
   (setf *w3* 1.0)
   (setf *other-category-parameter* 100)
   (setf *default-category* #\A)
   (setf *other-category* #\B)
   (make-shepard-hovland-jenkins-cases)
   (loop for type in '(
                       type1 
                        type2 
                        type3 
                        type4 
                        type5 
                        type6
                        )
          do (shepard-hovland-jenkins type))
   (report-results-of-shepard-hovland-jenkins-simulation)
   )
   

(defun shepard-hovland-jenkins-exit-criteria ()
  (or (>= *number-of-consecutive-perfect-trials* 2)
      (>= *trial* *maximum-learning-trials*)))


(defun shepard-hovland-jenkins (type)
  "This routine sets the following variables which govern the subject routine 
depending upon the condition:
          *find-category-routine*
          *study-routine*
and also initialized the following variables which are used by the 
experimenter routine to collect results:
      *learning-array* an array which shows the number of errors made by the 
                     subject for each case when the *phase-of-experiment* is 
                     'learning.  The the 0 cell in the array shows how many
                     subjects reached the criteria of a perfect trial.
      *transfer-array* an array which shows the number of errors made by the subject
                 for each case when the *phase-of-experiment* is 'transfer.
      *latency-array* an array which shows the number of errors made by the subject
                 for each case when the *phase-of-experiment* is 'latency."
   (setf *find-category-routine* 'find-category-in-standard-condition)
   (setf *study-routine* 'study-in-standard-condition)
   (cond ((eq type 'type1) 
          (setf *a* '(1 2 3 4))
          (setf *b* '(5 6 7 8)))
         ((eq type 'type2)
          (setf *a* '(1 2 7 8))
          (setf *b* '(3 4 5 6)))
         ((eq type 'type3)
          (setf *a* '(1 2 3 6))
          (setf *b* '(4 5 7 8)))
         ((eq type 'type4)
          (setf *a* '(1 2 3 5))
          (setf *b* '(4 6 7 8)))
         ((eq type 'type5)
          (setf *a* '(1 2 3 8))
          (setf *b* '(4 5 6 7)))
         ((eq type 'type6)
          (setf *a* '(1 4 6 7))
          (setf *b* '(2 3 5 8))))
   (setf *categories* '(#\A #\B))
   (setf *learning-array* (make-array 9 :initial-element nil)); array used to 
   ; hold errors during learning phase
   (setf *errors-per-block-array* (make-array *maximum-learning-trials* :initial-element nil)) ; errors during
   (loop repeat *number-of-sessions*
     do (loop initially (setf (aref *learning-array* 0)
                              (cons 0 (aref *learning-array* 0)))
          for n from 1 to 8
          do (setf (aref *learning-array* n) 
                   (cons nil (aref *learning-array* n))))
     (shepard-hovland-jenkins-experiment))
   (report-shepard-hovland-jenkins-errors type))

(defun shepard-hovland-jenkins-experiment ()
  "This routine runs a single subject through a medin-alton-freko experiment.
It is responsible for initializing all of the following variables.
      *sr-list* is the list of pairs of stimulus and response objects.
      *position* is the position.  If it is an even position then the next item
                 to place in the store is a stimulus.  If it is an odd position
                 then the next item to place in the store is the response.
                 if the position is the length of the sr-list then have reached
                 the end of the list and it is time to start over
      *trial* is the number of the trial, it gets incremented everytime the
              routine starts over.
      *starting-errors* is the number of total errors at the beginning of a 
                        trial if at the end of a trial it is equal to *errors*
                        then no errors have occurred on that trial.
      *output* is the response made by the subject routine
      *number-of-consecutive-perfect-trials* is the number of consecutive times 
                                             through the list without an error
                                             it should be 0 at beginning.
      *phase-of-experiment* may be 'learning 'transfer, 'latency, 'shepard and
                 so on.  The value of this variable determines what arrays are
                 filled with information by the experiment via the 
                 test-correctness-of-categorization-output routine and also
                 the exit criteria of the experiment via the 
                 exit-criteria routine.
      *consecutive-correct* must be set at zero at the beginning of a trial."
   (initialize-variables)
   (setf *experimenter* 'concept-formation-experimenter)
   (setf *phase-of-experiment* 'learning)
   (setf *sr-list* (loop for case in (reorder-list-randomly (append *cases* 
                                                                   *cases*))
                        for case-number = (get-value case 'case-number)
                        for correct-response = (cond ((member case-number *a*) #\A)
                                                     ('else #\B))
                        collect (list case correct-response)))
  (zero-out-clocks)
  (setf *position* 0
        *trial* 0
        *starting-errors* 0
        *output* nil
        *number-of-consecutive-perfect-trials* 0
        *new-stimulus-flag* nil
        *errors* 0
        (get 'visual 'imagery-store) nil
        *consecutive-correct*  0)
  (when *print-experimenter-updates* 
    (format t "~%~%~%Beginning Learning Phase here!~%"))
  (concept-formation-strategy))

(defun report-shepard-hovland-jenkins-errors (type)
   (let ((errors-by-block (make-array (1+ *maximum-learning-trials*) :initial-element 0))
         (central-items (cond ((eq type 'type3)'(1 2 7 8))
                              ((eq type 'type4)'(1 8))
                              ((eq type 'type5)'(1 5))
                              ('else nil)))
         (peripheral-items (cond ((eq type 'type3)'(3 4 5 6))
                                 ((eq type 'type4)'(2 7))
                                 ((eq type 'type5)'(2 3 6 7))
                                 ('else nil)))
         (exception-items (cond ((eq type 'type5)'(4 8))
                                ('else nil)))
         (central-errors-by-block (make-array (1+ *maximum-learning-trials*) :initial-element 0))
         (peripheral-errors-by-block (make-array (1+ *maximum-learning-trials*) :initial-element 0))
         (exception-errors-by-block (make-array (1+ *maximum-learning-trials*) :initial-element 0)))
      (format t "Results for ~a~%" type)
      (format t "~a subjects reached the criterion of 2 perfect blocks.~%"
        (loop for criterion in (aref *learning-array* 0)
          count (> criterion 1)))
      (loop with trials-to-criterion = 0
        for result-run in (aref *learning-array* 1)
        for number-of-trials = (/ (length result-run) 2.0)
        do (setf trials-to-criterion (+ trials-to-criterion number-of-trials))
        finally (progn 
                  (setf *overall-results* 
                        (append *overall-results* 
                          (list (/ trials-to-criterion
                                   *number-of-sessions*))))
                  (when *print-major-updates* 
                     (format t "Average trials-to-criterion = ~a~%"
                       (* 1.0 (/ trials-to-criterion
                                 *number-of-sessions*))))))
      (loop for case from 1 upto 8
        for correct-answer = (cond ((member case *a*) #\A)
                                   ('else #\B))
          do (loop for result-run in (aref *learning-array* case)
               do (loop for result-list on result-run by #'cddr
                    for result1 = (first result-list)
                    for result2 = (second result-list)
                    for n from 1
                    if (not (eq result1 correct-answer))
                    do (incf (aref errors-by-block n))
                    if (not (eq result2 correct-answer))
                    do (incf (aref errors-by-block n)))))
      (loop for block from 1 upto 25
        for total-errors = (aref errors-by-block block)
        for average-error = (/ total-errors (* 16.0 *number-of-sessions*))
        sum total-errors into errors-to-criterion
        do (when *print-major-updates* 
              (format t "For block ~a, error proportion = ~a~%" block 
                average-error))
        (setf *errors-by-case*
              (append *errors-by-case* (list average-error)))
        finally (when *print-major-updates* 
                   (format t "Average errors to criterion = ~a~%"
                     (* 1.0 
                        (/ errors-to-criterion *number-of-sessions*)))))
      (when central-items
         (loop for case in central-items
           for correct-answer = (cond ((member case *a*) #\A)
                                      ('else #\B))
           do (loop for result-run in (aref *learning-array* case)
                do (loop for result-list on result-run by #'cddr
                     for result1 = (first result-list)
                     for result2 = (second result-list)
                     for n from 1
                     if (not (eq result1 correct-answer))
                     do (incf (aref central-errors-by-block n))
                     if (not (eq result2 correct-answer))
                     do (incf (aref central-errors-by-block n)))))
         (loop for block from 1 upto 25
           for total-errors = (aref central-errors-by-block block)
           for average-error = (/ total-errors (* 2.0 (length central-items) *number-of-sessions*))
           sum total-errors into errors-to-criterion
           do (when *print-major-updates* 
                 (format t "For block ~a, central error proportion = ~a~%" block 
                   average-error))
           (setf *errors-by-case*
                 (append *errors-by-case* (list average-error)))
           finally (when *print-major-updates*
                      (format t "Average central-errors to criterion = ~a~%"
                        (* 1.0 
                           (/ errors-to-criterion *number-of-sessions*))))))
      (when peripheral-items
         (loop for case in peripheral-items
           for correct-answer = (cond ((member case *a*) #\A)
                                      ('else #\B))
           do (loop for result-run in (aref *learning-array* case)
                do (loop for result-list on result-run by #'cddr
                     for result1 = (first result-list)
                     for result2 = (second result-list)
                     for n from 1
                     if (not (eq result1 correct-answer))
                     do (incf (aref peripheral-errors-by-block n))
                     if (not (eq result2 correct-answer))
                     do (incf (aref peripheral-errors-by-block n)))))
         (loop for block from 1 upto 25
           for total-errors = (aref peripheral-errors-by-block block)
           for average-error = (/ total-errors (* 2.0 (length peripheral-items) *number-of-sessions*))
           sum total-errors into errors-to-criterion
           do (when *print-major-updates* 
                 (format t "For block ~a, peripheral error proportion = ~a~%" block 
                       average-error))
           (setf *errors-by-case*
                 (append *errors-by-case* (list average-error)))
           finally (when *print-major-updates* 
                      (format t "Average peripheral-errors to criterion = ~a~%"
                        (* 1.0 
                           (/ errors-to-criterion *number-of-sessions*)))))
         )
      (when exception-items
         (loop for case in exception-items
           for correct-answer = (cond ((member case *a*) #\A)
                                       ('else #\B))
           do (loop for result-run in (aref *learning-array* case)
                do (loop for result-list on result-run by #'cddr
                     for result1 = (first result-list)
                     for result2 = (second result-list)
                     for n from 1
                     if (not (eq result1 correct-answer))
                     do (incf (aref exception-errors-by-block n))
                     if (not (eq result2 correct-answer))
                     do (incf (aref exception-errors-by-block n)))))
         (loop for block from 1 upto 25
           for total-errors = (aref exception-errors-by-block block)
           for average-error = (/ total-errors (* 2.0 (length exception-items) *number-of-sessions*))
           sum total-errors into errors-to-criterion
           do (when *print-major-updates* 
                 (format t "For block ~a, exception error proportion = ~a~%" block 
                   average-error))
           (setf *errors-by-case*
                 (append *errors-by-case* (list average-error)))
           finally (when *print-major-updates* 
                      (format t "Average exception-errors to criterion = ~a~%"
                        (* 1.0 
                           (/ errors-to-criterion *number-of-sessions*)))))
         )
      ))



(defun report-results-of-shepard-hovland-jenkins-simulation ()
   (format t "~%Average Error Proportions for Each of Problem Types 1-VI,~%")
   (format t "in Blocks 1-25 of Learning.  Human data is from Nosofsky,~%")
   (format t "Bluck, Palmeri, McKinley, and Glauthier Table 1.~%~%")
   (format t "-----------------------------------------------------------------~%")
   (format t "               People                           EPAM VI~%")
   (format t "      ----------------------------   ----------------------------~%")          
   (format t "Block   I   II  III   IV    V   VI     I   II  III   IV    V   VI~%")
   (format t "-----------------------------------------------------------------~%")
   (format t "  1  0.21 0.38 0.46 0.42 0.47 0.50  ~5,2f~5,2f~5,2f~5,2f~5,2f~5,2f~%"
     (nth 0 *errors-by-case*) (nth 25 *errors-by-case*) (nth 50 *errors-by-case*) (nth 125 *errors-by-case*) (nth 200 *errors-by-case*) (nth 300 *errors-by-case*))
   (format t "  2  0.03 0.16 0.29 0.30 0.33 0.34  ~5,2f~5,2f~5,2f~5,2f~5,2f~5,2f~%"
     (nth 1 *errors-by-case*) (nth 26 *errors-by-case*) (nth 51 *errors-by-case*) (nth 126 *errors-by-case*) (nth 201 *errors-by-case*) (nth 301 *errors-by-case*))
   (format t "  3  0.00 0.08 0.22 0.22 0.23 0.28  ~5,2f~5,2f~5,2f~5,2f~5,2f~5,2f~%"
     (nth 2 *errors-by-case*) (nth 27 *errors-by-case*) (nth 52 *errors-by-case*) (nth 127 *errors-by-case*) (nth 202 *errors-by-case*) (nth 302 *errors-by-case*))
   (format t "  4  0.00 0.06 0.15 0.17 0.14 0.25  ~5,2f~5,2f~5,2f~5,2f~5,2f~5,2f~%"
     (nth 3 *errors-by-case*) (nth 28 *errors-by-case*) (nth 53 *errors-by-case*) (nth 128 *errors-by-case*) (nth 203 *errors-by-case*) (nth 303 *errors-by-case*))
   (format t "  5  0.00 0.03 0.08 0.15 0.11 0.22  ~5,2f~5,2f~5,2f~5,2f~5,2f~5,2f~%"
     (nth 4 *errors-by-case*) (nth 29 *errors-by-case*) (nth 54 *errors-by-case*) (nth 129 *errors-by-case*) (nth 204 *errors-by-case*) (nth 304 *errors-by-case*))
   (format t "  6  0.00 0.03 0.08 0.11 0.08 0.19  ~5,2f~5,2f~5,2f~5,2f~5,2f~5,2f~%"
     (nth 5 *errors-by-case*) (nth 30 *errors-by-case*) (nth 55 *errors-by-case*) (nth 130 *errors-by-case*) (nth 205 *errors-by-case*) (nth 305 *errors-by-case*))
   (format t "  7  0.00 0.03 0.06 0.09 0.07 0.19  ~5,2f~5,2f~5,2f~5,2f~5,2f~5,2f~%"
     (nth 6 *errors-by-case*) (nth 31 *errors-by-case*) (nth 56 *errors-by-case*) (nth 131 *errors-by-case*) (nth 206 *errors-by-case*) (nth 306 *errors-by-case*))
   (format t "  8  0.00 0.02 0.03 0.06 0.08 0.18  ~5,2f~5,2f~5,2f~5,2f~5,2f~5,2f~%"
     (nth 7 *errors-by-case*) (nth 32 *errors-by-case*) (nth 57 *errors-by-case*) (nth 132 *errors-by-case*) (nth 207 *errors-by-case*) (nth 307 *errors-by-case*))
   (format t "  9  0.00 0.02 0.02 0.03 0.05 0.17  ~5,2f~5,2f~5,2f~5,2f~5,2f~5,2f~%"
     (nth 8 *errors-by-case*) (nth 33 *errors-by-case*) (nth 58 *errors-by-case*) (nth 133 *errors-by-case*) (nth 208 *errors-by-case*) (nth 308 *errors-by-case*))
   (format t " 10  0.00 0.01 0.02 0.03 0.05 0.13  ~5,2f~5,2f~5,2f~5,2f~5,2f~5,2f~%"
     (nth 9 *errors-by-case*) (nth 34 *errors-by-case*) (nth 59 *errors-by-case*) (nth 134 *errors-by-case*) (nth 209 *errors-by-case*) (nth 309 *errors-by-case*))
   (format t " 11  0.00 0.00 0.02 0.02 0.05 0.14  ~5,2f~5,2f~5,2f~5,2f~5,2f~5,2f~%"
     (nth 10 *errors-by-case*) (nth 35 *errors-by-case*) (nth 60 *errors-by-case*) (nth 135 *errors-by-case*) (nth 210 *errors-by-case*) (nth 310 *errors-by-case*))
   (format t " 12  0.00 0.00 0.01 0.03 0.04 0.12  ~5,2f~5,2f~5,2f~5,2f~5,2f~5,2f~%"
     (nth 11 *errors-by-case*) (nth 36 *errors-by-case*) (nth 61 *errors-by-case*) (nth 136 *errors-by-case*) (nth 211 *errors-by-case*) (nth 311 *errors-by-case*))
   (format t " 13  0.00 0.01 0.01 0.01 0.03 0.10  ~5,2f~5,2f~5,2f~5,2f~5,2f~5,2f~%"
     (nth 12 *errors-by-case*) (nth 37 *errors-by-case*) (nth 62 *errors-by-case*) (nth 137 *errors-by-case*) (nth 212 *errors-by-case*) (nth 312 *errors-by-case*))
   (format t " 14  0.00 0.00 0.01 0.00 0.03 0.10  ~5,2f~5,2f~5,2f~5,2f~5,2f~5,2f~%"
     (nth 13 *errors-by-case*) (nth 38 *errors-by-case*) (nth 63 *errors-by-case*) (nth 138 *errors-by-case*) (nth 213 *errors-by-case*) (nth 313 *errors-by-case*))
   (format t " 15  0.00 0.00 0.01 0.00 0.02 0.11  ~5,2f~5,2f~5,2f~5,2f~5,2f~5,2f~%"
     (nth 14 *errors-by-case*) (nth 39 *errors-by-case*) (nth 64 *errors-by-case*) (nth 139 *errors-by-case*) (nth 214 *errors-by-case*) (nth 314 *errors-by-case*))
   (format t " 16  0.00 0.00 0.01 0.00 0.01 0.11  ~5,2f~5,2f~5,2f~5,2f~5,2f~5,2f~%"
     (nth 15 *errors-by-case*) (nth 40 *errors-by-case*) (nth 65 *errors-by-case*) (nth 140 *errors-by-case*) (nth 215 *errors-by-case*) (nth 315 *errors-by-case*))
   (format t " 17  0.00 0.00 0.01 0.00 0.01 0.08  ~5,2f~5,2f~5,2f~5,2f~5,2f~5,2f~%"
     (nth 16 *errors-by-case*) (nth 41 *errors-by-case*) (nth 66 *errors-by-case*) (nth 141 *errors-by-case*) (nth 216 *errors-by-case*) (nth 316 *errors-by-case*))
   (format t " 18  0.00 0.00 0.01 0.00 0.01 0.08  ~5,2f~5,2f~5,2f~5,2f~5,2f~5,2f~%"
     (nth 17 *errors-by-case*) (nth 42 *errors-by-case*) (nth 67 *errors-by-case*) (nth 142 *errors-by-case*) (nth 217 *errors-by-case*) (nth 317 *errors-by-case*))
   (format t " 19  0.00 0.00 0.01 0.00 0.01 0.08  ~5,2f~5,2f~5,2f~5,2f~5,2f~5,2f~%"
     (nth 18 *errors-by-case*) (nth 43 *errors-by-case*) (nth 68 *errors-by-case*) (nth 143 *errors-by-case*) (nth 218 *errors-by-case*) (nth 318 *errors-by-case*))
   (format t " 20  0.00 0.00 0.00 0.00 0.01 0.06  ~5,2f~5,2f~5,2f~5,2f~5,2f~5,2f~%"
     (nth 19 *errors-by-case*) (nth 44 *errors-by-case*) (nth 69 *errors-by-case*) (nth 144 *errors-by-case*) (nth 219 *errors-by-case*) (nth 319 *errors-by-case*))
   (format t " 21  0.00 0.00 0.01 0.00 0.01 0.06  ~5,2f~5,2f~5,2f~5,2f~5,2f~5,2f~%"
     (nth 20 *errors-by-case*) (nth 45 *errors-by-case*) (nth 70 *errors-by-case*) (nth 145 *errors-by-case*) (nth 220 *errors-by-case*) (nth 320 *errors-by-case*))
   (format t " 22  0.00 0.00 0.00 0.00 0.01 0.04  ~5,2f~5,2f~5,2f~5,2f~5,2f~5,2f~%"
     (nth 21 *errors-by-case*) (nth 46 *errors-by-case*) (nth 71 *errors-by-case*) (nth 146 *errors-by-case*) (nth 221 *errors-by-case*) (nth 321 *errors-by-case*))
   (format t " 23  0.00 0.00 0.00 0.00 0.01 0.04  ~5,2f~5,2f~5,2f~5,2f~5,2f~5,2f~%"
     (nth 22 *errors-by-case*) (nth 47 *errors-by-case*) (nth 72 *errors-by-case*) (nth 147 *errors-by-case*) (nth 222 *errors-by-case*) (nth 322 *errors-by-case*))
   (format t " 24  0.00 0.00 0.01 0.00 0.01 0.03  ~5,2f~5,2f~5,2f~5,2f~5,2f~5,2f~%"
     (nth 23 *errors-by-case*) (nth 48 *errors-by-case*) (nth 73 *errors-by-case*) (nth 148 *errors-by-case*) (nth 223 *errors-by-case*) (nth 323 *errors-by-case*))
   (format t " 25  0.00 0.00 0.00 0.00 0.01 0.04  ~5,2f~5,2f~5,2f~5,2f~5,2f~5,2f~%"
     (nth 24 *errors-by-case*) (nth 49 *errors-by-case*) (nth 74 *errors-by-case*) (nth 149 *errors-by-case*) (nth 224 *errors-by-case*) (nth 324 *errors-by-case*))
   (format t "Ave  0.01 0.03 0.06 0.07 0.08 0.14  ~5,2f~5,2f~5,2f~5,2f~5,2f~5,2f~%"
     (/ (loop for n from 0 upto 24 sum (nth n *errors-by-case*)) 25)
     (/ (loop for n from 25 upto 49 sum (nth n *errors-by-case*)) 25)
     (/ (loop for n from 50 upto 74 sum (nth n *errors-by-case*)) 25)
     (/ (loop for n from 125 upto 149 sum (nth n *errors-by-case*)) 25)
     (/ (loop for n from 200 upto 224 sum (nth n *errors-by-case*)) 25)
     (/ (loop for n from 300 upto 324 sum (nth n *errors-by-case*)) 25)
     )
   (format t "~%Observed and predicted learning data for the Type III problem,~%")
   (format t "in Blocks 1-16 of Learning.  Human data is from Nosofsky,~%")
   (format t "Bluck, Palmeri, McKinley, and Glauthier as estimated from~%")
   (format t "Figure 7 of Nosofsky, Palmeri, and McKinley, 1994. RULEX~%")
   (format t "data is from the same Figure.~%~%")
   (format t "------------------------------------------------------------------~%")
   (format t "              People             RULEX             EPAM VI~%")
   (format t "       ------------------ ------------------ ------------------~%")          
   (format t "Block  Central Peripheral Central Peripheral Central Peripheral~%")
   (format t "---------------------------------------------------------------~%")
   (format t "  1      0.41     0.52      0.36     0.58     ~5,2f    ~5,2f~%"
     (nth 75 *errors-by-case*) (nth 100 *errors-by-case*))
   (format t "  2      0.23     0.35      0.11     0.35     ~5,2f    ~5,2f~%"
     (nth 76 *errors-by-case*) (nth 101 *errors-by-case*))
   (format t "  3      0.17     0.28      0.06     0.23     ~5,2f    ~5,2f~%"
     (nth 77 *errors-by-case*) (nth 102 *errors-by-case*))
   (format t "  4      0.13     0.16      0.04     0.17     ~5,2f    ~5,2f~%"
     (nth 78 *errors-by-case*) (nth 103 *errors-by-case*))
   (format t "  5      0.07     0.10      0.02     0.13     ~5,2f    ~5,2f~%"
     (nth 79 *errors-by-case*) (nth 104 *errors-by-case*))
   (format t "  6      0.08     0.08      0.02     0.11     ~5,2f    ~5,2f~%"
     (nth 80 *errors-by-case*) (nth 105 *errors-by-case*))
   (format t "  7      0.05     0.08      0.01     0.09     ~5,2f    ~5,2f~%"
     (nth 81 *errors-by-case*) (nth 106 *errors-by-case*))
   (format t "  8      0.01     0.06      0.02     0.08     ~5,2f    ~5,2f~%"
     (nth 82 *errors-by-case*) (nth 107 *errors-by-case*))
   (format t "  9      0.01     0.04      0.01     0.07     ~5,2f    ~5,2f~%"
     (nth 83 *errors-by-case*) (nth 108 *errors-by-case*))
   (format t " 10      0.01     0.03      0.01     0.05     ~5,2f    ~5,2f~%"
     (nth 84 *errors-by-case*) (nth 109 *errors-by-case*))
   (format t " 11      0.02     0.03      0.01     0.04     ~5,2f    ~5,2f~%"
     (nth 85 *errors-by-case*) (nth 110 *errors-by-case*))
   (format t " 12      0.00     0.02      0.01     0.04     ~5,2f    ~5,2f~%"
     (nth 86 *errors-by-case*) (nth 111 *errors-by-case*))
   (format t " 13      0.00     0.02      0.01     0.03     ~5,2f    ~5,2f~%"
     (nth 87 *errors-by-case*) (nth 112 *errors-by-case*))
   (format t " 14      0.00     0.03      0.01     0.02     ~5,2f    ~5,2f~%"
     (nth 88 *errors-by-case*) (nth 113 *errors-by-case*))
   (format t " 15      0.00     0.02      0.01     0.02     ~5,2f    ~5,2f~%"
     (nth 89 *errors-by-case*) (nth 114 *errors-by-case*))
   (format t " 16      0.00     0.03      0.01     0.02     ~5,2f    ~5,2f~%"
     (nth 90 *errors-by-case*) (nth 115 *errors-by-case*))
   (format t "~%Observed and predicted learning data for the Type IV problem,~%")
   (format t "in Blocks 1-16 of Learning.  Human data is from Nosofsky,~%")
   (format t "Bluck, Palmeri, McKinley, and Glauthier as estimated from~%")
   (format t "Figure 6 of Nosofsky, Palmeri, and McKinley, 1994. RULEX~%")
   (format t "data is from the same Figure.~%~%")
   (format t "------------------------------------------------------------------~%")
   (format t "              People             RULEX             EPAM VI~%")
   (format t "       ------------------ ------------------ ------------------~%")          
   (format t "Block  Central Peripheral Central Peripheral Central Peripheral~%")
   (format t "---------------------------------------------------------------~%")
   (format t "  1      0.35     0.45      0.32     0.48     ~5,2f    ~5,2f~%"
     (nth 150 *errors-by-case*) (nth 175 *errors-by-case*))
   (format t "  2      0.18     0.34      0.05     0.26     ~5,2f    ~5,2f~%"
     (nth 151 *errors-by-case*) (nth 176 *errors-by-case*))
   (format t "  3      0.15     0.25      0.03     0.17     ~5,2f    ~5,2f~%"
     (nth 152 *errors-by-case*) (nth 177 *errors-by-case*))
   (format t "  4      0.06     0.21      0.02     0.13     ~5,2f    ~5,2f~%"
     (nth 153 *errors-by-case*) (nth 178 *errors-by-case*))
   (format t "  5      0.11     0.16      0.01     0.11     ~5,2f    ~5,2f~%"
     (nth 154 *errors-by-case*) (nth 179 *errors-by-case*))
   (format t "  6      0.09     0.12      0.01     0.08     ~5,2f    ~5,2f~%"
     (nth 155 *errors-by-case*) (nth 180 *errors-by-case*))
   (format t "  7      0.06     0.10      0.02     0.06     ~5,2f    ~5,2f~%"
     (nth 156 *errors-by-case*) (nth 181 *errors-by-case*))
   (format t "  8      0.04     0.07      0.01     0.05     ~5,2f    ~5,2f~%"
     (nth 157 *errors-by-case*) (nth 182 *errors-by-case*))
   (format t "  9      0.02     0.03      0.01     0.04     ~5,2f    ~5,2f~%"
     (nth 158 *errors-by-case*) (nth 183 *errors-by-case*))
   (format t " 10      0.02     0.04      0.02     0.04     ~5,2f    ~5,2f~%"
     (nth 159 *errors-by-case*) (nth 184 *errors-by-case*))
   (format t " 11      0.01     0.03      0.01     0.03     ~5,2f    ~5,2f~%"
     (nth 160 *errors-by-case*) (nth 185 *errors-by-case*))
   (format t " 12      0.03     0.03      0.01     0.02     ~5,2f    ~5,2f~%"
     (nth 161 *errors-by-case*) (nth 186 *errors-by-case*))
   (format t " 13      0.00     0.00      0.01     0.01     ~5,2f    ~5,2f~%"
     (nth 162 *errors-by-case*) (nth 187 *errors-by-case*))
   (format t " 14      0.00     0.00      0.01     0.01     ~5,2f    ~5,2f~%"
     (nth 163 *errors-by-case*) (nth 188 *errors-by-case*))
   (format t " 15      0.00     0.00      0.01     0.01     ~5,2f    ~5,2f~%"
     (nth 164 *errors-by-case*) (nth 189 *errors-by-case*))
   (format t " 16      0.00     0.00      0.01     0.01     ~5,2f    ~5,2f~%"
     (nth 165 *errors-by-case*) (nth 190 *errors-by-case*))
   (format t "~%Observed and predicted learning data for the Type V problem,~%")
   (format t "in Blocks 1-16 of Learning.  Human data is from Nosofsky,~%")
   (format t "Bluck, Palmeri, McKinley, and Glauthier as estimated from~%")
   (format t "Figure 8 of Nosofsky, Palmeri, and McKinley, 1994. RULEX~%")
   (format t "data is from the same Figure.~%~%")
   (format t "---------------------------------------------------------------------~%")
   (format t "              People               RULEX               EPAM VI~%")
   (format t "       -------------------- -------------------- --------------------~%")          
   (format t "Block  Central Periph Excep Central Periph Excep Central Periph Excep~%")
   (format t "---------------------------------------------------------------------~%")
   (format t "  1      0.36   0.47   0.62   0.44   0.51   0.61  ~5,2f  ~5,2f  ~5,2f~%"
     (nth 225 *errors-by-case*) (nth 250 *errors-by-case*) (nth 275 *errors-by-case*))
   (format t "  2      0.23   0.32   0.50   0.18   0.27   0.46  ~5,2f  ~5,2f  ~5,2f~%"
     (nth 226 *errors-by-case*) (nth 251 *errors-by-case*) (nth 276 *errors-by-case*))
   (format t "  3      0.15   0.24   0.31   0.11   0.16   0.31  ~5,2f  ~5,2f  ~5,2f~%"
     (nth 227 *errors-by-case*) (nth 252 *errors-by-case*) (nth 277 *errors-by-case*))
   (format t "  4      0.08   0.14   0.21   0.07   0.13   0.27  ~5,2f  ~5,2f  ~5,2f~%"
     (nth 228 *errors-by-case*) (nth 253 *errors-by-case*) (nth 278 *errors-by-case*))
   (format t "  5      0.07   0.08   0.22   0.07   0.10   0.20  ~5,2f  ~5,2f  ~5,2f~%"
     (nth 229 *errors-by-case*) (nth 254 *errors-by-case*) (nth 279 *errors-by-case*))
   (format t "  6      0.06   0.10   0.10   0.05   0.09   0.15  ~5,2f  ~5,2f  ~5,2f~%"
     (nth 230 *errors-by-case*) (nth 255 *errors-by-case*) (nth 280 *errors-by-case*))
   (format t "  7      0.07   0.07   0.08   0.05   0.08   0.13  ~5,2f  ~5,2f  ~5,2f~%"
     (nth 231 *errors-by-case*) (nth 256 *errors-by-case*) (nth 281 *errors-by-case*))
   (format t "  8      0.07   0.07   0.14   0.05   0.07   0.13  ~5,2f  ~5,2f  ~5,2f~%"
     (nth 232 *errors-by-case*) (nth 257 *errors-by-case*) (nth 282 *errors-by-case*))
   (format t "  9      0.03   0.05   0.09   0.05   0.07   0.11  ~5,2f  ~5,2f  ~5,2f~%"
     (nth 233 *errors-by-case*) (nth 258 *errors-by-case*) (nth 283 *errors-by-case*))
   (format t " 10      0.03   0.05   0.08   0.04   0.07   0.11  ~5,2f  ~5,2f  ~5,2f~%"
     (nth 234 *errors-by-case*) (nth 259 *errors-by-case*) (nth 284 *errors-by-case*))
   (format t " 11      0.02   0.05   0.10   0.03   0.06   0.09  ~5,2f  ~5,2f  ~5,2f~%"
     (nth 235 *errors-by-case*) (nth 260 *errors-by-case*) (nth 285 *errors-by-case*))
   (format t " 12      0.05   0.03   0.05   0.03   0.06   0.09  ~5,2f  ~5,2f  ~5,2f~%"
     (nth 236 *errors-by-case*) (nth 261 *errors-by-case*) (nth 286 *errors-by-case*))
   (format t " 13      0.02   0.02   0.09   0.02   0.05   0.09  ~5,2f  ~5,2f  ~5,2f~%"
     (nth 237 *errors-by-case*) (nth 262 *errors-by-case*) (nth 287 *errors-by-case*))
   (format t " 14      0.02   0.03   0.05   0.02   0.05   0.08  ~5,2f  ~5,2f  ~5,2f~%"
     (nth 238 *errors-by-case*) (nth 263 *errors-by-case*) (nth 288 *errors-by-case*))
   (format t " 15      0.00   0.03   0.00   0.01   0.05   0.08  ~5,2f  ~5,2f  ~5,2f~%"
     (nth 239 *errors-by-case*) (nth 264 *errors-by-case*) (nth 289 *errors-by-case*))
   (format t " 16      0.03   0.00   0.03   0.01   0.05   0.07  ~5,2f  ~5,2f  ~5,2f~%"
     (nth 240 *errors-by-case*) (nth 265 *errors-by-case*) (nth 290 *errors-by-case*))
      (format t "~%Average Error Proportions for Type I problems,~%")
   (format t "in Blocks 1-16 of Learning.  Human data is from Nosofsky,~%")
   (format t "Gluck, Palmeri, McKinley, and Glauthier Table 1. Rulex data ~%")
   (format t "is estimated from the graph shown as Figure 5 of  ~%")
   (format t "Nosofsky, Palmeri, McKinley (1994).~%~%")
   (format t "--------------------------------~%")
   (format t "Block  People    RULEX     EPAM~%")
   (format t "--------------------------------~%")
   (format t "   1     0.21     0.19    ~5,2f~%"  (nth 0 *errors-by-case*)) 
   (format t "   2     0.03     0.01    ~5,2f~%"  (nth 1 *errors-by-case*))
   (format t "   3     0.00     0.01    ~5,2f~%"  (nth 2 *errors-by-case*))
   (format t "   4     0.00     0.01    ~5,2f~%"  (nth 3 *errors-by-case*))
   (format t "   5     0.00     0.00    ~5,2f~%"  (nth 4 *errors-by-case*))
   (format t "   6     0.00     0.00    ~5,2f~%"  (nth 5 *errors-by-case*))
   (format t "   7     0.00     0.00    ~5,2f~%"  (nth 6 *errors-by-case*))
   (format t "   8     0.00     0.00    ~5,2f~%"  (nth 7 *errors-by-case*))
   (format t "   9     0.00     0.00    ~5,2f~%"  (nth 8 *errors-by-case*))
   (format t "  10     0.00     0.00    ~5,2f~%"  (nth 9 *errors-by-case*))
   (format t "  11     0.00     0.00    ~5,2f~%"  (nth 10 *errors-by-case*))
   (format t "  12     0.00     0.00    ~5,2f~%"  (nth 11 *errors-by-case*))
   (format t "  13     0.00     0.00    ~5,2f~%"  (nth 12 *errors-by-case*))
   (format t "  14     0.00     0.00    ~5,2f~%"  (nth 13 *errors-by-case*))
   (format t "  15     0.00     0.00    ~5,2f~%"  (nth 14 *errors-by-case*))
   (format t "  16     0.00     0.00    ~5,2f~%"  (nth 15 *errors-by-case*))
   (format t "~%Average Error Proportions for Type II problems,~%")
   (format t "in Blocks 1-16 of Learning.  Human data is from Nosofsky,~%")
   (format t "Gluck, Palmeri, McKinley, and Glauthier Table 1. Rulex data ~%")
   (format t "is estimated from the graph shown as Figure 5 of  ~%")
   (format t "Nosofsky, Palmeri, McKinley (1994).~%~%")
   (format t "--------------------------------~%")
   (format t "Block  People    RULEX     EPAM~%")
   (format t "--------------------------------~%")
   (format t "   1     0.38     0.42    ~5,2f~%"  (nth 25 *errors-by-case*))
   (format t "   2     0.16     0.12    ~5,2f~%"  (nth 26 *errors-by-case*))
   (format t "   3     0.08     0.08    ~5,2f~%"  (nth 27 *errors-by-case*))
   (format t "   4     0.06     0.07    ~5,2f~%"  (nth 28 *errors-by-case*))
   (format t "   5     0.03     0.05    ~5,2f~%"  (nth 29 *errors-by-case*))
   (format t "   6     0.03     0.04    ~5,2f~%"  (nth 30 *errors-by-case*))
   (format t "   7     0.03     0.03    ~5,2f~%"  (nth 31 *errors-by-case*))
   (format t "   8     0.02     0.03    ~5,2f~%"  (nth 32 *errors-by-case*))
   (format t "   9     0.02     0.03    ~5,2f~%"  (nth 33 *errors-by-case*))
   (format t "  10     0.01     0.02    ~5,2f~%"  (nth 34 *errors-by-case*))
   (format t "  11     0.00     0.02    ~5,2f~%"  (nth 35 *errors-by-case*))
   (format t "  12     0.01     0.02    ~5,2f~%"  (nth 36 *errors-by-case*))
   (format t "  13     0.00     0.02    ~5,2f~%"  (nth 37 *errors-by-case*))
   (format t "  14     0.00     0.02    ~5,2f~%"  (nth 38 *errors-by-case*))
   (format t "  15     0.00     0.02    ~5,2f~%"  (nth 39 *errors-by-case*))
   (format t "  16     0.00     0.02    ~5,2f~%"  (nth 40 *errors-by-case*))
   (format t "~%Average Error Proportions for Type III problems,~%")
   (format t "in Blocks 1-16 of Learning.  Human data is from Nosofsky,~%")
   (format t "Gluck, Palmeri, McKinley, and Glauthier Table 1. Rulex data ~%")
   (format t "is estimated from the graph shown as Figure 5 of  ~%")
   (format t "Nosofsky, Palmeri, McKinley (1994).~%~%")
   (format t "--------------------------------~%")
   (format t "Block  People    RULEX     EPAM~%")
   (format t "--------------------------------~%")
   (format t "   1     0.46     0.44    ~5,2f~%"  (nth 50 *errors-by-case*))
   (format t "   2     0.29     0.27    ~5,2f~%"  (nth 51 *errors-by-case*))
   (format t "   3     0.22     0.16    ~5,2f~%"  (nth 52 *errors-by-case*))
   (format t "   4     0.15     0.12    ~5,2f~%"  (nth 53 *errors-by-case*))
   (format t "   5     0.08     0.08    ~5,2f~%"  (nth 54 *errors-by-case*))
   (format t "   6     0.08     0.07    ~5,2f~%"  (nth 55 *errors-by-case*))
   (format t "   7     0.06     0.06    ~5,2f~%"  (nth 56 *errors-by-case*))
   (format t "   8     0.03     0.05    ~5,2f~%"  (nth 57 *errors-by-case*))
   (format t "   9     0.02     0.04    ~5,2f~%"  (nth 58 *errors-by-case*))
   (format t "  10     0.02     0.03    ~5,2f~%"  (nth 59 *errors-by-case*))
   (format t "  11     0.02     0.03    ~5,2f~%"  (nth 60 *errors-by-case*))
   (format t "  12     0.01     0.03    ~5,2f~%"  (nth 61 *errors-by-case*))
   (format t "  13     0.01     0.02    ~5,2f~%"  (nth 62 *errors-by-case*))
   (format t "  14     0.01     0.02    ~5,2f~%"  (nth 63 *errors-by-case*))
   (format t "  15     0.01     0.02    ~5,2f~%"  (nth 64 *errors-by-case*))
   (format t "  16     0.01     0.02    ~5,2f~%"  (nth 65 *errors-by-case*))
   (format t "~%Average Error Proportions for Type IV problems,~%")
   (format t "in Blocks 1-16 of Learning.  Human data is from Nosofsky,~%")
   (format t "Gluck, Palmeri, McKinley, and Glauthier Table 1. Rulex data ~%")
   (format t "is estimated from the graph shown as Figure 5 of  ~%")
   (format t "Nosofsky, Palmeri, McKinley (1994).~%~%")
   (format t "--------------------------------~%")
   (format t "Block  People    RULEX     EPAM~%")
   (format t "--------------------------------~%")
   (format t "   1     0.42     0.44    ~5,2f~%"  (nth 125 *errors-by-case*))
   (format t "   2     0.30     0.23    ~5,2f~%"  (nth 126 *errors-by-case*))
   (format t "   3     0.22     0.14    ~5,2f~%"  (nth 127 *errors-by-case*))
   (format t "   4     0.17     0.12    ~5,2f~%"  (nth 128 *errors-by-case*))
   (format t "   5     0.15     0.08    ~5,2f~%"  (nth 129 *errors-by-case*))
   (format t "   6     0.11     0.07    ~5,2f~%"  (nth 130 *errors-by-case*))
   (format t "   7     0.09     0.06    ~5,2f~%"  (nth 131 *errors-by-case*))
   (format t "   8     0.06     0.05    ~5,2f~%"  (nth 132 *errors-by-case*))
   (format t "   9     0.03     0.04    ~5,2f~%"  (nth 133 *errors-by-case*))
   (format t "  10     0.03     0.03    ~5,2f~%"  (nth 134 *errors-by-case*))
   (format t "  11     0.02     0.03    ~5,2f~%"  (nth 135 *errors-by-case*))
   (format t "  12     0.03     0.03    ~5,2f~%"  (nth 136 *errors-by-case*))
   (format t "  13     0.01     0.02    ~5,2f~%"  (nth 137 *errors-by-case*))
   (format t "  14     0.00     0.02    ~5,2f~%"  (nth 138 *errors-by-case*))
   (format t "  15     0.00     0.02    ~5,2f~%"  (nth 139 *errors-by-case*))
   (format t "  16     0.00     0.02    ~5,2f~%"  (nth 140 *errors-by-case*))
   (format t "~%Average Error Proportions for Type V problems,~%")
   (format t "in Blocks 1-16 of Learning.  Human data is from Nosofsky,~%")
   (format t "Gluck, Palmeri, McKinley, and Glauthier Table 1. Rulex data ~%")
   (format t "is estimated from the graph shown as Figure 5 of  ~%")
   (format t "Nosofsky, Palmeri, McKinley (1994).~%~%")
   (format t "--------------------------------~%")
   (format t "Block  People    RULEX     EPAM~%")
   (format t "--------------------------------~%")
   (format t "   1     0.47     0.50    ~5,2f~%"  (nth 200 *errors-by-case*))
   (format t "   2     0.33     0.27    ~5,2f~%"  (nth 201 *errors-by-case*))
   (format t "   3     0.23     0.16    ~5,2f~%"  (nth 202 *errors-by-case*))
   (format t "   4     0.14     0.13    ~5,2f~%"  (nth 203 *errors-by-case*))
   (format t "   5     0.11     0.11    ~5,2f~%"  (nth 204 *errors-by-case*))
   (format t "   6     0.08     0.08    ~5,2f~%"  (nth 205 *errors-by-case*))
   (format t "   7     0.07     0.07    ~5,2f~%"  (nth 206 *errors-by-case*))
   (format t "   8     0.08     0.06    ~5,2f~%"  (nth 207 *errors-by-case*))
   (format t "   9     0.05     0.04    ~5,2f~%"  (nth 208 *errors-by-case*))
   (format t "  10     0.05     0.03    ~5,2f~%"  (nth 209 *errors-by-case*))
   (format t "  11     0.05     0.03    ~5,2f~%"  (nth 210 *errors-by-case*))
   (format t "  12     0.04     0.03    ~5,2f~%"  (nth 211 *errors-by-case*))
   (format t "  13     0.03     0.03    ~5,2f~%"  (nth 212 *errors-by-case*))
   (format t "  14     0.03     0.03    ~5,2f~%"  (nth 213 *errors-by-case*))
   (format t "  15     0.02     0.03    ~5,2f~%"  (nth 214 *errors-by-case*))
   (format t "  16     0.01     0.03    ~5,2f~%"  (nth 215 *errors-by-case*))
   (format t "~%Average Error Proportions for Type VI problems,~%")
   (format t "in Blocks 1-16 of Learning.  Human data is from Nosofsky,~%")
   (format t "Gluck, Palmeri, McKinley, and Glauthier Table 1. Rulex data ~%")
   (format t "is estimated from the graph shown as Figure 5 of  ~%")
   (format t "Nosofsky, Palmeri, McKinley (1994).~%~%")
   (format t "--------------------------------~%")
   (format t "Block  People    RULEX     EPAM~%")
   (format t "--------------------------------~%")
   (format t "   1     0.50     0.59    ~5,2f~%"  (nth 300 *errors-by-case*))
   (format t "   2     0.34     0.45    ~5,2f~%"  (nth 301 *errors-by-case*))
   (format t "   3     0.28     0.33    ~5,2f~%"  (nth 302 *errors-by-case*))
   (format t "   4     0.25     0.27    ~5,2f~%"  (nth 303 *errors-by-case*))
   (format t "   5     0.22     0.23    ~5,2f~%"  (nth 304 *errors-by-case*))
   (format t "   6     0.19     0.20    ~5,2f~%"  (nth 305 *errors-by-case*))
   (format t "   7     0.19     0.18    ~5,2f~%"  (nth 306 *errors-by-case*))
   (format t "   8     0.18     0.17    ~5,2f~%"  (nth 307 *errors-by-case*))
   (format t "   9     0.17     0.16    ~5,2f~%"  (nth 308 *errors-by-case*))
   (format t "  10     0.13     0.15    ~5,2f~%"  (nth 309 *errors-by-case*))
   (format t "  11     0.14     0.13    ~5,2f~%"  (nth 310 *errors-by-case*))
   (format t "  12     0.12     0.12    ~5,2f~%"  (nth 311 *errors-by-case*))
   (format t "  13     0.10     0.11    ~5,2f~%"  (nth 312 *errors-by-case*))
   (format t "  14     0.10     0.11    ~5,2f~%"  (nth 313 *errors-by-case*))
   (format t "  15     0.11     0.10    ~5,2f~%"  (nth 314 *errors-by-case*))
   (format t "  16     0.11     0.10    ~5,2f~%"  (nth 315 *errors-by-case*))
   (format t "~%Average Error Proportions for Each of Problem Types 1-VI,~%")
   (format t "in Blocks 1-25 of Learning.  Human data is from Nosofsky,~%")
   (format t "Bluck, Palmeri, McKinley, and Glauthier Table 1.~%~%")
   (format t "-----------------------------------------------------------------------~%")
   (format t "               People                           EPAM VI~%")
   (format t "      ----------------------------   ----------------------------------~%")          
   (format t "Block   I   II  III   IV    V   VI     I    II   III    IV     V    VI~%")
   (format t "-----------------------------------------------------------------------~%")
   (format t "  1  0.21 0.38 0.46 0.42 0.47 0.50  ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~%"
     (nth 0 *errors-by-case*) (nth 25 *errors-by-case*) (nth 50 *errors-by-case*) (nth 125 *errors-by-case*) (nth 200 *errors-by-case*) (nth 300 *errors-by-case*))
   (format t "  2  0.03 0.16 0.29 0.30 0.33 0.34  ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~%"
     (nth 1 *errors-by-case*) (nth 26 *errors-by-case*) (nth 51 *errors-by-case*) (nth 126 *errors-by-case*) (nth 201 *errors-by-case*) (nth 301 *errors-by-case*))
   (format t "  3  0.00 0.08 0.22 0.22 0.23 0.28  ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~%"
     (nth 2 *errors-by-case*) (nth 27 *errors-by-case*) (nth 52 *errors-by-case*) (nth 127 *errors-by-case*) (nth 202 *errors-by-case*) (nth 302 *errors-by-case*))
   (format t "  4  0.00 0.06 0.15 0.17 0.14 0.25  ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~%"
     (nth 3 *errors-by-case*) (nth 28 *errors-by-case*) (nth 53 *errors-by-case*) (nth 128 *errors-by-case*) (nth 203 *errors-by-case*) (nth 303 *errors-by-case*))
   (format t "  5  0.00 0.03 0.08 0.15 0.11 0.22  ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~%"
     (nth 4 *errors-by-case*) (nth 29 *errors-by-case*) (nth 54 *errors-by-case*) (nth 129 *errors-by-case*) (nth 204 *errors-by-case*) (nth 304 *errors-by-case*))
   (format t "  6  0.00 0.03 0.08 0.11 0.08 0.19  ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~%"
     (nth 5 *errors-by-case*) (nth 30 *errors-by-case*) (nth 55 *errors-by-case*) (nth 130 *errors-by-case*) (nth 205 *errors-by-case*) (nth 305 *errors-by-case*))
   (format t "  7  0.00 0.03 0.06 0.09 0.07 0.19  ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~%"
     (nth 6 *errors-by-case*) (nth 31 *errors-by-case*) (nth 56 *errors-by-case*) (nth 131 *errors-by-case*) (nth 206 *errors-by-case*) (nth 306 *errors-by-case*))
   (format t "  8  0.00 0.02 0.03 0.06 0.08 0.18  ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~%"
     (nth 7 *errors-by-case*) (nth 32 *errors-by-case*) (nth 57 *errors-by-case*) (nth 132 *errors-by-case*) (nth 207 *errors-by-case*) (nth 307 *errors-by-case*))
   (format t "  9  0.00 0.02 0.02 0.03 0.05 0.17  ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~%"
     (nth 8 *errors-by-case*) (nth 33 *errors-by-case*) (nth 58 *errors-by-case*) (nth 133 *errors-by-case*) (nth 208 *errors-by-case*) (nth 308 *errors-by-case*))
   (format t " 10  0.00 0.01 0.02 0.03 0.05 0.13  ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~%"
     (nth 9 *errors-by-case*) (nth 34 *errors-by-case*) (nth 59 *errors-by-case*) (nth 134 *errors-by-case*) (nth 209 *errors-by-case*) (nth 309 *errors-by-case*))
   (format t " 11  0.00 0.00 0.02 0.02 0.05 0.14  ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~%"
     (nth 10 *errors-by-case*) (nth 35 *errors-by-case*) (nth 60 *errors-by-case*) (nth 135 *errors-by-case*) (nth 210 *errors-by-case*) (nth 310 *errors-by-case*))
   (format t " 12  0.00 0.00 0.01 0.03 0.04 0.12  ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~%"
     (nth 11 *errors-by-case*) (nth 36 *errors-by-case*) (nth 61 *errors-by-case*) (nth 136 *errors-by-case*) (nth 211 *errors-by-case*) (nth 311 *errors-by-case*))
   (format t " 13  0.00 0.01 0.01 0.01 0.03 0.10  ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~%"
     (nth 12 *errors-by-case*) (nth 37 *errors-by-case*) (nth 62 *errors-by-case*) (nth 137 *errors-by-case*) (nth 212 *errors-by-case*) (nth 312 *errors-by-case*))
   (format t " 14  0.00 0.00 0.01 0.00 0.03 0.10  ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~%"
     (nth 13 *errors-by-case*) (nth 38 *errors-by-case*) (nth 63 *errors-by-case*) (nth 138 *errors-by-case*) (nth 213 *errors-by-case*) (nth 313 *errors-by-case*))
   (format t " 15  0.00 0.00 0.01 0.00 0.02 0.11  ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~%"
     (nth 14 *errors-by-case*) (nth 39 *errors-by-case*) (nth 64 *errors-by-case*) (nth 139 *errors-by-case*) (nth 214 *errors-by-case*) (nth 314 *errors-by-case*))
   (format t " 16  0.00 0.00 0.01 0.00 0.01 0.11  ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~%"
     (nth 15 *errors-by-case*) (nth 40 *errors-by-case*) (nth 65 *errors-by-case*) (nth 140 *errors-by-case*) (nth 215 *errors-by-case*) (nth 315 *errors-by-case*))
   (format t " 17  0.00 0.00 0.01 0.00 0.01 0.08  ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~%"
     (nth 16 *errors-by-case*) (nth 41 *errors-by-case*) (nth 66 *errors-by-case*) (nth 141 *errors-by-case*) (nth 216 *errors-by-case*) (nth 316 *errors-by-case*))
   (format t " 18  0.00 0.00 0.01 0.00 0.01 0.08  ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~%"
     (nth 17 *errors-by-case*) (nth 42 *errors-by-case*) (nth 67 *errors-by-case*) (nth 142 *errors-by-case*) (nth 217 *errors-by-case*) (nth 317 *errors-by-case*))
   (format t " 19  0.00 0.00 0.01 0.00 0.01 0.08  ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~%"
     (nth 18 *errors-by-case*) (nth 43 *errors-by-case*) (nth 68 *errors-by-case*) (nth 143 *errors-by-case*) (nth 218 *errors-by-case*) (nth 318 *errors-by-case*))
   (format t " 20  0.00 0.00 0.00 0.00 0.01 0.06  ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~%"
     (nth 19 *errors-by-case*) (nth 44 *errors-by-case*) (nth 69 *errors-by-case*) (nth 144 *errors-by-case*) (nth 219 *errors-by-case*) (nth 319 *errors-by-case*))
   (format t " 21  0.00 0.00 0.01 0.00 0.01 0.06  ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~%"
     (nth 20 *errors-by-case*) (nth 45 *errors-by-case*) (nth 70 *errors-by-case*) (nth 145 *errors-by-case*) (nth 220 *errors-by-case*) (nth 320 *errors-by-case*))
   (format t " 22  0.00 0.00 0.00 0.00 0.01 0.04  ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~%"
     (nth 21 *errors-by-case*) (nth 46 *errors-by-case*) (nth 71 *errors-by-case*) (nth 146 *errors-by-case*) (nth 221 *errors-by-case*) (nth 321 *errors-by-case*))
   (format t " 23  0.00 0.00 0.00 0.00 0.01 0.04  ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~%"
     (nth 22 *errors-by-case*) (nth 47 *errors-by-case*) (nth 72 *errors-by-case*) (nth 147 *errors-by-case*) (nth 222 *errors-by-case*) (nth 322 *errors-by-case*))
   (format t " 24  0.00 0.00 0.01 0.00 0.01 0.03  ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~%"
     (nth 23 *errors-by-case*) (nth 48 *errors-by-case*) (nth 73 *errors-by-case*) (nth 148 *errors-by-case*) (nth 223 *errors-by-case*) (nth 323 *errors-by-case*))
   (format t " 25  0.00 0.00 0.00 0.00 0.01 0.04  ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~%"
     (nth 24 *errors-by-case*) (nth 49 *errors-by-case*) (nth 74 *errors-by-case*) (nth 149 *errors-by-case*) (nth 224 *errors-by-case*) (nth 324 *errors-by-case*))
   (format t "Ave  0.01 0.03 0.06 0.07 0.08 0.14  ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~%"
     (/ (loop for n from 0 upto 24 sum (nth n *errors-by-case*)) 25)
     (/ (loop for n from 25 upto 49 sum (nth n *errors-by-case*)) 25)
     (/ (loop for n from 50 upto 74 sum (nth n *errors-by-case*)) 25)
     (/ (loop for n from 125 upto 149 sum (nth n *errors-by-case*)) 25)
     (/ (loop for n from 200 upto 224 sum (nth n *errors-by-case*)) 25)
     (/ (loop for n from 300 upto 324 sum (nth n *errors-by-case*)) 25))
    )
   
(defun report-results-of-shepart-hovland-jenkins-simulation2 ()
   (format t "~%Average Error Proportions for Each of Problem Types 1-VI,~%")
   (format t "in Blocks 1-25 of Learning.  Human data is from Nosofsky,~%")
   (format t "Bluck, Palmeri, McKinley, and Glauthier Table 1.~%~%")
   (format t "-----------------------------------------------------------------------~%")
   (format t "               People                           EPAM VI~%")
   (format t "      ----------------------------   ----------------------------------~%")          
   (format t "Block   I   II  III   IV    V   VI     I    II   III    IV     V    VI~%")
   (format t "-----------------------------------------------------------------------~%")
   (format t "  1  0.21 0.38 0.46 0.42 0.47 0.50  ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~%"
     (nth 0 *errors-by-case*) (nth 25 *errors-by-case*) (nth 50 *errors-by-case*) (nth 125 *errors-by-case*) (nth 200 *errors-by-case*) (nth 300 *errors-by-case*))
   (format t "  2  0.03 0.16 0.29 0.30 0.33 0.34  ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~%"
     (nth 1 *errors-by-case*) (nth 26 *errors-by-case*) (nth 51 *errors-by-case*) (nth 126 *errors-by-case*) (nth 201 *errors-by-case*) (nth 301 *errors-by-case*))
   (format t "  3  0.00 0.08 0.22 0.22 0.23 0.28  ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~%"
     (nth 2 *errors-by-case*) (nth 27 *errors-by-case*) (nth 52 *errors-by-case*) (nth 127 *errors-by-case*) (nth 202 *errors-by-case*) (nth 302 *errors-by-case*))
   (format t "  4  0.00 0.06 0.15 0.17 0.14 0.25  ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~%"
     (nth 3 *errors-by-case*) (nth 28 *errors-by-case*) (nth 53 *errors-by-case*) (nth 128 *errors-by-case*) (nth 203 *errors-by-case*) (nth 303 *errors-by-case*))
   (format t "  5  0.00 0.03 0.08 0.15 0.11 0.22  ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~%"
     (nth 4 *errors-by-case*) (nth 29 *errors-by-case*) (nth 54 *errors-by-case*) (nth 129 *errors-by-case*) (nth 204 *errors-by-case*) (nth 304 *errors-by-case*))
   (format t "  6  0.00 0.03 0.08 0.11 0.08 0.19  ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~%"
     (nth 5 *errors-by-case*) (nth 30 *errors-by-case*) (nth 55 *errors-by-case*) (nth 130 *errors-by-case*) (nth 205 *errors-by-case*) (nth 305 *errors-by-case*))
   (format t "  7  0.00 0.03 0.06 0.09 0.07 0.19  ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~%"
     (nth 6 *errors-by-case*) (nth 31 *errors-by-case*) (nth 56 *errors-by-case*) (nth 131 *errors-by-case*) (nth 206 *errors-by-case*) (nth 306 *errors-by-case*))
   (format t "  8  0.00 0.02 0.03 0.06 0.08 0.18  ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~%"
     (nth 7 *errors-by-case*) (nth 32 *errors-by-case*) (nth 57 *errors-by-case*) (nth 132 *errors-by-case*) (nth 207 *errors-by-case*) (nth 307 *errors-by-case*))
   (format t "  9  0.00 0.02 0.02 0.03 0.05 0.17  ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~%"
     (nth 8 *errors-by-case*) (nth 33 *errors-by-case*) (nth 58 *errors-by-case*) (nth 133 *errors-by-case*) (nth 208 *errors-by-case*) (nth 308 *errors-by-case*))
   (format t " 10  0.00 0.01 0.02 0.03 0.05 0.13  ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~%"
     (nth 9 *errors-by-case*) (nth 34 *errors-by-case*) (nth 59 *errors-by-case*) (nth 134 *errors-by-case*) (nth 209 *errors-by-case*) (nth 309 *errors-by-case*))
   (format t " 11  0.00 0.00 0.02 0.02 0.05 0.14  ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~%"
     (nth 10 *errors-by-case*) (nth 35 *errors-by-case*) (nth 60 *errors-by-case*) (nth 135 *errors-by-case*) (nth 210 *errors-by-case*) (nth 310 *errors-by-case*))
   (format t " 12  0.00 0.00 0.01 0.03 0.04 0.12  ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~%"
     (nth 11 *errors-by-case*) (nth 36 *errors-by-case*) (nth 61 *errors-by-case*) (nth 136 *errors-by-case*) (nth 211 *errors-by-case*) (nth 311 *errors-by-case*))
   (format t " 13  0.00 0.01 0.01 0.01 0.03 0.10  ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~%"
     (nth 12 *errors-by-case*) (nth 37 *errors-by-case*) (nth 62 *errors-by-case*) (nth 137 *errors-by-case*) (nth 212 *errors-by-case*) (nth 312 *errors-by-case*))
   (format t " 14  0.00 0.00 0.01 0.00 0.03 0.10  ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~%"
     (nth 13 *errors-by-case*) (nth 38 *errors-by-case*) (nth 63 *errors-by-case*) (nth 138 *errors-by-case*) (nth 213 *errors-by-case*) (nth 313 *errors-by-case*))
   (format t " 15  0.00 0.00 0.01 0.00 0.02 0.11  ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~%"
     (nth 14 *errors-by-case*) (nth 39 *errors-by-case*) (nth 64 *errors-by-case*) (nth 139 *errors-by-case*) (nth 214 *errors-by-case*) (nth 314 *errors-by-case*))
   (format t " 16  0.00 0.00 0.01 0.00 0.01 0.11  ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~%"
     (nth 15 *errors-by-case*) (nth 40 *errors-by-case*) (nth 65 *errors-by-case*) (nth 140 *errors-by-case*) (nth 215 *errors-by-case*) (nth 315 *errors-by-case*))
   (format t " 17  0.00 0.00 0.01 0.00 0.01 0.08  ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~%"
     (nth 16 *errors-by-case*) (nth 41 *errors-by-case*) (nth 66 *errors-by-case*) (nth 141 *errors-by-case*) (nth 216 *errors-by-case*) (nth 316 *errors-by-case*))
   (format t " 18  0.00 0.00 0.01 0.00 0.01 0.08  ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~%"
     (nth 17 *errors-by-case*) (nth 42 *errors-by-case*) (nth 67 *errors-by-case*) (nth 142 *errors-by-case*) (nth 217 *errors-by-case*) (nth 317 *errors-by-case*))
   (format t " 19  0.00 0.00 0.01 0.00 0.01 0.08  ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~%"
     (nth 18 *errors-by-case*) (nth 43 *errors-by-case*) (nth 68 *errors-by-case*) (nth 143 *errors-by-case*) (nth 218 *errors-by-case*) (nth 318 *errors-by-case*))
   (format t " 20  0.00 0.00 0.00 0.00 0.01 0.06  ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~%"
     (nth 19 *errors-by-case*) (nth 44 *errors-by-case*) (nth 69 *errors-by-case*) (nth 144 *errors-by-case*) (nth 219 *errors-by-case*) (nth 319 *errors-by-case*))
   (format t " 21  0.00 0.00 0.01 0.00 0.01 0.06  ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~%"
     (nth 20 *errors-by-case*) (nth 45 *errors-by-case*) (nth 70 *errors-by-case*) (nth 145 *errors-by-case*) (nth 220 *errors-by-case*) (nth 320 *errors-by-case*))
   (format t " 22  0.00 0.00 0.00 0.00 0.01 0.04  ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~%"
     (nth 21 *errors-by-case*) (nth 46 *errors-by-case*) (nth 71 *errors-by-case*) (nth 146 *errors-by-case*) (nth 221 *errors-by-case*) (nth 321 *errors-by-case*))
   (format t " 23  0.00 0.00 0.00 0.00 0.01 0.04  ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~%"
     (nth 22 *errors-by-case*) (nth 47 *errors-by-case*) (nth 72 *errors-by-case*) (nth 147 *errors-by-case*) (nth 222 *errors-by-case*) (nth 322 *errors-by-case*))
   (format t " 24  0.00 0.00 0.01 0.00 0.01 0.03  ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~%"
     (nth 23 *errors-by-case*) (nth 48 *errors-by-case*) (nth 73 *errors-by-case*) (nth 148 *errors-by-case*) (nth 223 *errors-by-case*) (nth 323 *errors-by-case*))
   (format t " 25  0.00 0.00 0.00 0.00 0.01 0.04  ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~%"
     (nth 24 *errors-by-case*) (nth 49 *errors-by-case*) (nth 74 *errors-by-case*) (nth 149 *errors-by-case*) (nth 224 *errors-by-case*) (nth 324 *errors-by-case*))
   (format t "Ave  0.01 0.03 0.06 0.07 0.08 0.14  ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~5,3f ~%"
     (/ (loop for n from 0 upto 24 sum (nth n *errors-by-case*)) 25)
     (/ (loop for n from 25 upto 49 sum (nth n *errors-by-case*)) 25)
     (/ (loop for n from 50 upto 74 sum (nth n *errors-by-case*)) 25)
     (/ (loop for n from 125 upto 149 sum (nth n *errors-by-case*)) 25)
     (/ (loop for n from 200 upto 224 sum (nth n *errors-by-case*)) 25)
     (/ (loop for n from 300 upto 324 sum (nth n *errors-by-case*)) 25))
   (format t "~%Observed and predicted learning data for the Type III problem,~%")
   (format t "in Blocks 1-16 of Learning.  Human data is from Nosofsky,~%")
   (format t "Bluck, Palmeri, McKinley, and Glauthier as estimated from~%")
   (format t "Figure 7 of Nosofsky, Palmeri, and McKinley, 1994. RULEX~%")
   (format t "data is from the same Figure.~%~%")
   (format t "------------------------------------------------------------------~%")
   (format t "              People             RULEX             EPAM VI~%")
   (format t "       ------------------ ------------------ ------------------~%")          
   (format t "Block  Central Peripheral Central Peripheral Central Peripheral~%")
   (format t "---------------------------------------------------------------~%")
   (format t "  1      0.41     0.52      0.36     0.58     ~5,2f     ~5,2f~%"
     (nth 75 *errors-by-case*) (nth 100 *errors-by-case*))
   (format t "  2      0.23     0.35      0.11     0.35     ~5,2f    ~5,2f~%"
     (nth 76 *errors-by-case*) (nth 101 *errors-by-case*))
   (format t "  3      0.17     0.28      0.06     0.23     ~5,2f    ~5,2f~%"
     (nth 77 *errors-by-case*) (nth 102 *errors-by-case*))
   (format t "  4      0.13     0.16      0.04     0.17     ~5,2f    ~5,2f~%"
     (nth 78 *errors-by-case*) (nth 103 *errors-by-case*))
   (format t "  5      0.07     0.10      0.02     0.13     ~5,2f    ~5,2f~%"
     (nth 79 *errors-by-case*) (nth 104 *errors-by-case*))
   (format t "  6      0.08     0.08      0.02     0.11     ~5,2f    ~5,2f~%"
     (nth 80 *errors-by-case*) (nth 105 *errors-by-case*))
   (format t "  7      0.05     0.08      0.01     0.09     ~5,2f    ~5,2f~%"
     (nth 81 *errors-by-case*) (nth 106 *errors-by-case*))
   (format t "  8      0.01     0.06      0.02     0.08     ~5,2f    ~5,2f~%"
     (nth 82 *errors-by-case*) (nth 107 *errors-by-case*))
   (format t "  9      0.01     0.04      0.01     0.07     ~5,2f    ~5,2f~%"
     (nth 83 *errors-by-case*) (nth 108 *errors-by-case*))
   (format t " 10      0.01     0.03      0.01     0.05     ~5,2f    ~5,2f~%"
     (nth 84 *errors-by-case*) (nth 109 *errors-by-case*))
   (format t " 11      0.02     0.03      0.01     0.04     ~5,2f    ~5,2f~%"
     (nth 85 *errors-by-case*) (nth 110 *errors-by-case*))
   (format t " 12      0.00     0.02      0.01     0.04     ~5,2f    ~5,2f~%"
     (nth 86 *errors-by-case*) (nth 111 *errors-by-case*))
   (format t " 13      0.00     0.02      0.01     0.03     ~5,2f    ~5,2f~%"
     (nth 87 *errors-by-case*) (nth 112 *errors-by-case*))
   (format t " 14      0.00     0.03      0.01     0.02     ~5,2f    ~5,2f~%"
     (nth 88 *errors-by-case*) (nth 113 *errors-by-case*))
   (format t " 15      0.00     0.02      0.01     0.02     ~5,2f    ~5,2f~%"
     (nth 89 *errors-by-case*) (nth 114 *errors-by-case*))
   (format t " 16      0.00     0.03      0.01     0.02     ~5,2f    ~5,2f~%"
     (nth 90 *errors-by-case*) (nth 115 *errors-by-case*))
   (format t "~%Observed and predicted learning data for the Type IV problem,~%")
   (format t "in Blocks 1-16 of Learning.  Human data is from Nosofsky,~%")
   (format t "Bluck, Palmeri, McKinley, and Glauthier as estimated from~%")
   (format t "Figure 6 of Nosofsky, Palmeri, and McKinley, 1994. RULEX~%")
   (format t "data is from the same Figure.~%~%")
   (format t "------------------------------------------------------------------~%")
   (format t "              People             RULEX             EPAM VI~%")
   (format t "       ------------------ ------------------ ------------------~%")          
   (format t "Block  Central Peripheral Central Peripheral Central Peripheral~%")
   (format t "---------------------------------------------------------------~%")
   (format t "  1      0.35     0.45      0.32     0.48     ~5,2f    ~5,2f~%"
     (nth 150 *errors-by-case*) (nth 175 *errors-by-case*))
   (format t "  2      0.18     0.34      0.05     0.26     ~5,2f    ~5,2f~%"
     (nth 151 *errors-by-case*) (nth 176 *errors-by-case*))
   (format t "  3      0.15     0.25      0.03     0.17     ~5,2f    ~5,2f~%"
     (nth 152 *errors-by-case*) (nth 177 *errors-by-case*))
   (format t "  4      0.06     0.21      0.02     0.13     ~5,2f    ~5,2f~%"
     (nth 153 *errors-by-case*) (nth 178 *errors-by-case*))
   (format t "  5      0.11     0.16      0.01     0.11     ~5,2f    ~5,2f~%"
     (nth 154 *errors-by-case*) (nth 179 *errors-by-case*))
   (format t "  6      0.09     0.12      0.01     0.08     ~5,2f    ~5,2f~%"
     (nth 155 *errors-by-case*) (nth 180 *errors-by-case*))
   (format t "  7      0.06     0.10      0.02     0.06     ~5,2f    ~5,2f~%"
     (nth 156 *errors-by-case*) (nth 181 *errors-by-case*))
   (format t "  8      0.04     0.07      0.01     0.05     ~5,2f    ~5,2f~%"
     (nth 157 *errors-by-case*) (nth 182 *errors-by-case*))
   (format t "  9      0.02     0.03      0.01     0.04     ~5,2f    ~5,2f~%"
     (nth 158 *errors-by-case*) (nth 183 *errors-by-case*))
   (format t " 10      0.02     0.04      0.02     0.04     ~5,2f    ~5,2f~%"
     (nth 159 *errors-by-case*) (nth 184 *errors-by-case*))
   (format t " 11      0.01     0.03      0.01     0.03     ~5,2f    ~5,2f~%"
     (nth 160 *errors-by-case*) (nth 185 *errors-by-case*))
   (format t " 12      0.03     0.03      0.01     0.02     ~5,2f    ~5,2f~%"
     (nth 161 *errors-by-case*) (nth 186 *errors-by-case*))
   (format t " 13      0.00     0.00      0.01     0.01     ~5,2f    ~5,2f~%"
     (nth 162 *errors-by-case*) (nth 187 *errors-by-case*))
   (format t " 14      0.00     0.00      0.01     0.01     ~5,2f    ~5,2f~%"
     (nth 163 *errors-by-case*) (nth 188 *errors-by-case*))
   (format t " 15      0.00     0.00      0.01     0.01     ~5,2f    ~5,2f~%"
     (nth 164 *errors-by-case*) (nth 189 *errors-by-case*))
   (format t " 16      0.00     0.00      0.01     0.01     ~5,2f    ~5,2f~%"
     (nth 165 *errors-by-case*) (nth 190 *errors-by-case*))
   (format t "~%Observed and predicted learning data for the Type V problem,~%")
   (format t "in Blocks 1-16 of Learning.  Human data is from Nosofsky,~%")
   (format t "Bluck, Palmeri, McKinley, and Glauthier as estimated from~%")
   (format t "Figure 8 of Nosofsky, Palmeri, and McKinley, 1994. RULEX~%")
   (format t "data is from the same Figure.~%~%")
   (format t "---------------------------------------------------------------------~%")
   (format t "              People               RULEX               EPAM VI~%")
   (format t "       -------------------- -------------------- --------------------~%")          
   (format t "Block  Central Periph Excep Central Periph Excep Central Periph Excep~%")
   (format t "---------------------------------------------------------------------~%")
   (format t "  1      0.36   0.47   0.62   0.44   0.51   0.61  ~5,2f  ~5,2f  ~5,2f~%"
     (nth 225 *errors-by-case*) (nth 250 *errors-by-case*) (nth 275 *errors-by-case*))
   (format t "  2      0.23   0.32   0.50   0.18   0.27   0.46  ~5,2f  ~5,2f  ~5,2f~%"
     (nth 226 *errors-by-case*) (nth 251 *errors-by-case*) (nth 276 *errors-by-case*))
   (format t "  3      0.15   0.24   0.31   0.11   0.16   0.31  ~5,2f  ~5,2f  ~5,2f~%"
     (nth 227 *errors-by-case*) (nth 252 *errors-by-case*) (nth 277 *errors-by-case*))
   (format t "  4      0.08   0.14   0.21   0.07   0.13   0.27  ~5,2f  ~5,2f  ~5,2f~%"
     (nth 228 *errors-by-case*) (nth 253 *errors-by-case*) (nth 278 *errors-by-case*))
   (format t "  5      0.07   0.08   0.22   0.07   0.10   0.20  ~5,2f  ~5,2f  ~5,2f~%"
     (nth 229 *errors-by-case*) (nth 254 *errors-by-case*) (nth 279 *errors-by-case*))
   (format t "  6      0.06   0.10   0.10   0.05   0.09   0.15  ~5,2f  ~5,2f  ~5,2f~%"
     (nth 230 *errors-by-case*) (nth 255 *errors-by-case*) (nth 280 *errors-by-case*))
   (format t "  7      0.07   0.07   0.08   0.05   0.08   0.13  ~5,2f  ~5,2f  ~5,2f~%"
     (nth 231 *errors-by-case*) (nth 256 *errors-by-case*) (nth 281 *errors-by-case*))
   (format t "  8      0.07   0.07   0.14   0.05   0.07   0.13  ~5,2f  ~5,2f  ~5,2f~%"
     (nth 232 *errors-by-case*) (nth 257 *errors-by-case*) (nth 282 *errors-by-case*))
   (format t "  9      0.03   0.05   0.09   0.05   0.07   0.11  ~5,2f  ~5,2f  ~5,2f~%"
     (nth 233 *errors-by-case*) (nth 258 *errors-by-case*) (nth 283 *errors-by-case*))
   (format t " 10      0.03   0.05   0.08   0.04   0.07   0.11  ~5,2f  ~5,2f  ~5,2f~%"
     (nth 234 *errors-by-case*) (nth 259 *errors-by-case*) (nth 284 *errors-by-case*))
   (format t " 11      0.02   0.05   0.10   0.03   0.06   0.09  ~5,2f  ~5,2f  ~5,2f~%"
     (nth 235 *errors-by-case*) (nth 260 *errors-by-case*) (nth 285 *errors-by-case*))
   (format t " 12      0.05   0.03   0.05   0.03   0.06   0.09  ~5,2f  ~5,2f  ~5,2f~%"
     (nth 236 *errors-by-case*) (nth 261 *errors-by-case*) (nth 286 *errors-by-case*))
   (format t " 13      0.02   0.02   0.09   0.02   0.05   0.09  ~5,2f  ~5,2f  ~5,2f~%"
     (nth 237 *errors-by-case*) (nth 262 *errors-by-case*) (nth 287 *errors-by-case*))
   (format t " 14      0.02   0.03   0.05   0.02   0.05   0.08  ~5,2f  ~5,2f  ~5,2f~%"
     (nth 238 *errors-by-case*) (nth 263 *errors-by-case*) (nth 288 *errors-by-case*))
   (format t " 15      0.00   0.03   0.00   0.01   0.05   0.08  ~5,2f  ~5,2f  ~5,2f~%"
     (nth 239 *errors-by-case*) (nth 264 *errors-by-case*) (nth 289 *errors-by-case*))
   (format t " 16      0.03   0.00   0.03   0.01   0.05   0.07  ~5,2f  ~5,2f  ~5,2f~%"
     (nth 240 *errors-by-case*) (nth 265 *errors-by-case*) (nth 290 *errors-by-case*))
    )





(defun make-shepard-hovland-jenkins-cases ()
   (setf *possible-values* '((color (black white))
                             (shape (triangle square))
                             (size (large small))))
   (setf *binary-values* '((white black) (black white) 
                           (triangle square) (square triangle)
                           (large small) (small large)))
   (setf *list-of-possible-tests* '(color shape size))
   (setf *cases* 
         (loop with n = 0
           for first-value in '(black white)
           append (loop for second-value in '(triangle square)
                    append (loop for third-value in '(large small)
                             do (incf n)
                             collect 
                             (list (list
                                    (list 'is-a 'shj)
                                    (list 'case-number n)
                                    (list 'color first-value)
                                    (list 'shape second-value)
                                    (list 'size third-value))))))))

(defun test-for-shephard-concept (item concept-type feature1 feature2 feature3)
  (let* ((f1 (get-value item feature1))
         (f2 (get-value item feature2))
         (f3 (get-value item feature3))
         (v1 (caadr (assoc feature1 *possible-values*)))
         (v2 (caadr (assoc feature2 *possible-values*)))
         (v3 (caadr (assoc feature3 *possible-values*))))
    (cond
     ((eql concept-type 'type1)
      (eql f1 v1))
     ((eql concept-type 'type2)
      (or (and (eql f1 v1) (eql f2 v2))
          (and (not (eql f1 v1)) (not (eql f2 v2)))))
     ((eql concept-type 'type3)
      (or (and (eql f1 v1) (eql f3 v3))
          (and (not (eql f3 v3)) (eql f2 v2))))
     ((eql concept-type 'type4)
      (or (and (eql f1 v1) (eql f2 v2))
          (and (eql f1 v1) (eql f3 v3))
          (and (eql f2 v2) (eql f3 v3))))
     ((eql concept-type 'type5)
      (or (and (eql v1 f1) (eql f3 v3))
          (and (eql v1 f1) (eql f2 v2))
          (and (not (eql v1 f1)) (not (eql v2 f2)) (not (eql v3 f3)))))
     ((eql concept-type 'type6)
      (or (and (eql v1 f1) (eql v2 f2) (eql v3 f3))
          (and (not (eql v1 f1)) (eql v2 f2) (not (eql v3 f3)))
          (and (not (eql v1 f1)) (not (eql v2 f2)) (eql v3 f3))
          (and (eql v1 f1) (not (eql v2 f2)) (not (eql v3 f3)))))
     ('else (print "Error! concept-type = ~a is not yet supported!" 
                   concept-type))
     )))


(defun run-all-concept-formation-simulations ()
   (shepard-hovland-jenkins-simulation)
   (martin-caramazza-simulation)
   (nosofsky-palmeri-and-mckinley-experiment1-simulation)
   (palmeri-and-nosofsky-experiment1-simulation)
   (medin-and-smith-simulation)
   )

