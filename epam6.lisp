; epam6.lsp.  This is the main EPAM routine part of the EPAM6 program
; This is the February 24, 2002, version.  Copyright by Howard B. Richman


;;                   A. GLOBAL VARIABLE DECLARATIONS

; EPAM's Parameters

(defvar *sal-a* 50) ; This is the percentage of time that the system studies the 
                    ; stimulus then the association after an error is made when there
                    ; are multiple episodes at this node.
(defvar *sal-b* 100) ; This is the percentage of time when the system
                    ; studies the association after an error when EPAM learning is 
                    ; available.
(defvar *sal-c* 30) ; percentage of the time the subject will study a stimulus and 
                    ; the association when it is not making an 
                    ; error.
(defvar *fixed-time-required-for-recognition-and-response* 1100)

(defvar *variable-time-required-to-recognize* 250)

(defvar *create-episode-having-two-slots* nil) ; This variable is used in the whale-cigar
; simulation.  If it's value is true, then associate, when creating a new episode,
; will create one that has slots for the stimulus and the response.


;; EPAM VARIABLES
  (defvar *net*) ;; Holds the name of the EPAM net (which is its root node).
  (defvar *top-pseudochunk-node*) ;; Holds the name of the node which is the top
                                  ;; node where information about pseudochunks is 
                                  ;; kept.  It is used by the routine that gets 
                                  ;; a pseudochunks for an atom in order to find out
                                  ;; if the pseudochunk already exists in the net.
                                  ;; Pseudochunks are special epam nodes 
                                  ;; formed to hold info about LISP atoms.
  (defvar *non-testable-attributes* '(activation group time confidence))
               ;; These are attributes that will not be tested by the net
               ;;  they are reserved for use in holding information that will
               ;;  not become part of the tests in the net.
  (defvar *nodes-in-net* 0) ;; counts the number of nodes in the net.
  (defvar *number-of-episodes*) ;; counts the number of episodes created
  (defvar *node-list*) ;; This variable is used by print-net 
  (defvar *episode-list*) ;; This variable is used by print-net
  (defvar *activation* 0) ;; Holds a number corresponding to point
;;                         in time and is used in simulations of search for
;;                         recently studied chunks.
  (defvar *anchor-chunk*) ;; This is used to hold the chunk that is used by 
                          ;; test-value-finder in order to interpret the test  'after 
                          ;; or 'before.

(defvar *beginning-control-symbols* '(beginning group-2 group-3))

  (defvar *control-symbols* '(signal-to-respond end-of-list beginning)) 
  
  (defvar *forgetting-parameter* 21) ; This is the amount forgotten when an episode leaves
                                     ; the rsf.  It was estimated to be between 20 to 25
                                     ; by the contextual cues forgotten in the retrieval
                                     ; structure simulations

;; This variable is used by strategies to stop search routines.
  (defvar *search-interrupt*)
(defvar *guessing-routine* nil);  This routine is specified where 
                               ;  the subject can guess the response from among
                               ;  a list of familiar alternatives.  For 
                               ;  example where all of the responses are 
                               ;  numbers between 1 and 9
(defvar *search-list* nil) ; this list specifies the order that a particular subject uses 
;    to search through the values at a node.
;    Every new subject would have a different *search-list* as indicated
;    by the fact that *search-list* would become nil.  Search list would be grown whenever a search
;    takes place.  When searching through a leaf node, get all of the values at the leaf node.
;    If there already is a search list, check to see if all of the values are already on the 
;    search list.  If they are, then use that search list.  If not then re-randomize the values and
;    maket them the values on the search list.  Thus, every subject will have his own search list
;    and the order will get changed randomly when new branches are added at a node.

;; These variables set time requirements:
   (defvar *time-to-distinguish-an-object* 100) ; average amount of time taken by sorting in net.
   (defvar *time-to-enter-an-object* 120)
   (defvar *time-to-enter-a-syllable* 120)
   (defvar *time-to-enter-control-symbol*)
   (defvar *time-to-write-a-letter* 400)
;; These variables set the parameters for an experiment.
  (defvar *stimulus-duration-rate* 2000) ;; Length of exposure of the stimulus
  (defvar *response-duration-rate*) ;; Length of exposure of the correct response
  (defvar *presentation-rate*) ;; Time between presentations of item or pair
  (defvar *signal-to-respond-delay*) ;; Time after last that signal presented
  (defvar *time-allowed-for-recall*) ;; Time after signal-to-respond
  (defvar *suppression-delay*) ;; This marks the delay between the presentation
                               ;; of last stimulus and presentation of the
                               ;; suppression stimuli.
  (defvar *stimulus-list*) ;; The list of stimuli that will be presented.
  (defvar *stimulus-lists*) ;; lists of lists of stimulus that will be presented
  (defvar *digits-list*) ;; The list of digits when the *stimulus-list* consists
                         ;; of the auditory names of digits.
  (defvar *sr-list*) ;; Used to hold the stimulus-response pairs in paired ass.
  (defvar *suppression-stimuli*) ;; Used to hold stimuli for articulatory supp.
  (defvar *experimenter*) ;; Experimenter routine
  (defvar *extra-pairs*) ;; Used to hold extra pairs in some sr-experiments
  (defvar *inter-trial-interval*) ;; Pause between trials
  (defvar *method-for-counting-syllables* 'character-method)
                                  ;; Routine for counting syllables.
  (defvar *whole-word-parameter*) ;; Used in context effects experiment to
                                  ;; determine when to study the whole word
  (defvar *probabilities-of-masking-features*) ;; Used in  context effects
                                  ;; experiment to determine amount of mask in each
                                  ;; position
  (defvar *number-of-sessions*) ;; Number of times the simulation is run
  (defvar *similar-set*)
  (defvar *dissimilar-set*)
  (defvar *suppression-chunk*)
  (defvar *suppression-list*)
  (defvar *suppression-object*)
  (defvar *short-set*)
  (defvar *long-set*)
  (defvar *mask*) ;; Used in a context-effects-in-letter-perception experiment
                  ;; to hold the mask that will be added to a visual stimulus.
(defvar *valist* nil) ;; These variables are used by experimenter in converting between modalities
(defvar *avlist* nil)
; These variables are used for holding results from a collection of experiments
(defvar *overall-results* nil); This variable is used to collect and display the overall results
(defvar *list-length-results* nil)
(defvar *serial-position-results* nil)
(defvar *total-trials-by-list* nil)
(defvar *backward-response-record* nil)
(defvar *proportion-correct-by-trial* nil)
(defvar *precriterion-proportion-correct-by-trial* nil)
(defvar *errors-by-list* nil)
(defvar *errors-between-one-and-ten* nil)
(defvar *result-of-recognition-test* nil)
(defvar *proportion-of-recalls* nil)
(defvar *perfectly-correct-from-1-to-10* nil)
(defvar *perfectly-correct-from-11-to-20* nil)
(defvar *correct-from-1-to-10* nil)
(defvar *correct-from-11-to-20* nil)
(defvar *total-from-1-to-10* nil)
(defvar *total-from-11-to-20* nil)
(defvar *r0* nil)
(defvar *r1* nil)
(defvar *r2* nil)
(defvar *r3* nil)
(defvar *average-latency* nil)
(defvar *average-latency-when-correct* nil)
(defvar *average-latency-when-incorrect* nil)
(defvar *average-latency-of-last-error* nil)
(defvar *stimulus-is-a* nil)
(defvar *response-is-a* nil)
(defvar *possible-words* nil)  
;; These variables vary during an experiment
;;   so that the experimenter routine and the strategy routine can work together
;;   and so that the number of errors can be calculated.
  (defvar *position*) ;; Position in a list of stimuli
  (defvar *new-stimulus-flag*) ;; True if a new stimulus is in a sensory store
  (defvar *trial*) ;; The trial number in a multitrial experiment 0 is first.
  (defvar *errors*) ;; Total errors in an experiment
  (defvar *starting-errors*) ;; Errors at the beginning of a trial
  (defvar *output*) ;; The output produced by a strategy
  (defvar *errors-array*) ;; Holds errors produced in their serial positions
                          ;; or errors by trial.
  (defvar *qp-errors-array*) ;; Holds double-dimensioned errors by list and trial
  (defvar *position-array*)  ;; Holds errors by serial position
  (defvar *clock*) ;; A running clock of miliseconds elapsed in experiment
  (defvar *epam-clock*) ;; A clock telling when already initiated learning
                        ;; processes will terminate.
  (defvar *sorting-clock*) ;; A clock used to hold the amount of time taken
                           ;; by an EPAM sort.
  (defvar *articulation-clock*) ;; A clock used to hold time spent articulating
                                ;; aloud 
  (defvar *writing-clock*) ;; A clock used to hold time spent writing
  (defvar *response-latency* 0) ;; Holds the latency of response for use by record-keeping routines
                              ;; It is calculated by subtracting the *trial-clock* from the *clock*
  (defvar *trial-clock* 0)

;These variables hold result info
  (defvar *number-of-episodes-elicited*) ; The number of episodes found by find-episodes
  (defvar *position-of-episode-elicited*) ; The position of the episode chosen 
                                          ; by find-correct-episode-attached? 
  (defvar *number-of-slots-added-by-length*) ; This variable totals the number of empty slots 
                                             ; added by the routine put-length-on
  (defvar *chunks* nil) ;; The accumulator
  (defvar *loop-decay-parameter* .049) ;; This parameter controls exponential
                                     ;; decay in the phonological store.
; The *long-term-forgetting-parameter* gives the probability that the second episode 
;  of a two-epiosde list will pop.  This is essentially the same Sal's parameter d that 
;  the top of a push down stack is unstable and tends to pop off.  
; In EPAM-6 Hintzman's idea has been generalized slightly and given a precise exponential decay
;  function as the result of the passage of time.
;    1. When there are two episodes associated that are on the push-down stack of a node
;       then run the parameter once, if it succeeds then pop the most recently added episode.
;    2. When there are three episodes, run the parameter twice, if it succeeds either time,
;       then pop the most recently added episode, if it succeeds both times, pop both 
;       added episodes.
;    3. In other words run the parameter one less time than the number of episodes, and pop as 
;       many as there are hits.
; The actual value is the probability that the item will pop in the second position on the 
; list of episodes.  Its value was roughly estimated from the Briggs (1954) data.  
; It is the probability that the episode will be forgotten each second.  
; Based upon the Briggs data I estimated the value to be somehwere between .00001 and .00005
; A value below .00001 doesn't appear to cause enough forgetting.  A value above .00005 tends
; to flatten out the R2 curve too soon before 72 hours.
(defvar *long-term-forgetting-parameter* .000005) ; This value gives additional episodes at a chunk
                                                 ;  a half-life of 38.5 hours.
                                  
(defvar *long-term-forgetting* t) ; This parameter turns on long-term forgetting 
                                  ; so that episodes can be forgotten by being popped off a 
                                  ; list of episodes at a chunk where they are attached.
 
;; These variables affect the size of the printout.  In some systems an error
;;   will occur if the print-out gets too large. The function 
;;   (turn-printing-off) turns most of the printing off.  The function
;;   (turn-printing-on) would turn most of the printing back on.
  (defvar *print-time-updates* nil) ;; Whether reasons for adding time are printed
  (defvar *print-imagery-stores* nil) ;; Whether imagery-stores will be printed
  (defvar *print-experimenter-updates* nil) ;; Whether experimenter remarks printed
  (defvar *print-epam-updates* nil) ;; Whether EPAM learning activities printed
  (defvar *print-strategy-updates* nil) ;; Whether strategy activities printed.
  (defvar *print-major-updates* nil) 
  (defvar *print-net-updates* nil) ;; Whether net is to be printed automatically after every 
                                   ;; learning episode
  (defvar *print-forgetting-updates* nil) ; whether system will print a message whenever an 
                                          ; episode is popped from the top of a list of episodes
                                          ; associated with a stimulus or response.
(defvar *print-rsf-updates* nil)
;; These variables are subject to strategic control
  (defvar *ignore-new-stimulus*) ;; Whether one-trial or all-at-once strategy
  (defvar *concurrent-routine*) ;; The name of a routine that is executed at
                                ;; the same time that the system does another
                                ;; task
  (defvar *modality-of-output*) ;; visual or auditory
  (defvar *paired-strategy*) ;; which rehearsal strategy is used 
;; These variables can be turned off in order to see what the output would be
;;   like if a source of information is not used.
  (defvar *output-from-echo* t) ;; if nil then echo will not be used for output
  (defvar *output-from-ltm* t) ;; if nil then learned associations will not be
                               ;; used for output.

;; These variables are used to control the inputs to the master routines.
(defvar *syllable-list*)
(defvar *syllable-lists*)
(defvar *is-a-of-recoding*)
(defvar *stimfam-and-respfams*)
(defvar *extra-syllables*)
(defvar *list-length*) ;; This variable also supplies list-length to the 
                       ;; strategy for written output of auditory recall.
(defvar *net-file*)
(defvar *digit-lists*)
(defvar *probes*)
(defvar *rsf* nil)

(defvar *number-of-consecutive-perfect-trials*)
(defvar *exit-criteria-routine*)
(defvar *maximum-learning-trials* 32)
(defvar *noticing-order* nil)

(defvar *consecutive-correct* 0)
(defvar *rt-array*) ; array used for holding reaction time results
(defvar *er-array*) ; array used for holding error results
(defvar *total-clock* 0) ; This clock is used when an experiment has several
                         ; different sessions and allows correct updating of
                         ; node and link activation.
(defvar *session-clock* 0); this variable is used to keep track of time since the beginning
                        ; of a session 

(defvar *names-of-syllable-lists*)
; This variable and the next represent a normal distribution table.
(defvar *normal-50-99* 
  '(0.01 .04 .06 .09 .11 .14 .16 .19 .21 .24 
    .27 .29 .32 .35 .37 .4 .43 .46 .48 .51
    .54 .57 .6 .63 .66 .69 .72 .76 .79 .82
    .86 .9 .93 .97 1.02 1.06 1.1 1.15 1.2 1.25
    1.31 1.37 1.44 1.51 1.6 1.7 1.81 1.9 2.1 2.58))
(defvar *normal-99s*
  '(2.36 2.39 2.43 2.49 2.54 2.61 2.7 2.81 2.96 3.29))
(defvar *standard-deviation-time* .97)
(defvar *dont-print-list* '(parent time-tag activation last-update last-forgetting))

(defvar *remaining-nodes* nil) ; used in soybean simulations

;These variables can be helpful for debugging:
(defvar *pairs*)
(defvar *CORRECT-SYLLABLE*)
(defvar *PROBE-POSITION*)
(defvar *STIMULI*)
(defvar *PROBE*)
(defvar *ANSWER*)
(defvar *COMPARISON-ANSWER*)
(defvar *STIMULUS-OBJECT*)
(defvar *CORRECT-RESPONSE-OBJECT*)
(defvar *SUBJECTS-RESPONSE*)
(defvar *CORRECT*)
(defvar *section*)
(defvar *node*)
(defvar *episode*)
(defvar *context-attributes* nil)
; I tried out context attributes as a way of simulating paired-associate learning
; when real words were used, but created some bugs which I didn't care to spend 
; much time debugging.  
;'(context1 context2 context3 context4 context5 context6 context7 context8 context9))

(defvar *time-to-access-an-object* 100) 


;;                  B. INITIALIZATION OF VARIABLES
(defun initialize-variables ()
  "This routine creates the net and sets the default values of variables and
properties. Along with zero-out-variables it sets the conditions at the beginning
of an experiment to absolute zero."
  (setf *total-clock* 0)
  (setf *clock* 0)
  (setf *epam-clock* 0)
  (zero-out-clocks)
  (form-net) 
  (setf  *experimenter* 'null-experimenter
         *time-to-distinguish-an-object* 100
         *time-to-enter-control-symbol* 10
         *new-stimulus-flag* nil
         *rsf* (change-focus-to nil)
         *search-list* nil
         (get 'signal-to-respond 'visual-auditory) 'signal-to-respond
         (get 'signal-to-respond 'auditory-visual) 'signal-to-respond
         (get 'auditory 'imagery-persistence) 1600
         (get 'visual 'imagery-persistence) 1600
         (get 'auditory 'sensory-persistence) 3800
         (get 'visual 'sensory-persistence) 250
         (get 'auditory 'visual) 'auditory-visual 
         (get 'visual 'auditory) 'visual-auditory))

(defun zero-out-variables ()
  "This routine clears all of the registers in preparation for a simulation
without changing any of the parameters which are subject to strategic or
experiment routine control."
  (setf (get 'auditory 'imagery-store) nil
        (get 'visual 'imagery-store) nil
        (get 'auditory 'sensory-store) nil
        (get 'visual 'sensory-store) nil
        (get 'auditory 'new-stimulus) nil
        (get 'visual 'new-stimulus) nil
        (get 'auditory 'open-group) nil
        (get 'visual 'open-group) nil
        *chunks* nil
        ; *rsf* nil
        *new-stimulus-flag* nil
        *trial* 0
        *errors* 0
        *starting-errors* 0
        *output* nil
        *position* 0)
   (create-new-episode)
   (zero-out-clocks)
   )

(defun turn-printing-off ()
  "This routine turns off all printing options."
   (setf *print-time-updates* nil
         *print-imagery-stores* nil
         *print-experimenter-updates* nil
         *print-epam-updates* nil
         *print-forgetting-updates* nil
         *print-net-updates* nil
         *print-strategy-updates* nil))

(defun turn-printing-on ()
  "This routine turns on all printing options."
   (setf *print-time-updates* t
         *print-imagery-stores* t
         *print-experimenter-updates* t
         *print-forgetting-updates* t
         *print-epam-updates* t
         *print-net-updates* t
         *print-strategy-updates* t))

(defun zero-out-clocks ()
  (setf *total-clock* (+ *total-clock* *clock*)
        *clock* 0
        *epam-clock* 0
        *sorting-clock* 0
        *articulation-clock* 0
        *writing-clock* 0))

(defun zero-out-except-epam-clocks ()
  (setf *total-clock* (+ *total-clock* *clock*)
        *epam-clock* (- *epam-clock* *clock*)
        *articulation-clock* 0
        *writing-clock* 0
        *sorting-clock* 0
        *clock* 0))

(defun get-print-copy-of (image)
  (cond ((null image) nil)
        ((listp image) (mapcar 'get-print-copy-of image))
        ((characterp image) image)
        ((numberp image) image)
        ((get-image-of image) (get-print-copy-of (get-image-of image)))
        ('else image)))

;;;;;;             C. USEFUL GENERAL PURPOSE ROUTINES
(defun get-value (object attribute)
 "This routine finds the value associated with the attribute in the association
list which is in the head cell of the object.  This is a command called J10
of the computer language IPL-5 which routinely puts association lists in head cells"
  (second (assoc attribute (car object))))

(defun put-value (object value attribute)
  "This routine associates the value with the attribute in the association 
list which is the head cell of the object.  This is a command called J11
in the computer language IPL-5 which routinely puts association lists in head 
cells"
  (let ((alist (assoc attribute (car object))))
    (if alist 
      (rplaca (cdr alist) value)
      (rplaca object (append (car object) (list (list attribute value)))))
    object))

(defun remove-value (object attribute)
  "This routine removes all values of the given attribute from the association
list which is the head cell of the object.  This is a command called J14 in the 
computer language IPL-5 which routinely puts association lists in head cells."
  (loop for sublist in (car object)
        for attrib = (car sublist)
        when (not (eql attrib attribute))
        collect sublist into alist
        finally (rplaca object alist))
  object)

(defun find-attribute-given-value (given-atom value)
  "This is the reverse of the usual routine for property lists.  Given an
atom, it finds the first attribute on the symbol-plist of the atom which leads
to the given value."
  (when (atom given-atom)
    (loop for tail on (symbol-plist given-atom) by #'cddr
          for att = (first tail)
          for val = (second tail)
          if (eq value val) return att)))

(defun seed-random-generator ()
  "This routine advaces the random generator by a number of bits determined by
the time." 
  (loop repeat (mod (get-universal-time) 10000)
        do (random 1)))

(defun reorder-list-randomly (lis)
  "This routine puts elements of a list in a random order"
  (loop with oldlist = lis
        with rnd = nil
        while oldlist
        do (setf rnd (random (length oldlist)))
        collect (nth rnd oldlist)
        do (setf oldlist (remove-nth-element (1+ rnd) oldlist))))      
    
(defun remove-nth-element (n lis)
  "This routine returns the list with the nth element removed"
     (loop repeat (1- n)
           with foot = (cdr lis)
           for item in lis
           do (pop foot)
           collect item into head
           finally (return (nconc head foot))))

(defun specify (object)
  "This routine supposes that if an object has one subobject, then it is
just a version of that subobject.  Any object with exactly one subobject
is recursively replaced by its subobject.  This routine is used so that information
about decay can be stored with atoms while they are in short-term memory or 
retrieval structures. It was called simplify in one earlier version.  It calls
itself recursively and thus specifies values of subobjects and attribute-value pairs 
as well"
  (cond
     ((atom object) object)
     ((null object) object)
     ('else (let ((obj (copy-object object)))
              (cond ((and (= (length obj) 2)
                          (not (eql (second obj) '~)))
                     (specify (second obj)))
                    ('else 
                     (loop for pair in (car obj)
                           do (rplaca (cdr pair) (specify (second pair))))
                     (loop for remainder on (cdr obj)
                           do (rplaca remainder (specify (car remainder))))
                     obj))))))
              

; EPAM Miscellaneous II

(defun change-string-to-object (obj is-a)
   (let ((object (convert-strings-to-character-lists obj)))
      (cond ((atom object) object)
            ('else (setf object (put-in-heads object is-a))
             (change-character-squiggles-to-squiggles object)
             object))))



(defun convert-strings-to-character-lists (object)
    (cond 
        ((stringp object) (cond ((= (length object) 1) (coerce object 'character))
                                ('else (coerce object 'list))))
        ((atom object) object)
        ('else (mapcar 'convert-strings-to-character-lists object))))

(defun put-in-heads (object is-a)
    (if (atom object)
      object
      (let ((result
              (cons nil (mapcar #'(lambda (x) (put-in-heads x is-a)) object))))
        (put-value result is-a 'is-a)
        result)))

(defun change-object-to-string (object)
  "This changes object into string but ignore the association lists in its list
structure.  Assumption is that the first cell of any list within the 
list structure is an association list and all atoms within the list 
structure are characters."
  (cond ((characterp object) (coerce (list object) 'string))
        ((atom object) object)
        ('else 
         (coerce (delete-all-squiggles 
                  (fix-all-numbers
                   (delete-all-nils 
                    (delete-all-parentheses 
                     (delete-head-cells object))))) 'string))))

(defun change-character-squiggles-to-squiggles (object)
   (loop for remainder on (cdr object)
     if (eql (car remainder) #\~)
     do (rplaca remainder '~)
     if (listp (car remainder))
     do (change-character-squiggles-to-squiggles (car remainder))))

(defun delete-all-squiggles (object)
  (loop for item in object
        if (not (eql item '~))
        collect item))

(defun fix-all-numbers (object)
  (loop for item in object
        if (numberp item)
        collect (coerce (+ item 48) 'character)
        else if t
        collect item))

(defun delete-all-nils (object)
  (loop for item in object
        if item collect item))

(defun delete-head-cells (object)
  (if (atom object) 
    object
    (mapcar 'delete-head-cells (cdr object))))

(defun delete-all-parentheses (object)
   (cond
      ((atom object) object)
      ((atom (car object))
            (cons (car object) (delete-all-parentheses (cdr object))))
      ('else (append (delete-all-parentheses (car object) )
             (delete-all-parentheses (cdr object))))))

(defun count-characters-in (object)
  "This routine counts all of the characters in the cdr of
an object and in the subobjects of its list structure."
   (cond 
      ((null object) 0)
      ((characterp object) 1)
      ((atom object) 0)
      ('else (+ (count-characters-in (car object)) 
                (count-characters-in (cdr object))))))

(defun convert-chunk-to-object (chunk &optional modality preserve-squiggle)
  "This routine turns a chunk or a group of chunks into objects.  
It converts subobjects after the head cell on the image into the objects 
that the chunks represent.  If the chunk is actually a list of chunks all 
of which share the same is-a, then the object created is of that is-a.
If the objects are a list of chunks that do not share the same is-a
then the modality is placed upon the newly created larger object as 
its is-a."
  (when (not modality) (setf modality 'auditory))
  (cond
   ((characterp chunk) chunk)
   ((numberp chunk) chunk)
   ((null chunk) nil)
   ((listp chunk) (convert-chunks-to-object chunk modality preserve-squiggle))
   ((get chunk 'image) (copy-object (get-print-copy-of (get chunk 'image))))
   ('else chunk)))

(defun convert-chunks-to-object (chunks modality &optional preserve-squiggle)
   (let ((result (list nil))
         (is-a nil))
      (loop for chunk in chunks
        for object = (convert-chunk-to-object chunk modality)
        if (atom object)
        do (nconc result (list object))
        else if preserve-squiggle ; this happens in context-effects simulation
        do (cond ((or (eql (car (last object)) '~)
                      (not (member '~ object))
                      (and (eql (second object) '~)
                           (not (eql (car (last result)) '~))))
                  (loop for subobject in (cdr object)
                    do (nconc result (list subobject))))
                 ((eql (second object) '~)
                  (loop for subobject in (cddr object)
                    do (nconc result (list subobject)))))
        else if (member '~ (cdr object))
        do (loop for subobject in (cdr object)
             if (not (eql subobject '~))
             do (nconc result (list subobject)))
        else if t
        do (nconc result (list object)))
      (setf is-a (loop with subobject-is-a = nil
                   for subobject in (cdr result)
                   for subis-a = (when (not (atom subobject))
                                    (get-value subobject 'is-a))
                   if (and subis-a
                           (not subobject-is-a))
                   do (setf subobject-is-a subis-a)
                   else if (and subis-a
                                subobject-is-a
                                (not (eql subis-a subobject-is-a)))
                   return nil
                   finally (return subobject-is-a)))
      (when (not is-a)
         (setf is-a modality))
      (put-value result is-a 'is-a)
      (specify result)))

(defun change-is-a (object category)
  "A better name for this routine would be convert-is-a-of object.  This routine when 
given an object and its category returns an object whose 
is-a is the category and whose subobjects all have the category as its is-a so that the
object will be sorted in that region of the net."
  (let ((partial-image (copy-object object)))
    (cond
     ((atom partial-image) partial-image)
     ((listp partial-image)
      (put-value partial-image category 'is-a)
      (loop for image-remainder on (cdr partial-image)
            do (rplaca image-remainder 
                       (change-is-a (car image-remainder) category)))))
    partial-image))


(defun copy-association-list (alist)
  (loop for pair in alist
        collect (copy-list pair)))
(defun copy-object (object)
  (cond ((atom object) object)
        ('else (cons (copy-association-list (car object))
                     (loop for subobject in (cdr object)
                           if (atom subobject) collect subobject
                           else collect (copy-object subobject))))))
 
(defun tab-over (n)
  "This routine prints n groups of five spaces"
  (loop repeat n do (princ "     ")))
(defun convert-tree-to-list ()
  "Convert-tree-to-list changes the net into a list so that it can be saved
to a file. It converts gensym names of nodes such as g254 into string names such
as g254 with quotes around it.  In order to unpack a list back into a tree, 
use the function convert list to tree. At present the only info that it doesn't 
preserve is info about the ancestor or descendents of an episode.  This info 
is not preserved in order to keep the file that is saved smaller and because
this routine would have to be rewritten were a list of values permitted -- 
descendents have such a list."
  (let* ((root-number (convert-gensym-to-integer *net*))
         (top-number root-number)
         (list-of-all-nodes (list *net*))
         (list-of-unexamined-nodes (list *net*))
         number array)
    (loop for node = (pop list-of-unexamined-nodes)
          while node
          do 
          (loop for plist on (symbol-plist node) by #'cddr
                for attribute = (first plist)
                for value = (second plist)
                if (not (or (member attribute 
                                    '(image tests entries parent 
                                      activation last-update time-tag
                                      ancestor descendents))
                            (characterp value)
                            (numberp value)
                            (member value list-of-all-nodes)))
                do 
                (setf number (convert-gensym-to-integer value))
                (when (> number top-number) (setf top-number number))
                (setf list-of-unexamined-nodes 
                      (cons value list-of-unexamined-nodes))
                (setf list-of-all-nodes
                      (cons value list-of-all-nodes))
                if (eql attribute 'entries)
                do (loop for pair in value
                         for episodes = (second pair)
                         do (loop for episode in episodes
                                  if (not (member episode list-of-all-nodes))
                                  do 
                                  (setf number (convert-gensym-to-integer episode))
                                  (when (> number top-number) (setf top-number number))
                                  (setf list-of-unexamined-nodes 
                                        (cons episode list-of-unexamined-nodes))
                                  (setf list-of-all-nodes
                                        (cons episode list-of-all-nodes)))
                         )))
    (setf array (make-array (1+ (- top-number root-number))))
    (loop for node in list-of-all-nodes
          do (setf (aref array 
                         (- (convert-gensym-to-integer node) root-number))
                   (convert-chunk-to-property-list node)))
    (setf number (1+ (- top-number root-number)))
    (setf list-of-all-nodes nil)
    (loop for count from 0 to (1- number)
          collect (aref array (- number count 1)) into result
          finally (return (cons root-number (reverse result))))))

(defun convert-gensym-to-integer (symbol)
  "Convert-gensym-to-integer given a gensym such as G124 it returns the integer
124.  The same routine also converts the string
G124 or node124 with quotes around it, to 124."
   (let ((charlist (convert-name-to-character-list symbol)))
     (when charlist 
       (convert-character-list-to-integer (mapcar 'char-code charlist)))))

(defun convert-name-to-character-list (name)
  "This routine returns a string followed by digits into a string of
characters representing the digits.  For example
it converts the string node124 surrounded by quotes into the list 
(#\1 #\2 #\4).  This routine will produce bugs if there are strings 
containing digits on images in the net."
  (let (symbol)
    (if (null (stringp name))
      (setf symbol (symbol-name name))
      (setf symbol name))
    (setf symbol (coerce symbol 'list))
    (loop with digits = '(#\0 #\1 #\2 #\3 #\4 #\5 #\6 #\7 #\8 #\9)
          for item in symbol
          when (member item digits)
          collect item into digit-list
          finally (return digit-list))))

(defun convert-character-list-to-integer (clist)
  "This routine converts a list of integers such as (49 50) into the value
of a number represented by the ascii values of those integers one after the 
other.  For example, since  49 is the ascii of 1 and 50 is the ascii of 2
the call (convert-character-list-to-integer '(49 50)) results in 12."
  (loop for sublist on clist
        with n = 0
        do (setf n (+ n (* (expt 10 (1- (length sublist)))
                           (- (car sublist) 48))))
        finally (return n)))

(defun convert-chunk-to-property-list (chunk)
  "This routine takes a chunk as input and converts all
gensyms on its property list into string representations of their names.
For example a #:G243 on the property list is converted to G243.  This 
routine will give weird results if their are atoms with names beginning 
with G which are not gensyms." 
  (loop for plist on (symbol-plist chunk) by #'cddr
        for attribute = (first plist)
        for value = (second plist)
        if (and (not (eql attribute 'ancestor))
                (not (eql attribute 'descendents)))
        append (list (convert-gensyms-to-strings attribute)
                     (convert-gensyms-to-strings value))))
 
(defun convert-gensyms-to-strings (object)
  (cond ((characterp object) object)
        ((numberp object) object)
        ((null object) nil)
        ((and (atom object) 
              (not (get object 'image))
              (not (get object 'parent)))
         object)
        ((atom object) (if (convert-name-to-character-list object)
                         (symbol-name object)
                         object))
        ('else (mapcar 'convert-gensyms-to-strings object))))

(defun convert-list-to-tree (lis)
  "This routine changes a list created by convert-tree-to-list back
into a tree and it returns the root node.  The first item in the list gives
the number of the old root node.  If items in the list are nil there is
no need to generate a symbol for them.  The assumption is that there will
be no nodes that don't have information on their p-lists."
  (setf *net* nil)
  (let ((array (make-array (length (cdr lis))))
        (number (car lis)))
    (gensym "node")
    (loop for item in (cdr lis)
          for n = 0 then (1+ n)
          when item 
          do (setf (aref array n) (gensym "node")))
    (loop for item in (convert-strings-to-gensyms (cdr lis) number array)
          for n = 0 then (1+ n)
          when item do (put-values-on-chunk (aref array n) item))
    (setf *net* (aref array 0))
    (setf *top-pseudochunk-node* (get (get *net* 'is-a) 'pseudochunk))
    *net*))

(defun put-values-on-chunk (chunk plist)
  "This routine puts the values defined by the p-list onto the chunk"
  (loop for sublist on plist by #'cddr
        for attribute = (first sublist)
        for value = (second sublist)
        do (setf (get chunk attribute) value)))
        
(defun convert-strings-to-gensyms (object number array)  
  "Convert-strings-to-gensyms uses the gensyms stored in the array to convert
string values for gensyms into the generated symbols in the
array." 
   (cond
      ((and (atom object) (null (stringp object))) object)
      ((stringp object) 
           (aref array (- (convert-gensym-to-integer object) number)))
      ('else (mapcar #'(lambda (x) 
                               (convert-strings-to-gensyms x number array))
                     object))))

(defun print-node-clearly (plist)
  "This routine shortens attributes so that they fit on the line it also
puts dashes between attribute-value pairs."
  (loop for list-tail on plist by #'cddr
        for n from 1
        for attribute = (first list-tail)
        for value = (second list-tail)
        initially (princ "(")
        do (when (not (= n 1)) (princ " "))
        (princ (cond ((eq attribute 'activation) 'act)
                     ((eq attribute 'category) 'cat)
                     ((eq attribute 'visual) 'vis)
                     ((eq attribute 'auditory) 'aud)
                     ((eq attribute 'visual-auditory) 'vis-aud)
                     ((eq attribute 'auditory-visual) 'aud-vis)
                     ('else attribute)))
        (princ "-")
        if (eq attribute 'image)
        do (print-image-clearly value)
        else do (princ value)
        finally (format t ")~%")))

(defun print-image-clearly (image)
  "This routine shortens attributes so that they fit on the line it also
puts dashes between attribute-value pairs."
  (princ "(")
  (loop for pair in (car image) 
        for n from 1
        for attribute = (first pair)
        for value = (second pair)
        initially (princ "(")
        do (when (not (= n 1)) (princ " "))
        (princ (cond ((eq attribute 'activation) 'act)
                     ((eq attribute 'visual) 'vis)
                     ((eq attribute 'auditory-visual) 'aud-vis)
                     ((eq attribute 'auditory) 'aud)
                     ('else attribute)))
        (princ "-")
        (princ (cond ((eq value 'activation) 'act)
                             ((eq value 'visual) 'vis)
                             ((eq value 'auditory-visual) 'aud-vis)
                             ((eq value 'auditory) 'aud)
                             ('else value)))
        finally (princ ")"))
  (loop for item in (cdr image)
        do (format t " ~a" item))
  (princ ")"))

(defun read-net-from (file-name)
  "Read net from a file.  If a net has been saved to a file it will have
a name like net135 and will be on the current 
directory.  This routine inputs the net in its list form and then 
converts that list into a net which it assigns to *net*.  This routine
can be used instead of initialize-retrieval-structure whenever a net 
has been saved to a file."
   (format t "Reading net from file = ~a~%" file-name) 
   (with-open-file (stream-name file-name :direction :input)
     (convert-list-to-tree (read stream-name))))

(defun write-net-to (file-name)
   (format t "Writing net to file: ~a~%" file-name)
   (with-open-file (stream-name file-name :direction :output)
     (write (convert-tree-to-list) :stream stream-name)
     (terpri stream-name)))

; Routines from Manager1.lsp

;           These Routines Manage STM and Verbal Learning Simulations
;              CONTENTS
;                  A. Routines that update the clock
;                  B1. STM Experimenter routines 
;                  B2. Context Effects in Letter Perception Experimenter routines
;                  B3. Verbal Learning Experimenter routines
;                  C. Decay manager routines
;                  D. Routines which prepare the initial net.
;                  E. Routines used to organize experiments and collect results.

;                  A. Routines that update the clock

(defun time-tally-to-add-new-test-to-node ()
  (when (and *print-time-updates*
             *print-epam-updates*)
    (format t "     EPAM Clock Time tally to add test to node = 8000 msec.~%"))
  (setf *epam-clock* (+ *epam-clock* 8000)))

(defun time-tally-to-add-episode-to-chunk ()
  (when (and *print-time-updates*
             *print-epam-updates*)
    (format t "     EPAM Clock Time tally to add entry to node = 1000 msec.~%"))
  (setf *epam-clock* (+ *epam-clock* 1000)))

(defun time-tally-to-build-node ()
  (when (and *print-time-updates*
             *print-epam-updates*)
    (format t "     EPAM Clock Time tally to create a New Node = 4000 msec.~%"))
  (setf *epam-clock* (+ *epam-clock* 4000)))

(defun time-tally-to-build-detail ()
  (when (and *print-time-updates*
             *print-epam-updates*)
    (format t "     EPAM Clock Time tally to build detail = 4000 msec.~%"))
  (setf *epam-clock* (+ *epam-clock* 4000)))

(defun time-tally-to-create-new-episode ()
  (when (and *print-time-updates*
             *print-epam-updates*)
    (format t "     EPAM Clock Time tally to Create a New Episode = 4000 msec.~%"))
  (setf *epam-clock* (+ *epam-clock* 4000)))

(defun time-tally-to-change-detail ()
  (when (and *print-time-updates*
             *print-epam-updates*)
    (format t "     EPAM Clock Time tally to change value in slot = 250 msec.~%"))
  (setf *epam-clock* (+ *epam-clock* 250)))

(defun time-tally-to-take-out-squiggle ()
   (when (and *print-time-updates*
              *print-epam-updates*)
      (format t "     EPAM Clock Time tally to take out squiggle = 250 msec.~%"))
   (setf *epam-clock* (+ *epam-clock* 250)))

(defun time-tally-to-build-slot ()
  (when (and *print-time-updates*
             *print-epam-updates*)
    (format t "     EPAM Clock Time tally to add a slot without a value = 4000 msec.~%"))
  (setf *epam-clock* (+ *epam-clock* 4000)))

(defun time-tally-to-move-episode-up-on-ladder ()
  (when (and *print-time-updates*
             *print-epam-updates*)
    (format t "     EPAM Clock Time tally to move episode up on ladder = 4000 msec.~%"))
  (setf *epam-clock* (+ *epam-clock* 4000)))
  
(defun time-tally-to-make-a-decision ()
  (when *print-time-updates*
    (princ "     Time tally to make a decision = "))
  (advance-the-timer 250))

(defun time-tally-for-node-traversal ()
  (when *print-time-updates*
    (princ "     Time tally for node traversal = "))
  (advance-the-timer 250))

(defun time-tally-to-guess-a-response ()
  (when *print-time-updates*
    (princ "     Time tally to guess a response = "))
  (advance-the-timer 1000))

(defun fixed-time-tally-to-recognize ()
  (when *print-time-updates*
    (princ "     Fixed time for recognition task  = "))
  (advance-the-timer *fixed-time-required-for-recognition-and-response*))

(defun variable-time-tally-to-recognize ()
  (when *print-time-updates*
    (princ "     Variable time for recognition of an episode  = "))
  (advance-the-timer *variable-time-required-to-recognize*))

(defun base-time-tally-to-find-associate ()
  "This parameter was estimated from the Suppes data."
   (when *print-time-updates*
    (princ "     Base time tally to find associate = "))
  (advance-the-timer 1000))
 
(defun time-tally-to-visualize-attribute ()
   (when *print-time-updates*
    (princ "     Time tally to visualize attribute = "))
  (advance-the-timer 250))
  
(defun time-tally-to-vocalize (object &optional vocal-or-subvocal)
  "The time to vocalize is *time-to-enter-an-object* in miliseconds per 
object plus *time-to-enter-syllable* in msec for each syllable in
an object.  In the past this routine included *time-tally-to-enter-a-chunk*
but with this version it doesn't so that can be assessed once for a group.
     There are several ways to count the number of syllables including the 
'character-method 'letter-method, and the 'subobject-method. The optional
variable vocal-or-subvocal can either take the value 'aloud 'silently or 'both.
If it is not specified then the assumption is subvocal."
  (let ((time-requirement (truncate (* *time-to-enter-a-syllable* 
                                       (apply *method-for-counting-syllables*
                                              (list object))))))
    (cond ((eq vocal-or-subvocal 'aloud)
           (when *print-time-updates*
             (format t "     Time tally to vocalize per syllable aloud = ~a msec.~%"
                     time-requirement))
           (setf *articulation-clock* 
                 (+ *articulation-clock* time-requirement)))
          ((eq vocal-or-subvocal 'both)
           (when *print-time-updates*
             (format t "     Time tally to vocalize and subvocalize per syllable = "))
           (setf *articulation-clock* 
                 (+ *articulation-clock* time-requirement))
           (advance-the-timer time-requirement))
          ('else 
           (when *print-time-updates*
             (format t "     Time tally to subvocalize per syllable = "))
           (advance-the-timer time-requirement)))))

(defun time-tally-per-object (&optional vocal-or-subvocal)
  (let ((time-requirement *time-to-enter-an-object*))
    (cond ((eq vocal-or-subvocal 'aloud)
           (when *print-time-updates*
             (format t "     Articulation Clock Time tally to vocalize per object aloud = ~a msec.~%"
                     time-requirement))
           (setf *articulation-clock* 
                 (+ *articulation-clock* time-requirement)))
          ((eq vocal-or-subvocal 'both)
           (when *print-time-updates*
             (format t "     Time tally to vocalize and subvocalize per object = "))
           (setf *articulation-clock* 
                 (+ *articulation-clock* time-requirement))
           (advance-the-timer time-requirement))
          ('else 
           (when *print-time-updates*
             (format t "     Time tally to subvocalize per object = "))
           (advance-the-timer time-requirement)))))
  
             
(defun time-tally-to-write (object)
  (let ((time-requirement (* *time-to-write-a-letter* (1- (length object)))))
    (when *print-time-updates*
      (format t "     Time tally to write object = ~a msec.~%"
              time-requirement))
    (setf *writing-clock* (+ *writing-clock* time-requirement))))

(defun time-tally-to-input-concurrent-routine ()
  "This routine inputs the concurrent-routine chunk from long term memory."
  (when *print-time-updates*
    (format t "     Time tally to input concurrent routine = ~a~%"
            *time-to-access-an-object*))
  (setf *articulation-clock* 
        (+ *articulation-clock* *time-to-access-an-object*)))

 (defun time-tally-to-access-chunk ()
  "This routine uses EPAM to discover a chunk and then enters the chunk in
*chunks*."
  (when *print-time-updates*
    (princ "     Time tally to access chunk = "))
  (advance-the-timer *time-to-access-an-object*))

(defun time-tally-to-enter-control-symbol ()
  "The time required to enter a control symbol in an imagery store has been
 arbitrarily set at 10 msec."
   (if *print-time-updates* (princ "     Entering Control Symbol Time = "))
   (advance-the-timer *time-to-enter-control-symbol*))

(defun time-tally-to-sort-in-net (test)
  "This routine is the time taken to sort through a test in the net.  It 
adds 250 msec to the time when the test is an attribute and just 10 
msec. when the test if 'is-a or a number.  Assumption here 
is that tests for attributes require eye movements."
   (cond ((eql test 'is-a) (setf *sorting-clock* (+ *sorting-clock* 10)))
         ((numberp test) (setf *sorting-clock* (+ *sorting-clock* 10)))
         ((eql test 'length) (setf *sorting-clock* (+ *sorting-clock* 10)))
         ((null test) nil)
         ((atom test) (setf *sorting-clock* (+ *sorting-clock* 250)))
         ('else
          (loop for tst in test
            with time-requirement = 10
            if (and (not (numberp tst)) 
                    (not (eql tst 'length))
                    (atom tst))
            do (setf time-requirement 250)
            finally (return (setf *sorting-clock* 
                                  (+ *sorting-clock* time-requirement)))))))
           
                         
                         
(defun time-tally-to-predict-from-hypothesis ()
   (when *print-time-updates*
    (princ "     Time tally to predict from hypothesis = "))
  (advance-the-timer 250))

(defun time-tally-to-make-opposite-prediction-from-hypothesis ()
   (when *print-time-updates*
    (princ "     Time tally to make opposite prediction from hypothesis = "))
  (advance-the-timer 250))

;                            EPAM-VI Subject Routines 
;                             by Howard B. Richman

;;                     FORGETTING ROUTINES

(defun find-current-time ()
  (+ (* 86400000 *activation*) 
     (+ *total-clock* *clock*)))

(defun forget-episodes (node)
  "This routine updates the node-activation."
  (cond ((get node 'entries)
         (let ((last-forgetting (get node 'last-forgetting))
               (this-time (find-current-time))
               time-interval)
           (when (not last-forgetting) 
             (format t "ERROR: NO last-forgetting at node~%") (break))
           (setf time-interval (- this-time last-forgetting))
           (setf (get node 'last-forgetting) this-time)
           (when *long-term-forgetting*
             (loop for pair in (get node 'entries)
                   for value = (second pair)
                   if (cdr value) 
                   do (ltf value time-interval *long-term-forgetting-parameter* node)))))
        ('else 
         (setf (get node 'last-forgetting) (find-current-time)))))

(defun ltf (values-list time-interval &optional parameter node)
  "This routine given a list of values destructively cuts the end off the
list if the exponential function based upon the 
*long-term-forgetting-parameter*.  Forgetting is exponential
with different parameters depending upon how many items are on the list.
The formula for the decay uses these decay parameters.  The chance
of the last item disappearing would be (1 - (1 - parameter)^t) where
t is the time-interval that has elapsed over which the decay has occurred and
the *decay-parameter* is the proportion of an item which disappears each 
second.  Note: the time interval is given in msec."
  (when (< time-interval 100) (setf time-interval 100))
  (when (not parameter) (setf parameter *long-term-forgetting-parameter*))
  (loop repeat (1- (length values-list))
        if (< (1+ (random 100000000)) 
            (* 100000000 (- 1 (expt (- 1 parameter)
                                    (/ time-interval 1000.0)))))
        do (when *print-forgetting-updates*
             (format t "Deleting link to episode = ~a from node ~a with image ~a due to ltf.~%"
                   (get (car (last values-list)) 'image)
                   node
                   (get node 'image)))
        (rplacd (nthcdr (- (length values-list) 2) values-list) nil)))

(defun activate-image (node)
  "This routine puts a new time-tag on a node and updates the
link activation to that node. If activate-link is true then 
it also activates the link to the node."
   (when node  
      (setf (get node 'time-tag) (find-current-time))
      (when *print-epam-updates*
         (format t "Activating node ~a for image ~a with time = ~a~%"
           node (get-image-of node) (find-current-time)))
      (forget-episodes node)))

(defun activate-link-to (node)
  "This routine refreshes the link-activation at a node."
  (setf (get node 'activation) *activation*))
      
(defun find-subjective-time-elapsed (item)
  "This routine finds the subject's perception of the time elapsed since the
last time that the node was activated (i.e. studied).  The idea behind this 
routine is that a subject's perceived recency of an event is related to the 
actual recency but varies along a normal curve related to the logarithm of 
the time which has been calculated here from the Lockhart experiment.
In general, the smaller the time elapsed, the smaller the subjective 
time-elapsed."
  (let* ((node (cond ((get-image-of item) item)
                     ('else (get-pseudochunk-for item))))
         (time-tag (get node 'time-tag))
         (this-time (find-current-time))
         time-elapsed)
    (when (not time-tag) (setf time-tag 0))
    (setf time-elapsed (- this-time time-tag))
    (when (< time-elapsed 0) 
      (format t "WARNING PROGRAM HAS MOVED BACKWARD IN TIME!~%"))
    time-elapsed))

(defun update-activation (current-strength time-elapsed decay-rate)
  "This routine gives an updated value of the activation at a node if given
as its inputs the current-strength of activation and the time-elapsed in msec
since the last update.  The decay-rate is given and is a number
calculated by multiplying a random deviation about a normal distribution 
by the *standard-diviation-time* which is currently .97.
This number is constant for a particular item and indicates the speed at which
its strength decays.  The procedure used is the following:
   a. Calculate the actual-time-passed in terms of days.
   b. Calculate the subjective-time-passed by multiplying the actual time passed
      by the deviation.
   c. Calculate the strength-reduction by calculating e to the
      minus of the subjective-time-passed.  
   d. Multiply the strength reduction by the current strength to 
      figure out the resulting node-activation."
  (when (< time-elapsed 0) (format t "WARNING: PROGRAM HAS GONE BACKWARD IN TIME~%"))
  (let* ((actual-time-passed (/ time-elapsed 86400000))
         (subjective-time-passed (* actual-time-passed decay-rate))
         (strength-reduction (exp (* -1 subjective-time-passed))))
    (* current-strength strength-reduction)))

(defun find-random-deviation ()
  (let* ((positive (random 2))
         (n (random 50))
         (variance (nth n *normal-50-99*)))
    (when (= n 49)
      (setf n (random 10))
      (setf variance (nth n *normal-99s*)))
    (cond ((= positive 0) variance)
          ('else (* -1 variance)))))

;; This routine creates a chunk in the net which can be used to associate info
;;   with a character or with a number.
(defun get-pseudochunk-for (character-or-number)
  "This routine creates a chunk in the net which can be used to associate 
strength information with an atom such as a character, number, or symbol.  It 
is needed because when there is more than one associated response, often the 
one with the greatest strength indicating recency is chosen."
   (let ((object (list nil character-or-number))
         chunk)
      (put-value object 'pseudochunk 'is-a)
      (setf chunk (distinguish object))
      (when (eql chunk *top-pseudochunk-node*)
         (discriminate object)
         (when *print-epam-updates* 
            (format t "Making pseudochunk for ~a~%" character-or-number))
         (when (null character-or-number) (break))
         (decf *nodes-in-net*)
         (setf chunk (distinguish object)))
      chunk))

(defun get-grandparent-of (chunk)
  (cond ((numberp chunk) nil)
        ((characterp chunk) nil)
        ('else (get (get chunk 'parent) 'parent))))

;from epamain2.lsp
(defun increment-activation ()
  (setf *total-clock* 0)
  (setf *clock* 0)
  (setf *epam-clock* 0)
  (incf *activation*))

; ------------------------------------------------------------------
; THESE ROUTINES ARE CHANGED BASED UPON MY FALL, 1998 REVISIONS OF EPAM.
(defun discriminate (object &optional pre-specified-test)
  "If possible, this routine creates a new branch in the net based upon info that
is on the object but not the image.  If a test is not already 
present at the node, it adds that test to the node."
   (let* ((node (distinguish object))
          (tests (cond (pre-specified-test (list pre-specified-test))
                       ('else (create-noticing-order-from object))))
          (image (get-image-of node)))
      (loop for test in tests
        for subimage = (specify (test-value-finder image test 'not-context))
        for subobject = (specify (test-value-finder object test))
        if (eq node *top-pseudochunk-node*)
        do (create-node-for-object object 1 node)
        else if (and (listp subobject)
                     (study subobject))
        return t
        else if (and (or (not subimage)
                         (eql subimage #\_))
                     (and subobject
                          (not (eql subobject #\_))))
        do 
        (when *print-epam-updates*
           (format t "Discriminating object = ~a at node with image = ~a~%"
             object image))
        (add-test-to-node test node)
        (create-node-for-object object 
          (car (member test (get node 'tests) :test #'equal)) ; this finds the internal name of the test as stored at the node so that the branching node can be found
          node)
        (return t))))

          

(defun create-node-for-object (object test node)
  "This routine adds a new terminal node to the net which extends from the
given test node via its branching node for the given test."
  (let ((new-node (create-node object test node)))
    (instantiate-a-new-image object new-node)
    (activate-image new-node)
    (when (and *print-epam-updates* 
               (not (eql node *top-pseudochunk-node*))) 
      (format t "Creating new node with image = ~a for object = ~a~%" 
              (get-image-of new-node)
              object))
    new-node))

(defun create-node (object test node)
  "This routine creates a new terminal node in the net which extends from the 
given test node via its branching node for the given test."
  (let ((test-value (test-value-extractor object test))
        (branching-node (get node test))
        (new-node (form-node)))
    (when (and (not (eql test 'is-a))
               (not (eql node *top-pseudochunk-node*)))
      (time-tally-to-build-node))
    (setf (get branching-node test-value) new-node
          (get new-node 'parent) branching-node)
    (activate-image new-node)
    (activate-link-to new-node)
    new-node))

(defun study-when-overlearning (attribute stimulus response &optional position1 position2 rsf)
  (when *print-epam-updates*
    (format t "Random number is less then *sal-c* and epam learning is available.~%")
    (format t "So, studying-when-correct and re-associating with episode.~%"))
  (setf *epam-clock* *clock*)
  (cond ((= (random 2) 0) 
         (study stimulus)
         (associate attribute stimulus response position1 position2 rsf 'stimulus)
         )
        ('else 
         (study response)
         (associate attribute stimulus response position1 position2 rsf 'response)))
   (when *print-net-updates* (print-net)))

(defun move-episode-up-on-ladder (chunk)
   "This is called by associate routine when associating after *sal-c* 
in other words after correct responding.  The system has just tried to 
create a new node for the correct response, but it couldn't do so because 
there was nothing additional it could learn about the stimulus.  Thus the 
only way it could make the episode which is in the *rsf* and also on the 
stimulus more accessible would be to pop the older episode off of the 
chunk.  If this proves to be too powerful, I could limit it so that the older
episode only gets eliminated if it already sorts to another node further 
down the chunk."
   (let* ((is-a (get-value (get-image-of *rsf*) 'is-a))
          (entries (assoc is-a (get chunk 'entries))))
      (when (and (member *rsf* (second entries))
                 (not (eql *rsf* (car (second entries)))))
         (rplaca (cdr entries) (cdr (car (cdr entries))))
         (time-tally-to-move-episode-up-on-ladder)
         t)))
      
(defun associate (is-a stimulus-object response-object &optional position1 position2 rsf correct?)
  "The stimulus and response are EPAM objects Lisp atoms, 
position1 is the position of the stimulus, position2 is the position
of the response.  If rsf is true, whatever episode is in the 
retrieval structure focus is used as the episode of association.
The following algorithm is used to perform the association:
    1. If there is currently no subnet for either the stimulus or response,
       is-a of object then create a net by discriminating one or the other.
    2. Do one association of stimulus with response then exit."
   (let ((stimulus 
           (cond ((atom stimulus-object) (convert-chunk-to-object stimulus-object))
                 ('else stimulus-object)))
         (response 
           (cond ((atom response-object) (convert-chunk-to-object response-object))
                 ('else response-object))))
      (when (eql (get-chunk-for stimulus) *net*)
         (discriminate stimulus)) ; creates net for the is-a of object that 
      ; the stimulus is, if none currently exists
      (when (eql (get-chunk-for response) *net*)
         (discriminate response)) ; creates net for the is-a of object that
      ; the response is, if none currently exists
      (when (not position1) (setf position1 1))
      (when (not position2) (setf position2 2))
      (when (not is-a) (setf is-a 'episode))
      (when (or (eql position2 'before) (eql position2 'after))
         (setf *anchor-chunk* (get-chunk-for stimulus)))
      (let ((stimulus-response-object 
             (create-stimulus-response-object is-a stimulus response position1 position2)))
         (cond (rsf (or *rsf*
                        (create-new-episode)))
               ((and (get-image-of *rsf*)
                     (not-different? stimulus-response-object (get-image-of *rsf*))))
               ((recognize is-a stimulus response position1 position2))
               ((create-new-episode)))
         (cond
          ;This is called when association after *sal-c*
          ((eql correct? 'stimulus) 
           (or 
               (prog1
                (familiarize stimulus-response-object *rsf* 'surface)
                (create-link-to *rsf* (get-chunk-for stimulus-object))
                (create-link-to *rsf* (get-chunk-for response-object)))
               (move-episode-up-on-ladder (get-chunk-for stimulus-object))))
          (correct?  ; This occurs if over-learning studied the response
           (prog1
            (familiarize stimulus-response-object *rsf* 'surface)
            (create-link-to *rsf* (get-chunk-for stimulus-object))
            (create-link-to *rsf* (get-chunk-for response-object))))
           ;This is called when an error leads to the associate routine
          ('else 
           (prog1
            (associate-if-error stimulus-response-object position2)
            (create-link-to *rsf* (get-chunk-for stimulus-object))
            (create-link-to *rsf* (get-chunk-for response-object))
            ))))))

(defun associate-if-error (stimulus-response-object position2)
  "This routine assumes that the *rsf* contains the relevant episode. It 
exits t if something was learned, or it exits nil.  It is called repeatedly
when the goal of the system is to continue associating until the stimulus,
given the episode, elicits the response."
  (let* ((subjects-response (specify (test-value-finder (get-image-of *rsf*) position2)))
         (response (specify (test-value-finder stimulus-response-object position2)))
         (correct (perfect-subobject-match? subjects-response response)))
    (cond 
     ;if the response is already complete on the image, just familiarize at surface level
     (correct 
      (when *print-epam-updates*
        (format t "The response is already complete on the image so just familiarizing at surface level.~%"))
       (familiarize stimulus-response-object *rsf* 'surface))
     ;if there is no response on the episode, study the response, then add it to the episode
     ((or (null subjects-response)
          (eql subjects-response #\_))
      (when *print-epam-updates*
        (format t "There was no response on episode, so studying the response then familiarizing the episode at the surface level~%"))
      (study response)
      (loop while (familiarize stimulus-response-object *rsf* 'surface))
      t)
     ;if the response is not completely familiar than add detail to it.
     ((and (not (atom (specify response)))
           (familiarize stimulus-response-object *rsf* nil (list position2)))
      (when *print-epam-updates*
        (format t "The response was notcompletely familiar, so added detail to it.~%"))
      (familiarize stimulus-response-object *rsf* 'surface)
      t)
     ;or just familiarize to fill in slots and recreate pointers
     ('else 
      (familiarize stimulus-response-object *rsf* 'surface))
      )))
    


(defun create-stimulus-response-object (is-a stimulus response position1 position2)
  (let ((stimulus-response-object (list nil))
        (stimulus-object (cond ((atom stimulus) (convert-chunk-to-object stimulus))
                               ('else stimulus)))
        (response-object (cond ((atom response) (convert-chunk-to-object response))
                               ('else response))))
    (put-value stimulus-response-object is-a 'is-a)
    (cond ((and (member stimulus *beginning-control-symbols*)
                (eql response 'end-of-list))
           (format t "ERROR stimulus is 'beginning and response is 'end-of-list~%")
           (break))
          ((member stimulus *beginning-control-symbols*)
           (put-test-value-on stimulus-response-object response-object 1))
          ((eql response 'end-of-list)
           (put-test-value-on stimulus-response-object stimulus-object -1))
          ((and (eql position1 'before)
                (eql position2 'after))
           (put-test-value-on stimulus-response-object stimulus-object 2)
           (put-test-value-on stimulus-response-object response-object 3)
           (put-test-value-on stimulus-response-object '~ 1))
          ('else 
           (put-test-value-on stimulus-response-object stimulus-object position1)
           (put-test-value-on stimulus-response-object response-object position2)))
    stimulus-response-object))

(defun recognize-srobject (object &optional positions)
  "This routine returns the episode found attached to the chunk that goes with 
the given positions."
   (let ((locations (cond ((not positions)
                           (create-noticing-order-from object))
                          ('else (reorder-list-randomly positions)))))
      (loop with is-a = (test-value-extractor object 'is-a) 
        for location in locations
        for stimulus = (test-value-finder object location)
        for stimulus-chunk = (cond ((eql location 'after) *anchor-chunk*)
                                   ('else (get-chunk-or-pseudochunk-for (specify stimulus))))
        for episode = (find-correct-episode-attached? is-a stimulus-chunk object)
        if episode 
        return episode)))

(defun recognize (is-a stimulus response &optional position1 position2)
  "This routine returns the episode found attached to either the stimulus or 
response."
   (when (not position1) (setf position1 1))
   (when (not position2) (setf position2 2))
   (let ((object (create-stimulus-response-object is-a
                  stimulus
                  response
                  position1
                  position2)))
      (setf *rsf* (recognize-srobject object '(1 2)))))
     
(defun quick-recognize (is-a stimulus response &optional position1 position2)
  "This routine returns the episode found attached to either the stimulus or 
response.  It is used when the subject is in a hurry as in Anderson-and-Reder's
fan effect experiment.  The difference between this and recognize is that the 
subject only checks one -- either the stimulus or response -- not both."
   (when (not position1) (setf position1 1))
   (when (not position2) (setf position2 2))
   (let ((object (create-stimulus-response-object is-a
                  stimulus
                  response
                  position1
                  position2)))
      (cond ((< (random 2) 1)
             (setf *rsf* (recognize-srobject object '(1))))
            ('else
             (setf *rsf* (recognize-srobject object '(2)))))))

(defun find-correct-episode-attached? (is-a chunk object)
  (let ((sorting-clock *sorting-clock*))
     (when *print-epam-updates* 
        (format t "Searching for correct episode at ~a.~%" (get-print-copy-of chunk)))
     (when (and chunk
                (not (eql chunk #\_)))
        (setf *position-of-episode-elicited* 1)
        (loop 
          for episode in (find-episodes is-a chunk)
          for n from 1
          if (> n 1)
          do 
          (incf *position-of-episode-elicited*)
          (variable-time-tally-to-recognize)
          if (not-different? object (get-image-of episode))
          return (progn
                  (setf *sorting-clock* sorting-clock)
                  (when *print-epam-updates*
                     (format t "A possibly correct episode was found..~%"))
                  (change-focus-to episode))
          finally (setf *sorting-clock* sorting-clock)))))

(defun create-new-episode ()
  (when *print-epam-updates*
    (format t "Creating a New Episode.~%"))
  (change-focus-to (gensym "episode"))
  (incf *number-of-episodes*)
  (time-tally-to-create-new-episode)
   (cond (*create-episode-having-two-slots*
          (setf (get *rsf* 'image) (list nil #\_ #\_)))
         ('else (setf (get *rsf* 'image) (list nil)))))


(defun find-associate (is-a stimulus &optional position2 instant rsf)
  "This routine finds a recent response associated with a stimulus or 
stimulus-chunk. The given position is a locator which tells how to find the 
response in the episode. If no position2 is supplied, then the response in the 
second position is the default.  If instant is specified, then the first response
to be generated is always accepted.  Instant is specified for highly automatic
translations.  If use-search is true, then the subject takes his time to 
respond by searching in order to fill in blanks."
   (let ((position1 nil))
      (when (not position1)
         (setf position1
               (cond ((eql position2 'after) 'before)
                     ((eql position2 'before) 'after)
                     ('else 1))))
      (when (not position2) (setf position2 2))
      (when (not is-a) (setf is-a 'episode))
      (when (or (eql position2 'before)
                (eql position2 'after))
         (setf *anchor-chunk* (get-chunk-for stimulus)))
      (let ((stimulus-response-object 
             (create-stimulus-response-object is-a stimulus #\_ position1 position2)))
         (cond (rsf (find-response-on-rsf stimulus-response-object position2))
               (instant (find-first-response-at-stimulus-chunk is-a stimulus position2))
               ('else (find-response-at-stimulus-chunk stimulus-response-object 
                       position2 position1))))))

(defun find-response-at-stimulus-chunk (object position2 position1)
   (let ((episode (recognize-srobject object (list position1))))
      (get-print-copy-of (specify (test-value-finder (get-image-of episode) position2)))))
                    


(defun find-first-response-at-stimulus-chunk (is-a stimulus position2)
   (let* ((episode (car (find-episodes is-a (get-chunk-or-pseudochunk-for stimulus))))
          (response (specify (test-value-finder (get-image-of episode) position2))))
    (get-print-copy-of response)))



(defun extract-associate (is-a stimulus &optional position2 rsf)
  (get-chunk-for (find-associate is-a stimulus position2 'instant rsf)))

(defun find-response-on-rsf (object position2)
  (let ((image (specify (get-image-of *rsf*))))
    (when (and image
               (not-different? object image))
       (get-print-copy-of (test-value-finder image position2)))))

(defun search-for-more-complete-image (obj episode)
  "Search-for-more-complete-image searches net below the chunk for an object
looking for a descendent chunk which has the current episode, 
the one in *rsf* attached."
  (let ((object (specify obj)))
    (cond ((atom object) object)
          ((not (member #\_ object)) (get-print-copy-of object))
          ('else  (setf *search-interrupt* #'(lambda () (eval '*new-stimulus-flag*)))
                  (let* ((exit-test #'(lambda (x) 
                                        (member episode (find-episodes 'episode x))))
                         (chunk (get-chunk-for object))
                         (result (search-down-in-net chunk object exit-test)))
                    (when (or (null result)
                              (eql result 'blank))
                      (setf result chunk))
                    (combine-episodic-image-with-chunk-image object result))))))


(defun combine-episodic-image-with-chunk-image (image chunk)
  "This routine combines information from an episodic image 
with a chunk's image and returns the result"
  (cond ((atom (specify image)) (specify image))
        ('else 
         (let ((episodic-image (get-print-copy-of (copy-object image)))
               (chunk-image (get-print-copy-of (get-image-of chunk))))
           (loop for attribute in (create-noticing-order-from chunk-image)
                 for chunk-value = (test-value-finder chunk-image attribute)
                 for episodic-value = (test-value-finder episodic-image attribute)
                 if (and chunk-value (eql episodic-value #\_))
                 do (put-test-value-on episodic-image chunk-value attribute)
                 else if (atom episodic-value)
                 do 'nil
                 else if t
                 do (combine-episodic-image-with-chunk-image episodic-value chunk-value)
                 finally (return episodic-image))))))
                 


(defun active-node-continuation-test (node)
  (eql (get node 'activation) *activation*))

;; Null-experimenter is a routine that does nothing.  It is the initial value
;;   of experimenter so that the initial housekeeping can be completed without
;;   invoking the experimenter. 
(defun null-experimenter ())





(defun order-episodes-by-subjective-recency (episodes)
  (let ((pairs (loop for episode in episodes
                     for time-elapsed = (find-subjective-time-elapsed episode)
                     for recency = (* time-elapsed (exp (* *standard-deviation-time*
                                              (find-random-deviation))))
                     collect (list recency episode))))
    (loop for pair in (sort-alists pairs)
          collect (second pair))))
          
(defun sort-alists (alists)
  (when alists
    (loop for alist = alists then (cdr sorted-list)
          for sorted-list = (put-lowest-pair-first alist)
          collect (car sorted-list) into result
          if (null (cdr alist))
          return result)))
        
(defun put-lowest-pair-first (alists)
  (loop with lowest-value = (first (first alists))
        with lowest-pair = (first alists)
        for lis in (cdr alists)
        for value = (first lis)
        if (< value lowest-value)
        do (setf lowest-value value)
        (setf lowest-pair lis)
        finally (return (cons lowest-pair (remove lowest-pair alists)))))

(defun find-associates (is-a stimulus-chunk &optional position)
  "This routine finds a list of responses associated with a stimulus-chunk.The list is not ordered."
   (when (not position) (setf position 2))
   (when (or (eql position 'before) (eql position 'after))
      (setf *anchor-chunk* stimulus-chunk))
   (loop for episode in (find-episodes is-a stimulus-chunk)
        collect (get-print-copy-of (test-value-finder (get-image-of episode) 
                  position))))
  
         
(defun find-episodes (is-a stimulus-chunk)
  "This finds a list of all of the episodes associated with a stimulus object
or chunk. The episodes are ordered by subjective recency."
  (let ((episodes (get-episodes (get-chunk-or-pseudochunk-for (specify stimulus-chunk)) 
                                is-a)))
    (when episodes (setf *number-of-episodes-elicited* (length episodes))
          (order-episodes-by-subjective-recency episodes))))

;  These lines may need to be added back in to find-episodes under the let statement if I try 
;  to debug the *context-attributes* idea
;    (cond (episodes (setf *number-of-episodes-elicited* (length episodes))
;                     (order-episodes-by-subjective-recency episodes))
;          ((characterp stimulus-chunk) nil)
;          ((numberp stimulus-chunk) nil)
;          ((not (get stimulus-chunk 'parent)) nil)
;          ('else (find-episodes is-a (get (get stimulus-chunk 'parent) 'parent)))))

(defun are-there-multiple-episodes-here? (is-a stimulus-chunk)
  (cdr (get-episodes (get-chunk-or-pseudochunk-for (specify stimulus-chunk)) 
                                is-a)))
  
(defun get-episodes (chunk is-a)
  (forget-episodes chunk)
  (second (assoc is-a (get chunk 'entries))))  
         
(defun get-chunk-or-pseudochunk-for (item)
  (cond ((null item) nil)
        ((eql item #\_) nil)
        ((listp item) (get-chunk-for item))
        ((or (numberp item) (characterp item)) (get-pseudochunk-for item))
        ((not (get item 'image)) (get-pseudochunk-for item))
        ('else item)))
                             
(defun find-item-after (chunk lis)
  (cond ((member chunk *beginning-control-symbols*)
         (cond ((eql (car lis) '~) nil)
               ('else (car lis))))
        ('else (loop with result
                     for lis-remainder on lis
                     for current-chunk = (get-chunk-for (car lis-remainder))
                     if (is-it-either-same-or-ancestor? current-chunk chunk)
                     do (setf result (second lis-remainder))
                     (cond ((eql result '~) (return nil))
                           ('else (return result)))))))

(defun find-item-before (chunk lis)
  (cond ((eql chunk 'end-of-list)
         (cond ((eql (car (last lis)) '~) nil)
               ('else (car (last lis)))))
        ('else 
         (loop for previous-element = nil then element
               for element in lis
               for current-chunk = (get-chunk-for element)
               with result = nil
               if (is-it-either-same-or-ancestor? current-chunk chunk)
               do (cond ((eql previous-element '~) (setf result nil))
                        ('else (setf result previous-element)))
               finally (return result)))))

(defun insert-value-before-anchor-chunk (image value)
  "This routine inserts the value before the item on the image.  It tries 
to do so before the last instance of the item on the image, in other 
words, it works from the back to do so."
    (cond ((or (null *anchor-chunk*)
               (eql *anchor-chunk* 'end-of-list))
           (put-position-value-on image value -1)
           t)
           ((loop for lis-remainder on (cdr image)
                  for result = (car lis-remainder)
                  for current-chunk = (get-chunk-for (second lis-remainder))
                  with best-result = nil
                  with best-sublist = nil
                  if (is-it-either-same-or-ancestor? current-chunk *anchor-chunk*)
                  do (setf best-sublist lis-remainder)
                  (setf best-result result)
                  finally (cond ((null best-sublist) 
                                 (return nil))
                                ((eql best-result '~) 
                                 (rplacd best-sublist (cons value (cdr best-sublist)))
                                 (return t))
                                ('else 
                                 (rplaca best-sublist value)
                                 (return t)))))
           ((is-it-either-same-or-ancestor? (get-chunk-for (second image)) *anchor-chunk*)
            (rplacd image (cons value (cdr image))))
           ('else nil)))

(defun insert-value-after-anchor-chunk (image value)
  "This routine inserts the value after the item on the image.  It exits nil if
the item can't be found on the image."
  (cond ((or (null *anchor-chunk*)
             (member *anchor-chunk* *beginning-control-symbols*))
         (put-position-value-on image value 1)
         t)
        ((eql value 'end-of-list)
         (loop for lis-remainder on (cdr image)
               for current-chunk = (get-chunk-for (car lis-remainder))
               if (is-it-either-same-or-ancestor? current-chunk *anchor-chunk*)
               do (rplacd lis-remainder nil)
               and return t))
        ('else (loop with result 
                     for lis-remainder on (cdr image)
                     for current-chunk = (get-chunk-for (car lis-remainder))
                     if (is-it-either-same-or-ancestor? current-chunk *anchor-chunk*)
                     do (setf result (second lis-remainder))
                     (cond ((eql result '~) 
                            (rplacd lis-remainder (cons value (cdr lis-remainder)))
                            (return t))
                           ((null (cdr lis-remainder))
                            (nconc lis-remainder (list value))
                            (return t))
                           ('else
                            (rplaca (cdr lis-remainder) value)
                            (return t)))))))

(defun put-length-on (image length)
  (let ((desired-length (specify length)))
    (cond ((numberp desired-length)
           (shorten-image-to-size image desired-length))
          ((eql desired-length #\_)
           (when (not (eql (car (last image)) '~))
             (nconc image (list '~)))))))

(defun put-position-value-on (image test-value tst)
   (cond ((plusp tst) (position-value-from-front image test-value tst))
         ('else (position-value-from-back image test-value tst)))
   image)

(defun position-value-from-front (image value position)
  "This routine is called by the create-token routine to position a subobject
at the proper location of the image.  If there is a ~ on the image, to 
indicate an unknown number of subobjects after this point, then this routine
moves the squiggle backwards.  If after adding subobjects.  If there is 
a predetermined length on the image, then this routine removes the squiggle if
that length has been exceeded"
  (let ((predetermined-length (get-value image 'length)))
    (shorten-image-to-size image predetermined-length)
    (cond ((is-squiggle-in-way? image position) 
           (insert-value-before-squiggle image value position))
          ((<= (length image) position)
           (loop for n from (length image) to (1- position)
                 do (nconc image (list #\_))
                 finally (nconc image (list value '~))))
          ('else (rplaca (nthcdr position image) value)))
    (shorten-image-to-size image predetermined-length)
    image))

(defun is-squiggle-in-way? (image position)
  (loop for n from 0 upto position
        for subimage in image
        if (eql subimage '~) return t))
(defun insert-value-before-squiggle (image value position)
  (loop for subimage on image
        for coming-item = (second subimage)
        for n from 1
        with add-ons = (list value)
        if (eql coming-item '~)
        return (loop for m from (+ n 1) upto position
                     do (setf add-ons (cons #\_ add-ons))
                     finally (rplacd subimage (append add-ons (cdr subimage))
                                     ))))
(defun remove-squiggle-from-image (image)
  (loop for subimage on image
        for coming-item = (second subimage)
        if (eql coming-item '~)
        return (rplacd subimage (cddr subimage))))
(defun position-value-from-back (image value position)
  "This routine is called by the create-token routine to create an image based
upon the tests that are in the net.  It is used
with tests that are negative numbers (-2 indicates the second to last
subobject).  Chunks for the ends of visual words often use these tests."
  (let ((m (* -1 position))
        (predetermined-length (get-value image 'length)))
    (shorten-image-to-size image predetermined-length)
    (cond ((is-squiggle-in-way? (reverse image) m) 
           (insert-value-after-squiggle image value position))
          ((null (cdr image))
           (nconc image 
                  (append (list '~ value) (loop repeat (1- m) collect #\_))))
          ((>= m (length image))
           (format t "ERROR: Trying to use negative test to position value in front of image!~%")
           (break))
          ('else (rplaca (nthcdr (+ (length image) position) image) value)))
    (shorten-image-to-size image predetermined-length)
    image))

(defun shorten-image-to-size (image desired-length)
  "This routine shortens an image to the size specified -- which is usually the
size of an object which can't be differentiated from the image.  The idea is 
that a squiggle holds the place of an undertermined number of objects, but 
somehow it is unnecessary.  There are several possible cases:
   1. If the image was shorter or the same as the desired length, do nothing.
   2. If the image was longer than the desired length, then this is because
either there is an extra squiggle or there is some overlap between the front 
part and the back part.  For example the object might have been (nil a b c d e)
and so the length is 5 and the image might be (a b c ~ d e) or 
even (nil a b c d ~ c d e) and this routine will change it to (nil a b c d e).
This routine exits nil if the length was already the desired length."
  (when (numberp desired-length)
    (let ((current-length (length (cdr image))))
      (setf *number-of-slots-added-by-length* 0)
      (cond ((< current-length desired-length)
             (loop for image-remainder on image
                   if (eql (car image-remainder) '~)
                   do 
                   (rplaca image-remainder #\_)
                   (loop repeat (- desired-length current-length)
                         do (rplacd image-remainder (cons #\_ (cdr image-remainder))))
                   (return t)
                   finally (loop repeat (- desired-length current-length)
                                 do 
                                 (incf *number-of-slots-added-by-length*)
                                 (nconc image (list #\_))))
             t)
            ((= current-length desired-length)
             (loop for image-remainder on image
                   if (eql (car image-remainder) '~)
                   do 
                   (incf *number-of-slots-added-by-length*)
                   (rplaca image-remainder #\_)
                   (return t)))
            ('else 
             (loop for image-remainder on image
                   for coming-item = (second image-remainder)
                   for n from 1
                   if (eql coming-item '~)
                   return (rplacd image-remainder 
                                  (nthcdr (1+ (- current-length desired-length))
                                          image-remainder))
                   if (= n desired-length)
                   return (rplacd (cdr image-remainder) nil))
             t)))))

(defun insert-value-after-squiggle (image value position)
  (loop for subimage on image
        for item = (car subimage)
        for n from 1
        with add-ons = (list value)
        if (eql item '~)
        return (loop for m from (length image) upto (- n position 2)
                     do (setf add-ons (append add-ons (list #\_)))
                     finally (rplacd subimage (append add-ons (cdr subimage))
                                     ))))

(defun activate (item)
  "Item may be any atom including a number or a character."
  (activate-image (get-chunk-or-pseudochunk-for (specify item))))

(defun find-length-of-object (object)
  (cond ((get-value object 'length))
        ('else (let ((possible-length (1- (length object))))
                 (cond ((member '~ object) nil)
                       ((= possible-length 0) nil)
                       ('else possible-length))))))

(defun add-test-to-node (test node)
  (let ((tests (get node 'tests)))
    (cond ((member test tests :test #'equal) nil)
          ('else
           (when (get node 'tests)
             (when *print-epam-updates* 
               (format t "Adding additional test = ~a at node = ~a~%"
                       test node))
             )
           (let ((branching-node (gensym)))
             (setf (get node 'tests) (add-test-to-tests test tests)
                   (get node test) branching-node
                   (get branching-node 'parent) node)
             (time-tally-to-add-new-test-to-node))))))

(defun add-test-to-tests (test tests)
  "This routine adds tests to a list of test in such a way that 
tests are ordered in approximately the same order as the noticing
order. So if test is a positive number and a greater positive number
is enountered, test is put before that number.  If test is a 
negative number and a smaller negative number is encountered, then 
test is placed before that negative number.  If test is an attribute
on the noticing order and another attribute is encountered that is 
also on the noticing order than test is placed before that attribute
on the list of tests."
 (cond ((and (numberp test) (> test 0))
         (insert-into-positive-order test tests))
        ((and (numberp test) (< test 0))
         (insert-into-negative-order test tests))
        ((eql test 'length) (append tests (list 'length)))
        ((member test *noticing-order* :test #'equal)
         (insert-into-attribute-order test tests))
        ('else (append tests (list test)))))

(defun insert-into-positive-order (n list)
  (cond ((null list) (list n))
        ((and (numberp (car list)) (> (car list) n))
         (cons n list))
        ((eql (car list) 'length)
         (cons n list))
        ('else 
         (cons (car list) (insert-into-positive-order n (cdr list))))))

(defun insert-into-negative-order (n list)
  (cond ((null list) (list n))
        ((and (numberp (car list)) (< (car list) n))
         (cons n list))
        ((eql (car list) 'length)
         (cons n list))
        ('else 
         (cons (car list) (insert-into-negative-order n (cdr list))))))

(defun insert-into-attribute-order (attribute list)
  (cond ((null list) (list attribute))
        ((and (member (car list) *noticing-order* :test #'equal)
              (member (car list) 
                      (member attribute *noticing-order* :test #'equal)
                      :test #'equal))
         (cons attribute list))
        ('else 
         (cons (car list) 
               (insert-into-attribute-order attribute (cdr list))))))
 
(defun instantiate-a-new-image (object node)
  (let ((new-image (create-token object)))
    (setf (get node 'image) new-image)
    new-image))

(defun study (object &optional test)
  "This is EPAM's main learning routine.  Repeated applications create an exact 
image of an object on its own node in the net."
  (cond ((atom object) 
         (activate object)
	 nil)
  ((discriminate object test) t)
  ('else nil)))

(defun create-link-to (episode value)
   (when (and value
             (not (member value *beginning-control-symbols*))
             (not (eql value 'end-of-list)))
    (let* ((image (get-image-of episode))
           (is-a (specify (get-value image 'is-a)))
           (value-image (get-image-of value))
           (chunk (cond (value-image value)
                        ('else (get-pseudochunk-for value))))
           (episodes (get-episodes chunk is-a)))
      (when (and is-a (not (eql is-a #\_)))
        (activate-image chunk)
        (when (and chunk 
                   (not (member episode episodes)) 
                   (not (eql chunk episode))
                   (not (eql value episode))
                   (not (eql chunk *net*)))
          (add-episode-to-episodes episode chunk is-a))))))

(defun get-semantic-ancestor-of (node)
  (loop for possible-node = node then grandparent-node
        for parent-node = (get possible-node 'parent)
        for grandparent-node = (get parent-node 'parent)
        for test = (find-attribute-given-value grandparent-node parent-node)
        if (not grandparent-node) 
        return possible-node
        if (not test) 
        do (setf *node* node)
        (format t "Something is fishy. There is no branch from grandparent to parent.~%")
        (break)
        if (not (member test *context-attributes*))
        return possible-node))
        

   

(defun add-episode-to-episodes (episode chunk is-a-of-association)
    (forget-episodes chunk)
    (let* ((is-a (specify is-a-of-association))
           (associates (get chunk 'entries))
           (pair (assoc is-a associates))
           (value (second pair)))
      (cond ((null associates) 
             (when *print-epam-updates*
                (format t "Adding episode = ~a to chunk = ~a~%"
                  episode (get-print-copy-of chunk)))
             (time-tally-to-add-episode-to-chunk)
             (setf (get chunk 'entries)
                   (list (list is-a (list episode)))))
            ((null pair)
             (when *print-epam-updates*
                (format t "Adding episode = ~a to chunk = ~a~%"
                  episode (get-print-copy-of chunk)))
             (time-tally-to-add-episode-to-chunk)
             (nconc associates (list (list is-a (list episode)))))
            ((null value)
             (format t "ERROR! is-a present but not associated with any episodes!~%")
             (break))
            ((member episode value)
             nil)
            ('else 
             (when *print-epam-updates*
                (format t "Adding episode = ~a to chunk = ~a~%"
                  episode (get-print-copy-of chunk)))
             (time-tally-to-add-episode-to-chunk)
             (nconc value (list episode))))))
  
(defun create-slot (image test)
   "This routine turns the value of the test on the image into a slot."
   (let ((current-value (test-value-finder image test)))
      (when (and current-value
                 (not (eql current-value #\_)))
         (put-test-value-on image #\_ test)
         (time-tally-to-build-slot))))

(defun familiarize (object &optional node surface tests)
  "This routine returns t if something learned. It first goes
through the image at the node at the top level. If surface
is true, then the routine will only do a top level familiarization
-- it will not familiarize the subimages."
   (when (not node) (setf node (get-chunk-for object)))
   (activate node)
   (when (not tests) (setf tests (create-noticing-order-from object)))
   (loop with image = (get-image-of node)
     with subimage
     with value-image
     with subobject
     with value-object
     with result = nil
     for attribute in tests
     if (eql attribute 'before)
     do (setf *anchor-chunk* (get-anchor-chunk-closest-to-end object))
     if (eql attribute 'after)
     do (setf *anchor-chunk* (get-anchor-chunk-closest-to-beginning object))
        do
     (setf subimage (test-value-finder image attribute))
     (setf subobject (test-value-finder object attribute))
     (setf value-image (get-chunk-for (specify subimage)))
     (setf value-object (get-chunk-for (specify subobject)))
     
     ; don't do anything if there is no subobject
     if (or (null value-object)
            (eql value-object #\_))
     do 'nil
     else if (eql value-image #\_)
     do ; fill slot and return
     (put-test-value-on image (get-uncertain-copy-of-image subobject) attribute)
     (when *print-epam-updates*
        (format t "Just filled slot with image of ~a at position ~a of image =~a~%"
          subobject attribute image))
     (setf result t)
     (cond ((eql attribute 'is-a) nil) ; is-a comes for free 
           ((eql attribute 'length) nil) ; length comes for free
           ('else (time-tally-to-change-detail)))
     else if (null value-image)
     do 
     (cond  
           ((take-out-squiggle subobject image attribute))
           ((put-test-value-on image (get-copy-of-image subobject) attribute)
            (when *print-epam-updates*
               (format t "Just put image of subobject = ~a on image = ~a at position ~a~%"
                 subobject image attribute))
            (cond ((eql attribute 'is-a) 
                   (setf result t)); is-a comes for free 
                  ((eql attribute 'length)
                   (loop repeat *number-of-slots-added-by-length*
                     do (time-tally-to-build-slot))
                   (return t))
                  ((eql value-object #\_)
                   (time-tally-to-build-slot)
                   (return t))
                  ('else (time-tally-to-build-detail)
                   (return t))))
           ('else
            ; this is when there is no value on image.  Sometimes you can't put-test-value-on 
            ; the image, such as when the image is (nil a ~) and the object is (nil ~ c d ~)
            ; this happens when you are building a serial list since you can only build such 
            ; a list from anchor points.
                nil))
     else if (eql attribute 'before)
     do 'nil ; don't insert items before unless before is null or blank
     
     ; fill secondary slots if the image's chunk is an ancestor of the object's chunk
     else if (and (not (eql value-image value-object))
                  (not (atom (specify subobject)))
                  (not (atom (specify subimage)))
                  (combine-test-value-on-image (specify subobject) subimage))
     ; this will happen when there is already a subimage but it does not include 
     ; some of the info of the subobject's image.
     do 
     (when *print-epam-updates*
        (format t "Just put image of ~a at position ~a on image = ~a~%"
          subobject attribute image))
     (cond ((eql attribute 'is-a) nil) ; is-a comes for free 
           ('else (time-tally-to-build-detail)
            (return t)))
     ; Replace incorrect item at a time cost of about 4 seconds and return
     else if (add-detail-to-slots (specify subobject) subimage)
     do (setf result t)
     ; here the value is already correct
     else if (eql value-image value-object)
     do 'nil
     else if t ; here there is already a value, but it is either not as complete as the image 
                  ; at the response chunk and the two object's value and image's value do not sort to 
     ; the same node.  
     do (when *print-epam-updates*
           (format t "Putting image of ~a at position ~a on image = ~a~%"
             subobject attribute image))
     (put-test-value-on image (get-copy-of-image subobject) attribute)
     (cond ((eql attribute 'is-a) nil) ; is-a comes for free 
           ('else (time-tally-to-build-detail)
            (return t)))
     finally (return (cond (result t) ; if filled a slot return t
                           (surface nil) ; if only familiarizing at surface level return nil
                           ('else
                            (loop for subattribute in tests
                              with subsubobject
                              with subsubimage
                              if (eql subattribute 'before)
                              do (setf *anchor-chunk* (get-anchor-chunk-closest-to-end object))
                              if (eql subattribute 'after)
                              do (setf *anchor-chunk* (get-anchor-chunk-closest-to-beginning object))
                              do 
                              (setf subsubobject (test-value-finder object subattribute))
                              (setf subsubimage (test-value-finder image subattribute))
                              if (and (listp (specify subsubobject))
                                      (listp (specify subsubimage))
                                      (add-detail-at-subobject-level (specify subsubobject) 
                                       subsubimage))
                              return t))))))

(defun put-value-on-rsf (value attribute &optional rsf)
   (when (and value
              (not (eql value #\_)))
      (when (not rsf) (setf rsf *rsf*))
      (let ((sr-object (list nil)))
         (put-test-value-on sr-object value attribute)
         (wait-until-learning-available)
         (familiarize sr-object rsf (list attribute))
         (time-tally-to-add-detail-to-retrieval-structure))))

(defun remove-value-from-rsf (attribute &optional rsf)
   "This function is used to remove the attribute 'confused in concept formation
simulations when that episode is transferred to a new node." 
   (when (not rsf) (setf rsf *rsf*))
   (let ((image (get-image-of rsf)))
      (remove-value image attribute)))

(defun wait-until-learning-available ()
   (when (> *epam-clock* *clock*)
      (advance-the-timer (- *epam-clock* *clock*)))
   (setf *epam-clock* *clock*))

(defun take-out-squiggle (subobject image attribute)
  "This routine looks for a situation like the following.
     1. image = (a b c ~ d e f) 
     2. *anchor-chunk* = c
     3. subobject = d
     4. attribute is 'after
   And if it finds these conditions it takes out the squiggle."
  (cond ((eql attribute 'after) (take-out-squiggle-after subobject image))
        ((eql attribute 'before) (take-out-squiggle-before subobject image))
        ('else nil)))

(defun take-out-squiggle-after (subobject image)
   (let ((remainder (cond ((member *anchor-chunk* *beginning-control-symbols*) image)
                          ('else (loop for lis-remainder on (cdr image)
                                   for current-chunk = (get-chunk-for (car lis-remainder))
                                   if (is-it-either-same-or-ancestor? current-chunk *anchor-chunk*)
                                   return lis-remainder)))))
      (when (and remainder
                 (eql (second remainder) '~)
                 (is-it-either-same-or-ancestor? (get-chunk-for (third remainder))
                  (get-chunk-for subobject)))
         (rplacd remainder (cddr remainder))
         (time-tally-to-take-out-squiggle)
         t)))
               
(defun take-out-squiggle-before (value image)
   (cond ((eql *anchor-chunk* 'end-of-list) nil)
         ('else 
          (let ((value-chunk (get-chunk-for value)))
             (loop for lis-remainder on (cdr image)
               for possible-match = (car lis-remainder)
               for possible-squiggle = (second lis-remainder)
               for current-item = (third lis-remainder)
               if (and (eql possible-squiggle '~)
                       (is-it-either-same-or-ancestor? (get-chunk-for current-item) 
                        *anchor-chunk*)
                       (is-it-either-same-or-ancestor? (get-chunk-for possible-match)
                        value-chunk))
               do (rplacd lis-remainder (cddr lis-remainder))
               (time-tally-to-take-out-squiggle)
               and return t)))))
                                     
 
(defun add-detail-at-subobject-level (object image)
   ;(setf *object* object)
   ;(setf *image* image)
   ;(break)
    (loop for attribute in (create-noticing-order-from object)
          with result = nil
          for subimage = (test-value-finder image attribute)
          for subobject = (test-value-finder object attribute)
          for value-image = (get-chunk-for (specify subimage))
          for value-object = (get-chunk-for (specify subobject))
          
          ; don't do anything if there is no subobject
          if (or (null value-object)
                 (eql value-object #\_))
          do 'nil
          
          ;before putting the value for a subobject, discriminate it 
          if (study subobject)
          return t
          if (eql value-object value-image)
          do 'nil
          
          ; fill slot at a cost of about 500 msec and without returning 
          else if (eql value-image #\_)
          do 
          (put-test-value-on image (list nil value-object) attribute)
          (when *print-epam-updates*
            (format t "Just filled slot with image of ~a at position ~a of image =~a~%"
                    subobject attribute image))
          (setf result t)
          (cond ((eql attribute 'is-a) nil) ; is-a comes for free 
                ('else (time-tally-to-change-detail)
                       ))
          
          ; build and fill slots
          else if (null value-image)
          do
          (when *print-epam-updates*
            (format t "Putting image of subobject = ~a on image = ~a at position ~a~%"
                    subobject image attribute))
          (put-test-value-on image value-object attribute)
          (cond ((eql attribute 'is-a) 
                 (setf result t)); is-a comes for free 
                ((eql attribute 'length)
                 (loop repeat *number-of-slots-added-by-length*
                       do (time-tally-to-build-slot))
                 (return t))
                ((eql value-object #\_)
                 (time-tally-to-build-slot)
                 (return t))
                ('else (time-tally-to-build-detail)
                       (return t)))
          ; Replace incorrect item at a time cost of about 4 seconds and return
          else if t
          do (when *print-epam-updates*
               (format t "Putting image of ~a at position ~a on image = ~a~%"
                       subobject attribute image))
          (put-test-value-on image value-object attribute)
          (cond ((eql attribute 'is-a) nil) ; is-a comes for free 
                ('else (time-tally-to-build-detail)
                       (return t)))
          finally (return result)))
        
(defun combine-test-value-on-image (object image)
  (let ((objects-image (get-image-of (get-chunk-for object))))
    (cond ((atom objects-image) nil)
          ('else
           (loop for attribute in (create-noticing-order-from objects-image)
                 for subobject = (specify (test-value-finder objects-image attribute))
                 for subimage = (test-value-finder image attribute)
                 for specify-subimage = (specify subimage)
                 with result = nil
                 if (null subobject)
                 do 'nil
                 else if (eql subobject  #\_)
                 do 'nil
                 else if (eql specify-subimage subobject)
                 do 'nil
                 else if t 
                 do
                 (put-test-value-on image (get-chunk-for subobject) attribute)
                 (setf result t)
                 finally (return result))))))

(defun add-detail-to-slots (object image)
  (let ((value-object (get-chunk-for object))
        (value-image (get-chunk-for image)))
    (cond ((atom object) nil)
          ((atom (specify image)) nil)
          ((is-it-either-same-or-ancestor? value-image value-object)
           (loop for attribute in (create-noticing-order-from image)
                 for subobject = (test-value-finder object attribute)
                 for subimage = (test-value-finder image attribute)
                 for specify-subimage = (specify subimage)
                 with result = nil
                 if (null subobject)
                 do 'nil
                 else if (eql subobject  #\_)
                 do 'nil
                 else if (eql specify-subimage #\_)
                 do
                 (when *print-epam-updates*
                   (format t "Filling slot with image of ~a at position ~a of image =~a~%"
                           subobject attribute image))
                 (put-test-value-on image (list nil (get-chunk-for subobject)) attribute)
                 (setf result t)
                 (cond ((eql attribute 'is-a) nil) ; is-a comes for free 
                       ('else (time-tally-to-change-detail)))
                 finally (return result))))))
        
(defun get-copy-of-image (object)
  (cond ((atom (specify object)) (specify object))
        ('else (copy-object (get (get-chunk-for object) 'image)))))

(defun get-uncertain-copy-of-image (object)
  "This routine is called by add-detail -- which is called by familiarize -- 
in order to add uncertainty to an image.  This uncertainty appears as 
represented by (list nil subobject) so that the image at a chunk is subject to the 
*forgetting-parameter* which caused decay of images in the *rsf* during 
the expert-memory simulations."
  (let ((obj (specify object)))
    (cond ((eql #\_ obj) #\_)
          ((eql '~ obj) '~)
          ((null obj) nil)
          ('else (list nil (get-copy-of-image obj))))))


(defun put-test-value-on (image test-value test)
  (cond ((numberp test) (put-position-value-on image test-value test) t)
        ((eql test 'after) (insert-value-after-anchor-chunk image test-value))
        ((eql test 'before) (insert-value-before-anchor-chunk image test-value))
        ((eql test 'length) (put-length-on image test-value))
        ((member test *context-attributes*) (put-context-on image test-value test))
        ((listp test) (put-compound-test-value-on image test-value test) t)
        ('else (put-value image test-value test) t)))

(defun put-compound-test-value-on (object test-value compound-test)
  "This routine puts all of the values of a compound test onto the object and
then returns the object."
  (loop for test in compound-test
        for value in (cond ((null test-value) nil)
                           ((listp test-value) (cdr test-value))
                           ('else (cdr (get-image-of test-value))))
        do (put-test-value-on object value test)))

(defun create-token (object &optional image subnet)
  "The output of this routine is an image that is created from the tests that
are in the net.  This routine creates the image by sorting the object through
the net, and collecting information at each stop along the way"
  (when (not image) (setf image (list nil)))
  (when (not subnet) (setf subnet *net*))
  (cond ((atom object) object)
        ('else 
         (loop for test in (get subnet 'tests)
               for subobject = (test-value-finder object test)
               for value = (get-chunk-or-letter-for subobject)
               for node = (when value (get (get subnet test) value))
               if node 
               do 
               (put-test-value-on image
                                  value
                                  test)
               (return (create-token object image node)) ; recurse on subnet
               finally (return image)))))
                                                      

;;                 Manager Routines

(defun advance-the-timer (n)
  "This routine adds N time units to clock and calls experimenter.  Experimenter
will perform various updates based upon the clock. It also returns the unit
added to the time."
  (when *print-time-updates* (format t "~a msec.~%" n))
  (setf *clock* (+ *clock* n))
  (update-all-timers n)
  (apply *experimenter* nil)
  n)

(defun update-all-timers (decrement)
  "Update-all-timers decrements the time for each object in a sensory or imagery
store and eliminates items from the front of the store if their time is up."
  (duration-update-for-imagery-store 'auditory 'imagery-store decrement)
  (duration-update-for-sensory-store 'auditory 'sensory-store decrement)
  (duration-update-for-sensory-store 'visual 'sensory-store decrement)
  (shorten-pad)
  (shorten-chunks))

(defun duration-update-for-sensory-store (modality is-a-of-store decrement)
  "Duration-update changes the objects in the store beginning with
the first one on the list.  It subtracts the decrement from the time 
that is associated with each object. Output is the remaining items in the store
that have not yet run out of time."
  (loop for item in (get modality is-a-of-store)
        for time = (- (get-value item 'time) decrement)
        when (> time 0)
          do (put-value item time 'time)
          and collect item into memory-store
        finally (setf (get modality is-a-of-store) memory-store)))

(defun duration-update-for-imagery-store (modality is-a-of-store decrement)
  (duration-update-for-alist (get modality is-a-of-store) decrement))
(defun duration-update-for-alist (alist decrement)
  (loop for sublist in alist
        for item = (second sublist)
        with old-time = nil
        with time = nil
        with decay-time = nil
        if (atom item) do 'nil
        else if t 
        do (setf old-time (get-value item 'time))
        (setf time (- old-time decrement))
        (setf decay-time (if (> old-time 0) (abs time) decrement))
        (put-value item time 'time)
        (when (< time 0) 
          (decay-phonemes item decay-time *loop-decay-parameter*))))
  
(defun decay-phonemes (object time-interval parameter)
  "This routine is called when an item has reached the point where it is 
beginning to decay in the sensory store.  The formula for the decay uses the
decay parameter.  According to an estimate based on the Peterson & Peterson
data, the decay parameter may be about .049 of the information.  The chance
of any particular item decaying would be (1 - (1 - *loop-decay-parameter*)^t) where
t is the time-interval that has elapsed over which the decay has occurred and
the *decay-parameter* is the proportion of an item which disappears each 
second.  When a character has decayed, its value is replaced by #\_. This 
routine exits t if there is anything left of the item after decay."
  (loop for object-tail on (cdr object)
        for subobject = (car object-tail)
        if (eq subobject #\_) 
        do 'nil
        else if (listp subobject)
        do (decay-phonemes (car object-tail) time-interval parameter)
        else if (< (1+ (random 10000)) 
                   (* 10000 (- 1 (expt (- 1 parameter) 
                                       (/ time-interval 1000.0)))))
        do (rplaca object-tail #\_)))

(defun shorten-chunks ()
  "This routine limits the length of the accumulator to 5 by cutting off the
tail if it is longer."
   (cond
      ((< (length *chunks*) 6) nil)
      ('else (rplacd (nthcdr 4 *chunks*) nil))))

(defun shorten-pad ()
  "This routine limits the length of the pad to 2 by cutting off the
tail if it is longer."
  (let ((store (get 'visual 'imagery-store)))
    (when (cddr store)
      (rplacd (cdr store) nil))))

(defun change-focus-to (new-rsf)
  "This routine is used in order to put a new cell in *rsf*.  It causes decay 
of information in the image of the old cell that used to be in *rsf*"
  (when (and *rsf* 
             (not (eql *rsf* new-rsf)))
    (decay (get-image-of *rsf*)))
  (setf *rsf* new-rsf))

(defun decay (image)
  "This routine is called whenever an image leaves the *rsf*.  Several of the
newly added values of the image are replaced by #\_ depending upon the 
*forgetting-parameter*."
  (when image
    (loop for alist in (car image)
          for value = (second alist)
          if (listp value)
          do (cond ((null (car value))
                    (cond ((< (random 100) *forgetting-parameter*)
                           (rplaca (cdr alist) #\_))
                          ('else
                           (rplaca (cdr alist) (second value)))))
                   ('else (decay value))))
    (loop for sublist on (cdr image)
          for value = (car sublist)
          if (listp value)
          do (cond ((null (car value))
                    (cond ((< (random 100) *forgetting-parameter*)
                           (rplaca sublist #\_))
                          ('else
                           (rplaca sublist (second value)))))
                   ('else (decay value))))))

(defun create-noticing-order-from (object)
  "The noticing order consists of all of the tests that are 
in *noticing-order* followed by all of the attributes not on the noticing order
then the position tests either in forward order (represented by positive numbers
or in backward order represented by negative numbers and last by
the length of the list.  When non-context is true, the noticing
order does not include *context-attributes*"
  (let* ((possible-tests (find-all-tests-on object)))
    (loop for test in (cons 'is-a (remove 'is-a *noticing-order*))
          if (test-value-finder object test)
          collect test into priority-tests
          and do (setf possible-tests (remove test possible-tests))
          finally (return (append priority-tests possible-tests)))))
          
(defun find-all-tests-on (image)
  "This routine forms a list beginning with the attributes on the image and
then if there is an ordered list including the positions on the ordered list 
until the end or just before a ~ is encountered, then if a squiggle is 
encountered the positions from the back of the list back until just after 
the squiggle, or if no squiggle appears, the attribute length.  This routine
is used by the process which tries to find a difference between an object and
an image.
  For example 
     1. If the image is (((is-a syllable)) c a t) then the result
        would be (is-a 1 2 3 'length)
     2. If the image is (((is-a syllable) (category a)) c ~ t) then the 
        result would be (is-a category 1 -1)
     3. If the image is (((is-a syllable) ~ b c ~) then result would be (is-a after before)"
  (append (find-attributes-on image)
          (when (cdr image)
            (cond ((and (eql (second image) '~)
                        (eql (car (last image)) '~))
                   (list 'after 'before))
                  ('else 
                   (append (loop for n from 1 upto (1- (length image))
                                 for subimage in (cdr image)
                                 while (and subimage
                                            (not (eql subimage '~)))
                                 collect n)
                           (cond ((member '~ image)
                                  (loop for n from -1 downto  (* -1 (- (length image) 2))
                                        for subobject in (reverse (cdr image))
                                        while (not (eql subobject '~))
                                        collect n))
                                 ('else (list 'length)))))))))
 
(defun find-attributes-on (object)
  (loop for pair in (car object)
        for attribute = (car pair)
        if (not (member attribute *non-testable-attributes*))
        collect attribute))

(defun not-different? (object image)
  "A not-different? occurs if for all values on the object the image either has the 
same value, nil, or #\_.  If the image is  (nil a b c d e) and the object (nil #\_ #\_ c d), 
then the result will be t because c is in the third position and d is in 
the fourth in both the object and the image."
   (let ((specify-object (specify object))
         (specify-image (specify image)))
      (cond ((eql specify-object specify-image) t)
            ((null image) t)
            ((eql specify-image #\_) t)
            ((eql specify-object #\_) t)
            ((atom specify-object) nil)
            ((eql (get-chunk-for object) specify-image) t)
            ((atom specify-image) nil) 
            ('else (loop 
                     for test in (create-noticing-order-from specify-object)
                     for subobject = (test-value-finder specify-object test)
                     for subimage = (test-value-finder specify-image test)
                     if (and (eql test 'after)
                             (eql *anchor-chunk* 
                               (get-anchor-chunk-closest-to-beginning specify-object))
                             (not (is-anchor-chunk-on-image? specify-image)))
                     return nil
                     if (and (eql test 'before)
                             (eql *anchor-chunk*
                               (get-anchor-chunk-closest-to-end specify-object))
                             (not (is-anchor-chunk-on-image? specify-image)))
                     return nil
                     if (and (eql test 'before)
                             (not (eql *anchor-chunk*
                                    (get-anchor-chunk-closest-to-end specify-object))))
                     do 'nil
                     else if (and (eql test 'after)
                                  (not (eql *anchor-chunk* 
                                         (get-anchor-chunk-closest-to-beginning 
                                          specify-object))))
                     do 'nil
                     else if (not (not-different? subobject subimage))
                     return nil
                     finally (return t))))))


(defun is-anchor-chunk-on-image? (image)
   (loop 
     for item in (cdr image)
     for value-item = (get-chunk-for item)
     if (eql value-item *anchor-chunk*)
     return t))

(defun difference-finder (object image &optional tests)
  "This routine determines if any substantial aspect of an object differs
from the image at a node. This routine calls difference finder and recursively 
is called by difference finder in order to look for differences between subobjects
and subimages.  If either the the object  differ from its image or its subobjects
differ from their images, it returns the test.  If the optional tests are supplied
then difference finder just looks for a difference with the tests specified.  If 
not it looks for a difference with all of the values on the image.  Changed on 
November 11, 2000, so that if the object or subobject is #\_ "
  (cond ((eql object image) nil)
        ((null image) nil)
        ((eql image #\_) nil)
        ((eql object #\_) nil)
        ((atom object) t)
        ((and (atom image)
              (eql (get-chunk-for object) image))
         nil)
        ((atom image) t)
        ('else (when (not tests) 
                 (setf tests (create-noticing-order-from image)))
               (loop for test in tests
                     with subobject
                     with subimage
                     if (eql test 'before)
                     do (setf *anchor-chunk* (get-anchor-chunk-closest-to-end object))
                     if (eql test 'after)
                     do (setf *anchor-chunk* (get-anchor-chunk-closest-to-beginning object))
                     do 
                     (setf subobject (test-value-finder object test))
                     (setf subimage (test-value-finder image test))
                     if (difference-finder subobject subimage)
                     return test))))

(defun get-anchor-chunk-closest-to-end (object)
  (loop for subobject in (reverse (cdr object))
        if (or (null subobject)
               (eql subobject '~))
        do 'nil
        else if t
        return (get-chunk-for subobject)))

(defun get-anchor-chunk-closest-to-beginning (object)
  (loop for subobject in (cdr object)
        if (or (eql subobject '~)
               (null subobject))
        do 'nil
        else if t
        return (get-chunk-for subobject)))

      

(defun get-image-of (node)
  "This is a way to get the image of a node which returns nil if the node is a character or
a number."
  (cond ((or (numberp node) (characterp node)) nil)
        ((listp node) nil)
        ('else (get node 'image))))
 
(defun is-it-either-same-or-ancestor? (over-node under-node)
  (or (eql over-node under-node)
      (is-it-an-ancestor? over-node under-node)))

(defun is-it-an-ancestor? (over-node under-node)
  "This routine determines whether node1 is an ancestor of node2 not including
the *net* as an ancestor."
  (cond ((characterp over-node) nil)
        ((numberp over-node) nil)
        ((characterp under-node) nil)
        ((numberp under-node) nil)
        ('else (loop for parent = (get (get under-node 'parent) 'parent)
                     then (get (get parent 'parent) 'parent)
                     while parent
                     if (eql parent over-node) return t))))

(defun get-chunk-for (object)
  "This routine finds the chunk that an object sorts to in the net.  Atoms are
not sorted in the net since atoms are treated as the primitives of the system.
The chunk for an atom is an atom.  If the object has just one subobject then 
it is first simplified as the  subobject before sorting it in the net.  Thus
the object (nil a) becomes a.  If the object is (nil (nil a b)), then the
chunk found is the chunk that (nil a b) sorts to."
  (cond ((atom (specify object)) (specify object))
        ('else (distinguish object))))

(defun sort-to-node (object &optional category)
  "This routine sorts an object in the net.  Then if there is a category
associated with the object it does an additional sort."
  (let* ((obj (copy-object object))
         (chunk (get-chunk-for object)))
    (when (not category)
      (setf category (extract-associate 'category chunk)))
    (cond ((and category (listp obj))
           (setf obj (change-is-a object category))
           (get-chunk-for obj))
          ('else chunk))))

(defun get-chunk-or-letter-for (object)
  "This routine finds the chunk that an object sorts to in the net.  Atoms are
not sorted in the net since atoms are treated as the primitives of the system.
The chunk for an atom is an atom.  If the object has just one subobject then 
it is first simplified as the  subobject before sorting it in the net.  Thus
the object (nil a) becomes a.  If the object is (nil (nil a b)), then the
chunk found is the chunk that (nil a b) sorts to."
   (let ((obj (specify object))
         chunk
         letter)
      (cond ((atom obj) obj)
            ('else 
             (setf chunk (distinguish obj))
             (setf letter 
                   (when (get chunk 'image)
                      (find-associates 'letter chunk 2)))
             (cond ((null letter) chunk)
                   ((null (cdr letter))
                    (cond ((match-to-letter? obj chunk) (car letter))
                          ('else #\_)))
                   ('else #\_))))))

(defun match-to-letter? (object chunk)
   (let ((image (test-value-finder 
                  (get-image-of (car (get-episodes chunk 'letter)))
                  1)))
      (not (difference-finder object image))))
    
(defun distinguish (object &optional subnet)
  "This routine sorts object starting at top node in subnet.  It 
continues until it reaches a terminal node (a node without a test) or until 
it encounters a testing node whose tests do not have a branch for this object"
  (when (not subnet) (setf subnet *net*))
  (activate-link-to subnet)
  (loop 
    for test in (get subnet 'tests)
    for value = (test-value-extractor object test)
    for branching-node = (get subnet test)
    ; test-value-extractor calls get-chunk-or-letter-for which calls 
    ; get-chunk-for which in turn calls distinguish. This is where EPAM 
    ; recurses in order to distinguish subobjects while recognizing objects..
    for node = (when value (get branching-node value))
    do (time-tally-to-sort-in-net test)
    if node 
    return (distinguish object node)
    ; in other words recurse with next subnet
    if (eql value #\_) ; if info about a value is missing, return this node
    return (progn (decay (get-image-of subnet))
                       (return subnet))
    finally (progn (decay (get-image-of subnet))
                       (return subnet)))); if can't sort - return the top node in the subnet.

(defun find-value-for-compound-test (object compound-test)
  (loop for test in compound-test
        for subobject = (test-value-finder object test)
        if (or (null subobject)
               (eql subobject #\_))
        return nil
        collect (test-value-finder object test) into olist
        finally (return (cons (list (list 'is-a 'compound-value)) olist))))

(defun test-value-extractor (object tst)
  "This routine determines the tested value of object or image at test node
in net.  If the test stored with a node is a number then it performs a position
test, if the value stored with a node is an attribute, then it finds the value
for that attribute on the object."
(let ((subobject (test-value-finder object tst)))
    (cond ((atom subobject) subobject)
          ('else 
           (get-chunk-or-letter-for subobject)))))                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
; get-chunk-or-letter-for calls get-chunk-for which in turn calls distinguish 
; which in turn calls test-value extractor. 
;This is how EPAM's distintuish recurses when it encounters subobjects.

(defun test-value-finder (object test &optional not-context)
  "This routine finds the subobject that is the result of a test upon an object.
If not-context is true, then the routine will not use external factors such as
the value of *activation* to find the value of a test for context."
  (cond ((null test) nil)
        ((and (numberp test) (> test 0))
         (loop for n from 1
               for subobject in (cdr object)
               if (eql subobject '~)
               return nil
               else if (= test n)
               return subobject))
        ((numberp test)
         (loop for n = -1 then (1- n)
               for subobject in (reverse (cdr object))
               if (eql subobject '~)
               return nil
               else if (= test n)
               return subobject))
        ((and (member test *context-attributes*) (null not-context))
         (find-context-on object test))
        ((eql test 'length) (find-length-on object))
        ((eql test 'after)
         (find-item-after *anchor-chunk* (cdr object)))
        ((eql test 'before)
         (find-item-before *anchor-chunk* (cdr object)))
        ((eql test 'first)
         (first (cdr object)))
        ((eql test 'last)
         (car (last (cdr object))))
        ((listp test)
         (find-value-for-compound-test object test))
        ('else (get-value object test))))

(defun find-length-on (object)
  "Normally the length returned is the number of subobjects.  If there 
are no subobjects or there is a squiggle at the beginning or middle 
the value is nil.  If the number of subobjects is indeterminate because 
of a squiggle at the end, the value is #\_"
  (cond ((not (cdr object)) nil)
        ((member '~ object)
         (when (eql (car (last object)) '~) #\_))
        ('else (1- (length object)))))

(defun find-context-on (object context-attribute)
  "If there is no value for the context-attribute on the object then get
the current value of *activation* and use it."
  (or (get-value object context-attribute)
      *activation*))

(defun put-context-on (object value context-attribute)
  "The variable *activation* holds the number of the current day.  It is used
as a substitute for the current context."
  (cond (value (put-value object value context-attribute))
        ('else (put-value object *activation* context-attribute))))

(defun match (chunk object)
  "This routine finds if the subobjects  of an
object match those of the image of the given chunk."
 (let ((simplified-object (specify object))
       image)
   (cond
    ((atom simplified-object) (eql chunk object))
    ((numberp chunk) nil)
    ((characterp chunk) nil)
    ((null (setf image (get-print-copy-of (get chunk 'image)))) nil)
    ((good-subobject-match? simplified-object image) t)
    ('else nil))))

(defun does-object-match-chunk? (object chunk)
  (cond ((atom object) (eql object chunk))
        ('else 
         (let ((image (get-print-copy-of (get chunk 'image))))
           (not (difference-finder object 
                                   image
                                   (create-noticing-order-from (cons nil 
                                                                     (cdr image)))))))))

(defun does-chunk-match-object? (object chunk)
  "This match is used to determine whether an item in the auditory imagery store has 
sorted to a chunk that has sufficient info to match every blank that is on the object
plus every material item."
  (does-image-match-object? (convert-chunk-to-object chunk) object))

(defun does-image-match-object? (image object)
  (cond ((eql image #\_) nil)
        ((eql object #\_) (characterp image))
        ((and (eql object '~) (null image)) t)
        ((atom object) (eql object image))
        ((atom image) nil)
        ((not (= (length object) (length image))) nil)  
        ('else (loop for subobject in (cdr object)
                     for image-tail = (cdr image) then (cdr image-tail)
                     for subimage = (car image-tail)
                     if (not (does-image-match-object? subimage subobject))
                     return nil
                     finally (return t)))))

(defun perfect-subobject-match? (obj chunk-or-image)
  (let ((image (cond ((get-image-of (specify chunk-or-image)))
                     ('else (specify chunk-or-image))))
        (object (specify obj)))
    (cond ((atom object) (eql object image))
          ((atom image) nil)
          ('else (loop for object-tail = (cdr object) then (cdr object-tail)
                       for image-tail = (cdr image) then (cdr image-tail)
                       if (and (or (null object-tail)
                                        (eql (car object-tail) '~))
                                    (or (null image-tail)
                                        (eql (car image-tail) '~)))
                       return t
                       else if (and object-tail 
                                    (not (eql (car object-tail) '~))
                                    (null image-tail))
                       return nil
                       else if (and image-tail 
                                    (not (eql (car image-tail) '~))
                                    (null object-tail)) 
                       return nil
                       else if (and (or (null object-tail)
                                        (eql (car object-tail) '~))
                                    (or (null image-tail)
                                        (eql (car image-tail) '~)))
                       return t
                       else if (not (perfect-subobject-match? (car object-tail)
                                                              (car image-tail)))
                       return nil)))))

(defun good-subobject-match? (obj chunk-or-image)
  (let ((image (cond ((get-image-of (specify chunk-or-image)))
                     ('else (specify chunk-or-image))))
        (object (specify obj)))
    (cond ((atom object) (eql object image))
          ((atom image) nil)
          ('else (loop for object-tail = (cdr object) then (cdr object-tail)
                       for image-tail = (cdr image) then (cdr image-tail)
                       if (and object-tail 
                               (not (eql (car object-tail) '~))
                               (null image-tail))
                       return nil
                       else if (and image-tail 
                                    (not (eql (car image-tail) '~))
                                    (null object-tail)) 
                       return nil
                       else if (null object-tail) 
                       return t
                       else if (eql (car image-tail) #\_) 
                       return nil
                       else if (not (perfect-subobject-match? (car object-tail)
                                                              (car image-tail)))
                       return nil)))))
                                
(defun form-net ()
  "This routine makes a discrimination net and outputs its name which is the
same as its root node.  The initial net is an empty node with a test for 'is-a."
  (when *print-epam-updates* (format t "Creating new net!~%"))
  (setf *number-of-episodes* 0)
  (gensym "node")
  (gensym 0)
  (let ((root-node (form-node))
        (branching-node (gensym))
        (pseudochunk-branching-node (gensym)))
    (setf *top-pseudochunk-node* (gensym)
          (get root-node 'image) (list nil)
          (get root-node 'tests) (list 'is-a)
          (get root-node 'is-a) branching-node
          (get branching-node 'parent) root-node
          (get *top-pseudochunk-node* 'parent) branching-node
          (get branching-node 'pseudochunk) *top-pseudochunk-node*
          (get *top-pseudochunk-node* 'image) '(((is-a pseudochunk)) pseudochunk)
          (get *top-pseudochunk-node* 'tests) (list 1)
          (get *top-pseudochunk-node* 1) pseudochunk-branching-node
          (get pseudochunk-branching-node 'parent) 
          *top-pseudochunk-node*)
    (setf *net* root-node)))

(defun form-node ()
  "This routine does a gensym whenever a new node is created in the net."
  (incf *nodes-in-net*)
  (gensym "node"))
; from epamisc2.lsp
(defun print-tree (node level everything pseudochunks)
  "Changed on November 1, 1998, so that it would print the images of episodes." 
  (tab-over level)
  (format t "~a:" node)
  (setf *node-list* (cons node *node-list*))
  (print-higher-node-clearly (symbol-plist node) 
                             (get node 'tests) 
                             everything
                             level)
  (loop for test in (get node 'tests)
        for lower-node = (get node test)
        do (tab-over level)
        (format t "  Test=~a:" test)
        (print-lower-node-clearly (symbol-plist lower-node) 
                                  everything
                                  pseudochunks))
  (loop for test in (get node 'tests)
        for lower-node = (get node test)
        do
        (loop for remainder on (symbol-plist lower-node) by #'cddr
              for branch-label = (first remainder)
              for next-node = (second remainder)
              while remainder
              if (member branch-label *dont-print-list*) do 'nil
              else if (and (not pseudochunks)
                           (eql branch-label 'pseudochunk))
              do 'nil
              else do (print-tree next-node 
                                  (1+ level) 
                                  everything
                                  pseudochunks))))

(defun print-net (&optional net everything pseudochunks)
  "This routine prints an EPAM net in a somewhat readable form. If the variable
everything is specified then it prints the activations, otherwise it deletes
'parent, 'node-activation and 'activation.  If the variable pseudochunks is true
it also prints the pseudochunk subnet, otherwise it leaves that net out. 
 Each node is specified by several lines.  The first line begins
Node x: and the other lines begin test x:."
  (when (not net) (setf net *net*))
  (format t "~%EPAM NET:~%")
  (setf *node-list* nil)
  (setf *episode-list* nil)
  (print-tree net 0 everything pseudochunks)
  (format t "EPISODES:~%")
  (loop for episode in *episode-list*
        for image = (get episode 'image)
        for is-a = (specify (get-value image 'is-a))
        if (member episode *node-list*)
        do 'nil
        else if (or everything
                    (eql is-a 'episode)
                    (eql is-a #\_))
        do (format t "~a = ~a~%" episode (get-print-copy-of (get episode 'image))))
  (terpri))

(defun print-lower-node-clearly (plist everything pseudochunks)
  "This routine leaves out parent and activation in order to just include
branches and branch labels pairs."
  (loop for list-tail on plist by #'cddr
        for n from 1
        for attribute = (first list-tail)
        for value = (second list-tail)
        with image
        while list-tail
        if (and (not everything) 
                (member attribute *dont-print-list*))
        do 'nil
        else if (and (not pseudochunks)
                     (eql attribute 'pseudochunk))
        do 'nil
        else if t 
        do
        (when (not (= n 1)) (princ " "))
        (princ attribute)
        (when (setf image (get-image-of attribute))
          (format t "[~a]" image))
        (format t "-->~a" value)
        finally (terpri)))

(defun print-higher-node-clearly (plist tests everything level)
  "This routine shortens attributes so that they fit on the line it also
puts dashes between attribute-value pairs.  Unless everything is specified
the routine also leaves out parent branches."
  (loop for list-tail on plist by #'cddr
        for n from 1
        for attribute = (first list-tail)
        for value = (second list-tail)
        while list-tail
        if (member attribute tests :test #'equal) do 'nil
        else if (and (not everything) 
                     (member attribute *dont-print-list*))
        do 'nil
        else if (and everything (eql attribute 'entries))
        do (print-episodes value level)
        else if t 
        do
        (when (not (= n 1)) (princ " "))
        (princ (cond ((eql attribute 'activation) 'act)
                     ((eql attribute 'visual) 'vis)
                     ((eql attribute 'auditory) 'aud)
                     ((eql attribute 'visual-auditory) 'vis_aud)
                     ((eql attribute 'auditory-visual) 'aud_vis)
                     ('else attribute)))
        (princ "=")
        (princ value)
        finally (terpri)))

(defun print-episodes (episodes level)
  (loop initially (format t " ENTRIES:~%")
    for pair in episodes
    do (loop for episode in (second pair)
             do 
             (tab-over level)
             (format t " ~a [~a]~%" episode (get-image-of episode))
             if (not (member episode *episode-list*))
             do (setf *episode-list* (cons episode *episode-list*)))
    finally (tab-over level)))


(defun study-multiple-associations (attribute stimulus-object correct-response-objects
                                           &optional position1 position2)
   "This routine studies multiple associates as in a trial of a fan effect experiment where
the subject is given a stimulus and asked to elicit a list of associations to the stimulus 
and is then told which of those associations is correct.  Note: *sal-b* is only used 
in Hintzman Experiment 3 -- the simulation of probability learning.  In all other 
simulations it is set at 100. "
    (cond ((and (> *epam-clock* *clock*) (< (random 100) *sal-b*))
           (when *print-strategy-updates*
             (format t "Not studying because EPAM is occupied or due to *sal-b*~%")
             (format t "EPAM will be occupied until ~a, current time is ~a~%" *epam-clock* *clock*)))
          ((and (< (random 100) *sal-a*)
                (is-there-a-mistaken-response-here? 
                 attribute 
                 (get-chunk-for stimulus-object) 
                 position2
                 correct-response-objects))
           (when *print-strategy-updates*
             (format t "Studying stimulus then associations because of *SAL-A* and mistaken episodes at chunk.~%"))
           (setf *epam-clock* *clock*)
           (study stimulus-object)
           (loop for correct-response-object in correct-response-objects
             do (associate attribute stimulus-object correct-response-object 
                  position1 position2))
           (when *print-net-updates* (print-net)))
          ('else
           (when *print-strategy-updates*
             (format t "Studying association because random number less than *SAL-B*.~%"))
           (setf *epam-clock* *clock*)
           (loop for correct-response-object in correct-response-objects
             do (associate attribute stimulus-object correct-response-object 
                  position1 position2))
           (when *print-net-updates* (print-net)))
          ))

(defun is-there-a-mistaken-response-here? (attribute stimulus-chunk position correct-responses)
   (let ((responses (mapcar 
                      'specify 
                      (find-associates attribute stimulus-chunk position))))
      (loop 
        for response in responses
        if (loop 
             for correct-response in correct-responses
             if (not (difference-finder correct-response response))
             return nil
             finally (return t))
        return t)))



; These routines are used to find and study paired associations
(defun study-paired-association (attribute stimulus-object correct-response-object
                                           &optional position1 position2 rsf correct?)
   (cond (correct? 
           (when (and (< (random 100) *SAL-C*) 
                      (<= *epam-clock* *clock*))
              (study-when-overlearning attribute stimulus-object correct-response-object
                position1 position2 rsf)))
         ((> *epam-clock* *clock*) 
          (when *print-strategy-updates*
             (format t "Not studying because EPAM is occupied!~%")
             (format t "EPAM will be occupied until ~a, current time is ~a~%" 
               *epam-clock* *clock*))
          t)
         ((and (< (random 100) *sal-a*)
               (are-there-multiple-episodes-here? attribute (get-chunk-for stimulus-object)))
          (when *print-strategy-updates*
             (format t "Studying stimulus then association because of *SAL-A* and multiple episodes at chunk.~%"))
          (setf *epam-clock* *clock*)
          (study stimulus-object)
          (associate attribute stimulus-object correct-response-object position1 position2 rsf)
          t)
         ((< (random 100) *sal-b*)
          (when *print-strategy-updates*
             (format t "Studying association because random number less than *SAL-B*.~%"))
          (setf *epam-clock* *clock*)
          (associate attribute stimulus-object correct-response-object position1 
            position2 rsf))
         ('else 
          (when *print-strategy-updates*
             (format t "Not studying because random numbers less than *SAL-A* & *SAL-B*.~%")
             ))))


(defun find-response-given-stimulus (stimulus-object attribute)
  "This routine finds the response to a stimulus in a paired associate simulation
if instant is true, the search for a response is quick and there is no search 
for a response.  Instant is true when this routine is used as a test to see 
if association has worked through to completion."
    (or (find-associate attribute stimulus-object 2)
        (when *guessing-routine*
          (time-tally-to-guess-a-response)
          (apply *guessing-routine* nil))))

;;              These Routines Conduct depth-first-searches in Net

(defun depth-first-search-in-net (node object exit &optional top)
  "This is the routine for continuing a search that has already begun.  If given
the *net* as the initial node, it begins a test at the top.  Otherwise it starts
at the node given and works its way up and down from there.  
This routine will exit one of three ways:
   a. With a node that satisfies the exit test.
   b. nil -- if no node was found
   c 'blank -- if an interrupt stopped the search.
The following changes have been made from EPAM IV searches:
1. Required changes because of the new structure of a net having branching nodes
2. Additional aspect that if the tested subobject of a partial image is #\_
   then the system searches all of the branches in the order specified on the 
   *search-list*.  It also updates the *search-list* when it doesn't contain 
   one or more of the values of the branches at the node."
  (let (result)
    (when *print-epam-updates*
      (format t "Searching net with object: ~a~%" object)
      (format t "Searching ~a" node))
    (setf result
          (cond ((apply *search-interrupt* nil) 'blank)
                ((eql node *net*) 
                 (downward-search-in-net node object exit (get node 'tests)))
                ('else (search-up-then-down-net node object exit top))))
    (when *print-epam-updates*
      (terpri))
    result))
      

(defun downward-search-in-net (node object exit tests)
 "This routine searches the subnet below the given non-branching node.  
  It follows this algorithm:
    1. If there is a recognizable branch at this node, exit with the result 
of that branch without trying the exit test on this node.  
    2. If there is not, then try all possibiities and exit with one if it works.
    3. If none of them work, then try to apply the exit test to this node 
       and exit with this node if it applies."
 (loop for test in tests
       for branching-node = (get node test)
       for test-value = (specify (test-value-finder object test))
       with result
       if (apply *search-interrupt* nil) 
       return 'blank
       else if (null test-value)
       ; If there is no value for this test continue with next test
       do 'nil
       else if (and (atom test-value)
                    (not (eql test-value #\_))
                    (get branching-node test-value))
       ; If there is a value and branch for this test simply continue down the net
       ; without checking to see if this node is the correct node.
       return (progn (when *print-epam-updates*
                       (format t " Following test = ~a & value = ~a from ~a downto ~a.~%" 
                               test test-value node (get branching-node test-value)))
                     (search-down-in-net (get branching-node test-value)
                                         object exit))
       else if (and (eql test-value #\_)
                    (or (when *print-epam-updates*
                          (format t "Test-value of ~a is _ so trying all branches.~%"
                                  test))
                        t)
                    (setf result (search-branches 
                                  (find-possible-values-at-branching-node branching-node)
                                  branching-node object exit)))
       ; Here the value of the test is #\_, so try all of the possible branches
       ; in the subjective order.  If a node is found exit, otherwise continue
       ; either with next test or by testing this node and returning nil if this
       ; node doesn't meet the criteria.
       return result
       else if (and (listp test-value)
                    (setf result (search-recursively-for-test-value test-value 
                                                                    test 
                                                                    branching-node 
                                                                    object 
                                                                    exit)))
       ; Here the value of the test is itself an epam object, epam uses this test
       ; node as a test during a recursive sort for a value in the net that sorts
       ; at this node.  If one is found it is applied and sorting continues
       return result
       finally (when (apply exit (list node))
                  (return node))))
 
(defun search-recursively-for-test-value (test-value parent-test branching-node object exit)
  "This search is used when the object tested is itself a list and therefore 
needs to be sorted in the net.  It searches until a branch is found that has a value
at the higher node."
  (let ((value (get-chunk-or-letter-for test-value))
        found-value)
    (cond ((get branching-node value)
           (when *print-epam-updates*
             (format t " Following test = ~a & value = ~a from node = ~a~%" 
               parent-test value (get branching-node 'parent)))
           (search-down-in-net (get branching-node test-value)
                               object exit))
          ('else
           (when *print-epam-updates*
             (format t "Searching recursively for chunk for subobject = ~a~%" test-value))
           (setf found-value 
                 (downward-search-in-net *net* 
                                         test-value 
                                         #'(lambda (x) 
                                             (and (does-object-match-chunk? test-value x)
                                                  (get branching-node x)))
                                         (get *net* 'tests)
                                         ))
           (cond ((null found-value) nil)
                 ((eql found-value 'blank) 'blank)
                 ('else 
                  (when *print-epam-updates*
                    (format t " Following test = ~a & value = ~a from node = ~a~%" 
                      parent-test found-value (get branching-node 'parent)))
                  (search-down-in-net (get branching-node found-value)
                                      object exit)))))))

      
(defun search-branches (values branching-node object exit)
  (loop for value in values
        for item = (specify value)
        for last-descendent = nil then descendent
        for descendent = (get branching-node item)
        for success = (when (not (eql descendent last-descendent))
                        (when *print-epam-updates*
                          (format t " Following value = ~a " item))
                        (search-down-in-net descendent 
                                            object 
                                            exit 
                                            ))
        if (apply *search-interrupt* nil) return 'blank
        if success return success))

(defun find-possible-values-at-branching-node (branching-node)
  (let* ((branches (collect-values-at-branching-node branching-node))
         (alist (assoc branching-node *search-list*))
         (current-branches (second alist)))
    (cond ((not alist)
           (setf *search-list* (cons (list branching-node
                                           (reorder-list-randomly branches))
                                     *search-list*)))
          ('else 
           (loop for branch in branches
                 if (not (member branch current-branches))
                 return (rplaca (cdr alist) (reorder-list-randomly branches)))))
    (second (assoc branching-node *search-list*))))
    
        
(defun collect-values-at-branching-node (branching-node)
  (loop for remainder on (symbol-plist branching-node) by #'cddr
        for branch-label = (first remainder)
        while remainder
        if (member branch-label *dont-print-list*) do 'nil
        else if (eql branch-label 'pseudochunk) do 'nil
        else if t
        collect branch-label))

(defun search-down-in-net (subnet object exit)
  "This is basically downward-search-in-net.  The only differences are:
1. This routine exits nil after activating a branch if the branch to a subnet
is not activated.  In which case, no time is charged for reaching this subnet.
2. This routine exits with the subnet itself if that subnet passes the exit test.
3. This routine tallies the time required for subnet traversal in order to reach
this subnet."
  (cond ((null (eql (get subnet 'activation) *activation*)) 
         (activate-link-to subnet)
         nil)
        ('else 
         (time-tally-for-node-traversal)
         (activate-link-to subnet)
         (cond ((downward-search-in-net subnet object exit (get subnet 'tests)))
               ('else 
                (time-tally-for-node-traversal)
                (when *print-epam-updates* 
                  (format t " from ~a upto ~a~%" 
                    subnet (get (get subnet 'parent) 'parent)))
                nil)))))

(defun search-up-then-down-net (node object exit &optional top)
  "This routine searches up one node and then searches down the siblings.
     1.  Climb to parent, nil if no parent node because at root node of net.
     2.  Recurse if this node was an only child because of single value on
         object.
     3.  If there was a slot as value on partial image search down the
         remaining sibling nodes to find a node meeting the exit test.
     4.  If no success with siblings recurse."
  (let* ((parent-node (get node 'parent))
         (grandparent-node (get parent-node 'parent))
         (last-branch (find-attribute-given-value parent-node node))
         (last-test (find-attribute-given-value grandparent-node parent-node))
         test-values)
    (when (and grandparent-node 
               (not (eql node top)))
      (time-tally-for-node-traversal)
      (when *print-epam-updates* 
        (format t " from ~a upto ~a" node grandparent-node))
      (setf test-values (test-value-finder object last-test))
      (cond 
       ((apply *search-interrupt* nil) 'blank)
       ((or (listp test-values)
            (eql test-values last-branch))
        (search-up-then-down-net grandparent-node object 
                                 exit top))
       ; The remainder occurs when coming up after searching a blank value
       ; search branches checks other branches for this test
       ((search-branches (cdr (member last-branch 
                                           (second (assoc parent-node *search-list*))))
                              parent-node object exit))
       ; downward-search-in-net here checks other tests at node and then 
       ; checks the grandparent-node to see if it meets the criteria.
       ((downward-search-in-net grandparent-node 
                                object 
                                exit 
                                (cdr (member last-test 
                                             (get grandparent-node 'tests) 
                                             :test #'equal))))
       ; Otherwise keep going up.
       ('else (search-up-then-down-net grandparent-node object 
                                       exit top))))))
