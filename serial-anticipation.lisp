; serant.lsp.  This file runs the serial anticipation simulations as part of EPAM VI
; Feb. 24, 2002, version.  Copyright 2002 by Howard B. Richman

(defvar *proportion-rostorff* .86)

(defun serial-anticipation-experimenter ()
  "This experimenter presents a succession of trials until the subject achieves
one perfect trial.  Each trial consists of a 'beginning control signal
followed by a succession of syllables presented
at a constant rate followed by an 'end-of-list control signal."
 (let ((time-elapsed (- *clock* (* *position* *presentation-rate*))))
    (cond
     ((< time-elapsed 0))
     ((= *position* 0)
      (put-control-symbol-in-sensory-store 'beginning 'visual)
      (incf *position*))
     ((= *position* (length *sr-list*))
      (put-control-symbol-in-sensory-store 'end-of-list 'visual)
      (put-next-syllable-in-sensory-store 'visual time-elapsed)
      (incf *position*)
      (test-correctness-of-serial-anticipation-output (1- *position*)))
     ((< *position* (length *sr-list*))
      (put-next-syllable-in-sensory-store 'visual time-elapsed)
      (incf *position*)
      (test-correctness-of-serial-anticipation-output (1- *position*)))
     ((< time-elapsed *inter-trial-interval*))
     ((= *errors* *starting-errors*)
      (put-control-symbol-in-sensory-store 'signal-to-respond 'visual))
     ('else
      (setf *trial* (1+ *trial*))
      (when *print-experimenter-updates* 
         (format t "               *** BEGINNING TRIAL #~a ***~%" (1+ *trial*)))
      (zero-out-except-epam-clocks)
      (setf *starting-errors* *errors*
            *position* 0)))))

(defvar *group-2-presented* nil)

(defun serial-anticipation-rostorff-experimenter ()
  "This experimenter presents a succession of trials until the subject achieves
one perfect trial.  Each trial consists of a 'beginning control signal
followed by 7 syllables presented
at a constant rate followed by a 'group-2 control symbol
then the remaining syllables then an 'end-of-list control signal."
 (let ((time-elapsed (- *clock* (* *position* *presentation-rate*))))
    (cond
     ((< time-elapsed 0))
     ((= *position* 0)
      (put-control-symbol-in-sensory-store 'beginning 'visual)
      (incf *position*)
      (setf *group-2-presented* nil))
     ((and (= *position* 8)
           (not *group-2-presented*))
      (put-control-symbol-in-sensory-store 'group-2 'visual)
      (setf *group-2-presented* t))
     ((= *position* (length *sr-list*))
      (put-control-symbol-in-sensory-store 'end-of-list 'visual)
      (put-next-syllable-in-sensory-store 'visual time-elapsed)
      (incf *position*)
      (test-correctness-of-serial-anticipation-output (1- *position*)))
     ((< *position* (length *sr-list*))
      (put-next-syllable-in-sensory-store 'visual time-elapsed)
      (incf *position*)
      (test-correctness-of-serial-anticipation-output (1- *position*)))
     ((< time-elapsed *inter-trial-interval*))
     ((= *errors* *starting-errors*)
      (put-control-symbol-in-sensory-store 'signal-to-respond 'visual))
     ('else
      (setf *trial* (1+ *trial*))
      (when *print-experimenter-updates* 
         (format t "               *** BEGINNING TRIAL #~a ***~%" (1+ *trial*)))
      (zero-out-except-epam-clocks)
      (setf *starting-errors* *errors*
            *position* 0)))))


(defun create-retrieval-structure-with-sublists (n)
   (let* ((parent (gensym))
          (descendents (loop repeat n
                         for node = (gensym)
                         do (setf (get node 'image) (list nil))
                         (setf (get node 'ancestor) parent)
                         collect node)))
      (setf (get parent 'image) (list nil))
      (setf (get parent 'descendents) descendents)
      (setf *rsf* parent)))

      
   


(defun serial-anticipation-strategy ()
  "This routine is a subject's strategy in a serial-anticipation experiment 
where 'beginning is presented as a control symbol and should be anticipated
with the first item in the group.  The system simply rehearses either from the 
beginning or from a remembered anchor point."
   (zero-out-variables)
   (loop for cue = 'beginning then chunk
     with anchor = 'beginning
     with chunk
     do (when *print-strategy-updates* 
           (format t "Serial-anticipation cycle with Cue = chunk-for~a, Anchor = ~a~%" 
             (convert-chunk-to-object cue)
             (convert-chunk-to-object anchor)))
     if (not (does-this-group-exist? 'control-symbol 'visual 'sensory-store))
     do (wait-for-item)
     if (memory-store-search (get 'visual 'sensory-store) 
          'signal-to-respond)
     return t
     if (memory-store-search (get 'visual 'sensory-store) 
          'beginning
          'nil-it-out)
     do (setf *new-stimulus-flag* nil)
     (memory-store-search (get 'visual 'sensory-store) 
       'end-of-list
       'nil-it-out) ; This nils out an 'end-of-list that might have been missed.
     (setf chunk 'beginning)
     (setf anchor 'beginning)
     (setf cue 'beginning)
     (respond-to-probe chunk 'auditory 2000 'current-segment 'serial-anticipation)
     if (memory-store-search (get 'visual 'sensory-store)
          'group-2
          'nil-it-out)
     do (wait-for-item)
     if (access-syllable 'new 'visual 'auditory cue)
     do
     (setf chunk (sort-to-node (get-value-in-imagery-store cue 'auditory)))
     (respond-to-probe chunk 'auditory 2000 'current-segment 'serial-anticipation)
     (setf anchor (cumulative-rehearsal-strategy anchor cue))
     (when *print-rsf-updates* (print-rsf))
     else if (memory-store-search (get 'visual 'sensory-store) 
               'end-of-list
               'nil-it-out)
     do (setf *new-stimulus-flag* nil)
     (push 'top-of-stack *chunks*)
     (push 'end-of-list *chunks*)
     (subvocalize-chunks-into 'auditory cue)
     (setf chunk 'end-of-list)
     (current-segment-strategy anchor cue)))


(defun serial-anticipation-rostorff-strategy ()
  "This routine is a subject's strategy in a serial-anticipation experiment 
where items in the first half of the list are marked with the 
color 'black while items in the second half of the list are marked
with the color red.  In this strategy the system starts by 
creating a retrieval structure for two half lists tied together 
with a parent node.  When the first item is presented the system goes to 
the beginning node of the retrieval structure putting that node in 
the rsf and when a control symbol is presented the system goes to the next node of the retrieval 
node of the retrieval structure.
   " 
   (let (rsf)
      (zero-out-variables)
      (create-retrieval-structure-with-sublists 2)
      (loop for cue = 'beginning then chunk
        with anchor = 'beginning
        with chunk
        with group = 'beginning
        do (when *print-strategy-updates* 
              (format t "Serial-anticipation cycle with Cue = chunk-for~a, Anchor = ~a~%" 
                (convert-chunk-to-object cue)
                (convert-chunk-to-object anchor)))
        if (not (does-this-group-exist? 'control-symbol 'visual 'sensory-store))
        do (wait-for-item)
        if (memory-store-search (get 'visual 'sensory-store) 
             'signal-to-respond) ; this indicates end of experiment
        return t
        if (memory-store-search (get 'visual 'sensory-store) 
             'beginning
             'nil-it-out)
        do (setf *new-stimulus-flag* nil)
        (memory-store-search (get 'visual 'sensory-store) 
          'end-of-list
          'nil-it-out) ; This nils out an 'end-of-list that might have been missed.
        
        (setf chunk 'beginning)
        (setf anchor 'beginning)
        (setf cue 'beginning)
        (setf group 'beginning)
        (setf *rsf* (get-top-node-of-image *rsf*))
        (setf *rsf* (get-next-retrieval-structure-node))
        (respond-to-probe chunk 'auditory 2000 'current-segment 'serial-anticipation)
        
        if (memory-store-search (get 'visual 'sensory-store) 
             'group-2
             'nil-it-out)
        do (setf *new-stimulus-flag* nil)
        (when (eql group 'beginning)
           (push 'top-of-stack *chunks*)
           (push 'end-of-list *chunks*)
           (subvocalize-chunks-into 'auditory cue)
           (setf chunk 'end-of-list)
           (current-segment-strategy anchor cue) ; to associate last item with 'end-of-list
           (setf group 'group-2)
           (setf chunk 'group-2)
           (setf anchor 'group-2)
           (setf cue 'group-2)
           (setf rsf (get-next-retrieval-structure-node))
           (when rsf (setf *rsf* rsf)))
        if (access-syllable 'new 'visual 'auditory cue)
        do
        (setf chunk (sort-to-node (get-value-in-imagery-store cue 'auditory)))
        (cond ((and (eql group 'beginning)
                    chunk
                    (eql chunk (test-value-extractor (get-image-of *rsf*) -1)))
               (setf group 'group-2)
               (setf chunk 'group-2)
               (setf rsf (get-next-retrieval-structure-node))
               (when rsf (setf *rsf* rsf))
               (respond-to-probe chunk 'auditory 2000 'current-segment 'serial-anticipation)
               (setf rsf (get-previous-retrieval-structure-node))
               (when rsf (setf *rsf* rsf))
               (cumulative-rehearsal-strategy anchor cue)
               (setf anchor 'group-2)
               (setf cue 'group-2)
               (setf chunk 'group-2)
               (setf rsf (get-next-retrieval-structure-node))
               (when rsf (setf *rsf* rsf))
               )
              ('else
               (respond-to-probe chunk 'auditory 2000 'current-segment 'serial-anticipation)
               (setf anchor (cumulative-rehearsal-strategy anchor cue))
               (when *print-rsf-updates* (print-rsf))))
        else if (memory-store-search (get 'visual 'sensory-store) 
                  'end-of-list
                  'nil-it-out)
        do (setf *new-stimulus-flag* nil)
        (push 'top-of-stack *chunks*)
        (push 'end-of-list *chunks*)
        (subvocalize-chunks-into 'auditory cue)
        (setf chunk 'end-of-list)
        (current-segment-strategy anchor cue))))

   
  

(defvar *preliminary-syllables*)

(defun hovland-serial-anticipation-simulation ()
  "This routine performs a simulation of Hovland's (1938) serial anticipation
experiment from the Journal of Experimental Psychology V 6 329-339 of the
learning of nonsense syllables and the serial anticipation effect.  For the
original EPAM simulation of this effect see Feigenbaum & Simon's (1962) study
which appeared in the British Journal of Psychology, Volume 53, 307-320, which
was reprrinted as chapter 3.1 of Models of Thought Volume 1 by Simon.  
     Some of the parameters, such as *presentation-rate* and
*inter-trial-interval* can be adjusted.  Beware, however, a bug will appear
if the *stimulus-duration-rate* is almost as high or higher than the 
*presentation-rate*."
   (setf *overall-results* nil)
   (setf *syllable-lists* *hovland*)
   (setf *preliminary-syllables* *hovland-preliminaries*)
   (setf *inter-trial-interval* 2000)
   (setf *ignore-new-stimulus* nil)
   (setf *print-time-updates* nil) 
   (setf *print-imagery-stores* nil) 
   (setf *print-experimenter-updates* nil) 
   (setf *print-epam-updates* nil) 
   (setf *print-strategy-updates* nil)
   (setf *print-forgetting-updates* nil)
   (setf *guessing-routine* nil)
   (setf *concurrent-routine* nil)
   (setf *is-a-of-recoding* 'one-letter-one-sound)
   (setf *method-for-counting-syllables* 'character-method)
   (setf *indexes* '(1))
   (setf *index* 1)
   (setf *number-of-sessions* 100)
   (setf *overall-results* nil)
   (seed-random-generator)
   (loop for presentation-rate in '(2000 4000)
      do
     (setf *presentation-rate* presentation-rate
           *stimulus-duration-rate* (- presentation-rate 250))
     (hovland-serial-anticipation))
   (report-hovland-serial-anticipation-results)
   )

(defun hovland-serial-anticipation ()
   (let* ((list-length (length (car *syllable-lists*)))
          (total-presentation-time (/ (+ (* list-length *presentation-rate*) *inter-trial-interval*)
                                      1000.0)))
      (reset-data-collection-arrays)
      (setf *noticing-order* nil)
      (initialize-results-arrays)
      (setf *errors-array* (make-array list-length :initial-element 0))
      (loop repeat *number-of-sessions*
        do
        (setf (aref *response-record* *index*) nil)
        (setf (aref *actual-response-record* *index*) nil)
        (setf (aref *recognition-record* *index*) nil)
        (setf (aref *latency-record* *index*) nil)
        (initialize-variables)
        (loop repeat 2
          do (loop for item in *preliminary-syllables*
               for object = (change-string-to-object item 'auditory)
               do (study object)))
        (zero-out-variables)
        (loop for syllable-list in *syllable-lists*
          do (setf *syllable-list* syllable-list)
          (prepare-net-for-is-a-of-recoding *syllable-list*)
          (setf *sr-list* (change-syllables-to-objects *syllable-list* 'visual))
          (setf *experimenter* 'serial-anticipation-experimenter)
          (increment-activation)
          (serial-anticipation-strategy)
          (setf (aref *errors-on-list* *index*)
                (+ (aref *errors-on-list* *index*) *errors*))
          (setf *errors* 0)
          (setf (aref *trials-on-list* *index*)
                (+ (aref *trials-on-list* *index*) *trial*))
          ; (update-response-record)
          ))
      ; (print-paired-associate-results)
      (loop initially (format t "Average-errors:")
        for n from 0 upto (1- (length (car *syllable-lists*)))
        for errors = (aref *errors-array* n)
        for average-errors = (1- (/ errors (* *number-of-sessions* 
                                              (length *syllable-lists*) 
                                              1.0)))
        with total-errors = 0
        do (setf total-errors (+ total-errors errors))
        (format t " ~a" average-errors) 
        (setf *overall-results*
              (append *overall-results* (list average-errors))))
      (format t "Average Number of learning trials = ~a~%"
        (/ (aref *trials-on-list* *index*) (* 1.0 
                                              *number-of-sessions* 
                                              (length *syllable-lists*))))
      (format t "Total Learning Time per Item = ~a seconds~%"
        (/   (* (/ (aref *trials-on-list* *index*) (* *number-of-sessions* 
                                                      (length *syllable-lists*)))
                total-presentation-time)
           (length (car *syllable-lists*))))))
   


(defun report-hovland-serial-anticipation-results ()
   (let ((total2 (* .01 (loop for n from 0 upto 11
                          sum (nth n *overall-results*))))
         (total4 (* .01 (loop for n from 12 upto 23
                          sum (nth n *overall-results*)))))
      (format t "~%Mean Number of Failures to Respond Correctly to the Different Syllable~%")
      (format t "Positions During Learning to Mastery by Massed Practice with Two-Second~%")
      (format t "and Four-Second Rates of Presentation.  Data for people is from Hovland (1938)~%")
      (format t "  Position       People           EPAM6~%")
      (format t "of Syllable   -------------    ------------~%")
      (format t "               2-Sec  4-Sec    2-Sec  4-Sec~%")
      (format t "              -------------    ------------~%")
      (format t "     1         1.05   0.31    ~5,2f  ~5,2f~%" 
        (nth 0 *overall-results*) (nth 12 *overall-results*))
      (format t "     2         1.77   0.65    ~5,2f  ~5,2f~%"
        (nth 1 *overall-results*) (nth 13 *overall-results*))
      (format t "     3         3.86   1.28    ~5,2f  ~5,2f~%"
        (nth 2 *overall-results*) (nth 14 *overall-results*))
      (format t "     4         6.15   1.78    ~5,2f  ~5,2f~%"
        (nth 3 *overall-results*) (nth 15 *overall-results*))
      (format t "     5         7.36   2.59    ~5,2f  ~5,2f~%"
        (nth 4 *overall-results*) (nth 16 *overall-results*))
      (format t "     6         8.40   3.34    ~5,2f  ~5,2f~%"
        (nth 5 *overall-results*) (nth 17 *overall-results*))
      (format t "     7        10.45   3.94    ~5,2f  ~5,2f~%"
        (nth 6 *overall-results*) (nth 18 *overall-results*))
      (format t "     8         9.41   3.47    ~5,2f  ~5,2f~%"
        (nth 7 *overall-results*) (nth 19 *overall-results*))
      (format t "     9         8.60   3.25    ~5,2f  ~5,2f~%"
        (nth 8 *overall-results*) (nth 20 *overall-results*))
      (format t "    10         6.45   3.15    ~5,2f  ~5,2f~%"
        (nth 9 *overall-results*) (nth 21 *overall-results*))
      (format t "    11         5.41   2.56    ~5,2f  ~5,2f~%"
        (nth 10 *overall-results*) (nth 22 *overall-results*))
      (format t "    12         3.61   2.28    ~5,2f  ~5,2f~%"
        (nth 11 *overall-results*) (nth 23 *overall-results*))
      (format t "~%Percentage of Total Errors Made during Acquisition at each~%")
      (format t "Syllable Postition of a 12-item Serial List of Nonsense~%")
      (format t "Syllables Predited and Observed.~%")
      (format t "----------------------------------------~%")
      (format t "                               EPAM6~%")
      (format t "Syllable  McCrary           ------------ ~%")
      (format t "Position  &Hunter   EPAM1   2-Sec  4-sec ~%")
      (format t "----------------------------------------~%")
      (format t "    1       1.5      1.3   ~5,1f  ~5,1f~%" 
        (/ (nth 0 *overall-results*) total2) (/ (nth 12 *overall-results*) total4))
      (format t "    2       2.3      2.6   ~5,1f  ~5,1f~%"
        (/ (nth 1 *overall-results*) total2) (/ (nth 13 *overall-results*) total4))
      (format t "    3       4.5      6.3   ~5,1f  ~5,1f~%"   
        (/ (nth 2 *overall-results*) total2) (/ (nth 14 *overall-results*) total4))
      (format t "    4       7.0      7.5   ~5,1f  ~5,1f~%"
        (/ (nth 3 *overall-results*) total2) (/ (nth 15 *overall-results*) total4))
      (format t "    5      10.3     10.5   ~5,1f  ~5,1f~%"
        (/ (nth 4 *overall-results*) total2) (/ (nth 16 *overall-results*) total4))
      (format t "    6      11.6     11.3   ~5,1f  ~5,1f~%"
        (/ (nth 5 *overall-results*) total2) (/ (nth 17 *overall-results*) total4))
      (format t "    7      14.0     12.5   ~5,1f  ~5,1f~%"
        (/ (nth 6 *overall-results*) total2) (/ (nth 18 *overall-results*) total4))
      (format t "    8      12.4     12.5   ~5,1f  ~5,1f~%"
        (/ (nth 7 *overall-results*) total2) (/ (nth 19 *overall-results*) total4))
      (format t "    9      11.0     11.3   ~5,1f  ~5,1f~%"
        (/ (nth 8 *overall-results*) total2) (/ (nth 20 *overall-results*) total4))
      (format t "   10      10.0     10.5   ~5,1f  ~5,1f~%"
        (/ (nth 9 *overall-results*) total2) (/ (nth 21 *overall-results*) total4))
      (format t "   11       8.5      7.5   ~5,1f  ~5,1f~%"
        (/ (nth 10 *overall-results*) total2) (/ (nth 22 *overall-results*) total4))
      (format t "   12       7.0      6.3   ~5,1f  ~5,1f~%"
        (/ (nth 11 *overall-results*) total2) (/ (nth 23 *overall-results*) total4))
      ))

(defun wishner-serial-anticipation-control-group-simulation ()
     "This routine performs a simulation of Wishner, Shipley, and 
Hurvich's (1958) serial anticipation experiment from the American
Journal of Psychology V 70 358-262.  Using this experiment subjects
studied the list as a whole just as in the Hovland experiment.
The lists used are not the same as were used by Wishner et. al."
   (setf *overall-results* nil)
   (setf *syllable-lists* *fourteen-syllable-lists*)
   (setf *preliminary-syllables* *hovland-preliminaries*)
   (setf *inter-trial-interval* 2000)
   (setf *ignore-new-stimulus* nil)
   (setf *print-time-updates* nil) 
   (setf *print-imagery-stores* nil) 
   (setf *print-experimenter-updates* nil) 
   (setf *print-epam-updates* nil) 
   (setf *print-strategy-updates* nil)
   (setf *print-forgetting-updates* nil)
   (setf *guessing-routine* nil)
   (setf *concurrent-routine* nil)
   (setf *is-a-of-recoding* 'one-letter-one-sound)
   (setf *method-for-counting-syllables* 'character-method)
   (setf *indexes* '(1))
   (setf *index* 1)
   (setf *number-of-sessions* 100)
   (setf *presentation-rate* 4000)
   (setf *stimulus-duration-rate* (- *presentation-rate* 250))
   (hovland-serial-anticipation)
   (report-wishner-control-condition-results)
   )

(defun report-wishner-rostorff-results ()
   "The parameter *proportion-rostorff* determines the mix of divided list vs.
whole-list strategies.  If the proportion is .67 then about 2/3 will be the 
divided list strategy.  Based upon Feigenbaum & Simon (1962) the proportion
should be set so that 6% of the errors will occur in the 9th serial position." 
   (let* ((remainder (- 1 *proportion-rostorff*))
          (total2 (+ (* remainder (* .01 (loop for n from 0 upto 13
                                           sum (nth n *overall-results*))))
                     (* *proportion-rostorff* (* .01 (loop for n from 14 upto 27
                                                       sum (nth n *overall-results*))))
                     )))
      (format t "~%Mean Number of Failures to Respond Correctly to the Different Syllable~%")
      (format t "Positions During Learning to Mastery by Massed Practice with Four-Second~%")
      (format t "Rate of Presentation.  Data for people is from Wishner et. al. (1957)~%")
      (format t "  Position       People           EPAM6~%")
      (format t "of Syllable   -------------    ------------~%")
      (format t "     1             2.9           ~5,1f~%" 
        (+ (* remainder (nth 0 *overall-results*))
           (* *proportion-rostorff* (nth 14 *overall-results*))))
      (format t "     2             5.8           ~5,1f~%" 
        (+ (* remainder (nth 1 *overall-results*))
           (* *proportion-rostorff* (nth 15 *overall-results*))))
      (format t "     3             8.8           ~5,1f~%" 
        (+ (* remainder (nth 2 *overall-results*))
           (* *proportion-rostorff* (nth 16 *overall-results*))))
      (format t "     4            10.1           ~5,1f~%" 
        (+ (* remainder (nth 3 *overall-results*))
           (* *proportion-rostorff* (nth 17 *overall-results*))))
      (format t "     5            11.3           ~5,1f~%" 
        (+ (* remainder (nth 4 *overall-results*))
           (* *proportion-rostorff* (nth 18 *overall-results*))))
      (format t "     6            11.6           ~5,1f~%" 
        (+ (* remainder (nth 5 *overall-results*))
           (* *proportion-rostorff* (nth 19 *overall-results*))))
      (format t "     7            10.9           ~5,1f~%" 
        (+ (* remainder (nth 6 *overall-results*))
           (* *proportion-rostorff* (nth 20 *overall-results*))))
      (format t "     8             8.2           ~5,1f~%" 
        (+ (* remainder (nth 7 *overall-results*))
           (* *proportion-rostorff* (nth 21 *overall-results*))))
      (format t "     9             8.2           ~5,1f~%" 
        (+ (* remainder (nth 8 *overall-results*))
           (* *proportion-rostorff* (nth 22 *overall-results*))))
      (format t "    10            12.1           ~5,1f~%" 
        (+ (* remainder (nth 9 *overall-results*))
           (* *proportion-rostorff* (nth 23 *overall-results*))))
      (format t "    11            12.8           ~5,1f~%" 
        (+ (* remainder (nth 10 *overall-results*))
           (* *proportion-rostorff* (nth 24 *overall-results*))))
      (format t "    12            13.4           ~5,1f~%" 
        (+ (* remainder (nth 11 *overall-results*))
           (* *proportion-rostorff* (nth 25 *overall-results*))))
      (format t "    13            11.4           ~5,1f~%" 
        (+ (* remainder (nth 12 *overall-results*))
           (* *proportion-rostorff* (nth 26 *overall-results*))))
      (format t "    14             8.4           ~5,1f~%" 
        (+ (* remainder (nth 13 *overall-results*))
           (* *proportion-rostorff* (nth 27 *overall-results*))))
      (format t "~%Percentage of Total Errors Made during Acquisition at each~%")
      (format t "Syllable Postition of a 14-item Serial List of Nonsense~%")
      (format t "Syllables Predited and Observed.~%")
      (format t "---------------------------------~%")
      (format t "Syllable             ~%")
      (format t "Position  Wishner   EPAM1   EPAM6~%")
      (format t "---------------------------------~%")
      (format t "    1       2.2      2.9   ~5,1f  ~%" 
        (/ (+ (* remainder (nth 0 *overall-results*))
              (* *proportion-rostorff* (nth 14 *overall-results*)))
           total2))
      (format t "    2       4.3      3.1   ~5,1f  ~%"
        (/ (+ (* remainder (nth 1 *overall-results*))
              (* *proportion-rostorff* (nth 15 *overall-results*)))
           total2))
      (format t "    3       6.5      4.7   ~5,1f  ~%"   
        (/ (+ (* remainder (nth 2 *overall-results*))
              (* *proportion-rostorff* (nth 16 *overall-results*)))
           total2))
      (format t "    4       7.3      6.7   ~5,1f  ~%"
        (/ (+ (* remainder (nth 3 *overall-results*))
              (* *proportion-rostorff* (nth 17 *overall-results*)))
           total2))
      (format t "    5       8.2      9.1   ~5,1f  ~%"
        (/ (+ (* remainder (nth 4 *overall-results*))
              (* *proportion-rostorff* (nth 18 *overall-results*)))
           total2))
      (format t "    6       8.5     10.1   ~5,1f  ~%"
        (/ (+ (* remainder (nth 5 *overall-results*))
              (* *proportion-rostorff* (nth 19 *overall-results*)))
           total2))
      (format t "    7       8.1      8.9   ~5,1f  ~%"
        (/ (+ (* remainder (nth 6 *overall-results*))
              (* *proportion-rostorff* (nth 20 *overall-results*)))
           total2))
      (format t "    8       6.0      5.9   ~5,1f  ~%"
        (/ (+ (* remainder (nth 7 *overall-results*))
              (* *proportion-rostorff* (nth 21 *overall-results*)))
           total2))
      (format t "    9       6.0      6.0   ~5,1f  ~%"
        (/ (+ (* remainder (nth 8 *overall-results*))
              (* *proportion-rostorff* (nth 22 *overall-results*)))
           total2))
      (format t "   10       9.0      8.3   ~5,1f  ~%"
        (/ (+ (* remainder (nth 9 *overall-results*))
              (* *proportion-rostorff* (nth 23 *overall-results*)))
           total2))
      (format t "   11       9.5      9.7   ~5,1f  ~%"
        (/ (+ (* remainder (nth 10 *overall-results*))
              (* *proportion-rostorff* (nth 24 *overall-results*)))
           total2))
      (format t "   12       9.9      8.8   ~5,1f  ~%"
        (/ (+ (* remainder (nth 11 *overall-results*))
              (* *proportion-rostorff* (nth 25 *overall-results*)))
           total2))
      (format t "   13       8.5      9.9   ~5,1f  ~%"
        (/ (+ (* remainder (nth 12 *overall-results*))
              (* *proportion-rostorff* (nth 26 *overall-results*)))
           total2))
      (format t "   14       6.0      5.5   ~5,1f  ~%"
        (/ (+ (* remainder (nth 13 *overall-results*))
              (* *proportion-rostorff* (nth 27 *overall-results*)))
           total2))
      ))

(defun report-wishner-control-condition-results ()
   (let ((total2 (* .01 (loop for n from 0 upto 13
                          sum (nth n *overall-results*)))))
      (format t "~%Mean Number of Failures to Respond Correctly to the Different Syllable~%")
      (format t "Positions During Learning to Mastery by Massed Practice with Four-Second~%")
      (format t "Rate of Presentation.  Data for people is from Wishner et. al. (1957)~%")
      (format t "  Position       People           EPAM6~%")
      (format t "of Syllable   -------------    ------------~%")
      (format t "     1             2.7           ~5,1f~%" 
        (nth 0 *overall-results*))
      (format t "     2             4.5           ~5,1f~%" 
        (nth 1 *overall-results*))
      (format t "     3             8.1           ~5,1f~%" 
        (nth 2 *overall-results*))
      (format t "     4            10.5           ~5,1f~%" 
        (nth 3 *overall-results*))
      (format t "     5            12.7           ~5,1f~%" 
        (nth 4 *overall-results*))
      (format t "     6            13.6           ~5,1f~%" 
        (nth 5 *overall-results*))
      (format t "     7            13.7           ~5,1f~%" 
        (nth 6 *overall-results*))
      (format t "     8            14.1           ~5,1f~%" 
        (nth 7 *overall-results*))
      (format t "     9            12.9           ~5,1f~%" 
        (nth 8 *overall-results*))
      (format t "    10            11.9           ~5,1f~%" 
        (nth 9 *overall-results*))
      (format t "    11            13.3           ~5,1f~%" 
        (nth 10 *overall-results*))
      (format t "    12            12.5           ~5,1f~%" 
        (nth 11 *overall-results*))
      (format t "    13            12.0           ~5,1f~%" 
        (nth 12 *overall-results*))
      (format t "    14             7.7           ~5,1f~%" 
        (nth 13 *overall-results*))
      (format t "~%Percentage of Total Errors Made during Acquisition at each~%")
      (format t "Syllable Postition of a 14-item Serial List of Nonsense~%")
      (format t "Syllables Predited and Observed.~%")
      (format t "---------------------------------~%")
      (format t "Syllable  McCrary           ~%")
      (format t "Position  &Hunter   EPAM1   EPAM6~%")
      (format t "---------------------------------~%")
      (format t "    1       1.0      0.95  ~5,1f  ~%" 
        (/ (nth 0 *overall-results*) total2) )
      (format t "    2       3.5      1.9   ~5,1f  ~%"
        (/ (nth 1 *overall-results*) total2) )
      (format t "    3       4.3      4.7   ~5,1f  ~%"   
        (/ (nth 2 *overall-results*) total2))
      (format t "    4       6.0      5.6   ~5,1f  ~%"
        (/ (nth 3 *overall-results*) total2))
      (format t "    5       8.0      8.1   ~5,1f  ~%"
        (/ (nth 4 *overall-results*) total2))
      (format t "    6       8.9      8.9   ~5,1f  ~%"
        (/ (nth 5 *overall-results*) total2))
      (format t "    7       9.2     10.5   ~5,1f  ~%"
        (/ (nth 6 *overall-results*) total2))
      (format t "    8      10.0     10.8   ~5,1f  ~%"
        (/ (nth 7 *overall-results*) total2))
      (format t "    9       9.5     10.8   ~5,1f  ~%"
        (/ (nth 8 *overall-results*) total2))
      (format t "   10      10.6     10.5   ~5,1f  ~%"
        (/ (nth 9 *overall-results*) total2))
      (format t "   11       8.8      8.9   ~5,1f  ~%"
        (/ (nth 10 *overall-results*) total2))
      (format t "   12       8.9      8.1   ~5,1f  ~%"
        (/ (nth 11 *overall-results*) total2))
      (format t "   13       7.2      5.6   ~5,1f  ~%"
        (/ (nth 12 *overall-results*) total2))
      (format t "   14       4.0      4.7   ~5,1f  ~%"
        (/ (nth 13 *overall-results*) total2))
      ))






(defun wishner-serial-anticipation-simulation ()
     "This routine performs a simulation of Wishner, Shipley, and 
Hurvich's (1958) serial anticipation experiment from the American
Journal of Psychology V 70 358-262.  Using this experiment subjects
studied the list as a whole just as in the Hovland experiment.
The lists used are not the same as were used by Wishner et. al."
   (setf *overall-results* nil)
   (setf *syllable-lists* *fourteen-syllable-lists*)
   (setf *preliminary-syllables* *hovland-preliminaries*)
   (setf *inter-trial-interval* 2000)
   (setf *ignore-new-stimulus* nil)
   (setf *print-time-updates* nil) 
   (setf *print-imagery-stores* nil) 
   (setf *print-experimenter-updates* nil) 
   (setf *print-epam-updates* nil) 
   (setf *print-strategy-updates* nil)
   (setf *print-forgetting-updates* nil)
   (setf *guessing-routine* nil)
   (setf *concurrent-routine* nil)
   (setf *is-a-of-recoding* 'one-letter-one-sound)
   (setf *method-for-counting-syllables* 'character-method)
   (setf *indexes* '(1))
   (setf *index* 1)
   (setf *number-of-sessions* 100)
   (setf *presentation-rate* 4000)
   (setf *stimulus-duration-rate* (- *presentation-rate* 250))
   (loop for strategy in '(
                           serial-anticipation-strategy
                           serial-anticipation-rostorff-strategy
                           )
     do (rostorff-serial-anticipation strategy))
   (report-wishner-rostorff-results)
   )


(defun rostorff-serial-anticipation (strategy)
   "The only difference between this and hovland-serial-anticipation
is that this routine is given a strategy as input while the hovland
routine always uses the serial-anticipation-strategy.  Also this 
routine uses the serial-anticipation-rostorff experimenter while the 
hovland routine uses serial-anticipation experimenter." 
   (let* ((list-length (length (car *syllable-lists*)))
          (total-presentation-time (/ (+ (* list-length *presentation-rate*) *inter-trial-interval*)
                                      1000.0)))
      (reset-data-collection-arrays)
      (setf *noticing-order* nil)
      (initialize-results-arrays)
      (setf *errors-array* (make-array list-length :initial-element 0))
      (loop repeat *number-of-sessions*
        do
        (setf (aref *response-record* *index*) nil)
        (setf (aref *actual-response-record* *index*) nil)
        (setf (aref *recognition-record* *index*) nil)
        (setf (aref *latency-record* *index*) nil)
        (initialize-variables)
        (loop repeat 2
          do (loop for item in *preliminary-syllables*
               for object = (change-string-to-object item 'auditory)
               do (study object)))
        (zero-out-variables)
        (loop for syllable-list in *syllable-lists*
          do (setf *syllable-list* syllable-list)
          (prepare-net-for-is-a-of-recoding *syllable-list*)
          (setf *sr-list* (change-syllables-to-objects *syllable-list* 'visual))
          (setf *experimenter* 'serial-anticipation-rostorff-experimenter)
          (increment-activation)
          (apply strategy nil)
          (setf (aref *errors-on-list* *index*)
                (+ (aref *errors-on-list* *index*) *errors*))
          (setf *errors* 0)
          (setf (aref *trials-on-list* *index*)
                (+ (aref *trials-on-list* *index*) *trial*))
          ; (update-response-record)
          ))
      ; (print-paired-associate-results)
      (loop initially (format t "Average-errors:")
        for n from 0 upto (1- (length (car *syllable-lists*)))
        for errors = (aref *errors-array* n)
        for average-errors = (1- (/ errors (* *number-of-sessions* 
                                              (length *syllable-lists*) 
                                              1.0)))
        with total-errors = 0
        do (setf total-errors (+ total-errors errors))
        (format t " ~a" average-errors) 
        (setf *overall-results*
              (append *overall-results* (list average-errors))))
      (format t "Average Number of learning trials = ~a~%"
        (/ (aref *trials-on-list* *index*) (* 1.0 
                                              *number-of-sessions* 
                                              (length *syllable-lists*))))
      (format t "Total Learning Time per Item = ~a seconds~%"
        (/   (* (/ (aref *trials-on-list* *index*) (* *number-of-sessions* 
                                                      (length *syllable-lists*)))
                total-presentation-time)
           (length (car *syllable-lists*))))))

   
   
   
   
   
(defun run-all-serial-anticipation-simulations ()
   (hovland-serial-anticipation-simulation)
   (wishner-serial-anticipation-control-group-simulation)
   (wishner-serial-anticipation-simulation))
