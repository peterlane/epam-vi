; Conteff.lsp part of the EPAM VI program.  This is the February 24, 2002 version.
; Copyright 2002 by Howard B. Richman



;;          Context Effects in Letter Perception Simulation

(defun context-effects-in-letter-perception-simulation ()
   (setf *stimulus-lists* (list *context-effects-words*
                            *context-effects-scrambled-letters*
                            *context-effects-just-letters*))
   (setf *probabilities-of-masking-features* '(110 110 110 110))
   (setf *mask* '(#\1 #\1 #\1 #\0 #\1 #\1 #\0 #\0 #\1 #\1 #\0 #\1 #\1 #\1))
   (setf *print-epam-updates* nil)
   (setf *print-time-updates* nil) 
   (setf *print-imagery-stores* nil) 
   (setf *print-experimenter-updates* nil) 
   (setf *print-epam-updates* nil) 
   (setf *print-strategy-updates* nil)
   (setf *print-forgetting-updates* nil)
   (setf *number-of-sessions* 100)
   (setf *guessing-routine* nil)
   (setf *probabilities-of-masking-features* '(250 250 250 250))
   (context-effects-in-letter-perception))


(defun context-effects-in-letter-perception ()
   (setf *errors-array* (make-array '(3 23)))
   (seed-random-generator)
   (initialize-variables)
   (visual-word-learning)
   (create-letters-transformation-property-list)
   (loop repeat *number-of-sessions*
     do (increment-activation)
     (loop for list-of-items in *stimulus-lists*
       for is-a-of-items in '(words scrambled-letters just-letters)
       do 
       (cond ((eql is-a-of-items 'words) (setf *index* 0))
             ((eql is-a-of-items 'scrambled-letters) (setf *index* 1))
             ((eql is-a-of-items 'just-letters) (setf *index* 2)))
       (setf *stimulus-list* (loop for items in list-of-items
                                  collect (first items)))
       (zero-out-variables)
       (setf *presentation-rate* 1000)
       (setf *stimulus-duration-rate* 0)
       (setf *experimenter* 'tachistoscopic-experimenter)
       (loop for items in list-of-items
         do (setf (get 'visual 'imagery-store) nil)
         ; (setf *items* items)
         ; (break)
         (wait-for-item)
         ;(setf *visual-memory-store* (get 'visual 'sensory-store))
         if (memory-store-search (get 'visual 'sensory-store) 
              'signal-to-respond)
         return t
         if (recognize-frontwards-and-backwards 'new 'visual)
         do (when *print-epam-updates* 
               (format t "Front chunk = ~a, Back chunk = ~a~%" 
                 (get-image-of (second *chunks*))
                 (get-image-of (first *chunks*))))
         (when *print-epam-updates*
            (format t "Front chunk episode = ~a~%"
              (loop for episode in (find-episodes 'visual-auditory (second *chunks*))
                collect (test-value-finder 
                          (get-image-of episode)
                          1))))
         ; (break)
         (subvocalize-chunks-into 'visual 'beginning nil nil nil t)
         (let ((object (get-object-for 'beginning 'visual)))
            (put-test-value-on object 4 'length)
            (when (member #\_ object)
               (loop for episode 
                 in (find-episodes 'visual-auditory (get-chunk-for object))
                 for stimulus-object = (test-value-finder (get-image-of episode) 1)
                 if (not-different? object stimulus-object)
                 do 
                 (when *print-epam-updates* 
                    (format t "Trying to add info from episode = ~a to ~a~%" stimulus-object object))
                 ; (break)
                 (loop for subobject on (cdr object)
                   for subimage in (cdr stimulus-object)
                   if (and (eql (car subobject) #\_)
                           (not (eql subimage '~)))
                   do (rplaca subobject subimage))))
            (increment-context-effects-results 
             (change-object-to-string object)
             items)))))
   (report-context-effects-results))

(defvar *probabilities-of-masking-tested-feature*)
(defvar *probabilities-of-masking-context-feature*)
(defvar *stimultaneous-onset-of-first-and-last*)
(defvar *initial-index*)

(defun rumelhart-and-mcclelland-experiment1-simulation ()
   (let ((normal 160))
      (setf *stimulus-lists* (list *context-effects-words*))
      (setf *probabilities-of-masking-tested-feature* 
            (list 
              (list normal (calculate-mask-probability .65 normal))
              (list normal (calculate-mask-probability .75 normal))
              (list normal normal)
              (list normal normal)
              (list normal normal)))
      (setf *probabilities-of-masking-context-feature*
            (list 
              (list (calculate-mask-probability .65 normal)
                (calculate-mask-probability .65 normal))
              (list (calculate-mask-probability .75 normal)
                (calculate-mask-probability .75 normal))
              (list normal normal)
              (list normal
                (calculate-mask-probability 1.33 normal))
              (list normal
                (calculate-mask-probability 1.67 normal))))
      (setf *stimultaneous-onset-of-first-and-last*
            (list 
              (list t t t t)
              (list t t t t)
              (list t t t t)
              (list t t t t)
              (list t t t t)))
      (setf *mask* '(#\1 #\1 #\1 #\0 #\1 #\1 #\0 #\0 #\1 #\1 #\0 #\1 #\1 #\1))
      (setf *print-epam-updates* nil)
      (setf *print-time-updates* nil) 
      (setf *print-imagery-stores* nil) 
      (setf *print-experimenter-updates* nil) 
      (setf *print-epam-updates* nil) 
      (setf *print-strategy-updates* nil)
      (setf *print-forgetting-updates* nil)
      (setf *number-of-sessions* 100)
      (setf *guessing-routine* nil)
      (setf *initial-index* 0)
      (setf *errors-array* (make-array '(5 23)))
      (rumelhart-and-mcclelland-part2)
      (report-rumelhart-and-mcclelland-experiment1-results)))
   
(defun calculate-mask-probability (proportion normal)
   (round (* normal (/ 1 proportion) (/ 1 proportion))))


(defun rumelhart-and-mcclelland-part2 ()
   (seed-random-generator)
   (initialize-variables)
   (visual-word-learning)
   (create-letters-transformation-property-list)
   (loop repeat *number-of-sessions*
     do (increment-activation)
     (loop with simultaneous-onset-of-first-and-last = nil
       for test-masks in *probabilities-of-masking-tested-feature*
       for context-masks in *probabilities-of-masking-context-feature*
       for simultaneous-onset-list in *stimultaneous-onset-of-first-and-last*
       for list-of-items = (car *stimulus-lists*)
       for index from *initial-index*
       do 
       (setf *index* index)
       (setf *stimulus-list* (loop for items in list-of-items
                                  collect (first items)))
       (zero-out-variables)
       (setf *presentation-rate* 1000)
       (setf *stimulus-duration-rate* 0)
       (setf *experimenter* 'tachistoscopic-experimenter)
       (loop for items in list-of-items
         do (setf (get 'visual 'imagery-store) nil)
         (setf *probabilities-of-masking-features*
               (cond ((= (third items) 1)
                      (list (first test-masks) (first context-masks)
                        (first context-masks) (first context-masks)))
                     ((= (third items) 2)
                      (list (second context-masks) (second test-masks)
                        (second context-masks) (second context-masks)))
                     ((= (third items) 3)
                      (list (second context-masks) (second context-masks)
                        (second test-masks) (second context-masks)))
                     ((= (third items) 4)
                      (list (second context-masks) (second context-masks)
                        (second context-masks) (second test-masks)))))
         (setf simultaneous-onset-of-first-and-last 
               (nth (1- (third items)) simultaneous-onset-list))
         (wait-for-item)
         ;(setf *visual-memory-store* (get 'visual 'sensory-store))
         if (memory-store-search (get 'visual 'sensory-store) 
              'signal-to-respond)
         return t
         if 
         (cond (simultaneous-onset-of-first-and-last
                (recognize-frontwards-and-backwards 'new 'visual)
                )
               ('else 
                (recognize-frontwards 'new 'visual)))
         do (when *print-epam-updates* 
               (format t "Front chunk = ~a, Back chunk = ~a~%" 
                 (get-image-of (second *chunks*))
                 (get-image-of (first *chunks*))))
         ;(break)
         (subvocalize-chunks-into 'visual 'beginning nil nil nil t)
         (let ((object (get-object-for 'beginning 'visual)))
            (put-test-value-on object 4 'length)
            (when (member #\_ object)
               (loop for episode 
                 in (find-episodes 'visual-auditory (get-chunk-for object))
                 for stimulus-object = (test-value-finder (get-image-of episode) 1)
                 if (not-different? object stimulus-object)
                 do (loop 
                      for subobject on (cdr object)
                      for subimage in (cdr stimulus-object)
                      if (and (eql (car subobject) #\_)
                              (not (eql subimage '~)))
                      do (rplaca subobject subimage))))
            (increment-context-effects-results 
             (change-object-to-string object)
             items))))))

(defun rumelhart-and-mcclelland-experiment2-simulation ()
   "There are three conditions here -- target after, target same, and target before.
Each will be characterized by two different probabilities the first given will be 
the probabilities when the first letter is the target and the second will be 
when any other letter is the target.  This experiment first presents either the 
target or the mask, then follows whatever was presented first with a mask while the
second is presented.  Our simulation is based upon the assumption that the subject
begins to fill the icon when the first letter position is filled. Normal, low, and 
high are free parameters so long is low is a higher number, indicating more mask
and high is a lower number meaning less mask or high clarity.  Low occurs when 
the letter has already been masked when the first letter is presented, high indicates
that when the first letter was presented there was no mask in this letter position,
however and soon after the letter was presented in that position.  Thus, little 
mask would be perceived in that letter position." 
   (let ((normal 110))
       (setf *probabilities-of-masking-tested-feature* 
            (list 
              (list normal normal)
              (list normal normal)))
      (setf *probabilities-of-masking-context-feature*
            (list 
              (list normal normal)
              (list normal
                (calculate-mask-probability 2.0 normal))))
      (setf *stimultaneous-onset-of-first-and-last*
            (list 
              (list t t t t)
              (list t t t t)))
      (setf *mask* '(#\1 #\1 #\1 #\0 #\1 #\1 #\0 #\0 #\1 #\1 #\0 #\1 #\1 #\1))
      (setf *print-epam-updates* nil)
      (setf *print-time-updates* nil) 
      (setf *print-imagery-stores* nil) 
      (setf *print-experimenter-updates* nil) 
      (setf *print-epam-updates* nil) 
      (setf *print-strategy-updates* nil)
      (setf *print-forgetting-updates* nil)
      (setf *number-of-sessions* 100)
      (setf *guessing-routine* nil)
      (setf *errors-array* (make-array '(5 23)))
      (setf *initial-index* 0)
      (setf *stimulus-lists* (list *context-effects-words*))
      (rumelhart-and-mcclelland-part2)
      (setf *initial-index* 2)
      (setf *stimulus-lists* (list *context-effects-just-letters*))
      (rumelhart-and-mcclelland-part2)
      (report-rumelhart-and-mcclelland-experiment2-results)
      ))


(defun rumelhart-and-mcclelland-experiment3-simulation ()
   "There are three conditions here -- target after, target same, and target before.
Each will be characterized by two different probabilities the first given will be 
the probabilities when the first letter is the target and the second will be 
when any other letter is the target.  This experiment first presents either the 
target or the mask, then follows whatever was presented first with a mask while the
second is presented.  Our simulation is based upon the assumption that the subject
begins to fill the icon when the first letter position is filled. Normal, low, and 
high are free parameters so long is low is a higher number, indicating more mask
and high is a lower number meaning less mask or high clarity.  Low occurs when 
the letter has already been masked when the first letter is presented, high indicates
that when the first letter was presented there was no mask in this letter position,
however and soon after the letter was presented in that position.  Thus, little 
mask would be perceived in that letter position." 
   (let ((normal 140)
         (low 180)
         (high 100))
      (setf *stimulus-lists* (list *context-effects-words*))
      (setf *probabilities-of-masking-tested-feature* 
            (list
             (list normal high)
             (list normal normal)
             (list normal low)))
      (setf *probabilities-of-masking-context-feature*
            (list 
              (list low normal)
              (list normal normal)
              (list high normal)))
      (setf *stimultaneous-onset-of-first-and-last*
            (list 
              (list nil t t nil)
              (list t t t t)
              (list nil t t nil)))
      (setf *mask* '(#\1 #\1 #\1 #\0 #\1 #\1 #\0 #\0 #\1 #\1 #\0 #\1 #\1 #\1))
      (setf *print-epam-updates* nil)
      (setf *print-time-updates* nil) 
      (setf *print-imagery-stores* nil) 
      (setf *print-experimenter-updates* nil) 
      (setf *print-epam-updates* nil) 
      (setf *print-strategy-updates* nil)
      (setf *print-forgetting-updates* nil)
      (setf *number-of-sessions* 100)
      (setf *guessing-routine* nil)
      (setf *errors-array* (make-array '(5 23)))
      (setf *initial-index* 0)
      (rumelhart-and-mcclelland-part2)
      (report-rumelhart-and-mcclelland-experiment3-results)
      ))

(defun rumelhart-and-mcclelland-experiment4-simulation ()
   "There are three conditions here -- target after, target same, and target before.
Each will be characterized by two different probabilities the first given will be 
the probabilities when the first letter is the target and the second will be 
when any other letter is the target.  This experiment first presents either the 
target or the mask, then follows whatever was presented first with a mask while the
second is presented.  Our simulation is based upon the assumption that the subject
begins to fill the icon when the first letter position is filled. Normal, low, and 
high are free parameters so long is low is a higher number, indicating more mask
and high is a lower number meaning less mask or high clarity.  Low occurs when 
the letter has already been masked when the first letter is presented, high indicates
that when the first letter was presented there was no mask in this letter position,
however and soon after the letter was presented in that position.  Thus, little 
mask would be perceived in that letter position." 
   (let ((high 70)
         (low 280)
         (medium 140))
       (setf *probabilities-of-masking-tested-feature* 
            (list 
              (list low low)
              (list low medium)))
      (setf *probabilities-of-masking-context-feature*
            (list 
              (list high high)
              (list low high)))
      (setf *stimultaneous-onset-of-first-and-last*
            (list 
              (list t t t t)
              (list t t t t)))
      (setf *mask* '(#\1 #\1 #\1 #\0 #\1 #\1 #\0 #\0 #\1 #\1 #\0 #\1 #\1 #\1))
      (setf *print-epam-updates* nil)
      (setf *print-time-updates* nil) 
      (setf *print-imagery-stores* nil) 
      (setf *print-experimenter-updates* nil) 
      (setf *print-epam-updates* nil) 
      (setf *print-strategy-updates* nil)
      (setf *print-forgetting-updates* nil)
      (setf *number-of-sessions* 100)
      (setf *guessing-routine* nil)
      (setf *errors-array* (make-array '(5 23)))
      (setf *initial-index* 0)
      (setf *stimulus-lists* (list *context-effects-words*))
      (rumelhart-and-mcclelland-part2)
      (setf *initial-index* 2)
      (setf *stimulus-lists* (list *context-effects-just-letters*))
      (rumelhart-and-mcclelland-part2)
      (report-rumelhart-and-mcclelland-experiment4-results)
      ))

(defun rumelhart-and-mcclelland-experiment5-simulation ()
   "Low is for the long interval, high is for the interval half as long.
These can be calculated to fit the data which is about 86 for the high 
and about 60 for the low.  Ideally, the ration will be 1 to 4 in mask
if I am to fit the equation used in some of these simulations, thus I'll
start with 50 and 200 as the estimates.  The medium should be something 
in between.  There is no mask while it is blank, then it is presented." 
   (let ((high 70)
         (low 280)
         (medium 140))
      (setf *stimulus-lists* (list *context-effects-words*))
      (setf *probabilities-of-masking-tested-feature* 
            (list 
              (list low low)
              (list high low)
              (list low medium)
              (list high high)))
      (setf *probabilities-of-masking-context-feature*
            (list 
              (list low low)
              (list low low)
              (list low high)
              (list high high)))
      (setf *stimultaneous-onset-of-first-and-last*
            (list 
              (list t t t t)
              (list t t t t)
              (list t t t t)
              (list t t t t)
              (list t t t t)))
      (setf *mask* '(#\1 #\1 #\1 #\0 #\1 #\1 #\0 #\0 #\1 #\1 #\0 #\1 #\1 #\1))
      (setf *print-epam-updates* nil)
      (setf *print-time-updates* nil) 
      (setf *print-imagery-stores* nil) 
      (setf *print-experimenter-updates* nil) 
      (setf *print-epam-updates* nil) 
      (setf *print-strategy-updates* nil)
      (setf *print-forgetting-updates* nil)
      (setf *number-of-sessions* 100)
      (setf *guessing-routine* nil)
      (setf *initial-index* 0)
      (setf *errors-array* (make-array '(5 23)))
      (rumelhart-and-mcclelland-part2)
      (report-rumelhart-and-mcclelland-experiment5-results)))


(defun recognize-frontwards-and-backwards (section modality)
  (let ((object (get-first-object-in-sensory-store section modality)))
    (cond (object (reset-new-stimulus-flags modality)
                  (push 'top-of-stack *chunks*)
                  (time-tally-to-access-chunk)
                  (push (get-chunk-for object) *chunks*)
                  (push (get-chunk-for (cons (car object) (cons '~ (cdr object))))
                    *chunks*)
                  t)
          ('else (reset-new-stimulus-flags modality)
                 nil))))

(defun recognize-frontwards (section modality)
  (let ((object (get-first-object-in-sensory-store section modality)))
    (cond (object (reset-new-stimulus-flags modality)
                  (push 'top-of-stack *chunks*)
                  (time-tally-to-access-chunk)
                  (push (get-chunk-for object) *chunks*)
                  t)
          ('else (reset-new-stimulus-flags modality)
                 nil))))



;;        These Routines are Specific to Context Effects Simulation

(defun visual-word-learning (&optional net-file)
   "When a net-file is given to this routine, it saves the result of its 
 learning onto a file with the name given by the net-file.  The net-file must
 be a file not already on the directory and must be given in quotes."
  (format t "This is the study stage.~%")
  (study-letters)
  (learn-basic-phonics)
  (setf *whole-word-parameter* 0)
  (study-words 1176)
  (setf *whole-word-parameter* 0)
  (study-words 1176)
  (setf *whole-word-parameter* 10)
  (study-words 1176)
  (setf *whole-word-parameter* 20)
  (study-words 1058)
  (setf *whole-word-parameter* 40)
  (study-words 941)
  (setf *whole-word-parameter* 80)
  (study-words 823)
  (setf *whole-word-parameter* 100)
  (study-words 705)
  (when net-file (write-net-to net-file)))

(defvar *rumelhart-letter-feature-order*
  '((9 2 1 6 13 8 7)
    (1 6 13 14 4 11 8)
    (1 2 9 14)
    (1 6 13 14 4 11)
    (1 2 9 14 7)
    (1 2 9 7)
    (1 2 9 14 13 8)
    (2 9 6 13 7 8)
    (4 11 1 14)
    (6 13 14 9)
    (2 9 7 5 12)
    (2 9 14)
    (2 9 3 5 6 13)
    (2 9 3 12 13 6)
    (2 9 14 13 6 1)
    (2 9 1 6 8 7)
    (2 9 14 13 6 1 12)
    (2 9 1 6 8 7 12)
    (1 2 7 8 13 14)
    (4 11 1)
    (2 9 14 13 6)
    (2 9 10 5)
    (2 9 10 12 13 6)
    (3 12 5 10)
    (3 5 11)
    (1 5 10 14)))

    
 (defun study-letters ()
   (format t "Learning the Rumelhart Letters.~%")
   (let ((stimuli (loop for stimulus in (nthcdr 4 *rumelhart-letters*) by #'cddr
                    for order in *rumelhart-letter-feature-order*
                    collect (create-rumelhart-letter-object stimulus order)))
         (responses (loop for stimulus in (nthcdr 5 *rumelhart-letters*) by #'cddr
                      ; do (setf *stimulus* stimulus)
                      collect (change-string-to-object stimulus 'visual))))
      (loop with any-not-learned? = nil
        do 
        (setf any-not-learned? nil)
        (loop 
          for stimulus in stimuli
          for response in responses
          if (study stimulus)
          do (setf any-not-learned? t)
          (loop while (associate 'letter stimulus response))
          else if t
          do (when (associate 'letter stimulus response 1 2 nil 'stimulus)
                (setf any-not-learned? t)))
        if (null any-not-learned?) 
        return t)))


(defun create-letters-transformation-property-list ()
  (loop for letters on *rumelhart-letters* by #'cddr
        for digit-string = (first letters)
        for character = (coerce (second letters) 'character)
        do (setf (get 'letters-transformation-property-list character)
                 digit-string)))

(defun change-string-to-phonic-object (object1 is-a)
   (let ((object (cons nil (coerce object1 'list))))
      (put-value object is-a 'is-a)))
     

(defun learn-basic-phonics ()
   (let ((visual-letters (loop for visual-letter 
                           in *basic-phonics* 
                           by #'cddr 
                           collect visual-letter))
         (auditory-sounds (loop for auditory-sound 
                            in (cdr *basic-phonics*) 
                            by #'cddr
                            collect auditory-sound)))
      (loop while
        (member t (mapcar #'(lambda (x) 
                              (study 
                                (append (change-string-to-phonic-object x 'visual) 
                                  (list '~))))
                    visual-letters)))
      (loop while
        (member t (mapcar #'(lambda (x) 
                              (study 
                                (append (change-string-to-phonic-object x 'auditory)
                                  (list '~))))
            auditory-sounds)))
      (loop while
        (member t 
          (mapcar #'(lambda (x y)
                      (associate
                       'visual-auditory
                       (append (change-string-to-phonic-object x 'visual) (list '~))
                       (append (change-string-to-phonic-object y 'auditory) (list '~))
                       ))
            visual-letters
            auditory-sounds)))
      ;(loop for letter in *letters* ; do this so system can recognize all letters in final position
      ;  for object = (list nil '~ letter)
      ;  do (put-value object 'visual 'is-a)
      ;  (study object))(associate 'visual-auditory object correct-response)(associate 'visual-auditory object correct-response)(associate 'visual-auditory object correct-response)(associate 'visual-auditory object correct-response)(associate 'visual-auditory object correct-response)
      ))


(defun study-words (n)
   (loop repeat 100
     do (increment-activation))
   (loop 
     initially 
     (when *print-major-updates*
        (format t 
          "Studying ~a most frequent words with whole-word parameter at ~a%~%"
          n *whole-word-parameter*))
     repeat n
     for words on *1176-common-4-letter-words* by #'cddr
     for object = (change-string-to-object (first words) 'visual)
     for backwards-object = (cons (car object) (cons '~ (cdr object)))
     for correct-response = (change-string-to-object (second words) 'auditory)
     for 1st-chunk = (get-chunk-for object)
     for 1st-translation = (extract-associate 'visual-auditory 1st-chunk)
     for 2nd-chunk = (get-chunk-for backwards-object)
     for 2nd-translation = (extract-associate 'visual-auditory 2nd-chunk)
     with remainder = nil
     ;do (setf *words* words)
     ;(format t "~a~%" (car words))
     ;(break)
     if (< (random 100) *whole-word-parameter*)
     do (loop while (study object)
          do (loop while (study correct-response))
          (loop while (associate 'visual-auditory object correct-response)))
     (increment-activation)
     else if (or (is-translation-correct? 1st-translation correct-response)
                 (is-combined-translation-correct? 1st-translation
                  2nd-translation
                  correct-response))
     do 'nil
     else if (is-translation-correct? 1st-translation (second correct-response))
     do (setf remainder (get-end-by-subtracting 1st-chunk object))
     (when remainder
        (loop while (study remainder))
        (loop while (study (third correct-response)))
        (loop while (associate 'visual-auditory
                      remainder
                      (third correct-response)))
        (increment-activation)
        )
     else if (is-translation-correct? 2nd-translation (third correct-response))
     do (setf remainder (get-beginning-by-subtracting 2nd-chunk object))
     (when remainder
        (loop while (study remainder))
        (loop while (study (second correct-response)))
        (loop while (associate 'visual-auditory
                      remainder
                      (second correct-response)))
        (increment-activation)
        )
     else do 'nil))


(defun is-translation-correct? (translation-chunk correct-response)
  (let ((translation (convert-chunk-to-object translation-chunk)))
    (equal (change-object-to-string translation)
           (change-object-to-string correct-response))))


(defun is-combined-translation-correct? (1st-trans 2nd-trans correct-response)
  (let ((translation1 
         (change-object-to-string (convert-chunk-to-object 1st-trans)))
        (translation2 
         (change-object-to-string (convert-chunk-to-object 2nd-trans)))
        (response (change-object-to-string correct-response)))
    (when (equal (concatenate 'string translation1 translation2) response)
      (when *print-strategy-updates*
        (format t "Success!  Combined ~a and ~a to read ~a~%"
                translation1 translation2 response))
      t)))

(defun get-end-by-subtracting (chunk object)
  "This routine tries to find a part at the end of the object after subtracting
the image of the chunk from the beginning of the object. Revised Feb 22, 1998,
so that end is not represented as having the is-a backwards but is instead 
recognizing as having an ordered list beginning with a squiggle."
  (let ((image (if (characterp chunk) 
                  (list nil chunk) 
                  (get chunk 'image))))
    (loop for subobject in (cdr image)
          with object-tail = (cdr object)
          if (null object-tail) return nil
          else if (equal (specify subobject)
                         (specify (car object-tail)))
          do (setf object-tail (cdr object-tail))
          else if (eql (specify subobject) '~)
          return (cons '((is-a visual))
                        (cons '~ object-tail))
          else if t 
          return nil
          finally (return (cons '((is-a visual))
                                (cons '~ object-tail))))))

(defun get-beginning-by-subtracting (chunk object)
  "This routine tries to find a part at the beginning of the object after
subtracting the image of the chunk from the end of the object."
  (let ((image (if (characterp chunk) 
                 (list nil chunk) 
                 (get chunk 'image))))
    (loop for subobject in (reverse (cdr image))
          with object-tail = (reverse (cdr object))
          if (null object-tail) return nil
          else if (equal (specify subobject)
                         (specify (car object-tail)))
          do (setf object-tail (cdr object-tail))
          else if (eql (specify subobject) '~)
          return (cons '((is-a visual))
                       (append (reverse object-tail)
                               (list '~)))
          else if t 
          return nil
          finally (return (cons '((is-a visual))
                                (append (reverse object-tail)
                                        (list '~)))))))

(defun increment-context-effects-results (results items)
   (when *print-major-updates* 
      (format t "Results = ~a, Items = ~a~%" results items))
   (let* ((stimulus (first items))
          (choices (second items))
          (position (1- (third items)))
          (constraint (fourth items))
          (frequency (fifth items))
          (correct-answer (char stimulus position))
          (possible-answers (list (char choices 0) (char choices 1)))
          (answer (char results position))
          (fr-score (cond ((eql answer correct-answer) 1)
                          ('else 0)))
          (score (cond ((eql answer correct-answer) 1)
                       ((member answer possible-answers) 0)
                       ('else .5)))
          (ww-score (loop for i from 0 to 3
                      for correct-letter = (char stimulus i)
                      for response-answer = (char results i)
                      if (not (eq correct-letter response-answer))
                      return 0
                      finally (return 1))))
      (when *print-major-updates*
         (format t "answer = ~a, correct-answer = ~a~%" answer correct-answer)
         (format t "Position-score = ~a~%" score)
         (format t "Whole-word-correct? = ~a~%~%" ww-score))
      (setf (aref *errors-array* *index* position) 
            (cons score (aref *errors-array* *index* position)))
      (setf (aref *errors-array* *index* (+ 16 position)) 
            (cons fr-score (aref *errors-array* *index* (+ 16 position))))
      (setf (aref *errors-array* *index* 8) (cons score (aref *errors-array* *index* 8)))
      (setf (aref *errors-array* *index* 9) (cons ww-score (aref *errors-array* *index* 9)))
      (setf (aref *errors-array* *index* 22) (cons fr-score (aref *errors-array* *index* 22)))
      (when (eq constraint 'high) 
         (setf (aref *errors-array* *index* 4) (cons score (aref *errors-array* *index* 4)))
         (setf (aref *errors-array* *index* 10) (cons ww-score (aref *errors-array* *index* 10)))
         (setf (aref *errors-array* *index* 14) (cons fr-score (aref *errors-array* *index* 14))))
      (when (eq constraint 'low) 
         (setf (aref *errors-array* *index* 5) (cons score (aref *errors-array* *index* 5)))
         (setf (aref *errors-array* *index* 11) (cons ww-score (aref *errors-array* *index* 11)))
         (setf (aref *errors-array* *index* 15) (cons fr-score (aref *errors-array* *index* 15))))
      (when (and frequency (> frequency 648))
         (setf (aref *errors-array* *index* 6) (cons score (aref *errors-array* *index* 6)))
         (setf (aref *errors-array* *index* 12) (cons ww-score (aref *errors-array* *index* 12)))
         (setf (aref *errors-array* *index* 20) (cons fr-score (aref *errors-array* *index* 20))))
      (when (and frequency (< frequency 54))
         (setf (aref *errors-array* *index* 7) (cons score (aref *errors-array* *index* 7)))
         (setf (aref *errors-array* *index* 13) (cons ww-score (aref *errors-array* *index* 13)))
         (setf (aref *errors-array* *index* 21) (cons fr-score (aref *errors-array* *index* 21))))))

(defun report-context-effects-results ()
   (format t "~%Word Constraint Effect -- Proportion Correct, High Letter~%")
   (format t "Constraint Versus Low Letter Constraint. Data for people is~%")
   (format t "from Johnston (1974).~%")
   (format t "-----------------------------------------------------------~%")
   (format t "Condition/Letter~%")
   (format t "   constraint      People   EPAM3A   EPAM6   IAM   IAMSP~%")
   (format t "-----------------------------------------------------------~%")
   (format t "Forced-choice~%")       
   (format t "  High               .77       .83     ~2,2f   .78     .76~%"
     (find-average-from-errors-array 0 4))
   (format t "  Low                .80       .82     ~2,2f   .77     .75~%"
     (find-average-from-errors-array 0 5))
   (format t "Free-report of~%")
   (format t " critical letters~%")
   (format t "  High               .55       .65     ~2,2f   .59     .54~%"
     (find-average-from-errors-array 0 14))
   (format t "  Low                .54       .65     ~2,2f   .57     .52~%"
     (find-average-from-errors-array 0 15))
   (format t "Complete report~%")
   (format t " of word~%")
   (format t "  High               .31       .22     ~2,2f~%"
     (find-average-from-errors-array 0 10))
   (format t "  Low                .31       .26     ~2,2f~%"
     (find-average-from-errors-array 0 11))
   (format t "~%Serial-Position Effect -- Proportion Correct by Serial Position~%")
   (format t "Free Report (FR) and Forced Choice (FC). Data for people is estimated~%")
   (format t "from graph of Johnston (1974 p.85)~%")
   (format t "----------------------------------------------------------~%")
   (format t "           People    EPAM3A    EPAM6      IAM      IAMSP~%")
   (format t " Serial   --------  --------  --------  --------  --------~%")
   (format t "position   FR   FC   FR   FC   FR   FC   FR   FC   FR   FC~%")
   (format t "----------------------------------------------------------~%")
   (format t "   1      .55  .82  .72  .86  ~2,2f  ~2,2f  .54  .76  .60  .79~%"
     (find-average-from-errors-array 0 16) (find-average-from-errors-array 0 0))
   (format t "   2      .56  .77  .67  .83  ~2,2f  ~2,2f  .59  .78  .62  .80~%"
     (find-average-from-errors-array 0 17) (find-average-from-errors-array 0 1))
   (format t "   3      .50  .75  .56  .78  ~2,2f  ~2,2f  .58  .78  .44  .70~%"
     (find-average-from-errors-array 0 18) (find-average-from-errors-array 0 2))
   (format t "   4      .54  .77  .67  .82  ~2,2f  ~2,2f  .60  .79  .47  .72~%"
     (find-average-from-errors-array 0 19) (find-average-from-errors-array 0 3))
   (format t "~%Word-Frequency Effect-- Difference in Proportion Correct for~%")
   (format t "Letters in High-Frequency Versus Low-Frequency Words.~%")
   (format t "Data for people is from Johnston (1974)~%")
   (format t "-----------------------------------------------------------~%")
   (format t "Difference    People    EPAM3A    EPAM6   IAM     IAMSP~%")
   (format t "-----------------------------------------------------------~%")
   (format t "Free-report    .08        .04      ~2,2f    .04      .04~%"
     (- (find-average-from-errors-array 0 20) (find-average-from-errors-array 0 21)))
   (format t "Forced-choice  .01        .02      ~2,2f    .02      .02~%"
     (- (find-average-from-errors-array 0 6) (find-average-from-errors-array 0 7)))
   (format t "~%Word-Superiority Effect-- Proportion Correct~%")
   (format t "Words Versus Unrelated Letters. Human data is from~%")
   (format t "Johnston (1974).~%")
   (format t "------------------------------------------------------------~%")
   (format t "Condition            People    EPAM3A    EPAM6   IAM   IAMSP~%")
   (format t "-------------------------------------------------------------~%")
   (format t "Forced-choice ~%")         
   (format t "  Words                .85       .82      ~2,2f    .78    .75~%"
     (find-average-from-errors-array 0 8))
   (format t "  Unrelated letters    .68       .74      ~2,2f    .71    .70~%"
     (find-average-from-errors-array 1 8))
   (format t "Free-report of~%")
   (format t "    critical letters~%")
   (format t "  Words                .68       .65      ~2,2f    .58    .53~%"
     (find-average-from-errors-array 0 22))
   (format t "  Unrelated letters    .28       .49      ~2,2f    .44    .43~%"
     (find-average-from-errors-array 1 22))
   (format t "Complete report of~%")
   (format t "    all four letters~%")
   (format t "  Words                .43       .24      ~2,2f   ~%"
     (find-average-from-errors-array 0 9))
   (format t "  Unrelated letters    .02       .03      ~2,2f    ~%"
     (find-average-from-errors-array 1 9))
   (format t "~%Word-Letter Effect-- Proportion Correct~%")
   (format t "Words Versus Letters surrounded by Number signs.~%")
   (format t "Human data is from Johnston (1974).~%")
   (format t "------------------------------------------------------------~%")
   (format t "Condition            People    EPAM3A    EPAM6   IAM   IAMSP~%")
   (format t "-------------------------------------------------------------~%")
   (format t "Forced-choice ~%")         
   (format t "  Words                .85       .82      ~2,2f    .78    .75~%"
     (find-average-from-errors-array 0 8))
   (format t "  Unrelated letters    .71       .68      ~2,2f    .66    .66~%"
     (find-average-from-errors-array 2 8))
   (format t "Free-report of~%")
   (format t "    critical letters~%")
   (format t "  Words                .68       .65      ~2,2f    .58    .53~%"
     (find-average-from-errors-array 0 22))
   (format t "  Unrelated letters    .36       .37      ~2,2f    .36    .35~%"
     (find-average-from-errors-array 2 22))
   (format t "Complete report of~%")
   (format t "    all four letters~%")
   (format t "  Words                .43       .24      ~2,2f~%"
     (find-average-from-errors-array 0 9))
   (format t "  Unrelated letters    ---       ---      ---~%")
   )
 
(defun find-average-from-errors-array (index position)
   (let ((items (aref *errors-array* index position)))
      (when items
         (loop for item in items
           for n from 1
           sum item into total
           finally (return (/ total n))))))

(defun report-rumelhart-and-mcclelland-experiment1-results ()
   (format t "~%Proportion of correct forced-choice responses to~%")
   (format t "the target letter as a function of the relative duration~%")
   (format t "of the context to the duration of the target letter.~%")
   (format t "Data for people is from Rumelhart & McClelland,~%")
   (format t "1982, estimated from their Figure 4.~%")
   (format t "-----------------------------------------------------------~%")
   (format t "Ratio      People   EPAM6   IAM~%")
   (format t "-----------------------------------------------------------~%")
   (format t " .65        .65      ~2,2f    .66~%"
     (find-average-from-errors-array 0 8))
   (format t " .75        .67      ~2,2f    .70~%"
     (find-average-from-errors-array 1 8))
   (format t "1.00        .73      ~2,2f    .73~%"
     (find-average-from-errors-array 2 8))
   (format t "1.33        .78      ~2,2f    .76~%"
     (find-average-from-errors-array 3 8))
   (format t "1.67        .80      ~2,2f    .81~%"
     (find-average-from-errors-array 4 8)))

(defun report-rumelhart-and-mcclelland-experiment2-results ()
   (format t "~%Effects of doubling the duration of the context~%")
   (format t "on the perception of letters in words and in strings~%")
   (format t "of numerals (for people and IAM) and in plus signs~%")
   (format t "for EPAM.  Data for people is from Rumelhart & McClelland,~%")
   (format t "1982, estimated from their Figure 6.~%")
   (format t "----------------------------------------------------~%")
   (format t "                  People       EPAM6       IAM~%")
   (format t "                ----------- ----------- -----------~%")
   (format t "Condition       Word  Digit Word  Digit Word  Digit ~%") 
   (format t "---------------------------------------------------~%")
   (format t "Normal           .81   .63   ~2,2f   ~2,2f   .80   .67~%"
     (find-average-from-errors-array 0 8) (find-average-from-errors-array 2 8))
   (format t "Extra Context    .87   .62   ~2,2f   ~2,2f   .90   .67~%"
     (find-average-from-errors-array 1 8) (find-average-from-errors-array 3 8)))



(defun report-rumelhart-and-mcclelland-experiment3-results ()
   (format t "~%Proportion Correct Responses as,~%")
   (format t "a Function of the Relative Times of Offset for.~%")
   (format t "the Target and Context Letters.  Data for ~%")
   (format t "people is from Rumelhart & McClelland, 1982,~%")
   (format t "Experiment 3.~%")
   (format t "-----------------------------------------------------------~%")
   (format t "Condition    People    EPAM6   IAM~%")
   (format t "-----------------------------------------------------------~%")
   (format t "Context early  .75      ~2,2f    .83~%"
     (find-average-from-errors-array 0 8))
   (format t "Simultaneous   .74      ~2,2f    .77~%"
     (find-average-from-errors-array 1 8))
   (format t "Context late   .68      ~2,2f    .65~%"
     (find-average-from-errors-array 2 8))
   (format t "~%Serial-position results for the context-early,~%")
   (format t "simultaneous, and context-late conditions.~%")
   (format t "Data for people is from Rumelhart & McClelland,~%")
   (format t "1982, Experiment 3.  Those statistics not~%")
   (format t "reported in Rumelhart & McClelland's Table 2~%")
   (format t "are estimated from their figures 8 and 19.~%")
   (format t "-----------------------------------------------------------~%")
   (format t "Serial-position   People   EPAM6   IAMSP~%")
   (format t "-----------------------------------------------------------~%")
   (format t "Context early~%")
   (format t "    1               .75     ~2,2f    .77~%"
     (find-average-from-errors-array 0 0))
   (format t "    2               .81     ~2,2f    .84~%"
     (find-average-from-errors-array 0 1))
   (format t "    3               .77     ~2,2f    .80~%"
     (find-average-from-errors-array 0 2))
   (format t "    4               .67     ~2,2f    .77~%"
     (find-average-from-errors-array 0 3))
   (format t "Simultaneious~%")
   (format t "    1               .76     ~2,2f    .76~%"
     (find-average-from-errors-array 1 0))
   (format t "    2               .76     ~2,2f    .78~%"
     (find-average-from-errors-array 1 1))
   (format t "    3               .74     ~2,2f    .69~%"
     (find-average-from-errors-array 1 2))
   (format t "    4               .73     ~2,2f    .69~%"
     (find-average-from-errors-array 1 3))
   (format t "Context late~%")
   (format t "    1               .72     ~2,2f    .71~%"
     (find-average-from-errors-array 2 0))
   (format t "    2               .67     ~2,2f    .67~%"
     (find-average-from-errors-array 2 1))
   (format t "    3               .65     ~2,2f    .60~%"
     (find-average-from-errors-array 2 2))
   (format t "    4               .67     ~2,2f    .64~%"
     (find-average-from-errors-array 2 3))
   )

(defun report-rumelhart-and-mcclelland-experiment4-results ()
   (format t "~%Proportion correct responses for strings containing word~%")
   (format t "and digit contexts as a function of whether the target letter~%")
   (format t "came early or late in the interval.~%")
   (format t "fData for people is from Rumelhart & McClelland,~%")
   (format t "1982, experiment 4 estimated from their Figure 10.~%")
   (format t "----------------------------------------------------~%")
   (format t "                  People       EPAM6       IAM~%")
   (format t "                ----------- ----------- -----------~%")
   (format t "Condition       Word  Digit Word  Digit Word  Digit ~%") 
   (format t "---------------------------------------------------~%")
   (format t "Early            .71   .56   ~2,2f   ~2,2f   .65   .59~%"
     (find-average-from-errors-array 0 8) (find-average-from-errors-array 2 8))
   (format t "Late             .75   .55   ~2,2f   ~2,2f   .76   .59~%"
     (find-average-from-errors-array 1 8) (find-average-from-errors-array 3 8)))


(defun report-rumelhart-and-mcclelland-experiment5-results ()
   (format t "~%Proportion correct responses as a function of~%")
   (format t "the duration of the context and target presentations.~%")
   (format t "Data for people is from Rumelhart & McClelland,~%")
   (format t "1982, Experiment 5 estimated from their Figure 12.~%")
   (format t "-----------------------------------------------------------~%")
   (format t "Target-Context      People   EPAM6   IAM~%")
   (format t "-----------------------------------------------------------~%")
   (format t "Short-Short          .64      ~2,2f    .59~%"
     (find-average-from-errors-array 0 8))
   (format t "Long-Short           .70      ~2,2f    .66~%"
     (find-average-from-errors-array 1 8))
   (format t "Short-Long           .77      ~2,2f    .82~%"
     (find-average-from-errors-array 2 8))
   (format t "Long-Long            .87      ~2,2f    .88~%"
     (find-average-from-errors-array 3 8)))
