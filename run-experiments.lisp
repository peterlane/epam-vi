;; This script loads and runs all the experiments in EPAM 6
;; > sbcl --script run-experiments.lisp
;;
;; Written by Peter Lane

(load "epam6.lisp")
(load "epamdata.lisp")
(load "master.lisp")
(load "concept-formation.lisp")
(load "context-effects.lisp")
(load "exptsim.lisp")
(load "serial-anticipation.lisp")

(run-all-paired-associate-simulations)
(run-all-stm-simulations)

