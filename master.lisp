; master.lsp.  These routines run the paired-associate and short-term memory
; simulations.  They also include EPAM VI's model of STM.
; This is the February 24, 2002 version. Copyright 2002 by Howard B. Richman

(defvar *fixed-time-required-in-anderson-and-reder-1999* 1260)
(defvar *fixed-time-required-in-anderson-1974* 1110)

(defun safe-list (ilist)
  "Provided for error-arrays, which are improper lists.
   TODO: Is the last item (after the dot) needed?"
  (cond ((not (listp ilist)) '()) ; (list ilist)) ; ignore last item
        ((null ilist) '())
        (t (cons (car ilist) (safe-list (cdr ilist))))))

(defun quick-lockhart-simulation ()
  (setf *overall-results* nil)
  (loop for j in '(9 5 3 2.33 2 1.8 1.67 1.5 1.4 1.33 1.25 1.2 1.17)
        do (format t "For time-ratio = ~a~%" j)
        (quick-lockhart 1 j))
  (format t "Estimates of Lockhart's (1969) data, also plotted this way~%") 
  (format t "by Estes (1995), as a function of time-ratio.  EPAM6 is ~%")
  (format t "based upon the idea that people's subjective memory for~%")
  (format t "the time that they learned an episode follows a normal~%")
  (format t "curve with a standard deviation of 0.97 when plotted on a~%")
  (format t "lot-time scale.~%")
  (format t "Actual  Ratio   People   EPAM6~%")
  (format t "------------------------------~%")
  (format t "36:4      9      0.88   ~5,3f~%" (nth 0 *overall-results*))
  (format t "20:4      5      0.82   ~5,3f~%" (nth 1 *overall-results*))
  (format t "40:8      5      0.78   ~5,3f~%" (nth 1 *overall-results*))
  (format t "12:4:     3      0.78   ~5,3f~%" (nth 2 *overall-results*))
  (format t "24:8      3      0.73   ~5,3f~%" (nth 2 *overall-results*))
  (format t "48:16     3      0.62   ~5,3f~%" (nth 2 *overall-results*))
  (format t "28:12     2.33   0.67   ~5,3f~%" (nth 3 *overall-results*))
  (format t "8:4       2      0.70   ~5,3f~%" (nth 4 *overall-results*))
  (format t "16:8      2      0.65   ~5,3f~%" (nth 4 *overall-results*))
  (format t "32:16     2      0.69   ~5,3f~%" (nth 4 *overall-results*))
  (format t "64:32     2      0.63   ~5,3f~%" (nth 4 *overall-results*))    
  (format t "36:20     1.8    0.62   ~5,3f~%" (nth 5 *overall-results*))
  (format t "20:12     1.67   0.62   ~5,3f~%" (nth 6 *overall-results*))
  (format t "40:24     1.67   0.58   ~5,3f~%" (nth 6 *overall-results*))
  (format t "12:8      1.5    0.59   ~5,3f~%" (nth 7 *overall-results*))
  (format t "24:16     1.5    0.60   ~5,3f~%" (nth 7 *overall-results*))
  (format t "28:20     1.4    0.60   ~5,3f~%" (nth 8 *overall-results*))
  (format t "16:12     1.33   0.54   ~5,3f~%" (nth 9 *overall-results*))
  (format t "32:24     1.33   0.55   ~5,3f~%" (nth 9 *overall-results*))
  (format t "20:16     1.25   0.56   ~5,3f~%" (nth 10 *overall-results*))
  (format t "24:20     1.2    0.56   ~5,3f~%" (nth 11 *overall-results*))
  (format t "28:24     1.17   0.54   ~5,3f~%" (nth 12 *overall-results*))
  (loop with sum-of-squares = 0
    for n in '(0 1 1 2 2 
               2 3 4 4 4 
               4 5 6 6 7 
               7 8 9 9 10 
               11 12)
    for people in '(.88 .82 .78 .78 .73 
                    .62 .67 .70 .65 .69
                    .63 .62 .62 .58 .59
                    .60 .60 .54 .55 .56 
                    .56 .54)
     for epam = (nth n *overall-results*)
     for diff = (expt (- people epam) 2)
     do (setf sum-of-squares (+ sum-of-squares diff))
     finally (format t "Sum of squares for EPAM = ~a~%" sum-of-squares))
   )

(defun lockhart-regular-intervals-simulation ()
  (setf *overall-results* nil)
  (loop for j in '(1.0 1.1 1.17 1.2 1.25 1.3 1.33 1.4 1.5 1.51 1.6 1.67 1.7 1.8 1.9 1.99
                   2.0 2.01 2.02 2.1 2.2 2.3 2.4 2.5 2.6 2.7 2.8 2.9 2.99
                   3.0 3.01 3.1 3.2 3.3 3.4 3.5 3.6 3.7 3.8 3.9 
                   4.0 4.1 4.2 4.3 4.4 4.5 4.6 4.7 4.8 4.9 
                   5.0 5.01 5.1 5.2 5.3 5.4 5.5 5.6 5.7 5.8 5.9 
                   6.0 6.1 6.2 6.3 6.4 6.5 6.6 6.7 6.8 6.9 
                   7.0 7.1 7.2 7.3 7.4 7.5 7.6 7.7 7.8 7.9 
                   8.0 8.1 8.2 8.3 8.4 8.5 8.6 8.7 8.8 8.9 
                   9.0 9.1 9.2 9.3 9.4 9.5 9.6 9.7 9.8 9.9 
                   10.0             
                   )
        do (format t "For time-ratio = ~a~%" j)
        (quick-lockhart 1 j))
   (format t "Estimates of Lockhart's (1969) data, as a function of time-ratio.  EPAM6 is ~%")
   (format t "based upon the idea that people's subjective memory for~%")
   (format t "the time that they learned an episode follows a normal~%")
   (format t "curve with a standard deviation of 1.24 when plotted on a~%")
   (format t "lot-time scale.~%")
   (format t "Actual  EPAM6~%")
   (format t "------------------------------~%")
   (format t "1.0  ~5,3f~%" (nth 0 *overall-results*))
   (format t "1.1  ~5,3f~%" (nth 1 *overall-results*))
   (format t "1.17 ~5,3f~%" (nth 2 *overall-results*))
   (format t "1.2  ~5,3f~%" (nth 3 *overall-results*))
   (format t "1.25 ~5,3f~%" (nth 4 *overall-results*))
   (format t "1.3  ~5,3f~%" (nth 5 *overall-results*))
   (format t "1.33 ~5,3f~%" (nth 6 *overall-results*))
   (format t "1.4  ~5,3f~%" (nth 7 *overall-results*))
   (format t "1.5  ~5,3f~%" (nth 8 *overall-results*))
   (format t "1.51 ~5,3f~%" (nth 9 *overall-results*))
   (format t "1.6  ~5,3f~%" (nth 10 *overall-results*))
   (format t "1.67 ~5,3f~%" (nth 11 *overall-results*))
   (format t "1.7  ~5,3f~%" (nth 12 *overall-results*))
   (format t "1.8  ~5,3f~%" (nth 13 *overall-results*))
   (format t "1.9  ~5,3f~%" (nth 14 *overall-results*))
   (format t "1.99 ~5,3f~%" (nth 15 *overall-results*))
   (format t "2.0  ~5,3f~%" (nth 16 *overall-results*))
   (format t "2.01 ~5,3f~%" (nth 17 *overall-results*))
   (format t "2.02 ~5,3f~%" (nth 18 *overall-results*))
   (format t "2.1  ~5,3f~%" (nth 19 *overall-results*))
   (format t "2.2  ~5,3f~%" (nth 20 *overall-results*))
   (format t "2.3  ~5,3f~%" (nth 21 *overall-results*))
   (format t "2.4  ~5,3f~%" (nth 22 *overall-results*))
   (format t "2.5  ~5,3f~%" (nth 23 *overall-results*))
   (format t "2.6  ~5,3f~%" (nth 24 *overall-results*))
   (format t "2.7  ~5,3f~%" (nth 25 *overall-results*))
   (format t "2.8  ~5,3f~%" (nth 26 *overall-results*))
   (format t "2.9  ~5,3f~%" (nth 27 *overall-results*))
   (format t "2.99 ~5,3f~%" (nth 28 *overall-results*))
   (format t "3.0  ~5,3f~%" (nth 29 *overall-results*))
   (format t "3.01 ~5,3f~%" (nth 30 *overall-results*))
   (format t "3.1  ~5,3f~%" (nth 31 *overall-results*))
   (format t "3.2  ~5,3f~%" (nth 32 *overall-results*))
   (format t "3.3  ~5,3f~%" (nth 33 *overall-results*))
   (format t "3.4  ~5,3f~%" (nth 34 *overall-results*))
   (format t "3.5  ~5,3f~%" (nth 35 *overall-results*))
   (format t "3.6  ~5,3f~%" (nth 36 *overall-results*))
   (format t "3.7  ~5,3f~%" (nth 37 *overall-results*))
   (format t "3.8  ~5,3f~%" (nth 38 *overall-results*))
   (format t "3.9  ~5,3f~%" (nth 39 *overall-results*))
   (format t "4.0  ~5,3f~%" (nth 40 *overall-results*))
   (format t "4.1  ~5,3f~%" (nth 41 *overall-results*))
   (format t "4.2  ~5,3f~%" (nth 42 *overall-results*))
   (format t "4.3  ~5,3f~%" (nth 43 *overall-results*))
   (format t "4.4  ~5,3f~%" (nth 44 *overall-results*))
   (format t "4.5  ~5,3f~%" (nth 45 *overall-results*))
   (format t "4.6  ~5,3f~%" (nth 46 *overall-results*))
   (format t "4.7  ~5,3f~%" (nth 47 *overall-results*))
   (format t "4.8  ~5,3f~%" (nth 48 *overall-results*))
   (format t "4.9  ~5,3f~%" (nth 49 *overall-results*))
   (format t "5.0  ~5,3f~%" (nth 50 *overall-results*))
   (format t "5.01 ~5,3f~%" (nth 51 *overall-results*))
   (format t "5.1  ~5,3f~%" (nth 52 *overall-results*))
   (format t "5.2  ~5,3f~%" (nth 53 *overall-results*))
   (format t "5.3  ~5,3f~%" (nth 54 *overall-results*))
   (format t "5.4  ~5,3f~%" (nth 55 *overall-results*))
   (format t "5.5  ~5,3f~%" (nth 56 *overall-results*))
   (format t "5.6  ~5,3f~%" (nth 57 *overall-results*))
   (format t "5.7  ~5,3f~%" (nth 58 *overall-results*))
   (format t "5.8  ~5,3f~%" (nth 59 *overall-results*))
   (format t "5.9  ~5,3f~%" (nth 60 *overall-results*))
   (format t "6.0  ~5,3f~%" (nth 61 *overall-results*))
   (format t "6.1  ~5,3f~%" (nth 62 *overall-results*))
   (format t "6.2  ~5,3f~%" (nth 63 *overall-results*))
   (format t "6.3  ~5,3f~%" (nth 64 *overall-results*))
   (format t "6.4  ~5,3f~%" (nth 65 *overall-results*))
   (format t "6.5  ~5,3f~%" (nth 66 *overall-results*))
   (format t "6.6  ~5,3f~%" (nth 67 *overall-results*))
   (format t "6.7  ~5,3f~%" (nth 68 *overall-results*))
   (format t "6.8  ~5,3f~%" (nth 69 *overall-results*))
   (format t "6.9  ~5,3f~%" (nth 70 *overall-results*))
   (format t "7.0  ~5,3f~%" (nth 71 *overall-results*))
   (format t "7.1  ~5,3f~%" (nth 72 *overall-results*))
   (format t "7.2  ~5,3f~%" (nth 73 *overall-results*))
   (format t "7.3  ~5,3f~%" (nth 74 *overall-results*))
   (format t "7.4  ~5,3f~%" (nth 75 *overall-results*))
   (format t "7.5  ~5,3f~%" (nth 76 *overall-results*))
   (format t "7.6  ~5,3f~%" (nth 77 *overall-results*))
   (format t "7.7  ~5,3f~%" (nth 78 *overall-results*))
   (format t "7.8  ~5,3f~%" (nth 79 *overall-results*))
   (format t "7.9  ~5,3f~%" (nth 80 *overall-results*))
   (format t "8.0  ~5,3f~%" (nth 81 *overall-results*))
   (format t "8.1  ~5,3f~%" (nth 82 *overall-results*))
   (format t "8.2  ~5,3f~%" (nth 83 *overall-results*))
   (format t "8.3  ~5,3f~%" (nth 84 *overall-results*))
   (format t "8.4  ~5,3f~%" (nth 85 *overall-results*))
   (format t "8.5  ~5,3f~%" (nth 86 *overall-results*))
   (format t "8.6  ~5,3f~%" (nth 87 *overall-results*))
   (format t "8.7  ~5,3f~%" (nth 88 *overall-results*))
   (format t "8.8  ~5,3f~%" (nth 89 *overall-results*))
   (format t "8.9  ~5,3f~%" (nth 90 *overall-results*))
   (format t "9.0  ~5,3f~%" (nth 91 *overall-results*))
   (format t "9.1  ~5,3f~%" (nth 92 *overall-results*))
   (format t "9.2  ~5,3f~%" (nth 93 *overall-results*))
   (format t "9.3  ~5,3f~%" (nth 94 *overall-results*))
   (format t "9.4  ~5,3f~%" (nth 95 *overall-results*))
   (format t "9.5  ~5,3f~%" (nth 96 *overall-results*))
   (format t "9.6  ~5,3f~%" (nth 97 *overall-results*))
   (format t "9.7  ~5,3f~%" (nth 98 *overall-results*))
   (format t "9.8  ~5,3f~%" (nth 99 *overall-results*))
   (format t "9.9  ~5,3f~%" (nth 100 *overall-results*))
   (format t "10.0  ~5,3f~%" (nth 101 *overall-results*))
   )


(defun quick-lockhart (i j)
   (loop for n from 1 to 1000000
     with correct = 0
     with errors = 0
     for decay-rate1 = (exp (* *standard-deviation-time* 
                               (find-random-deviation)))
     for decay-rate2 = (exp (* *standard-deviation-time* 
                               (find-random-deviation)))
     for recency1 = (update-activation 1 i decay-rate1)
     for recency2 = (update-activation 1 j decay-rate2)
     if (> recency1 recency2) do (incf correct)
     else if (> recency2 recency1) do (incf errors)
        else if (= (random 2) 0) do (incf correct)
        else do (incf errors)
        finally (progn (format t "Correct = ~a, Errors = ~a~%~%" correct errors)
                       (setf *overall-results*
                             (append *overall-results* (list (/ correct (+ correct errors))))))))

; ------------------------------------------------------------------------------------------

;                  QUICK PAIRED ASSOCIATE SIMULATIONS

; ------------------------------------------------------------------------------------------

(defvar *total-errors* 0)
(defvar *track-intrusions*)
(defvar *errors-from-1-to-10* 0)
(defvar *errors-from-11-to-20* 0)
(defvar *errors-from-21-to-30* 0)
(defvar *one-to-ten-errors*)
(defvar *eleven-to-twenty-errors*)
(defvar *twenty-one-to-thirty-errors*)
(defvar *one-to-ten-mmfr-correct-responses*)
(defvar *moved-one-to-ten-mmfr-correct-responses*)
(defvar *one-to-ten-mmfr-incorrect-responses*)
(defvar *eleven-to-twenty-mmfr-correct-responses*)
(defvar *moved-eleven-to-twenty-mmfr-correct-responses*)
(defvar *eleven-to-twenty-mmfr-incorrect-responses*)
(defvar *r0-correct*)
(defvar *r1-correct*)
(defvar *r2-correct*)
(defvar *r3-correct*)
(defvar *total-backward-errors*)
(defvar *total-nodes-in-net* 0)
(defvar *pre-criterion-success-array*) ; Holds pre-criterion successes
(defvar *pre-criterion-failure-array*) ; Holds pre-criterion errors
(defvar *response-record*)
(defvar *actual-response-record*)
(defvar *probabilities-record*)
(defvar *backward-recall-trial*)
(defvar *mmfr-recall-trial*)
(defvar *mfr-recall-trial*)
 (defvar *pa-exit-criteria*)         ; absolute-trials-criteria or consecutive-trials-criteria -- 
                                    ; absolute means do a certain number
                                    ; of trials without regard to success.  consecutive means do
                                    ; until a certain number of consecutive perfect trials. 
(defvar *pa-exit-value*)            ; Number of trials that must occur meeting criteria before 
                                    ;

(defvar *consecutive-trials-correct*)
(defvar *track-response-latencies*)
(defvar *track-response-frequencies*)
(defvar *latency-record*)
(defvar *latency-array*)
(defvar *latency-array-when-correct*)
(defvar *latency-array-when-incorrect*)
(defvar *trials-array*)
(defvar *trials-array-when-correct*)
(defvar *trials-array-when-incorrect*)
(defvar *last-error-trials* 0)
(defvar *latency-for-last-error* 0)

(defvar *do-recognition-test*)
(defvar *recognition-errors* 0)
(defvar *recognition-record* nil)
(defvar *hit-when-correct*)
(defvar *false-alarm-when-correct*)
(defvar *hit-when-incorrect*)
(defvar *false-alarm-when-incorrect*)
(defvar *incorrect-rejection-when-correct*)
(defvar *correct-rejection-when-correct*)
(defvar *incorrect-rejection-when-incorrect*)
(defvar *correct-rejection-when-incorrect*)
(defvar *lowest-recognition-trial* 1)
(defvar *highest-recognition-trial* 4)
(defvar *lowest-extra-recalls-trial* 1)
(defvar *highest-extra-recalls-trial* 4)
(defvar *do-extra-recalls-test* )
(defvar *second-recall-correct*)
(defvar *second-recall-incorrect*)
(defvar *third-recall-correct*)
(defvar *third-recall-incorrect*)
(defvar *index*)
(defvar *indexes*)
(defvar *number-of-pairs*)
(defvar *trials-on-list*)
(defvar *errors-on-list*)

; For data -- load vrblvars.lsp

; Example (quick-rote-learning-simulation *ab*) or (quick-rote-learning-simulation (*low-low*)
; or (quick-rote-learning-simualation *low-high*) The purpose is simply to ascertain that 
; chunking takes about 6 seconds per chunk and it doesn't really matter if the chunks are 
; simialar to each other or not.

(defun quick-rote-learning-simulation (list-of-items)
  (initialize-variables)
  (zero-out-variables)
  (let ((objects (loop for item in list-of-items
                        collect (change-string-to-object item 'syllable))))
    (loop while (loop for object in objects
                      for result = (study object)
                      with still-more-to-learn = nil
                      if result do (setf still-more-to-learn t)
                      finally (return still-more-to-learn)))
    (format t "~a words learned in ~a seconds -- ~a per word -- ~a nodes-in-net~%" 
            (length list-of-items) (/ *epam-clock* 1000.0)
            (/ (/ *epam-clock* 1000.0) (length list-of-items))
            *nodes-in-net*)))





(defun quick-paired-associate-session (pairs)
   "This routine goes through the *exit-criteria* occurs."
   (setf *session-clock* 0)
   (setf *epam-clock* 0)
   (setf *total-clock* (+ *total-clock* *clock*))
   (setf *clock* 0)
   (setf *trial* -1)
   (setf *consecutive-trials-correct* 0)
   (setf (aref *response-record* *index*) nil)
   (setf (aref *actual-response-record* *index*) nil)
   (setf (aref *recognition-record* *index*) nil)
   (setf (aref *latency-record* *index*) nil)
   (setf *pairs* pairs)
   (when (> *trial* 200) (break))
   (loop while (apply *pa-exit-criteria* nil)
     with errors = t
     do (incf *trial*)
     if (> *epam-clock* *clock*)
     do 
     (setf *epam-clock* (- *epam-clock* *clock*))
     else if t
     do (setf *epam-clock* 0)
     do (setf *total-clock* (+ *total-clock* *clock*))
     (setf *clock* 0)
     (when (aref *track-response-frequencies* *trial*)
        (setf (aref *actual-response-record* *index*) nil))
     ; The above command allows collection of data for probabilities matching 
     ; experiment.  The problem is that the first trial is not counted.
     ; This allows the *actual-response-record* to have data from one trial
     (setf errors (quick-paired-associate-trial (reorder-list-randomly pairs)))
     (when (aref *track-response-frequencies* *trial*)
        (update-response-frequencies-record))
     ; The above command collects data for probabilities matching experiment.
     (setf *session-clock* (+ *session-clock* *clock*))
     if errors do (setf *consecutive-trials-correct* 0)
     else if t do (incf *consecutive-trials-correct*)
     finally (progn (update-response-record)
               (when (aref *do-recognition-test* *index*)
                  (update-recognition-record))
               (when (aref *track-response-latencies* *index*)
                  (update-response-latencies))
               (when (aref *mmfr-recall-trial* *index*)
                  (mmfr-trial pairs))
               (when (aref *mfr-recall-trial* *index*)
                  (mfr-trial pairs))
               (when (aref *backward-recall-trial* *index*) 
                  (quick-paired-associate-trial-backward pairs)))))

(defun quick-paired-associate-trial (pairs)
  "This routine goes through the list one time and returns t if an error was made,
 nil if not.  Note: This routine assumes that the *trial* number is available with the 
first trial being 0."
  (setf *trial-clock* *clock*)
  (setf *pairs* pairs)
  (when (> *trial* 200) (break))
  (when *print-experimenter-updates*
    (format t "~%Beginning Trial ~a~%" (1+ *trial*)))
  (loop with error = nil
        with response-object
        with correct
        for n from 0
        for pair in (reorder-list-randomly pairs)
        for stimulus-object = (copy-object (first pair))
        for correct-response-object = (copy-object (second pair))
        do 
        (when (< *clock* *trial-clock*)
          (setf *clock* *trial-clock*))
        (setf *trial-clock* (+ *trial-clock* *presentation-rate*))
        (when *print-experimenter-updates*
          (format t "~%~a Presenting stimulus = ~a~%" *clock* stimulus-object))
        (base-time-tally-to-find-associate)
        (setf response-object (find-response-given-stimulus stimulus-object 'episode))
        (setf correct 
              (perfect-subobject-match? response-object 
                                        correct-response-object))
        (when (not correct) (setf error t))
        (setf *response-latency* (- (+ *clock* *presentation-rate*)
                                    *trial-clock*))
        (record-info-from-paired-associate-trial correct 
                                                 response-object
                                                 pair
                                                 correct-response-object)
        (study-paired-association 'episode stimulus-object correct-response-object 
                                  nil nil nil correct)
        finally (progn (setf *clock* (+ *trial-clock* *inter-trial-interval*))
                       (return error))))



(defun record-info-from-paired-associate-trial (correct 
                                                subjects-response
                                                pair
                                                correct-response-object)
  (let ((response-latency *response-latency*))
    (when (null subjects-response)
      (setf response-latency *stimulus-duration-rate*))
    (when *print-experimenter-updates* 
      (format t "Response latency = ~a~%" response-latency))
    (put-response-on-alist-record subjects-response
                            (first pair)
                            correct)
    (when *print-experimenter-updates*
      (format t "~a Subject's response = ~a~%" *clock* subjects-response)
      (format t "Correct response = ~a~%" correct-response-object)
      (cond (correct (format t "Experimenter says response was correct!~%"))
            ('else (format t "Experimenter says response was wrong!~%"))))
    (when (aref *track-response-latencies* *index*)
      (put-value-on-latency-record (first pair) response-latency))
    (when (aref *do-recognition-test* *index*)
      (recognition-test (first pair) correct-response-object))
    (when (not correct)
      (incf *total-errors*)
      (incf (aref *qp-errors-array* *trial* *index*))
      (when (numberp subjects-response)
        (when (and (> subjects-response 0) (< subjects-response 11))
          (incf *errors-from-1-to-10*))
        (when (and (> subjects-response 10) (< subjects-response 21))
          (incf *errors-from-11-to-20*))
        (when (and (> subjects-response 20) (< subjects-response 31))
          (incf *errors-from-21-to-30*)))
      (when (and (aref *do-extra-recalls-test* *index*)
                 (> (+ *trial* 2) *lowest-extra-recalls-trial*)
                 (< *trial* *highest-extra-recalls-trial*))
        (extra-recalls-test subjects-response 
                            (first pair)
                            correct-response-object)))))





(defun quick-paired-associate-trial-backward (pairs)
  (setf *trial-clock* *clock*)
  (setf *pairs* pairs)
  (loop with error = nil
        with subjects-response
        with correct
        for n from 0
        for pair in (reorder-list-randomly pairs)
        for stimulus = (copy-object (second pair))
        for correct-response-object = (copy-object (first pair))
        do 
        (when (< *clock* *trial-clock*)
          (setf *clock* *trial-clock*))
        (setf *trial-clock* (+ *trial-clock* *inter-trial-interval*))
        (when *print-experimenter-updates*
          (format t "~%~a Presenting stimulus = ~a~%" *clock* stimulus))
        (setf subjects-response 
              (find-associate 'episode stimulus 'before))
        (setf correct 
              (perfect-subobject-match? subjects-response 
                                        correct-response-object))
        if (not correct)
        do (setf error t)
        (incf (aref *total-backward-errors* *index*))
        if *print-experimenter-updates*
        do (format t "~a Subject's response = ~a~%" *clock* subjects-response)
        (format t "Correct response = ~a~%" correct-response-object)
        (cond (correct (format t "Experimenter says response was correct!~%"))
              ('else (format t "Experimenter says response was wrong!~%")))
        finally (return error)))

(defvar *time-leeway-parameter* 1.75) ; this parameter determines the leeway 
                                      ; given in an mfr-trial to determine 
                                      ; whether an item is new or not.

(defun mfr-trial (pairs)
   "This routine goes through the list one time and collects infor for an mfr trial
in which subject is directed to preport the first response that comes into his mind.
This trile is like that ued in the Briggs, 1954, experiment."
   (loop 
     for pair in (reorder-list-randomly pairs)
     for stimulus = (copy-object (first pair))
     for correct-response-object = (copy-object (second pair))
     for response = (specify (find-associate 'episode stimulus))
     if (or (not (numberp correct-response-object))
            (< correct-response-object 1)
            (> correct-response-object 20))
     do (format t "Error!  MFR trial only works if response is a number.~%")
     (format t "First list must be associated with numbers between 1 and 10~%")
     (format t "Second list must be associated with numbers between 11 and 20~%")       
     (break) 
     do 
    (when *print-experimenter-updates*
       (format t "Stimulus = ~a, Correct-response = ~a, response = ~a~%"
         stimulus
         correct-response-object
         response))
    if (eql response correct-response-object)
    do (incf (aref *r2-correct* *index*))
    else if (eql response (- correct-response-object 10))
    do (incf (aref *r1-correct* *index*))
    else if (and (numberp response)
                     (> response 0)
                     (< response 21))
    do (incf (aref *r3-correct* *index*))
    else if t
    do (incf (aref *r0-correct* *index*))))


(defun mmfr-trial (pairs)
  "This routine goes through the list one time and collects info for an mmfr trial."
  (loop 
    with old-response
    with new-response
    for pair in (reorder-list-randomly pairs)
    for stimulus = (copy-object (first pair))
    for correct-response-object = (copy-object (second pair))
    for responses = (mapcar 'specify (find-associates 'episode stimulus))
    if (or (not (numberp correct-response-object))
           (< correct-response-object 1)
           (> correct-response-object 20))
    do (format t "Error!  MMFR trial only works if response is a number.~%")
    (format t "First list must be associated with numbers between 1 and 10~%")
    (format t "Second list must be associated with numbers between 11 and 20~%")       
    (break) 
    do 
    (setf new-response nil)
    (setf old-response nil)
    (loop 
         for response in responses
         for recency = (* (find-subjective-time-elapsed response)
                          (exp (* *standard-deviation-time* 
                                  (find-random-deviation))))
         for is-it-new? = (< recency (* *time-leeway-parameter* (+ *session-clock* *clock*)))
         if (and is-it-new?
                 (null new-response))
         do (setf new-response response)
         else if (null old-response)
         do (setf old-response response))
    ; (format t "responses=~a, new-response=~a, old-response=~a~%" responses new-response old-response)
    (when *print-experimenter-updates*
       (format t "Stimulus = ~a, Correct-response = ~a, new-response = ~a, old-response=~a~%"
         stimulus
         correct-response-object
         new-response
         old-response))
    if (eql new-response correct-response-object)
    do (incf (aref *eleven-to-twenty-mmfr-correct-responses* *index*))
    if (eql old-response (- correct-response-object 10))
    do (incf (aref *one-to-ten-mmfr-correct-responses* *index*))
    if (eql old-response correct-response-object)
    do (incf (aref *moved-eleven-to-twenty-mmfr-correct-responses* *index*))
    if (eql new-response (- correct-response-object 10))
    do (incf (aref *moved-one-to-ten-mmfr-correct-responses* *index*))
    if (and (numberp new-response)
            (< new-response 11)
            (not (and (numberp old-response)
                      (< old-response 11))))
    do (incf (aref *one-to-ten-mmfr-incorrect-responses* *index*))
    if (and (numberp old-response)
            (> old-response 10)
            (not (and (numberp new-response)
                      (> new-response 10))))
    do (incf (aref *eleven-to-twenty-mmfr-incorrect-responses* *index*))
    if (and (numberp new-response)
            (> new-response 10)
            (not (eql new-response correct-response-object)))
    do (incf (aref *eleven-to-twenty-mmfr-incorrect-responses* *index*))
    if (and (numberp old-response)
            (< old-response 11)
            (not (= old-response (- correct-response-object 10))))
    do (incf (aref *one-to-ten-mmfr-incorrect-responses* *index*))
    if (or (eql new-response correct-response-object)
           (eql old-response correct-response-object))
    do (incf (aref *r2-correct* *index*))
    else if (or (eql new-response (- correct-response-object 10))
                (eql old-response (- correct-response-object 10)))
    do (incf (aref *r1-correct* *index*))
    else if (or (and (numberp new-response)
                     (> new-response 0)
                     (< new-response 21))
                (and (numberp old-response)
                     (> old-response 0)
                     (< old-response 21)))
    do (incf (aref *r3-correct* *index*))
    else if t
    do (incf (aref *r0-correct* *index*))))


(defun consecutive-trials-criteria ()
  (< *consecutive-trials-correct* *pa-exit-value*))

(defun absolute-trials-criteria ()
  (< (1+ *trial*) *pa-exit-value*))

(defun quick-intralist-similarity-simulation ()
  (setf *total-trials-by-list* nil)
  (seed-random-generator)
  (format t "Quick Intralist Similarity Simulation:")
  (setf *syllable-lists* (list *low-low* 
                               *low-medium*
                               *low-high*
                               *medium-low*
                               *high-low*))
  (setf *names-of-syllable-lists* '(*low-low*
                                    *low-medium* 
                                    *low-high*
                                    *medium-low*
                                    *high-low*))
  (setf *print-major-updates* t)
  (setf *presentation-rate* 4000)
  (setf *inter-trial-interval* 4000)
  (setf *number-of-sessions* 1000)
  (setf *guessing-routine* nil)
  (setf *pa-exit-criteria* 'consecutive-trials-criteria)
  (setf *pa-exit-value* 1)
  (reset-data-collection-arrays)
  (setf *noticing-order* nil)
  (simple-paired-associate-experiment)
  (format t "Mean Trials to Learn as a Function of Stimulus and Response Similarity:~%")
  (format t "Results for people from Underwood, 1953, Journal of Experimental Psychology, 45, 133-142:~%")
  (format t "Results for EPAM III from Simon & Feigenbaum, 1964, Models of Thought 3.2~%")
  (format t "Condition      People       EPAM3         EPAM6~%")
  (format t "           --------------  --------   --------------~%")
  (format t "Low-Low      23.2  100       100       ~5,1f ~5,0f~%"
          (nth 0 *total-trials-by-list*)
          (* 100 (/ (nth 0 *total-trials-by-list*) (nth 0 *total-trials-by-list*))))
  (format t "Low-Medium   22.4   96        88       ~5,1f ~5,0f~%"
          (nth 1 *total-trials-by-list*)
          (* 100 (/ (nth 1 *total-trials-by-list*) (nth 0 *total-trials-by-list*))))
  (format t "Low-High     24.4  105        91       ~5,1f ~5,0f~%"
          (nth 2 *total-trials-by-list*)
          (* 100 (/ (nth 2 *total-trials-by-list*) (nth 0 *total-trials-by-list*))))
  (format t "Medium-Low   25.5  110       140       ~5,1f ~5,0f~%"
          (nth 3 *total-trials-by-list*)
          (* 100 (/ (nth 3 *total-trials-by-list*) (nth 0 *total-trials-by-list*))))
  (format t "High-Low     30.7  132       146       ~5,1f ~5,0f~%"
          (nth 4 *total-trials-by-list*)
          (* 100 (/ (nth 4 *total-trials-by-list*) (nth 0 *total-trials-by-list*))))
  )

(defun simple-paired-associate-experiment ()
  "This routine conducts an experiment in which each subject is just given 
a single list"
  (setf *indexes* '(1))
  (loop for list-or-routine in *syllable-lists*
        for list-name in *names-of-syllable-lists*
        do 
        (initialize-results-arrays)
        (format t "~%~%BEGINNING SIMULATION FOR LIST: ~a:~%" list-name)
        (loop repeat *number-of-sessions*
              for syllable-list = (cond ((listp list-or-routine) list-or-routine)
                                        ('else (apply list-or-routine nil)))
              do (multi-list-session (list syllable-list)
                                     (list *pa-exit-criteria*)
                                     (list *pa-exit-value*)
                                     '(0)
                                     '(1)))
        (print-paired-associate-results)))
  
(defun print-paired-associate-results ()
   "This routine prints out all of the results that have been collected in a 
 paired-associate simulation."
   (print-out-errors-by-list)
   (print-out-total-trials-by-list)
   (print-out-proportion-correct-by-list-and-trial)
   (print-out-precriterion-proportion-correct-by-list-and-trial)
   (print-out-response-latencies)
   (print-out-backward-response-record)
   (print-out-results-of-extra-recalls-tests)
   (print-out-results-of-recognition-test)
   (print-out-intrusions-record)
   (print-out-mmfr-response-record)
   (print-out-mfr-response-record))

(defun initialize-results-arrays ()
  (setf *number-of-pairs* (make-array 50 :initial-element 0))
  (setf *response-record* (make-array 50 :initial-element nil))
  (setf *actual-response-record* (make-array 50 :initial-element nil))
  (setf *recognition-record* (make-array 50 :initial-element nil))
  (setf *latency-record* (make-array 50 :initial-element nil))
  (setf *total-backward-errors* (make-array 50 :initial-element 0))
  (setf *qp-errors-array* (make-array '(300 50) :initial-element 0))
  (setf *pre-criterion-success-array* (make-array '(300 50) :initial-element 0))
  (setf *pre-criterion-failure-array* (make-array '(300 50) :initial-element 0))
  (setf *errors-on-list* (make-array 50 :initial-element 0))
  (setf *trials-on-list* (make-array 50 :initial-element 0))
  (setf *hit-when-correct* (make-array 50 :initial-element 0))
  (setf *false-alarm-when-correct* (make-array 50 :initial-element 0))
  (setf *hit-when-incorrect* (make-array 50 :initial-element 0))
  (setf *false-alarm-when-incorrect* (make-array 50 :initial-element 0))
  (setf *incorrect-rejection-when-correct* (make-array 50 :initial-element 0))
  (setf *correct-rejection-when-correct* (make-array 50 :initial-element 0))
  (setf *incorrect-rejection-when-incorrect* (make-array 50 :initial-element 0))
  (setf *correct-rejection-when-incorrect* (make-array 50 :initial-element 0))
  (setf *second-recall-correct* (make-array '(200 50) :initial-element 0))
  (setf *second-recall-incorrect* (make-array '(200 50):initial-element 0))
  (setf *third-recall-correct* (make-array '(200 50):initial-element 0))
  (setf *third-recall-incorrect* (make-array '(200 50):initial-element 0))
  (setf *r0-correct* (make-array 50 :initial-element 0))
  (setf *r1-correct* (make-array 50 :initial-element 0))
  (setf *r2-correct* (make-array 50 :initial-element 0))
  (setf *r3-correct* (make-array 50 :initial-element 0))
  (setf *one-to-ten-errors* (make-array 50 :initial-element 0))
  (setf *eleven-to-twenty-errors* (make-array 50 :initial-element 0))
  (setf *twenty-one-to-thirty-errors* (make-array 50 :initial-element 0))
  (setf *one-to-ten-mmfr-correct-responses* (make-array 50 :initial-element 0))
  (setf *moved-one-to-ten-mmfr-correct-responses* (make-array 50 :initial-element 0))
  (setf *one-to-ten-mmfr-incorrect-responses* (make-array 50 :initial-element 0))
  (setf *eleven-to-twenty-mmfr-correct-responses* (make-array 50 :initial-element 0))
  (setf *moved-eleven-to-twenty-mmfr-correct-responses* (make-array 50 :initial-element 0))
  (setf *eleven-to-twenty-mmfr-incorrect-responses* (make-array 50 :initial-element 0))
  (setf *latency-array* (make-array '(300 50) :initial-element 0))
  (setf *trials-array* (make-array '(300 50) :initial-element 0))
  (setf *latency-array-when-correct* (make-array '(300 50) :initial-element 0))
  (setf *trials-array-when-correct* (make-array '(300 50) :initial-element 0))
  (setf *latency-array-when-incorrect* (make-array '(300 50) :initial-element 0))
  (setf *trials-array-when-incorrect* (make-array '(300 50) :initial-element 0))
  (setf *last-error-trials* (make-array 50 :initial-element 0))
  (setf *latency-for-last-error* (make-array 50 :initial-element 0)))

(defun reset-data-collection-arrays ()
  (setf *backward-recall-trial* (make-array 50 :initial-element nil))
  (setf *mmfr-recall-trial* (make-array 50 :initial-element nil))
  (setf *mfr-recall-trial* (make-array 50 :initial-element nil))
  (setf *guessing-routine* nil)
  (setf *do-recognition-test* (make-array 50 :initial-element nil))
  (setf *do-extra-recalls-test* (make-array 50 :initial-element nil))
  (setf *track-intrusions* (make-array 50 :initial-element nil))
  (setf *track-response-latencies* (make-array 50 :initial-element nil))
  (setf *track-response-frequencies* (make-array 201 :initial-element nil)))

(defun hintzman-experiment1 ()
  "This is Expermiment 1 reported in Hintzman's article about SAL.  See the
Journal of Mathematical Psychology, 5, 123-162, 1968."
  (format t "~%Hintzman Experiment 1. Intralist Stimulus Similarity:")
  (seed-random-generator)
  (setf *total-trials-by-list* nil)
  (reset-data-collection-arrays)
  (setf *noticing-order* nil)
  (setf *syllable-lists* '(make-sal-low make-sal-medium make-sal-high))
  (setf *names-of-syllable-lists* '(low-similarity medium-similarity high-similarity))
  (setf *print-major-updates* t)
  (setf *presentation-rate* 4000)
  (setf *inter-trial-interval* 4000)
  (setf *number-of-sessions* 1000)
  (setf *guessing-routine* 'pick-a-number-between-1-and-8)
  (setf *pa-exit-criteria* 'consecutive-trials-criteria)
  (setf *pa-exit-value* 2)
  (simple-paired-associate-experiment)
  (format t "Mean Trials to Learn as a Function of Stimulus Similarity:~%")
  (format t "Condition          SALI      EPAM6~%")
  (format t "                 ---------  ---------~%")
  (format t "Low-similarity      4.60     ~5,2f~%"
          (nth 0 *total-trials-by-list*))
  (format t "Medium-similarity   5.50     ~5,2f~%"
          (nth 1 *total-trials-by-list*))
  (format t "High-similarity     6.75     ~5,2f~%"
          (nth 2 *total-trials-by-list*))
  )


(defun hintzman-experiment2 ()
  (format t "~%Hintzman Experiment 2. Number of Response Alternatives:")
  (seed-random-generator)
  (setf *proportion-correct-by-trial* nil)
  (setf *precriterion-proportion-correct-by-trial* nil)
  (reset-data-collection-arrays)
  (setf *noticing-order* nil)
  (setf *print-major-updates* t)
  (setf *presentation-rate* 4000)
  (setf *inter-trial-interval* 4000)
  (setf *number-of-sessions* 1000)
  (setf *pa-exit-criteria* 'consecutive-trials-criteria)
  (setf *pa-exit-value* 1)
  (setf *syllable-lists* (list *sal-9-R*))
  (setf *names-of-syllable-lists* '(sal-9-R))
  (setf *guessing-routine* 'pick-a-number-between-1-and-9)
  (simple-paired-associate-experiment)
  (setf *syllable-lists* (list *sal-2-R*))
  (setf *names-of-syllable-lists* '(sal-2-r))
  (setf *guessing-routine* 'pick-a-number-between-1-and-2)
  (simple-paired-associate-experiment)
  (format t "~%Learning Curves for 9-Response lists, Blocks of 3 trials.~%")
  (format t "Human data from Hintzman, 1967, estimated from dissertation graph~%")
  (format t "Blocks of Trials       People             EPAM6~%")
  (format t "                   ----------------  ----------------~%")
  (format t "                   Correct Pre-crit  Correct Pre-crit~%")
  (format t "                   ----------------  ----------------~%")
  (format t "     1-3            0.12    0.10      ~5,2f   ~5,2f~%"
          (when (and (nth 0 (first *proportion-correct-by-trial*))
                     (nth 1 (first *proportion-correct-by-trial*))
                     (nth 2 (first *proportion-correct-by-trial*)))
            (/ (+ (nth 0 (first *proportion-correct-by-trial*))
                  (nth 1 (first *proportion-correct-by-trial*))
                  (nth 2 (first *proportion-correct-by-trial*)))
               3))
          (when (and (nth 0 (first *precriterion-proportion-correct-by-trial*))
                     (nth 1 (first *precriterion-proportion-correct-by-trial*))
                     (nth 2 (first *precriterion-proportion-correct-by-trial*)))
            (/ (+ (nth 0 (first *precriterion-proportion-correct-by-trial*))
                  (nth 1 (first *precriterion-proportion-correct-by-trial*))
                  (nth 2 (first *precriterion-proportion-correct-by-trial*)))
               3)))
  (format t "     4-6            0.26    0.18      ~5,2f   ~5,2f~%"    
          (when (and (nth 3 (first *proportion-correct-by-trial*))
                     (nth 4 (first *proportion-correct-by-trial*))
                     (nth 5 (first *proportion-correct-by-trial*)))
            (/ (+ (nth 3 (first *proportion-correct-by-trial*))
                  (nth 4 (first *proportion-correct-by-trial*))
                  (nth 5 (first *proportion-correct-by-trial*)))
               3))
          (when (and (nth 3 (first *precriterion-proportion-correct-by-trial*))
                     (nth 4 (first *precriterion-proportion-correct-by-trial*))
                     (nth 5 (first *precriterion-proportion-correct-by-trial*)))
            (/ (+ (nth 3 (first *precriterion-proportion-correct-by-trial*))
                  (nth 4 (first *precriterion-proportion-correct-by-trial*))
                  (nth 5 (first *precriterion-proportion-correct-by-trial*)))
               3)))
  (format t "     7-9            0.42    0.26      ~5,2f   ~5,2f~%"    
          (when (and (nth 6 (first *proportion-correct-by-trial*))
                     (nth 7 (first *proportion-correct-by-trial*))
                     (nth 8 (first *proportion-correct-by-trial*)))
            (/ (+ (nth 6 (first *proportion-correct-by-trial*))
                  (nth 7 (first *proportion-correct-by-trial*))
                  (nth 8 (first *proportion-correct-by-trial*)))
               3))
          (when (and (nth 6 (first *precriterion-proportion-correct-by-trial*))
                     (nth 7 (first *precriterion-proportion-correct-by-trial*))
                     (nth 8 (first *precriterion-proportion-correct-by-trial*)))
            (/ (+ (nth 6 (first *precriterion-proportion-correct-by-trial*))
                  (nth 7 (first *precriterion-proportion-correct-by-trial*))
                  (nth 8 (first *precriterion-proportion-correct-by-trial*)))
               3)))
  (format t "    10-12           0.62    0.38      ~5,2f   ~5,2f~%"    
          (when (and (nth 9 (first *proportion-correct-by-trial*))
                     (nth 10 (first *proportion-correct-by-trial*))
                     (nth 11 (first *proportion-correct-by-trial*)))
            (/ (+ (nth 9 (first *proportion-correct-by-trial*))
                  (nth 10 (first *proportion-correct-by-trial*))
                  (nth 11 (first *proportion-correct-by-trial*)))
               3))
          (when (and (nth 9 (first *precriterion-proportion-correct-by-trial*))
                     (nth 10 (first *precriterion-proportion-correct-by-trial*))
                     (nth 11 (first *precriterion-proportion-correct-by-trial*)))
            (/ (+ (nth 9 (first *precriterion-proportion-correct-by-trial*))
                  (nth 10 (first *precriterion-proportion-correct-by-trial*))
                  (nth 11 (first *precriterion-proportion-correct-by-trial*)))
               3)))
  (format t "    13-15           0.72    0.41      ~5,2f   ~5,2f~%"    
          (when (and (nth 12 (first *proportion-correct-by-trial*))
                     (nth 13 (first *proportion-correct-by-trial*))
                     (nth 14 (first *proportion-correct-by-trial*)))
            (/ (+ (nth 12 (first *proportion-correct-by-trial*))
                  (nth 13 (first *proportion-correct-by-trial*))
                  (nth 14 (first *proportion-correct-by-trial*)))
               3))
          (when (and (nth 12 (first *precriterion-proportion-correct-by-trial*))
                     (nth 13 (first *precriterion-proportion-correct-by-trial*))
                     (nth 14 (first *precriterion-proportion-correct-by-trial*)))
            (/ (+ (nth 12 (first *precriterion-proportion-correct-by-trial*))
                  (nth 13 (first *precriterion-proportion-correct-by-trial*))
                  (nth 14 (first *precriterion-proportion-correct-by-trial*)))
               3)))
  (format t "    16-18           0.77    0.30      ~5,2f   ~5,2f~%"
          (when (and (nth 15 (first *proportion-correct-by-trial*))
                     (nth 16 (first *proportion-correct-by-trial*))
                     (nth 17 (first *proportion-correct-by-trial*)))
            (/ (+ (nth 15 (first *proportion-correct-by-trial*))
                  (nth 16 (first *proportion-correct-by-trial*))
                  (nth 17 (first *proportion-correct-by-trial*)))
               3))
          (when (and (nth 15 (first *precriterion-proportion-correct-by-trial*))
                     (nth 16 (first *precriterion-proportion-correct-by-trial*))
                     (nth 17 (first *precriterion-proportion-correct-by-trial*)))
            (/ (+ (nth 15 (first *precriterion-proportion-correct-by-trial*))
                  (nth 16 (first *precriterion-proportion-correct-by-trial*))
                  (nth 17 (first *precriterion-proportion-correct-by-trial*)))
               3)))
  (format t "~%Learning Curves for 2-Response lists for Successive Blocks of 3 trials.~%")
  (format t "Human data is from Hintzman, 1967, estimated from his dissertation graph~%")
  (format t "Blocks of Trials       People             EPAM6~%")
  (format t "                   ----------------  ----------------~%")
  (format t "                   Correct Pre-crit  Correct Pre-crit~%")
  (format t "                   ----------------  ----------------~%")
  (format t "     1-3            0.47    0.44      ~5,2f   ~5,2f~%"
          (when (and (nth 0 (second *proportion-correct-by-trial*))
                     (nth 1 (second *proportion-correct-by-trial*))
                     (nth 2 (second *proportion-correct-by-trial*)))
            (/ (+ (nth 0 (second *proportion-correct-by-trial*))
                  (nth 1 (second *proportion-correct-by-trial*))
                  (nth 2 (second *proportion-correct-by-trial*)))
               3))
          (when (and (nth 0 (second *precriterion-proportion-correct-by-trial*))
                     (nth 1 (second *precriterion-proportion-correct-by-trial*))
                     (nth 2 (second *precriterion-proportion-correct-by-trial*)))
            (/ (+ (nth 0 (second *precriterion-proportion-correct-by-trial*))
                  (nth 1 (second *precriterion-proportion-correct-by-trial*))
                  (nth 2 (second *precriterion-proportion-correct-by-trial*)))
               3)))
  (format t "     4-6            0.50    0.45      ~5,2f   ~5,2f~%"    
          (when (and (nth 3 (second *proportion-correct-by-trial*))
                     (nth 4 (second *proportion-correct-by-trial*))
                     (nth 5 (second *proportion-correct-by-trial*)))
            (/ (+ (nth 3 (second *proportion-correct-by-trial*))
                  (nth 4 (second *proportion-correct-by-trial*))
                  (nth 5 (second *proportion-correct-by-trial*)))
               3))
          (when (and (nth 3 (second *precriterion-proportion-correct-by-trial*))
                     (nth 4 (second *precriterion-proportion-correct-by-trial*))
                     (nth 5 (second *precriterion-proportion-correct-by-trial*)))
            (/ (+ (nth 3 (second *precriterion-proportion-correct-by-trial*))
                  (nth 4 (second *precriterion-proportion-correct-by-trial*))
                  (nth 5 (second *precriterion-proportion-correct-by-trial*)))
               3)))
  (format t "     7-9            0.60    0.50      ~5,2f   ~5,2f~%"    
          (when (and (nth 6 (second *proportion-correct-by-trial*))
                     (nth 7 (second *proportion-correct-by-trial*))
                     (nth 8 (second *proportion-correct-by-trial*)))
            (/ (+ (nth 6 (second *proportion-correct-by-trial*))
                  (nth 7 (second *proportion-correct-by-trial*))
                  (nth 8 (second *proportion-correct-by-trial*)))
               3))
          (when (and (nth 6 (second *precriterion-proportion-correct-by-trial*))
                     (nth 7 (second *precriterion-proportion-correct-by-trial*))
                     (nth 8 (second *precriterion-proportion-correct-by-trial*)))
            (/ (+ (nth 6 (second *precriterion-proportion-correct-by-trial*))
                  (nth 7 (second *precriterion-proportion-correct-by-trial*))
                  (nth 8 (second *precriterion-proportion-correct-by-trial*)))
               3)))
  (format t "    10-12           0.67    0.51      ~5,2f   ~5,2f~%"    
          (when (and (nth 9 (second *proportion-correct-by-trial*))
                     (nth 10 (second *proportion-correct-by-trial*))
                     (nth 11 (second *proportion-correct-by-trial*)))
            (/ (+ (nth 9 (second *proportion-correct-by-trial*))
                  (nth 10 (second *proportion-correct-by-trial*))
                  (nth 11 (second *proportion-correct-by-trial*)))
               3))
          (when (and (nth 9 (second *precriterion-proportion-correct-by-trial*))
                     (nth 10 (second *precriterion-proportion-correct-by-trial*))
                     (nth 11 (second *precriterion-proportion-correct-by-trial*)))
            (/ (+ (nth 9 (second *precriterion-proportion-correct-by-trial*))
                  (nth 10 (second *precriterion-proportion-correct-by-trial*))
                  (nth 11 (second *precriterion-proportion-correct-by-trial*)))
               3)))
  (format t "    13-15           0.73    0.49      ~5,2f   ~5,2f~%"    
          (when (and (nth 12 (second *proportion-correct-by-trial*))
                     (nth 13 (second *proportion-correct-by-trial*))
                     (nth 14 (second *proportion-correct-by-trial*)))
            (/ (+ (nth 12 (second *proportion-correct-by-trial*))
                  (nth 13 (second *proportion-correct-by-trial*))
                  (nth 14 (second *proportion-correct-by-trial*)))
               3))
          (when (and (nth 12 (second *precriterion-proportion-correct-by-trial*))
                     (nth 13 (second *precriterion-proportion-correct-by-trial*))
                     (nth 14 (second *precriterion-proportion-correct-by-trial*)))
            (/ (+ (nth 12 (second *precriterion-proportion-correct-by-trial*))
                  (nth 13 (second *precriterion-proportion-correct-by-trial*))
                  (nth 14 (second *precriterion-proportion-correct-by-trial*)))
               3)))
  (format t "    16-18           0.82    0.51      ~5,2f   ~5,2f~%"
          (when (and (nth 15 (second *proportion-correct-by-trial*))
                     (nth 16 (second *proportion-correct-by-trial*))
                     (nth 17 (second *proportion-correct-by-trial*)))
            (/ (+ (nth 15 (second *proportion-correct-by-trial*))
                  (nth 16 (second *proportion-correct-by-trial*))
                  (nth 17 (second *proportion-correct-by-trial*)))
               3))
          (when (and (nth 15 (second *precriterion-proportion-correct-by-trial*))
                     (nth 16 (second *precriterion-proportion-correct-by-trial*))
                     (nth 17 (second *precriterion-proportion-correct-by-trial*)))
            (/ (+ (nth 15 (second *precriterion-proportion-correct-by-trial*))
                  (nth 16 (second *precriterion-proportion-correct-by-trial*))
                  (nth 17 (second *precriterion-proportion-correct-by-trial*)))
               3)))
  )


(defun pick-a-number-between-1-and-9 ()
  (1+ (random 9)))

(defun pick-a-number-between-1-and-8 ()
  (1+ (random 8)))

(defun pick-a-number-between-1-and-2 ()
  (1+ (random 2)))

(defun pick-a-number-between-1-and-3 ()
  (1+ (random 3)))

(defun pick-a-number-between-1-and-11 ()
  (1+ (random 11)))

(defun pick-a-number-between-1-and-14 ()
  (1+ (random 14)))




(defun put-response-on-alist-record (response stim correct?)
  "The response-record is an association list which indexes all of the responses
made to a particular stimulus.  If the response was correct, its value is t.  
Otherwise, it is the actual response made."
  (let ((current-values (second (assoc stim (aref *response-record* *index*) :test #'equal)))
        (new-value (or correct? response)))
    (cond ((null current-values) 
           (setf (aref *response-record* *index*)
                 (cons (list stim (list new-value)) 
                       (aref *response-record* *index*))))
          ('else 
           (nconc current-values (list new-value)))))
  (let ((current-values 
         (second (assoc stim (aref *actual-response-record* *index*) :test #'equal))))
    (cond ((null current-values) 
           (setf (aref *actual-response-record* *index*)
                 (cons (list stim (list response)) 
                       (aref *actual-response-record* *index*))))
          ('else 
           (nconc current-values (list response))))))

(defun update-response-record ()
  "Note this response display won't work if a possible response is t"
  (when *print-experimenter-updates*
    (format t "~%Responses to stimuli were the following: ~%"))
  (loop for alist in (aref *response-record* *index*) 
        for values = (second alist)
        do (when *print-experimenter-updates* (format t "~a:" (first alist)))
        (loop for value in values
              for n from 0
              do (when *print-experimenter-updates* (format t " ~a" value))
              finally (when *print-experimenter-updates* (format t "~%")))
        (loop for value in (pre-criterion values)
              for n from 0
              if (eql value t)
              do (incf (aref *pre-criterion-success-array* n *index*))
              else if t 
              do (incf (aref *pre-criterion-failure-array* n *index*)))))

(defun pre-criterion (lis)
  "This routine returns the list after cutting all t from the end"
  (loop for list-end on (reverse lis)
        if (not (eql (car list-end) t))
        return (reverse (cdr list-end))))

(defun hintzman-experiment3 ()
  (format t "~%Hintzman Experiment 3. Probability Learning:")
  (seed-random-generator)
  (reset-data-collection-arrays)
  (setf *noticing-order* nil)
  (setf *print-major-updates* t)
  (setf *presentation-rate* 4000)
  (setf *inter-trial-interval* 4000)
  (setf *number-of-sessions* 1000)
  (setf *pa-exit-criteria* 'absolute-trials-criteria)
  (setf *pa-exit-value* 7)
  (setf *syllable-lists* (list 'make-sal-probabilistic))
  (setf *names-of-syllable-lists* '(probability-matching-list))
  (setf *guessing-routine* 'pick-a-number-between-1-and-11)
  (setf *probabilities-record* (make-array 12 :initial-element 0))
  (loop for n from 2 upto 6
    do (setf (aref *track-response-frequencies* n) t))
  (simple-paired-associate-experiment)
  (loop for n from 1 upto 11
        do (format t "~a = ~a~%" n (aref *probabilities-record* n)))
  (format t "~%Input Probability and Response Proportion:~%")
  (format t "Input Prob   SAL I Resp Prop  EPAM6 Resp Prop~%")
  (format t "----------   ---------------  ---------------~%")
  (format t "   0.1            0.10            ~4,2f~%" 
    (* .01 (/ (aref *probabilities-record* 3) *number-of-sessions*)))
  (format t "   0.2            0.21            ~4,2f~%" 
    (* .01 (/ (aref *probabilities-record* 5) *number-of-sessions*)))
  (format t "   0.3            0.30            ~4,2f~%" 
    (* .01 (/ (aref *probabilities-record* 7) *number-of-sessions*)))
  (format t "   0.4            0.38            ~4,2f~%" 
    (* .01 (/ (aref *probabilities-record* 9) *number-of-sessions*)))
  (format t "   0.5            0.50            ~4,2f~%" 
    (* .005 (/ (+ (aref *probabilities-record* 10)
                  (aref *probabilities-record* 11))
               *number-of-sessions*)))
  (format t "   0.6            0 62            ~4,2f~%" 
    (* .01 (/ (aref *probabilities-record* 8) *number-of-sessions*)))
  (format t "   0.7            0.70            ~4,2f~%" 
    (* .01 (/ (aref *probabilities-record* 6) *number-of-sessions*)))
  (format t "   0.8            0.79            ~4,2f~%" 
    (* .01 (/ (aref *probabilities-record* 4) *number-of-sessions*)))
  (format t "   0.9            0.90            ~4,2f~%" 
    (* .01 (/ (aref *probabilities-record* 2) *number-of-sessions*)))
  (format t "   1.0            1.00            ~4,2f~%" 
    (* .01 (/ (aref *probabilities-record* 1) *number-of-sessions*))))

(defun hintzman-experiment3-with-different-parameter-settings ()
   "Beware, if this simulation is not run to completion, it will change the 
parameter settings for *sal-b* and *sal-c* which could screw
up the results of other simulations." 
   (format t "~%Hintzman Experiment 3. Probability Learning:")
   (let ((sal-c *sal-c*)
         (sal-b *sal-b*))
      (setf *sal-c* 100)
      (setf *sal-b* 25)
      (seed-random-generator)
      (reset-data-collection-arrays)
      (setf *noticing-order* nil)
      (setf *print-major-updates* t)
      (setf *presentation-rate* 4000)
      (setf *inter-trial-interval* 4000)
      (setf *number-of-sessions* 1000)
      (setf *pa-exit-criteria* 'absolute-trials-criteria)
      (setf *pa-exit-value* 7)
      (setf *syllable-lists* (list 'make-sal-probabilistic))
      (setf *names-of-syllable-lists* '(probability-matching-list))
      (setf *guessing-routine* 'pick-a-number-between-1-and-11)
      (setf *probabilities-record* (make-array 12 :initial-element 0))
      (loop for n from 2 upto 6
        do (setf (aref *track-response-frequencies* n) t))
      (simple-paired-associate-experiment)
      (loop for n from 1 upto 11
        do (format t "~a = ~a~%" n (aref *probabilities-record* n)))
      (format t "~%Input Probability and Response Proportion.  In this version parameters are reset to increase learning after correct and reduce learning after error:~%")
      (format t "Input Prob   SAL I Resp Prop  EPAM6 Resp Prop~%")
      (format t "----------   ---------------  ---------------~%")
      (format t "   0.1            0.10            ~4,2f~%" 
        (* .01 (/ (aref *probabilities-record* 3) *number-of-sessions*)))
      (format t "   0.2            0.21            ~4,2f~%" 
        (* .01 (/ (aref *probabilities-record* 5) *number-of-sessions*)))
      (format t "   0.3            0.30            ~4,2f~%" 
        (* .01 (/ (aref *probabilities-record* 7) *number-of-sessions*)))
      (format t "   0.4            0.38            ~4,2f~%" 
        (* .01 (/ (aref *probabilities-record* 9) *number-of-sessions*)))
      (format t "   0.5            0.50            ~4,2f~%" 
        (* .005 (/ (+ (aref *probabilities-record* 10)
                      (aref *probabilities-record* 11))
                   *number-of-sessions*)))
      (format t "   0.6            0 62            ~4,2f~%" 
        (* .01 (/ (aref *probabilities-record* 8) *number-of-sessions*)))
      (format t "   0.7            0.70            ~4,2f~%" 
        (* .01 (/ (aref *probabilities-record* 6) *number-of-sessions*)))
      (format t "   0.8            0.79            ~4,2f~%" 
        (* .01 (/ (aref *probabilities-record* 4) *number-of-sessions*)))
      (format t "   0.9            0.90            ~4,2f~%" 
        (* .01 (/ (aref *probabilities-record* 2) *number-of-sessions*)))
      (format t "   1.0            1.00            ~4,2f~%" 
        (* .01 (/ (aref *probabilities-record* 1) *number-of-sessions*)))
      (setf *sal-b* sal-b)
      (setf *sal-c* sal-c) 
      ))
   
   
   (defun update-response-frequencies-record ()
      (loop for pair in (aref *actual-response-record* *index*)
     for values = (second pair)
     do 
     (loop for value in values
       do (incf (aref *probabilities-record* value)))))
 


(defun hintzman-backward-recall-experiment ()
  (format t "~%Hintzman Backward Recall Experiment.")
  (setf *backward-response-record* nil)
  (seed-random-generator)
  (reset-data-collection-arrays)
  (setf *noticing-order* nil)
  (setf *pa-exit-value* 2)
  (setf *syllable-lists* '(make-sal-high-14 make-sal-low-14))
  (setf *names-of-syllable-lists* '(high-similarity low-similarity))
  (setf *print-major-updates* t)
  (setf *presentation-rate* 4000)
  (setf *inter-trial-interval* 4000)
  (setf *number-of-sessions* 1000)
  (setf *guessing-routine* 'pick-a-number-between-1-and-14)
  (setf *pa-exit-criteria* 'consecutive-trials-criteria)
  (setf *pa-exit-value* 2)
  (setf (aref *backward-recall-trial* 1) t)
  (simple-paired-associate-experiment)
  (format t "Results for Backward Recall experiment by Hintzman (1967) and reported in Chapter IV of his dissertation.~%")
  (format t "~%Percent Backward Recall of stimulus as function of intralist similarity.~%")
  (format t "Condition       People     EPAM6~%")
  (format t "High-Similarity    87.8     ~4,1f~%" (/ (- 14 (first *backward-response-record*)) .14))
  (format t "Low-Similarity   48.6     ~4,1f~%" (/ (- 14 (second *backward-response-record*)) .14))   
  )

(defun hintzman-experiment6 ()
   "This is Expermiment 6 reported in Hintzman's article about SAL.  See the
 Journal of Mathematical Psychology, 5, 123-162, 1968."
   (format t "~%Hintzman Experiment 1. List Length:")
   (seed-random-generator)
   (setf *errors-by-list* nil)
   (reset-data-collection-arrays)
   (setf *noticing-order* nil)
   (setf *syllable-lists* '(make-sal-medium make-sal-long))
   (setf *names-of-syllable-lists* '(short-list long-list))
   (setf *print-major-updates* t)
   (setf *presentation-rate* 4000)
   (setf *inter-trial-interval* 4000)
   (setf *number-of-sessions* 1000)
   (setf *guessing-routine* 'pick-a-number-between-1-and-8)
   (setf *pa-exit-criteria* 'consecutive-trials-criteria)
   (setf *pa-exit-value* 2)
   (simple-paired-associate-experiment)
   (format t "Errors per pair to Learn as a Function of List Length:~%")
   (format t "Condition          SALI      EPAM6~%")
   (format t "                 ---------  ---------~%")
   (format t "Short List          1.59     ~5,2f~%"
     (/ (nth 0 *errors-by-list*) 8))
   (format t "Long List           1.55     ~5,2f~%"
     (/ (nth 1 *errors-by-list*) 16))
   )


(defun hintzman-experiment7 ()
  (format t "~%Hintzman Experiment 7. Interlist Similarity and Retroactive Interference:")
  (setf *proportion-correct-by-trial* nil)
  (seed-random-generator)
  (reset-data-collection-arrays)
  (setf *noticing-order* nil)
  (setf *names-of-syllable-lists* '(list1-highs-list1 list1-mediums-list1 list1-lows-list1))
  (setf *syllable-lists* '(make-list-high-list make-list-medium-list make-list-low-list))
  (setf *print-major-updates* t)
  (setf *number-of-sessions* 1000)
  (setf *presentation-rate* 4000)
  (setf *inter-trial-interval* 4000)
  (setf *guessing-routine* nil)
  (setf *pa-exit-criteria* 'consecutive-trials-criteria)
  (setf *pa-exit-value* 1)
  (setf *indexes* '(1 2 3))
  (loop for list-or-routine in *syllable-lists*
        for syllable-name in *names-of-syllable-lists*
        do
        (initialize-results-arrays)
        (format t "~%Beginning ~a simulation~%" syllable-name)
        (loop repeat *number-of-sessions*
              for syllable-lists = (cond ((listp list-or-routine) list-or-routine)
                                         ('else (apply list-or-routine nil)))
              do (multi-list-session syllable-lists
                                     '(consecutive-trials-criteria 
                                       consecutive-trials-criteria 
                                       consecutive-trials-criteria) 
                                     '(1 1 1)
                                     '(15 15 15)
                                     '(1 2 3)))
        (print-paired-associate-results))
  (format t "~%Interlist Similarity and Proportion Correct Relearning Trial 1:~%")
  (format t "Condition       SAL2   EPAM6~%")
  (format t "----------------------------~%")
  (format t "High-sim        0.49   ~4,2f~%" (car (nth 2 *proportion-correct-by-trial*)))
  (format t "Medium-sim      0.76   ~4,2f~%" (car (nth 5 *proportion-correct-by-trial*)))
  (format t "Low-similarity  0.99   ~4,2f~%" (car (nth 8 *proportion-correct-by-trial*)))
)

(defun hintzman-experiment8 ()
  (format t "~%Hintzman Experiment 8. RI and Amount of Original Learning:")
  (seed-random-generator)
  (setf *proportion-correct-by-trial* nil)
  (reset-data-collection-arrays)
  (setf *noticing-order* nil)
  (setf *names-of-syllable-lists* '(list1-mediums-list1))
  (setf *syllable-lists* 'make-list-medium-list)
  (setf *print-major-updates* t)
  (setf *number-of-sessions* 1000)
  (setf *presentation-rate* 4000)
  (setf *inter-trial-interval* 4000)
  (setf *guessing-routine* nil)
  (setf *pa-exit-criteria* 'consecutive-trials-criteria)
  (setf *pa-exit-value* 1)
  (setf *indexes* '(1 2 3))
  (loop for initial-learning-trials in '(4 8 12 16)
        do 
        (initialize-results-arrays)
        (format t "~%Simulation with ~a trials of ORIGINAL LEARNING:~%"
                initial-learning-trials)
        (loop repeat *number-of-sessions*
              for list-or-routine = *syllable-lists*
              for syllable-lists = (cond ((listp list-or-routine) list-or-routine)
                                         ('else (apply list-or-routine nil)))
              do 
              (multi-list-session syllable-lists
                                     '(absolute-trials-criteria 
                                       consecutive-trials-criteria 
                                       consecutive-trials-criteria) 
                                     `(,initial-learning-trials 1 1)
                                     '(15 15 15)
                                     '(1 2 3)))
        (print-paired-associate-results))
  (format t "~%Number of errors due to RI on RL-1~%")
  (format t "Trials-of-OL     SAL3     EPAM6~%")
  (format t "    4             2.7      ~4,1f~%"
          (* 9
             (- (- 1 (car (nth 2 *proportion-correct-by-trial*)))
                (- 1 (nth 4 (nth 9 *proportion-correct-by-trial*))))))
  (format t "    8             1.3      ~4,1f~%"
          (* 9
             (- (- 1 (car (nth 5 *proportion-correct-by-trial*)))
                (- 1 (nth 8 (nth 9 *proportion-correct-by-trial*))))))
  (format t "   12             0.5      ~4,1f~%"
          (* 9
             (- (- 1 (car (nth 8 *proportion-correct-by-trial*)))
                (- 1 (nth 12 (nth 9 *proportion-correct-by-trial*))))))
  )


(defun hintzman-experiment9 ()
  (format t "~%Hintzman Experiment 9. RI and Amount of Interpolated Learning")
  (seed-random-generator)
  (setf *errors-between-one-and-ten* nil)
  (setf *errors-by-list* nil)
  (reset-data-collection-arrays)
  (setf *noticing-order* nil)
  (setf *names-of-syllable-lists* '(list1-highs-list1))
  (setf *syllable-lists* 'make-low-high-low-list)
  (format t "~%List1 is associated with digits from 11 to 20, List2 with digits from 1 to 10~%")
  (setf *print-major-updates* t)
  (setf *number-of-sessions* 1000)
  (setf *presentation-rate* 4000)
  (setf *inter-trial-interval* 4000)
  (setf *pa-exit-criteria* 'consecutive-trials-criteria)
  (setf *pa-exit-value* 1)
  (setf (aref *track-intrusions* 3) t)
  (setf *indexes* '(1 2 3))
  (loop for interpolated-learning-trials in '(1 2 4 5 7 10 12 20 40)
        do (format t "~%Simulation with ~a trials of INTERPOLATED LEARNING:~%"
                   interpolated-learning-trials)
        (initialize-results-arrays)
        (loop repeat *number-of-sessions*
              for list-or-routine = *syllable-lists*
              for syllable-lists = (cond ((listp list-or-routine) list-or-routine)
                                         ('else (apply list-or-routine nil)))
              do 
              (multi-list-session syllable-lists
                                     '(consecutive-trials-criteria 
                                       absolute-trials-criteria 
                                       absolute-trials-criteria) 
                                     `(1 ,interpolated-learning-trials 1)
                                     '(15 15 15)
                                     '(1 2 3)))
        (print-paired-associate-results))
  (format t "Total Errors and intrusion errors on the first trial related to number of trials of interpolated learning.~%")
  (format t "Human data comes from Thune and Underwood, 1943, Journal of Experimental Psychology, pp 185-199~%")
  (format t "Thune and Underwood paired adjectives, EPAM and SAL nonsense-syllable stimuli with digit responses.~%")
  (format t "Thune and Underwood used same adjectives as stimuli in first and second lists, EPAM & SAL used~%")
  (format t "nonsense syllables which were highly similar such that for every syllable in list1 there~%")
  (format t "was a syllable in list2 that differed only in the third position.~%")
  (format t "We estimated SAL Results from the published chart.~%~%")
  (format t "Interpolated-Trials     People         SAL2           EPAM6~%")
  (format t "                     Intrus Total  Intrus Total   Intrus  Total~%")
  (format t "         1                           4.0   4.0     ~4,2f  ~5,2f~%"
          (nth 0 *errors-between-one-and-ten*) (nth 2 *errors-by-list*)  )
  (format t "         2             0.00  0.88    5.5   6.0     ~4,2f  ~5,2f~%"                    
          (nth 1 *errors-between-one-and-ten*) (nth 5 *errors-by-list*) )
  (format t "         4                           4.5   6.5     ~4,2f  ~5,2f~%"
          (nth 2 *errors-between-one-and-ten*) (nth 8 *errors-by-list*) )
  (format t "         5             1.50  1.92                  ~4,2f  ~5,2f~%"
          (nth 3 *errors-between-one-and-ten*) (nth 11 *errors-by-list*) )
  (format t "         7                           3.2   5.8     ~4,2f  ~5,2f~%"
          (nth 4 *errors-between-one-and-ten*) (nth 14 *errors-by-list*) )
  (format t "        10             2.17  2.42                  ~4,2f  ~5,2f~%"
          (nth 5 *errors-between-one-and-ten*) (nth 17 *errors-by-list*) )
  (format t "        12                           1.3   7.1     ~4,2f  ~5,2f~%"
          (nth 6 *errors-between-one-and-ten*) (nth 20 *errors-by-list*) )
  (format t "        20             0.25  2.50                  ~4,2f  ~5,2f~%"
          (nth 7 *errors-between-one-and-ten*) (nth 23 *errors-by-list*) )
  (format t "        40                                         ~4,2f  ~5,2f~%"
          (nth 8 *errors-between-one-and-ten*) (nth 26 *errors-by-list*) )
  )



(defun hintzman-experiment11 ()
  (format t "~%Hintzman Experiment 11. Recognition vs. Recall:")
  (seed-random-generator)
  (setf *result-of-recognition-test* nil)
  (reset-data-collection-arrays)
  (setf *noticing-order* nil)
  (setf *print-major-updates* t)
  (setf *presentation-rate* 4000)
  (setf *inter-trial-interval* 4000)
  (setf *number-of-sessions* 1000)
  (setf *pa-exit-criteria* 'consecutive-trials-criteria)
  (setf *pa-exit-value* 1)
  (setf *syllable-lists* (list *sal-9-R*))
  (setf *names-of-syllable-lists* '(sal-9-R))
  (setf *guessing-routine* 'pick-a-number-between-1-and-9)
  (setf *do-recognition-test* (make-array 50 :initial-element nil))
  (setf (aref *do-recognition-test* 1) t)
  (setf *lowest-recognition-trial* 2)
  (setf *highest-recognition-trial* 8)
  (simple-paired-associate-experiment)
  (format t "The key finding here is that Pr(H)>Pr(F) on pairs which were recalled incorrectly.~%")
  (format t "~%Recognition Hits and Failures with Correct and Incorrect Recall.~%")
  (format t "Proportion                   Sal3     EPAM6~%")
  (format t "Pr(H) with correct recall    1.00     ~4,2f~%" (nth 0 *result-of-recognition-test*))
  (format t "Pr(F) with correct recall    0.05     ~4,2f~%" (nth 1 *result-of-recognition-test*))
  (format t "Pr(H) with incorrect recall  0.41     ~4,2f~%" (nth 2 *result-of-recognition-test*))
  (format t "Pr(F) with incorrect recall  0.22     ~4,2f~%" (nth 3 *result-of-recognition-test*))
  )

(defun recognition-test (stimulus correct-response)
  "Note *guessing-routine* must be specified and correct-response must be an atom 
for this to work"
  (when (null *guessing-routine*) 
    (print "Error Recognition test but no guessing routine!"))
  (let* ((paired-response (cond ((< (random 2) 1) correct-response)
                                ('else (loop for guess = (apply *guessing-routine* nil)
                                             if (not (eql guess correct-response))
                                             return guess))))
         (recognition-response (recognize 'episode stimulus paired-response))
         (result (cond ((and (eql correct-response paired-response)
                             recognition-response)
                        'hit)
                       ((and (eql correct-response paired-response)
                             (not recognition-response))
                        'incorrect-rejection)
                       ((and (not (eql correct-response paired-response))
                             recognition-response)
                        'false-alarm)
                       ((and (not (eql correct-response paired-response))
                             (not recognition-response))
                        'correct-rejection))))
    (fixed-time-tally-to-recognize)
    (when *print-experimenter-updates*
      (format t "Recognition test for stimulus = ~a with response = ~a and correct = ~a:~%"
              stimulus paired-response correct-response)
      (format t "Subject responded ~a -- ~a~%" recognition-response result))
    (when (or (eql result 'incorrect-rejection) 
              (eql result 'false-alarm)) 
      (incf *recognition-errors*))
    (put-value-on-recognition-record stimulus result)))

(defun extra-recalls-test (subjects-response stimulus correct-response)
  "Note this routine will have to be revised if response is not an atom."
  (let* ((stimulus-chunk (get-chunk-for stimulus))
         (possible-responses (when (does-object-match-chunk? stimulus stimulus-chunk)
                               (mapcar 'specify (find-associates 'episode stimulus-chunk)))))
    ;the first problem is that sometimes the subject might have guessed his initial response
    ;in which case it wouldn't be on the list of possible responses.
    (when (not (member subjects-response possible-responses))
      (setf possible-responses (cons subjects-response possible-responses)))
    ;the second problem is that sometimes the list of possible responses is to short.  In 
    ;such cases we will add responses that haven't already been guessed to the list. 
    (loop while (< (length possible-responses) 3)
          for possible-response = (apply *guessing-routine* nil)
          if (not (member possible-response possible-responses))
          do (setf possible-responses (append possible-responses
                                              (list possible-response))))
    (setf possible-responses (remove subjects-response 
                                     possible-responses))
    (when *print-experimenter-updates*
      (format t "Recalls for stimulus ~a = ~a~%"
              stimulus
              (list subjects-response 
                    (first possible-responses) 
                    (second possible-responses))))
    (cond ((eql (first possible-responses) correct-response)
           (when *print-experimenter-updates*
             (format t "Subject responded correctly with second response.~%"))
           (incf (aref *second-recall-correct* *trial* *index*))
           (incf (aref *third-recall-incorrect* *trial* *index*)))
          ((eql (second possible-responses) correct-response)
           (when *print-experimenter-updates*
             (format t "Subject responded correctly with third response.~%"))
           (incf (aref *third-recall-correct* *trial* *index*))
           (incf (aref *second-recall-incorrect* *trial* *index*)))
          ('else
           (incf (aref *second-recall-incorrect* *trial* *index*))
           (incf (aref *third-recall-incorrect* *trial* *index*))
           ))))

(defun put-value-on-recognition-record (stim new-value)
  (let ((current-values (second (assoc stim (aref *recognition-record* *index*) :test #'equal))))
    (cond ((null current-values) 
           (setf (aref *recognition-record* *index*)
                 (cons (list stim (list new-value)) 
                       (aref *recognition-record* *index*))))
          ('else 
           (nconc current-values (list new-value))))))

(defun put-value-on-latency-record (stim latency)
  (let ((current-values (second (assoc stim (aref *latency-record* *index*) :test #'equal))))
    (cond ((null current-values) 
           (setf (aref *latency-record* *index*)
                 (cons (list stim (list latency)) (aref *latency-record* *index*))))
          ('else 
           (nconc current-values (list latency))))))

(defun update-recognition-record ()
  "This routine keeps track of the recognition statistics for trials 2 through 5."
  (when *print-experimenter-updates*
    (format t "~%Responses to stimuli were the following: ~%"))
  (loop for alist in (aref *response-record* *index*) 
        for attribute = (first alist)
        for values = (second alist)
        for recognition-alist = (assoc attribute (aref *recognition-record* *index*) :test #'equal)
        for recognition-values = (second recognition-alist)
        do (when *print-experimenter-updates* (format t "~a:" (first recognition-alist)))
        (loop for value in values
              for rvalue in recognition-values
              for n from 0
              do 
              (when *print-experimenter-updates* 
                (format t " ~a" rvalue))
              if (< n *lowest-recognition-trial*) do 'nil
              else if (> n *highest-recognition-trial*) do 'nil
              else if (and (eql value t) (eql rvalue 'hit))
              do (incf (aref *hit-when-correct* *index*))
              else if (and (eql value t) (eql rvalue 'false-alarm))
              do (incf (aref *false-alarm-when-correct* *index*))
              else if (and (not (eql value t)) (eql rvalue 'hit))
              do (incf (aref *hit-when-incorrect* *index*))
              else if (and (not (eql value t)) (eql rvalue 'false-alarm))
              do (incf (aref *false-alarm-when-incorrect* *index*))
              else if (and (eql value t) (eql rvalue 'incorrect-rejection))
              do (incf (aref *incorrect-rejection-when-correct* *index*))
              else if (and (eql value t) (eql rvalue 'correct-rejection))
              do (incf (aref *correct-rejection-when-correct* *index*))
              else if (and (not (eql value t)) (eql rvalue 'incorrect-rejection))
              do (incf (aref *incorrect-rejection-when-incorrect* *index*))
              else if (and (not (eql value t)) (eql rvalue 'correct-rejection))
              do (incf (aref *correct-rejection-when-incorrect* *index*))
              finally (when *print-experimenter-updates* (format t "~%")))))

(defun hintzman-experiment12 ()
  (format t "~%Hintzman Experiment 12. Second and Third Guesses:")
  (seed-random-generator)
  (reset-data-collection-arrays)
  (setf *noticing-order* nil)
  (setf *print-major-updates* t)
  (setf *presentation-rate* 4000)
  (setf *inter-trial-interval* 4000)
  (setf *number-of-sessions* 1000)
  (setf *pa-exit-criteria* 'consecutive-trials-criteria)
  (setf *pa-exit-value* 1)
  (setf *syllable-lists* (list *sal-9-R*))
  (setf *names-of-syllable-lists* '(sal-9-R))
  (setf *guessing-routine* 'pick-a-number-between-1-and-9)
  (setf (aref *do-extra-recalls-test* 1) t)
  (setf *lowest-extra-recalls-trial* 2)
  (setf *highest-extra-recalls-trial* 12)
  (simple-paired-associate-experiment)
  (format t "~%Proportion Correct on First, Second and Third Guesses:~%")
  (format t "Trial number         SAL3                EPAM6~%")
  (format t "                -----------------   -----------------~%")
  (format t "                  1     2     3       1     2     3  ~%")
  (format t "                -----------------   -----------------~%")
  (format t "     2          .172  .161  .144   ~5,3f ~5,3f ~5,3f~%"
          (nth 0 *proportion-of-recalls*)
          (nth 11 *proportion-of-recalls*)
          (nth 22 *proportion-of-recalls*))
  (format t "     3          .350  .256  .287   ~5,3f ~5,3f ~5,3f~%"
          (nth 1 *proportion-of-recalls*)
          (nth 12 *proportion-of-recalls*)
          (nth 23 *proportion-of-recalls*))
  (format t "     4          .561  .418  .283   ~5,3f ~5,3f ~5,3f~%"
          (nth 2 *proportion-of-recalls*)
          (nth 13 *proportion-of-recalls*)
          (nth 24 *proportion-of-recalls*))
  (format t "     5          .750  .489  .348   ~5,3f ~5,3f ~5,3f~%"
          (nth 3 *proportion-of-recalls*)
          (nth 14 *proportion-of-recalls*)
          (nth 25 *proportion-of-recalls*))
  (format t "     6          .822  .531  .467   ~5,3f ~5,3f ~5,3f~%"
          (nth 4 *proportion-of-recalls*)
          (nth 15 *proportion-of-recalls*)
          (nth 26 *proportion-of-recalls*))
  (format t "     7          .894  .526  .111   ~5,3f ~5,3f ~5,3f~%"
          (nth 5 *proportion-of-recalls*)
          (nth 16 *proportion-of-recalls*)
          (nth 27 *proportion-of-recalls*))
  (format t "     8                             ~5,3f ~5,3f ~5,3f~%"
          (nth 6 *proportion-of-recalls*)
          (nth 17 *proportion-of-recalls*)
          (nth 28 *proportion-of-recalls*))
  (format t "     9                             ~5,3f ~5,3f ~5,3f~%"
          (nth 7 *proportion-of-recalls*)
          (nth 18 *proportion-of-recalls*)
          (nth 29 *proportion-of-recalls*))
  (format t "    10                             ~5,3f ~5,3f ~5,3f~%"
          (nth 8 *proportion-of-recalls*)
          (nth 19 *proportion-of-recalls*)
          (nth 30 *proportion-of-recalls*))
  (format t "    11                             ~5,3f ~5,3f ~5,3f~%"
          (nth 9 *proportion-of-recalls*)
          (nth 20 *proportion-of-recalls*)
          (nth 31 *proportion-of-recalls*))
  (format t "    12                             ~5,3f ~5,3f ~5,3f~%"
          (nth 10 *proportion-of-recalls*)
          (nth 21 *proportion-of-recalls*)
          (nth 32 *proportion-of-recalls*))
  )

(defun hintzman-experiment16 ()
  "This is a simulation of an experiment conducted by Suppes, P., Groen, G.,
and Schlag-Rey, M. A model for response latency in paired-associate learning
from the Journal of Mathematical Psychology 3, 99-128, 1996. and first
simulated by Hintzman."
  (format t "~%Hintzman-experiment16. Response Latencies:~%")
  (format t "List1 is practice-set1, List2 is experimental-set1, List3 is practice-set2 and List4 is experimental-set2.~%")
  (seed-random-generator)
  (setf *average-latency* nil)
  (setf *average-latency-when-correct* nil)
  (setf *average-latency-when-incorrect* nil)
  (setf *average-latency-of-last-error* nil)
  (reset-data-collection-arrays)
  (setf *noticing-order* nil)
  (setf *print-major-updates* t)
  (setf *presentation-rate* 6250)
  (setf *inter-trial-interval* 0)
  (setf *number-of-sessions* 1000)
  (setf *pa-exit-criteria* 'consecutive-trials-criteria)
  (setf *pa-exit-value* 1)
  (setf *syllable-lists* 'make-suppes-lists)
  (setf *guessing-routine* 'pick-a-number-between-1-and-3)
  (setf (aref *track-response-latencies* 2) t)
  (setf (aref *track-response-latencies* 4) t)
  (initialize-results-arrays)
  (setf *indexes* '(1 2 3 4))
  (loop repeat *number-of-sessions*
        for list-or-routine = *syllable-lists*
        for syllable-lists = (cond ((listp list-or-routine) list-or-routine)
                                   ('else (apply list-or-routine nil)))
        do (multi-list-session syllable-lists
                               '(absolute-trials-criteria 
                                 absolute-trials-criteria 
                                 absolute-trials-criteria 
                                 absolute-trials-criteria)
                               '(3 25 3 25)
                               '(300 604800000 15 0)
                               '(1 2 3 4)))
  (print-paired-associate-results)
  (format t "Response latency in PA Learning.~%")
  (format t "Human data is form Suppes et al, 1966, Jrnl of Mathematical Psychology, 3 99-128.~%")
  (format t "~%Mean Response Latency Curve for Session 1~%")
  (format t "Trial       People       EPAM6~%")
  (format t "  1          1.23        ~5,2f~%" (* .001 (nth 0 *average-latency*)))
  (format t "  2          1.44        ~5,2f~%" (* .001 (nth 1 *average-latency*)))
  (format t "  3          1.46        ~5,2f~%" (* .001 (nth 2 *average-latency*)))
  (format t "  4          1.43        ~5,2f~%" (* .001 (nth 3 *average-latency*)))
  (format t "  5          1.44        ~5,2f~%" (* .001 (nth 4 *average-latency*)))
  (format t "  6          1.47        ~5,2f~%" (* .001 (nth 5 *average-latency*)))
  (format t "  7          1.44        ~5,2f~%" (* .001 (nth 6 *average-latency*)))
  (format t "  8          1.40        ~5,2f~%" (* .001 (nth 7 *average-latency*)))
  (format t "  9          1.33        ~5,2f~%" (* .001 (nth 8 *average-latency*)))
  (format t " 10          1.30        ~5,2f~%" (* .001 (nth 9 *average-latency*)))
  (format t " 11          1.22        ~5,2f~%" (* .001 (nth 10 *average-latency*)))
  (format t " 11          1.28        ~5,2f~%" (* .001 (nth 11 *average-latency*)))
  (format t " 13          1.23        ~5,2f~%" (* .001 (nth 12 *average-latency*)))
  (format t " 14          1.20        ~5,2f~%" (* .001 (nth 13 *average-latency*)))
  (format t " 15          1.17        ~5,2f~%" (* .001 (nth 14 *average-latency*)))
  (format t " 16          1.17        ~5,2f~%" (* .001 (nth 15 *average-latency*)))
  (format t " 17          1.16        ~5,2f~%" (* .001 (nth 16 *average-latency*)))
  (format t " 18          1.16        ~5,2f~%" (* .001 (nth 17 *average-latency*)))
  (format t " 19          1.16        ~5,2f~%" (* .001 (nth 18 *average-latency*)))
  (format t " 20          1.13        ~5,2f~%" (* .001 (nth 19 *average-latency*)))
  (format t " 21          1.12        ~5,2f~%" (* .001 (nth 20 *average-latency*)))
  (format t " 22          1.12        ~5,2f~%" (* .001 (nth 21 *average-latency*)))
  (format t " 23          1.12        ~5,2f~%" (* .001 (nth 22 *average-latency*)))
  (format t " 24          1.11        ~5,2f~%" (* .001 (nth 23 *average-latency*)))
  (format t " 25          1.11        ~5,2f~%" (* .001 (nth 24 *average-latency*)))
  (format t "~%Mean Response Latency Curve for Session 2~%")
  (format t "Trial       People       EPAM6~%")
  (format t "  1          1.07        ~5,2f~%" (* .001 (nth 25 *average-latency*)))
  (format t "  2          1.28        ~5,2f~%" (* .001 (nth 26 *average-latency*)))
  (format t "  3          1.34        ~5,2f~%" (* .001 (nth 27 *average-latency*)))
  (format t "  4          1.39        ~5,2f~%" (* .001 (nth 28 *average-latency*)))
  (format t "  5          1.35        ~5,2f~%" (* .001 (nth 29 *average-latency*)))
  (format t "  6          1.26        ~5,2f~%" (* .001 (nth 30 *average-latency*)))
  (format t "  7          1.23        ~5,2f~%" (* .001 (nth 31 *average-latency*)))
  (format t "  8          1.24        ~5,2f~%" (* .001 (nth 32 *average-latency*)))
  (format t "  9          1.18        ~5,2f~%" (* .001 (nth 33 *average-latency*)))
  (format t " 10          1.17        ~5,2f~%" (* .001 (nth 34 *average-latency*)))
  (format t " 11          1.12        ~5,2f~%" (* .001 (nth 35 *average-latency*)))
  (format t " 11          1.09        ~5,2f~%" (* .001 (nth 36 *average-latency*)))
  (format t " 13          1.08        ~5,2f~%" (* .001 (nth 37 *average-latency*)))
  (format t " 14          1.09        ~5,2f~%" (* .001 (nth 38 *average-latency*)))
  (format t " 15          1.08        ~5,2f~%" (* .001 (nth 39 *average-latency*)))
  (format t " 16          1.12        ~5,2f~%" (* .001 (nth 40 *average-latency*)))
  (format t " 17          1.08        ~5,2f~%" (* .001 (nth 41 *average-latency*)))
  (format t " 18          1.04        ~5,2f~%" (* .001 (nth 42 *average-latency*)))
  (format t " 19          1.06        ~5,2f~%" (* .001 (nth 43 *average-latency*)))
  (format t " 20          1.05        ~5,2f~%" (* .001 (nth 44 *average-latency*)))
  (format t " 21          1.06        ~5,2f~%" (* .001 (nth 45 *average-latency*)))
  (format t " 22          1.04        ~5,2f~%" (* .001 (nth 46 *average-latency*)))
  (format t " 23          1.02        ~5,2f~%" (* .001 (nth 47 *average-latency*)))
  (format t " 24          1.05        ~5,2f~%" (* .001 (nth 48 *average-latency*)))
  (format t " 25          1.03        ~5,2f~%" (* .001 (nth 49 *average-latency*)))
  (format t "~%Mean Latency Curves Conditionalized on Errors and Successes for Session 1~%")
  (format t "Trial        People         EPAM6~%")
  (format t "         --------------  --------------~%")
  (format t "         Errors Success  Errors Success~%")
  (format t "         --------------  --------------~%")
  (format t "Last-error    1.65           ~5,2f~%" (* .001 (first *average-latency-of-last-error*)))
  (format t "  1       1.22   1.26    ~5,2f  ~5,2f~%" (* .001 (nth 0 *average-latency-when-incorrect*)) (* .001 (nth 0 *average-latency-when-correct*)))
  (format t "  2       1.51   1.36    ~5,2f  ~5,2f~%" (* .001 (nth 1 *average-latency-when-incorrect*)) (* .001 (nth 1 *average-latency-when-correct*)))
  (format t "  3       1.53   1.40    ~5,2f  ~5,2f~%" (* .001 (nth 2 *average-latency-when-incorrect*)) (* .001 (nth 2 *average-latency-when-correct*)))
  (format t "  4       1.56   1.33    ~5,2f  ~5,2f~%" (* .001 (nth 3 *average-latency-when-incorrect*)) (* .001 (nth 3 *average-latency-when-correct*)))
  (format t "  5       1.65   1.34    ~5,2f  ~5,2f~%" (* .001 (nth 4 *average-latency-when-incorrect*)) (* .001 (nth 4 *average-latency-when-correct*)))
  (format t "  6       1.71   1.39    ~5,2f  ~5,2f~%" (* .001 (nth 5 *average-latency-when-incorrect*)) (* .001 (nth 5 *average-latency-when-correct*)))
  (format t "  7       1.72   1.35    ~5,2f  ~5,2f~%" (* .001 (nth 6 *average-latency-when-incorrect*)) (* .001 (nth 6 *average-latency-when-correct*)))
  (format t "  8       1.61   1.35    ~5,2f  ~5,2f~%" (* .001 (nth 7 *average-latency-when-incorrect*)) (* .001 (nth 7 *average-latency-when-correct*)))
  (format t "  9       1.58   1.28    ~5,2f  ~5,2f~%" (* .001 (nth 8 *average-latency-when-incorrect*)) (* .001 (nth 8 *average-latency-when-correct*)))
  (format t " 10       1.77   1.23    ~5,2f  ~5,2f~%" (* .001 (nth 9 *average-latency-when-incorrect*)) (* .001 (nth 9 *average-latency-when-correct*)))
  (format t " 11       1.54   1.18    ~5,2f  ~5,2f~%" (* .001 (nth 10 *average-latency-when-incorrect*)) (* .001 (nth 10 *average-latency-when-correct*)))
  (format t " 11              1.22           ~5,2f~%" (* .001 (nth 11 *average-latency-when-correct*)))
  (format t " 13              1.21           ~5,2f~%" (* .001 (nth 12 *average-latency-when-correct*)))
  (format t " 14              1.19           ~5,2f~%" (* .001 (nth 13 *average-latency-when-correct*)))
  (format t " 15              1.16           ~5,2f~%" (* .001 (nth 14 *average-latency-when-correct*)))
  (format t " 16              1.15           ~5,2f~%" (* .001 (nth 15 *average-latency-when-correct*)))
  (format t " 17              1.16           ~5,2f~%" (* .001 (nth 16 *average-latency-when-correct*)))
  (format t " 18              1.16           ~5,2f~%" (* .001 (nth 17 *average-latency-when-correct*)))
  (format t " 19              1.17           ~5,2f~%" (* .001 (nth 18 *average-latency-when-correct*)))
  (format t " 20              1.14           ~5,2f~%" (* .001 (nth 19 *average-latency-when-correct*)))
  (format t " 21              1.12           ~5,2f~%" (* .001 (nth 20 *average-latency-when-correct*)))
  (format t " 22              1.11           ~5,2f~%" (* .001 (nth 21 *average-latency-when-correct*)))
  (format t " 23              1.12           ~5,2f~%" (* .001 (nth 22 *average-latency-when-correct*)))
  (format t " 24              1.10           ~5,2f~%" (* .001 (nth 23 *average-latency-when-correct*)))
  (format t " 25              1.10           ~5,2f~%" (* .001 (nth 24 *average-latency-when-correct*)))
  (format t "~%Mean Latency Curves Conditionalized on Errors and Successes for Session 2~%")
  (format t "Trial        People         EPAM6~%")
  (format t "         --------------  --------------~%")
  (format t "         Errors Success  Errors Success~%")
  (format t "         --------------  --------------~%")
  (format t "Last-error    1.46          ~5,2f~%" (* .001 (second *average-latency-of-last-error*)))
  (format t "  1       1.16   1.10    ~5,2f  ~5,2f~%" (* .001 (nth 25 *average-latency-when-incorrect*)) (* .001 (nth 25 *average-latency-when-correct*)))
  (format t "  2       1.38   1.21    ~5,2f  ~5,2f~%" (* .001 (nth 26 *average-latency-when-incorrect*)) (* .001 (nth 26 *average-latency-when-correct*)))
  (format t "  3       1.42   1.28    ~5,2f  ~5,2f~%" (* .001 (nth 27 *average-latency-when-incorrect*)) (* .001 (nth 27 *average-latency-when-correct*)))
  (format t "  4       1.49   1.35    ~5,2f  ~5,2f~%" (* .001 (nth 28 *average-latency-when-incorrect*)) (* .001 (nth 28 *average-latency-when-correct*)))
  (format t "  5       1.49   1.30    ~5,2f  ~5,2f~%" (* .001 (nth 29 *average-latency-when-incorrect*)) (* .001 (nth 29 *average-latency-when-correct*)))
  (format t "  6       1.43   1.23    ~5,2f  ~5,2f~%" (* .001 (nth 30 *average-latency-when-incorrect*)) (* .001 (nth 30 *average-latency-when-correct*)))
  (format t "  7       1.44   1.19    ~5,2f  ~5,2f~%" (* .001 (nth 31 *average-latency-when-incorrect*)) (* .001 (nth 31 *average-latency-when-correct*)))
  (format t "  8       1.48   1.21    ~5,2f  ~5,2f~%" (* .001 (nth 32 *average-latency-when-incorrect*)) (* .001 (nth 32 *average-latency-when-correct*)))
  (format t "  9       1.25   1.18    ~5,2f  ~5,2f~%" (* .001 (nth 33 *average-latency-when-incorrect*)) (* .001 (nth 33 *average-latency-when-correct*)))
  (format t " 10       1.30   1.16    ~5,2f  ~5,2f~%" (* .001 (nth 34 *average-latency-when-incorrect*)) (* .001 (nth 34 *average-latency-when-correct*)))
  (format t " 11       1.42   1.10    ~5,2f  ~5,2f~%" (* .001 (nth 35 *average-latency-when-incorrect*)) (* .001 (nth 35 *average-latency-when-correct*)))
  (format t " 11              1.08           ~5,2f~%" (* .001 (nth 36 *average-latency-when-correct*)))
  (format t " 13              1.08           ~5,2f~%" (* .001 (nth 37 *average-latency-when-correct*)))
  (format t " 14              1.08           ~5,2f~%" (* .001 (nth 38 *average-latency-when-correct*)))
  (format t " 15              1.07           ~5,2f~%" (* .001 (nth 39 *average-latency-when-correct*)))
  (format t " 16              1.12           ~5,2f~%" (* .001 (nth 40 *average-latency-when-correct*)))
  (format t " 17              1.07           ~5,2f~%" (* .001 (nth 41 *average-latency-when-correct*)))
  (format t " 18              1.04           ~5,2f~%" (* .001 (nth 42 *average-latency-when-correct*)))
  (format t " 19              1.07           ~5,2f~%" (* .001 (nth 43 *average-latency-when-correct*)))
  (format t " 20              1.05           ~5,2f~%" (* .001 (nth 44 *average-latency-when-correct*)))
  (format t " 21              1.06           ~5,2f~%" (* .001 (nth 45 *average-latency-when-correct*)))
  (format t " 22              1.05           ~5,2f~%" (* .001 (nth 46 *average-latency-when-correct*)))
  (format t " 23              1.02           ~5,2f~%" (* .001 (nth 47 *average-latency-when-correct*)))
  (format t " 24              1.05           ~5,2f~%" (* .001 (nth 48 *average-latency-when-correct*)))
  (format t " 25              1.03           ~5,2f~%" (* .001 (nth 49 *average-latency-when-correct*)))
)

(defun Bruce-Interlist-Similarity-Simulation ()
  "This is a simulation of an experiment conducted by Bruce (1933) as 
reported in the Journal of Experimental Psychology Vol. 16, 343-361, and
later simulated by Simon and Feigenbaum (1964) using Epam III."
  (format t "Bruce Interlist Similarity Experiment:~%")
  (seed-random-generator)
  (setf *total-trials-by-list* nil)
  (reset-data-collection-arrays)
  (setf *noticing-order* nil)
  (setf *print-major-updates* t)
  (setf *presentation-rate* 6250)
  (setf *inter-trial-interval* 0)
  (setf *number-of-sessions* 1000) ; Note this number must be divisible by 4
  (setf *indexes* '(20 22 26 32))
  (format t "Note: Subtract 10 from the List Number for integration condition.~%")
  (format t "Thus, 10 means 0 studying of first list, 12 means two times through first list, and so on.~%")
  (loop for list-or-routine in '(make-abcd make-abcb make-abac)
        for name-of-experiment in '(ab-cd ab-cb ab-ac)
        do (format t "Name-of-experiment = ~a~%" name-of-experiment)
        (initialize-results-arrays)
        (loop repeat (/ *number-of-sessions* 4)
              for syllable-lists = (cond ((listp list-or-routine) list-or-routine)
                                         ('else (apply list-or-routine nil)))
              for order-of-presentation1 = (reorder-list-randomly '(0 2 6 12))
              for order-of-presentation2 = (reorder-list-randomly '(0 2 6 12))
              for order-of-presentation3 = (reorder-list-randomly '(0 2 6 12))
              for order-of-presentation4 = (reorder-list-randomly '(0 2 6 12))
              for indices1 = (loop for order in order-of-presentation1
                                   append (list order (+ 20 order)))
              for indices2 = (loop for order in order-of-presentation2
                                   append (list order (+ 20 order)))
              for indices3 = (loop for order in order-of-presentation3
                                   append (list order (+ 20 order)))
              for indices4 = (loop for order in order-of-presentation4
                                   append (list order (+ 20 order)))
              for exits1 = (loop for order in order-of-presentation1
                                  append (list order 1))
              for exits2 = (loop for order in order-of-presentation2
                                  append (list order 1))
              for exits3 = (loop for order in order-of-presentation3
                                  append (list order 1))
              for exits4 = (loop for order in order-of-presentation4
                                  append (list order 1))
              do (multi-list-session syllable-lists
                                     '(absolute-trials-criteria 
                                       consecutive-trials-criteria
                                       absolute-trials-criteria 
                                       consecutive-trials-criteria
                                       absolute-trials-criteria 
                                       consecutive-trials-criteria
                                       absolute-trials-criteria
                                       consecutive-trials-criteria
                                       absolute-trials-criteria 
                                       consecutive-trials-criteria
                                       absolute-trials-criteria 
                                       consecutive-trials-criteria
                                       absolute-trials-criteria 
                                       consecutive-trials-criteria
                                       absolute-trials-criteria
                                       consecutive-trials-criteria
                                       absolute-trials-criteria 
                                       consecutive-trials-criteria
                                       absolute-trials-criteria 
                                       consecutive-trials-criteria
                                       absolute-trials-criteria 
                                       consecutive-trials-criteria
                                       absolute-trials-criteria
                                       consecutive-trials-criteria
                                       absolute-trials-criteria 
                                       consecutive-trials-criteria
                                       absolute-trials-criteria 
                                       consecutive-trials-criteria
                                       absolute-trials-criteria 
                                       consecutive-trials-criteria
                                       absolute-trials-criteria
                                       consecutive-trials-criteria)
                                     (append exits1 exits2 exits3 exits4)
                                     '(0 86400000 
                                       0 86400000 
                                       0 86400000 
                                       0 86400000 
                                       0 86400000 
                                       0 86400000 
                                       0 86400000 
                                       0 86400000 
                                       0 86400000 
                                       0 86400000 
                                       0 86400000 
                                       0 86400000 
                                       0 86400000 
                                       0 86400000 
                                       0 86400000 
                                       0 86400000)
                                     (append indices1 indices2 indices3 indices4)))
        (print-paired-associate-results))
  (format t "~%Bruce Interlist Similarity Simulation.~%")
  (format t "Human data is from Bruce, Bruce, 1933, Journal of Experimental Psychology Vol. 16, 343-361~%")
  (format t "Relation of Transfer of Learning to Make a New Response to an Old~%")
  (format t "Stimulus, an Old Response to a New Stimulus, A New Response to a~%")
  (format t "New Stimulus as a function of the number of trials of study of the AB list.~%~%")
  (format t "~%Actual Results in terms of number of trials to learn second list.~%")
  (format t "Condition           People                     EPAM6~%")
  (format t "            ------------------------  -------------------------~%")
  (format t "               0     2     6    12        0     2     6    12~%")
  (format t "            ------------------------  -------------------------~%")
  (format t "  AB-AC      10.3  12.1  11.9  11.2    ~5,1f ~5,1f ~5,1f ~5,1f~%" 
          (nth 8 *total-trials-by-list*)
          (nth 9 *total-trials-by-list*)
          (nth 10 *total-trials-by-list*)
          (nth 11 *total-trials-by-list*))
  (format t "  AB-CB      14.4  16.6  12.0   9.0    ~5,1f ~5,1f ~5,1f ~5,1f~%"
          (nth 4 *total-trials-by-list*)
          (nth 5 *total-trials-by-list*)
          (nth 6 *total-trials-by-list*)
          (nth 7 *total-trials-by-list*))
  (format t "  AB-CD       9.9   9.9  10.7   8.3    ~5,1f ~5,1f ~5,1f ~5,1f~%"
          (nth 0 *total-trials-by-list*)
          (nth 1 *total-trials-by-list*)
          (nth 2 *total-trials-by-list*)
          (nth 3 *total-trials-by-list*))
  (format t "~%Results expressed as a ratio to 0-first-list-study condition.~%")
  (format t "Condition           People                     EPAM6~%")
  (format t "            ------------------------  -------------------------~%")
  (format t "               0     2     6    12        0     2     6    12~%")
  (format t "            ------------------------  -------------------------~%")
  (format t "  AB-AC      100   117   116   109    ~5,0f ~5,0f ~5,0f ~5,0f~%"
          (* 100 (/ (nth 8 *total-trials-by-list*) (nth 8 *total-trials-by-list*)))
          (* 100 (/ (nth 9 *total-trials-by-list*) (nth 8 *total-trials-by-list*)))
          (* 100 (/ (nth 10 *total-trials-by-list*) (nth 8 *total-trials-by-list*)))
          (* 100 (/ (nth 11 *total-trials-by-list*) (nth 8 *total-trials-by-list*))))
  (format t "  AB-CB      100   115    83    63    ~5,0f ~5,0f ~5,0f ~5,0f~%"
          (* 100 (/ (nth 4 *total-trials-by-list*) (nth 4 *total-trials-by-list*)))
          (* 100 (/ (nth 5 *total-trials-by-list*) (nth 4 *total-trials-by-list*)))
          (* 100 (/ (nth 6 *total-trials-by-list*) (nth 4 *total-trials-by-list*)))
          (* 100 (/ (nth 7 *total-trials-by-list*) (nth 4 *total-trials-by-list*))))
  (format t "  AB-CD      100   100   108    84    ~5,0f ~5,0f ~5,0f ~5,0f~%"
          (* 100 (/ (nth 0 *total-trials-by-list*) (nth 0 *total-trials-by-list*)))
          (* 100 (/ (nth 1 *total-trials-by-list*) (nth 0 *total-trials-by-list*)))
          (* 100 (/ (nth 2 *total-trials-by-list*) (nth 0 *total-trials-by-list*)))
          (* 100 (/ (nth 3 *total-trials-by-list*) (nth 0 *total-trials-by-list*))))
  )

(defun update-response-latencies ()
  (loop for alist in (aref *response-record* *index*) 
        for attribute = (first alist)
        for values = (second alist)
        for latency-alist = (assoc attribute (aref *latency-record* *index*) :test #'equal)
        for latency-values = (second latency-alist)
        do 
        (loop for value in latency-values
              for i from 1
              do 
              (setf (aref *latency-array*  i *index*)
                    (+ (aref *latency-array* i *index*) value))
              (incf (aref *trials-array* i *index*)))
        (loop for value in values
              for lvalue in latency-values
              for i from 1
              if (eql value t) 
              do (setf (aref *latency-array-when-correct* i *index*)
                       (+ (aref *latency-array-when-correct* i *index*)
                          lvalue))
              (incf (aref *trials-array-when-correct* i *index*))
              if (not (eql value t)) 
              do (setf (aref *latency-array-when-incorrect* i *index*)
                       (+ (aref *latency-array-when-incorrect* i *index*)
                          lvalue))
              (incf (aref *trials-array-when-incorrect* i *index*)))
        (loop for value in (reverse values)
              for lvalue in (reverse latency-values)
              if (not (eql value t)) 
              do (incf (aref *last-error-trials* *index*))
              (setf (aref *latency-for-last-error* *index*) 
                    (+ (aref *latency-for-last-error* *index*) lvalue))
              (return t))))
             
(defun multi-list-session (syllable-lists exits exit-values intervals indexes)
  "This routine is used when the same subject is given several lists to learn in a paired
   associate simulation."
  (initialize-variables)
  (zero-out-variables)
  (loop for syllable-list in syllable-lists
        for objects = (loop for item in syllable-list
                            collect (change-string-to-object item 'syllable))
        for pairs = (loop for object-tail on objects by #'cddr
                          for stimulus = (first object-tail)
                          for response = (second object-tail)
                          collect (list stimulus response))
        for exit in exits
        for exit-value in exit-values
        for interval in intervals
        for index in indexes
        do 
        (setf (aref *number-of-pairs* index) (length pairs))
        (setf *pa-exit-criteria* exit)
        (setf *pa-exit-value* exit-value)
        (setf *total-errors* 0)
        (setf *total-nodes-in-net* 0)
        (setf *errors-from-1-to-10* 0)
        (setf *errors-from-11-to-20* 0)
        (setf *errors-from-21-to-30* 0)
        (setf *index* index)
        (cond ((= *pa-exit-value* 0) ;; When the instruction is to do 0 trials, then if an
                                     ;; mmfr trial is called for do it.
               (when (aref *mmfr-recall-trial* *index*)
                 (mmfr-trial pairs))
               (when (aref *mfr-recall-trial* *index*)
                 (mfr-trial pairs)))
              ('else
               (quick-paired-associate-session pairs)
               (setf (aref *errors-on-list* *index*)
                     (+ (aref *errors-on-list* *index*) *total-errors*))
               (setf (aref *trials-on-list* *index*)
                     (+ (aref *trials-on-list* *index*) *trial*))
               (when (aref *track-intrusions* *index*)
                 (setf (aref *one-to-ten-errors* *index*)
                       (+ (aref *one-to-ten-errors* *index*)
                          *errors-from-1-to-10*))
                 (setf (aref *eleven-to-twenty-errors* *index*)
                       (+ (aref *eleven-to-twenty-errors* *index*)
                          *errors-from-11-to-20*))
	         (setf (aref *twenty-one-to-thirty-errors* *index*)
	               (+ (aref *twenty-one-to-thirty-errors* *index*)
                          *errors-from-21-to-30*)))))
        (setf *total-clock* (+ *total-clock* interval))
        ))

(defun print-out-errors-by-list ()
  (loop for index in *indexes*
        for errors-by-list = (* 1.0 (/ (aref *errors-on-list* index) *number-of-sessions*))
         do (format t "There was an average of ~a errors for list ~a~%"
                          errors-by-list 
                          index)
         (setf *errors-by-list* (append *errors-by-list* (list errors-by-list)))))

(defun print-out-total-trials-by-list ()
  (loop for index in *indexes*
        for total-trials-by-list = (* 1.0 (/ (aref *trials-on-list* index) *number-of-sessions*))
        do (format t "There was an average of ~a trials for list ~a~%"
                   total-trials-by-list index)
        (setf *total-trials-by-list* (append *total-trials-by-list* (list total-trials-by-list)))))

(defun print-out-proportion-correct-by-list-and-trial ()
  (loop for index in *indexes*
        for proportion-correct-by-trial 
        = (loop with number-of-presentations = (* (aref *number-of-pairs* index)
                                                  *number-of-sessions*
                                                  1.0)
                for n from 0
                for errata = (aref *qp-errors-array* n index)
                for proportion-correct = (when (> number-of-presentations 0)
                                           (/ (- number-of-presentations
                                                 errata)
                                              number-of-presentations))
                collect proportion-correct into result
                if (= errata 0) return result)
        do (format t "Proportion Correct by Trial for List ~a = ~a~%"
                   index proportion-correct-by-trial)
        (setf *proportion-correct-by-trial*
              (append *proportion-correct-by-trial* (list proportion-correct-by-trial)))))


(defun print-out-precriterion-proportion-correct-by-list-and-trial ()
  (loop for index in *indexes*
        for proportion-correct-by-trial
        = (loop for n from 0
                         for errata = (aref *pre-criterion-failure-array* n index)
                         for okdata = (aref *pre-criterion-success-array* n index)
                         for alldata = (+ errata okdata)
                         for proportion-correct = (when (> alldata 0)
                                                    (* 1.0
                                                     (/ okdata alldata)))
                         collect proportion-correct into result
                         if (= errata 0) return result)
        do (format t "Precriterion Proportion Correct by Trial for List ~a = ~a~%"
                   index
                   proportion-correct-by-trial)
        (setf *precriterion-proportion-correct-by-trial*
              (append *precriterion-proportion-correct-by-trial* 
                      (list proportion-correct-by-trial)))))

(defun print-out-response-latencies ()
  (loop for index in *indexes*
        for average-latency = 0
        for trials = 0
        for average-latency-when-correct = 0
        for trials-when-correct = 0
        for average-latency-when-incorrect = 0
        for trials-when-incorrect = 0
        if (aref *track-response-latencies* index)
        do
        (loop for n from 1 upto 25
              do 
              (setf average-latency (+ average-latency
                                       (aref *latency-array* n index)))
              (setf trials (+ trials
                              (aref *trials-array* n index)))
              (when (> (aref *trials-array* n index) 0)
                (format t "Average Latency for Trial ~a on list ~a = ~a msec (n = ~a).~%"
                        n
                        index
                        (* 1.0 (/ (aref *latency-array* n index)
                                  (aref *trials-array* n index)))
                        (aref *trials-array* n index)))
              (setf *average-latency*
                    (append *average-latency*
                            (list (when (> (aref *trials-array* n index) 0)
                                    (/ (aref *latency-array* n index)
                                       (aref *trials-array* n index)))
                                  ))))
        (loop for n from 1 upto 25
              do  
              (setf average-latency-when-correct (+ average-latency-when-correct
                                                    (aref *latency-array-when-correct* n index)))
              (setf trials-when-correct (+ trials-when-correct
                                           (aref *trials-array-when-correct* n index)))
              (when (> (aref *trials-array-when-correct* n index) 0)
                (format t "Average Latency When Correct for Trial ~a on List ~a = ~a msec (n = ~a).~%"
                        n 
                        index
                        (* 1.0 (/ (aref *latency-array-when-correct* n index)
                                  (aref *trials-array-when-correct* n index)))
                        (aref *trials-array-when-correct* n index)))
              (setf *average-latency-when-correct*
                    (append *average-latency-when-correct*
                            (list (when (> (aref *trials-array-when-correct* n index) 0)
                        (* 1.0 (/ (aref *latency-array-when-correct* n index)
                                  (aref *trials-array-when-correct* n index)))
                                  ))))
              )
        (loop for n from 1 upto 25
              do 
              (setf average-latency-when-incorrect (+ average-latency-when-incorrect
                                                      (aref *latency-array-when-incorrect* n index)))
              (setf trials-when-incorrect (+ trials-when-incorrect
                                             (aref *trials-array-when-incorrect* n index)))
              (when (>  (aref *trials-array-when-incorrect* n index) 0)
                (format t "Average Latency When Not Correct for Trial ~a on List ~a = ~a msec (n = ~a).~%"
                        n 
                        index
                        (* 1.0 (/ (aref *latency-array-when-incorrect* n index)
                                  (aref *trials-array-when-incorrect* n index)))
                        (aref *trials-array-when-incorrect* n index)))
              (setf *average-latency-when-incorrect*
                    (append *average-latency-when-incorrect*
                            (list (when (> (aref *trials-array-when-incorrect* n index) 0)
                        (* 1.0 (/ (aref *latency-array-when-incorrect* n index)
                                  (aref *trials-array-when-incorrect* n index)))
                                  ))))
              )
        (format t "trials=~a, trials-when-correct=~a, trials-when-incorrect=~a, last-error-trials=~a~%"
                trials trials-when-correct trials-when-incorrect (aref *last-error-trials* index))
        (when (> trials 0)
          (format t "Average Latency for List ~a = ~a (n = ~a)~%"
                  index
                  (* 1.0 (/ average-latency trials))
                  trials))
        (when (> trials-when-correct 0)
          (format t "Average Latency When Correct for List ~a = ~a (n = ~a)~%"
                  index
                  (* 1.0 (/ average-latency-when-correct trials-when-correct))
                  trials-when-correct))
        (when (> trials-when-incorrect 0)
          (format t "Average Latency When Incorrect for List ~a = ~a (n = ~a)~%"
                  index
                  (* 1.0 (/ average-latency-when-incorrect trials-when-incorrect))
                  trials-when-incorrect))
        (when (> (aref *last-error-trials* index) 0)
          (format t "Average Latency for List ~a Last Error = ~a (n = ~a)~%"
                  index
                  (* 1.0 (/ (aref *latency-for-last-error* index)
                            (aref *last-error-trials* index)))
                  (aref *last-error-trials* index))
          (setf *average-latency-of-last-error* 
                (append *average-latency-of-last-error*
                        (list (/ (aref *latency-for-last-error* index)
                            (aref *last-error-trials* index)))))
          )))

(defun print-out-backward-response-record ()
  "Note this response display won't work if a possible stimulus is t"
  (loop for index in *indexes*
        if (aref *backward-recall-trial* index)
        do 
        (setf *backward-response-record* 
              (append *backward-response-record*
                      (list (* 1.0 (/ (aref *total-backward-errors* index) 
                                      *number-of-sessions*)))))
        (format t 
                "Average number-of-errors in backward recall for List ~a trials was ~a~%"
                index (* 1.0 (/ (aref *total-backward-errors* index) 
                                                 *number-of-sessions*)))))

(defun print-out-mfr-response-record ()
  "Note this response display won't work if a possible stimulus is t"
  (loop for index in *indexes*
        if (aref *mfr-recall-trial* index)
        do 
        (format t "There was an average of ~a r0 responses -- not between 1 and 20 when not R1 R2 or R3-- for list ~a~%"
                (* 1.0 (/ (aref *r0-correct* index)
                          *number-of-sessions*))
                index)
        (setf *r0*
              (append *r0*
                      (list (/ (aref *r0-correct* index)
                               *number-of-sessions*))))
        (format t "There was an average of ~a r1 responses -- between 1 and 10 when not R2 -- for list ~a~%"
                (* 1.0 (/ (aref *r1-correct* index)
                          *number-of-sessions*))
                index)
        (setf *r1*
              (append *r1*
                      (list (/ (aref *r1-correct* index)
                               *number-of-sessions*))))
        (format t "There was an average of ~a R2 responses -- between 11 and 20 -- for list ~a~%"
                (* 1.0 (/ (aref *r2-correct* index)
                          *number-of-sessions*))
                index)
        (setf *r2*
              (append *r2*
                      (list (/ (aref *r2-correct* index)
                               *number-of-sessions*))))
        (format t "There was an average of ~a r3 responses -- between 1 and 20 when not R1 or R2 -- for list ~a~%"
                (* 1.0 (/ (aref *r3-correct* index)
                          *number-of-sessions*))
                index)
        (setf *r3*
              (append *r3*
                      (list (/ (aref *r3-correct* index)
                               *number-of-sessions*))))
        ))

(defun print-out-mmfr-response-record ()
  "Note this response display won't work if a possible stimulus is t"
  (loop for index in *indexes*
        if (aref *mmfr-recall-trial* index)
        do (format t "There was an average of ~a perfectly correct responses between 1 and 10 for list ~a~%"
                   (* 1.0 (/ (aref *one-to-ten-mmfr-correct-responses* index)
                             *number-of-sessions*))
                   index)
        (setf *perfectly-correct-from-1-to-10* 
              (append *perfectly-correct-from-1-to-10*
                      (list (/ (aref *one-to-ten-mmfr-correct-responses* index)
                             *number-of-sessions*))))
        (format t "There was an average of ~a correct but out of position responses between 1 and 10 for list ~a~%"
                (* 1.0 (/ (aref *moved-one-to-ten-mmfr-correct-responses* index)
                          *number-of-sessions*))
                index)
        (setf *correct-from-1-to-10* 
              (append *correct-from-1-to-10*
                      (list (/ (+ (aref *one-to-ten-mmfr-correct-responses* index)
                                  (aref *moved-one-to-ten-mmfr-correct-responses* index))
                               *number-of-sessions*))))

        (format t "There was an average of ~a perfectly correct responses between 11 and 20 for list ~a~%"
                (* 1.0 (/ (aref *eleven-to-twenty-mmfr-correct-responses* index)
                          *number-of-sessions*))
                index)
        (setf *perfectly-correct-from-11-to-20*
              (append *perfectly-correct-from-11-to-20*
                      (list (/ (aref *eleven-to-twenty-mmfr-correct-responses* index)
                          *number-of-sessions*))))
         (format t "There was an average of ~a correct but out of position responses between 11 and 20 for list ~a~%"
                (* 1.0 (/ (aref *moved-eleven-to-twenty-mmfr-correct-responses* index)
                          *number-of-sessions*))
                index)
        (setf *correct-from-11-to-20*
              (append *correct-from-11-to-20*
                      (list (/ (+ (aref *eleven-to-twenty-mmfr-correct-responses* index)
                                  (aref *moved-eleven-to-twenty-mmfr-correct-responses* index))
                          *number-of-sessions*))))
       (format t "There was an average of ~a total-responses between 1 and 10 for list ~a~%"
                (* 1.0 (/ (+ (aref *one-to-ten-mmfr-correct-responses* index)
                          (aref *one-to-ten-mmfr-incorrect-responses* index))
                   *number-of-sessions*))
                index)
        (setf *total-from-1-to-10*
              (append *total-from-1-to-10* 
                      (list (/ (+ (aref *one-to-ten-mmfr-correct-responses* index)
                                  (aref *one-to-ten-mmfr-incorrect-responses* index))
                               *number-of-sessions*))))
        (format t "There was an average of ~a total responses between 11 and 20 for list ~a~%"
                (* 1.0 (/ (+ (aref *eleven-to-twenty-mmfr-correct-responses* index)
                             (aref *eleven-to-twenty-mmfr-incorrect-responses* index))
                          *number-of-sessions*))
                index)
        (setf *total-from-11-to-20*
              (append *total-from-11-to-20*
                      (list (/ (+ (aref *eleven-to-twenty-mmfr-correct-responses* index)
                             (aref *eleven-to-twenty-mmfr-incorrect-responses* index))
                          *number-of-sessions*))))
        (format t "There was an average of ~a r0 responses -- not between 1 and 20 when not R1 R2 or R3-- for list ~a~%"
                (* 1.0 (/ (aref *r0-correct* index)
                          *number-of-sessions*))
                index)
        (setf *r0*
              (append *r0*
                      (list (/ (aref *r0-correct* index)
                               *number-of-sessions*))))
        (format t "There was an average of ~a r1 responses -- between 1 and 10 when not R2 -- for list ~a~%"
                (* 1.0 (/ (aref *r1-correct* index)
                          *number-of-sessions*))
                index)
        (setf *r1*
              (append *r1*
                      (list (/ (aref *r1-correct* index)
                               *number-of-sessions*))))
        (format t "There was an average of ~a R2 responses -- between 11 and 20 -- for list ~a~%"
                (* 1.0 (/ (aref *r2-correct* index)
                          *number-of-sessions*))
                index)
        (setf *r2*
              (append *r2*
                      (list (/ (aref *r2-correct* index)
                               *number-of-sessions*))))
        (format t "There was an average of ~a r3 responses -- between 1 and 20 when not R1 or R2 -- for list ~a~%"
                (* 1.0 (/ (aref *r3-correct* index)
                          *number-of-sessions*))
                index)
        (setf *r3*
              (append *r3*
                      (list (/ (aref *r3-correct* index)
                               *number-of-sessions*))))
        ))

(defun print-out-results-of-extra-recalls-tests ()
  (loop for index in *indexes*
        for number-of-presentations = (* (aref *number-of-pairs* index)
                                         *number-of-sessions*)
        if (aref *do-extra-recalls-test* index)
        do 
        (format t "~%Proportion of recalls of List ~a on first try:~%" index)
        (loop for i from (1- *lowest-extra-recalls-trial*) upto (1- *highest-extra-recalls-trial*)
              for errata = (aref *qp-errors-array* i index)
              for proportion-correct = (/ (- number-of-presentations
                                             errata)
                                          number-of-presentations)
              do (format t "For Trial ~a, proportion = ~a~%"
                         (1+ i)
                         (* 1.0 proportion-correct))
              (setf *proportion-of-recalls* 
                    (append *proportion-of-recalls* (list proportion-correct)))
              )
        (format t "~%Proportion of recalls of List ~a on second try:~%" index)
        
        (loop for i from (1- *lowest-extra-recalls-trial*) upto (1- *highest-extra-recalls-trial*)
              for correct = (aref *second-recall-correct* i index) 
              for incorrect =  (aref *second-recall-incorrect* i index)
              for total = (+ correct incorrect)
              do (format t "For Trial ~a, proportion = ~a~%"
                         (1+ i)
                         (when (> total 0)
                           (* 1.0 (/ correct total))))
              (setf *proportion-of-recalls* 
                    (append *proportion-of-recalls* (list (/ correct total))))
              )
        (format t  "~%Proportion of recalls of List ~a on third try:~%" index)
        (loop for i from (1- *lowest-extra-recalls-trial*) upto (1- *highest-extra-recalls-trial*)
              for correct = (aref *third-recall-correct* i index) 
              for incorrect =  (aref *third-recall-incorrect* i index)
              for total = (+ correct incorrect)
              do (format t "For Trial ~a, proportion = ~a~%"
                         (1+ i)
                         (when (> total 0)
                           (* 1.0 (/ correct total))))
              (setf *proportion-of-recalls* 
                    (append *proportion-of-recalls* (list (/ correct total))))
              )
        ))

(defun print-out-results-of-recognition-test ()
  (loop for index in *indexes*
        if (aref *do-recognition-test* index)
        do
        (format t
                "Proportion of recognition hits on List ~a when correct recall = ~a~%"
                index
                (* 1.0 (/ (aref *hit-when-correct* index) 
                          (+ (aref *hit-when-correct* index)
                             (aref *incorrect-rejection-when-correct* index)))))
        (setf *result-of-recognition-test* 
              (append *result-of-recognition-test* 
                      (list (* 1.0 (/ (aref *hit-when-correct* index) 
                                      (+ (aref *hit-when-correct* index)
                                         (aref *incorrect-rejection-when-correct* index)))))))
        (format t
                "Proportion of false-alarms hits on List ~a when correct recall = ~a~%"
                index
                (* 1.0 (/ (aref *false-alarm-when-correct* index) 
                          (+ (aref *false-alarm-when-correct* index)
                             (aref *correct-rejection-when-correct* index)))))
        (setf *result-of-recognition-test* 
              (append *result-of-recognition-test* 
                      (list (* 1.0 (/ (aref *false-alarm-when-correct* index) 
                                      (+ (aref *false-alarm-when-correct* index)
                                         (aref *correct-rejection-when-correct* index)))))))
        (format t
                "Proportion of recognition hits on List ~a when incorrect recall = ~a~%"
                index
                (* 1.0 (/ (aref *hit-when-incorrect* index) 
                          (+ (aref *hit-when-incorrect* index)
                             (aref *incorrect-rejection-when-incorrect* index)))))
        (setf *result-of-recognition-test* 
              (append *result-of-recognition-test* 
                      (list (* 1.0 (/ (aref *hit-when-incorrect* index) 
                                      (+ (aref *hit-when-incorrect* index)
                                         (aref *incorrect-rejection-when-incorrect* index)))))))
        (format t
                "Proportion of false-alarms hits on List ~a when incorrect recall = ~a~%"
                index
                (* 1.0 (/ (aref *false-alarm-when-incorrect* index) 
                          (+ (aref *false-alarm-when-incorrect* index)
                             (aref *correct-rejection-when-incorrect* index)))))
        (setf *result-of-recognition-test* 
              (append *result-of-recognition-test* 
                      (list (* 1.0 (/ (aref *false-alarm-when-incorrect* index) 
                                      (+ (aref *false-alarm-when-incorrect* index)
                                         (aref *correct-rejection-when-incorrect* index)))))))
        ))
  
(defun print-out-intrusions-record ()
  (loop for index in *indexes*
        if (aref *track-intrusions* index)
        do
        (format t "There was an average of ~a errors between 1 and 10 for list ~a~%"
                (* 1.0 (/ (aref *one-to-ten-errors* index)
                          *number-of-sessions*))
                index)
        (setf *errors-between-one-and-ten* 
              (append *errors-between-one-and-ten* 
                      (list (* 1.0 (/ (aref *one-to-ten-errors* index)
                                      *number-of-sessions*)))))
        (format t "There was an average of ~a errors between 11 and 20 for list ~a~%"
                (* 1.0 (/ (aref *eleven-to-twenty-errors* index)
                          *number-of-sessions*))
                index)
        (format t "There was an average of ~a errors between 21 and 30 for list ~a~%"
                (* 1.0 (/ (aref *twenty-one-to-thirty-errors* index)
                          *number-of-sessions*))
                index)))

(defun hintzman-experiment13 ()
  "This is a simulation of an experiment conducted by Barnes 
& Underwood (1959) as 
reported in the Journal of Experimental Psychology Vol. 58, 97-105, and
later simulated by Hintzman using SAL III."
  (format t "~%Hintzman Experiment 13. Unlearning and Modified Free Recall:~%")
  (format t "Barnes and Underwood Fate of List 1 Associations Experiment:~%")
  (format t "List 1 associated with digits from 1 to 8, List 2 with digits from 11 to 18.~%")
  (seed-random-generator)
  (setf *perfectly-correct-from-1-to-10* nil)
  (setf *perfectly-correct-from-11-to-20* nil)
  (setf *correct-from-1-to-10* nil)
  (setf *correct-from-11-to-20* nil)
  (setf *total-from-1-to-10* nil)
  (setf *total-from-11-to-20* nil)
  (reset-data-collection-arrays)
  (setf *noticing-order* nil)
  (setf *print-major-updates* t)
  (setf *presentation-rate* 4000)
  (setf *inter-trial-interval* 4000)
  (setf *number-of-sessions* 1000)
  (setf *guessing-routine* nil)
  (setf (aref *mmfr-recall-trial* 2) t)
  (setf *indexes* '(1 2))
  (loop for list-or-routine = *barnes-and-underwood*
        for condition in '(1 5 10 20)
        do (format t "~%Name-of-experiment = ~a Trials of List 2 Learning~%" condition)
        (initialize-results-arrays)
        (loop repeat *number-of-sessions*
              for syllable-lists = (cond ((listp list-or-routine) list-or-routine)
                                         ('else (apply list-or-routine nil)))
              do (multi-list-session syllable-lists
                                     '(consecutive-trials-criteria
                                       absolute-trials-criteria )
                                     `(1 ,condition)
                                     '(60000 0)
			             '(1 2)))
        (print-paired-associate-results))
  (format t "~%Responses recalled in the AB-AC paradigm as a function of trials on second list.~%")
  (format t "using the perfectly correct scoring method counting EPAM List1 items correct if either.~%")
   (format t "they were elicited second or if they were the only response made and an absolute~%")
   (format t "time judgement puts the recency of the episode out of the time period of the current~%")
   (format t "list. Data for people is from Barnes and Underwood (1959).~%~%")
  (format t "Second-list-Trials         People           EPAM6~%")
  (format t "                       --------------   --------------~%")
  (format t "                        List1  List2     List1  List2~%")
  (format t "       1                 6.67   3.46     ~5,2f  ~5,2f~%"
          (nth 0 *perfectly-correct-from-1-to-10*) (nth 0 *perfectly-correct-from-11-to-20*))
  (format t "       5                 5.38   6.29     ~5,2f  ~5,2f~%"
          (nth 1 *perfectly-correct-from-1-to-10*) (nth 1 *perfectly-correct-from-11-to-20*))
  (format t "      10                 5.00   7.33     ~5,2f  ~5,2f~%"
          (nth 2 *perfectly-correct-from-1-to-10*) (nth 2 *perfectly-correct-from-11-to-20*))
  (format t "      20                 4.12   7.38     ~5,2f  ~5,2f~%"
          (nth 3 *perfectly-correct-from-1-to-10*) (nth 3 *perfectly-correct-from-11-to-20*))
   (format t "~%Responses recalled in the AB-AC paradigm as a function of trials on second list.~%")
  (format t "Data for both people and EPAM use the loose scoring method with correctness not important.~%~%")
  (format t "Second-list-Trials         People           EPAM6~%")
  (format t "                       --------------   --------------~%")
  (format t "                        List1  List2     List1  List2~%")
  (format t "       1                 6.96   4.12     ~5,2f  ~5,2f~%"
          (nth 0 *total-from-1-to-10*) (nth 0 *total-from-11-to-20*))
  (format t "       5                 5.58   6.71     ~5,2f  ~5,2f~%"
          (nth 1 *total-from-1-to-10*) (nth 1 *total-from-11-to-20*))
  (format t "      10                 5.42   7.71     ~5,2f  ~5,2f~%"
          (nth 2 *total-from-1-to-10*) (nth 2 *total-from-11-to-20*))
  (format t "      20                 4.29   7.42     ~5,2f  ~5,2f~%"
          (nth 3 *total-from-1-to-10*) (nth 3 *total-from-11-to-20*))
)

          

(defun hintzman-experiment14 ()
  "This is a partial simulation of an experiment conducted by Briggs (1954) as 
reported in the Journal of Experimental Psychology Vol. 47, 285-293, and
later simulated by Hintzman using SAL III.  Briggs however used 
adjectives as his stimuli and responses.  This simulation uses nonsense
syllables as stimuli and numbers as responses  Briggs stimuli were chosen
such that they would each already have responses associated with them.
We simulate that by having the system study an original list of 
responses."
  (format t "~%Hintzman Experiment 14. Retroactive vs. Proactive Interference.~%")
  (format t "Briggs, 1954, experiment from Journal of Exp Psych, 47, 285-293:~%")
  (format t "Assume that Original associates represented by R0 are digits from 20 to 30.~%")
  (format t "Assume that the R1 list studied 24 hours earlier are ditits from 1 to 10.~%")
  (format t "Assume that the R2 list are digits from 11 to 20.~%")
  (seed-random-generator)
   (setf *long-term-forgetting* t)
  (setf *r0* nil)
  (setf *r1* nil)
  (setf *r2* nil)
  (setf *r3* nil)
  (reset-data-collection-arrays)
  (setf *noticing-order* nil)
  (setf *print-major-updates* t)
  (setf *presentation-rate* 4000)
  (setf *inter-trial-interval* 4000)
  (setf *number-of-sessions* 1000) 
  (setf *guessing-routine* nil)
  (setf (aref *track-intrusions* 4) t)
  (setf *indexes* '(1 2 3 4))
  (setf (aref *mfr-recall-trial* 4) t)
  (loop for list-or-routine = *briggs*
        for name-of-experiment in '(4-minutes 6-hours 1-day 2-days 3-days)         
        for condition in '(240000 21600000 86400000 172800000 259200000)
        do (format t "~%Recall after ~a~%" name-of-experiment)
        (initialize-results-arrays)
        (loop repeat *number-of-sessions*
              for syllable-lists = (cond ((listp list-or-routine) list-or-routine)
                                         ('else (apply list-or-routine nil)))
              do (multi-list-session syllable-lists
                                     '(absolute-trials-criteria
                                       consecutive-trials-criteria
                                       consecutive-trials-criteria
                                       absolute-trials-criteria )
                                     '(1 1 1 0)
                                     `(31536000000 8640000 ,condition 0)
			             '(1 2 3 4)))
        (print-paired-associate-results))
   (format t "~%In this experiment an AB stimulus-response list was taught first,~%")
   (format t "then 24 hours later a second stimulus-response list was taught~%")
   (format t "using the same stimuli.   Then after various time intervals~%")
   (format t "with different subjects an MFR test was given in which subjects~%")
   (format t "were asked to respond with the first response word that came~%")
   (format t "into their mind. Data for people is from Briggs, 1954, ~%")
   (format t "using adjectives as stimuli and responses.  The EPAM simulation used~%")
   (format t "nonsense syllables as stimuli and numbers as responses.~%")
   (format t "~%Frequency for Response Classes by retention interval.~%")
   (format t "   Class              People                       EPAM6~%")
   (format t "            ----------------------------  ----------------------------~%")
   (format t "            4min   6hr  24hr  48hr  72hr  4min   6hr  24hr  48hr  72hr~%")
   (format t "            ----------------------------  ----------------------------~%")
   (format t "List 0      0.36  1.40  0.84  1.80  2.12 ~5,2f ~5,2f ~5,2f ~5,2f ~5,2f~%"
     (* 1.2 (nth 0 *r0*)) 
     (* 1.2 (nth 1 *r0*)) 
     (* 1.2 (nth 2 *r0*)) 
     (* 1.2 (nth 3 *r0*)) 
     (* 1.2 (nth 4 *r0*)))
   (format t "List 1      2.56  4.36  5.32  4.96  5.04 ~5,2f ~5,2f ~5,2f ~5,2f ~5,2f~%"
     (* 1.2 (nth 0 *r1*)) 
     (* 1.2 (nth 1 *r1*)) 
     (* 1.2 (nth 2 *r1*)) 
     (* 1.2 (nth 3 *r1*)) 
     (* 1.2 (nth 4 *r1*)))
   (format t "List 2      8.20  5.20  4.80  4.08  3.80 ~5,2f ~5,2f ~5,2f ~5,2f ~5,2f~%"
     (* 1.2 (nth 0 *r2*)) 
     (* 1.2 (nth 1 *r2*)) 
     (* 1.2 (nth 2 *r2*)) 
     (* 1.2 (nth 3 *r2*)) 
     (* 1.2 (nth 4 *r2*)))
   (format t "Other       0.88  1.04  1.04  1.16  1.04 ~5,2f ~5,2f ~5,2f ~5,2f ~5,2f~%"
     (* 1.2 (nth 0 *r3*)) 
     (* 1.2 (nth 1 *r3*)) 
     (* 1.2 (nth 2 *r3*)) 
     (* 1.2 (nth 3 *r3*)) 
     (* 1.2 (nth 4 *r3*)))
   )

; This variable will hold an association list such that all of the locations are
; associated with a string representing the person and all of the people are 

(defvar *anderson-and-reder-answers* nil)
(defvar *anderson-and-reder-people* nil)
(defvar *anderson-and-reder-places* nil)


(defun initialize-anderson-1974 ()
  (setf *anderson-and-reder-answers* (list nil))
  (setf *anderson-and-reder-people* nil)
  (setf *anderson-and-reder-places* nil)
  (loop for target in *anderson-1974-study-set*
        for person = (first target)
        for location = (second target)
        do 
        (add-assoc *anderson-and-reder-answers* person location)
        (add-assoc *anderson-and-reder-answers* location person)
        if (not (member person *anderson-and-reder-people* :test #'equal))
        do (setf *anderson-and-reder-people* (cons person *anderson-and-reder-people*))
        if (not (member location *anderson-and-reder-places* :test #'equal))
        do (setf *anderson-and-reder-places* (cons location *anderson-and-reder-places*))))


(defun initialize-anderson-and-reder ()
  (setf *anderson-and-reder-answers* (list nil))
  (setf *anderson-and-reder-people* nil)
  (setf *anderson-and-reder-places* nil)
  (loop for target in *anderson-and-reder-targets*
        for person = (first target)
        for location = (second target)
        do 
        (add-assoc *anderson-and-reder-answers* person location)
        (add-assoc *anderson-and-reder-answers* location person)
        if (not (member person *anderson-and-reder-people* :test #'equal))
        do (setf *anderson-and-reder-people* (cons person *anderson-and-reder-people*))
        if (not (member location *anderson-and-reder-places* :test #'equal))
        do (setf *anderson-and-reder-places* (cons location *anderson-and-reder-places*))))
                
(defun add-assoc (object value attribute)
  "This routine is like put-value except that it assumes that each attribute has 
a list of values and permits values to be lists or strings."
  (let* ((alist (car object))
         (old-value (second (assoc attribute alist :test #'equal))))
    (cond 
     ((null old-value) (rplaca object (cons (list attribute (list value)) alist)))
     ((member value old-value :test #'equal) nil)
     ('else (nconc old-value (list value))))))

(defun get-assoc (object attribute)
  (second (assoc attribute (car object) :test #'equal)))
       
  (defvar *facilitation-errors* (make-array 5 :initial-element 0))
  (defvar *interference-errors* (make-array 5 :initial-element 0))
  (defvar *suppression-errors* (make-array 5 :initial-element 0))
  (defvar *control-errors* (make-array 5 :initial-element 0))
  (defvar *mixed-errors* (make-array 5 :initial-element 0))
  (defvar *high-errors* (make-array 5 :initial-element 0))
  (defvar *low-errors* (make-array 5 :initial-element 0))
  (defvar *facilitation-latencies* (make-array 5 :initial-element nil))
  (defvar *interference-latencies* (make-array 5 :initial-element nil))
  (defvar *suppression-latencies* (make-array 5 :initial-element nil))
  (defvar *control-latencies* (make-array 5 :initial-element nil))
  (defvar *mixed-latencies* (make-array 5 :initial-element nil))
  (defvar *high-latencies* (make-array 5 :initial-element nil))
  (defvar *low-latencies* (make-array 5 :initial-element nil))
  (defvar *low-latencies-when-correct* (make-array 5 :initial-element nil))
  (defvar *facilitation-latencies-when-correct* (make-array 5 :initial-element nil))
  (defvar *interference-latencies-when-correct* (make-array 5 :initial-element nil))
  (defvar *suppression-latencies-when-correct* (make-array 5 :initial-element nil))
  (defvar *control-latencies-when-correct* (make-array 5 :initial-element nil))
  (defvar *mixed-latencies-when-correct* (make-array 5 :initial-element nil))
  (defvar *high-latencies-when-correct* (make-array 5 :initial-element nil))
  (defvar *facilitation-number-of-episodes-when-correct* (make-array 5 :initial-element nil))
  (defvar *interference-number-of-episodes-when-correct* (make-array 5 :initial-element nil))
  (defvar *suppression-number-of-episodes-when-correct* (make-array 5 :initial-element nil))
  (defvar *control-number-of-episodes-when-correct* (make-array 5 :initial-element nil))
  (defvar *mixed-number-of-episodes-when-correct* (make-array 5 :initial-element nil))
  (defvar *high-number-of-episodes-when-correct* (make-array 5 :initial-element nil))
  (defvar *low-number-of-episodes-when-correct* (make-array 5 :initial-element nil))
  (defvar *second-phase-trials-array*)
  (defvar *facilitation-position-of-episode-when-correct* (make-array 5 :initial-element nil))
  (defvar *interference-position-of-episode-when-correct* (make-array 5 :initial-element nil))
  (defvar *suppression-position-of-episode-when-correct* (make-array 5 :initial-element nil))
  (defvar *control-position-of-episode-when-correct* (make-array 5 :initial-element nil))
  (defvar *mixed-position-of-episode-when-correct* (make-array 5 :initial-element nil))
  (defvar *high-position-of-episode-when-correct* (make-array 5 :initial-element nil))
  (defvar *low-position-of-episode-when-correct* (make-array 5 :initial-element nil))
; These variables hold results of anderson-1974
(defvar *true-latencies* (make-array '(5 5) :initial-element nil))
(defvar *false-latencies* (make-array '(5 5) :initial-element nil))
(defvar *true-latencies-when-correct* (make-array '(5 5) :initial-element nil))
(defvar *false-latencies-when-correct* (make-array '(5 5) :initial-element nil))
(defvar *true-number-of-episodes-when-correct* (make-array '(5 5) :initial-element nil))
(defvar *false-number-of-episodes-when-correct* (make-array '(5 5) :initial-element nil))
(defvar *true-position-of-episode-when-correct* (make-array '(5 5) :initial-element nil))
(defvar *false-position-of-episode-when-correct* (make-array '(5 5) :initial-element nil))
(defvar *true-errors* (make-array '(5 5) :initial-element 0))
(defvar *false-errors* (make-array '(5 5) :initial-element 0))


(defun anderson-1974 ()
  "This is Anderson's classic fan effect experiment from 
Cognitive Psychology, 6, 451-474."
   (format t "~%Anderson, 1974, Fan Effect Experiment~%")
   (seed-random-generator)
   (reset-data-collection-arrays)
   (setf *fixed-time-required-for-recognition-and-response*
         *fixed-time-required-in-anderson-1974*)
   (setf *noticing-order* nil)
   (setf *index* 1)
   (setf *print-major-updates* t)
   (setf *presentation-rate* 5000)
   (setf *inter-trial-interval* 1000)
   (setf *number-of-sessions* 1000) 
   (setf *guessing-routine* nil)
   (initialize-results-arrays)
   ; first record is number of trials to correct result
   (setf *second-phase-trials-array* (make-array 2 :initial-element nil))
   ; records are number of errors for 2 fan and number of errors for 4 fan
   (setf *true-latencies* (make-array '(5 5) :initial-element nil))
   (setf *false-latencies* (make-array '(5 5) :initial-element nil))
   (setf *true-latencies-when-correct* (make-array '(5 5) :initial-element nil))
   (setf *false-latencies-when-correct* (make-array '(5 5) :initial-element nil))
   (setf *true-number-of-episodes-when-correct* (make-array '(5 5) :initial-element nil))
   (setf *false-number-of-episodes-when-correct* (make-array '(5 5) :initial-element nil))
   (setf *true-position-of-episode-when-correct* (make-array '(5 5) :initial-element nil))
   (setf *false-position-of-episode-when-correct* (make-array '(5 5) :initial-element nil))
   (setf *true-errors* (make-array '(5 5) :initial-element 0))
   (setf *false-errors* (make-array '(5 5) :initial-element 0))
   (setf *trial* 0)
   (change-focus-to nil)
   (initialize-anderson-1974)
   (loop repeat *number-of-sessions*
     do (anderson-1974-trial))
   (report-anderson-1974-results)
   (format t "~%Mean Reaction Times (in Seconds) for TARGETS for Fans of 1, 2, and 3.~%")
   (format t "Act-R data are from Anderson and Reder, 1999.~%")
   (format t "Human data are from Anderson 1974, Cognitive Psychology, 6, 451-474.~%")
   (format t "~%")
   (format t "Location     People              ACT-R              EPAM6~%")
   (format t "  Fan   -----------------  -----------------  -----------------~%")
   (format t "         Fan per Person     Fan per Person     Fan per Person~%")   
   (format t "        Fan-1 Fan-2 Fan-3  Fan-1 Fan-2 Fan-3  Fan-1 Fan-2 Fan-3~%")
   (format t "        -----------------  -----------------  -----------------~%")
   (format t "   1     1.11  1.17  1.22   1.11  1.15  1.18  ~5,2f ~5,2f ~5,2f~%"
     (* (nth 2 *overall-results*) .001) 
     (* (nth 32 *overall-results*) .001)
     (* (nth 62 *overall-results*) .001))
   (format t "   2     1.17  1.20  1.22   1.15  1.22  1.26  ~5,2f ~5,2f ~5,2f~%"
     (* (nth 12 *overall-results*) .001) 
     (* (nth 42 *overall-results*) .001)
     (* (nth 72 *overall-results*) .001))
   (format t "   3     1.15  1.23  1.36   1.18  1.26  1.33  ~5,2f ~5,2f ~5,2f~%"
     (* (nth 22 *overall-results*) .001) 
     (* (nth 52 *overall-results*) .001)
     (* (nth 82 *overall-results*) .001))
   (format t "~%Mean Reaction Times (in Seconds) for FOILS for Fans of 1, 2, and 3.~%")
   (format t "Act-R data are from Anderson and Reder, 1999.~%")
   (format t "Human data are from Anderson 1974, Cognitive Psychology, 6, 451-474.~%")
   (format t "~%")
   (format t "Location     People              ACT-R              EPAM6~%")
   (format t "  Fan   -----------------  -----------------  -----------------~%")
   (format t "         Fan per Person     Fan per Person     Fan per Person~%")   
   (format t "        Fan-1 Fan-2 Fan-3  Fan-1 Fan-2 Fan-3  Fan-1 Fan-2 Fan-3~%")
   (format t "        -----------------  -----------------  -----------------~%")
   (format t "   1     1.20  1.22  1.26   1.22  1.27  1.31  ~5,2f ~5,2f ~5,2f~%"
     (* (nth 3 *overall-results*) .001) 
     (* (nth 33 *overall-results*) .001)
     (* (nth 63 *overall-results*) .001))
   (format t "   2     1.25  1.36  1.29   1.27  1.32  1.36  ~5,2f ~5,2f ~5,2f~%"
     (* (nth 13 *overall-results*) .001) 
     (* (nth 43 *overall-results*) .001)
     (* (nth 73 *overall-results*) .001))
   (format t "   3     1.26  1.47  1.47   1.31  1.36  1.39  ~5,2f ~5,2f ~5,2f~%"
     (* (nth 23 *overall-results*) .001) 
     (* (nth 53 *overall-results*) .001)
     (* (nth 83 *overall-results*) .001)))



(defun anderson-and-reder-1999 ()
   "This is Anderson and Reder's Fan Effect Experiment from 
 JEP:General, Val 12, pages 186-197"
   (format t "~%Anderson & Reder, 1999, Fan Effect Experiment~%")
   (seed-random-generator)
   (reset-data-collection-arrays)
   (setf *fixed-time-required-for-recognition-and-response*
         *fixed-time-required-in-anderson-and-reder-1999*)
   (setf *noticing-order* nil)
   (setf *index* 1)
   (setf *print-major-updates* t)
   (setf *presentation-rate* 5000)
   (setf *inter-trial-interval* 1000)
   (setf *number-of-sessions* 1000) 
   (setf *guessing-routine* nil)
   (initialize-results-arrays)
   ; first record is number of trials to correct result
   (setf *second-phase-trials-array* (make-array 2 :initial-element nil))
   ; records are number of errors for 2 fan and number of errors for 4 fan
   (setf *facilitation-errors* (make-array 5 :initial-element 0))
   (setf *interference-errors* (make-array 5 :initial-element 0))
   (setf *suppression-errors* (make-array 5 :initial-element 0))
   (setf *control-errors* (make-array 5 :initial-element 0))
   (setf *mixed-errors* (make-array 5 :initial-element 0))
   (setf *high-errors* (make-array 5 :initial-element 0))
    (setf *low-errors* (make-array 5 :initial-element 0))
    (setf *facilitation-latencies* (make-array 5 :initial-element nil))
    (setf *interference-latencies* (make-array 5 :initial-element nil))
    (setf *suppression-latencies* (make-array 5 :initial-element nil))
    (setf *control-latencies* (make-array 5 :initial-element nil))
    (setf *mixed-latencies* (make-array 5 :initial-element nil))
    (setf *high-latencies* (make-array 5 :initial-element nil))
    (setf *low-latencies* (make-array 5 :initial-element nil))
    (setf *facilitation-latencies-when-correct* (make-array 5 :initial-element nil))
    (setf *interference-latencies-when-correct* (make-array 5 :initial-element nil))
    (setf *suppression-latencies-when-correct* (make-array 5 :initial-element nil))
    (setf *control-latencies-when-correct* (make-array 5 :initial-element nil))
    (setf *mixed-latencies-when-correct* (make-array 5 :initial-element nil))
    (setf *high-latencies-when-correct* (make-array 5 :initial-element nil))
    (setf *low-latencies-when-correct* (make-array 5 :initial-element nil))
    (setf *facilitation-position-of-episode-when-correct* (make-array 5 :initial-element nil))
    (setf *interference-position-of-episode-when-correct* (make-array 5 :initial-element nil))
    (setf *suppression-position-of-episode-when-correct* (make-array 5 :initial-element nil))
    (setf *control-position-of-episode-when-correct* (make-array 5 :initial-element nil))
    (setf *mixed-position-of-episode-when-correct* (make-array 5 :initial-element nil))
    (setf *high-position-of-episode-when-correct* (make-array 5 :initial-element nil))
    (setf *low-position-of-episode-when-correct* (make-array 5 :initial-element nil))
    (setf *trial* 0)
    (change-focus-to nil)
    (initialize-anderson-and-reder)
    (loop repeat *number-of-sessions*
          do (anderson-and-reder-trial))
    (report-anderson-and-reder-results)
    (format t "Human data and ACT-R Data are from Anderson & Reder, 1999, Journal of Experimental Psychology, 128, 186-197.~%")
    (format t "~%Mean Latencies in seconds & Error Rates in percent for 2 and 4 Fans~%")
    (format t "Condition          People       Act-R        EPAM6~%")
    (format t "                 -----------  -----------  -----------~%")
    (format t "                 Fan-2 Fan-4  Fan-2 Fan-4  Fan-2 Fan-4~%")
    (format t "                 -----------  -----------  -----------~%")
    (format t "Targets ~%")       
    (format t "  Facilitation~%")
    (format t "    Mean latency  1.33  1.42   1.36  1.41  ~5,2f ~5,2f~%"
            (* .001 (nth 10 *overall-results*)) (* .001 (nth 55 *overall-results*)))
    (format t "    Error rate    6.2   6.0               ~5,1f ~5,1f~%"
            (* (nth 1 *overall-results*) 10) (* (nth 46 *overall-results*) 10))
    (format t "  Interference~%")
    (format t "    Mean latency  1.57  1.58   1.53  1.62  ~5,2f ~5,2f~%"
            (* .001 (nth 11 *overall-results*)) (* .001 (nth 56 *overall-results*)))
    (format t "    Error rate   10.5  14.0               ~5,1f ~5,1f~%"
            (* (nth 2 *overall-results*) 10) (* (nth 47 *overall-results*) 10))
    (format t "  Suppression~%") 
    (format t "    Mean latency  1.43  1.51   1.43  1.49  ~5,2f ~5,2f~%"
            (* .001 (nth 12 *overall-results*)) (* .001 (nth 57 *overall-results*)))
    (format t "    Error rate    4.9   7.8               ~5,1f ~5,1f~%"
            (* (nth 3 *overall-results*) 10) (* (nth 48 *overall-results*) 10))
    (format t "  Control~%")
    (format t "    Mean latency  1.42  1.53   1.43  1.49  ~5,2f ~5,2f~%"
            (* .001 (nth 13 *overall-results*)) (* .001 (nth 58 *overall-results*)))
    (format t "    Error rate    5.5   8.9               ~5,1f ~5,1f~%"
            (* (nth 4 *overall-results*) 10) (* (nth 49 *overall-results*) 10))
    (format t "Foils~%")          
    (format t "  High~%")
    (format t "    Mean latency  1.51  1.60   1.55  1.60  ~5,2f ~5,2f~%"
            (* .001 (nth 15 *overall-results*)) (* .001 (nth 60 *overall-results*)))
    (format t "    Error rate    7.5   9.9               ~5,1f ~5,1f~%"
            (* (nth 6 *overall-results*) 10) (* (nth 51 *overall-results*) 10))
    (format t "  Mixed~%") 
    (format t "    Mean latency  1.58  1.67   1.59  1.64  ~5,2f ~5,2f~%"
            (* .001 (nth 16 *overall-results*)) (* .001 (nth 61 *overall-results*)))
    (format t "    Error rate    8.3  13.8               ~5,1f ~5,1f~%"
            (* (nth 7 *overall-results*) 10) (* (nth 52 *overall-results*) 10))
    (format t "  Low~%")
    (format t "    Mean latency  1.59  1.70   1.62  1.68  ~5,2f ~5,2f~%"
            (* .001 (nth 17 *overall-results*)) (* .001 (nth 62 *overall-results*)))
    (format t "    Error rate   11.7  14.4               ~5,1f ~5,1f~%"
            (* (nth 8 *overall-results*) 10) (* (nth 53 *overall-results*) 10))
  )

(defun anderson-1974-trial ()
   (initialize-variables)
   (anderson-and-reder-first-phase *anderson-1974-study-set*)
   (setf *index* 0)
   (anderson-and-reder-second-phase) 
   (setf *index* 1)
   (anderson-and-reder-second-phase)
   (setf *presentation-rate* 1000)
   (anderson-1974-third-phase))
  

          

(defun anderson-and-reder-trial ()
  (initialize-variables)
  (anderson-and-reder-first-phase *anderson-and-reder-targets*)
  (setf *index* 0)
  (anderson-and-reder-second-phase)
  (setf *index* 1)
  (anderson-and-reder-second-phase)
  (setf *presentation-rate* 1000)
  (anderson-and-reder-third-phase))

(defun report-anderson-and-reder-results ()
  (setf *overall-results* nil)
  (loop for index in '(0 1)
        do (format t "During run ~a of second phase, items averaged ~a trials.~%"
                   (1+ index) (find-phase2-arerrors index)))
  (loop for fan in '(2 4)
        for facilitation-errors = (aref *facilitation-errors* fan)
        for interference-errors = (aref *interference-errors* fan)
        for suppression-errors = (aref *suppression-errors* fan)
        for control-errors = (aref *control-errors* fan)
        for mixed-errors = (aref *mixed-errors* fan)
        for high-errors = (aref *high-errors* fan)
        for low-errors = (aref *low-errors* fan)
        for target-errors = (+ facilitation-errors
                               interference-errors
                               suppression-errors
                               control-errors)
        for foil-errors = (+ mixed-errors
                             high-errors
                             low-errors)
        for facilitation-latencies = (aref *facilitation-latencies* fan)
        for interference-latencies = (aref *interference-latencies* fan)
        for suppression-latencies = (aref *suppression-latencies* fan)
        for control-latencies = (aref *control-latencies* fan)
        for mixed-latencies = (aref *mixed-latencies* fan)
        for high-latencies = (aref *high-latencies* fan)
        for low-latencies = (aref *low-latencies* fan)
        for target-latencies = (append facilitation-latencies
                                       interference-latencies
                                       suppression-latencies
                                       control-latencies)
        for foil-latencies = (append mixed-latencies
                                     high-latencies
                                     low-latencies)
        for facilitation-latencies-when-correct = (aref *facilitation-latencies-when-correct* fan)
        for interference-latencies-when-correct = (aref *interference-latencies-when-correct* fan)
        for suppression-latencies-when-correct = (aref *suppression-latencies-when-correct* fan)
        for control-latencies-when-correct = (aref *control-latencies-when-correct* fan)
        for mixed-latencies-when-correct = (aref *mixed-latencies-when-correct* fan)
        for high-latencies-when-correct = (aref *high-latencies-when-correct* fan)
        for low-latencies-when-correct = (aref *low-latencies-when-correct* fan)
        for target-latencies-when-correct = (append facilitation-latencies-when-correct
                                                    interference-latencies-when-correct
                                                    suppression-latencies-when-correct
                                                    control-latencies-when-correct)
        for foil-latencies-when-correct = (append mixed-latencies-when-correct
                                                  high-latencies-when-correct
                                                  low-latencies-when-correct)
        for facilitation-number-of-episodes-when-correct = (aref *facilitation-number-of-episodes-when-correct* fan)
        for interference-number-of-episodes-when-correct = (aref *interference-number-of-episodes-when-correct* fan)
        for suppression-number-of-episodes-when-correct = (aref *suppression-number-of-episodes-when-correct* fan)
        for control-number-of-episodes-when-correct = (aref *control-number-of-episodes-when-correct* fan)
        for mixed-number-of-episodes-when-correct = (aref *mixed-number-of-episodes-when-correct* fan)
        for high-number-of-episodes-when-correct = (aref *high-number-of-episodes-when-correct* fan)
        for low-number-of-episodes-when-correct = (aref *low-number-of-episodes-when-correct* fan)
        for target-number-of-episodes-when-correct = (append facilitation-number-of-episodes-when-correct
                                                             interference-number-of-episodes-when-correct
                                                             suppression-number-of-episodes-when-correct
                                                             control-number-of-episodes-when-correct)
        for foil-number-of-episodes-when-correct = (append mixed-number-of-episodes-when-correct
                                                           high-number-of-episodes-when-correct
                                                           low-number-of-episodes-when-correct)
        for facilitation-position-of-episode-when-correct = (aref *facilitation-position-of-episode-when-correct* fan)
        for interference-position-of-episode-when-correct = (aref *interference-position-of-episode-when-correct* fan)
        for suppression-position-of-episode-when-correct = (aref *suppression-position-of-episode-when-correct* fan)
        for control-position-of-episode-when-correct = (aref *control-position-of-episode-when-correct* fan)
        for mixed-position-of-episode-when-correct = (aref *mixed-position-of-episode-when-correct* fan)
        for high-position-of-episode-when-correct = (aref *high-position-of-episode-when-correct* fan)
        for low-position-of-episode-when-correct = (aref *low-position-of-episode-when-correct* fan)
        for target-position-of-episode-when-correct = (append facilitation-position-of-episode-when-correct
                                                              interference-position-of-episode-when-correct
                                                              suppression-position-of-episode-when-correct
                                                              control-position-of-episode-when-correct)
        for foil-position-of-episode-when-correct = (append mixed-position-of-episode-when-correct
                                                            high-position-of-episode-when-correct
                                                            low-position-of-episode-when-correct)
        do 
        (format t "~%~%Phase 3 Results for Fan = ~a:~%" fan)
        (format t "Target Error Rate = ~a~%"
                (/ target-errors (* 1.0 (length target-latencies))))
        (setf *overall-results* 
              (append *overall-results* 
                      (list (/ target-errors (length target-latencies)))))
        (format t "Facilitation Error Rate = ~a~%"
                (/ facilitation-errors (* 1.0 (length facilitation-latencies))))
        (setf *overall-results* 
              (append *overall-results* 
                      (list (/ facilitation-errors (length facilitation-latencies)))))
        (format t "Interference Error Rate = ~a~%"
                (/ interference-errors (* 1.0 (length interference-latencies))))
        (setf *overall-results* 
              (append *overall-results* 
                      (list (/ interference-errors (length interference-latencies)))))
        (format t "Suppression Error Rate = ~a~%"
                (/ suppression-errors (* 1.0 (length suppression-latencies))))
        (setf *overall-results* 
              (append *overall-results* 
                      (list (/ suppression-errors (length suppression-latencies)))))
        (format t "Control Error Rate = ~a~%"
                (/ control-errors (* 1.0 (length control-latencies))))
        (setf *overall-results* 
              (append *overall-results* 
                      (list (/ control-errors (* 1.0 (length control-latencies))))))
        (format t "Foil Error Rate = ~a~%"
                (/ foil-errors (* 1.0 (length foil-latencies))))
        (setf *overall-results* 
              (append *overall-results* 
                      (list (/ foil-errors (* 1.0 (length foil-latencies))))))
        (format t "High Error Rate = ~a~%"
                (/ high-errors (* 1.0 (length high-latencies))))
        (setf *overall-results* 
              (append *overall-results* 
                      (list (/ high-errors (* 1.0 (length high-latencies))))))
        (format t "Mixed Error Rate = ~a~%"
                (/ mixed-errors (* 1.0 (length mixed-latencies))))
        (setf *overall-results* 
              (append *overall-results* 
                      (list (/ mixed-errors (* 1.0 (length mixed-latencies))))))
        (format t "Low Error Rate = ~a~%"
                (/ low-errors (* 1.0 (length low-latencies))))
        (setf *overall-results* 
              (append *overall-results* 
                      (list (/ low-errors (* 1.0 (length low-latencies))))))
        (format t "Average Target Latency = ~a~%"
                (loop with sum = 0
                      for latency in target-latencies
                      for n from 1
                      do (setf sum (+ sum latency))
                      finally (return (/ sum (* n 1.0)))))
        (setf *overall-results* 
              (append *overall-results* (list 
                                         (loop with sum = 0
                                               for latency in target-latencies
                                               for n from 1
                                               do (setf sum (+ sum latency))
                                               finally (return (/ sum (* n 1.0)))))))
        (format t "Average facilitation Latency = ~a~%"
                (loop with sum = 0
                      for latency in facilitation-latencies
                      for n from 1
                      do (setf sum (+ sum latency))
                      finally (return (/ sum (* n 1.0)))))
        (setf *overall-results* 
              (append *overall-results* (list 
                                         (loop with sum = 0
                                               for latency in facilitation-latencies
                                               for n from 1
                                               do (setf sum (+ sum latency))
                                               finally (return (/ sum (* n 1.0)))))))
        
        (format t "Average interference Latency = ~a~%"
                (loop with sum = 0
                      for latency in interference-latencies
                      for n from 1
                      do (setf sum (+ sum latency))
                      finally (return (/ sum (* n 1.0)))))
        (setf *overall-results* 
              (append *overall-results* (list 
                                         (loop with sum = 0
                                               for latency in interference-latencies
                                               for n from 1
                                               do (setf sum (+ sum latency))
                                               finally (return (/ sum (* n 1.0)))))))
        (format t "Average suppression Latency = ~a~%"
                (loop with sum = 0
                      for latency in suppression-latencies
                      for n from 1
                      do (setf sum (+ sum latency))
                      finally (return (/ sum (* n 1.0)))))
        (setf *overall-results* 
              (append *overall-results* (list                 
                                         (loop with sum = 0
                                               for latency in suppression-latencies
                                               for n from 1
                                               do (setf sum (+ sum latency))
                                               finally (return (/ sum (* n 1.0)))))))
        
        (format t "Average control Latency = ~a~%"
                (loop with sum = 0
                      for latency in control-latencies
                      for n from 1
                      do (setf sum (+ sum latency))
                      finally (return (/ sum (* n 1.0)))))
        (setf *overall-results* 
              (append *overall-results* (list 
                                         (loop with sum = 0
                                               for latency in control-latencies
                                               for n from 1
                                               do (setf sum (+ sum latency))
                                               finally (return (/ sum (* n 1.0)))))))
        
        (format t "Average foil Latency = ~a~%"
                (loop with sum = 0
                      for latency in foil-latencies
                      for n from 1
                      do (setf sum (+ sum latency))
                      finally (return (/ sum (* n 1.0)))))
        (setf *overall-results* 
              (append *overall-results* (list 
                                         (loop with sum = 0
                                               for latency in foil-latencies
                                               for n from 1
                                               do (setf sum (+ sum latency))
                                               finally (return (/ sum (* n 1.0)))))))
        
        (format t "Average high Latency = ~a~%"
                (loop with sum = 0
                      for latency in high-latencies
                      for n from 1
                      do (setf sum (+ sum latency))
                      finally (return (/ sum (* n 1.0)))))
        (setf *overall-results* 
              (append *overall-results* (list 
                                         (loop with sum = 0
                                               for latency in high-latencies
                                               for n from 1
                                               do (setf sum (+ sum latency))
                                               finally (return (/ sum (* n 1.0)))))))
        
        (format t "Average mixed Latency = ~a~%"
                (loop with sum = 0
                      for latency in mixed-latencies
                      for n from 1
                      do (setf sum (+ sum latency))
                      finally (return (/ sum (* n 1.0)))))
        (setf *overall-results* 
              (append *overall-results* (list 
                                         (loop with sum = 0
                                               for latency in mixed-latencies
                                               for n from 1
                                               do (setf sum (+ sum latency))
                                               finally (return (/ sum (* n 1.0)))))))
        
        (format t "Average low Latency = ~a~%"
                (loop with sum = 0
                      for latency in low-latencies
                      for n from 1
                      do (setf sum (+ sum latency))
                      finally (return (/ sum (* n 1.0)))))
        (setf *overall-results* 
              (append *overall-results* (list 
                                         (loop with sum = 0
                                               for latency in low-latencies
                                               for n from 1
                                               do (setf sum (+ sum latency))
                                               finally (return (/ sum (* n 1.0)))))))
        
        (format t "Average Target Latency when not error = ~a~%"
                (loop with sum = 0
                      for latency in target-latencies-when-correct
                      for n from 1
                      do (setf sum (+ sum latency))
                      finally (return (/ sum (* n 1.0)))))
        (setf *overall-results* 
              (append *overall-results* (list 
                                         (loop with sum = 0
                                               for latency in target-latencies-when-correct
                                               for n from 1
                                               do (setf sum (+ sum latency))
                                               finally (return (/ sum (* n 1.0)))))))
        
        (format t "Average facilitation Latency when not error = ~a~%"
                (loop with sum = 0
                      for latency in facilitation-latencies-when-correct
                      for n from 1
                      do (setf sum (+ sum latency))
                      finally (return (/ sum (* n 1.0)))))
        (setf *overall-results* 
              (append *overall-results* (list 
                                         (loop with sum = 0
                                               for latency in facilitation-latencies-when-correct
                                               for n from 1
                                               do (setf sum (+ sum latency))
                                               finally (return (/ sum (* n 1.0)))))))
        
        (format t "Average interference Latency when not error = ~a~%"
                (loop with sum = 0
                      for latency in interference-latencies-when-correct
                      for n from 1
                      do (setf sum (+ sum latency))
                      finally (return (/ sum (* n 1.0)))))
        (setf *overall-results* 
              (append *overall-results* (list 
                                         (loop with sum = 0
                                               for latency in interference-latencies-when-correct
                                               for n from 1
                                               do (setf sum (+ sum latency))
                                               finally (return (/ sum (* n 1.0)))))))
        
        (format t "Average suppression Latency when not error = ~a~%"
                (loop with sum = 0
                      for latency in suppression-latencies-when-correct
                      for n from 1
                      do (setf sum (+ sum latency))
                      finally (return (/ sum (* n 1.0)))))
        (setf *overall-results* 
              (append *overall-results* (list 
                                         (loop with sum = 0
                                               for latency in suppression-latencies-when-correct
                                               for n from 1
                                               do (setf sum (+ sum latency))
                                               finally (return (/ sum (* n 1.0)))))))
        
        (format t "Average control Latency when not error = ~a~%"
                (loop with sum = 0
                      for latency in control-latencies-when-correct
                      for n from 1
                      do (setf sum (+ sum latency))
                      finally (return (/ sum (* n 1.0)))))
        (setf *overall-results* 
              (append *overall-results* (list 
                                         (loop with sum = 0
                                               for latency in control-latencies-when-correct
                                               for n from 1
                                               do (setf sum (+ sum latency))
                                               finally (return (/ sum (* n 1.0)))))))
        
        (format t "Average foil Latency when not error = ~a~%"
                (loop with sum = 0
                      for latency in foil-latencies-when-correct
                      for n from 1
                      do (setf sum (+ sum latency))
                      finally (return (/ sum (* n 1.0)))))
        (setf *overall-results* 
              (append *overall-results* (list 
                                         (loop with sum = 0
                                               for latency in foil-latencies-when-correct
                                               for n from 1
                                               do (setf sum (+ sum latency))
                                               finally (return (/ sum (* n 1.0)))))))
        
        (format t "Average high Latency when not error = ~a~%"
                (loop with sum = 0
                      for latency in high-latencies-when-correct
                      for n from 1
                      do (setf sum (+ sum latency))
                      finally (return (/ sum (* n 1.0)))))
        (setf *overall-results* 
              (append *overall-results* (list 
                                         (loop with sum = 0
                                               for latency in high-latencies-when-correct
                                               for n from 1
                                               do (setf sum (+ sum latency))
                                               finally (return (/ sum (* n 1.0)))))))
        
        (format t "Average mixed Latency when not error = ~a~%"
                (loop with sum = 0
                      for latency in mixed-latencies-when-correct
                      for n from 1
                      do (setf sum (+ sum latency))
                      finally (return (/ sum (* n 1.0)))))
        (setf *overall-results* 
              (append *overall-results* (list 
                                         (loop with sum = 0
                                               for latency in mixed-latencies-when-correct
                                               for n from 1
                                               do (setf sum (+ sum latency))
                                               finally (return (/ sum (* n 1.0)))))))
        
        (format t "Average low Latency when not error = ~a~%"
                (loop with sum = 0
                      for latency in low-latencies-when-correct
                      for n from 1
                      do (setf sum (+ sum latency))
                      finally (return (/ sum (* n 1.0)))))
        (setf *overall-results* 
              (append *overall-results* (list 
                                         (loop with sum = 0
                                               for latency in low-latencies-when-correct
                                               for n from 1
                                               do (setf sum (+ sum latency))
                                               finally (return (/ sum (* n 1.0)))))))
        
        (format t "Average Target Number of Episodes when not error = ~a~%"
                (loop with sum = 0
                      for latency in target-number-of-episodes-when-correct
                      for n from 1
                      do (setf sum (+ sum latency))
                      finally (return (/ sum (* n 1.0)))))
        (setf *overall-results* 
              (append *overall-results* (list 
                                         (loop with sum = 0
                                               for latency in target-number-of-episodes-when-correct
                                               for n from 1
                                               do (setf sum (+ sum latency))
                                               finally (return (/ sum (* n 1.0)))))))
        (format t "Average facilitation Number of Episodes when not error = ~a~%"
                (loop with sum = 0
                      for latency in facilitation-number-of-episodes-when-correct
                      for n from 1
                      do (setf sum (+ sum latency))
                      finally (return (/ sum (* n 1.0)))))
        (setf *overall-results* 
              (append *overall-results* (list 
                                         (loop with sum = 0
                                               for latency in facilitation-number-of-episodes-when-correct
                                               for n from 1
                                               do (setf sum (+ sum latency))
                                               finally (return (/ sum (* n 1.0)))))))
        
        (format t "Average interference Number of Episodes when not error = ~a~%"
                (loop with sum = 0
                      for latency in interference-number-of-episodes-when-correct
                      for n from 1
                      do (setf sum (+ sum latency))
                      finally (return (/ sum (* n 1.0)))))
        (setf *overall-results* 
              (append *overall-results* (list 
                                         (loop with sum = 0
                                               for latency in interference-number-of-episodes-when-correct
                                               for n from 1
                                               do (setf sum (+ sum latency))
                                               finally (return (/ sum (* n 1.0)))))))
        
        (format t "Average suppression Number of Episodes when not error = ~a~%"
                (loop with sum = 0
                      for latency in suppression-number-of-episodes-when-correct
                      for n from 1
                      do (setf sum (+ sum latency))
                      finally (return (/ sum (* n 1.0)))))
        (setf *overall-results* 
              (append *overall-results* (list 
                                         (loop with sum = 0
                                               for latency in suppression-number-of-episodes-when-correct
                                               for n from 1
                                               do (setf sum (+ sum latency))
                                               finally (return (/ sum (* n 1.0)))))))
        
        (format t "Average control Number of Episodes when not error = ~a~%"
                (loop with sum = 0
                      for latency in control-number-of-episodes-when-correct
                      for n from 1
                      do (setf sum (+ sum latency))
                      finally (return (/ sum (* n 1.0)))))
        (setf *overall-results* 
              (append *overall-results* (list 
                                         (loop with sum = 0
                                               for latency in control-number-of-episodes-when-correct
                                               for n from 1
                                               do (setf sum (+ sum latency))
                                               finally (return (/ sum (* n 1.0)))))))
        (format t "Average foil Number of Episodes when not error = ~a~%"
                (loop with sum = 0
                      for latency in foil-number-of-episodes-when-correct
                      for n from 1
                      do (setf sum (+ sum latency))
                      finally (return (/ sum (* n 1.0)))))
        (setf *overall-results* 
              (append *overall-results* (list 
                                         (loop with sum = 0
                                               for latency in foil-number-of-episodes-when-correct
                                               for n from 1
                                               do (setf sum (+ sum latency))
                                               finally (return (/ sum (* n 1.0)))))))
        
        (format t "Average high Number of Episodes when not error = ~a~%"
                (loop with sum = 0
                      for latency in high-number-of-episodes-when-correct
                      for n from 1
                      do (setf sum (+ sum latency))
                      finally (return (/ sum (* n 1.0)))))
        (setf *overall-results* 
              (append *overall-results* (list 
                                         (loop with sum = 0
                                               for latency in high-number-of-episodes-when-correct
                                               for n from 1
                                               do (setf sum (+ sum latency))
                                               finally (return (/ sum (* n 1.0)))))))
        
        (format t "Average mixed Number of Episodes when not error = ~a~%"
                (loop with sum = 0
                      for latency in mixed-number-of-episodes-when-correct
                      for n from 1
                      do (setf sum (+ sum latency))
                      finally (return (/ sum (* n 1.0)))))
        (setf *overall-results* 
              (append *overall-results* (list 
                                         (loop with sum = 0
                                               for latency in mixed-number-of-episodes-when-correct
                                               for n from 1
                                               do (setf sum (+ sum latency))
                                               finally (return (/ sum (* n 1.0)))))))
        
        (format t "Average low Number of Episodes when not error = ~a~%"
                (loop with sum = 0
                      for latency in low-number-of-episodes-when-correct
                      for n from 1
                      do (setf sum (+ sum latency))
                      finally (return (/ sum (* n 1.0)))))
        (setf *overall-results* 
              (append *overall-results* (list 
                                         (loop with sum = 0
                                               for latency in low-number-of-episodes-when-correct
                                               for n from 1
                                               do (setf sum (+ sum latency))
                                               finally (return (/ sum (* n 1.0)))))))
        
        (format t "Average Target Position of Episode when not error = ~a~%"
                (loop with sum = 0
                      for latency in target-position-of-episode-when-correct
                      for n from 1
                      do (setf sum (+ sum latency))
                      finally (return (/ sum (* n 1.0)))))
        (setf *overall-results* 
              (append *overall-results* (list 
                                         (loop with sum = 0
                                               for latency in target-position-of-episode-when-correct
                                               for n from 1
                                               do (setf sum (+ sum latency))
                                               finally (return (/ sum (* n 1.0)))))))
        
        (format t "Average facilitation Position of Episode when not error = ~a~%"
                (loop with sum = 0
                      for latency in facilitation-position-of-episode-when-correct
                      for n from 1
                      do (setf sum (+ sum latency))
                      finally (return (/ sum (* n 1.0)))))
        (setf *overall-results* 
              (append *overall-results* (list 
                                         (loop with sum = 0
                                               for latency in facilitation-position-of-episode-when-correct
                                               for n from 1
                                               do (setf sum (+ sum latency))
                                               finally (return (/ sum (* n 1.0)))))))
        
        (format t "Average interference Position of Episode when not error = ~a~%"
                (loop with sum = 0
                      for latency in interference-position-of-episode-when-correct
                      for n from 1
                      do (setf sum (+ sum latency))
                      finally (return (/ sum (* n 1.0)))))
        (setf *overall-results* 
              (append *overall-results* (list 
                                         (loop with sum = 0
                                               for latency in interference-position-of-episode-when-correct
                                               for n from 1
                                               do (setf sum (+ sum latency))
                                               finally (return (/ sum (* n 1.0)))))))
        
        (format t "Average suppression Position of Episode when not error = ~a~%"
                (loop with sum = 0
                      for latency in suppression-position-of-episode-when-correct
                      for n from 1
                      do (setf sum (+ sum latency))
                      finally (return (/ sum (* n 1.0)))))
        (setf *overall-results* 
              (append *overall-results* (list 
                                         (loop with sum = 0
                                               for latency in suppression-position-of-episode-when-correct
                                               for n from 1
                                               do (setf sum (+ sum latency))
                                               finally (return (/ sum (* n 1.0)))))))
        (format t "Average control Position of Episode when not error = ~a~%"
                (loop with sum = 0
                      for latency in control-position-of-episode-when-correct
                      for n from 1
                      do (setf sum (+ sum latency))
                      finally (return (/ sum (* n 1.0)))))
        (setf *overall-results* 
              (append *overall-results* (list 
                                         (loop with sum = 0
                                               for latency in control-position-of-episode-when-correct
                                               for n from 1
                                               do (setf sum (+ sum latency))
                                               finally (return (/ sum (* n 1.0)))))))
        (format t "Average foil Position of Episode when not error = ~a~%"
                (loop with sum = 0
                      for latency in foil-position-of-episode-when-correct
                      for n from 1
                      do (setf sum (+ sum latency))
                      finally (return (/ sum (* n 1.0)))))
        (setf *overall-results* 
              (append *overall-results* (list 
                                         (loop with sum = 0
                                               for latency in foil-position-of-episode-when-correct
                                               for n from 1
                                               do (setf sum (+ sum latency))
                                               finally (return (/ sum (* n 1.0)))))))
        (format t "Average high Position of Episode when not error = ~a~%"
                (loop with sum = 0
                      for latency in high-position-of-episode-when-correct
                      for n from 1
                      do (setf sum (+ sum latency))
                      finally (return (/ sum (* n 1.0)))))
        (setf *overall-results* 
              (append *overall-results* (list 
                                         (loop with sum = 0
                                               for latency in high-position-of-episode-when-correct
                                               for n from 1
                                               do (setf sum (+ sum latency))
                                               finally (return (/ sum (* n 1.0)))))))
        
        (format t "Average mixed Position of Episode when not error = ~a~%"
                (loop with sum = 0
                      for latency in mixed-position-of-episode-when-correct
                      for n from 1
                      do (setf sum (+ sum latency))
                      finally (return (/ sum (* n 1.0)))))
        (setf *overall-results* 
              (append *overall-results* (list                
                                         (loop with sum = 0
                                               for latency in mixed-position-of-episode-when-correct
                                               for n from 1
                                               do (setf sum (+ sum latency))
                                               finally (return (/ sum (* n 1.0)))))))
        
        (format t "Average low Position of Episode when not error = ~a~%"
                (loop with sum = 0
                      for latency in low-position-of-episode-when-correct
                      for n from 1
                      do (setf sum (+ sum latency))
                      finally (return (/ sum (* n 1.0)))))
        (setf *overall-results* 
              (append *overall-results* (list 
                                         (loop with sum = 0
                                               for latency in low-position-of-episode-when-correct
                                               for n from 1
                                               do (setf sum (+ sum latency))
                                               finally (return (/ sum (* n 1.0)))))))
        
        ))


(defun report-anderson-1974-results ()
   (setf *overall-results* nil)
   (loop for index in '(0 1)
     do (format t "During run ~a of second phase, items averaged ~a trials.~%"
          (1+ index) (find-phase2-arerrors index)))
   (loop 
     for sfan in '(1 2 3)
     do (loop 
          for rfan in '(1 2 3)
          for target-errors = (aref *true-errors* sfan rfan)
          for foil-errors = (aref *false-errors* sfan rfan)
          for target-latencies = (aref *true-latencies* sfan rfan)
          for foil-latencies = (aref *false-latencies* sfan rfan)
          for target-latencies-when-correct = (aref *true-latencies-when-correct* sfan rfan)
          for foil-latencies-when-correct = (aref *false-latencies-when-correct* sfan rfan)
          for target-number-of-episodes-when-correct = 
          (aref *true-number-of-episodes-when-correct* sfan rfan)
          for foil-number-of-episodes-when-correct = 
          (aref *false-number-of-episodes-when-correct* sfan rfan)
          for target-position-of-episode-when-correct = 
          (aref *true-position-of-episode-when-correct* sfan rfan)
          for foil-position-of-episode-when-correct = 
          (aref *false-position-of-episode-when-correct* sfan rfan)
          do 
          (format t "~%~%Results for People-Fan=~a, Places-Fan=~a:~%" sfan rfan)
          (format t "Target Error Rate = ~a~%"
            (/ target-errors (* 1.0 (length target-latencies))))
          (setf *overall-results* 
                (append *overall-results* 
                  (list (/ target-errors (length target-latencies)))))
          (format t "Foil Error Rate = ~a~%"
            (/ foil-errors (* 1.0 (length foil-latencies))))
          (setf *overall-results* 
                (append *overall-results* 
                  (list (/ foil-errors (* 1.0 (length foil-latencies))))))
          (format t "Average Target Latency = ~a~%"
            (loop with sum = 0
              for latency in target-latencies
              for n from 1
              do (setf sum (+ sum latency))
              finally (return (/ sum (* n 1.0)))))
          (setf *overall-results* 
                (append *overall-results* (list 
                                            (loop with sum = 0
                                              for latency in target-latencies
                                              for n from 1
                                              do (setf sum (+ sum latency))
                                              finally (return (/ sum (* n 1.0)))))))
                 (format t "Average foil Latency = ~a~%"
                (loop with sum = 0
                      for latency in foil-latencies
                      for n from 1
                      do (setf sum (+ sum latency))
                      finally (return (/ sum (* n 1.0)))))
          (setf *overall-results* 
                (append *overall-results* (list 
                                            (loop with sum = 0
                                              for latency in foil-latencies
                                              for n from 1
                                              do (setf sum (+ sum latency))
                                              finally (return (/ sum (* n 1.0)))))))
          
          (format t "Average Target Latency when not error = ~a~%"
            (loop with sum = 0
              for latency in target-latencies-when-correct
              for n from 1
              do (setf sum (+ sum latency))
              finally (return (/ sum (* n 1.0)))))
          (setf *overall-results* 
                (append *overall-results* (list 
                                            (loop with sum = 0
                                              for latency in target-latencies-when-correct
                                              for n from 1
                                              do (setf sum (+ sum latency))
                                              finally (return (/ sum (* n 1.0)))))))
          
          (format t "Average foil Latency when not error = ~a~%"
            (loop with sum = 0
              for latency in foil-latencies-when-correct
              for n from 1
              do (setf sum (+ sum latency))
              finally (return (/ sum (* n 1.0)))))
          (setf *overall-results* 
                (append *overall-results* (list 
                                            (loop with sum = 0
                                              for latency in foil-latencies-when-correct
                                              for n from 1
                                              do (setf sum (+ sum latency))
                                              finally (return (/ sum (* n 1.0)))))))
          
          (format t "Average Target Number of Episodes when not error = ~a~%"
            (loop with sum = 0
              for latency in target-number-of-episodes-when-correct
              for n from 1
              do (setf sum (+ sum latency))
              finally (return (/ sum (* n 1.0)))))
          (setf *overall-results* 
                (append *overall-results* (list 
                                            (loop with sum = 0
                                              for latency in target-number-of-episodes-when-correct
                                              for n from 1
                                              do (setf sum (+ sum latency))
                                              finally (return (/ sum (* n 1.0)))))))
          (format t "Average foil Number of Episodes when not error = ~a~%"
            (loop with sum = 0
              for latency in foil-number-of-episodes-when-correct
              for n from 1
              do (setf sum (+ sum latency))
              finally (return (/ sum (* n 1.0)))))
          (setf *overall-results* 
                (append *overall-results* (list 
                                            (loop with sum = 0
                                              for latency in foil-number-of-episodes-when-correct
                                              for n from 1
                                              do (setf sum (+ sum latency))
                                              finally (return (/ sum (* n 1.0)))))))
          
          (format t "Average Target Position of Episode when not error = ~a~%"
            (loop with sum = 0
              for latency in target-position-of-episode-when-correct
              for n from 1
              do (setf sum (+ sum latency))
              finally (return (/ sum (* n 1.0)))))
          (setf *overall-results* 
                (append *overall-results* (list 
                                            (loop with sum = 0
                                              for latency in target-position-of-episode-when-correct
                                              for n from 1
                                              do (setf sum (+ sum latency))
                                              finally (return (/ sum (* n 1.0)))))))
          
          (format t "Average foil Position of Episode when not error = ~a~%"
            (loop with sum = 0
              for latency in foil-position-of-episode-when-correct
              for n from 1
              do (setf sum (+ sum latency))
              finally (return (/ sum (* n 1.0)))))
          (setf *overall-results* 
                (append *overall-results* (list 
                                            (loop with sum = 0
                                              for latency in foil-position-of-episode-when-correct
                                              for n from 1
                                              do (setf sum (+ sum latency))
                                              finally (return (/ sum (* n 1.0)))))))
          )))


(defun find-phase2-arerrors (index)
  (loop with sum = 0.0
        for result in (aref *second-phase-trials-array* index)
        for n from 1
        do (setf sum (+ sum result))
        finally (return (/ sum n))))
        


(defun anderson-1974-third-phase ()
   (when *print-experimenter-updates*
      (format t "~%Beginning Anderson 1974 Third Phase Trial~%"))
   (setf *epam-clock* *clock*)
   (loop 
     repeat 3
     do
     (setf *clock* (+ *clock* 120000)) ; assume 2 minute break between blocks
     (loop with recognition-response
       with result
       for probe in (reorder-list-randomly *anderson-1974-test-set*)
       for stimulus = (change-string-to-object (first probe) 'word)
       for response = (change-string-to-object (second probe) 'word)
       for stimulus-fan = (third probe) ; a number from 1 to 3
       for response-fan = (fourth probe) ; a number from 1 to 3
       for is-a = (fifth probe) ; values are 'true, 'false, and 'filler
       do 
       (setf *trial-clock* (+ *clock* *presentation-rate*))
       (setf *clock* *trial-clock*)
       (when *print-experimenter-updates*
          (format t "~%~a Presenting stimulus = ~a~%" *clock* stimulus))
       (setf recognition-response (quick-recognize 'episode stimulus response))
       (fixed-time-tally-to-recognize)
       (cond ((eql is-a 'true)
              (setf (aref *true-latencies* stimulus-fan response-fan)
                    (cons (- *clock* *trial-clock*)
                      (aref *true-latencies* stimulus-fan response-fan))))
             ((eql is-a 'false)
              (setf (aref *false-latencies* stimulus-fan response-fan)
                    (cons (- *clock* *trial-clock*)
                      (aref *false-latencies* stimulus-fan response-fan)))))
       (cond ((and (eql is-a 'true)
                   recognition-response)
              (setf result 'hit)
              (setf (aref *true-latencies-when-correct* stimulus-fan response-fan)
                    (cons (- *clock* *trial-clock*)
                      (aref *true-latencies-when-correct* stimulus-fan response-fan)))
              (setf (aref *true-number-of-episodes-when-correct* stimulus-fan response-fan)
                    (cons *number-of-episodes-elicited*
                      (aref *true-number-of-episodes-when-correct* 
                        stimulus-fan response-fan)))
              (setf (aref *true-position-of-episode-when-correct* stimulus-fan response-fan)
                    (cons *position-of-episode-elicited*
                      (aref *true-position-of-episode-when-correct* stimulus-fan response-fan)
                      )))
             ((and (eql is-a 'true)
                   (not recognition-response))
              (setf result 'incorrect-rejection)
              (incf (aref *true-errors* stimulus-fan response-fan)))
             ((and (eql is-a 'false)
                   recognition-response)
              (setf result 'false-alarm)
              (incf (aref *false-errors* stimulus-fan response-fan)))
             ((and (eql is-a 'false)
                   (not recognition-response))
              (setf result 'correct-rejection)
              (setf (aref *false-latencies-when-correct* stimulus-fan response-fan)
                    (cons (- *clock* *trial-clock*)
                      (aref *false-latencies-when-correct* stimulus-fan response-fan)))
              (setf (aref *false-number-of-episodes-when-correct* stimulus-fan response-fan)
                    (cons *number-of-episodes-elicited*
                      (aref *false-number-of-episodes-when-correct* stimulus-fan response-fan)
                      ))
              (setf (aref *false-position-of-episode-when-correct* stimulus-fan response-fan)
                    (cons *position-of-episode-elicited*
                      (aref *false-position-of-episode-when-correct* 
                        stimulus-fan response-fan)))))
       (when *print-experimenter-updates*
          (format t "Recognition test for (~a ~a) -- ~a~%"
            stimulus response is-a)
          (format t "Subject responded ~a in ~a msec -- ~a~%" 
            recognition-response (- *clock* *trial-clock*) result))
       if (and (eql result 'hit)
               (< (random 100) *SAL-C*)
               (<= *epam-clock* *clock*))
       do (study-when-overlearning 'episode stimulus response)
      ; else if (and (eql result 'incorrect-rejection)
      ;              (<= *epam-clock* *clock*)
       ;             (< (random 100) *sal-a*)
        ;            )
       ;do (when *print-strategy-updates*
        ;     (format t "Random number is less then *sal-a* epam learning is available~%")
         ;    (format t "So, studying after error and re-associating with episode.~%"))
       ;(setf *epam-clock* *clock*)
       ;(cond ((= (random 2) 0) 
        ;      (when (are-there-multiple-episodes-here? 'episode (get-chunk-for stimulus))
         ;        (study stimulus)))
          ;   ('else 
           ;   (when (are-there-multiple-episodes-here? 'episode (get-chunk-for response))
            ;     (study response))))
       ;(assoc iate 'episode stimulus response)
       ;(when *print-net-updates* (print-net))
       else if (and (eql result 'incorrect-rejection)
                    (<= *epam-clock* *clock*)
                    (< (random 100) *sal-b*))
       do (when *print-strategy-updates*
             (format t "Random number is not less then *sal-a* and epam learning is available.~%")
             (format t "So, associating stimulus with response because another random number less than *SAL-B*.~%"))
       (setf *epam-clock* *clock*)
       (associate 'episode stimulus response nil nil nil t)
       ( when *print-net-updates* (print-net)))))
     
                           
(defun anderson-and-reder-third-phase ()
  (when *print-experimenter-updates*
    (format t "~%Beginning Anderson and Reder Third Phase Trial~%"))
  (setf *epam-clock* *clock*)
  (let ((probes (get-anderson-and-reder-probes)))
    (loop repeat 3
          do
          (setf *clock* (+ *clock* 120000)) ; assume 2 minute break between blocks
          (loop with recognition-response
                with result
                for probe in (reorder-list-randomly probes)
                for stimulus = (first probe)
                for response = (second probe)
                for is-a = (third probe)
                for fan = (fourth probe)
                for target? = (fifth probe)
                do 
                (setf *trial-clock* (+ *clock* *presentation-rate*))
                (setf *clock* *trial-clock*)
                (when *print-experimenter-updates*
                  (format t "~%~a Presenting stimulus = ~a~%" *clock* stimulus))
                (setf recognition-response (quick-recognize 'episode stimulus response))
                (fixed-time-tally-to-recognize)
                (cond ((eql is-a 'facilitation)
                       (setf (aref *facilitation-latencies* fan)
                             (cons (- *clock* *trial-clock*)
                                   (aref *facilitation-latencies* fan))))
                      ((eql is-a 'interference)
                       (setf (aref *interference-latencies* fan)
                             (cons (- *clock* *trial-clock*)
                                   (aref *interference-latencies* fan))))
                      ((eql is-a 'suppression)
                       (setf (aref *suppression-latencies* fan)
                             (cons (- *clock* *trial-clock*)
                                   (aref *suppression-latencies* fan))))
                      ((eql is-a 'control)
                       (setf (aref *control-latencies* fan)
                             (cons (- *clock* *trial-clock*)
                                   (aref *control-latencies* fan))))
                      ((eql is-a 'mixed)
                       (setf (aref *mixed-latencies* fan)
                             (cons (- *clock* *trial-clock*)
                                   (aref *mixed-latencies* fan))))
                      ((eql is-a 'high)
                       (setf (aref *high-latencies* fan)
                             (cons (- *clock* *trial-clock*)
                                   (aref *high-latencies* fan))))
                      ((eql is-a 'low)
                       (setf (aref *low-latencies* fan)
                             (cons (- *clock* *trial-clock*)
                                   (aref *low-latencies* fan)))))
                (cond ((and (eql target? 'target)
                            recognition-response)
                       (setf result 'hit)
                       (cond ((eql is-a 'facilitation)
                              (setf (aref *facilitation-latencies-when-correct* fan)
                                    (cons (- *clock* *trial-clock*)
                                          (aref *facilitation-latencies-when-correct* fan))))
                             ((eql is-a 'interference)
                              (setf (aref *interference-latencies-when-correct* fan)
                                    (cons (- *clock* *trial-clock*)
                                          (aref *interference-latencies-when-correct* fan))))
                             ((eql is-a 'suppression)
                              (setf (aref *suppression-latencies-when-correct* fan)
                                    (cons (- *clock* *trial-clock*)
                                          (aref *suppression-latencies-when-correct* fan))))
                             ((eql is-a 'control)
                              (setf (aref *control-latencies-when-correct* fan)
                                    (cons (- *clock* *trial-clock*)
                                          (aref *control-latencies-when-correct* fan)))))
                       (cond ((eql is-a 'facilitation)
                              (setf (aref *facilitation-number-of-episodes-when-correct* fan)
                                    (cons *number-of-episodes-elicited*
                                          (aref *facilitation-number-of-episodes-when-correct* fan))))
                             ((eql is-a 'interference)
                              (setf (aref *interference-number-of-episodes-when-correct* fan)
                                    (cons *number-of-episodes-elicited*
                                          (aref *interference-number-of-episodes-when-correct* fan))))
                             ((eql is-a 'suppression)
                              (setf (aref *suppression-number-of-episodes-when-correct* fan)
                                    (cons *number-of-episodes-elicited*
                                          (aref *suppression-number-of-episodes-when-correct* fan))))
                             ((eql is-a 'control)
                              (setf (aref *control-number-of-episodes-when-correct* fan)
                                    (cons *number-of-episodes-elicited*
                                          (aref *control-number-of-episodes-when-correct* fan)))))
                       (cond ((eql is-a 'facilitation)
                              (setf (aref *facilitation-position-of-episode-when-correct* fan)
                                    (cons *position-of-episode-elicited*
                                          (aref *facilitation-position-of-episode-when-correct* fan))))
                             ((eql is-a 'interference)
                              (setf (aref *interference-position-of-episode-when-correct* fan)
                                    (cons *position-of-episode-elicited*
                                          (aref *interference-position-of-episode-when-correct* fan))))
                             ((eql is-a 'suppression)
                              (setf (aref *suppression-position-of-episode-when-correct* fan)
                                    (cons *position-of-episode-elicited*
                                          (aref *suppression-position-of-episode-when-correct* fan))))
                             ((eql is-a 'control)
                              (setf (aref *control-position-of-episode-when-correct* fan)
                                    (cons *position-of-episode-elicited*
                                          (aref *control-position-of-episode-when-correct* fan)))))
                       )
                      ((and (eql target? 'target)
                            (not recognition-response))
                       (setf result 'incorrect-rejection)
                       (cond ((eql is-a 'facilitation)
                              (incf (aref *facilitation-errors* fan)))
                             ((eql is-a 'interference)
                              (incf (aref *interference-errors* fan)))
                             ((eql is-a 'suppression)
                              (incf (aref *suppression-errors* fan)))
                             ((eql is-a 'control)
                              (incf (aref *control-errors* fan)))
                             ('else
                              (format t "ERROR! Unknown is-a = ~a!~%" is-a)
                              (break))))
                      ((and (eql target? 'foil)
                            recognition-response)
                       (setf result 'false-alarm)
                       (cond ((eql is-a 'mixed)
                              (incf (aref *mixed-errors* fan)))
                             ((eql is-a 'high)
                              (incf (aref *high-errors* fan)))
                             ((eql is-a 'low)
                              (incf (aref *low-errors* fan)))
                             ('else
                              (format t "ERROR! Unknown is-a = ~a!~%" is-a)
                              (break))))
                       ((and (eql target? 'foil)
                             (not recognition-response))
                        (setf result 'correct-rejection)
                        (cond ((eql is-a 'mixed)
                               (setf (aref *mixed-latencies-when-correct* fan)
                                     (cons (- *clock* *trial-clock*)
                                           (aref *mixed-latencies-when-correct* fan))))
                              ((eql is-a 'high)
                               (setf (aref *high-latencies-when-correct* fan)
                                     (cons (- *clock* *trial-clock*)
                                           (aref *high-latencies-when-correct* fan))))
                              ((eql is-a 'low)
                               (setf (aref *low-latencies-when-correct* fan)
                                     (cons (- *clock* *trial-clock*)
                                           (aref *low-latencies-when-correct* fan)))))
                        (cond ((eql is-a 'mixed)
                               (setf (aref *mixed-number-of-episodes-when-correct* fan)
                                     (cons *number-of-episodes-elicited*
                                           (aref *mixed-number-of-episodes-when-correct* fan))))
                              ((eql is-a 'high)
                               (setf (aref *high-number-of-episodes-when-correct* fan)
                                     (cons *number-of-episodes-elicited*
                                           (aref *high-number-of-episodes-when-correct* fan))))
                              ((eql is-a 'low)
                               (setf (aref *low-number-of-episodes-when-correct* fan)
                                     (cons *number-of-episodes-elicited*
                                           (aref *low-number-of-episodes-when-correct* fan)))))
                        (cond ((eql is-a 'mixed)
                               (setf (aref *mixed-position-of-episode-when-correct* fan)
                                     (cons *position-of-episode-elicited*
                                           (aref *mixed-position-of-episode-when-correct* fan))))
                              ((eql is-a 'high)
                               (setf (aref *high-position-of-episode-when-correct* fan)
                                     (cons *position-of-episode-elicited*
                                           (aref *high-position-of-episode-when-correct* fan))))
                              ((eql is-a 'low)
                               (setf (aref *low-position-of-episode-when-correct* fan)
                                     (cons *position-of-episode-elicited*
                                           (aref *low-position-of-episode-when-correct* fan)))))

                        ))
                (when *print-experimenter-updates*
                  (format t "Recognition test for (~a ~a) -- ~a~%"
                          stimulus response target?)
                  (format t "Subject responded ~a in ~a msec -- ~a~%" 
                          recognition-response (- *clock* *trial-clock*) result))
                if (and (eql result 'hit)
                        (< (random 100) *SAL-C*)
                        (<= *epam-clock* *clock*))
                do (study-when-overlearning 'episode stimulus response)
               ; else if (and (eql result 'incorrect-rejection)
                ;             (<= *epam-clock* *clock*)
                 ;            (< (random 100) *sal-a*)
                  ;           )
                ;do (when *print-strategy-updates*
                     ;(format t "Random number is less then *sal-a* epam learning is available~%")
                 ;    (format t "So, studying after error and re-associating with episode.~%"))
               ; (setf *epam-clock* *clock*)
                ;(cond ((= (random 2) 0) 
                 ;      (when (are-there-multiple-episodes-here? 'episode (get-chunk-for stimulus))
                  ;       (study stimulus)))
                   ;   ('else 
                    ;   (when (are-there-multiple-episodes-here? 'episode (get-chunk-for response))
                     ;    (study response))))
                ;(associate 'episode stimulus response nil nil nil t)
                ;(when *print-net-updates* (print-net))
                else if (and (eql result 'incorrect-rejection)
                             (<= *epam-clock* *clock*)
                             (< (random 100) *sal-b*))
                do (when *print-strategy-updates*
                     ;(format t "Random number is not less then *sal-a* and epam learning is available.~%")
                     (format t "So, associating stimulus with response because another random number less than *SAL-B*.~%"))
                (setf *epam-clock* *clock*)
                (associate 'episode stimulus response nil nil nil t)
                (when *print-net-updates* (print-net))))));


(defun get-anderson-and-reder-probes ()
  (let ((probes nil))
    (loop for target in *anderson-and-reder-targets*
          for stimulus = (change-string-to-object (first target) 'word)
          for response = (change-string-to-object (second target) 'word)
          for is-a = (third target)
          for fan = (fourth target)
          if (eql is-a 'facilitation)
          do (loop repeat 5
                   do
                   (setf probes (cons (list stimulus response is-a fan 'target) probes)))
          else if t
          do (setf probes (cons (list stimulus response is-a fan 'target) probes)))
    (loop for target in *anderson-and-reder-foils*
          for stimulus = (change-string-to-object (first target) 'word)
          for response = (change-string-to-object (second target) 'word)
          for is-a = (third target)
          for fan = (fourth target)
          if (eql is-a 'high)
          do (loop repeat 4
                   do
                   (setf probes (cons (list stimulus response is-a fan 'foil) probes)))
          else if t
          do (setf probes (cons (list stimulus response is-a fan 'foil) probes)))
    probes))

          


                                                
(defun anderson-and-reder-first-phase (anderson-and-reder-targets)
  "This routine goes through the list with five seconds of studying per association.
Note it is the same as quick-paired-associate-trial"
  (when *print-experimenter-updates*
    (format t "~%Beginning First Phase Trial in which pairs studied as paired associates~%"))
  (let ((pairs (loop for target in anderson-and-reder-targets
                     for stimulus = (change-string-to-object (first target) 'word)
                     for response = (change-string-to-object (second target) 'word)
                     collect (list stimulus response))))
    (setf *presentation-rate* 5000)
    (setf *inter-trial-interval* 0)
    (quick-paired-associate-trial pairs)))


(defun anderson-and-reder-second-phase ()
  (let ((stimuli 
         (reorder-list-randomly (append *anderson-and-reder-people*
                                        *anderson-and-reder-places*))))
    (setf *trial* 0)
    (loop while stimuli
          do (incf *trial*)
          (when *print-experimenter-updates*
            (format t "Trial = ~a, stimuli = ~a~%" *trial* (length stimuli)))
          (setf stimuli (anderson-and-reder-second-phase-trial stimuli)))))

(defun anderson-and-reder-second-phase-trial (stimuli)
  (when *print-experimenter-updates*
    (format t "~%Beginning Anderson and Reder Second Phase Trial~%"))
  (loop 
    for word in stimuli
    for person? = (member word *anderson-and-reder-people* :test #'equal)
    for stimulus = (change-string-to-object word 'word)
    for stimulus-chunk = (get-chunk-for stimulus)
    for responses = (cond (person?
                           (mapcar 
                             'specify 
                             (find-associates 'episode stimulus-chunk 2)))
                          ('else
                           (mapcar
                            'specify
                            (find-associates 'episode stimulus-chunk 1))))
    for subject-responses = (mapcar 'change-object-to-string responses)
    for correct-responses = (get-assoc *anderson-and-reder-answers* word)
    for do-they-match? = (loop for correct-response in correct-responses
                           if (not (member correct-response subject-responses :test #'equal))
                           return nil
                           finally (return t))
    for correct-response-objects = (loop for correct-response in correct-responses
                                     collect (change-string-to-object 
                                               correct-response 
                                               'word))
    do
    ;(setf *stimulus* stimulus)
    ;(setf *responses* responses)
    ;(setf *correct-responses* correct-response-objects)
    ;(break)
    (when *print-experimenter-updates*
       (format t "subject-responses = ~a, correct-responses = ~a, match = ~a~%"
         subject-responses correct-responses do-they-match?))
    if do-they-match?
    do (setf (aref *second-phase-trials-array* *index*)
             (cons *trial* (aref *second-phase-trials-array* *index*)))
    else if t 
    do (setf *clock* *epam-clock*)
    (cond (person?
           (study-multiple-associations 'episode stimulus correct-response-objects 1 2))
          ('else
           (study-multiple-associations 'episode stimulus correct-response-objects 2 1)))
    and collect word))




(defun find-anderson-and-reder-words ()
  (loop for target in *anderson-and-reder-targets*
        collect (change-string-to-object (first target) 'word)
        collect (change-string-to-object (second target) 'word)
        ))



; stmsim644.lsp
;   This revision is designed to expand the auditory-recall routine so that 
;   the same procedures can be used in an experiment which goes through the list more 
;   than one time and for an experiment which studies nonsense syllables.
;   This means all of the following changes:
;      1. If there is not a single chunk that can be used to access an object in stm
;         then it can be accessed in two pieces and then those two pieces can be put
;         together in order to reconstruct the object.
;             a) Wrote does-it-sort-to-a-list-of-chunks?  now I have to rewrite the teams
;                that use access-next-object-in-imagery-store.
;             This means that all routines that use access-next have to change such that they
;             all use the object not the chunk.
;             b) rewrote convert-chunk-to-object
;             c) change subvocalize-one-image so it has object as input and doesn't pop chunks
;             d) change subvocalize-one-image-aloud the same way
;             e) change speak-entering-it-in-echo the same way
;      2. This is allowing me to get rid of a whole layer of hierarchy in the store.  From now 
;         on 'beginning and 'group-2 are simply going to be cues and the members of their sections
;         are going to be objects.
;      3. Simplified remove-value and put-value and get-value because no further need for section.  
;      4. Access-next no longer takes number-in-section as one of its inputs.
;      5. One thing that is clearly lost is that the imagery store can only specify one category
;         now.  Previously, each section could have its own modality.
;      6. rehearse-section now does not have section length as one of its arguments
;      7. changed get-stm-object-for because convert-chunk-to-object no longer accepts objects
;         as input.
;      8. Currently end-of-list simply means that there are no further items and that 
;         rehearse-one-by-one or output-one-by-one has exited successfully.  In some cases 
;         I need to put end-of-list specifically into the loop so that the system will know that 
;         it has reached the end of the loop and during rehearsal will study.  Perhaps I need 
;         to replace all occurrence of 'end-of-list with 'no-more-items
;         Then I could have 'end-of-list be added as a kind of anchor point.  Thus 'end-of-list
;         would be the same as 'no-more-items.
;      9. Replaced all occurrences of 'end-of-list with 'no-more-items
;      10. Changed print-aud-loop so that end-of-list could be an element.  This required specifying
;          an item before trying to convert it to a string.
;      11. Changed rehearse-section in a way that may not work.  It didn't seem to be set to work
;          as it only rehearsed when access resulted in nil!
;      12. Simplified by eliminating access-next and by, in general, making all access more direct
;          as access-next-item-in-sensory-store or access-next-item-in-imagery-store




; stmsim643.lsp
;   Changed search-for-matching-chunk in recognition of the changed arguments 
;    for searches.
;   Changed make-partial-image-of so that all that is done is to put the category
;    on the object as its is-a.
;   Added another step to search-for-matching-chunk so that the result is the default 
;   chunk when there is a chunk whose image has multiple nodes.
;    Changed access-next-object-in-imagery-store so that it specifies the object
;   so that it will work if an object is a single letter.

; stmsim642.lsp
;   Changed get-object-for so that it returns nil if there is a blank in an stm-object.
;   changed get-object-for so that it simplifies the object if the object is one subobject.
;   changed all (associate nil ...) to (associate 'episode ...) and changed all
;      (find-associate nil ...) to (find-associate 'episode ...)
;   changed rehearse-one-by-one so that associate and find-associate to use 'after and 'before
;   changed so that find-associate-in-ltm so that it uses after and before
;   I could turn rehearse-one-by-one into a routine that does the serial-recall task without 
;     hardly any change.  I would just need to do the following:
;       1. Instead of anchor point, I would check to see if the stimulus or response was already 
;          in the *rsf*
;       2. The final representation when going correctly could be
;            (((is-a episode)) a b c d e f ~ f g h i j k)  This would require skipping over 
;            the ~ to see if there is another f with an item after it.
;       3. In order to handle the vanrorsdorf effect I would just need to rehears the list 
;          in two parts using the one-at-a-time method on each.
;       4. I would need to make sure that beginning and end pull up a different *rsf* if the 
;          one that's there has a different beginning or ending.
;       5. I would need to fix the access one item so that it could access a nonsense syllable
;          even though it consisted of two or three chunks.  I think auditory recoding would be
;          necessary plus I would have to access the object, not just the image.  The item would 
;          have to be broken up or put together as necessary.
;       6. It would make more sense to have objects that were not completely familiar.
;   Added printing of *rsf* image to print-aud-loop
;   Changed make-partial-image-of so that it collected the list of randomly reordered 
;   phonemes into the partial-image instead of just the word 'phoneme.


; stmsim641.lsp
;  This version separates out time-tally-per-object from time-tally-to-vocalize
;    so that a group can be vocalized as one word (or object).
; Replace routine does-object-match-chunk? with does-chunk-match-object? for 
; routines that decice whether to continue rehearsal. 


; stmsim640.lsp

; I am going back to the old version of STM with the goal of changing things 
; only as much as I have to do.

; Changes
; 1. Change all occurrence of specify to specify
; 2. Change all occurrence of the word recognize to access
; 3. Removed routine sort-to-node which was already in epam640.lsp
; 4. Need to find:
;      a. print-store
;      b. print-imagery-store
;      c. get-stm-object-for
; 5. Changed timer to advance-the-timer
; 6. Guessed that *time-to-access-an-object* should be 100 not remembering 
;    what it was all about.
; 7. Rewrote get-object-for so that instead of (((is-a auditory)) chunk1 chunk2 chunk3) it would be (((is-a auditory)) image1 image2 image3)
;    In the future I might rewrite it so that it can get the object exactly as it is in the sensory store.
; 8. Redid get-object-for so it gets an imagery store object without taking
;      any time.  Also, now it returns nil if there is a specified length that 
;      the object should be, but the object is not that long.  While doing so
;      I removed the time interrupt.  This will let me examine when get-object-for
;      is called
; 9. This involved redoing get-chunk-for-section to eliminate one of its inputs
;     and to give it an access chunk timer

; This is from the epam5a current directory.

;;       These are the Subject Routines for Short Term Memory Simulations

; History
; 1. Feb 22, 1998 redid find-remainder
; 2. Feb 22, 1998 sort object to category
; 3. Added in the defvars that appear at the top.

;;               Index:
;                   A. Routines that get information from memory stores.
;                   B. Routines that put information into memory stores.
;                   C. Routines that manipulate section names.
;                   D. Routines which answer questions about memory stores.
;                   E. Rehearsal and output routines
;                   F. Subjects' strategies used in STM simulations

;            A. ROUTINES THAT GET INFO FROM MEMORY STORES


(defun access (section modality is-a-of-store &optional exit-time)
  "This routine accesses the first object of a group within
a is-a of store and deposits its chunk or chunks in *chunks*.
It exits differently depending upon the is-a of store.
   If it is recognizing a sensory store it exits nil if nothing found or t
if something was found.
   If it was recognizing an imagery store it exits with the last cue tried if
it was unable to find a chunk to match its object or nil if it reached the end
of the list."
  (print-store modality is-a-of-store)
  (cond ((eq is-a-of-store 'sensory-store)
         (access-next-object-in-sensory-store section modality))
        ('else 
         (access-next-object-in-imagery-store section modality exit-time))))
 
(defun access-next-object-in-sensory-store (section modality &optional same-group dont-mark-as-old)
  (let* ((object (get-first-object-in-sensory-store section modality dont-mark-as-old))
         (chunks (when object 
                   (cond ((does-it-sort-to-matching-chunk? object))
                         ((does-it-sort-to-a-list-of-chunks? object))))))
    (reset-new-stimulus-flags modality)
    (when object
      (when (not same-group) (push 'top-of-stack *chunks*))
      (cond ((atom chunks) 
             (time-tally-to-access-chunk)
             (push chunks *chunks*))
            ('else 
             (loop for chunk in chunks
                   do (time-tally-to-access-chunk)
                   (push chunk *chunks*))))
      chunks)))
    

(defun get-first-object-in-sensory-store (section modality &optional dont-mark-as-old)
  (loop for item in (get modality 'sensory-store)
        if (eq (get-value item 'group) section)
        do (when (not dont-mark-as-old) 
             (put-value item 'old 'group))
        and return item))


(defun access-next-object-in-imagery-store (cue modality &optional exit-time
                                                interrupt use-rsf enable-search)
  "This routine accesses the next object in an imagery store 
using search to find the item if it is not clearly recognizable.  Several
alternative sources of information are searched if the information is not 
found simply by sorting the imagery store object.  Information is tried
in the following order:
   1. Sort to node -- if object matches node output node.
   2. Try to find an association in long-term-memory that matches the object.
If the object matches the association output the association.
   3. Search for match in net using the category to guide the search if it
is associated with the section in the imagery-store.
   This routine does not put a chunk in the chunk store.  Its output is one
of the following:
   A.  If a chunk is found, it outputs that chunk.
   A'. If a pair of chunks is found for the beginning and end of the object it outputs the pair.
   B.  If no object is found, it outputs 'no-more-items
   C.  If an object is found, but not accessd, it outputs nil."
  (let* ((object (specify (get-value-in-imagery-store cue modality)))
         (chunks (cond ((null object) 'no-more-items)
                       ((eql object 'no-more-items) 'no-more-items)
                       ((and use-rsf (can-matching-chunk-be-found-in-ltm? cue object t)))
                       ((does-it-sort-to-matching-chunk? object))
                       ((does-it-sort-to-a-list-of-chunks? object exit-time interrupt enable-search))
                       ((and (not use-rsf) (can-matching-chunk-be-found-in-ltm? cue object)))
                       (enable-search (search-for-matching-chunk object exit-time interrupt))
                       ('else nil))))
    (push 'top-of-stack *chunks*)
    (cond ((atom chunks) 
           (time-tally-to-access-chunk)
           (push chunks *chunks*))
          ('else 
           (loop for chunk in chunks
                 do (time-tally-to-access-chunk)
                 (push chunk *chunks*))))
    chunks))
    
    
(defun get-object-for (section modality)
  "This routine gets an object that takes in all of the members of a group in
an imagery store.  It returns the object or returns nil if the members of the
group could not be accessd due to decay."
  (let ((object (copy-object (get-value-in-imagery-store section modality))))
    (when object
      (remove-value object 'time)
      (setf object (specify object))
      (cond ((is-there-a-blank-in-an-stm-object? object)
             nil)
            ('else object)))))

(defun is-there-a-blank-in-an-stm-object? (object)
  (loop for subobject in object
        if (eql subobject #\_)
        return t
        if (and (listp subobject)
                (is-there-a-blank-in-an-stm-object? subobject))
        return t))

(defun does-chunk-match-section? (chunk section modality)
  (let* ((object (get-object-for section modality)))
    (cond ((null object) nil)
          ('else (does-object-match-chunk? object chunk)))))

(defun does-it-sort-to-matching-chunk? (object)      
  (let ((chunk (sort-to-node object)))
    (cond ((does-chunk-match-object? object chunk) chunk)
          ('else nil))))

(defun does-it-sort-to-a-list-of-chunks? (object &optional exit-time interrupt enable-search)
  (loop for pair = (find-chunk-and-remainder object exit-time interrupt enable-search) 
        then (find-chunk-and-remainder remainder exit-time interrupt enable-search)
        for chunk = (first pair)
        for remainder = (second pair)
        if pair
        collect chunk into chunks
        if (null pair)
        return nil
        if (null remainder)
        return chunks))

          
(defun find-chunk-and-remainder (obj &optional exit-time interrupt enable-search)
  (loop with result
        for object = obj then (reverse (cdr (reverse object)))
        for n from 0
        for chunk = (sort-to-node (specify object))
        if (does-chunk-match-object? (specify object) chunk)
        return (list chunk (find-remainder-of obj n))
        if (atom (specify object))
        return nil
        if (null (cdr object))
        return nil
        if (null (cddr object)) ; Here there is only one subobject and it is not an atom.
        do 
        (when enable-search 
          (setf result (search-for-matching-chunk (specify object) 
                                                  exit-time 
                                                  interrupt)))
        (when (not result) (return nil))
        (return (list result (find-remainder-of obj n)))))
        

(defun find-remainder-of (obj n)
  (let ((object (copy-object obj)))
    (cond ((= n 0) nil)
          ('else (let ((result 
                        (cons (car object) (nthcdr (- (length object) n) object))))
                   (cond ((null (cdr result)) nil)
                         ('else result)))))))


        
(defun can-matching-chunk-be-found-in-ltm? (cue object &optional use-rsf)
  (when *output-from-ltm*
    (let* ((image (find-associate 'episode cue 'after nil use-rsf))
           (combined-object (combine-object-with-image object image)))
      (cond ((null combined-object) nil)
            ((does-it-sort-to-matching-chunk? combined-object))
            ((does-it-sort-to-a-list-of-chunks? combined-object))))))


 
(defun combine-object-with-image (obj image)
  "This routine forms a temporary new version of the object which includes the info on 
image that was blank on the object.  It returns nil if there was a difference between 
the image and object or if there are remaining blanks on the object not filled by the 
image.  Otherwise it returns the combined version of object and image."
  (let ((object (copy-object (specify obj))))
     (combine-object-and-image object image)))

(defun combine-object-and-image (object image)
   (cond ((atom object) (cond ((and (eql object #\_) (eql image #\_))
                               nil)
                              ((eql image #\_) object)
                              ((eql object #\_) image)
                              ((eql object image) object)
                              ('else nil)))
         ('else
          (loop for object-remainder on (cdr object)
            for image-remainder = (cdr image) then (cdr image-remainder)
            for subobject = (car object-remainder)
            for subimage = (car image-remainder)
            if (and (eql subobject '~)
                    (null (cdr object-remainder)))
            return object
            if (eql subobject '~)
            return nil
            if (and (eql subobject #\_) 
                    (or (eql subimage #\_)
                        (null subimage)
                        (eql subimage '~)))
            return nil
            else if (eql subobject #\_) 
            do (rplaca object-remainder subimage)
            else if (or (eql subimage #\_)
                        (null subimage)
                        (eql subimage '~))
            do 'nil
            else if (and (listp subimage) (listp subobject))
            do (or (combine-object-and-image subobject subimage)
                   (return nil))
            else if (and (listp subobject) (characterp subimage))
            return nil
            else if (and (listp subobject) (numberp subimage))
            return nil
            else if (and (listp subobject) 
                         (not (does-object-match-chunk? subobject subimage)))
            return nil
            else if (and (not (listp subobject))
                         (not (eql subobject subimage)))
            return nil
            finally (return object)))))


(defun search-for-matching-chunk (object &optional exit-time interrupt)
  "This routine searches the net for a node whose image is a complete match for the 
given object.  That object is assumed to have some of its subobjects 
blanked out.  Search is interrupted,
and the routine thus exits nil if a given exit-time is exceeded or if
a given interrupt evaluates to t during the search process. Once a chunk is 
found, its image is sorted to the net to get to the default terminal node 
for images with that information."
  (when (not (eql (get-value object 'is-a)
                  'auditory))
    ;; Note searches in the auditory net are unlikely to work
    ;; and are difficult to simulate since every word that a 
    ;; person hears throughout the day gets sorted through this net
    (let ((exit-test #'(lambda (x) 
                         (does-chunk-match-object? object x)))
          (result nil))
      (setf *search-interrupt* (cond ((and exit-time interrupt)
                                      #'(lambda () 
                                          (or (> *clock* (eval exit-time))
                                              (eval interrupt))))
                                     (exit-time 
                                      #'(lambda () 
                                          (> *clock* (eval exit-time))))
                                     (interrupt
                                      #'(lambda () (eval interrupt)))        
                                     ('else 'null-experimenter)))
      (when *print-strategy-updates*
        (format t "Now searching net for match to: ~a~%"
                (change-object-to-string object)))
      (setf result (depth-first-search-in-net *net*
                                           object
                                           exit-test))
      (cond ((null result) nil)
            ((eql result 'blank) nil)
            ; The purpose of the following 'else is to find the test node
            ; where the bottom image of a chunk is located in case there
            ; are redundant images.
            ('else (get-chunk-for (get-image-of result)))))))
  
(defun reset-new-stimulus-flags (modality)
  (when (not (is-this-group-in-sensory-store? 'new modality))
    (setf *new-stimulus-flag* nil)
    (setf (get modality 'new-stimulus) nil)))

(defun is-this-group-in-sensory-store? (section modality)
  (loop for object in (get modality 'sensory-store)
        for group = (get-value object 'group)
        if (eq group section) return t))

(defun access-frontwards-and-backwards (section modality)
  (let ((object (get-first-object-in-sensory-store section modality)))
    (cond (object (print-store modality 'sensory-store)
                  (reset-new-stimulus-flags modality)
                  (push 'top-of-stack *chunks*)
                  (time-tally-to-access-chunk)
                  (push (sort-to-node (specify object)) *chunks*)
                  (push (get-chunk-for (find-remainder object (car *chunks*)))
                        *chunks*)
                  t)
          ('else (reset-new-stimulus-flags modality)
                 nil))))

(defun find-remainder (object chunk)
  "This routine finds the remainder of an object after cdring off the number
of subobjects corresponding to the number of subobjects of the chunk. New on
February 22, 1998, this routine puts a squiggle in front of the object so that
recognition will proceed from the back of the chunk in."
  (cond ((null chunk) object)
        ((numberp chunk) object)
        ((characterp chunk) object)
        ('else (let* ((image-length (find-length-of-object (get chunk 'image))))
                 (cond ((null image-length) object)
                       ((= image-length 0) object)
                       ('else 
                        (cons (car object) 
                              (cons '~ (nthcdr (1+ image-length) object))))
                       )))))



(defun sort-object-to-category (object)
  (cond ((characterp object) nil)
        ((numberp object) nil)
        ('else (extract-associate 'category object))))


(defun get-chunks-off-top-of-stack ()
  "This routine starts at the front of the stack and collects all of the
chunks until it reaches the 'top-of-stack symbol.  Then it deletes them from
the stack and returns the chunks."
  (loop for chunk = (pop *chunks*)
        if (eq chunk 'top-of-stack)
        return (reverse chunklist)
        else if (not *chunks*)
        return (reverse chunklist)
        else collect chunk into chunklist))
        
(defun translate-chunks (attribute)
  "This routine finds the associate of each chunk in the stack up to the top
of the stack with the given attribute and replaces the chunks in the stack
with their associates.  For example if the attribute is 'visual-auditory then
the auditory chunks in the chunk stack are replaced with their visual 
associates.  If the attribute is nil, the system finds the paired associate."
  (let ((chunks (get-chunks-off-top-of-stack)))
    (push 'top-of-stack *chunks*)
    (loop for chunk in chunks
          do (push (extract-associate attribute chunk) *chunks*))))


(defun translate-chunks-from-auditory-to-visual ()
  (translate-chunks 'auditory-visual))

(defun translate-chunks-from-visual-to-auditory ()
  (translate-chunks 'visual-auditory))

(defun is-chunk-on-chunks? (chunk)
  (let ((chunks (get-chunks-off-top-of-stack)))
    (push 'top-of-stack *chunks*)
    (loop for chunk2 in chunks
          do (push chunk2 *chunks*))
    (member chunk chunks)))

(defun how-many-chunks-are-on-chunks? ()
  (let ((chunks (get-chunks-off-top-of-stack)))
    (push 'top-of-stack *chunks*)
    (loop for chunk in chunks
          count chunk
          do (push chunk *chunks*))))

(defun convert-initial-cv-to-chunk ()
  "This routine is used in the intra-list similarity simulation to 
group the first two chunks in the chunk store into a visual chunk"
  (let* ((chunks (get-chunks-off-top-of-stack))
         (object (list '((is-a visual)) 
                       (convert-chunk-to-object (first chunks))
                       (convert-chunk-to-object (second chunks))))
         (chunk (get-chunk-for object)))
    (push 'top-of-stack *chunks*)
    (if (does-chunk-match-object? object chunk)
      (progn (push chunk *chunks*)
             (push (third chunks) *chunks*))
      (loop for chnk in chunks do (push chnk *chunks*)))))

(defun convert-terminal-vc-to-chunk ()
  "This routine is used in the bugelski simulation to 
group the second and third chunks in the chunk store into a visual chunk"
  (let* ((chunks (get-chunks-off-top-of-stack))
         (object (list '((is-a visual)) 
                       (convert-chunk-to-object (second chunks)) 
                       (convert-chunk-to-object (third chunks))))
         (chunk (get-chunk-for object)))
    (push 'top-of-stack *chunks*)
    (if (does-chunk-match-object? object chunk)
      (progn (push (first chunks) *chunks*)
             (push chunk *chunks*))
      (loop for chnk in chunks do (push chnk *chunks*)))))


(defun convert-digits-to-number (section)
  "This is an operator that can be called by the rehearse-group routine.  It
converts two or three or four digit groups to a chunked number if the given
section is full.  If the group has two digits the numbers one-two would
get converted to twelve, if the group has three digits one-one-three would be
converted to one-thirteen, if the group has four digits the number
one-two-two-three would be converted to twelve-twenty-three."
  (print "I don't understand what is happening here!!!")
  (print "How can you have an object with no is-a???")
  (setf *section* section)
  (break)
  (let* ((chunks (get-chunks-off-top-of-stack))
         (n (length chunks)))
    (push 'top-of-stack *chunks*)
    (cond ((not (= n (1- (length (get section 'image)))))
           (loop for chnk in chunks do (push chnk *chunks*)))
          ((= n 2) 
           (push 
            (extract-associate 
             'visual-auditory
             (get-chunk-for 
              (list nil 
                    (extract-associate 'value (first chunks))
                    (extract-associate 'value (second chunks)))
              ))
            *chunks*))
          ((= n 3)
           (push (first chunks) *chunks*)
           (push 
            (extract-associate 
             'visual-auditory
             (get-chunk-for 
              (list nil 
                    (extract-associate 'value (second chunks))
                    (extract-associate 'value (third chunks)))))
            *chunks*))
          ((= n 4) 
           (push 
            (extract-associate 
             'visual-auditory
             (get-chunk-for 
              (list nil 
                    (extract-associate 'value (first chunks))
                    (extract-associate 'value (second chunks)))))
            *chunks*)
           (push 
            (extract-associate 
             'visual-auditory
             (get-chunk-for 
              (list nil 
                    (extract-associate 'value (third chunks))
                    (extract-associate 'value (fourth chunks)))))
            *chunks*)))))

(defun access-syllable (group modality1 modality2 section &optional operator aloud 
                              dont-mark-as-old is-a)
  "This routine looks at the first object with the group label in the
sensory-store of modality1.  it uses access and divide to 
divide it into its subobjects.  If modality1 differs from modality2 it uses
translate to change the modality of the subobjects.  If an operator is given
it applies the operator to further change the chunks.  Then it subvocalizes the
object into the imagery store of modality2 with the given section name."
  (when (access-next-object-in-sensory-store group modality1 nil dont-mark-as-old)
    (when operator (apply operator nil))
    (translate-chunks (get modality1 modality2))
    (subvocalize-chunks-into modality2 section nil aloud is-a)
    (print-imagery-store modality2)
    t))



;;            B. ROUTINES THAT PUT INFO INTO MEMORY STORES

(defun remove-value-from-imagery-store (attribute modality)
  "This routine removes a value from the imagery store.  If a section is
given but there is not yet an association list for this section this routine
creates a null association list for the section.  This aspect of the routine
is especially useful when this routine is called by put-value-in-imagery-store."
  (loop for sublist in (get modality 'imagery-store)
        for attrib = (car sublist)
        if (not (eql attrib attribute))
        collect sublist into alists
        finally (setf (get modality 'imagery-store) alists)))

(defun put-value-in-imagery-store (attribute value modality)
  (remove-value-from-imagery-store attribute modality)
  (setf (get modality 'imagery-store) 
        (cons (list attribute value) (get modality 'imagery-store))))


(defun get-value-in-imagery-store (attribute modality)
  (loop for pair in (get modality 'imagery-store)
        if (eql (first pair) attribute)
        return (second pair)))

(defun visualize-value-into-sketch-pad (section value attribute)
  (let ((object (get-value-in-imagery-store section 'visual)))
    (when (not object) (setf object (list (list (list 'is-a 'visual)))))
    (put-test-value-on object value attribute)
    (time-tally-to-visualize-attribute)
    (put-value-in-imagery-store section object 'visual)))

(defun remove-value-from-sketch-pad (section attribute)
  (let ((object (get-value-in-imagery-store section 'visual)))
    (when (not object) (setf object (list (list (list 'is-a 'visual)))))
    (remove-value object attribute)
    (time-tally-to-visualize-attribute)
    (put-value-in-imagery-store section object 'visual)))


(defun subvocalize-chunks-into (modality section 
                                 &optional concurrent-routine aloud is-a 
                                 preserve-squiggle)
   "This routine subvocalizes the chunks that were in chunks into the imagery 
 store of the given modality.  The chunks are assumed to make up the entire 
 section and to have complete information."
   (let* ((chunks (get-chunks-off-top-of-stack))
          (object (cond ((null chunks) nil)
                        ((null (cdr chunks)) (convert-chunk-to-object (car chunks)))
                        ('else (convert-chunk-to-object chunks modality 
                                 preserve-squiggle)))))
      (cond (aloud (subvocalize-one-image-aloud section object modality is-a))
            ('else (subvocalize-one-image section object modality 
                     concurrent-routine is-a)))))



(defun subvocalize-one-image (cue obj modality &optional concurrent-routine is-a)
  (let ((object (cond ((atom obj) (list nil obj))
                      ('else (copy-object obj)))))
    (put-value-in-imagery-store cue object modality)
    (put-value object (get modality 'imagery-persistence) 'time)
    (when is-a (put-value object is-a 'is-a))
    (synchronize-clocks)
    (when (not (atom obj)) (time-tally-per-object))
    (when (not (atom obj)) (time-tally-to-vocalize object))
    (when concurrent-routine (apply concurrent-routine nil))
    (synchronize-clocks)
    ))

(defun subvocalize-one-image-aloud (cue obj modality is-a)
  "This routine subvocalizes a chunk and speaks it aloud at same time."
  (cond ((atom obj) 
         ; control symbols such as 'end-of-list do not get vocalized
         (subvocalize-one-image cue obj modality nil is-a))
        ('else 
         (let ((object (copy-object obj)))
           (put-value-in-imagery-store cue object modality)
           (put-value object (get modality 'imagery-persistence) 'time)
           (when is-a (put-value object is-a 'is-a))
           (synchronize-clocks)
           (speak-object-entering-it-in-echo object 'old 'both)))))

    


        
;;             C. ROUTINES THAT MANIPULATE SECTION NAMES



(defun rename-section (modality is-a-of-store group1 group2 &optional upto-control-symbol)
  "Rename section renames all items of group1 as group2.  If group2 is nil,
this routine causes that group to be ignored."
  (cond ((eq is-a-of-store 'sensory-store)
         (loop for item in (get modality 'sensory-store)
               if (eq (get-value item 'group) group1)
               do (put-value item group2 'group)
               else if (and (eq (get-value item 'group) 'control-symbol)
                            (eq (second item) upto-control-symbol))
               return t))
        ((eq is-a-of-store 'imagery-store)
         (let ((values (get-value-in-imagery-store group1 modality)))
           (remove-value-from-imagery-store group1 modality)
           (put-value-in-imagery-store group2 values modality)))
        ('else (print "ERROR! That is-a-of-store is not supported!")
               (break))))

;;          D. ROUTINES WHICH ANSWER QUESTIONS ABOUT MEMORY STORES


(defun does-this-group-exist? (group modality is-a-of-store)
  "This routine returns t if there is at least one member of the
group in question in the memory store and returns nil if not."
  (cond ((eq is-a-of-store 'sensory-store)
         (loop for item in (get modality 'sensory-store)
               if (eq (get-value item 'group) group) return t))
        ((eq is-a-of-store 'imagery-store)
         (get-value-in-imagery-store group modality))
        ('else (print "ERROR! That is-a-of-store is not supported!")
               (break))))

(defun how-many-of-this-group? (group sensory-store)
  (loop for item in sensory-store
        count (eq (get-value item 'group) group)))        


(defun wait-for-item ()
  "Wait for item to enter a sensory store.  This routine tests for a 
*new-stimulus-flag*.  It exits t when new-stimulus is presented.
Note that the advance-the-timer routine acts to set the experimenter routine in 
motion.  The experimenter routine will set the new stimulus flag if
it has placed something in a sensory store."
  (loop initially (advance-the-timer 10)
        until *new-stimulus-flag*
        do (when *print-time-updates* (princ "Waiting for "))
        (advance-the-timer 100)))

(defun wait-and-check-for-signal-to-respond ()
  "This routine waits for a new-stimulus-flag and when one appears checks if the
new stimulus is the signal-to-respond.  If it is the signal to respond it exits
t if not it exits nil."
  (when *print-strategy-updates*
    (format t "Waiting for item to appear in icon.~%"))
  (wait-for-item)
  (eq (cadar (get 'visual 'sensory-store)) 'signal-to-respond))


(defun memory-store-search (memory-store control-symbol &optional nil-it-out)
  "This routine searches for a control-symbol within a sensory store
It returns nil if the control symbol is not found or t if it is found. 
Control symbol is defined as a member of the group 'control-symbol. 
Even if the control symbol is in group nil, it returns t."
    (loop for item in memory-store
          for group = (get-value item 'group)
          if (and (eq group 'control-symbol)
                  (eq (cadr item) control-symbol))
          do (when nil-it-out (put-value item nil 'group))
          and return t))

;;                  E. REHEARSAL AND OUTPUT ROUTINES

(defvar *this-chunk* nil) ; This is used when debugging rehearse-one-by-one

(defun rehearse-one-by-one (attribute modality &optional exit-time 
                                      interrupt concurrent-routine exit-cue non-cumulative-strategy)
  "This routine rehearses a memory store or group one-at-a-time.  Beginning with the 
attribute.  If the rehearsal starts at the beginning of the list that attribute
might be 'beginning.  This routine attempts to engage in learning during each rehearsal.
It has the following exits:
   1. 'no-more-items means it reached a point where it could not find another item to rehearse.
       This can happen at the end of a list not marked by 'end-of-list.  It can also occur
       if there is a broken link in the rehearsal chain.
   2. 'end-of-list means it reached the end of the list symbol or the response to the exit-cue 
   3. 'interrupted means that rehearsal was interrupted by the interrupt -- usually a new stimulus
      flag."
  (let ((rsf (null non-cumulative-strategy))) ; set rsf nil when non-cumulative strategy is specified
    (advance-the-timer 10)
    (when *print-strategy-updates*
      (format t "~a Rehearsing one-by-one: " *clock*))
    (loop for cue = attribute then object
          for chunk-or-chunks = (access-next-object-in-imagery-store (get-chunk-for cue) 
                                                                     modality 
                                                                     exit-time
                                                                     interrupt)
          for object = (convert-chunk-to-object chunk-or-chunks 'auditory)
          for n = 1 then (1+ n)
          with maximum-size = 20
          do (when *print-strategy-updates*
               (format t "~a " (change-object-to-string object)))
          (cond ((atom chunk-or-chunks)
                 (pop *chunks*)
                 (pop *chunks*))
                ('else
                 (loop repeat (length chunk-or-chunks)
                       do (pop *chunks*)
                       finally (pop *chunks*))))                              
          if (null chunk-or-chunks)
          do (when *print-strategy-updates* (terpri))
          and return nil
          if (eval interrupt) 
          do (when *print-strategy-updates* (terpri))
          and return 'interrupted 
          if (eql chunk-or-chunks 'no-more-items)
          do (when *print-strategy-updates* (terpri))
          and return 'no-more-items
          if (> *epam-clock* *clock*)
          do (when *print-epam-updates* 
               (format t "~%EPAM-clock = ~a, Clock = ~a. Not studying because EPAM is occupied.~%"
                       *epam-clock* *clock*))
          if (<= *epam-clock* *clock*)
          do (study-serial-paired-association 'episode cue object rsf)
          do (subvocalize-one-image (get-chunk-for cue) object 'auditory concurrent-routine)
          if (eql object 'end-of-list)
          do (when *print-strategy-updates* (terpri))
          and return 'end-of-list
          if (eql (get-chunk-for cue) exit-cue)
          do (when *print-strategy-updates* (terpri))
          and return 'end-of-list
          if (= n maximum-size) 
          do (when *print-strategy-updates* (terpri))
          and return 'no-more-items)))

            

(defun study-serial-paired-association (episode cue object rsf &optional correct?)
  (let (subjects-response stimulus-response-object)
     (identity episode) ; included to prevent error message as the variable episode is not used.
    (setf *epam-clock* *clock*)
    (when (or (listp cue)
              (characterp cue)
              (numberp cue)
              (not (get-image-of cue)))
      ; only do this routine when cue is an object, if it is a node, exit without learning.
      (setf subjects-response (find-associate 'episode cue 'after 'instant rsf))
      (when (not correct?)
        (setf correct? (perfect-subobject-match? subjects-response object)))
      (cond ((not rsf) ; if using a paired-associate strategy to learn a serial list
             (when *print-epam-updates*
               (format t "~%EPAM is not occupied so studying stimulus = ~a and response ~a as paired association.~%"
                       cue object))
             (study-paired-association 'episode cue object 'before 'after nil correct?))
            ((not (is-cue-or-object-on-rsf? cue object))
             (when *print-epam-updates*
               (format t "Not studying because not at an anchor-point.~%"))
             nil)
            ('else
             (when *print-epam-updates*
               (format t "Studying because at an anchor-point and random-number less than *sal-b*.~%"))
             ;(break)
             (setf stimulus-response-object 
                   (create-stimulus-response-object 'episode cue object 'before 'after))
             (or *rsf* (create-new-episode))
             (cond ((study cue)
                    (loop while (study cue))
                    (when *print-epam-updates*
                       (format t "cue = ~a was not familiar so studying it.~%"
                              cue))
                    t)
                   ((study object)
                    (loop while (study object))
                    (when *print-epam-updates*
                       (format t "object = ~a was not familiar so studying it.~%"
                              object)))
                   ((familiarize stimulus-response-object *rsf* 'surface)
                    (create-link-to *rsf* (get-chunk-for cue))
                    (create-link-to *rsf* (get-chunk-for object))
                    (when *print-epam-updates*
                       (format t "Familiarizing sr-object = ~a~%"
                         stimulus-response-object))
                    t)))))))
 

(defun is-cue-on-rsf? (cue)
  (cond ((member cue *beginning-control-symbols*) t)
        ((eql cue 'end-of-list) t)
        ('else (let ((image (get-image-of *rsf*))
                     (chunk1 (get-chunk-for cue)))
                 (loop for item in (cdr image)
                       for chunk3 = (get-chunk-for (specify item))
                       if (is-it-either-same-or-ancestor? chunk3 chunk1)
                       return t)))))


(defun is-cue-or-object-on-rsf? (cue object)
  (or (is-cue-on-rsf? cue)
      (is-cue-on-rsf? object)))
 

(defun synchronize-clocks ()
  (when (< *articulation-clock* *clock*) 
    (setf *articulation-clock* *clock*))
  (when (< *writing-clock* *clock*)
    (setf *writing-clock* *clock*))
  (loop for difference1 = (- *articulation-clock* *clock*)
        for difference2 = (- *writing-clock* *clock*)
        if (> difference1 100) do (advance-the-timer 100)
        else if (> difference2 100) 
        do (when *print-time-updates* (format t "Synchronizing clocks:"))
        (advance-the-timer 100)
        else if (> difference1 0) 
        do (when *print-time-updates* (format t "Synchronizing clocks:"))
        (advance-the-timer difference1)
        else if (> difference2 0) 
        do (when *print-time-updates* (format t "Synchronizing clocks:"))
        (advance-the-timer difference2)
        else do (return)))


(defun rehearse-section (section modality &optional operator exit-time interrupt is-a)
  "This routine rehearses a section.  If an operator is specified, than
while the section is in *chunks* the operator is applied to the section."
  (advance-the-timer 10)
  (when *print-strategy-updates*
    (format t "~a Rehearsing Section anchored by chunk-for ~a:~%" 
            *clock* 
            (get-print-copy-of section)))
  (access-next-object-in-imagery-store section 
                                       modality 
                                       exit-time)
  (when (not (eval interrupt))
    (when operator (apply operator nil))
    (subvocalize-chunks-into modality section nil nil is-a)
    (print-imagery-store 'auditory)
    t))

(defun subtract-three ()
  "This is an operator that can be called by the rehearse-group routine.  It
subtracts three from the spoken number that is in chunks and reenters the 
spoken chunks of the result into chunks.  If there are two chunks in chunks,
it assumes that the first one represents the hundreds place.  If there are
three chunks in chunks, it assumes that the first one represents the hundreds
place and the second one represents the tens place."
  (let* ((chunks (get-chunks-off-top-of-stack))
         (digit1 (extract-associate 'value (first chunks)))
         (digit2 (extract-associate 'value (second chunks)))
         (digit3 (when (third chunks) 
                   (extract-associate 'value (third chunks)))))
    (cond ((or (and (first chunks) (null digit1))
               (and (second chunks) (null digit2))
               (and (third chunks) (null digit3)))
           (loop for chunk in chunks
                 initially (push 'top-of-stack *chunks*)
                 do (push chunk *chunks*)))
          ('else
           (let ((number (cond (digit3
                                (+ (* 100 digit1) (* 10 digit2) digit3  -3))
                               (digit2
                                (+ (* 100 digit1) digit2  -3))
                               ('else digit1))))
           (setf digit1 (truncate (/ number 100)))
           (setf digit3 (mod number 100))
           (cond ((< digit3 10) (setf digit2 0))
                 ('else (setf digit2 nil)))
           (push 'top-of-stack *chunks*)
           (push (extract-associate 'visual-auditory digit1) *chunks*)
           (when digit2
             (push (extract-associate 'visual-auditory digit2) *chunks*))
           (push (extract-associate 'visual-auditory digit3) *chunks*))))))


(defun output-section (section modality &optional operator exit-time)
  (when (access-next-object-in-imagery-store section modality exit-time nil nil 'enable-search)
    (when *print-strategy-updates*
      (format t "~a Outputting ~a from ~a imagery-store:~%" 
              *clock* 
              section 
              modality))
    (when operator (apply operator nil))
    (setf *output* (append *output* (output-chunks)))))


(defun output-one-by-one (modality &optional exit-time number operator
                                   concurrent-routine)
  "This routine accesses all of the objects of a group within
the phonological loop and outputs them.  When an operator is specified
that operator is executed on the chunk before it is output.  A sample operator
would be one that converts auditory chunks to visual chunks.
It will stop when either the time limit or the optional number of items are
accessd or it can't find the next object.  It uses access-next-with-search
to perform the recognition.  Output is the result of output one-by-one.  Also
the value of *cue* which is the last cue if there was a cue whose object could 
not be matched."
  (when *print-strategy-updates*
    (format t "~a Outputing auditory imagery-store:~%" *clock*))
  (print-store 'auditory 'imagery-store)
  (loop for cue = 'beginning then chunk
        for chunk-or-chunks = (access-next-object-in-imagery-store cue 'auditory exit-time)
        for object = (convert-chunk-to-object chunk-or-chunks 'auditory)
        for chunk = (get-chunk-for object)
        for n = 1 then (1+ n)
        with maximum-size = (or number 9)
        if (> n maximum-size) return nil
        else if (and exit-time (> *clock* exit-time)) return nil
        else if t 
        do 
        (when (eq chunk-or-chunks 'no-more-items) 
          (pop *chunks*)
          (return nil))
        (when (null chunk) (return cue))
        (output-depending-upon-modality object modality operator concurrent-routine)))

(defun output-depending-upon-modality (object modality &optional operator concurrent-routine)
  "In this routine the object is an auditory object.  If the output modality is 
auditory, then just speak it.  If the modality is visual then it will be necessary
to translate it and then speak it."
  (cond ((eql modality 'auditory)
         (setf *output* (append *output* (speak-object-entering-it-in-echo object 'old 'aloud))))
        ('else (let ((chunk (get-chunk-for object)))
                 ; this is not debugged
                 (push 'top-of-stack *chunks*)
                 (push chunk *chunks*)
                 (when operator (apply operator nil))
                 (setf *output* (append *output*
                                        (output-chunks nil concurrent-routine)))))))

(defun speak-object-entering-it-in-echo (obj section &optional aloud-or-both)
  (let* ((object (copy-object obj))
         (string (change-object-to-string object)))
    (when (not aloud-or-both) (setf aloud-or-both 'aloud))
    (put-value object section 'group)
    (put-value object (get 'auditory 'sensory-persistence) 'time)
    (setf (get 'auditory 'sensory-store)
          (append (get 'auditory 'sensory-store) (list object)))
    (synchronize-clocks)
    (time-tally-per-object aloud-or-both)
    (time-tally-to-vocalize object aloud-or-both)
    (synchronize-clocks)
    (list string)
    ))



(defun output-chunks (&optional backwards concurrent-routine)
  "This routine prints the images of the chunks in the chunks stack up to the
'top-of-stack control symbol and then it outputs a list of those chunks in
string form."
  (let ((chunks (get-chunks-off-top-of-stack)))
    (loop for chunk in (if backwards (reverse chunks) chunks)
          for object = (convert-chunk-to-object chunk)
          for string = (change-object-to-string object)
          collect string
          do (when *print-major-updates* (format t "~a~%" string))
          (synchronize-clocks)
          (cond ((and (listp object) (eq (get-value object 'is-a) 'visual))
                 (time-tally-to-write object))
                ('else (time-tally-per-object 'aloud)
                       (time-tally-to-vocalize object 'aloud)))
          (when concurrent-routine (apply concurrent-routine nil))
          (synchronize-clocks))))

(defun output-from-echo (cue &optional concurrent-routine visual)
  (when *output-from-echo*
    (let ((object (get-value-in-imagery-store cue 'auditory))
          (operator (when visual 'translate-chunks-from-auditory-to-visual))
          (modality (cond (visual 'visual)
                          ('else 'auditory))))
      (when *print-strategy-updates*
        (format t "Switching to echo box. Looking for match to ~a~%"
                (change-object-to-string object)))
      ; (setf *object* object)
      ; (setf *cue* cue)
      ; (break)
      (rename-section 'auditory 'sensory-store 'old 'new 'signal-to-respond)
      (loop with chunk-or-chunks
            with object2
            if (access-next-object-in-sensory-store 'new 'auditory)
            do ;if find the searched for object in the front of the remaining new objects
            ; in the echo box, then output that object and all those following it.
            (setf chunk-or-chunks (get-chunks-off-top-of-stack))
            (setf object2 (convert-chunk-to-object chunk-or-chunks 'auditory))
            (when (does-image-match-object? object2 object)
              (output-depending-upon-modality object2 modality operator concurrent-routine)
              (loop while (access-next-object-in-sensory-store 'new 'auditory)
                    do (setf chunk-or-chunks (get-chunks-off-top-of-stack))
                    (setf object2 (convert-chunk-to-object chunk-or-chunks 'auditory))
                    (output-depending-upon-modality object2 modality operator concurrent-routine))
              (return t))
            else if t
            do ; if can't find a match between objects in sensory store and searched for object
            ; then output 'blank followed by all of the items that are in the sensory store.
            (setf *output* (append *output* (list 'blank)))
            (rename-section 'auditory 'sensory-store 'old 'new 'signal-to-respond)
            (loop while (access-next-object-in-sensory-store 'new 'auditory)
                  do (setf chunk-or-chunks (get-chunks-off-top-of-stack))
                  (setf object2 (convert-chunk-to-object chunk-or-chunks 'auditory))
                  (output-depending-upon-modality object2 modality operator concurrent-routine))
            (return t)))))
            

(defun can-matching-chunk-be-found-in-echo? (cue)
  "This routine leaves the chunks found in *chunks* and returns the object if a matching
object found in ECHO."
  (when *output-from-echo*
    (let ((object (get-value-in-imagery-store cue 'auditory)))
      (rename-section 'auditory 'sensory-store 'old 'new 'signal-to-respond)
      (loop for chunks = (access 'new 'auditory 'sensory-store)
            for echo-object = (when chunks (convert-chunk-to-object chunks 'auditory))
            if (not chunks)
            return nil
            if (does-image-match-object? echo-object object)
            do (when *print-strategy-updates*
                 (format t "Found match = ~a to object = ~a in ECHO!~%"
                         (change-object-to-string echo-object)
                         (change-object-to-string object)))
            and return echo-object))))




;;           F. SUBJECTS' STRATEGIES USED IN STM SIMULATIONS


(defun delayed-recall ()
  "This strategy follows the following algorithm: 
1. Input the three stimulus words and put them in beginning
2. Input the number and put it in group-2.
3. While waiting for the signal-to respond
   Replace the number in group-2 with a number that is three lower
4. Finally output the three words searching for missing information during
   output.
5. Returns the three letters found."
  (zero-out-variables)
  (wait-for-item)
  (access-next-object-in-sensory-store 'new 'auditory)
  (wait-for-item)
  (access-next-object-in-sensory-store 'new 'auditory 'same-group)
  (wait-for-item)
  (access-next-object-in-sensory-store 'new 'auditory 'same-group)
  (subvocalize-chunks-into 'auditory 'beginning)
  (wait-for-item)
  (access-next-object-in-sensory-store 'new 'auditory)
  (wait-for-item)
  (access-next-object-in-sensory-store 'new 'auditory 'same-group)
  (wait-for-item)
  (access-next-object-in-sensory-store 'new 'auditory 'same-group)
  (subtract-three)
  (subvocalize-chunks-into 'auditory 'group-2)
  (loop until *new-stimulus-flag*
        do (access-next-object-in-imagery-store 'group-2 'auditory)
        (subtract-three)
        (subvocalize-chunks-into 'auditory 'group-2)
        (print-imagery-store 'auditory))
  (auditory-output 'beginning 
                   (+ *clock* *time-allowed-for-recall*)
                   3))

(defun auditory-recall ()
  "This routine is the strategy for doing auditory span experiments when the 
stimuli are presented in auditory fashion and the subject does not group the
stimuli into rehearsal groups but instead tries to rehearse from beginning of
the list until the stimuli are too closely spaced to permit such rehearsal.
Several global variables control the strategy of this routine:
  If *modality-of-output* is 'visual then output will be in the form of written
responses.  If it is 'auditory then output will be in the form of spoken 
responses. 
  If a *concurrent-routine* is specified, then it will be executed each time
an item is subvocalizeed by the routine subvocalize-one-image, or output in 
writing or speech by the routine output-chunks.
  If the system knows the exact total length of the objects to be outputed that
is supplied as the *list-length*.  During written recall this list length is
used to place the output from the echo at the end of the list."
  (zero-out-variables)
  (when *concurrent-routine* 
    (setf *suppression-chunk* (sort-to-node *suppression-object*)))
  (wait-for-item)
  (loop with cue = 'beginning
        with strategy = 'cumulative
        with result
        if (and *new-stimulus-flag* 
                (access-next-object-in-sensory-store 'new 'auditory))
        do 
        (when *print-strategy-updates* 
          (format t "Subvocalizing it into auditory imagery store.~%"))
        (subvocalize-chunks-into 'auditory cue *concurrent-routine*)
        (setf cue (sort-to-node (get-value-in-imagery-store cue 'auditory)))
        else if (memory-store-search (get 'auditory 'sensory-store) 
                                     'signal-to-respond)
        return (auditory-recall-output-routine
                (+ *clock* *time-allowed-for-recall*))
        else if (eq strategy 'cumulative)
        do (setf result (rehearse-one-by-one 'beginning
                                             'auditory 
                                             nil
                                             '*new-stimulus-flag*
                                             *concurrent-routine*))
        (when (null result) (setf strategy 'paired))
        else do (rehearse-one-by-one cue
                                     'auditory 
                                     nil
                                     '*new-stimulus-flag*
                                     *concurrent-routine*)))

(defun auditory-recall-output-routine (&optional exit-time)
  "Subject's strategy is controlled by several variables, see the routine
auditory-recall for a description of these variables."
  (cond ((eq *modality-of-output* 'auditory)
         (auditory-output nil exit-time))
        ('else
         (visual-output exit-time *concurrent-routine*))))
           
(defun visual-output (&optional exit-time concurrent-routine)
  (let ((cue (output-one-by-one 'visual 
                                exit-time 
                                nil
                                'translate-chunks-from-auditory-to-visual
                                concurrent-routine)))
    (when cue (output-from-echo cue concurrent-routine 'visual)
          (loop while (< (length *output*) *list-length*)
                do (put-blank-in-output)))
    *output*))

(defun put-blank-in-output ()
  (loop for output-tail on *output*
        for item = (car output-tail)
        if (or (null item)
               (eq item 'blank))
        return (rplacd output-tail (cons 'blank (cdr output-tail)))
        finally (setf *output* (nconc *output* (list 'blank)))))


(defun auditory-output (&optional section exit-time section-length)
  (cond (section 
          (output-section section 'auditory nil exit-time))
        ('else
         (let ((cue (output-one-by-one 'auditory 
                                       exit-time 
                                       section-length)))
           (when cue (output-from-echo cue section))
           *output*))))


(defun concurrent-articulation ()
  (time-tally-to-input-concurrent-routine)
  (loop for chunk in (cdr (get *suppression-chunk* 'image))
        for object = (convert-chunk-to-object chunk)
        do 
        (when *print-imagery-stores*
             (format t "~a Articulating ~a~%" 
                     *articulation-clock* (change-object-to-string object)))
        (time-tally-to-vocalize object 'aloud)))



(defun serial-probe-recall (rehearsal-strategy &optional delayed-recall)
  "Note the end-of-list signal is interpreted differently depending upon whether or not
delayed-recall is true.  If it is true, then end-of-list begins the interpolated task
and probe stage.  If not, then end-of-list simply indicates that one iteration of the 
list has been completed."
  (zero-out-variables)
   ;(setf *rehearsal-strategy* rehearsal-strategy)
   ;(turn-printing-on)
   (setf *print-net-updates* nil)
  (loop with cue = 'beginning 
        with anchor = 'beginning
        with chunk = 'beginning
        do 
        (setf cue chunk)
        (when *print-strategy-updates* 
             (format t "Serial-probe cycle with Anchor = chunk-for ~a, Cue = chunk-for ~a~%" 
                     (get-print-copy-of anchor)
                     (get-print-copy-of cue)))
        if (not (does-this-group-exist? 'control-symbol 'visual 'sensory-store))
        do (wait-for-item)
        if (memory-store-search (get 'visual 'sensory-store) 
                                'beginning
                                'nil-it-out)
        do (setf *new-stimulus-flag* nil)
        (memory-store-search (get 'visual 'sensory-store) 
                             'end-of-list
                             'nil-it-out) ; This nils out an 'end-of-list that might have been missed.
        (setf chunk 'beginning)
        (setf anchor 'beginning)
        (setf cue 'beginning)
        else if (memory-store-search (get 'visual 'sensory-store) 
                                      'end-of-list
                                      'nil-it-out)
        do (cond (delayed-recall
                  (return (do-interpolated-task-and-probe rehearsal-strategy)))
                 ('else
                  (setf *new-stimulus-flag* nil)
                  (push 'top-of-stack *chunks*)
                  (push 'end-of-list *chunks*)
                  (subvocalize-chunks-into 'auditory cue)
                  (setf chunk 'end-of-list)
                  (cond ((eql rehearsal-strategy 'cumulative)
                         ; Instructions are to always rehearse from beginning of list
                         ; so here the anchor will always be 'beginning.
                         (cumulative-rehearsal-strategy 'beginning cue))
                        ((eql rehearsal-strategy 'current-segment)
                         (current-segment-strategy anchor cue))
                        ((eql rehearsal-strategy 'no-rehearsal)
                         (no-rehearsal-strategy anchor cue))
                        ('else nil))))
        else if (access-syllable 'new 'visual 'auditory cue nil 'aloud)
        do
        ;(when (< *epam-clock* *clock*)
        ;  (setf *epam-clock* *clock*)
        ;  (discriminate (get-value-in-imagery-store cue 'auditory)))
        (setf chunk (sort-to-node (get-value-in-imagery-store cue 'auditory)))
        (cond ((memory-store-search (get 'visual 'sensory-store) 
                                    'signal-to-respond)
               (return (respond-to-probe chunk 'auditory 
                                         (+ *clock* *time-allowed-for-recall*)
                                         rehearsal-strategy)))
              ((eql rehearsal-strategy 'cumulative)
               (cumulative-rehearsal-strategy anchor cue))
              ((eql rehearsal-strategy 'current-segment)
               (setf anchor (current-segment-strategy anchor cue)))
              ((eql rehearsal-strategy 'paired)
               (setf anchor (associate-strategy 'episode anchor cue)))
              ('else 
               (format t "Rehearsal-Strategy = ~a is not supported!" rehearsal-strategy)
               (break)))))


(defun cumulative-rehearsal-strategy (anchor cue)
   "This rehearsal strategy is given an anchor -- usually 'beginning which
 is the place to begin rehearsal and a cue -- the response to which is the 
 place to end rehearsal.  Its output is the anchor for the next rehearsal."
   (let ((result (rehearse-one-by-one anchor 'auditory nil nil
                   *concurrent-routine* cue)))
      (cond ((null result) cue)
            ((or (does-this-group-exist? 'control-symbol 'visual 'sensory-store)
                 (does-this-group-exist? 'control-symbol 'auditory 'sensory-store))
             anchor)
            ((eql result 'interrupted) cue)
            ((eql result 'no-more-items) cue)
            (*new-stimulus-flag* cue)
            ('else
             (loop do (setf result (rehearse-one-by-one anchor 'auditory nil '*new-stimulus-flag*
                                     *concurrent-routine* cue))
               if (null result) return cue
               if (not (eql result 'end-of-list)) return anchor
               if (or (does-this-group-exist? 'control-symbol 'visual 'sensory-store)
                      (does-this-group-exist? 'control-symbol 'auditory 'sensory-store))
               return anchor
               if *new-stimulus-flag* return cue)))))                             


(defun current-segment-strategy (anchor cue)
  (loop for result =  (rehearse-one-by-one anchor 'auditory nil '*new-stimulus-flag*
                                        *concurrent-routine* cue)
        if (not (eql result 'no-more-items))
        return cue
        else if  (or (does-this-group-exist? 'control-symbol 'visual 'sensory-store)
                     (does-this-group-exist? 'control-symbol 'auditory 'sensory-store))
        return cue))
  
(defun no-rehearsal-strategy (anchor cue)
  (identity anchor)
  (loop for result =  (rehearse-one-by-one cue 'auditory nil '*new-stimulus-flag*
                                        *concurrent-routine* cue)
        if (not (eql result 'no-more-items))
        return cue
        else if  (or (does-this-group-exist? 'control-symbol 'visual 'sensory-store)
                     (does-this-group-exist? 'control-symbol 'auditory 'sensory-store))
        return cue))


(defun do-interpolated-task-and-probe (rehearsal-strategy)
  (let (chunk-or-chunks chunk object)
    (print-store 'visual 'sensory-store)
    (wait-for-item)
    (access-next-object-in-sensory-store 'new 'visual)
    (translate-chunks-from-visual-to-auditory)
    (subtract-three)
    (subvocalize-chunks-into 'auditory 'group-2)
    (loop until *new-stimulus-flag*
          do (access-next-object-in-imagery-store 'group-2 'auditory)
          (subtract-three)
          (subvocalize-chunks-into 'auditory 'group-2)
          (print-imagery-store 'auditory))
    (access-next-object-in-sensory-store 'new 'visual)
    (translate-chunks-from-visual-to-auditory)
    (setf chunk-or-chunks (get-chunks-off-top-of-stack))
    (setf object (convert-chunk-to-object chunk-or-chunks 'auditory))
    (setf chunk (sort-to-node object)) ; This line would have to be rewritten if nonsense syllable are 
    ; used as probes in a delayed recall simulation.
    (respond-to-probe chunk 'auditory 
                      (+ *clock* *time-allowed-for-recall*)
                      rehearsal-strategy)))

(defun respond-to-probe (probe modality &optional exit-time rehearsal-strategy output-form)
  (let (use-rsf chunk-or-chunks object)
    (when *print-strategy-updates*
      (format t "Responding to probe = chunk-for-~a~%"
              (change-object-to-string (convert-chunk-to-object probe))))
    ;    These variables can be used when debugging
    ;    (setf *chunk* probe)
    ;    (setf *rehearsal-strategy* rehearsal-strategy)
    ;    (setf *output-form* output-form)
    (setf use-rsf (cond ((eql rehearsal-strategy 'cumulative) t)
                        ((eql rehearsal-strategy 'current-segment) t)
                        ('else nil)))
    (setf chunk-or-chunks (access-next-object-in-imagery-store probe modality exit-time nil use-rsf))
    (setf object (convert-chunk-to-object chunk-or-chunks modality))
    (when (or (eql chunk-or-chunks 'no-more-items)
              (null chunk-or-chunks))
      (cond ((can-matching-chunk-be-found-in-echo? probe)
             (setf chunk-or-chunks (get-chunks-off-top-of-stack))
             (setf object (convert-chunk-to-object chunk-or-chunks modality)))
            (*output-from-ltm*
             (setf object (find-associate 'episode probe 'after nil use-rsf)))))
    (setf *output* object)
    (cond ((eql output-form 'serial-anticipation)
           (setf *output* object))
          ('else 
           (setf *output* (list object))))
    (when *print-strategy-updates*
      (format t "Subject's response to probe = ~a~%"
              (change-object-to-string object)))
    (change-object-to-string object)))


;;;                 E. ROUTINES THAT PRINT MEMORY STORES

(defun print-store (modality is-a-of-store)
  "This routine prints a memory store whether it be an imagery store or a
sensory store."
  (if (eq is-a-of-store 'sensory-store)
    (print-sensory-store modality)
    (print-imagery-store modality)))


(defun print-sensory-store (modality)
  "This routine prints the sensory store of the given modality."
  (when *print-imagery-stores*
    (princ *clock*)
     (if (eq modality 'auditory)
      (princ " ECHO:")
      (princ " ICON:"))
    (print-memory-store (get modality 'sensory-store))
    (terpri)))

(defun print-imagery-store (modality)
  (if (eq modality 'auditory)
    (print-aud-loop)
    (print-pad)))

(defun print-aud-loop ()
  "This routine prints the auditory imagery-store."
    (when *print-imagery-stores*
      (princ "LOOP: ")
      (print-aud-loop-section (get 'auditory 'imagery-store))
      (terpri)
      (print-rsf)
      ))


(defun print-aud-loop-section (alists)
   (loop for pair in alists
     for attribute = (first pair)
     for item = (second pair)
     for attribute-object = (get-print-copy-of attribute)
     for n from 1
     if (atom attribute-object) 
     do (format t "~a->~a(~a), "
          attribute
          (change-object-to-string item)
          (get-value item 'time))
     else if t 
     do (format t "chunk-for-~a->~a(~a), "
          (change-object-to-string attribute-object)
          (change-object-to-string (specify item))
          (get-value item 'time))))

(defun print-pad ()
  "This routine prints the visual imagery-store."
   (loop initially (format t "PAD: ")
     for item in (get 'visual 'imagery-store)
     for section = (first item)
     for object = (second item)
     do (format t "~a->~a " section object)
     finally (terpri)))
                        

(defun print-memory-store (memory-store)
  "Print-Memory-Store prints the contents of a memory store in readable fashion.
Each member of the memory store is assumed to be an EPAM object."
  (loop for item in memory-store
        for time = (get-value item 'time)
        for simplified-object = (specify item)
        for object = (if (atom simplified-object) 
                       simplified-object
                       (change-object-to-string simplified-object))
        for group = (get-value item 'group)
        for label = (or (get group 'label) group)
        do (format t "~a(~a)~a " object time label)))



(defun get-stm-object-for (chnk &optional modality)
  "Get-stm-object-for gets an object that can be put in a memory store given
a chunk or an object"
  (let ((object chnk))
    (cond ((listp object) (copy-object object))
          ('else
           (setf object (convert-chunk-to-object object modality))
           (when (atom object)
             (setf object (list nil object)))
           object))))
  
(defun put-is-a-on-copy-of (object is-a)
  (cond ((atom object)
         (put-value (list nil object) is-a 'is-a))
        ('else (put-value (copy-object object) is-a 'is-a))))



; manager644.lsp
;
; Added a routine auditory-vc-parts that makes *auditory-cv-parts* and *auditory-cv-counterparts*


; manager642.lsp
;   added *trial-clock* in put-stimulus-in-sensory-store
;   added variable *net-list* in order to avoid the error message
;   changed so convert-tree-to-list doesn't have an argument since *net* is assumed.
;   wrote associate-if-necessary and replaced all occurrences of associate with that new routine.
;   restore-net-from-file now also sets *rsf* nil.
;   set *search-list* nil in restore-net-from-file 


; manager641.lsp
; rewrote underwood-experimenter so that it records the same info for a paired associate trial
; as does the quick routine.


; manager640.lsp
; This is a continuation of manager.lsp from epam5a.lsp
; Changes added in several variables
; Renamed timer advance-the-timer
; Rewrote convert-modality-of so that it would be able to convert modality of a character
; Rewrote create-chunks-for-words so that it used study-when-correct so that 
;     there would old words in memory wouldn't be changed when studying new ones.

;           These Routines Manage STM and Verbal Learning Simulations

;              CONTENTS
;                  A. Routines that update the clock
;                  B1. STM Experimenter routines 
;                  B2. Context Effects in Letter Perception Experimenter routines
;                  B3. Verbal Learning Experimenter routines
;                  C. Decay manager routines
;                  D. Routines which prepare the initial net.
;                  E. Routines used to organize experiments and collect results.

(defvar *net-list*) ; this variable is used to hold the beginning net of an experiment
                    ; to save time so that the experiment can be run multiple times 
                    ; without each time preparing the net through the manager processes
                    ; that do so.  For example, most STM experiments assume the 
                    ; existence of an auditory vocabulary and so the pre-experiment
                    ; net must be prepared with just such a vocabulary.

;                  A. Routines that update the clock


(defun subobject-method (object)
  "Subobject-method is a method for counting the number of syllables in an 
object so that the correct amount of time for vocalizing the object can
be allocated.  First, 
the system first checks the subobjects to determine if any of them
is itself a list.  If not, then it assumes that there is just one syllable
in the object.  If so, then it assumes that the number of syllable equals
the number of subobjects.  Any object that is an atom is assumed to have
just one syllable."
   (cond
      ((atom object) 1)
      ((some #'listp (cdr object)) (length (cdr object)))
      ('else 1)))


(defun letters-method (object)
  "This method for counting the number of syllables in an object assumes that 
every letter is spelled aloud.  All letters count
for one except w which counts for two."
   (cond 
      ((null object) 0)
      ((eq #\w object) 2)
      ((characterp object) 1)
      ((atom object) 0)
      ('else (+ (letters-method (car object)) 
                (letters-method (cdr object))))))

(defun character-method (object)
  "Character-method is a method for counting the number of syllables in an 
object so that the correct amount of time for vocalizing the object can
be allocated.  It uses a formula based upon the number
of characters in the object.  The formula is:
          Number of syllables = .5(number of characters) - .5.
An additional provision is made that the number of syllables can never
be less than one.  This formula leads to accurate estimates for
one to three character objects with one syllable, 5 character objects with
two syllables, and 7 character objects with three syllables." 
  (let ((n (- (* (count-characters-in object) .5) .5)))
    (when (< n 1) (setf n 1))
    n))


;                  B1. STM Experimenter routines 

(defun put-new-item-in-sensory-store (modality time-elapsed)
  "This routine puts a new stimulus in the sensory store
if *position* is less than the length of the stimulus list.
If not, then it puts 'signal-to-respond in the sensory store.  It also
sets the new-stimulus flag and attaches
the appropriate value for the 'time attribute for the stimuli in the 
sensory store.  That value is a function of the sensory persistence in
that store, the *stimulus-duration-rate* which is the amount of time that a
stimulus is held in front of the subject by the experimenter, and
the time elapsed since the stimulus would have been first presented to
the subject.  Exits nil if no more stimuli to present."
  (let ((signal (get-stm-object-for (nth *position* *stimulus-list*))))
    (put-value signal modality 'is-a)
    (put-value signal 'new 'group)
    (put-value signal (+ (get modality 'sensory-persistence)
                        (- *stimulus-duration-rate* time-elapsed)) 'time)
    (setf (get modality 'sensory-store)
          (append (get modality 'sensory-store) (list signal))
          *new-stimulus-flag* t
          (get modality 'new-stimulus) t)))

(defun put-probe-in-sensory-store (modality time-elapsed)
  "This routine is just like put-stimulus-in-sensory store except
that the probe has three times the duration in the store of the 
normal stimulus."
  (let ((signal (get-stm-object-for (nth *position* *stimulus-list*))))
    (put-value signal modality 'is-a)
    (put-value signal 'new 'group)
    (put-value signal (+ (get modality 'sensory-persistence)
                        (- (* 4 *stimulus-duration-rate*) time-elapsed)) 'time)
    (setf (get modality 'sensory-store)
          (append (get modality 'sensory-store) (list signal))
          *new-stimulus-flag* t
          (get modality 'new-stimulus) t)))

(defun put-suppression-item-in-visual-store (time-elapsed)
  "This routine is used in a delayed recall experiment to put a secondary
list item in the given sensory store."
  (let* ((signal (cons nil *suppression-stimuli*)))
    (put-value signal 'visual 'is-a)
    (put-value signal 'new 'group)
    (put-value signal (+ (get 'visual 'sensory-persistence)
                         (- (* 4 *stimulus-duration-rate*) time-elapsed)) 'time)
    (setf (get 'visual 'sensory-store)
          (append (get 'visual 'sensory-store) (list signal))
          *new-stimulus-flag* t
          (get 'visual 'new-stimulus) t)))

(defun put-suppression-item-in-auditory-store (position time-elapsed)
  "This routine is used in a delayed recall experiment to put a secondary
list item in the 'auditory sensory store."
  (let* ((item (nth position *suppression-stimuli*))
         (signal (get-stm-object-for item)))
    (put-value signal 'auditory 'is-a)
    (put-value signal 'new 'group)
    (put-value signal (+ (get 'auditory 'sensory-persistence)
                         (- *stimulus-duration-rate* time-elapsed)) 'time)
    (setf (get 'auditory 'sensory-store)
          (append (get 'auditory 'sensory-store) (list signal))
          *new-stimulus-flag* t
          (get 'auditory 'new-stimulus) t)))



(defun visual-span-experimenter ()
  "This routine puts visual words in the icon one word at a time
until the entire list has been presented.  Then it waits an interval
which is specified as the *signal-to-respond-delay* and then begins feeding
the signal to respond at regular intervals."
  (let ((time-elapsed (- *clock* (* *position* *presentation-rate*))))
    (cond ((and (< *position* (length *stimulus-list*)) 
                (> time-elapsed 0))
           (put-new-item-in-sensory-store 'visual time-elapsed)
           (incf *position*))
          ((and (= *position* (length *stimulus-list*))
                (> time-elapsed (- *signal-to-respond-delay* 
                                   *presentation-rate*)))
           (put-control-symbol-in-sensory-store 'signal-to-respond 'visual)
           (incf *position*)))))


(defun serial-probe-experimenter ()
  "This routine is just like visual span experimenter except that 
the probe word stays in vew for twice as long as normal.  I was 
getting some bugs in the serial probe task using visual span 
experimenter when the program went into an endless loop just because
the probe had disappeared by the time that the system perceived that
it was time to respond to it."
  (let ((time-elapsed (- *clock* (* *position* *presentation-rate*))))
    (cond ((and (< *position* (1- (length *stimulus-list*))) 
                (> time-elapsed 0))
           (put-new-item-in-sensory-store 'visual time-elapsed)
           (incf *position*))
          ((and (= *position* (1- (length *stimulus-list*))) 
                (> time-elapsed 0))
           (put-probe-in-sensory-store 'visual time-elapsed)
           (incf *position*))
          ((and (= *position* (length *stimulus-list*))
                (> time-elapsed (- *signal-to-respond-delay* 
                                   *presentation-rate*)))
           (put-control-symbol-in-sensory-store 'signal-to-respond 'visual)
           (incf *position*)))))



(defun auditory-span-experimenter ()
  (let ((time-elapsed (- *clock* (* *position* *presentation-rate*))))
    (cond ((and (< *position* (length *stimulus-list*)) 
                (> time-elapsed 0))
           (put-new-item-in-sensory-store 'auditory time-elapsed)
           (incf *position*))
          ((and (>= *position* (length *stimulus-list*))
                (> time-elapsed (- *signal-to-respond-delay* 
                                   *presentation-rate*)))
           (put-control-symbol-in-sensory-store 'signal-to-respond 'auditory)
           (incf *position*)))))

(defun delayed-recall-experimenter ()
  "Delayed recall experimenter presents the stimuli for a delayed recall 
experiment.  First the group of letters or words is presented at a very rapid
*presentation-rate*.  In the Peterson and Peterson (1959) experiment that 
presentation rate was presentation of letters each 250 msec. Then beginning at
time *suppression-onset* the suppression-stimulus is given.  In the Peterson
and Peterson experiment the suppression stimuli was a three digit number and
it was presented at a *suppression-delay* of 500 msec after presentation of
the last letter of the stimulus at 500 msec.  So the number was presented at 
the 1000 msec mark.  Finally the signal to
respond is presented at a given *signal-to-respond-delay* which is calculated.
In the Peterson and Peterson experiment there were various delays.  The shortest
was 3 seconds.  These three seconds are added to the time when the last of the
letters was presented (which was at the 500 msec mark.  So, the signal to
respond for a *signal-to-respond-delay* of 3000 msec occurs when the clock
reaches  second delay occurred at the 3500 msec. mark."
  (let ((time-elapsed (- *clock* (* *position* *presentation-rate*)))
        (st (length *stimulus-list*))
        (ss (length *suppression-stimuli*)))
    (cond ((and (< *position* st) (> time-elapsed 0))
           (put-new-item-in-sensory-store 'auditory time-elapsed)
           (incf *position*))
          ((< *position* st) nil)
          ((and (< *position* (+ st ss))
                (> time-elapsed (- *suppression-delay* 
                                   *presentation-rate*)))
           (put-suppression-item-in-auditory-store (- *position* st)
                                                   (- time-elapsed
                                                      *suppression-delay*))
           (incf *position*))
          ((< *position* (+ st ss)) nil)
          ((and (= *position* (+ st ss))
               (> time-elapsed (- *signal-to-respond-delay*
                                  (* (1- st) *presentation-rate*))))
           (put-control-symbol-in-sensory-store 'signal-to-respond 'visual)
           (incf *position*)))))


(defun delayed-serial-probe-recall-experimenter ()
  "This experimenter routine presents all but the last item of the *stimulus-
list* visually (the last item is the probe) then it presents an 'end-of-list
control symbol. Then it presents a three digit number as three separate items.
Then after a delay as defined by the *signal-to-respond-delay*
it presents the probe.  There is no need to present the signal-to-respond
because the probe serves as the signal to respond."
  (let ((time-elapsed (- *clock* (* *position* *presentation-rate*)))
        (st (1- (length *stimulus-list*))))
    (cond ((and (< *position* st) (> time-elapsed 0))
           (put-new-item-in-sensory-store 'visual time-elapsed)
           (incf *position*))
          ((< *position* st) nil)
          ((and (= *position* st) (> time-elapsed 0))
           (put-control-symbol-in-sensory-store 'end-of-list 'visual)
           (put-suppression-item-in-visual-store time-elapsed)
           (incf *position*))
          ((= *position* st) nil)
          ((and (= *position* (1+ st))
               (> time-elapsed (- *signal-to-respond-delay*
                                  (* 2 *presentation-rate*))))
           (setf *position* st)
           (put-new-item-in-sensory-store 'visual 0)
           (setf *position* (+ *position* 2)))
          ('else nil))))

 

;;;        B2.  Context Effects in Letter Perception Experimenter


(defun tachistoscopic-experimenter ()
  "This routine puts visual words in the icon that have been covered with
a percentage of a mask until the entire list has been presented.  Words are
presented at intervals specified by the *presentation-rate* and are taken
from the list *stimulus-list*."
   (let ((time-elapsed (- *clock* (* *position* *presentation-rate*))))
       (when (and (< *position* (length *stimulus-list*)) 
                  (> time-elapsed 0))
         (put-new-item-combined-with-mask-in-icon)
         (setf *position* (1+ *position*)))
       (setf time-elapsed (- *clock* (* (1- *position*) *presentation-rate*)))
       (when (and (= *position* (length *stimulus-list*))
                  (> time-elapsed *presentation-rate*))
         (put-control-symbol-in-sensory-store 'signal-to-respond 'visual))))

(defvar *rumelhart-features* '(f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14))


(defun create-rumelhart-letter-object (stimulus &optional order)
   (let ((stimulus-as-list (coerce stimulus 'list))
         (order-of-features order))
      (loop for n from 1 to 14
        unless (member n order-of-features)
        do (setf order-of-features (append order-of-features (list n))))
      (list 
        (cons (list 'is-a 'visual)
          (loop for position in order-of-features
            for attribute = (nth (1- position) *rumelhart-features*)
            for value = (nth (1- position) stimulus-as-list)
            if (eql value #\1)
            collect (list attribute t))))))
      

(defun put-new-item-combined-with-mask-in-icon ()
  "This routine puts a new stimulus in the sensory store
if *position* is less than the length of the stimulus list.
If not, then it puts 'signal-to-respond in the sensory store.  It also
sets the *new-stimulus-flag* at t and attaches
the appropriate time tag the stimuli in the 
sensory store.  That value is a function of the sensory persistence in
that store, the *stimulus-duration-rate* which is the amount of time that a
stimulus is held in front of the subject by the experimenter, and
the time elapsed since the stimulus would have been first presented to
the subject.  Exits nil if no more stimuli to present."
   (let ((signal (change-string-to-object (nth *position* *stimulus-list*) 
                        'visual)))
      (when *print-major-updates*
         (format t "Experimenter says list was: ~a~%" signal))
      (loop for lis on (cdr signal)
        for letter = (car lis)
        for property-string = (get 'letters-transformation-property-list letter)
        for subobject = (create-rumelhart-letter-object property-string)
        do (rplaca lis subobject))
      (loop for subobject in (cdr signal)
        for probability-of-masking-feature in *probabilities-of-masking-features*
        do (loop for masking-feature in *mask*
             for attribute in *rumelhart-features*
             if (and (< (random 1000) 
                        probability-of-masking-feature)
                     (eq masking-feature #\1))
             do (put-value subobject t attribute)))
      (put-value signal 'new 'group)
      (put-value signal (get 'visual 'sensory-persistence) 'time)
      (setf (get 'visual 'sensory-store) (list signal)
            *new-stimulus-flag* t
            (get 'visual 'new-stimulus) t)))




;;;                B3. Verbal Learning Experimenter Routines

(defun put-stimulus-in-sensory-store (modality time-elapsed)
  "This routine is used by a verbal learning experiment to put a stimulus
syllable in a sensory store deleting the previous contents."
  (let ((signal (copy-object (car (nth (/ *position* 2) *sr-list*)))))
    (put-value signal 'stimulus 'group)
    (put-value signal  (+ (get modality 'sensory-persistence)
                          *stimulus-duration-rate*
                          *response-duration-rate*
                          (* -1 time-elapsed))
               'time)
    (setf *trial-clock* (- *clock* time-elapsed))
    (setf (get modality 'sensory-store) (list signal)
          *new-stimulus-flag* t 
          (get modality 'new-stimulus) t)))


(defun put-response-in-sensory-store (modality time-elapsed)
  "This routine is used by a paired-associate experimenter to put a response
syllable at the end of a sensory store and at the same time test the correctness
of the output."
  (let ((signal (cadr (nth (truncate (/ *position* 2)) *sr-list*))))
    (put-value signal 'response 'group)
    (put-value signal  (+ (get modality 'sensory-persistence)
                          *response-duration-rate*
                          (* -1 time-elapsed))
                       'time)
    (setf (get modality 'sensory-store)
           (append (get modality 'sensory-store) (list signal))
          *new-stimulus-flag* t 
          (get modality 'new-stimulus) t)
    (test-correctness-of-paired-associate-output 
     (1+ (truncate (/ *position* 2))))))

(defun put-test-stimulus-in-sensory-store (modality time-elapsed)
  "This routine is used by a verbal learning experiment during the test
stage when just stimuli are presented for the full amount of time."
  (let ((signal (first (nth (truncate (/ *position* 2)) *sr-list*))))
    (put-value signal 'stimulus 'group)
    (put-value signal  (+ *stimulus-duration-rate*
                          *response-duration-rate*
                          (get modality 'sensory-persistence)
                          (* -1 time-elapsed))
               'time)
    (setf (get modality 'sensory-store) (list signal)
          *new-stimulus-flag* t 
          (get modality 'new-stimulus) t)))

(defun put-test-response-in-sensory-store (modality time-elapsed)
  "This routine is used in a replacement experiment during the study 
stage when stimuli then response are presented for the full amount of time."
  (let ((signal (second (nth (truncate (/ *position* 2)) *sr-list*))))
    (put-value signal 'response 'group)
    (put-value signal  (+ *response-duration-rate*
                          (get modality 'sensory-persistence)
                          (* -1 time-elapsed))
               'time)
    (setf (get modality 'sensory-store) (append (get modality 'sensory-store) 
                                          (list signal))
          *new-stimulus-flag* t 
          (get modality 'new-stimulus) t)))


(defun put-stimulus-and-response-in-sensory-store (modality time-elapsed)
  "This routine is used by a verbal learning experiment during the study
stage when stimulus and response are presented together for the full amount
of time."
  (let ((signal (first (nth *position* *sr-list*))))
    (put-value signal 'stimulus 'group)
    (put-value signal  (+ *stimulus-duration-rate*
                          (get modality 'sensory-persistence)
                          (* -1 time-elapsed))
               'time)
    (setf (get modality 'sensory-store) (list signal))
    (setf signal (second (nth *position* *sr-list*)))
    (put-value signal 'response 'group)
    (put-value signal (+ *stimulus-duration-rate*
                          (get modality 'sensory-persistence)
                          (* -1 time-elapsed))
               'time)
    (setf (get modality 'sensory-store)
           (append (get modality 'sensory-store) (list signal))
          *new-stimulus-flag* t 
          (get modality 'new-stimulus) t)))


(defun put-next-syllable-in-sensory-store (modality time-elapsed)
  "This routine is used by a serial-anticipation experiment to put the 
next syllable in a sensory store."
  (let ((syllable (nth (1- *position*) *sr-list*)))
    (put-value syllable 'new 'group)
    (put-value syllable (+ (get modality 'sensory-persistence)
                           *stimulus-duration-rate* (* -1 time-elapsed)) 'time)
    (setf (get modality 'sensory-store)
            (nconc (get modality 'sensory-store) (list syllable))
          *new-stimulus-flag* t 
          (get modality 'new-stimulus) t)))



(defun underwood-experimenter ()
  "This is a paired associate experimenter routine which presents both stimuli
and responses each trial."
  (let ((time-elapsed (- *clock* (time-of-next-update))))
    (cond
     ((< time-elapsed 0))
     ((< *position* (* 2 (length *sr-list*)))
      (if (= (mod *position* 2) 0) 
        (put-stimulus-in-sensory-store 'visual time-elapsed)
        (put-response-in-sensory-store 'visual time-elapsed))
      (incf *position*))
     ((< time-elapsed *inter-trial-interval*))
     ((= *errors* *starting-errors*)
      (incf *consecutive-trials-correct*)
      (cond ((>= *consecutive-trials-correct* *pa-exit-value*)
             (put-control-symbol-in-sensory-store 'signal-to-respond 'visual))
            ('else
             (incf *trial*)
             (zero-out-except-epam-clocks)
             (when *print-experimenter-updates* 
               (format t "               *** BEGINNING TRIAL #~a ***~%" (1+ *trial*)))
             (setf *starting-errors* *errors*
                   *position* 0
                   *sr-list* (reorder-list-randomly *sr-list*)))))
     ('else
      (setf *consecutive-trials-correct* 0)
      (incf *trial*)
      (zero-out-except-epam-clocks)
      (when *print-experimenter-updates* 
         (format t "               *** BEGINNING TRIAL #~a ***~%" (1+ *trial*)))
      (setf *starting-errors* *errors*
            *position* 0
            *sr-list* (reorder-list-randomly *sr-list*))))))



(defun time-of-next-update ()
  "The time of the next update in a paired associate experiment is a function
of a number of factors:  
  1. Stimuli are added when the position is an even number and the
clock is position divided by two times the *presentation-rate*.
  2. Responses are added when the position is an odd number and the
clock is the truncation of position divided by two  times the presentation
rate plus the *stimulus-duration-rate*."
 (if (= (mod *position* 2) 0)
     (* *presentation-rate* (/ *position* 2))
     (+ *stimulus-duration-rate* 
        (* *presentation-rate* (truncate (/ *position* 2))))))

(defun time-of-next-update-in-replacement-experiment ()
  (cond ((= (mod *trial* 2) 0) (cond ((= (mod *position* 2) 0)
                                      (* *presentation-rate* (/ *position* 2)))
                                     ('else
                                      (+ *stimulus-duration-rate* 
                                         (* *presentation-rate* (truncate (/ *position* 2)))))))
        ('else (* (/ *position* 2) *presentation-rate*))))

(defun replacement-experimenter ()
  "This is the experimenter routine for use in a verbal learning
experiment which presents paired words from a list with a study trial
followed by a test trial in which the pair is replaced whenever there is
an error."
  (let ((time-elapsed (- *clock* (time-of-next-update-in-replacement-experiment))))
    (cond 
     ((< time-elapsed 0))
     ((< *position* (* 2 (length *sr-list*)))
      (cond ((= (mod *trial* 2) 0)
             (cond ((= (mod *position* 2) 0)
                    (put-test-stimulus-in-sensory-store 'visual time-elapsed))
                   ('else 
                    (put-test-response-in-sensory-store 'visual time-elapsed)))
             (incf *position*))
            ('else (present-test-stimulus-replace-pair-if-wrong time-elapsed))))
     ((< time-elapsed *inter-trial-interval*))
     ((= (mod *trial* 2) 1)
      (when (= *position* (* 2 (length *sr-list*)))
        (test-correctness-and-replace-pair-if-wrong (truncate (/ *position* 2)))
        (setf (aref *errors-array* (/ (1- *trial*) 2))
              (+ (aref *errors-array* (/ (1- *trial*) 2))
                 (- *errors* *starting-errors*)))
        (when *print-experimenter-updates*
          (format t "End of test trial: ~a~%" (/ (1+ *trial*) 2))))
      (if (= *trial* 19)
        (progn 
          (setf *position* (+ 2 *position*))
          (put-control-symbol-in-sensory-store 'signal-to-respond 'visual))
        (progn 
          (setf *trial* (1+ *trial*))
          (setf *sr-list* (reorder-sr-list *sr-list*))
          (when *print-experimenter-updates*
            (format t "~%~%               *** BEGINNING STUDY TRIAL #~a ***~%"
                    (1+ (truncate (/ *trial* 2))))
            (loop for pair in *sr-list*
                  for stimulus = (first pair)
                  for response = (second pair)
                  collect (list (change-object-to-string stimulus)
                                (change-object-to-string response))
                  into current-list
                  finally (format t "Current list: ~a~%" current-list)))
          (zero-out-except-epam-clocks)
          (setf *starting-errors* *errors*
                *position* 0
                *output* nil
                (get 'visual 'sensory-store) nil))))
     ('else
      (setf *trial* (1+ *trial*))
      (when *print-experimenter-updates*
         (format t "~%~%               *** BEGINNING TEST TRIAL #~a ***~%"
           (1+ (truncate (/ *trial* 2)))))
      (zero-out-except-epam-clocks)
      (setf (get 'visual 'sensory-store) nil)
      (put-control-symbol-in-sensory-store 'beginning-test-trial 'auditory)
      (setf *starting-errors* *errors*
            *position* 0
            *output* nil)))))



(defun non-replacement-experimenter ()
  "This is the experimenter routine for use in a verbal learning
experiment which presents paired words from a list with a study trial
followed by a test trial in which the pair is not replaced whenever there is
an error."
  (let ((time-elapsed (- *clock* (time-of-next-update-in-replacement-experiment))))
    (cond 
     ((< time-elapsed 0))
     ((< *position* (* 2 (length *sr-list*)))
      (cond ((= (mod *trial* 2) 0)
             (cond ((= (mod *position* 2) 0)
                    (put-test-stimulus-in-sensory-store 'visual time-elapsed))
                   ('else 
                    (put-test-response-in-sensory-store 'visual time-elapsed)))
             (incf *position*))
            ('else (present-test-stimulus-non-replacement time-elapsed))))
     ((< time-elapsed *inter-trial-interval*))
     ((= (mod *trial* 2) 1)
      (when (= *position* (* 2 (length *sr-list*)))
        (test-correctness-of-paired-associate-output (truncate (/ *position* 2)))
        (setf (aref *errors-array* (/ (1- *trial*) 2))
              (+ (aref *errors-array* (/ (1- *trial*) 2))
                 (- *errors* *starting-errors*)))
        (when *print-experimenter-updates* 
          (format t "End of test trial: ~a~%" (/ (1+ *trial*) 2))))
      (if (= *trial* 19)
        (progn 
          (setf *position* (+ 2 *position*))
          (put-control-symbol-in-sensory-store 'signal-to-respond 'visual))
        (progn 
          (setf *trial* (1+ *trial*))
          (setf *sr-list* (reorder-sr-list *sr-list*))
          (when *print-experimenter-updates*
            (format t "~%~%               *** BEGINNING STUDY TRIAL #~a ***~%"
                    (1+ (truncate (/ *trial* 2))))
            (loop for pair in *sr-list*
                  for stimulus = (first pair)
                  for response = (second pair)
                  collect (list (change-object-to-string stimulus)
                                (change-object-to-string response))
                  into current-list
                  finally (format t "Current list: ~a~%" current-list)))
          (zero-out-except-epam-clocks)
          (setf *starting-errors* *errors*
                *position* 0
                *output* nil
                (get 'visual 'sensory-store) nil))))
     ('else
      (setf *trial* (1+ *trial*))
      (when *print-experimenter-updates*
         (format t "~%~%               *** BEGINNING TEST TRIAL #~a ***~%"
           (1+ (truncate (/ *trial* 2)))))
      (zero-out-except-epam-clocks)
      (setf (get 'visual 'sensory-store) nil)
      (put-control-symbol-in-sensory-store 'beginning-test-trial 'auditory)
      (setf *starting-errors* *errors*
            *position* 0
            *output* nil)))))

(defun present-study-stimuli (time-elapsed)
  (put-stimulus-and-response-in-sensory-store 'visual time-elapsed)
  (setf *position* (1+ *position*)))

(defun present-test-stimulus-replace-pair-if-wrong (time-elapsed)
  (when (> *position* 0)
    (test-correctness-and-replace-pair-if-wrong (truncate (/ *position* 2))))
  (put-test-stimulus-in-sensory-store 'visual time-elapsed)
  (setf *position* (+ *position* 2)))

(defun present-test-stimulus-non-replacement (time-elapsed)
  (when (> *position* 0)
    (test-correctness-of-paired-associate-output (truncate (/ *position* 2))))
  (put-test-stimulus-in-sensory-store 'visual time-elapsed)
  (setf *position* (+ *position* 2)))

(defun test-correctness-of-paired-associate-output (position)
  "This routine tests the correctness of the output of a paired-associate
experiment."
  (test-correctness (cadr (nth (1- position) *sr-list*))
                    (car (nth (1- position) *sr-list*))))

(defun test-correctness-and-replace-pair-if-wrong (position)
  (when (not (test-correctness (cadr (nth (1- position) *sr-list*))
                               (car (nth (1- position) *sr-list*))))
    (change-this-stimulus-pair)))

(defun test-correctness-of-serial-anticipation-output (position)
  (when (nth (1- position) *sr-list*)
    (unless (test-correctness (nth (1- position) *sr-list*)
                              position)
      (incf (aref *errors-array* (1- position))))))





(defun test-correctness (correct-syllable &optional correct-stimulus)
  "This routine prints what the response was and whether it was correct.  It
also exits t if it was correct and nil if it was incorrect."
  (setf *correct-syllable* correct-syllable)
  (let* ((output (get-print-copy-of *output*))
         (response 
          (change-object-to-string 
           (convert-modality-of output 'auditory 'visual)))
         (correct-response 
          (change-object-to-string correct-syllable))
         (stimulus
          (when correct-stimulus
            (change-object-to-string correct-stimulus)))
         (correct (equal response correct-response)))
    (when *print-experimenter-updates* 
      (format t "~%              *** RESPONSE WAS: ~a ***~%" 
              (if response response "___"))
      (format t "         *** RESPONSE SHOULD HAVE BEEN: ~a ***~%"
              correct-response)
      (format t "~a  *** EXPERIMENTER SAYS RESPONSE WAS ~a ***~%"
              *clock*
              (if (equal response correct-response) "CORRECT" "WRONG")))
    (unless correct (incf *errors*))
    (when *print-experimenter-updates* 
      (format t "                 *** ERRORS SO FAR = ~a ***~%" *errors*))
    (when (> *trial* 200) (break))

    (setf *output* nil)
    (record-info-from-paired-associate-trial correct 
                                             response
                                             (list stimulus correct-response)
                                             correct-response)
    correct))




(defun convert-modality-of (object modality1 modality2)
  (cond ((and (eql modality1 'visual)
              (eql modality2 'auditory))
         (convert-to-auditory object))
        ((and (eql modality1 'auditory)
              (eql modality2 'visual))
         (convert-to-visual object))
        ('else (print "Those modalities are not supported!")
               (break)      
               )))

(defun convert-to-auditory (obj)
  (let ((object (specify obj)))
    (cond ((null object) nil)
          ((atom object) 
           (let ((alist (assoc object *valist*)))
             (cond ((second alist))
                   ((characterp object) (char-upcase object))
                   ('else object))))
          ('else
           (let ((alist (assoc (cdr object) *valist* :test #'equal)))
             (cond ((second alist))
                   ('else 
                    (let ((obj (copy-object object)))
                      (put-value obj 'auditory 'is-a)
                      (loop for remainder on (cdr obj)
                            do (rplaca remainder (convert-to-auditory (car remainder))))
                      obj))))))))

(defun convert-to-visual (obj)
  (let ((object (specify obj)))
    (cond ((null object) nil)
          ((atom object) 
           (let ((alist (assoc object *avlist*)))
             (cond ((second alist))
                   ((characterp object) (char-downcase object))
                   ('else object))))
          ('else
           (let ((alist (assoc (cdr object) *avlist* :test #'equal)))
             (cond ((second alist))
                   ('else 
                    (let ((obj (copy-object object)))
                      (put-value obj 'visual 'is-a)
                      (loop for remainder on (cdr obj)
                            do (rplaca remainder (convert-to-visual (car remainder))))
                      obj))))))))


(defun change-this-stimulus-pair ()
  "This routine is called by a recall trial in a Rock-is-a paired associate if
an error is made for the pair in a particular position.  This routine
replaces the stimulus pair in a position on the *sr-list* by
substituting the first stimulus pair that is on the *extra-pairs*
list.  It also pops the first pair of the *extra-pairs* list and puts the
pair that is replaced at the end of the *extra-pairs* list for use if the
system runs out of new *extra-pairs*"
  (rplaca (nthcdr (1- (truncate (/ *position* 2))) *sr-list*) (pop *extra-pairs*)))
  

(defun reorder-sr-list (sr-list)
  "This routine reorders the sr-list as is commonly done
in some simulations of paired associate learning.  There are some
common schemes of reordering.  In the Gregg et. al. experiment, the 
new order for eight stimuli was used."
  (cond
   ((= (length sr-list) 8)
    (mapcar #'(lambda (x) (nth (1- x) sr-list)) '(3 7 8 6 2 1 4 5)))
   ('else (reorder-list-randomly sr-list))))


(defun change-syllables-to-srlist (syllables modality)
  "This routine takes a list of syllables from the data section
and converts them into pairs of visual objects for a paired-associate experiment.
It also reorders the list randomly before exiting."
   (loop for tail on syllables by #'cddr
         for stimulus = (change-string-to-object (car tail) modality)
         for response = (change-string-to-object (cadr tail) modality)
         collect (list stimulus response) into srlist
         finally (return (reorder-list-randomly srlist))))

(defun change-syllables-to-objects (syllables modality)
  "This routine takes a list of syllables from the data section and converts
them to a list of visual objects for a serial-anticipation
experiment.  It also reorders the list randomly before exiting."
  (let ((reordered-syllables (reorder-list-randomly syllables)))
    (when *print-major-updates*
      (format t "Syllables for this simulation:~%~a~%~%" reordered-syllables))
    (loop for syllable in reordered-syllables
          for object = (change-string-to-object syllable modality)
          collect object into srlist
          finally (return srlist))))


(defun put-control-symbol-in-sensory-store (control-symbol modality)
  (let ((signal (cons nil (list control-symbol))))
    (put-value signal 'control-symbol 'is-a)
    (put-value signal 'control-symbol 'group)
    (put-value signal 10000 'time)
    (setf (get modality 'sensory-store) 
           (append (get modality 'sensory-store) (list signal))
          *new-stimulus-flag* t 
          (get modality 'new-stimulus) t)
    (when *print-time-updates*
      (format t "~a: ~a entered in ~a sensory store.~%" 
              *clock* control-symbol modality))))
              

;;;                   C.  Decay Manager Routines 

;                  D. Routines which prepare the initial net.

(defun get-auditory-three-digit-number ()
  "This routine returns a two chunk list containing the auditory representation
of a three digit number in terms of two words such as six eighty-three."
  (list (change-string-to-object (nth (1+ (random 8)) *spoken-digits*) 'auditory)
        (change-string-to-object (nth (random 10) *spoken-digits*) 'auditory)
        (change-string-to-object (nth (random 10) *spoken-digits*) 'auditory)))

(defun get-visual-three-digit-number ()
  (list (1+ (random 8)) (random 10) (random 10)))
        

(defun get-list-of-color-chunks (n)
  "This routine returns a list of three chunks chosen randomly from *colors*
for use as suppression stimuli."
  (loop for word in (choose-words-from n *colors* 'auditory)
        collect (get-chunk-for word)))
 
(defun create-random-list-of (strings list-length)
  (loop repeat list-length
        with result = nil
        for word = (loop for item = (nth (random (length strings))
                                              strings)
                              until (not (member item result))
                              finally (return item))
        do (setf result (cons word result))
        finally (return result)))
                            
(defun create-random-list-of-digits (n)
  "Given n, this routine reates a random list of digits of length n.  The 
auditory version of the digits is assigned to *stimulus-list* and the list of
digits themselves is assigned to *digits-list*"
  (loop repeat n
        initially (setf *stimulus-list* nil *digits-list* nil)
        for rnd = (random 10)
        for word = (change-string-to-object (nth rnd *spoken-digits*) 'auditory)
        do (setf *stimulus-list* (nconc *stimulus-list* (list word))
                 *digits-list* (nconc *digits-list* (list (nth rnd *digits*))))
        finally 
        (when *print-major-updates* 
          (format t "The Random Digits were: ~a~%" *digits-list*))))

(defun study-auditory-vocabulary ()
  (let ((forgetting-parameter *forgetting-parameter*)
        (context-attributes *context-attributes*))
    (setf *forgetting-parameter* 0) ; This allows the net preparation to run quickly 
                       ; because it discards interfering associations while 
                       ; studying whenever there are multiple associations.
                       ; at the end of this routine *forgetting-parameter* will be restored
                       ; to normal.
    (setf *context-attributes* nil)
    (when *print-major-updates*
      (format t "~a Creating fully familiar chunks for auditory vocabulary.~%" *clock*))
    (create-chunks-for-words (append *net-builders*
                                     *spoken-consonants*
                                     *spoken-vowels*
                                     *spoken-digits*
                                     *spoken-two-digit-numbers*
                                     *colors*
                                     *nicolson-short-words*
                                     *nicolson-medium-words*
                                     *nicolson-long-words*
                                     *similar-words*
                                     *baddeley-lewis-and-vallar-long-words*
                                     *baddeley-lewis-and-vallar-short-words*)
                             'auditory)
    ; (setf (get *net* 'number) (form-node))
    ; (setf (get *net* 'letter) (form-node))
    (when *print-major-updates*
      (format t "~a Associating letters with their category.~%" *clock*))
    (associate-words-with-category (append *spoken-consonants* *spoken-vowels*) 
                                   'letter)
    (when *print-major-updates*
      (format t "~a Associating numbers with their category.~%" *clock*))
    (associate-words-with-category (append *spoken-digits*
                                           *spoken-two-digit-numbers*)
                                   'number)
    (when *print-major-updates*
      (format t "~a Again creating fully familiar chunks for letters.~%" *clock*))
    (create-chunks-for-words (append *spoken-consonants*
                                     *spoken-vowels*
                                     *partial-spoken-consonants*)
                             'letter)
    (when *print-major-updates*
      (format t "~a Again creating fully familiar chunks for numbers.~%" *clock*))
    (create-chunks-for-words (append *spoken-digits*
                                     *spoken-two-digit-numbers*
                                     *partial-spoken-digits*)
                             'number)
    (setf *context-attributes* context-attributes)
    (setf *forgetting-parameter* forgetting-parameter)))


(defun associate-spoken-digits-with-digits ()
  (let ((forgetting-parameter *forgetting-parameter*)
        (context-attributes *context-attributes*))
    (setf *forgetting-parameter* 0) ; This allows the net preparation to run quickly 
                       ; because it discards interfering associations while 
                       ; studying whenever there are multiple associations.
                       ; at the end of this routine *forgetting-parameter* will be restored
                       ; to normal.
    (setf *context-attributes* nil)
    (setf *experimenter* 'null-experimenter)
    (format t "Associating names with numbers takes a minute!~%")
    (loop while
          (member t 
                  (mapcar #'(lambda (x y)
                              (associate-if-necessary
                               'visual-auditory 
                               x
                               (change-string-to-object y 'number)))
                          (append *digits* *two-digit-numbers*)
                          (append *spoken-digits* *spoken-two-digit-numbers*)))
          do (advance-clocks))
    (loop while
          (member t 
                  (mapcar #'(lambda (x y)
                              (associate-if-necessary
                               'value 
                               (change-string-to-object x 'number)
                               y))
                          (append *spoken-digits* *spoken-two-digit-numbers*)
                          (append *digits* *two-digit-numbers*)))
          do (advance-clocks))
    (setf *context-attributes* context-attributes)
    (setf *forgetting-parameter* forgetting-parameter)))


(defun advance-clocks ()
  (cond ((> *epam-clock* *clock*) (setf *clock* *epam-clock*))
        ('else (setf *epam-clock* *clock*))))

(defun associate-consonants-with-their-names ()
  (let ((forgetting-parameter *forgetting-parameter*)
        (context-attributes *context-attributes*))
    (setf *forgetting-parameter* 0) ; This allows the net preparation to run quickly 
                       ; because it discards interfering associations while 
                       ; studying whenever there are multiple associations.
                       ; at the end of this routine *forgetting-parameter* will be restored
                       ; to normal.
    (setf *context-attributes* nil)
    (setf *experimenter* 'null-experimenter)
    (format t "Associating consonants with names takes a minute!~%")
    (loop while
          (member t 
                  (mapcar #'(lambda (x y)
                              (associate-if-necessary
                               'visual-auditory 
                               x
                               (change-string-to-object y 'letter)))
                          *consonants*
                          *spoken-consonants*))
          do (advance-clocks))
    (loop while
          (member t 
                  (mapcar #'(lambda (x y)
                              (associate-if-necessary
                               'auditory-visual 
                               (change-string-to-object x 'letter)
                               y))
                          *spoken-consonants* 
                          *consonants*))
          do (advance-clocks))
    (setf *context-attributes* context-attributes)
    (setf *forgetting-parameter* forgetting-parameter)))


(defun associate-words-with-category (list-of-words category)
  (let ((forgetting-parameter *forgetting-parameter*)
        (context-attributes *context-attributes*))
    (setf *forgetting-parameter* 0) ; This allows the net preparation to run quickly 
                       ; because it discards interfering associations while 
                       ; studying whenever there are multiple associations.
                       ; at the end of this routine *forgetting-parameter* will be restored
                       ; to normal.
    (setf *context-attributes* nil)
    (loop while
          (member t
                  (mapcar #'(lambda (x)
                              (associate
                               'category 
                               (change-string-to-object x 'auditory)
                               category
                               ))
                          list-of-words))
          do (advance-clocks))
    (setf *context-attributes* context-attributes)
    (setf *forgetting-parameter* forgetting-parameter)))


(defun create-chunks-for-words (words is-a)
  (when *print-experimenter-updates* 
    (format t "Creating fully familiar chunks for ~a words~%" is-a))
  (loop while
        (member t (mapcar #'(lambda (x) 
                              (study (change-string-to-object x is-a)))
                          words))
        do (advance-clocks))
  )




(defun practice-with-words (words is-a)
  (when *print-experimenter-updates*
    (format t "Practicing with words."))
  (loop for word in words
        for object = (change-string-to-object word is-a)
        do (sort-to-node object)))

(defun create-srchunks (visual-words auditory-words)
  "This routine creates associations between visual and auditory words.  The
words are not categorized."
  (associate-words visual-words auditory-words 'visual 'auditory 
                   'visual-auditory)
  (associate-words auditory-words visual-words 'auditory 'visual
                   'auditory-visual))


(defun associate-words (words1 words2 is-a1 is-a2 association-attribute)
  "If a word to be associated is a member of a particular category, then
the optional starting point should be given as the top node in the category net.
If a is-a is nil then assume the words are atoms that should not be
changed into objects."
  (let ((forgetting-parameter *forgetting-parameter*)
        (context-attributes *context-attributes*))
    (setf *forgetting-parameter* 0) ; This allows the net preparation to run quickly 
                       ; because it discards interfering associations while 
                       ; studying whenever there are multiple associations.
                       ; at the end of this routine *forgetting-parameter* will be restored
                       ; to normal.
    (setf *context-attributes* nil)
    (setf *experimenter* 'null-experimenter)
    (loop for letter in words1
          for spoken-letter in words2
          for letter-object = (change-string-to-object letter is-a1)
          for spoken-letter-object = (change-string-to-object spoken-letter is-a2)
          if (eql is-a1 'auditory)
          do (setf *avlist* (cons (list (cdr spoken-letter-object) letter-object) *avlist*))
          (setf *valist* (cons (list (cdr letter-object) spoken-letter-object) *valist*))
          else if t
          do (setf *avlist* (cons (list (cdr letter-object) spoken-letter-object) *valist*))
          (setf *valist* (cons (list (cdr spoken-letter-object) letter-object) *avlist*)))
    (loop while (member t (mapcar #'(lambda (x)
                                      (study (change-string-to-object x is-a1)))
                                  words1)))
    (loop while (member t (mapcar #'(lambda (x)
                                      (study (change-string-to-object x is-a2)))
                                  words2)))
    (loop while (member t (mapcar #'(lambda (x y)
                                      (associate-if-necessary
                                       association-attribute
                                       (change-string-to-object x is-a1)
                                       (change-string-to-object y is-a2)))
                                  words1
                                  words2)))
    (setf *context-attributes* context-attributes)
    (setf *forgetting-parameter* forgetting-parameter)))

                    

(defun associate-if-necessary (attribute stimulus correct-response)
  "This routine calls associate if the stimulus does not already 
elicit the chunk for the correct-response. If the two are already associated
then it exits nil."
  (let ((response (extract-associate attribute stimulus)))
    (when (not (eql response
                    (get-chunk-for correct-response)))
      (associate attribute stimulus correct-response)
      t)))


(defun choose-words-from (n stimulus-list modality)
  "This routine chooses n words randomly from a stimulus list which is
assumed to have at least that many words."
  (loop while (< (length words) n)
        with words = nil
        initially 
        (when (> n (length stimulus-list))
          (return (print "ERROR! YOUR N IS TOO HIGH FOR YOUR STIMULUS-LIST!")))
        for word =  (change-string-to-object 
                     (nth (random (length stimulus-list)) stimulus-list)
                     modality)
        when (not (member word words :test #'equal))
          do (push word words)
        finally (return words)))
        
(defun make-suppression-object (suppression-list)
  (loop for item in suppression-list
        collect (change-string-to-object item 'auditory) into result
        finally (return (put-value (cons nil result) 'auditory 'is-a))))


(defun translate-from-spoken-to-written-letters (stimuli)
  "This routine is used by the experimenter to check whether
spoken letters that were input were output correctly in written
form."
  (loop for stimulus in stimuli
        collect (loop for spoken-letter in *spoken-letters*
                      for letter in *letters*
                      if (equal spoken-letter stimulus)
                      return (coerce (list letter) 'string))))

(defun translate-baddeley-lewis-and-vallar-words (stimuli)
  "This routine is used by the experimenter to translate the spoken
words used in baddeley-lewis-and-vallar's experiment 5"
  (loop for stimulus in stimuli
        collect (loop for spoken-word in *baddeley-lewis-and-vallar-combined-set*
                      for written-word in *baddeley-lewis-and-vallar-combined-set-written*
                      if (equal spoken-word stimulus)
                      return written-word)))

(defun translate-palmer-and-ornstein-words (stimuli modality)
  "This routine is used by the experimenter to translate the words
used in baddeley-lewis-and-vallar's experiment 5.  The modality at
input as the modality of the words to be translated from."
  (cond ((eql modality 'auditory)
         (loop for stimulus in stimuli
               collect (loop for spoken-word in *palmer-and-ornstein-auditory-stimuli*
                             for written-word in *palmer-and-ornstein-visual-stimuli*
                             if (equal spoken-word stimulus)
                             return written-word)))
        ('else
         (loop for stimulus in stimuli
               collect (loop for spoken-word in *palmer-and-ornstein-visual-stimuli*
                             for written-word in *palmer-and-ornstein-auditory-stimuli*
                             if (equal spoken-word stimulus)
                             return written-word)))))




(defun combine-strings (strings)
  (loop for item in strings
        if (listp item)
        collect (apply 'concatenate (cons 'string item)) 
        into string-representation
        else collect item into string-representation
        finally (return string-representation)))

;       E. Routines used to organize experiments and collect results.

(defun report-nicolson-and-fawcett-results ()
  (loop for word-length from 1 to (length *stimulus-lists*)
        for correct-trials = (loop for list-length from 2 to 6
                                   for results = (safe-list 
                                                   (aref *errors-array* 
                                                         (1- word-length)
                                                         (1- list-length)))
                                   sum (loop for result in results
                                             count (= result list-length)))
        with number-of-lists = (* *number-of-sessions* 4.0)
        for span = (+ 1.5 (/ correct-trials number-of-lists))
        do (format t "For word-length of ~a, span = ~a~%" word-length span)
        (setf *overall-results* (append *overall-results* (list span)))
        finally (terpri))
  (loop for word-length from 1 to (length *stimulus-lists*)
        do (loop for list-length from 2 to 6
                 for correct-words = (loop for result in (safe-list (aref *errors-array* 
                                                                          (1- word-length)
                                                                          (1- list-length)))
                                           sum result)
                 with number-of-lists = (* *number-of-sessions* 4.0)
                 for recalls = (/ correct-words number-of-lists)
                 do 
                 (format t "For word-length ~a, list-length ~a, recalls = ~a~%"
                         word-length list-length recalls)
                 (setf *overall-results* (append *overall-results* (list recalls)))
                 (setf (aref *list-length-results* list-length)
                       (+ (aref *list-length-results* list-length)
                          recalls))
                 finally (terpri)))
  (loop for word-length from 1 to (length *stimulus-lists*)
        do 
        (loop for serial-position from 1 to 6
              for correct-words = (loop for hits in (safe-list (aref *position-array*
                                                                     (1- word-length)
                                                                     (1- serial-position)))
                                        count hits)
              with number-of-lists = (* *number-of-sessions* 4.0)
              for recalls = (/ correct-words number-of-lists)
              do 
              (format t "For word-length ~a, serial-position ~a, recalls = ~a~%"
                      word-length serial-position recalls)
              (setf *overall-results* (append *overall-results* (list recalls)))
              (setf (aref *serial-position-results* serial-position)
                    (+ (aref *serial-position-results* serial-position)
                       recalls))
              )))


(defun nicolson-and-fawcett-experiment ()
  (loop for stimulus-list in *stimulus-lists*
        for word-length from 1 to (length *stimulus-lists*)
        do (increment-activation)
        (restore-net-from-file)
        (loop repeat 4
              do (loop for n from 2 to 6
                       for words = (create-random-list-of stimulus-list n)
                       for comparison-words = (combine-strings words)
                       with result = nil
                       with hits = nil
                       do 
                       (setf *list-length* n)
                       (setf *stimulus-list* (loop for word in words
                                                   collect 
                                                   (change-string-to-object 
                                                    word
                                                    'auditory)))              
                       (setf result (auditory-recall))
                       (setf hits (loop for word1 in comparison-words
                                        for word2 in result
                                        count (equal word1 word2)))
                       (when (= n 6)
                         (loop for word1 in comparison-words
                               for word2 in result
                               for serial-position = 0 then (1+ serial-position)
                               for correct-word = (equal word1 word2)
                               do (setf (aref *position-array*
                                              (1- word-length)
                                              serial-position)
                                        (cons correct-word 
                                              (aref *position-array*
                                                    (1- word-length)
                                                    serial-position)))))
                       (when *print-major-updates*
                         (format t "For word-length = ~a, list-length = ~a~%"
                                 word-length n)
                         (format t "Words: ~a~%" comparison-words)
                         (format t "Result: ~a~%" result)
                         (format t "There were ~a correct words~%~%" hits))
                       (setf (aref *errors-array* (1- word-length) (1- n))
                             (cons hits (aref *errors-array* (1- word-length)
                                              (1- n))))))))

(defun restore-net-from-file ()
  (change-focus-to nil)
  (setf *search-list* nil)
  (setf *net* (convert-list-to-tree *net-list*)))

(defun baddeley-lewis-and-vallar-exp3-experiment (stimulus-list condition)
  "Changed Feb. 23, 1998, so routine no longer produces cws and rws variables
because did not understand them."
  (cond ((eq condition 'control)
         (setf *concurrent-routine* nil))
        ((eq condition 'suppression) 
         (setf *concurrent-routine* 'concurrent-articulation))
        ('else (format t "Error Condition = ~a not supported!~%" condition)
               (break)))
  (increment-activation)
  (restore-net-from-file)
    (loop repeat 3
          do 
          (loop for n from 2 to 7
                for words = (create-random-list-of stimulus-list n)
                for comparison-words = (translate-from-spoken-to-written-letters words)
                with result = nil
                with hits = nil
                do 
                (setf *list-length* n)
                (setf *stimulus-list* (loop for word in words
                                            collect 
                                            (change-string-to-object word 
                                                                     'auditory)))              
                (setf result (auditory-recall))
                (setf hits (loop for word1 in comparison-words
                                 for word2 in result
                                 count (equal word1 word2)))
                (when *print-major-updates*
                  (format t "Stimuli were: ~a~%" comparison-words)
                  (format t "Results were: ~a~%" result)
                  (format t "There were ~a correct out of ~a~%" hits n))
                (setf (aref *errors-array* (1- n)) 
                      (cons hits (aref *errors-array* (1- n)))))))
  
(defun report-baddeley-lewis-and-vallar-exp3-results ()
  (loop for list-length from 2 to 7
        for results = (safe-list (aref *errors-array* (1- list-length)))
        for sum = (loop for result in results sum result)
        for number-of-items = (* (length results) list-length 1.0)
        for percent-error = (* 100.0 (/ (- number-of-items sum) number-of-items))
        do (format t "List Length = ~a, N = ~a, Percent Error = ~a%~%"
                   list-length number-of-items percent-error)
        (setf *overall-results* (append *overall-results* (list percent-error)))
        finally (terpri)))

(defun report-baddeley-lewis-and-vallar-exp5-results ()
  (loop for list-length from 3 to 5
        do 
        (loop for serial-position from 1 to list-length
              for results = (safe-list(aref *position-array* (1- list-length)
                                            (1- serial-position)))
              for correct-words = (loop for hits in results
                                        count hits)
              for number-of-lists = (* (length results) 1.0)
              for errors = (* 100.0 (/ (- number-of-lists correct-words)
                                       number-of-lists))
              do 
              (format t "For list-length ~a, serial-position ~a, errors = ~a%~%"
                      list-length serial-position errors)
              (setf *overall-results* (append *overall-results* (list errors)))
              finally (terpri))
        finally (terpri)))

(defun baddeley-lewis-and-vallar-exp5-experiment (stimulus-list condition)
  (cond ((eq condition 'control)
         (setf *concurrent-routine* nil))
        ((eq condition 'suppression) 
         (setf *concurrent-routine* 'concurrent-articulation))
        ('else (format t "Error Condition = ~a not supported!~%" condition)
               (break)))
  (increment-activation)
  (restore-net-from-file)
  (loop repeat 5
        do 
        (loop for n from 3 to 5
              for words = (create-random-list-of stimulus-list n)
              for comparison-words = (translate-baddeley-lewis-and-vallar-words words) 
              with result = nil
              with hits = nil
              do 
              (setf *list-length* n)
              (setf *stimulus-list* (loop for word in words
                                          collect 
                                          (change-string-to-object word 
                                                                   'auditory)))              
              (setf result (auditory-recall))
              (setf hits (loop for word1 in comparison-words
                               for word2 in result
                               count (equal word1 word2)))
              (when *print-major-updates*
                (format t "Stimuli were: ~a~%" comparison-words)
                (format t "Results were: ~a~%" result)
                (format t "There were ~a correct out of ~a~%" hits n))
              (loop for word1 in comparison-words
                    for word2 in result
                    for serial-position = 0 then (1+ serial-position)
                    for correct-word = (equal word1 word2)
                    do (setf (aref *position-array* (1- n) serial-position)
                             (cons correct-word 
                                   (aref *position-array*
                                         (1- n)
                                         serial-position))))))
  )

(defun palmer-and-ornstein-exp1-experiment (n)
  (loop for strategy in (cond ((= n 0) (list 'cumulative *paired-strategy*))
                              ((= n 1) (list *paired-strategy* 'cumulative))
                              ((= n 2) `(cumulative cumulative))
                              ((= n 3) (list *paired-strategy* *paired-strategy*)))
        for array-position in (cond ((= n 0) '(0 1))
                                    ((= n 1) '(1 0))
                                    ((= n 2) '(0 0))
                                    ((= n 3) '(1 1)))
        do 
        (loop for probe-position in (reorder-list-randomly '(0 1 2 3 4 5 6 7))
              for stimuli in *stimulus-lists*
              for stimulus-list = (reorder-list-randomly stimuli)
              for probe = (nth probe-position stimulus-list)
              for answer = (nth (1+ probe-position) stimulus-list)
              for comparison-answer = (car (translate-palmer-and-ornstein-words (list answer) 
                                                                                'visual))
              with result = nil
              do (when *print-major-updates*
                   (format t "Stimulus list = ~a~%" stimulus-list)
                   (format t "Probe = ~a~%" probe)
                   (format t "Answer = ~a~%" comparison-answer))
              (when (not comparison-answer)
                (setf *probe-position* probe-position)
                (setf *stimuli* stimuli)
                (setf *probe* probe)
                (setf *answer* answer)
                (setf *comparison-answer* comparison-answer)
                (format t "ERROR:  No comparison answer!")
                (break))
              (setf *stimulus-list* (loop for word in (append stimulus-list
                                                              (list probe))
                                          collect 
                                          (change-string-to-object word 
                                                                   'visual)))      
              (setf result (serial-probe-recall strategy))
              (when *print-major-updates*
                (format t "Result should be: ~a~%" comparison-answer)
                (format t "      Result was: ~a~%" result)
                (cond ((equal comparison-answer result)
                       (format t "Result was correct!~%"))
                      ('else (format t "Result was incorrect!~%"))))
              (setf (aref *errors-array* array-position probe-position)
                    (cons (equal comparison-answer result)
                          (aref *errors-array* 
                                array-position probe-position))))))

(defun palmer-and-ornstein-exp2-experiment (n)
  (loop for strategy in (cond ((= n 0) (list 'cumulative *paired-strategy*))
                              ((= n 1) (list *paired-strategy* 'cumulative))
                              ((= n 2) (list 'cumulative 'cumulative))
                              ((= n 3) (list *paired-strategy* *paired-strategy*)))
        for array-position in (cond ((= n 0) '(0 1))
                                    ((= n 1) '(1 0))
                                    ((= n 2) '(0 0))
                                    ((= n 3) '(1 1)))
        do 
        (loop for probe-position in (reorder-list-randomly '(0 1 2 3 4 5 6 7))
              for stimuli in *stimulus-lists*
              for stimulus-list = (reorder-list-randomly stimuli)
              for probe = (nth probe-position stimulus-list)
              for answer = (nth (1+ probe-position) stimulus-list)
              for comparison-answer = (car (translate-palmer-and-ornstein-words (list answer) 
                                                                                'visual))
              with result = nil
              do (when *print-major-updates*
                   (format t "Stimulus list = ~a~%" stimulus-list)
                   (format t "Probe = ~a~%" probe)
                   (format t "Answer = ~a~%" comparison-answer))
              (setf *stimulus-list* (loop for word in (append stimulus-list
                                                              (list probe))
                                          collect 
                                          (change-string-to-object word 
                                                                   'visual)))
              (setf *suppression-stimuli* (get-visual-three-digit-number))
              (setf result (serial-probe-recall strategy 'delayed))
              (when *print-major-updates*
                (format t "Result should be: ~a~%" comparison-answer)
                (format t "      Result was: ~a~%" result)
                (cond ((equal comparison-answer result)
                       (format t "Result was correct!~%"))
                      ('else (format t "Result was incorrect!~%"))))
              (setf (aref *errors-array* array-position probe-position)
                    (cons (equal comparison-answer result)
                          (aref *errors-array* 
                                array-position probe-position))))))



(defun report-palmer-and-ornstein-results ()
   (loop for array-position in '(0 1)
     do (cond ((= array-position 0)
               (format t "Results for cumulative-rehearsal strategy:~%"))
              ('else
               (format t "Results for paired-rehearsal strategy:~%")))
     (loop for serial-position from 2 to 9
       for results = (safe-list
                       (aref *errors-array* 
                             array-position (- serial-position 2)))
       for hits = (loop for result in results count result)
       for number-of-lists = (* (length results) 1.0)
       for proportion-correct = (/ hits number-of-lists)
       do 
       (format t 
         "Serial Position = ~a, n = ~a, Proportion Correct = ~a~%"
         serial-position number-of-lists proportion-correct)
       (setf *overall-results*
             (append *overall-results* (list proportion-correct)))
       finally (terpri))))
  




(defun peterson-and-peterson-experiment ()
  (loop for interval in (reorder-list-randomly '(3 6 9 12 15 18))
        for delay = (* interval 1000)
        for item = (create-random-list-of *spoken-consonants* *list-length*)
        for comparison-item = (combine-strings item)
        with result = nil
        with time-taken = nil
        do (setf *signal-to-respond-delay* delay)
        (setf *stimulus-list*  (loop for letter in item
                                     collect (change-string-to-object 
                                              letter
                                              'auditory)))
        (setf *suppression-stimuli* (get-auditory-three-digit-number))
        (when *print-major-updates*
          (format t "~%Recall of list ~a after delay ~a:~%" item interval))
        (setf result (delayed-recall))
        (setf time-taken (- *clock* 
                            *signal-to-respond-delay*
                            (* 2 *presentation-rate*)))
        (when *print-major-updates* 
          (format t "Item: ~a~%" comparison-item)
          (format t "Result: ~a~%" result)
          (format t "Time taken to respond was ~a msec~%" time-taken)
          (if (equal comparison-item result)
            (format t "Recall was correct in ~a msec!~%" time-taken)
            (format t "Recall was incorrect!~%")))
        (cond ((and (equal comparison-item result)
                   (<= time-taken *time-allowed-for-recall*))
               (when *print-major-updates*
                 (format t "Recall was correct in ~a msec!~%" time-taken))
               (setf (aref *errors-array* (1- interval))
                     (cons time-taken (aref *errors-array* (1- interval)))))
              ('else (setf (aref *errors-array* (1- interval))
                           (cons nil (aref *errors-array* (1- interval))))))))


(defun peterson-and-peterson-practice-trials ()
  "The practice trials are just like the real trials except that the results
are not put in the *errors-array* and therefore not tabulated as part of the
experiment."
  (loop repeat 2
        for interval in (reorder-list-randomly '(3 6 9 12 15 18))
        for delay = (* interval 1000)
        for item = (create-random-list-of *spoken-consonants* *list-length*)
        for comparison-item = (combine-strings item)
        with result = nil
        with time-taken = nil
        do (setf *signal-to-respond-delay* delay)
        (setf *stimulus-list*  (loop for letter in item
                                     collect (change-string-to-object 
                                              letter
                                              'auditory)))
        (setf *suppression-stimuli* (get-auditory-three-digit-number))
        (when *print-major-updates*
          (format t "~%Recall of practice list ~a after delay ~a:~%" 
                  item interval))
        (setf result (delayed-recall))
        (setf time-taken (- *clock* 
                            *signal-to-respond-delay*
                            (* 2 *presentation-rate*)))
        (when *print-major-updates* 
          (format t "Item: ~a~%" comparison-item)
          (format t "Result: ~a~%" result)
          (format t "Time taken to respond was ~a msec~%" time-taken)
          (if (equal comparison-item result)
            (format t "Recall was correct in ~a msec!~%" time-taken)
            (format t "Recall was incorrect!~%")))))


(defun posner-and-konick-experiment (stimulus-list)
  "The main difference between scoring for this experiment and scoring for
the Peterson and Peterson experiment is that the results are reported for 
items not lists as whole."
  (loop for interval in (reorder-list-randomly '(0 5 10 20))
        for delay = (* interval 1000)
        for item = (create-random-list-of stimulus-list *list-length*)
        for comparison-item = (combine-strings item)
        with result = nil
        with time-taken = nil
        with score = nil
        do (setf *signal-to-respond-delay* delay)
        (setf *stimulus-list*  (loop for letter in item
                                     collect (change-string-to-object 
                                              letter
                                              'auditory)))
        (setf *suppression-stimuli* (get-auditory-three-digit-number))
        (when *print-major-updates*
          (format t "~%Recall of list ~a after delay ~a:~%" item interval))
        (setf result (delayed-recall))
        (setf score (loop for item-element in comparison-item
                          for result-list = result then (cdr result-list)
                          for result-element = (car result-list)
                          collect (equal item-element result-element)))
        (setf time-taken (- *clock* 
                            *signal-to-respond-delay*
                            (* 2 *presentation-rate*)))
        (when *print-major-updates* 
          (format t "Item: ~a~%" comparison-item)
          (format t "Result: ~a~%" result)
          (format t "Time taken to respond was ~a msec~%" time-taken)
          (format t "Score was: ~a~%" score))
        (loop for score-element in score
              do (setf (aref *errors-array* interval)
              (cons score-element (aref *errors-array* interval))))))



(defun wickens-born-and-allen-experiment ()
  (loop for condition from 1 to 10
        do (loop initially (increment-activation)
                 with time-taken = nil
                 for item in (get-wickens-list-for condition)
                 for comparison-item = (combine-strings item)
                 for n = 1 then (1+ n)
                 with result = nil
                 do 
                 (setf *stimulus-list* 
                       (loop for letter in item
                             collect (change-string-to-object 
                                      letter
                                      'auditory)))
                 (setf *suppression-stimuli* (get-list-of-color-chunks 3))
                 (when *print-major-updates*
                   (format t "~%Recall of list ~a in position ~a:~%" item n))
                 (setf result (delayed-recall))
                 (setf time-taken (- *clock* 
                                     *signal-to-respond-delay*
                                     (* 2 *presentation-rate*)))
                 (when *print-major-updates* 
                   (format t "Item: ~a~%" item)
                   (format t "Result: ~a~%" result))
                 (cond ((and (equal comparison-item result)
                             (<= time-taken *time-allowed-for-recall*))
                        (when *print-major-updates*
                          (format t "Recall was correct in ~a msec!~%"
                                  time-taken))
                        (setf (aref *errors-array* (1- n) (1- condition))
                              (cons time-taken 
                                    (aref *errors-array* 
                                          (1- n) (1- condition)))))
                       ('else (setf (aref *errors-array* (1- n) (1- condition))
                                    (cons nil (aref *errors-array*
                                                    (1- n) (1- condition)))))))))


(defun get-wickens-list-for (condition)
  "Here are the different conditions:
1. 1 from test-numbers 
2. 1 from test-consonants. 
3. c1a or c1b, 1 randomly from test-numbers
4. n1, 1 randomly from test-consonants
5. c1a or c1b, 1 randomly from test-consonants, c2, 1 randomly from test-numbers
6. n1, 1 randomly from test-numbers, n2, 1 randomly from test-consonants
7. c1, 1 randomly from test-consonants, c2, 1
   1 randomly from remaining test-consonants, c3, 1 randomly from test-numbers
8. n1, 1 randomly from test-numbers, n2, 1 randomly from test-numbers, n3
   1 randomly from test-consonants.
9. c1 1 randomly from test consonants, c2, 1 randomly from remaining test-cons
   c3, 1 randomly from remaining test-cons.
10. n1 1 randomly from test-numbers, n2, 1 randomly from remaining test-numbers
    n3, 1 randomly from remaining test-numbers."
  (case condition
    (1 (list (nth (random 4) *wickens-ntest*)))
    (2 (list (nth (random 4) *wickens-ctest*)))
    (3 (append (if (= (random 1) 0) *wickens-c1a* *wickens-c1b*)
               (list (nth (random 4) *wickens-ntest*))))
    (4 (append *wickens-n1* 
               (list (nth (random 4) *wickens-ctest*))))
    (5 (append (if (= (random 1) 0) *wickens-c1a* *wickens-c1b*)
               (list (nth (random 4) *wickens-ctest*))
               *wickens-c2*
               (list (nth (random 4) *wickens-ntest*))))
    (6 (append *wickens-n1*
               (list (nth (random 4) *wickens-ntest*))
               *wickens-n2*
               (list (nth (random 4) *wickens-ctest*))))
    (7 (let ((test-letters (reorder-list-randomly *wickens-ctest*)))
         (append (if (= (random 1) 0) *wickens-c1a* *wickens-c1b*)
                 (list (pop test-letters))
                 *wickens-c2*
                 (list (pop test-letters))
                 *wickens-c3*
                 (list (nth (random 4) *wickens-ntest*)))))
    (8 (let ((test-numbers (reorder-list-randomly *wickens-ntest*)))
         (append *wickens-n1*
                 (list (pop test-numbers))
                 *wickens-n2*
                 (list (pop test-numbers))
                 *wickens-n3*
                 (list (nth (random 4) *wickens-ctest*)))))
    (9 (let ((test-letters (reorder-list-randomly *wickens-ctest*)))
         (append (if (= (random 1) 0) *wickens-c1a* *wickens-c1b*)
                 (list (pop test-letters))
                 *wickens-c2*
                 (list (pop test-letters))
                 *wickens-c3*
                 (list (pop test-letters)))))
    (10 (let ((test-numbers (reorder-list-randomly *wickens-ntest*)))
         (append *wickens-n1*
                 (list (pop test-numbers))
                 *wickens-n2*
                 (list (pop test-numbers))
                 *wickens-n3*
                 (list (pop test-numbers)))))))



(defun keppel-and-underwood-experiment ()
  (loop for intervals in '((3 9 18) (3 18 9) (9 3 18) 
                           (9 18 3) (18 3 9) (18 9 3))
        do 
        (loop for lis in *stimulus-lists*
              do (increment-activation)
              (loop for interval in intervals
                    for item in lis
                    for comparison-item = (combine-strings item)
                    for n = 1 then (1+ n)
                    for delay = (* interval 1000)
                    with result = nil
                    do (setf *stimulus-list* 
                             (loop for letter in item
                                   collect (change-string-to-object 
                                            letter
                                            'auditory)))
                    (setf *signal-to-respond-delay* delay)
                    (when *print-major-updates*
                      (format 
                       t 
                       "~%Recall of list ~a in position: ~a after delay ~a:~%"
                       item n interval))
                    (setf *suppression-stimuli* 
                          (get-auditory-three-digit-number))
                    (setf result (delayed-recall))
                    (when *print-major-updates* 
                      (format t "Item: ~a~%" item)
                      (format t "Result: ~a~%" result)
                      (if (equal comparison-item result)
                        (format t "Recall was correct!~%")
                        (format t "Recall was incorrect!~%")))
                    (setf (aref *errors-array* (1- n) (1- interval))
                          (cons (equal comparison-item result)
                                (aref *errors-array* (1- n) (1- interval))))))))

(defun auditory-turvey-brick-and-osborn-experiment (initial-delay)
  (loop for interval in (list initial-delay initial-delay initial-delay 
                              initial-delay 15)
        for item in (reorder-list-randomly *stimulus-lists*)
        for comparison-item = (combine-strings item)
        for n = 1 then (1+ n)
        for delay = (* interval 1000)
        with result = nil
        with time-taken
        with correct
        do (setf *stimulus-list* 
                 (loop for letter in item
                       collect (change-string-to-object letter 'auditory)))
        (setf *signal-to-respond-delay* delay)
        (when *print-major-updates*
          (format t "~%Recall of list ~a in position: ~a after delay ~a:~%"
                  item n interval))
        (setf *suppression-stimuli* (get-auditory-three-digit-number))
        (setf result (delayed-recall))
        (setf time-taken (- *clock* 
                            *signal-to-respond-delay*
                            (* 2 *presentation-rate*)))
        (when *print-major-updates* 
          (format t "Item: ~a~%" item)
          (format t "Result: ~a~%" result)
          (if (equal comparison-item result)
            (format t "Recall was correct in ~a msec.!~%" time-taken)
            (format t "Recall was incorrect!~%")))
        (setf correct (and (<= time-taken *time-allowed-for-recall*)
                           (equal comparison-item result)))
        (setf (aref *errors-array* (1- n) (1- initial-delay))
              (cons correct (aref *errors-array* (1- n) (1- interval))))))


       
; These routines are used to collect totals in quick versions of paired-associate
; experiments

(defun put-response-on-record (response record stim correct?)
  "The response-record is an association list which indexes all of the responses
made to a particular stimulus.  If the response was correct, its value is t.  
Otherwise, it is the actual response made."
  (let ((current-values (second (assoc stim record :test #'equal)))
        (new-value (or correct? response)))
    (cond ((null current-values) (cons (list stim (list new-value)) record))
          ('else 
           (nconc current-values (list new-value))
           record))))

(defun display-response-record (record)
  "Note this response display won't work if a possible response is t"
  (when *print-experimenter-updates*
    (format t "~%Responses to stimuli were the following: ~%"))
  (loop for alist in record 
        for values = (second alist)
        do (when *print-experimenter-updates* (format t "~a:" (first alist)))
        (loop for value in values
              for n from 0
              do (when *print-experimenter-updates* (format t " ~a" value))
              if (not (eql value t))
              do (incf (aref *errors-array* n))
              finally (when *print-experimenter-updates* (format t "~%")))
        (loop for value in (pre-criterion values)
              for n from 0
              if (eql value t)
              do (incf (aref *pre-criterion-success-array* n))
              else if t 
              do (incf (aref *pre-criterion-failure-array* n)))))


(defun make-auditory-vc-parts (syllable-list)
  "This routine given a list of nonsense-syllables in the form of strings makes 
two lists *auditory-cv-parts* and *auditory-cv-counterparts* that are to be 
when chunking the graphonemes."
  (setf *auditory-cv-parts* nil)
  (setf *auditory-cv-counterparts* nil)
  (loop for syllable in syllable-list
        for visual-graphoneme = (cdr (coerce syllable 'list))
        for auditory-graphoneme = (mapcar 'char-upcase visual-graphoneme)
        for visual-string = (coerce visual-graphoneme 'string)
        for auditory-string = (coerce auditory-graphoneme 'string)
        do 
        (setf *auditory-cv-parts* (cons auditory-string *auditory-cv-parts*))
        (setf *auditory-cv-counterparts* (cons visual-string *auditory-cv-counterparts*))))
    
(defun make-auditory-cv-parts (syllable-list)
  "This routine given a list of nonsense-syllables in the form of strings makes 
two lists *auditory-cv-parts* and *auditory-cv-counterparts* that are to be 
when chunking the graphonemes."
  (setf *auditory-cv-parts* nil)
  (setf *auditory-cv-counterparts* nil)
  (loop for syllable in syllable-list
        for visual-graphoneme = (reverse (cdr (reverse (coerce syllable 'list))))
        for auditory-graphoneme = (mapcar 'char-upcase visual-graphoneme)
        for visual-string = (coerce visual-graphoneme 'string)
        for auditory-string = (coerce auditory-graphoneme 'string)
        do 
        (setf *auditory-cv-parts* (cons auditory-string *auditory-cv-parts*))
        (setf *auditory-cv-counterparts* (cons visual-string *auditory-cv-counterparts*))))

(defun make-auditory-one-letter-one-sound-parts ()
  "This routine given a list of nonsense-syllables in the form of strings makes 
two lists *auditory-cv-parts* and *auditory-cv-counterparts* that are to be 
when chunking the graphonemes."
  (setf *auditory-cv-parts* nil)
  (setf *auditory-cv-counterparts* nil))


    
; vrblsims644.lsp
;  Deleted a bunch of routines that don't seem to be being used


; vrblsims642.lsp
;   I'm having trouble fitting the Bugelski data.  I suspect that it is partly because
; the system stops learning and just waits once it gets one correct response even if 
; the stimulus is not at all familiarized.  The purpose of this version is to change
; the paired associate strategy so it doesn't just wait.
;   Added *response-latency* into test-strategy so that record-info-from-paired-associate-trial 
; would work correctly.
;   Changed respond-to-stimulus-in-group so that it responds to the stimulus object not to the 
; stimulus chunk.  This should change all of the simulation results so far.
;   Errors were occurring because associate-strategy is not pausing its rehearsal when
; there is an interrupt.  So I changed it so that rehearsals are interrupted if there is a 
; *new-stimulus-flag*

; vrblsims641.lsp
;   Changed association-strategy so that it checks for new stimulus flag after every rehearsal.
;   in order to avoid occasions when the subject responds late even though he knows the 
;   correct response.



; verblsims640.lsp

; This routine was borrowed from epam5a-current.  The following changes were
; made:
;    1. Replace all specify with specify
;    2. Replace all access with access
;    3. defvar the variables immediately following
;    4. rename all timer to advance-the-timer


;;      These Routines are Most often Used in Verbal Learning Simulations
;;    This file is divided into Subject Routines and Experimenter Routines
;;    At the end are routines specifically used in Context Effects Simulation


;; History 
;;    1. on Feb 22, 1998 redid get-beginning-by-subtracting and get-end-by-subtracting
;;  

(defvar *total-trials*)


;;                        Subject Routines



(defun print-rsf ()
  (let ((image (get-image-of *rsf*)))
    (format t "RSF: ")
    (loop for item in (cdr image)
          do (format t "~a " (change-object-to-string (get-print-copy-of item))))
    (terpri)))
    


(defun paired-associate-strategy (attribute &optional is-a-of-recoding)
  "This routine is a subject's strategy in a a paired associate experiment
where the words or syllables in a group have at most three chunks in them.
The reorganizer can be used to regroup auditory chunks after their visual
chunks have been accessed during the access procedure"
  (let ((reorganizer (cond ((eql is-a-of-recoding 'cv) 'convert-initial-cv-to-chunk)
                           ((eql is-a-of-recoding 'vc) 'convert-terminal-vc-to-chunk)
                           ('else nil))))
    (loop until (wait-and-check-for-signal-to-respond)
          when (paired-associate-test-strategy attribute reorganizer)
          do (associate-strategy attribute 'beginning 'group-2 reorganizer))))


(defun associate-strategy (attribute &optional group1 group2 reorganizer)
   "This strategy rehearses the stimulus response pair and 
 then calls the associate routine to associate the stimulus with the response.
 It does one step of learning and loops to repeat itself if
 either there is no new stimulus or the strategy is to ignore new stimuli.
 It also exits if the correct response is familiar and is associated with
 the stimulus.  It also exits if the stimulus group or response group have
 disappeared from the articulatory loop."
   ; (setf *attribute* attribute)
   ; (setf *group1* group1)
   ; (setf *group2* group2)
   ; (setf *reorganizer* reorganizer)
   ; (break)
   (when (does-this-group-exist? 'response 'visual 'sensory-store)
      (let (stimulus-object correct-response-object subjects-response correct n)
         (when (not group1) (setf group1 'beginning))
         (when (not group2) (setf group2 'end))
         (access-syllable 'stimulus 'visual 'auditory group1 reorganizer nil 
           'dont-mark-stimulus-as-old *stimulus-is-a*)
         (access-syllable 'response 'visual 'auditory group2 reorganizer nil 
           'dont-mark-stimulus-as-old *response-is-a*)
         (setf stimulus-object (get-object-for group1 'auditory))
         (setf correct-response-object (get-object-for group2 'auditory))
         (setf subjects-response (find-response-given-stimulus stimulus-object attribute))
         (setf correct (perfect-subobject-match? subjects-response 
                        correct-response-object))
         ;  the following commands can be useful for debugging
         ;(setf *stimulus-object* stimulus-object)
         ;(setf *correct-response-object* correct-response-object)
         ;(setf *subjects-response* subjects-response)
         ;(setf *correct* correct)
         ; (break)
         (setf n 0)
         (loop 
           if (null stimulus-object) 
           return t
           if (memory-store-search (get 'auditory 'sensory-store) 'beginning-test-trial)
           return t
           if (null correct-response-object) 
           return t
           if (and (> n 0) correct) ; This prevents more than 1 overlearning attempt
           return t
           do 
           (incf n)
           (change-focus-to nil) ; this enables a subject to learn in one long trial
           (study-paired-association attribute stimulus-object correct-response-object 
            nil nil nil correct)
           (when (and *new-stimulus-flag* (or correct (not *ignore-new-stimulus*))) (return t))
           (rehearse-section group1 'auditory nil nil nil *stimulus-is-a*)
           (when (and *new-stimulus-flag* (or correct (not *ignore-new-stimulus*))) (return t))
           (rehearse-section group2 'auditory nil nil nil *response-is-a*)
           (when (and *new-stimulus-flag* (or correct (not *ignore-new-stimulus*))) (return t))
           (setf stimulus-object (get-object-for group1 'auditory))
           (when (null stimulus-object) (return t))
           (setf correct-response-object (get-object-for group2 'auditory))
           (when (null correct-response-object) (return t))
           (when (and *new-stimulus-flag* (or correct (not *ignore-new-stimulus*))) (return t))
           (setf subjects-response (find-response-given-stimulus stimulus-object attribute))
           (setf correct (perfect-subobject-match? subjects-response 
                          correct-response-object))
           (when (and *new-stimulus-flag* (or correct (not *ignore-new-stimulus*))) 
              (return t))))))
   




(defun paired-associate-test-strategy (attribute &optional reorganizer)
   "This test strategy is used in a paired-associate experiment.  It inputs the 
 stimulus, outputs tentative response, and then waits until a new-stimulus-flag
 appears. It is used in experiments where first the stimulus is presented by itself
 and then the stimulus and response are presented together.  It exits nil if the 
 signal-to-respond has been presented, otherwise it exits t."
   (access-syllable 'stimulus 'visual 'auditory 'beginning reorganizer nil 
     'dont-mark-stimulus-as-old *stimulus-is-a*)
   (respond-to-stimulus-in-group attribute 'beginning *response-is-a*)
   (setf *response-latency* (- *clock* *trial-clock*))
   (when *print-experimenter-updates*
      (format t "Subject just responded at Clock = ~a for a response latency of ~a~%"
        *clock* *response-latency*))
   (or (does-this-group-exist? 'response 'visual 'sensory-store)
       (wait-for-item))
   (cond 
         ((eq (cadar (get 'visual 'sensory-store)) 'signal-to-respond) nil)
         ('else t)))

(defun respond-to-stimulus-in-group (attribute group is-a)
  "This routine causes a subject to respond to a stimulus that is defined by 
a rehearsal group in the auditory imagery-store."
  (let* ((object (get-object-for group 'auditory))
         (subjects-response (copy-object 
                              (find-response-given-stimulus object attribute))))
     (when (and is-a 
                (not (atom subjects-response)))
        (put-value subjects-response is-a 'is-a))
     (setf *output* subjects-response)
     (when *print-strategy-updates* 
        (format t "Responding: ~a~%" (change-object-to-string 
                                       (get-print-copy-of subjects-response))))))







(defun associate-visual-and-auditory-letters ()
  "This routine makes familiar chunks for the names of all the letters and
associates the visual letters with their names and associates the names with 
the visual letters."
  (setf *experimenter* 'null-experimenter)
  (loop for letter in *letters*
        for spoken-letter in *spoken-letters*
        for spoken-letter-object = (change-string-to-object spoken-letter 'auditory)
        do
        (setf *avlist* (cons (list (cdr spoken-letter-object) letter) *avlist*))
        (setf *valist* (cons (list letter spoken-letter-object) *valist*)))
  (create-chunks-for-words *spoken-letters* 'auditory)
  (loop while
         (member t 
                 (mapcar #'(lambda (x y)
                             (associate 'visual-auditory 
                                        x 
                                        (change-string-to-object y 'auditory)))
                         *letters*
                         *spoken-letters*)))
  (loop while
         (member t 
                 (mapcar #'(lambda (x y)
                             (associate 'auditory-visual 
                                        (change-string-to-object x 'auditory)
                                        y))
                          *spoken-letters*
                          *letters*))))

(defun associate-word-parts (auditory-list visual-list)
  (create-chunks-for-words auditory-list 'auditory)
  (create-chunks-for-words visual-list 'visual)
  (loop while
        (member t 
                (mapcar #'(lambda (x y)
                            (associate 'visual-auditory 
                                       (change-string-to-object x 'visual)
                                       (change-string-to-object y 'auditory)))
                        visual-list
                        auditory-list)))
  (loop while
        (member t 
                (mapcar #'(lambda (x y)
                            (associate 'auditory-visual 
                                       (change-string-to-object x 'auditory)
                                       (change-string-to-object y 'visual)))
                        auditory-list
                        visual-list)))
  (loop while
        (member t 
                (mapcar #'(lambda (x y)
                            (associate 'visual-auditory 
                                       x
                                       y))
                        *letters*
                        (mapcar 'char-upcase *letters*))))
  (loop while
        (member t 
                (mapcar #'(lambda (x y)
                            (associate 'auditory-visual 
                                       x
                                       y))
                        (mapcar 'char-upcase *letters*)
                        *letters*)))
  )
  

  




(defun make-sal-probabilistic ()
  "Unlike most of the list-making routines, this one returns a list in pairs form with the first
item being an epam-object.  This is to allow the system to total the results correctly for each
stimulus syllable.  It is not strictly necessary."
  (let ((response-lists (list (reorder-list-randomly '(1  1  1  1  1  1  1  1  1  1))
                              (reorder-list-randomly '(1  1  1  1  1  1  1  1  1  1))
                              (reorder-list-randomly '(2  2  2  2  2  2  2  2  2  3))
                              (reorder-list-randomly '(2  2  2  2  2  2  2  2  2  3))
                              (reorder-list-randomly '(4  4  4  4  4  4  4  4  5  5))
                              (reorder-list-randomly '(4  4  4  4  4  4  4  4  5  5))
                              (reorder-list-randomly '(6  6  6  6  6  6  6  7  7  7))
                              (reorder-list-randomly '(6  6  6  6  6  6  6  7  7  7))
                              (reorder-list-randomly '(8  8  8  8  8  8  9  9  9  9))
                              (reorder-list-randomly '(8  8  8  8  8  8  9  9  9  9))
                              (reorder-list-randomly '(10 10 10 10 10 11 11 11 11 11))
                              (reorder-list-randomly '(10 10 10 10 10 11 11 11 11 11))))
        (syllable-list (reorder-list-randomly '("xun" "vod" "toq" "hax" "wep" "caz" "duf" "jal" "myd" "siq" "ruk" "fec"))))
    (loop for n from 0 upto 9
          append (loop for stimulus in syllable-list
                       for response-list in response-lists
                       for response = (nth n response-list)
                       append (list stimulus response)))))


(defun make-sal-2r ()
 (loop for syllable in (reorder-list-randomly '("GXJ" "ZXK" "GXK" "LQF" "GXF" "LXJ" "ZHJ" "MBW" "GQK"))
        for response in (reorder-list-randomly '(1 1 1 1 1 2 2 2 2))
        append (list syllable response)))


(defun make-sal-low ()
 (loop for syllable in (reorder-list-randomly '("GXJ" "ZHK" "LBF" "MQW" "XJG" "HKZ" "BFL" "QWM"))
        for response in (reorder-list-randomly '(1 2 3 4 5 6 7 8))
        append (list syllable response)))
  

(defun make-sal-medium ()
  (loop repeat 8
        for syllable in (reorder-list-randomly 
                         (loop for item1 in '(#\G #\Z #\L #\M)
                               append (loop for item2 in '(#\X #\H #\B #\Q)
                                            append (loop for item3 in '(#\J #\K #\F #\W)
                                                         collect (coerce (list item1 item2 item3) 'string)))))
        for response in (reorder-list-randomly '(1 2 3 4 5 6 7 8))
        append (list syllable response)))
 
(defun make-sal-long ()
   (loop repeat 16
        for syllable in (reorder-list-randomly 
                         (loop for item1 in '(#\G #\Z #\L #\M)
                               append (loop for item2 in '(#\X #\H #\B #\Q)
                                            append (loop for item3 in '(#\J #\K #\F #\W)
                                                         collect (coerce (list item1 item2 item3) 'string)))))
        for response in (reorder-list-randomly '(1 2 3 4 5 6 7 8 1 2 3 4 5 6 7 8))
        append (list syllable response)))


(defun make-sal-high ()
  (loop for syllable in (reorder-list-randomly '("GXJ" "GXK" "GQJ" "GQK" "ZXJ" "ZXK" "ZQJ" "ZQK"))
        for response in (reorder-list-randomly '(1 2 3 4 5 6 7 8))
        append (list syllable response)))

(defun make-sal-low-14 ()
  (let ((first-letters (reorder-list-randomly '(#\B #\C #\D #\F #\G #\H #\J
                                                #\L #\M #\N #\P #\R #\S #\T)))
        (second-letters (reorder-list-randomly '(#\B #\C #\D #\F #\G #\H #\J
                                                 #\L #\M #\N #\P #\R #\S #\T)))
        (third-letters (reorder-list-randomly '(#\B #\C #\D #\F #\G #\H #\J 
                                                #\L #\M #\N #\P #\R #\S #\T)))
        (responses (reorder-list-randomly '(1 2 3 4 5 6 7 8 9 10 11 12 13 14))))
    (loop for first-letter in first-letters
          for second-letter in second-letters
          for third-letter in third-letters
          for response in responses
          for syllable = (coerce (list first-letter second-letter third-letter) 'string)
          append (list syllable response))))

(defun make-sal-high-14 ()
  (let ((first-letters '(#\B #\B #\B #\B #\B #\B #\B
                         #\C #\C #\C #\C #\C #\C #\C))
        (second-letters '(#\D #\D #\D #\F #\F #\G #\G
                          #\D #\D #\F #\F #\F #\G #\G))
        (third-letters '(#\H #\J #\L #\H #\J #\L #\H 
                         #\J #\L #\H #\J #\L #\H #\J))
        (responses (reorder-list-randomly '(1 2 3 4 5 6 7 8 9 10 11 12 13 14))))
    (loop for first-letter in first-letters
          for second-letter in second-letters
          for third-letter in third-letters
          for response in responses
          for syllable = (coerce (list first-letter second-letter third-letter) 'string)
          append (list syllable response))))



(defun make-low-high-low-list ()
  (let* ((first-letters (reorder-list-randomly  '(#\B #\C #\D #\F #\G #\H #\J #\K #\L #\M #\N #\P #\Q #\R #\S #\T #\V #\W #\X #\Z)))
         (second-letters (reorder-list-randomly '(#\B #\C #\D #\F #\G #\H #\J #\K #\L #\M #\N #\P #\Q #\R #\S #\T #\V #\W #\X #\Z)))
         (third-letters1 (reorder-list-randomly '(#\B #\D #\G #\J #\L #\N #\Q #\S #\V #\X)))
         (third-letters2 (reorder-list-randomly '(#\C #\F #\H #\K #\M #\P #\R #\T #\W #\Z)))
         (low-digits (reorder-list-randomly '(1 2 3 4 5 6 7 8 9 10)))
         (high-digits (reorder-list-randomly '(11 12 13 14 15 16 17 18 19 20)))
         (syllables (loop for first-letter in first-letters
                          for second-letter in second-letters
                          for third1 in third-letters1
                          for third2 in third-letters2
                          collect (list (coerce (list first-letter second-letter third1) 'string)
                                        (coerce (list first-letter second-letter third2) 'string)))))
    (loop repeat 10
          with list1-list = nil
          with highs-list = nil
          for stimuli in (reorder-list-randomly syllables)
          for list1 = (first stimuli)
          for highs = (second stimuli)
          for digit1 in low-digits
          for digit2 in high-digits
          do (setf list1-list (append list1-list (list list1 digit2)))
          (setf highs-list (append highs-list (list highs digit1)))
          finally (return (list list1-list highs-list list1-list)))))


(defun make-list-high-list ()
  (let ((syllables 
         (loop for list1 in (loop for item1 in '(#\G #\Z #\L #\M)
                                  append (loop for item2 in '(#\X #\H #\B #\Q)
                                               append (loop for item3 in '(#\J #\K #\F #\W)
                                                            collect (coerce (list item1 item2 item3) 'string))))
               for highs in (loop for item1 in '(#\G #\Z #\L #\M)
                                  append (loop for item2 in '(#\X #\H #\B #\Q)
                                               append (loop for item3 in '(#\R #\S #\T #\V)
                                                            collect (coerce (list item1 item2 item3) 'string))))
               collect (list list1 highs)))
        (low-digits (reorder-list-randomly '(1 2 3 4 5 6 7 8 9)))
        (high-digits (reorder-list-randomly '(11 12 13 14 15 16 17 18 19))))
    (loop repeat 9
          with list1-list = nil
          with highs-list = nil
          for stimuli in (reorder-list-randomly syllables)
          for list1 = (first stimuli)
          for highs = (second stimuli)
          for digit1 in low-digits
          for digit2 in high-digits
          do (setf list1-list (append list1-list (list list1 digit2)))
          (setf highs-list (append highs-list (list highs digit1)))
          finally (return (list list1-list highs-list list1-list)))))

(defun make-list-low-list ()
  (let ((syllables 
         (loop for list1 in (loop for item1 in '(#\G #\Z #\L #\M)
                                  append (loop for item2 in '(#\X #\H #\B #\Q)
                                               append (loop for item3 in '(#\J #\K #\F #\W)
                                                            collect (coerce (list item1 item2 item3) 'string))))
               for lows in (loop for item1 in '(#\R #\W #\C #\D)
                                 append (loop for item2 in '(#\W #\C #\Y #\Z)
                                              append (loop for item3 in '(#\R #\S #\T #\V)
                                                           collect (coerce (list item1 item2 item3) 'string))))
               collect (list list1 lows)))
        (low-digits (reorder-list-randomly '(1 2 3 4 5 6 7 8 9)))
        (high-digits (reorder-list-randomly '(11 12 13 14 15 16 17 18 19))))
    (loop repeat 9
          with list1-list = nil
          with lows-list = nil
          for stimuli in (reorder-list-randomly syllables)
          for list1 = (first stimuli)
          for lows = (second stimuli)
          for digit1 in low-digits
          for digit2 in high-digits
          do (setf list1-list (append list1-list (list list1 digit2)))
          (setf lows-list (append lows-list (list lows digit1)))
          finally (return (list list1-list lows-list list1-list)))))

(defun make-list-medium-list ()
  (let ((syllables 
         (loop for list1 in (loop for item1 in '(#\G #\Z #\L #\M)
                                  append (loop for item2 in '(#\X #\H #\B #\Q)
                                               append (loop for item3 in '(#\J #\K #\F #\W)
                                                            collect (coerce (list item1 item2 item3) 'string))))
               for mediums in (loop for item1 in '(#\G #\Z #\L #\M)
                                    append (loop for item2 in '(#\W #\C #\Y #\Z)
                                                 append (loop for item3 in '(#\R #\S #\T #\V)
                                                              collect (coerce (list item1 item2 item3) 'string))))
               collect (list list1 mediums)))
        (low-digits (reorder-list-randomly '(1 2 3 4 5 6 7 8 9)))
        (medium-digits (reorder-list-randomly '(11 12 13 14 15 16 17 18 19))))
    (loop repeat 9
          with list1-list = nil
          with mediums-list = nil
          for stimuli in (reorder-list-randomly syllables)
          for list1 = (first stimuli)
          for mediums = (second stimuli)
          for digit1 in low-digits
          for digit2 in medium-digits
          do (setf list1-list (append list1-list (list list1 digit2)))
          (setf mediums-list (append mediums-list (list mediums digit1)))
          finally (return (list list1-list mediums-list list1-list)))))

(defun make-suppes-lists ()
  (list (make-suppes-practice-set1) 
        (make-suppes-experiment-set1) 
        (make-suppes-practice-set2) 
        (make-suppes-experiment-set2)))

(defun make-suppes-practice-set1 ()
 (loop for syllable in (reorder-list-randomly '("GUH" "KEC" "ZAJ"))
        for response in (reorder-list-randomly '(1 2 3))
        append (list syllable response)))

(defun make-suppes-experiment-set1 () 
  (loop for syllable in (reorder-list-randomly '("BOF" "GED" "JUC" "KAX" "NID" "QIG" "QOW" "TAH" "TEV" "WOG" "WUX" "ZIN"))
        for response in (reorder-list-randomly '(1 1 1 1 2 2 2 2 3 3 3 3))
        append (list syllable response)))

(defun make-suppes-practice-set2 ()
 (loop for syllable in (reorder-list-randomly '("GAW" "KOJ" "ZUG"))
        for response in (reorder-list-randomly '(1 2 3))
        append (list syllable response)))

(defun make-suppes-experiment-set2 () 
  (loop for syllable in (reorder-list-randomly '("BEH" "BIX" "GOC" "JIV" "KUW" "NAF" "NEJ" "QAD" "QEN" "TUD" "WIH" "ZOV"))
        for response in (reorder-list-randomly '(1 1 1 1 2 2 2 2 3 3 3 3))
        append (list syllable response)))

(defvar *bruce* '("tav" "kex" "cih" "sob" "ray" "bew" "qim" "gat" "lem" "foz"
                  "dyq" "myp" "yyl" "xyg" "vuf" "jak" "zes" "lij" "byc" "pyw" 
                  "faz" "ver" "lif" "mox" "kul" "nam" "wyt" "jin" "dub" "viw" 
                  "fyx" "tif" "loh" "taz" "gec" "qit" "roc" "sug" "gaj" "vew" 
                  "cyz" "seq" "giy" "nyr" "kur" "wah" "xyr" "coz" "myn" "rof" 
                  "ney" "fig" "lor" "vup" "vyg" "dap" "wes" "jys" "hox" "hyj" 
                  "paz" "jel" "kyv" "fyv" "qut" "jyr" "sez" "lib" "foh" "kun" 
                  "wap" "jew" "dif" "yol" "mud" "fac" "rek" "kiw" "nos" "yur" 
                  "byj" "lez" "ciq" "kom" "juv" "rag" "zel" "xin" "gow" "tuq"
                  "vad" "yeg" "hix" "nyl" "nuw" "jap" "reh" "myq" "yox" "zup"
                  "vod" "yug" "hax" "nol" "niw" "jep" "ruh" "miq" "yax" "zep" 
                  "baj" "loz" "coq" "kem" "jev" "ryg" "zul" "xun" "guw" "toq" 
                  "wep" "juw" "duf" "yul" "myd" "fec" "ruk" "kaw" "nas" "yor" 
                  "poz" "jal" "kiv" "fov" "qyt" "jir" "soz" "lyb" "fah" "kon" 
                  "nuy" "feg" "ler" "vap" "vag" "dyp" "wus" "jis" "hex" "huj" 
                  "caz" "siq" "gey" "nur" "kar" "weh" "bih" "xir" "cez" "mun" 
                  "fax" "soq" "tof" "lah" "dup" "tez" "gac" "qet" "rec" "sig" 
                  "fuz" "var" "laf" "mex" "kil" "num" "wat" "jon" "gop" "deb"
                  "daq" "mep" "yil" "xog" "vef" "jik" "zos" "luj" "boc" "puw" 
                  "tiv" "kax" "cuh" "seb" "ruy" "biw" "qom" "git" "lim" "fiz"
                  "req" "taw" "mip" "qix" "bul" "wam" "nic" "zed" "caj" "kiy" 
                  "guh" "kec" "zaj" "bof" "ged" "jux" "nid" "qig" "qow" "tah"
                  "tev" "wog" "wux" "zin" "gaw" "koj" "zug" "tud" "wih" "zov"
                  "beh" "bix" "goc" "jiv" "kuw" "naf" "nej" "qad" "qen" "ruq" 
                  "pov" "vaq" "yob" "xel" "mic" "suj" "xow" "zet" "vok" "xan"
                  "gij" "vuw" "bek" "vut" "qoz" "hef" "kam" "tis" "pob" "duy" 
                  "kor" "wem" "daf" "lub" "poy" "bup" "naw" "ceg" "puk" "yez"
                  "raq" "tew" "mup" "qax" "bel" "wim" "noc" "zud" "cej" "koy" 
                  "gah" "kic" "zoj" "baf" "gid" "jox" "nud" "qag" "qew" "tih"
                  "weg" "wix" "zon" "kaj" "zeg" "tid" "woh" "zuv" "goh" "ker"   
                  "bah" "bex" "gic" "jov" "nef" "nij" "qod" "qun" "noy" "qat"
                  "pav" "veq" "yib" "xol" "muc" "saj" "xew" "zit" "vuk" "xen"              
                  ))

(defun make-abac ()
  "This routine creates four sets of ab-ac lists for use in Bruce experiments."
  (loop with biglist = (reorder-list-randomly *bruce*)
        for n from 0 upto 15
        for n1 = (* 20 n)
        for ab = nil
        for ac = nil
        for a = (loop repeat 5 for item in (nthcdr (+ n1 0) biglist) collect item)
        for b = (loop repeat 5 for item in (nthcdr (+ n1 5) biglist) collect item)
        for c = (loop repeat 5 for item in (nthcdr (+ n1 10) biglist) collect item)
        append (loop for a1 in a
                     for b1 in b
                     for c1 in c
                     do
                     (setf ab (append ab (list a1 b1)))
                     (setf ac (append ac (list a1 c1)))
                     finally (return (list ab ac)))))

(defun make-abcb ()
  "This routine creates four sets of ab-cb lists for use in Bruce experiments."
  (loop with biglist = (reorder-list-randomly *bruce*)
        for n from 0 upto 15
        for n1 = (* 20 n)
        for ab = nil
        for cb = nil
        for a = (loop repeat 5 for item in (nthcdr (+ n1 0) biglist) collect item)
        for b = (loop repeat 5 for item in (nthcdr (+ n1 5) biglist) collect item)
        for c = (loop repeat 5 for item in (nthcdr (+ n1 10) biglist) collect item)
        append (loop for a1 in a
                     for b1 in b
                     for c1 in c
                     do
                     (setf ab (append ab (list a1 b1)))
                     (setf cb (append cb (list c1 b1)))
                     finally (return (list ab cb)))))
                                     
(defun make-abcd ()
  "This routine creates four sets of ab-ac lists for use in Bruce experiments."
  (loop with biglist = (reorder-list-randomly *bruce*)
        for n from 0 upto 15
        for n1 = (* 20 n)
        for ab = nil
        for cd = nil
        for a = (loop repeat 5 for item in (nthcdr (+ n1 0) biglist) collect item)
        for b = (loop repeat 5 for item in (nthcdr (+ n1 5) biglist) collect item)
        for c = (loop repeat 5 for item in (nthcdr (+ n1 10) biglist) collect item)
        for d = (loop repeat 5 for item in (nthcdr (+ n1 15) biglist) collect item)
        append (loop for a1 in a
                      for b1 in b
                      for c1 in c
                      for d1 in d
                      do
                      (setf ab (append ab (list a1 b1)))
                      (setf cd (append cd (list c1 d1)))
                      finally (return (list ab cd)))))
                                     

                                         

                               
; This is a continuation of master1.lsp
; Changes:
;   1. Commented off routine mark-chunks-as-instance-of because I don't think that
;      'instance-of is being used.
;   2. Added in   (setf *print-forgetting-updates* nil)
;   3. Made more consistent use of convert-tree-to-list and restore-net-from-file in order 
;      to save the beginning conditions of an experiment and restore them.  convert-tree-to-list
;      no longer requires *net* as one of its inputs.  

;;                   Short Term Memory Simulations


;; There are several routines for running simulations of STM span experiments.
;;   Several parameters can be changed:
;;     1. The presentation rate is the rate at which the experimenter presents
;;        the words.  1000 means 1 word each second.
;;     2. The duration rate is the length of time that the word is exposed upon
;;        visual presentation.
;;     3. The signal to respond delay is the time delay after the presentation
;;        of the last item until the signal to respond is given.  If it is
;;        set to 0, then the signal to respond will be given immediately after
;;        the presentation of the last syllable.



;;                        Word Span Experiments

(defun nicolson-and-fawcett-simulation ()
  (format t "~%~%Nicolson and Facett simulation.~%")
  (setf *presentation-rate* 1000)
  (setf *signal-to-respond-delay* 0)
  (setf *print-major-updates* nil)
  (setf *print-time-updates* nil)
  (setf *print-imagery-stores* nil)
  (setf *print-experimenter-updates* nil)
  (setf *print-epam-updates* nil)
  (setf *print-strategy-updates* nil)
  (setf *print-forgetting-updates* nil)
  (setf *number-of-sessions* 1000)
  (setf *output-from-ltm* t)
  (setf *output-from-echo* t)
  (setf *stimulus-lists* (list *nicolson-short-words* 
                               *nicolson-medium-words* 
                               *nicolson-long-words*))
  (setf *time-allowed-for-recall* 10000)
  (seed-random-generator)
  (initialize-variables)
  (study-auditory-vocabulary)
  (setf *net-list* (convert-tree-to-list))
  (nicolson-and-fawcett)
  )

(defun nicolson-and-fawcett-simulation-without-echo ()
  (format t "~%~%Nicolson & Fawcett without contribution from ECHO~%")
  (setf *presentation-rate* 1000)
  (setf *signal-to-respond-delay* 0)
  (setf *print-major-updates* nil)
  (setf *print-time-updates* nil) 
  (setf *print-imagery-stores* nil) 
  (setf *print-experimenter-updates* nil) 
  (setf *print-epam-updates* nil) 
  (setf *print-strategy-updates* nil)
  (setf *print-forgetting-updates* nil)
  (setf *number-of-sessions* 1000)
  (setf *output-from-ltm* t)
  (setf *output-from-echo* nil)
  (setf *stimulus-lists* (list *nicolson-short-words* 
                               *nicolson-medium-words* 
                               *nicolson-long-words*))
  (setf *time-allowed-for-recall* 10000)
  (seed-random-generator)
  (initialize-variables)
  (study-auditory-vocabulary)
  (setf *net-list* (convert-tree-to-list))
  (nicolson-and-fawcett)
  )

(defun nicolson-and-fawcett-simulation-without-echo-or-ltm ()
  (format t "~%~%Nicolson & Fawcett without contribution from ECHO or LTM~%")
  (setf *presentation-rate* 1000)
  (setf *signal-to-respond-delay* 0)
  (setf *print-major-updates* nil)
  (setf *print-time-updates* nil) 
  (setf *print-imagery-stores* nil) 
  (setf *print-experimenter-updates* nil) 
  (setf *print-epam-updates* nil) 
  (setf *print-strategy-updates* nil)
  (setf *print-forgetting-updates* nil)
  (setf *number-of-sessions* 1000)
  (setf *output-from-ltm* nil)
  (setf *output-from-echo* nil)
  (setf *stimulus-lists* (list *nicolson-short-words* 
                               *nicolson-medium-words* 
                               *nicolson-long-words*))
  (setf *time-allowed-for-recall* 10000)
  (seed-random-generator)
  (initialize-variables)
  (study-auditory-vocabulary)
  (setf *net-list* (convert-tree-to-list))
  (nicolson-and-fawcett)
  )

(defun nicolson-and-fawcett ()
  (setf *overall-results* nil)
  (setf *list-length-results* (make-array 7 :initial-element 0))
  (setf *serial-position-results* (make-array 7 :initial-element 0))
  (setf *experimenter* 'auditory-span-experimenter)
  (setf *modality-of-output* 'auditory)
  (setf *concurrent-routine* nil)
  (setf *method-for-counting-syllables* 'character-method)
  (setf *stimulus-duration-rate* 0)
  (loop for enter-chunk in '(117 150 233 243 182)
        for enter-syllable in '(119 118 145 165 126)
        for simulation in '(cont15 cont11 cont8 dys11 dys15)
        do (setf *time-to-enter-an-object* enter-chunk
                 *time-to-enter-a-syllable* enter-syllable)
        (format t "~a simulation with enter-chunk=~a, enter-syllable=~a~%"
                simulation enter-chunk enter-syllable)
        do (setf *errors-array* 
                 (make-array `(,(length *stimulus-lists*) 6)))
        (setf *position-array* 
              (make-array `(,(length *stimulus-lists*) 6)))
        (loop repeat *number-of-sessions*
              do 
              (nicolson-and-fawcett-experiment))
        (report-nicolson-and-fawcett-results))
  ; Now reset these parameters to their default values
  (setf *time-to-enter-an-object* 120)
  (setf *time-to-enter-a-syllable* 120)
  (format t "~%Memory span using the span measure for EPAM plotted against~%")
  (format t "best fitting lines for human subjects in Nicholson and Fawcett's (1991)~%")
  (format t "experiment.  (Each data point indicates the recall of one of five groups~%")
  (format t "of students with one of three lengths of words, 1, 2, or 3 syllables.~%")
  (format t "The articulation rate is in terms of words-per-second.~%")
  (format t "Group         ArtRate    People      EPAM6~%")
  (format t "             ---------  ---------  ---------~%")
  (format t "dyslexic11-3    1.4        4.0      ~5,1f~%" (nth 110 *overall-results*))
  (format t "control8-3      1.5        3.7      ~5,1f~%" (nth 74 *overall-results*))
  (format t "dyslexic11-2    1.7        4.3      ~5,1f~%" (nth 109 *overall-results*))
  (format t "dyslexic15-3    1.8        4.5      ~5,1f~%" (nth 146 *overall-results*))
  (format t "control11-3     1.9        4.3      ~5,1f~%" (nth 38 *overall-results*))
  (format t "control8-2      2.0        4.3      ~5,1f~%" (nth 73 *overall-results*))
  (format t "control15-3     2.1        4.7      ~5,1f~%" (nth 2 *overall-results*))
  (format t "dyslexic15-2    2.3        4.7      ~5,1f~%" (nth 145 *overall-results*))
  (format t "dyslexic11-1    2.5        4.8      ~5,1f~%" (nth 108 *overall-results*))
  (format t "control8-1      2.6        4.7      ~5,1f~%" (nth 72 *overall-results*))
  (format t "control11-2     2.7        4.6      ~5,1f~%" (nth 37 *overall-results*))
  (format t "control15-2     2.8        5.2      ~5,1f~%" (nth 1 *overall-results*))
  (format t "dyslexic15-1    3.3        5.3      ~5,1f~%" (nth 144 *overall-results*))
  (format t "control11-1     3.6        5.2      ~5,1f~%" (nth 36 *overall-results*))
  (format t "control15-1     4.2        5.4      ~5,1f~%" (nth 0 *overall-results*))
  (format t "~%Mean number of correct items in recall as a function~%")
  (format t "of list-length averaged across all groups and word lengths~%")
  (format t "Data for people is from Nicolson and Fawcett (1991)~%")
  (format t "List Length    People     EPAM6~%")
  (format t "    2            2.0     ~5,1f~%" (/ (aref *list-length-results* 2) 15))
  (format t "    3            2.9     ~5,1f~%" (/ (aref *list-length-results* 3) 15))
  (format t "    4            3.5     ~5,1f~%" (/ (aref *list-length-results* 4) 15))
  (format t "    5            3.2     ~5,1f~%" (/ (aref *list-length-results* 5) 15))
  (format t "    6            2.6     ~5,1f~%" (/ (aref *list-length-results* 6) 15))
  (format t "~%Mean number of correct items in recall as a function~%")
  (format t "of serial position in six-word lists  averaged across~%")
  (format t "all groups and word lengths in simulation of Nicolson and~%")
  (format t "Fawcett (1991)~%")
  (format t "List Length     EPAM6~%")
  (format t "    1          ~5,2f~%" (/ (aref *serial-position-results* 1) 15))
  (format t "    2          ~5,2f~%" (/ (aref *serial-position-results* 2) 15))
  (format t "    3          ~5,2f~%" (/ (aref *serial-position-results* 3) 15))
  (format t "    4          ~5,2f~%" (/ (aref *serial-position-results* 4) 15))
  (format t "    5          ~5,2f~%" (/ (aref *serial-position-results* 5) 15))
  (format t "    6          ~5,2f~%" (/ (aref *serial-position-results* 6) 15))
  )



(defun baddeley-lewis-and-vallar-exp3-simulation ()
  (setf *signal-to-respond-delay* 1500)
  (setf *print-major-updates* nil)
  (setf *print-time-updates* nil) 
  (setf *print-imagery-stores* nil) 
  (setf *print-experimenter-updates* nil) 
  (setf *print-epam-updates* nil) 
  (setf *print-strategy-updates* nil)
  (setf *print-forgetting-updates* nil)
  (setf *number-of-sessions* 1000)
  (setf *time-to-enter-an-object* 120)
  (setf *time-to-enter-a-syllable* 120)
  (setf *similar-set*  *baddeley-lewis-and-vallar-similar-set*)
  (setf *dissimilar-set* *baddeley-lewis-and-vallar-dissimilar-set*)
  (setf *time-allowed-for-recall* 10000)
  (setf *suppression-list* *one-two-suppression-list*)
  (setf *output-from-ltm* t)
  (setf *output-from-echo* t)
  (baddeley-lewis-and-vallar-exp3)
  )

(defun baddeley-lewis-and-vallar-exp3-simulation-without-search ()
  (format t "Baddeley Lewis and Vallar exp3 without search~%")
  (setf *signal-to-respond-delay* 1500)
  (setf *print-major-updates* nil)
  (setf *print-time-updates* nil) 
  (setf *print-imagery-stores* nil) 
  (setf *print-experimenter-updates* nil) 
  (setf *print-epam-updates* nil) 
  (setf *print-strategy-updates* nil)
  (setf *print-forgetting-updates* nil)
  (setf *number-of-sessions* 1000)
  (setf *time-to-enter-an-object* 120)
  (setf *time-to-enter-a-syllable* 120)
  (setf *similar-set*  *baddeley-lewis-and-vallar-similar-set*)
  (setf *dissimilar-set* *baddeley-lewis-and-vallar-dissimilar-set*)
  (setf *time-allowed-for-recall* 10000)
  (setf *suppression-list* *one-two-suppression-list*)
  (setf *output-from-ltm* t)
  (setf *output-from-echo* t)
  (baddeley-lewis-and-vallar-exp3))

(defun baddeley-lewis-and-vallar-exp3-simulation-without-search-or-echo ()
  (format t "Baddeley Lewis and Vallar exp3 without search or echo~%")
  (setf *signal-to-respond-delay* 1500)
  (setf *print-major-updates* nil)
  (setf *print-time-updates* nil) 
  (setf *print-imagery-stores* nil) 
  (setf *print-experimenter-updates* nil) 
  (setf *print-epam-updates* nil) 
  (setf *print-strategy-updates* nil)
  (setf *print-forgetting-updates* nil)
  (setf *number-of-sessions* 1000)
  (setf *time-to-enter-an-object* 120)
  (setf *time-to-enter-a-syllable* 120)
  (setf *similar-set*  *baddeley-lewis-and-vallar-similar-set*)
  (setf *dissimilar-set* *baddeley-lewis-and-vallar-dissimilar-set*)
  (setf *time-allowed-for-recall* 10000)
  (setf *suppression-list* *one-two-suppression-list*)
  (setf *output-from-ltm* t)
  (setf *output-from-echo* nil)
  (baddeley-lewis-and-vallar-exp3))

(defun baddeley-lewis-and-vallar-exp3-simulation-with-the-the-the ()
  (setf *signal-to-respond-delay* 1500)
  (setf *print-major-updates* nil)
  (setf *print-time-updates* nil) 
  (setf *print-imagery-stores* nil) 
  (setf *print-experimenter-updates* nil) 
  (setf *print-epam-updates* nil) 
  (setf *print-strategy-updates* nil)
  (setf *print-forgetting-updates* nil)
  (setf *number-of-sessions* 1000)
  (setf *time-to-enter-an-object* 120)
  (setf *time-to-enter-a-syllable* 120)
  (setf *similar-set*  *baddeley-lewis-and-vallar-similar-set*)
  (setf *dissimilar-set* *baddeley-lewis-and-vallar-dissimilar-set*)
  (setf *time-allowed-for-recall* 10000)
  (setf *suppression-list* *the-the-the-suppression-list*)
  (setf *output-from-ltm* t)
  (setf *output-from-echo* t)
  (baddeley-lewis-and-vallar-exp3))

(defun baddeley-lewis-and-vallar-exp3 ()
  (setf *overall-results* nil)
  (seed-random-generator)
  (initialize-variables)
  (study-auditory-vocabulary)
  (associate-consonants-with-their-names)
  (setf *suppression-object* (make-suppression-object *suppression-list*))
  (loop while (study *suppression-object*))
  (setf *net-list* (convert-tree-to-list))
  (setf *experimenter* 'auditory-span-experimenter)
  (setf *modality-of-output* 'visual)
  (setf *method-for-counting-syllables* 'character-method)
  (setf *stimulus-duration-rate* 0)
  (loop for presentation-rate in '(500 1500)
        do (setf *presentation-rate* presentation-rate)
        (loop for stimulus-list in '(*similar-set* *dissimilar-set*)
              do (loop for condition in '(control suppression)
                       do (setf *errors-array* (make-array 7))
                       (loop repeat *number-of-sessions*
                             do 
                             (baddeley-lewis-and-vallar-exp3-experiment
                              (eval stimulus-list)
                              condition))
                       (format t "Condition = ~a, Set = ~a, Rate = ~a msec.:~%"
                               condition stimulus-list presentation-rate)
                       (report-baddeley-lewis-and-vallar-exp3-results))))
  (format t "~%Memory Span for Letters as a Function of Phonological~%")
  (format t "Similarity, Rate of Presentation and Articulatory~%")
  (format t "Suppression.  Percentage Error at Each Sequence Length~%")
  (format t "               ~%Control Condition~%")
  (format t "--------------------------------------------------------~%")
  (format t "                 People                   EPAM6~%")
  (format t "        -----------------------  ----------------------~%")
  (format t "            Fast        Slow         Fast        Slow~%")
  (format t "Length  Sim  Dissim  Sim Dissim  Sim Dissim  Sim Dissim~%")
  (format t "  2     0.0    0.0   0.0   0.0 ~5,1f ~5,1f ~5,1f ~5,1f~%"
          (nth 0 *overall-results*) (nth 12 *overall-results*) 
          (nth 24 *overall-results*) (nth 36 *overall-results*))
  (format t "  3     0.0    0.0   3.5   0.0 ~5,1f ~5,1f ~5,1f ~5,1f~%"
          (nth 1 *overall-results*) (nth 13 *overall-results*) 
          (nth 25 *overall-results*) (nth 37 *overall-results*))
  (format t "  4     5.2    1.0   9.9   0.0 ~5,1f ~5,1f ~5,1f ~5,1f~%"
          (nth 2 *overall-results*) (nth 14 *overall-results*) 
          (nth 26 *overall-results*) (nth 38 *overall-results*))
  (format t "  5    15.8    5.8  23.8   5.4 ~5,1f ~5,1f ~5,1f ~5,1f~%"
          (nth 3 *overall-results*) (nth 15 *overall-results*) 
          (nth 27 *overall-results*) (nth 39 *overall-results*))
  (format t "  6    33.0   20.0  26.4  13.5 ~5,1f ~5,1f ~5,1f ~5,1f~%"
          (nth 4 *overall-results*) (nth 16 *overall-results*) 
          (nth 28 *overall-results*) (nth 40 *overall-results*))
  (format t "  7    44.0   26.8  42.9  28.0 ~5,1f ~5,1f ~5,1f ~5,1f~%"
          (nth 5 *overall-results*) (nth 17 *overall-results*) 
          (nth 29 *overall-results*) (nth 41 *overall-results*))
  (format t " Mean  16.3    8.9  17.8   7.8 ~5,1f ~5,1f ~5,1f ~5,1f~%"
          (/ (loop for n from 0 upto 5 sum (nth n *overall-results*)) 6)
          (/ (loop for n from 12 upto 17 sum (nth n *overall-results*)) 6)
          (/ (loop for n from 24 upto 29 sum (nth n *overall-results*)) 6)
          (/ (loop for n from 32 upto 41 sum (nth n *overall-results*)) 6)
          )      
  (format t "             ~%Suppression Condition~%")
  (format t "--------------------------------------------------------~%")
  (format t "                 People                   EPAM6~%")
  (format t "        -----------------------  ----------------------~%")
  (format t "            Fast        Slow         Fast        Slow~%")
  (format t "Length  Sim  Dissim  Sim Dissim  Sim Dissim  Sim Dissim~%")
  (format t "  2     1.0    0.0   0.0   0.0 ~5,1f ~5,1f ~5,1f ~5,1f~%"
          (nth 6 *overall-results*) (nth 18 *overall-results*) 
          (nth 30 *overall-results*) (nth 38 *overall-results*))
  (format t "  3     2.8    0.0   4.9   0.0 ~5,1f ~5,1f ~5,1f ~5,1f~%"
          (nth 7 *overall-results*) (nth 19 *overall-results*) 
          (nth 31 *overall-results*) (nth 43 *overall-results*))
  (format t "  4    10.9    4.7  11.5   4.2 ~5,1f ~5,1f ~5,1f ~5,1f~%"
          (nth 8 *overall-results*) (nth 20 *overall-results*) 
          (nth 32 *overall-results*) (nth 44 *overall-results*))
  (format t "  5    31.3    9.2  38.3  15.4 ~5,1f ~5,1f ~5,1f ~5,1f~%"
          (nth 9 *overall-results*) (nth 21 *overall-results*) 
          (nth 33 *overall-results*) (nth 45 *overall-results*))
  (format t "  6    34.7   25.3  46.5  29.2 ~5,1f ~5,1f ~5,1f ~5,1f~%"
          (nth 10 *overall-results*) (nth 22 *overall-results*) 
          (nth 34 *overall-results*) (nth 46 *overall-results*))
  (format t "  7    51.2   39.3  58.0  49.4 ~5,1f ~5,1f ~5,1f ~5,1f~%"
          (nth 11 *overall-results*) (nth 23 *overall-results*) 
          (nth 35 *overall-results*) (nth 47 *overall-results*))
  (format t " Mean  22.0   13.0  26.5  16.4 ~5,1f ~5,1f ~5,1f ~5,1f~%"
          (/ (loop for n from 6 upto 11 sum (nth n *overall-results*)) 6)
          (/ (loop for n from 18 upto 23 sum (nth n *overall-results*)) 6)
          (/ (loop for n from 30 upto 35 sum (nth n *overall-results*)) 6)
          (/ (loop for n from 38 upto 47 sum (nth n *overall-results*)) 6))
  )


(defun baddeley-lewis-and-vallar-exp5-simulation ()
  (setf *presentation-rate* 1500)
  (setf *signal-to-respond-delay* 0)
  (setf *print-major-updates* nil)
  (setf *print-time-updates* nil) 
  (setf *print-imagery-stores* nil) 
  (setf *print-experimenter-updates* nil) 
  (setf *print-epam-updates* nil) 
  (setf *print-strategy-updates* nil)
  (setf *print-forgetting-updates* nil)
  (setf *number-of-sessions* 1000)
  (setf *time-to-enter-an-object* 120)
  (setf *time-to-enter-a-syllable* 120)
  (setf *short-set*  *baddeley-lewis-and-vallar-short-words*)
  (setf *long-set* *baddeley-lewis-and-vallar-long-words*)
  (setf *time-allowed-for-recall* 20000)
  (setf *suppression-list* *one-through-four-suppression-list*)
  (setf *output-from-ltm* t)
  (setf *output-from-echo* t)
  (setf *method-for-counting-syllables* 'character-method)
  (baddeley-lewis-and-vallar-exp5)
  )

(defun baddeley-lewis-and-vallar-exp5 ()
  (setf *overall-results* nil)
  (seed-random-generator)
  (initialize-variables)
  (study-auditory-vocabulary)
  (associate-words (append *baddeley-lewis-and-vallar-short-words*
                           *baddeley-lewis-and-vallar-long-words*)
                   (append *baddeley-lewis-and-vallar-short-words-written*
                           *baddeley-lewis-and-vallar-long-words-written*)
                   'auditory
                   'visual
                   'auditory-visual)
  (setf *suppression-object* (make-suppression-object *suppression-list*))
  (loop while (study *suppression-object*))
  (setf *net-list* (convert-tree-to-list))
  (setf *experimenter* 'auditory-span-experimenter)
  (setf *modality-of-output* 'visual)
  (setf *stimulus-duration-rate* 0)
  (loop for stimulus-list in '(*short-set* *long-set*)
        do (loop for condition in '(control suppression)
                 do (setf *position-array* (make-array '(5 5)))
                 (loop repeat *number-of-sessions*
                       do 
                       (baddeley-lewis-and-vallar-exp5-experiment
                           (eval stimulus-list)
                           condition))
                 (format t "Condition = ~a, Set = ~a:~%"
                         condition stimulus-list)
                 (report-baddeley-lewis-and-vallar-exp5-results)))
  (format t "~%The influence of word length and articulatory~%")
  (format t "suppression on immediate memory for sequences of three, four and~%")
  (format t "five auditorially presented words for human subjects in Baddeley,~%")
  (format t "Lewis, and Vallar's (1984) Experiment 5 and for EPAM.~%~%")
  (format t " Serial             People                    EPAM6~%")
  (format t "Position    ------------------------  ------------------------~%")
  (format t "              Control    Suppression    Control    Suppression~%")
  (format t "            -----------  -----------  -----------  -----------~%")
  (format t "            Short  Long  Short  Long  Short  Long  Short  Long~%")     
  (format t "            --------------------------------------------------~%")
  (format t "3-Word Lists~%")
  (format t "    1        0.03  0.03   0.03  0.06  ~5,2f ~5,2f  ~5,2f ~5,2f~%"
          (/ (nth 0 *overall-results*) 100)
          (/ (nth 24 *overall-results*) 100) 
          (/ (nth 12 *overall-results*) 100)
          (/ (nth 36 *overall-results*) 100)
          )
  (format t "    2        0.04  0.13   0.22  0.25  ~5,2f ~5,2f  ~5,2f ~5,2f~%"
          (/ (nth 1 *overall-results*) 100)
          (/ (nth 25 *overall-results*) 100) 
          (/ (nth 13 *overall-results*) 100)
          (/ (nth 37 *overall-results*) 100))
  (format t "    3        0.05  0.18   0.28  0.31  ~5,2f ~5,2f  ~5,2f ~5,2f~%"
          (/ (nth 2 *overall-results*) 100)
          (/ (nth 26 *overall-results*) 100) 
          (/ (nth 14 *overall-results*) 100)
          (/ (nth 38 *overall-results*) 100))
  (format t "4-Word Lists~%")
  (format t "    1        0.03  0.06   0.08  0.11  ~5,2f ~5,2f  ~5,2f ~5,2f~%"
          (/ (nth 3 *overall-results*) 100)
          (/ (nth 27 *overall-results*) 100) 
          (/ (nth 15 *overall-results*) 100)
          (/ (nth 39 *overall-results*) 100))
  (format t "    2        0.16  0.28   0.31  0.40  ~5,2f ~5,2f  ~5,2f ~5,2f~%"
          (/ (nth 4 *overall-results*) 100)
          (/ (nth 28 *overall-results*) 100) 
          (/ (nth 16 *overall-results*) 100)
          (/ (nth 40 *overall-results*) 100))
  (format t "    3        0.21  0.42   0.54  0.59  ~5,2f ~5,2f  ~5,2f ~5,2f~%"
          (/ (nth 5 *overall-results*) 100)
          (/ (nth 29 *overall-results*) 100) 
          (/ (nth 17 *overall-results*) 100)
          (/ (nth 41 *overall-results*) 100))
  (format t "    4        0.20  0.37   0.47  0.49  ~5,2f ~5,2f  ~5,2f ~5,2f~%"
          (/ (nth 6 *overall-results*) 100)
          (/ (nth 30 *overall-results*) 100) 
          (/ (nth 18 *overall-results*) 100)
          (/ (nth 42 *overall-results*) 100))
  (format t "5-Word Lists~%")
  (format t "    1        0.06  0.13   0.08  0.19  ~5,2f ~5,2f  ~5,2f ~5,2f~%"
          (/ (nth 7 *overall-results*) 100)
          (/ (nth 31 *overall-results*) 100) 
          (/ (nth 19 *overall-results*) 100)
          (/ (nth 43 *overall-results*) 100))
  (format t "    2        0.27  0.37   0.40  0.48  ~5,2f ~5,2f  ~5,2f ~5,2f~%"
          (/ (nth 8 *overall-results*) 100)
          (/ (nth 32 *overall-results*) 100) 
          (/ (nth 20 *overall-results*) 100)
          (/ (nth 44 *overall-results*) 100))
  (format t "    3        0.44  0.57   0.61  0.70  ~5,2f ~5,2f  ~5,2f ~5,2f~%"
          (/ (nth 9 *overall-results*) 100)
          (/ (nth 33 *overall-results*) 100) 
          (/ (nth 21 *overall-results*) 100)
          (/ (nth 45 *overall-results*) 100))
  (format t "    4        0.51  0.72   0.70  0.76  ~5,2f ~5,2f  ~5,2f ~5,2f~%"
          (/ (nth 10 *overall-results*) 100)
          (/ (nth 34 *overall-results*) 100) 
          (/ (nth 22 *overall-results*) 100)
          (/ (nth 46 *overall-results*) 100))
  (format t "    5        0.43  0.48   0.67  0.60  ~5,2f ~5,2f  ~5,2f ~5,2f~%"
          (/ (nth 11 *overall-results*) 100)
          (/ (nth 35 *overall-results*) 100) 
          (/ (nth 23 *overall-results*) 100)
          (/ (nth 47 *overall-results*) 100))
  )

(defun palmer-and-ornstein-exp1-simulation ()
  (setf *presentation-rate* 2500)
  (setf *stimulus-duration-rate* 2250)
  (setf *signal-to-respond-delay* 0)
  (setf *print-major-updates* nil)
  (setf *print-time-updates* nil) 
  (setf *print-imagery-stores* nil) 
  (setf *print-experimenter-updates* nil) 
  (setf *print-epam-updates* nil) 
  (setf *print-strategy-updates* nil)
  (setf *print-forgetting-updates* nil)
   (setf *print-net-updates* nil)
  (setf *number-of-sessions* 1000)
  (setf *time-to-enter-an-object* 120)
  (setf *time-to-enter-a-syllable* 120)
  (setf *time-allowed-for-recall* 10000)
  (setf *output-from-ltm* t)
  (setf *output-from-echo* t)
  (setf *ignore-new-stimulus* nil)
  (setf *stimulus-lists*
        (list *palmer-and-ornstein-visual-list1*
              *palmer-and-ornstein-visual-list2*
              *palmer-and-ornstein-visual-list3*
              *palmer-and-ornstein-visual-list4*
              *palmer-and-ornstein-visual-list5*
              *palmer-and-ornstein-visual-list6*
              *palmer-and-ornstein-visual-list7*
              *palmer-and-ornstein-visual-list8*))
  (setf *method-for-counting-syllables* 'character-method)
  (initialize-variables)
  (study-auditory-vocabulary)
  (setf *avlist* nil)
  (setf *valist* nil)
  (associate-words (append *palmer-and-ornstein-visual-list1*
                           *palmer-and-ornstein-visual-list2*
                           *palmer-and-ornstein-visual-list3*
                           *palmer-and-ornstein-visual-list4*
                           *palmer-and-ornstein-visual-list5*
                           *palmer-and-ornstein-visual-list6*
                           *palmer-and-ornstein-visual-list7*
                           *palmer-and-ornstein-visual-list8*)
                   (append *palmer-and-ornstein-list1*
                           *palmer-and-ornstein-list2*
                           *palmer-and-ornstein-list3*
                           *palmer-and-ornstein-list4*
                           *palmer-and-ornstein-list5*
                           *palmer-and-ornstein-list6*
                           *palmer-and-ornstein-list7*
                           *palmer-and-ornstein-list8*)
                   'visual
                   'auditory
                   'visual-auditory)
  (setf *net-list* (convert-tree-to-list))
  (setf *concurrent-routine* nil)
  (setf *paired-strategy* 'current-segment) ; alternatives include 'paired and 'no-rehearsal
  (palmer-and-ornstein-exp1))


(defun palmer-and-ornstein-exp1 ()
   (setf *overall-results* nil)
   (setf *experimenter* 'serial-probe-experimenter)
   (setf *errors-array* (make-array '(2 9)))
   (loop repeat *number-of-sessions*
     for n = 0 then (cond ((= n 3) 0) ('else (1+ n)))
     do (restore-net-from-file)
     (palmer-and-ornstein-exp1-experiment n))
   (report-palmer-and-ornstein-results)
   (format t "~%Serial probe proportion recalled as a function~%")
   (format t "of serial position for human subjects using a cumulative-~%")
   (format t "rehearsal strategy (Group CR) and a paired-rehearsal strategy~%")
   (format t "(Group PA) in Palmer and Ornstein's (1971) Experiment 1.~%")
   (format t " Serial      People      EPAM6~%")
   (format t "Position    --------    --------~%")
   (format t "            CR    PA    CR    PA~%")
   (format t "           ----------  ----------~%")
   (format t "    2      0.65  0.19 ~5,2f ~5,2f~%"
     (nth 0 *overall-results*) (nth 8 *overall-results*))
   (format t "    3      0.46  0.14 ~5,2f ~5,2f~%"
     (nth 1 *overall-results*) (nth 9 *overall-results*))
   (format t "    4      0.40  0.21 ~5,2f ~5,2f~%"
     (nth 2 *overall-results*) (nth 10 *overall-results*))
   (format t "    5      0.14  0.14 ~5,2f ~5,2f~%"
     (nth 3 *overall-results*) (nth 11 *overall-results*))
   (format t "    6      0.28  0.28 ~5,2f ~5,2f~%"
     (nth 4 *overall-results*) (nth 12 *overall-results*))
   (format t "    7      0.43  0.50 ~5,2f ~5,2f~%"
     (nth 5 *overall-results*) (nth 13 *overall-results*))
   (format t "    8      0.50  0.75 ~5,2f ~5,2f~%"
     (nth 6 *overall-results*) (nth 14 *overall-results*))
   (format t "    9      0.87  0.78 ~5,2f ~5,2f~%"
     (nth 7 *overall-results*) (nth 15 *overall-results*))
   )


(defun palmer-and-ornstein-exp2-simulation ()
  (setf *presentation-rate* 2500)
  (setf *stimulus-duration-rate* 2250)
  (setf *signal-to-respond-delay* 15000)
  (setf *print-major-updates* nil)
  (setf *print-time-updates* nil) 
  (setf *print-imagery-stores* nil) 
  (setf *print-experimenter-updates* nil) 
  (setf *print-epam-updates* nil) 
  (setf *print-strategy-updates* nil)
  (setf *print-forgetting-updates* nil)
   (setf *print-net-updates* nil)
  (setf *number-of-sessions* 1000)
  (setf *time-to-enter-an-object* 120)
  (setf *time-to-enter-a-syllable* 120)
  (setf *time-allowed-for-recall* 10000)
  (setf *output-from-ltm* t)
  (setf *output-from-echo* t)
  (setf *ignore-new-stimulus* nil)
  (setf *stimulus-lists*
        (list *palmer-and-ornstein-visual-list1*
              *palmer-and-ornstein-visual-list2*
              *palmer-and-ornstein-visual-list3*
              *palmer-and-ornstein-visual-list4*
              *palmer-and-ornstein-visual-list5*
              *palmer-and-ornstein-visual-list6*
              *palmer-and-ornstein-visual-list7*
              *palmer-and-ornstein-visual-list8*))
  (setf *paired-strategy* 'paired) ; alternatives include 'current-segment and 'paired
  (initialize-variables)
  (study-auditory-vocabulary)
  (setf *avlist* nil)
  (setf *valist* nil)
  (associate-words (append *palmer-and-ornstein-visual-list1*
                           *palmer-and-ornstein-visual-list2*
                           *palmer-and-ornstein-visual-list3*
                           *palmer-and-ornstein-visual-list4*
                           *palmer-and-ornstein-visual-list5*
                           *palmer-and-ornstein-visual-list6*
                           *palmer-and-ornstein-visual-list7*
                           *palmer-and-ornstein-visual-list8*)
                   (append *palmer-and-ornstein-list1*
                           *palmer-and-ornstein-list2*
                           *palmer-and-ornstein-list3*
                           *palmer-and-ornstein-list4*
                           *palmer-and-ornstein-list5*
                           *palmer-and-ornstein-list6*
                           *palmer-and-ornstein-list7*
                           *palmer-and-ornstein-list8*)
                   'visual
                   'auditory
                   'visual-auditory)
  (associate-spoken-digits-with-digits)
  (setf *net-list* (convert-tree-to-list))
  (setf *concurrent-routine* nil)
  (palmer-and-ornstein-exp2))

(defun palmer-and-ornstein-exp2 ()
  (setf *overall-results* nil)
  (seed-random-generator)
  (setf *experimenter* 'delayed-serial-probe-recall-experimenter)
  (setf *errors-array* (make-array '(2 9)))
  (loop repeat *number-of-sessions*
    for i from 1
        for n = 0 then (cond ((= n 3) 0) ('else (1+ n)))
        do (format t "~a~%" i)
    (restore-net-from-file)
       (palmer-and-ornstein-exp2-experiment n))
  (report-palmer-and-ornstein-results)
  (format t "~%Serial probe proportion recalled as a function~%")
  (format t "of serial position for human subjects using a cumulative-~%")
  (format t "rehearsal strategy (Group CR) and a no-rehearsal strategy~%")
  (format t "(Group NR) under delayed recall conditions in Palmer and~%")
  (format t "Ornstein's (1971) Experiment 2. EPAM group actually uses~%")
   (format t "the paired rehearsal strategy instead of the no rehearsal~%")
   (format t "strategy.~%")
  (format t " Serial      People      EPAM6~%")
  (format t "Position    --------    --------~%")
  (format t "            CR    NR    CR    PA~%")
  (format t "           ----------  ----------~%")
  (format t "    2      0.38  0.16 ~5,2f ~5,2f~%"
          (nth 0 *overall-results*) (nth 8 *overall-results*))
  (format t "    3      0.31  0.12 ~5,2f ~5,2f~%"
          (nth 1 *overall-results*) (nth 9 *overall-results*))
  (format t "    4      0.27  0.15 ~5,2f ~5,2f~%"
          (nth 2 *overall-results*) (nth 10 *overall-results*))
  (format t "    5      0.09  0.13 ~5,2f ~5,2f~%"
          (nth 3 *overall-results*) (nth 11 *overall-results*))
  (format t "    6      0.19  0.27 ~5,2f ~5,2f~%"
          (nth 4 *overall-results*) (nth 12 *overall-results*))
  (format t "    7      0.21  0.09 ~5,2f ~5,2f~%"
          (nth 5 *overall-results*) (nth 13 *overall-results*))
  (format t "    8      0.12  0.24 ~5,2f ~5,2f~%"
          (nth 6 *overall-results*) (nth 14 *overall-results*))
  (format t "    9      0.16  0.09 ~5,2f ~5,2f~%"
          (nth 7 *overall-results*) (nth 15 *overall-results*))
  )



;;                     Delayed Recall Simulations

(defun peterson-and-peterson-simulation ()
  "This is a simulation of delayed recall experiments in the Peterson and
Peterson paradigm.  The strategy employed by the simulation does not include
any rehearsal even though the conditions employed in delayed recall
experiments sometimes permit occasional rehearsals."
  (setf *overall-results* nil)
  (setf *list-length* 3)
  (setf *presentation-rate* 250)
  (setf *suppression-delay* 250)
  (setf *print-time-updates* nil) 
  (setf *print-imagery-stores* nil) 
  (setf *print-experimenter-updates* nil) 
  (setf *print-epam-updates* nil) 
  (setf *print-strategy-updates* nil)
  (setf *print-forgetting-updates* nil)
  (setf *print-major-updates* nil)
  (setf *method-for-counting-syllables* 'character-method)
  (setf *number-of-sessions* 1000)
  (setf *time-allowed-for-recall* 14000)
  (setf *time-to-enter-an-object* 120)
  (setf *time-to-enter-a-syllable* 120)
  (setf *output-from-ltm* t)
  (setf *output-from-echo* t)
  (peterson-and-peterson)
  (format t "~%Correct recalls of all three letters for human subjects and for~%")
  (format t "           EPAM as a function of retention interval.~%")
  (format t "Data for people is from Peterson & Peterson's (1959) Experiment 1.~%")
  (format t "Interval    People     EPAM6~%")
  (format t "   3         0.78       ~5,2f~%" (nth 0 *overall-results*))
  (format t "   6         0.56       ~5,2f~%" (nth 8 *overall-results*))
  (format t "   9         0.33       ~5,2f~%" (nth 16 *overall-results*))
  (format t "  12         0.23       ~5,2f~%" (nth 24 *overall-results*))
  (format t "  15         0.13       ~5,2f~%" (nth 32 *overall-results*))
  (format t "  18         0.10       ~5,2f~%" (nth 40 *overall-results*))
  (format t "~%Correct recalls of all three letters for human subjects and for~%")
  (format t "           EPAM as cumulative functions of latency.~%")
  (format t "Data for people is from Peterson & Peterson's (1959) Experiment 1.~%")
  (format t "Latencies         People                       EPAM6~%")
  (format t "        ----------------------------- -----------------------------~%")
  (format t "         3    6    9    12   15   18   3    6    9    12   15   18~%")
  (format t "   2    0.32 0.20 0.12 0.04 0.03 0.04~5,2f~5,2f~5,2f~5,2f~5,2f~5,2f~%" 
          (nth 1 *overall-results*) (nth 9 *overall-results*) (nth 17 *overall-results*) 
          (nth 25 *overall-results*) (nth 33 *overall-results*) (nth 41 *overall-results*))
  (format t "   4    0.67 0.52 0.29 0.18 0.10 0.06~5,2f~5,2f~5,2f~5,2f~5,2f~5,2f~%" 
          (nth 2 *overall-results*) (nth 10 *overall-results*) (nth 18 *overall-results*) 
          (nth 26 *overall-results*) (nth 34 *overall-results*) (nth 42 *overall-results*))
  (format t "   6    0.73 0.55 0.31 0.21 0.11 0.07~5,2f~5,2f~5,2f~5,2f~5,2f~5,2f~%" 
          (nth 3 *overall-results*) (nth 11 *overall-results*) (nth 19 *overall-results*) 
          (nth 27 *overall-results*) (nth 35 *overall-results*) (nth 43 *overall-results*))
  (format t "   8    0.77 0.56 0.33 0.21 0.12 0.08~5,2f~5,2f~5,2f~5,2f~5,2f~5,2f~%" 
          (nth 4 *overall-results*) (nth 12 *overall-results*) (nth 20 *overall-results*) 
          (nth 28 *overall-results*) (nth 36 *overall-results*) (nth 44 *overall-results*))
  (format t "  10    0.77 0.56 0.33 0.21 0.12 0.09~5,2f~5,2f~5,2f~5,2f~5,2f~5,2f~%" 
          (nth 5 *overall-results*) (nth 13 *overall-results*) (nth 21 *overall-results*) 
          (nth 29 *overall-results*) (nth 37 *overall-results*) (nth 45 *overall-results*))
  (format t "  12    0.78 0.56 0.33 0.23 0.13 0.10~5,2f~5,2f~5,2f~5,2f~5,2f~5,2f~%" 
          (nth 6 *overall-results*) (nth 14 *overall-results*) (nth 22 *overall-results*) 
          (nth 30 *overall-results*) (nth 38 *overall-results*) (nth 46 *overall-results*))
  (format t "  14    0.78 0.56 0.33 0.23 0.13 0.10~5,2f~5,2f~5,2f~5,2f~5,2f~5,2f~%" 
          (nth 7 *overall-results*) (nth 15 *overall-results*) (nth 23 *overall-results*) 
          (nth 31 *overall-results*) (nth 39 *overall-results*) (nth 47 *overall-results*))
  )

(defun peterson-and-peterson ()
  (setf *errors-array* (make-array 18))
  (seed-random-generator)
  (initialize-variables)
  (study-auditory-vocabulary)
  (associate-spoken-digits-with-digits)
  (setf *net-list* (convert-tree-to-list)) ; This saves net to *net-list*  
  (setf *experimenter* 'delayed-recall-experimenter)
  (setf *stimulus-duration-rate* 0)
  (increment-activation)
  (loop repeat *number-of-sessions*
        do (restore-net-from-file) ; This restores net from *net-list*
        (increment-activation)
        (peterson-and-peterson-practice-trials)
        (loop repeat 8
              do (peterson-and-peterson-experiment)))
(loop for delay in '(3 6 9 12 15 18)
        for result-list = (safe-list (aref *errors-array* (1- delay)))
        for total = (length result-list)
        for hits = (loop for hit in result-list
                         count hit)
        do (format t "There were ~a trials.~%" total)
        (format t "Percent hits for ~a second delay = ~a%~%"
                   delay (round (* 100.0 (/ hits total))))
        (setf *overall-results* 
              (append *overall-results* (list (/ hits total))))
        (loop for latency in '(2000 4000 6000 8000 10000 12000 14000)
              for cumulative-hits = (loop for hit in result-list
                                          if (and hit (<= hit latency))
                                          count hit)
              do (format t "    After latency = ~a msec. = ~a%~%"
                         latency (round (* 100.0 (/ cumulative-hits total))))
              (setf *overall-results* 
                    (append *overall-results* (list (/ cumulative-hits total))))
              finally (terpri))
        finally (terpri)))


(defun posner-and-konick-simulation ()
  "This is a simulation of the fourth experiment conducted by Posner and
Konick in 1966 and reported in the Journal of Experimental Psychology, Volume
72 Number 2 pages 221-231. The simulation compares low and high similarity
stimuli."
   (setf *overall-results* nil)
   (setf *list-length* 3)
   (setf *presentation-rate* 500)
   (setf *suppression-delay* 500)
   (setf *print-time-updates* nil) 
   (setf *print-imagery-stores* nil) 
   (setf *print-experimenter-updates* nil) 
   (setf *print-epam-updates* nil) 
   (setf *print-strategy-updates* nil)
   (setf *print-forgetting-updates* nil)
   (setf *print-major-updates* nil)
   (setf *method-for-counting-syllables* 'character-method)
   (setf *number-of-sessions* 1000)
   (setf *time-allowed-for-recall* 20000)
   (setf *time-to-enter-an-object* 120)
   (setf *time-to-enter-a-syllable* 120)
   (setf *output-from-ltm* t)
   (setf *output-from-echo* t)
   (setf *stimulus-lists* (list *posner-and-konick-high-similarity*
                            *posner-and-konick-low-similarity*))
   (posner-and-konick))

(defun posner-and-konick ()
   (seed-random-generator)
   (initialize-variables)
   (study-auditory-vocabulary)
   (associate-spoken-digits-with-digits)
   (setf *net-list* (convert-tree-to-list))
   (setf *experimenter* 'delayed-recall-experimenter)
   (setf *stimulus-duration-rate* 0)
   (loop for stimulus-list in *stimulus-lists*
     for stimulus-is-a in '(high-similarity low-similarity)
     do (setf *errors-array* (make-array 21))
     (increment-activation)
     (loop repeat *number-of-sessions*
       do (restore-net-from-file)
       (increment-activation)
       (loop repeat 3
         do (posner-and-konick-experiment stimulus-list)))
     (loop for delay in '(0 5 10 20)
       for result-list = (safe-list (aref *errors-array* delay))
       for total = (length result-list)
       for hits = (loop for hit in result-list
                    count hit)
       do (format t "For ~a there were ~a trials.~%" stimulus-is-a total)
       (format t "Percent hits for ~a second delay = ~a%~%"
         delay (round (* 100.0 (/ hits total))))
       (setf *overall-results*
             (append *overall-results* (list (/ hits total))))
       finally (terpri)))
   (format t "Mean Percentage of Items Correct as a function of Delay for~%")
   (format t "high-similarity letters and low-similarity letters.~%")
   (format t "Human data is from Posner & Konick, 1966, Journal of~%")
   (format t "Experimental Psychology, Vol 72, 221-231 and is estimated~%")
   (format t "from Figure 4 on page 228 using their most strenuous~%") 
   (format t "interfering task -- classification of digit pairs into~%")
   (format t "high or low and odd or even -- as the interfering~%")
   (format t "task during retention interval.~%~%")
   (format t "Interval     People       EPAM~%")
   (format t "           ----------  ----------~%")
   (format t "           High  Low   High  Low~%")
   (format t "           ----------  ----------~%")
   (format t "    0      0.93  0.99 ~5,2f ~5,2f~%"
     (nth 0 *overall-results*) (nth 4 *overall-results*))
   (format t "    5      0.78  0.90 ~5,2f ~5,2f~%"
     (nth 1 *overall-results*) (nth 5 *overall-results*))
   (format t "   10      0.77  0.84 ~5,2f ~5,2f~%"
     (nth 2 *overall-results*) (nth 6 *overall-results*))
   (format t "   20      0.71  0.84 ~5,2f ~5,2f~%"
     (nth 3 *overall-results*) (nth 7 *overall-results*)))
     

(defun auditory-turvey-brick-and-osborn-simulation ()
  (setf *list-length* 3)
  (setf *presentation-rate* 250)
  (setf *suppression-delay* 250)
  (setf *print-time-updates* nil) 
  (setf *print-imagery-stores* nil) 
  (setf *print-experimenter-updates* nil) 
  (setf *print-epam-updates* nil) 
  (setf *print-strategy-updates* nil)
  (setf *print-forgetting-updates* nil)
  (setf *print-major-updates* nil)
  (setf *method-for-counting-syllables* 'character-method)
  (setf *number-of-sessions* 1000)
  (setf *time-allowed-for-recall* 10000)
  (setf *time-to-enter-an-object* 120)
  (setf *time-to-enter-a-syllable* 120)
  (setf *output-from-ltm* t)
  (setf *output-from-echo* t)
  (setf *stimulus-lists* *richman-and-simon-experiment-lists*)
  (auditory-turvey-brick-and-osborn))


(defun auditory-turvey-brick-and-osborn ()
  (setf *errors-array* (make-array '(5 25)))
  (seed-random-generator)
  (initialize-variables)
  (study-auditory-vocabulary)
  (associate-spoken-digits-with-digits)
  (setf *net-list* (convert-tree-to-list))
  (setf *experimenter* 'delayed-recall-experimenter)
  (setf *stimulus-duration-rate* 0)
  (increment-activation)
  (loop repeat *number-of-sessions*
        do (loop for delay in '(5 10 15 20 25)
                 do (restore-net-from-file)
                 (increment-activation)
                 (auditory-turvey-brick-and-osborn-experiment delay)))
  (loop for delay in '(5 10 15 20 25)
        do (format t "~%Results for ~a second delay:" delay)
        (loop for i from 1 to 5
              for result-list = (safe-list (aref *errors-array* (1- i) (1- delay)))
              for total = (length result-list)
              for hits = (loop for hit in result-list
                               count hit)
              for percent-hits = (round (* 100.0 (/ hits total)))
              do (format t "For ~a sessions of trial ~a there were ~a% hits~%" 
                         total i percent-hits)
              finally (terpri))))




(defun keppel-and-underwood-simulation ()
  (setf *list-length* 3)
  (setf *presentation-rate* 250)
  (setf *suppression-delay* 250)
  (setf *print-major-updates* nil)
  (setf *print-time-updates* nil) 
  (setf *print-imagery-stores* nil) 
  (setf *print-experimenter-updates* nil) 
  (setf *print-epam-updates* nil) 
  (setf *print-strategy-updates* nil)
  (setf *print-forgetting-updates* nil)
  (setf *stimulus-lists* *keppel-and-underwood-lists*)
  (setf *method-for-counting-syllables* 'character-method)
  (setf *number-of-sessions* 1000)
  (setf *time-allowed-for-recall* 14000)
  (setf *time-to-enter-an-object* 120)
  (setf *time-to-enter-a-syllable* 120)
  (setf *output-from-ltm* t)
  (setf *output-from-echo* t)
  (keppel-and-underwood)
  )
  
(defun keppel-and-underwood ()
  (setf *overall-results* nil)
  (setf *errors-array* (make-array '(3 18)))
  (seed-random-generator)
  (initialize-variables)
  (study-auditory-vocabulary)
  (associate-spoken-digits-with-digits)
  (setf *net-list* (convert-tree-to-list))
  (setf *stimulus-duration-rate* 0)
  (setf *experimenter* 'delayed-recall-experimenter)
  (loop repeat *number-of-sessions*
        do (restore-net-from-file)
        (keppel-and-underwood-experiment))
  (loop for delay in '(3 9 18)
        do (format t "~%Results for ~a second delay:" delay)
        (loop for i from 1 to 3
              for result-list = (safe-list (aref *errors-array* (1- i) (1- delay)))
              for total = (length result-list)
              for hits = (loop for hit in result-list
                               count hit)
              for percent-hits = (round (* 100.0 (/ hits total)))
              do (format t " ~a%" percent-hits)
              (setf *overall-results*
                    (append *overall-results* (list (/ hits total))))
              finally (terpri)))
  (format t "~%Retention of three-letter consonant syllables as~%")
  (format t "a function of length of interval and number of prior~%")
  (format t "trials for human subjects from Keppel & Underwood, 1962,~%")
  (format t "Experiment 1 and from EPAM~%")
  (format t "Interval          People                    EPAM6~%")
  (format t "           ----------------------   ----------------------~%")
  (format t "           Number of Prior Trials   Number of Prior Trials~%")
  (format t "           ----------------------   ----------------------~%")
  (format t "              0      1       2         0      1       2 ~%")      
  (format t "           ----------------------   ----------------------~%")
  (format t "   3         0.82   0.73    0.50     ~5,2f  ~5,2f   ~5,2f~%"
          (nth 0 *overall-results*) (nth 1 *overall-results*) (nth 2 *overall-results*))
  (format t "   9         0.55   0.20    0.23     ~5,2f  ~5,2f   ~5,2f~%"
          (nth 3 *overall-results*) (nth 4 *overall-results*) (nth 5 *overall-results*))
  (format t "  18         0.47   0.29    0.06     ~5,2f  ~5,2f   ~5,2f~%"
          (nth 6 *overall-results*) (nth 7 *overall-results*) (nth 8 *overall-results*))
)


(defun wickens-born-and-allen-simulation ()
  "This experiment measures release from proactive inhibition."
  (setf *overall-results* nil)
  (setf *list-length* 3)
  (setf *presentation-rate* 250)
  (setf *suppression-delay* 250)
  (setf *print-major-updates* nil)
  (setf *print-time-updates* nil) 
  (setf *print-imagery-stores* nil) 
  (setf *print-experimenter-updates* nil) 
  (setf *print-epam-updates* nil) 
  (setf *print-strategy-updates* nil)
  (setf *print-forgetting-updates* nil)
  (setf *method-for-counting-syllables* 'character-method)
  (setf *number-of-sessions* 1000)
  (setf *time-allowed-for-recall* 10000)
  (setf *time-to-enter-an-object* 120)
  (setf *time-to-enter-a-syllable* 120)
  (setf *output-from-ltm* t)
  (setf *output-from-echo* t)
  (setf *signal-to-respond-delay* 10000)
  (wickens-born-and-allen)
  (format t "~%Proportion correct recall as a function of number of~%")
  (format t "trials when an old class of stimuli is used or when there is a~%")
  (format t "switch to a new class of stimuli.  Human subject data is from~%")
  (format t "Wickens, Born, and Allen (1963).~%")
  (format t "Trials           People                 EPAM6~%")
  (format t "        ----------------------  ----------------------~%")
  (format t "          Numbers     Letters     Numbers     Letters~%")
  (format t "        ----------  ----------  ----------  ----------~%")
  (format t "         Old  New    Old  New    Old  New    Old  New~%")     
  (format t "        ----------  ----------  ----------  ----------~%")
  (format t "   1    0.71 0.71   0.68 0.68  ~5,2f~5,2f  ~5,2f~5,2f~%"
          (/ (+ (nth 0 *overall-results*) (nth 12 *overall-results*) (nth 20 *overall-results*) 
                (nth 28 *overall-results*) (nth 36 *overall-results*)) 5)
          (/ (+ (nth 0 *overall-results*) (nth 12 *overall-results*) (nth 20 *overall-results*) 
                (nth 28 *overall-results*) (nth 36 *overall-results*)) 5)
          (/ (+ (nth 4 *overall-results*) (nth 8 *overall-results*) (nth 16 *overall-results*) 
                (nth 24 *overall-results*) (nth 32 *overall-results*)) 5)
          (/ (+ (nth 4 *overall-results*) (nth 8 *overall-results*) (nth 16 *overall-results*) 
                (nth 24 *overall-results*) (nth 32 *overall-results*)) 5))
  (format t "   4    0.38 0.58   0.10 0.67  ~5,2f~5,2f  ~5,2f~5,2f~%"
          (/ (+ (nth 21 *overall-results*) (nth 29 *overall-results*) (nth 37 *overall-results*)) 3)
          (nth 9 *overall-results*)
          (/ (+ (nth 17 *overall-results*) (nth 25 *overall-results*) (nth 33 *overall-results*)) 3)
          (nth 13 *overall-results*))
  (format t "   7    0.46 0.66   0.09 0.54  ~5,2f~5,2f  ~5,2f~5,2f~%"
          (/ (+ (nth 30 *overall-results*) (nth 38 *overall-results*)) 2)
          (nth 18 *overall-results*)
          (/ (+ (nth 26 *overall-results*) (nth 34 *overall-results*)) 2)
          (nth 22 *overall-results*))
  (format t "  10    0.46 0.79   0.21 0.67  ~5,2f~5,2f  ~5,2f~5,2f~%"
          (nth 39 *overall-results*)
          (nth 27 *overall-results*)
          (nth 35 *overall-results*)
          (nth 31 *overall-results*)))



(defun wickens-born-and-allen ()
  (setf *errors-array* (make-array '(10 10)))
  (seed-random-generator)
  (initialize-variables)
  (study-auditory-vocabulary)
  (associate-spoken-digits-with-digits)
  (setf *net-list* (convert-tree-to-list))
  (setf *stimulus-duration-rate* 0)
  (setf *experimenter* 'delayed-recall-experimenter)
  (loop repeat *number-of-sessions*
        do (restore-net-from-file)
        (wickens-born-and-allen-experiment))
  (loop for condition from 1 to 10
        do (format t "~%Results for condition ~a:~%" condition)
        (loop for i in '(1 4 7 10)
              for result-list = (safe-list (aref *errors-array* (1- i) (1- condition)))
              for total = (length result-list)
              for hits = (loop for hit in result-list
                               count hit)
              for percent-hits = (when (> total 0)
                                   (/ hits total))
              if total
              do (format t "    ~a trials in position ~a = ~a%~%" 
                         total i percent-hits)
              (setf *overall-results*
                    (append *overall-results* (list percent-hits)))
              finally (terpri))))

; master2641.lsp
; expanded selected verbal learning simulations so that they would each be run 
; a number of times and give average data.



; master2640.lsp
; This is a continuation of master2.lsp.  Changes are the following:
;   1. added (setf *guessing-routine* nil) to paired associate simulations
;   2. replaced (paired-associate-strategy nil) with (paired-associate-strategy 'episode)
;   3. set *length-of-beginning* and *length-of-group-2* at 3 when not using auditory-cv-parts
;      in intralist-similarity so that the system would know if it couldn't recognize
;      the entire object.

;;;;;;                 7. MASTER ROUTINES



;;                  Verbal Learning Simuations


(defun prepare-net-for-is-a-of-recoding (syllable-list)
  (cond 
   ((eql *is-a-of-recoding* 'vc)
    (setf *avlist* nil)
    (setf *valist* nil)
    (make-auditory-vc-parts syllable-list)
    (associate-word-parts *auditory-cv-parts* 
                          *auditory-cv-counterparts*))
   ((eql *is-a-of-recoding* 'cv)
    (setf *avlist* nil)
    (setf *valist* nil)
    (make-auditory-cv-parts syllable-list)
    (associate-word-parts *auditory-cv-parts*
                          *auditory-cv-counterparts*))
   ((eql *is-a-of-recoding* 'spoken-letter-names) 
    (setf *avlist* nil)
    (setf *valist* nil)
    (associate-visual-and-auditory-letters))
   ((eql *is-a-of-recoding* 'one-letter-one-sound)
    (setf *avlist* nil)
    (setf *valist* nil)
    (make-auditory-one-letter-one-sound-parts)
    (associate-word-parts *auditory-cv-parts*
                          *auditory-cv-counterparts*))
   ('else (format t "*is-a-of-recoding* = ~a is not supported." *is-a-of-recoding*))))


(defun intralist-similarity-simulation ()
  "This routine performs a simulation of Underwood's (1953) study of intralist
similarity.  The results will replicate the results found in Simon and 
Feigenbaum (1964).  See Models of Thought Volume I by Simon reprinted from 
in the J. Verb. Learn. Verb. Behav. Vol. 3, 385-396.  Several parameters can
be changed by changing the commands of this routine.  If *ignore-new-stimulus*
is t, then the system will utilize the One-at-a-time strategy described in 
Gregg and Simon's 1967 paper.  Also the *stimulus-duration-rate* is the
amount of time the stimulus is exposed before the response enters the picture.
The *response-duration-rate* is the amount of time the stimulus and response are
exposed together before they both disappear. The *presentation-rate* should be
at least as long as the *stimulus-duration-rate* plus the *response-duration-
rate*.  It controls the amount of time for the total presentation of stimulus
and response plus a blank time after before presentation of the next stimulus.
The *inter-trial-interval* is the amount of time after the presentation of one
list before the presentation of the next list begins.  If *use-aural-chunks*
is set to t, the system recodes a stimulus into two parts one of which 
represents the aural-encoding of the initial consonant-vowell pair."
  (setf *total-trials-by-list* nil)
  (setf *syllable-lists* (list 
                          *low-low* 
                          *low-medium*
                          *low-high*
                          *medium-low*
                          *high-low*))
  (setf *names-of-syllable-lists* '(
                                    *low-low*
                                    *low-medium* 
                                    *low-high*
                                    *medium-low*
                                    *high-low*))
  (setf *presentation-rate* 4000)
  (setf *stimulus-duration-rate* 2000)
  (setf *response-duration-rate* 2000)
  (setf *inter-trial-interval* 4000)
  (setf *ignore-new-stimulus* nil)
  (setf *print-time-updates* nil) 
  (setf *print-imagery-stores* nil) 
  (setf *print-experimenter-updates* nil) 
  (setf *print-epam-updates* nil) 
  (setf *print-strategy-updates* nil)
  (setf *print-forgetting-updates* nil)
  (setf *print-net-updates* nil)
  (setf *method-for-counting-syllables* 'character-method)
  (setf *indexes* '(1))
  (setf *index* 1)
  (setf *number-of-sessions* 1000)
  (setf *guessing-routine* nil)
  (setf *pa-exit-criteria* 'consecutive-trials-criteria)
  (setf *pa-exit-value* 1)
  (setf *stimulus-is-a* 'syllable)
  (setf *response-is-a* 'syllable)
  (setf *is-a-of-recoding* 'one-letter-one-sound)
  (format t "Intralist Similarity Simulation without regrouping:~%")
  (intralist-similarity)
  (format t "Intralist Similarity Simulation with regrouping of initial consonant-vowel pairs:~%")
  (setf *is-a-of-recoding* 'cv)
  (intralist-similarity)
  (format t "Mean Trials to Learn as a Function of Stimulus and Response Similarity:~%")
  (format t "Results for people from Underwood, 1953, Journal of Experimental Psychology, 45, 133-142:~%")
  (format t "Results for EPAM III from Simon & Feigenbaum, 1964, Models of Thought 3.2~%")
  (format t "Normal signifies EPAM's normal mode, CV-grouping signifies that the initial~%")
  (format t "consonant-vowel were grouped at input.~%")
  (format t "Condition      People      EPAM3         EPAM6           EPAM6~%")
  (format t "                       Normal   CV       Normal       CV-grouping~%")
  (format t "           ------------ ------------  -------------  --------------~%")
  (format t "Low-Low      23.2  100   100   100    ~5,1f ~5,0f    ~5,1f ~5,0f~%"
          (nth 0 *total-trials-by-list*)
          (* 100 (/ (nth 0 *total-trials-by-list*) (nth 0 *total-trials-by-list*)))
          (nth 5 *total-trials-by-list*)
          (* 100.0 (/ (nth 5 *total-trials-by-list*) (nth 5 *total-trials-by-list*))))
  (format t "Low-Medium   22.4   96    88   100    ~5,1f ~5,0f    ~5,1f ~5,0f~%"
          (nth 1 *total-trials-by-list*)
          (* 100 (/ (nth 1 *total-trials-by-list*) (nth 0 *total-trials-by-list*)))
          (nth 6 *total-trials-by-list*)
          (* 100.0 (/ (nth 6 *total-trials-by-list*) (nth 5 *total-trials-by-list*))))
  (format t "Low-High     24.4  105    91   100    ~5,1f ~5,0f    ~5,1f ~5,0f~%"
          (nth 2 *total-trials-by-list*)
          (* 100 (/ (nth 2 *total-trials-by-list*) (nth 0 *total-trials-by-list*)))
          (nth 7 *total-trials-by-list*)
          (* 100.0 (/ (nth 7 *total-trials-by-list*) (nth 5 *total-trials-by-list*))))
  (format t "Medium-Low   25.5  110   140   100    ~5,1f ~5,0f    ~5,1f ~5,0f~%"
          (nth 3 *total-trials-by-list*)
          (* 100 (/ (nth 3 *total-trials-by-list*) (nth 0 *total-trials-by-list*)))
          (nth 8 *total-trials-by-list*)
          (* 100.0 (/ (nth 8 *total-trials-by-list*) (nth 5 *total-trials-by-list*))))
  (format t "High-Low     30.7  132   146   114    ~5,1f ~5,0f    ~5,1f ~5,0f~%"
          (nth 4 *total-trials-by-list*)
          (* 100 (/ (nth 4 *total-trials-by-list*) (nth 0 *total-trials-by-list*)))
          (nth 9 *total-trials-by-list*)
          (* 100.0 (/ (nth 9 *total-trials-by-list*) (nth 5 *total-trials-by-list*))))
  )


(defun intralist-similarity ()
  (seed-random-generator)
  (loop for syllable-list in *syllable-lists*
        for list-name in *names-of-syllable-lists*
        do 
        (reset-data-collection-arrays)
        (setf *noticing-order* nil)
        (setf *consecutive-trials-correct* 0)
        (setf (aref *track-response-latencies* 1) t)
        (initialize-results-arrays)
        (setf (aref *number-of-pairs* *index*) (/ (length syllable-list) 2))
        (format t "~%~%BEGINNING SIMULATION FOR LIST: ~a:~%" list-name)
        (loop repeat *number-of-sessions*
              do
              (setf (aref *response-record* *index*) nil)
              (setf (aref *actual-response-record* *index*) nil)
              (setf (aref *recognition-record* *index*) nil)
              (setf (aref *latency-record* *index*) nil)
              (initialize-variables)
              (prepare-net-for-is-a-of-recoding syllable-list)
              (setf *sr-list* (change-syllables-to-srlist syllable-list 'visual)
                    *method-for-counting-syllables* 'character-method
                    *experimenter* 'underwood-experimenter)
              (zero-out-variables)
              (paired-associate-strategy 'episode *is-a-of-recoding*)
              (setf (aref *errors-on-list* *index*)
                    (+ (aref *errors-on-list* *index*) *errors*))
              (setf (aref *trials-on-list* *index*)
                    (+ (aref *trials-on-list* *index*) *trial*))
              (when (aref *track-response-latencies* *index*)
                (update-response-latencies))
              (update-response-record))
        (print-paired-associate-results)))
          


(defun effects-of-familiarization-simulation ()
  "This routine performs the simulations of the effects of stimulus and response
familiarization that allow EPAM to simulate Chenzoff's (1962) data.  The
EPAM results were reported in Simon and Feigenbaum's 1964 article.  Each
parameter pair defines the number of times a familiarization-operation is
to be performed on each stimulus syllable followed by the number of times 
it is to be performed on the response syllable.  This familiarization takes
place prior to the presentation of the stimulus-response pairs in usual
paired associate form.  The paramebers (3 3) define the F-F condition of
the 1964 simulation while (0 3) defines the U-F condition, (3 0) defines the
F-U condition and (0 0) defines the U-U condition."
   (setf *errors-by-list* nil)
   (setf *syllable-list* *low-low*)
   (setf *stimfam-and-respfams* '((6 6) (0 6) (6 0) (0 0)))
   (setf *presentation-rate* 4000)
   (setf *stimulus-duration-rate* 2000)
   (setf *response-duration-rate* 2000)
   (setf *inter-trial-interval* 0)
   (setf *ignore-new-stimulus* nil)
   (setf *print-time-updates* nil)
   (setf *print-imagery-stores* nil)
   (setf *print-experimenter-updates* nil) 
   (setf *print-epam-updates* nil)
   (setf *print-strategy-updates* nil)
   (setf *print-forgetting-updates* nil)
   (setf *print-net-updates* nil)
   (setf *print-major-updates* t)
   (setf *guessing-routine* nil)
   (setf *pa-exit-criteria* 'consecutive-trials-criteria)
   (setf *pa-exit-value* 1)
   (setf *stimulus-is-a* 'syllable)
   (setf *response-is-a* 'syllable)
   (setf *is-a-of-recoding* 'one-letter-one-sound)
   (setf *method-for-counting-syllables* 'character-method)
   (setf *indexes* '(1))
   (setf *index* 1)
   (setf *number-of-sessions* 1000)
   (setf *method-for-counting-syllables* 'character-method
         *experimenter* 'underwood-experimenter)
   (effects-of-familiarization)
   (format t "~%Effects of Stimulus and Response Familiarization.~%")
   (format t "Human data is from Chenzoff. 1962, as analyzed in Models~%")
   (format t "of Thought, Chapter 3.2.  EPAM3 data is from the same chapter~%")
   (format t "Data is the reciprocal of errors in ratio to the F-F condition.~%~%")
   (format t "Condition      People       EPAM3        EPAM6~%")
   (format t "   F-F           1.0         1.0        ~5,1f~%"
     (/ (nth 0 *errors-by-list*) (nth 0 *errors-by-list*)))
   (format t "   U-F           1.2         1.3        ~5,1f~%"
     (/ (nth 1 *errors-by-list*) (nth 0 *errors-by-list*)))
   (format t "   F-U           1.6         1.8        ~5,1f~%"
     (/ (nth 2 *errors-by-list*) (nth 0 *errors-by-list*)))
   (format t "   U-U           1.8         2.5        ~5,1f~%"
     (/ (nth 3 *errors-by-list*) (nth 0 *errors-by-list*)))
   )
  
    
(defun effects-of-familiarization ()
  (seed-random-generator)
  (loop for parameter-list in *stimfam-and-respfams*
        for stimfams = (first parameter-list)
        for respfams = (second parameter-list)
        do 
        (setf *sr-list* (change-syllables-to-srlist *syllable-list* 'visual))
        (reset-data-collection-arrays)
        (setf *noticing-order* nil)
        (setf *consecutive-trials-correct* 0)
        (setf (aref *track-response-latencies* 1) t)
        (initialize-results-arrays)
        (setf (aref *number-of-pairs* *index*) (/ (length *syllable-list*) 2))
        (format t "~%~%BEGINNING SIMULATION FOR STIMULUS FAMILIARIZATION = ~a, RESPONSE FAMILIARIZATION = ~a:~%" stimfams respfams)
        (loop repeat *number-of-sessions*
              with s
              with r
              do
              (setf (aref *response-record* *index*) nil)
              (setf (aref *actual-response-record* *index*) nil)
              (setf (aref *recognition-record* *index*) nil)
              (setf (aref *latency-record* *index*) nil)
              (initialize-variables)
              (prepare-net-for-is-a-of-recoding *syllable-list*)
              (zero-out-variables)
              (setf s stimfams)
              (setf r respfams)
              (loop while (or (plusp s) (plusp r))
                    do 
                    (when (plusp s)
                      (mapcar 
                       #'(lambda (x) 
                           (study (put-value (convert-modality-of (car x) 'visual 'auditory) 'syllable 'is-a)))
                       *sr-list*))
                    (when (plusp r) 
                      (mapcar 
                       #'(lambda (x) 
                           (study (put-value (convert-modality-of (cadr x) 'visual 'auditory) 'syllable 'is-a)))
                       *sr-list*))
                    (setq s (- s 1))
                    (setq r (- r 1)))
              (setf *experimenter* 'underwood-experimenter)
              (zero-out-variables)
              (paired-associate-strategy 'episode *is-a-of-recoding*)
              (setf (aref *errors-on-list* *index*)
                    (+ (aref *errors-on-list* *index*) *errors*))
              (setf (aref *trials-on-list* *index*)
                    (+ (aref *trials-on-list* *index*) *trial*))
              (when (aref *track-response-latencies* *index*)
                (update-response-latencies))
              (update-response-record))
        (print-paired-associate-results)))
 

(defun trotting-vs-smoking-simulation ()
    "This routine simulates the basic experiment discussed by Simon (1972) in 
 is paper *What is Visual Imagery: An Information-Processing Interpretation"
   (format t "~%Trotting vs. Smoking simulation with 1 trial and 5 second presentation rate:~%")
   (setf *overall-results* nil)
   (setf *syllable-list* (append *list1* *list4*))
   (setf *stimulus-duration-rate* 2000)
   (setf *response-duration-rate* 3000)
   (setf *presentation-rate* 5000)
   (setf *inter-trial-interval* 5000)
   (setf *print-time-updates* nil) 
   (setf *print-imagery-stores* nil) 
   (setf *print-experimenter-updates* nil) 
   (setf *print-epam-updates* nil) 
   (setf *print-strategy-updates* nil)
   (setf *print-forgetting-updates* nil)
   (setf *guessing-routine* nil)
   (setf *pa-exit-criteria* 'absolute-trials-criteria)
   (setf *pa-exit-value* 1)
   (setf *stimulus-is-a* 'syllable)
   (setf *response-is-a* 'syllable)
   (setf *is-a-of-recoding* 'one-letter-one-sound)
   (setf *method-for-counting-syllables* 'character-method)
   (setf *indexes* '(1))
   (setf *index* 1)
   (setf *number-of-sessions* 1000)
   (trotting-vs-smoking))

(defun trotting-vs-smoking ()
   (seed-random-generator)
   (setf *overall-results* nil)
   (setf *sr-list* (change-syllables-to-srlist *syllable-list* 'visual))
   (setf *extra-syllables* nil)
   (setf *create-episode-having-two-slots* nil)
   (let ((strategy 'one-at-a-time))
      (loop for condition in '(smoking trotting)
        do 
        (format t "~%~%BEGINNING SIMULATION FOR ~a CONDITION:~%"
          condition)
        (reset-data-collection-arrays)
        (setf *noticing-order* nil)
        (setf *consecutive-trials-correct* 0)
        (initialize-results-arrays)
        (setf *errors-array* (make-array 10 :initial-element 0))
        (setf (aref *number-of-pairs* *index*) 8)
        (loop repeat *number-of-sessions*
          with s
          with r
          with stimfams = 6
          with respfams = 6
          do
          (setf (aref *response-record* *index*) nil)
          (setf (aref *actual-response-record* *index*) nil)
          (setf (aref *recognition-record* *index*) nil)
          (setf (aref *latency-record* *index*) nil)
          (setf *create-episode-having-two-slots* nil)
          (initialize-variables)
          (prepare-net-for-is-a-of-recoding *syllable-list*)
          (zero-out-variables)
          (setf s stimfams)
          (setf r respfams)
          (loop while (or (plusp s) (plusp r))
            do 
            (when (plusp s)
               (mapcar 
                 #'(lambda (x) 
                   (study (put-value (convert-modality-of (car x) 'visual 'auditory) 'syllable 'is-a)))
                 *sr-list*))
            (when (plusp r) 
               (mapcar 
                 #'(lambda (x) 
                     (study (put-value (convert-modality-of (cadr x) 'visual 'auditory) 'syllable 'is-a)))
                 *sr-list*))
            (setq s (- s 1))
            (setq r (- r 1)))
          (zero-out-variables)
          (setf *experimenter* 'non-replacement-experimenter)
          (setf *ignore-new-stimulus* (eql strategy 'one-at-a-time))
          (when (eql condition 'smoking)
             (setf *create-episode-having-two-slots* t))
          (paired-associate-strategy 'episode *is-a-of-recoding*)
          (setf (aref *errors-on-list* *index*)
                (+ (aref *errors-on-list* *index*) *errors*))
          (setf (aref *trials-on-list* *index*)
                (+ (aref *trials-on-list* *index*) *trial*))
          (when (aref *track-response-latencies* *index*)
             (update-response-latencies))
          (update-response-record))
        (print-paired-associate-results)
        (loop for n from 0 to 0
          initially 
          (format t "PROPORTION IN ERROR BY TRIAL ~a CONDITION:~%"
            condition)
          collect (* .05 (/ (aref *errors-array* n) *number-of-sessions*))
          into errors-by-trial
          finally (format t " ~a~%" errors-by-trial))
        (loop for n from 0 to 0 ; 5 means results from 6th trial will be reported
          initially 
          (format t "PROPORTION CORRECT BY TRIAL PER SUBJECT ~a CONDITION:~%"
            condition)
          for average-error = (/ (aref *errors-array* n) *number-of-sessions*)
          for average-correct = (* .05 (- 20.0 average-error))
          collect average-correct into correct-by-trial
          finally (progn (format t " ~a~%" correct-by-trial)
                    (setf *overall-results* 
                          (append *overall-results* (list average-correct)))))))
   (format t "~%Mental Imagery and Associative Learning Simulation~%")
   (format t "Percentage of immediate recall after 1 trial of learning~%")
   (format t "with a 5 second presentation rate.  Human data is estimated~%")
   (format t "from list 1 of Figure 3.1 of Bower (1972)~%~%")
   (format t "Condition      People       EPAM6~%")
   (format t "Imagery          0.68       ~5,2f~%" (first *overall-results*))
   (format t "Control          0.37       ~5,2f~%~%" (second *overall-results*)))
     
   



(defun one-trial-and-incremental-simulations ()
   (setf *overall-results* nil)
   (fast-one-trial-and-incremental-simulation)
   (slow-one-trial-and-incremental-simulation)
   ; the order of *overall-results* for tenth trial is:
   ;     fast-one-rep fast-one-non fast-all-rep fast-all-non 
   ;     slow-one-rep slow-one-non slow-all-rep slow-all-non
   (format t "~%Results for people are from Gregg et. al, Amer Jrnl Psychol, 1963, 76, 110-115~%")
   (format t "as reanalyzed by strategy by Gregg & Simon Models of Thought I Chapter 3.3.~%")
   (format t "People's results are number correct on Tenth Trial extrapolated to an n of 20.~%")
   (format t "EPAM results are number correct on Sixth Trial extrapolated to an n of 20.~%")
   (format t "Conditions are fast and slow presentation and Replace or Non-replace of incorrect~%")
   (format t "response after error. Strategies for both EPAM and people are 'one-at-a-time'~%") 
   (format t "and 'all-at-once'.~%~%")
   (format t " Condition       People             EPAM6~%")
   (format t "            ------------------    ---------~%")
   (format t "            Overall  One   All    One   All~%")
   (format t "            ------------------    ---------~%")
   (format t "Slow-Replace   57    100    48  ~5,0f ~5,0f~%"
     (nth 4 *overall-results*) (nth 6 *overall-results*))
   (format t "Fast-Replace   59     68    28  ~5,0f ~5,0f~%"
     (nth 0 *overall-results*) (nth 2 *overall-results*))
   (format t "Slow-Keep     146     --    --  ~5,0f ~5,0f~%"
     (nth 5 *overall-results*) (nth 7 *overall-results*))
   (format t "Fast-Keep     110     --    --  ~5,0f ~5,0f~%"
     (nth 1 *overall-results*) (nth 3 *overall-results*)))


(defun fast-one-trial-and-incremental-simulation ()
  (format t "~%One-trial-and-incremental simulation with 5 second presentation rate:~%")
  (setf *extra-syllables* (append *melton-mcqueen-list1*
                                  *melton-mcqueen-list2* 
                                  *melton-mcqueen-list3* 
                                  *melton-mcqueen-list4* 
                                  *melton-mcqueen-list5*
                                  *melton-mcqueen-list6*
                                  *melton-mcqueen-list7*
                                  *melton-mcqueen-list8*
                                  *melton-mcqueen-list9*
                                  *melton-mcqueen-list10*
                                  ))
  (setf *stimulus-duration-rate* 2000)
  (setf *response-duration-rate* 3000)
  (setf *presentation-rate* 5000)
  (setf *inter-trial-interval* 15000)
  (setf *print-time-updates* nil) 
  (setf *print-imagery-stores* nil) 
  (setf *print-experimenter-updates* nil) 
  (setf *print-epam-updates* nil) 
  (setf *print-strategy-updates* nil)
  (setf *print-forgetting-updates* nil)
  (setf *guessing-routine* nil)
  (setf *pa-exit-criteria* 'consecutive-trials-criteria)
  (setf *pa-exit-value* 1)
  (setf *stimulus-is-a* 'syllable)
  (setf *response-is-a* 'syllable)
  (setf *is-a-of-recoding* 'one-letter-one-sound)
  (setf *method-for-counting-syllables* 'character-method)
  (setf *indexes* '(1))
  (setf *index* 1)
  (setf *number-of-sessions* 1000)
  (one-trial-and-incremental))

(defun slow-one-trial-and-incremental-simulation ()
  (format t "~%One-trial-and-incremental simulation with 8 second presentation rate:~%")
  (setf *extra-syllables* (append *melton-mcqueen-list1*
                                  *melton-mcqueen-list2* 
                                  *melton-mcqueen-list3* 
                                  *melton-mcqueen-list4* 
                                  *melton-mcqueen-list5*
                                  *melton-mcqueen-list6*
                                  *melton-mcqueen-list7*
                                  *melton-mcqueen-list8*
                                  *melton-mcqueen-list9*
                                  *melton-mcqueen-list10*
                                  ))
  (setf *stimulus-duration-rate* 3500)
  (setf *response-duration-rate* 4500)
  (setf *presentation-rate* 8000)
  (setf *inter-trial-interval* 15000)
  (setf *print-time-updates* nil) 
  (setf *print-imagery-stores* nil) 
  (setf *print-experimenter-updates* nil) 
  (setf *print-epam-updates* nil) 
  (setf *print-strategy-updates* nil)
  (setf *print-forgetting-updates* nil)
  (setf *guessing-routine* nil)
  (setf *pa-exit-criteria* 'consecutive-trials-criteria)
  (setf *pa-exit-value* 1)
  (setf *stimulus-is-a* 'syllable)
  (setf *response-is-a* 'syllable)
  (setf *is-a-of-recoding* 'one-letter-one-sound)
  (setf *method-for-counting-syllables* 'character-method)
  (setf *indexes* '(1))
  (setf *index* 1)
  (setf *number-of-sessions* 1000)
  (one-trial-and-incremental))


(defun one-trial-and-incremental ()
  (seed-random-generator)
  ; The *sr-list* will be chosen from the *extra-pairs* 
  (loop 
    for strategy in '(one-at-a-time all-at-once)
    ; for strategy in '(all-at-once)
    do (loop 
         for condition in '(replacement non-replacement)
         ; for condition in '(non-replacement)
         do (reset-data-collection-arrays)
         (setf *noticing-order* nil)
         (setf *consecutive-trials-correct* 0)
         (setf (aref *track-response-latencies* 1) t)
         (initialize-results-arrays)
         (setf *errors-array* (make-array 10 :initial-element 0))
         (setf (aref *number-of-pairs* *index*) 8)
         (format t "~%~%BEGINNING SIMULATION FOR ~a STRATEGY, ~a CONDITION:~%"
                 strategy condition)
         (loop repeat *number-of-sessions*
               do
               (setf (aref *response-record* *index*) nil)
               (setf (aref *actual-response-record* *index*) nil)
               (setf (aref *recognition-record* *index*) nil)
               (setf (aref *latency-record* *index*) nil)
               (initialize-variables)
               (prepare-net-for-is-a-of-recoding *extra-syllables*)
               (setf *extra-pairs* (reorder-list-randomly 
                                    (change-syllables-to-srlist 
                                     *extra-syllables* 'visual)))
               (setf *sr-list* (loop repeat 8
                                     collect (pop *extra-pairs*)))
               (zero-out-variables)
               (setf *experimenter* 
                     (if (eq condition 'replacement)
                       'replacement-experimenter
                       'non-replacement-experimenter))
               (setf *ignore-new-stimulus* (eql strategy 'one-at-a-time))
               (paired-associate-strategy 'episode *is-a-of-recoding*)
               (setf (aref *errors-on-list* *index*)
                     (+ (aref *errors-on-list* *index*) *errors*))
               (setf (aref *trials-on-list* *index*)
                     (+ (aref *trials-on-list* *index*) *trial*))
               (when (aref *track-response-latencies* *index*)
                 (update-response-latencies))
               (update-response-record))
         (print-paired-associate-results)
         (loop for n from 0 to 9
               initially 
               (format t "PROPORTION IN ERROR BY TRIAL ~a STRATEGY ~a CONDITION:~%"
                       strategy condition)
               collect (* 1.0 (/ (aref *errors-array* n) *number-of-sessions*))
               into errors-by-trial
               finally (format t " ~a~%" errors-by-trial))
         (loop for n from 0 to 5 ; 5 means results from 6th trial will be reported
               initially 
               (format t "CORRECT BY TRIAL PER 20 SUBJECTS ~a STRATEGY ~a CONDITION:~%"
                       strategy condition)
               for average-error = (/ (aref *errors-array* n) *number-of-sessions*)
               for average-correct = (* 20 (- 8.0 average-error))
               collect average-correct into correct-by-trial
               finally (progn (format t " ~a~%" correct-by-trial)
                              (setf *overall-results* 
                                    (append *overall-results* (list average-correct)))))
         )))


(defun bugelski-simulation ()
  "Bugelski (1962) conducted a study in which he showed that the total learning
time in stimulus-response learning is approximately the same for various
presentation rates."
  (setf *total-trials-by-list* nil)
  (setf *syllable-list* *bugelski*)
  (setf *ignore-new-stimulus* nil)
  (setf *print-time-updates* nil) 
  (setf *print-imagery-stores* nil) 
  (setf *print-experimenter-updates* nil) 
  (setf *print-epam-updates* nil) 
  (setf *print-strategy-updates* nil)
  (setf *print-forgetting-updates* nil)
  (setf *guessing-routine* nil)
  (setf *stimulus-is-a* 'syllable)
  (setf *response-is-a* 'syllable)
  (setf *is-a-of-recoding* 'one-letter-one-sound)
  (setf *method-for-counting-syllables* 'character-method)
  (setf *indexes* '(1))
  (setf *index* 1)
  (setf *number-of-sessions* 1000)
  (setf *pa-exit-criteria* 'consecutive-trials-criteria)
  (setf *pa-exit-value* 2)
  (bugelski)
  (format t "Results for people are from Bugelski, 1962, Journal of Experimental Psychology, 63,409-412.~%~%")
  (format t "Mean Trials and Total Times to Learn Lists.~%")
  (format t "Presentation         People                 EPAM6~%")
  (format t "   Time       ---------------------  ---------------------~%")
  (format t "              Trials  Exposure-Time  Trials  Exposure-Time~%")
  (format t "              ---------------------  ---------------------~%")
  (format t "   6 sec       10.2        61.2      ~5,1f        ~5,1f~%" 
          (1- (nth 4 *total-trials-by-list*)) (* (1- (nth 4 *total-trials-by-list*)) 6))
  (format t "   8 sec        8.8        70.1      ~5,1f        ~5,1f~%"
          (1- (nth 3 *total-trials-by-list*)) (* (1- (nth 3 *total-trials-by-list*)) 8))
  (format t "  10 sec        5.8        57.9      ~5,1f        ~5,1f~%"
          (1- (nth 2 *total-trials-by-list*)) (* (1- (nth 2 *total-trials-by-list*)) 10))
  (format t "  12 sec        4.7        56.1      ~5,1f        ~5,1f~%"
          (1- (nth 1 *total-trials-by-list*)) (* (1- (nth 1 *total-trials-by-list*)) 12))
  (format t "  19 sec        3.3        62.2      ~5,1f        ~5,1f~%"
          (1- (nth 0 *total-trials-by-list*)) (* (1- (nth 0 *total-trials-by-list*)) 19))
  )



(defun bugelski ()
  (seed-random-generator)
  (loop for response-duration-rate in '(15 8 6 4 2)
        for total-presentation-time = (+ response-duration-rate 4)
        do (setf *response-duration-rate* (* 1000 response-duration-rate))
        (setf *stimulus-duration-rate* 2000)
        (setf *inter-trial-interval* 0)
        (setf *presentation-rate* (+ *stimulus-duration-rate*
                                     *response-duration-rate*
                                     2000))
        (reset-data-collection-arrays)
        (setf *noticing-order* nil)
        (setf *consecutive-trials-correct* 0)
        (setf (aref *track-response-latencies* 1) t)
        (initialize-results-arrays)
        (setf (aref *number-of-pairs* *index*) (/ (length *syllable-list*) 2))
        (format t "~%~%BEGINNING SIMULATION FOR PRESENTATION TIME = ~a seconds:~%" total-presentation-time)
        (loop repeat *number-of-sessions*
              do
              (setf (aref *response-record* *index*) nil)
              (setf (aref *actual-response-record* *index*) nil)
              (setf (aref *recognition-record* *index*) nil)
              (setf (aref *latency-record* *index*) nil)
              (initialize-variables)
              (prepare-net-for-is-a-of-recoding *syllable-list*)
              (zero-out-variables)
              (setf *sr-list* (change-syllables-to-srlist *syllable-list* 'visual)
                    *method-for-counting-syllables* 'character-method
                    *experimenter* 'underwood-experimenter)
              (paired-associate-strategy 'episode *is-a-of-recoding*)
              (setf (aref *errors-on-list* *index*)
                    (+ (aref *errors-on-list* *index*) *errors*))
              (setf (aref *trials-on-list* *index*)
                    (+ (aref *trials-on-list* *index*) *trial*))
              (when (aref *track-response-latencies* *index*)
                (update-response-latencies))
              (update-response-record))
        (print-paired-associate-results)
        (format t "TOTAL EXPOSURE TIME WAS ~F SECONDS~%~%"
                (* (1- (/ (aref *trials-on-list* *index*) *number-of-sessions*))
                   total-presentation-time)))) ; note the 1- comes from the fact that
                                               ; exit criterial is 2 trials correct and
                                               ; so must subtract one to find the last
                                               ; imperfect trial.




(defun run-all-paired-associate-simulations ()
   (hintzman-experiment1)
   (hintzman-experiment2)
   (hintzman-experiment3)
   (hintzman-experiment3-with-different-parameter-settings)
   (hintzman-experiment6)
   (hintzman-experiment7)
   (hintzman-experiment8)
   (hintzman-experiment9)
   (hintzman-experiment11)
   (hintzman-experiment12)
   (hintzman-experiment13)
   (hintzman-experiment14)
   (hintzman-experiment16)
   (hintzman-backward-recall-experiment)
   (quick-intralist-similarity-simulation)
   (intralist-similarity-simulation)
   (one-trial-and-incremental-simulations)
   (effects-of-familiarization-simulation)
   (Bruce-Interlist-Similarity-Simulation)
   (bugelski-simulation)
   (anderson-1974)
   (anderson-and-reder-1999)
   (trotting-vs-smoking-simulation)
   (quick-lockhart-simulation)
   (lockhart-regular-intervals-simulation)
   )

(defun run-all-stm-simulations ()
   (peterson-and-peterson-simulation)
   (posner-and-konick-simulation)
   (keppel-and-underwood-simulation)
   (wickens-born-and-allen-simulation)
   (nicolson-and-fawcett-simulation)
   (nicolson-and-fawcett-simulation-without-echo)
   (nicolson-and-fawcett-simulation-without-echo-or-ltm)
   (baddeley-lewis-and-vallar-exp3-simulation)
   (baddeley-lewis-and-vallar-exp3-simulation-without-search)
   (baddeley-lewis-and-vallar-exp3-simulation-without-search-or-echo)
   (baddeley-lewis-and-vallar-exp5-simulation)
   (palmer-and-ornstein-exp1-simulation)
   (palmer-and-ornstein-exp2-simulation)
   )




 
