; exptsim.lsp.  This is the EPAM 6 version of the EPAM memory simulation.
; Feb. 24, 2002, version. Copyright 2002 by Howard B. Richman

;
;;             CONTENTS
;;               A. Variable Declarations
;;               B. Initialization Routines
;;               C. Experimenter Routines
;;               D. Routines that Search Retrieval Structures
;;               E. Routines that Create Retrieval Structures
;;               F. Routines that Answer Questions about Retrieval Structures
;;               G. Experimenter Routines
;;               H. Conversion Routines
;;               I. Depth First Search in Net
;;               J. Study Stage Routines
;;               K. Rehearsal Stage Routines
;;               L. Ordered Recall Stage Routines
;;               M. Free Recall Stage Routines
;;               N. Overall Strategies

;;               A. VARIABLE DECLARATIONS

;;          These parameters affect DD's strategy

(defvar *use-contextual-codes* t) ;; this variable determines whether context cues
                              ;;  such as symmetry's back-to-backs and add-em-ups
                              ;;  will be noted. 

;;          These parameters affect the experimenter's presenations

(defvar *how-many-lists-per-session* 3) ;; this parameter is used to 
                                          ;; determine how many lists should
                                          ;; be presented to DD during a
                                          ;; random digit session.  The
                                          ;; number of lists presented to 
                                          ;; DD per session was gradually
                                          ;; reduced as the list length got
                                          ;; longer.

(defvar *list-number* 0) ; This variable is used to hold a number of a
                         ; list so that information can be added correctly
                         ; to the latency-array that can be used for reporting 
                         ; latency times at the end of the simulation.

(defvar *trial-position* 0) ; This variable is used to hold the number of
                            ; a list so that information can be added correctly
                            ; to the times-array that can be used to report time
                            ; spent in rehearsal and ordered recall and errors
                            ; as a function of trial position within a session.

(defvar *current-list-length* 50) ;; Number of digits that are currently being
                                  ;; presented in simulations in which 
                                  ;; random digits are presented.

;;         These variables collect information for the experimenter

(defvar *record-of-rs-growth* nil)

(defvar *record-of-nodes-in-net* nil)

(defvar *times-array* nil)



(defvar *list-of-errors* nil) ;; this variable holds each group of digits where
                         ;; at least one digit was missed during ordered recall.

(defvar *codes-found* nil) ;; this list holds record of codes found during study stage

(defvar *record-codes-found* nil) ; this variable determines whether codes found will
                                  ; be recorded in the *codes-found* variable.

(defvar *free-recall-found* nil)
(defvar *record-free-recall* nil)

(defvar *forward-latencies-found* nil)
(defvar *backward-latencies-found* nil)

;;       These are miscellaneous lists and variables used in simulation


(defvar *ascending-races* '(quart_m half_m threequart_m one_m))

;; *Initial-episodic-knowledge* is a list which can be converted into the 
;;  beginnings of the episodic nets.  A single element of a category is 
;;  sufficient to begin the net for that category.  Three elements which differ
;;  by first digit are enough to make sure that the first digit is used to 
;;  for the first test in that category.
(defvar *initial-episodic-knowledge*
  '((5 0 0 quart_m) (4 0 0 quart_m) 
    (2 0 0 0 half_m) (1 5 0 0 half_m) (2 5 0 0 half_m)
    (3 0 0 0 threequart_m) (3 1 0 0 threequart_m)
    (3 5 0 0 one_m) (4 5 0 0 one_m) (5 5 0 0 one_m)
    (7 0 0 0 three_k) (7 3 0 0 three_k)
    (8 0 0 0 fast_2m) (9 0 0 0 fast_2m) (1 0 0 0 slow_2m) (1 2 0 0 slow_2m)
    (1 2 0 0 three_m) (1 4 0 0 three_m)
    (2 6 0 0 ten_k) (3 6 0 0 ten_k) (2 6 0 0 ten_k)
    (4 6 0 0 ten_m) (5 6 0 0 ten_m) (6 6 0 0 ten_m)
    (1 7 7 6 date) (1 9 9 2 date)
    (0 0 0 0 misc) (1 1 1 1 misc)
    (2 2 0 age) (3 2 0 age) (2 9 2 2 dbl_age) (8 9 2 2 dbl_age)))


;; Rs-components are the components of retrieval structures that the system
;;   already has at the beginning of the simulation.  The system has been 
;;   given more original components than DD had so that chance would not
;;   lead the system to construct different nets than DD.  This list, when
;;   studied will produce chunks equivalent to the following:
;;    d3 = (digit digit digit)
;;    d4 = (digit digit digit digit)
;;    d5 = (digit digit digit digit digit)
;;    d6 = (d3 d3)
;;    d7 = (d3 d4)
;;    d8 = (d4 d4)
;;    d9 = (d3 d3 d3)
;;    d10 = (d3 d3 d4)
;;    d11 = (d3 d3 d5)
;;    d12 = (d4 d4 d4)
;;    d13 = (d4 d4 d5)
;;    d14 = (d4 d4 d3 d3)
;;    d16 = (d4 d4 d4 d4)
;;    d21 = (d9 d12)
;;    d22 = (d9 d13)
;;    d23 = (d9 d14)
(defvar *rs-components* 
      '((digit digit digit)
        (digit digit digit digit) 
        (digit digit digit digit digit) 
        ((digit digit digit) (digit digit digit))
        ((digit digit digit) (digit digit digit digit))
        ((digit digit digit digit) (digit digit digit digit))
        ((digit digit digit) (digit digit digit) (digit digit digit))
        ((digit digit digit) (digit digit digit) (digit digit digit digit))
        ((digit digit digit) (digit digit digit)
         (digit digit digit digit digit))
        ((digit digit digit digit) (digit digit digit digit)
         (digit digit digit digit))
        ((digit digit digit digit) (digit digit digit digit)
         (digit digit digit digit digit))
        ((digit digit digit digit) (digit digit digit digit) 
         (digit digit digit) (digit digit digit))
        ((digit digit digit digit) (digit digit digit digit)
         (digit digit digit digit) 
         (digit digit digit digit))
        (((digit digit digit) (digit digit digit) (digit digit digit))
         ((digit digit digit digit) (digit digit digit digit) 
          (digit digit digit digit)))
        (((digit digit digit) (digit digit digit) (digit digit digit))
         ((digit digit digit digit) (digit digit digit digit) 
          (digit digit digit digit digit)))
        (((digit digit digit) (digit digit digit) (digit digit digit))
         ((digit digit digit digit) (digit digit digit digit)
          (digit digit digit) (digit digit digit)))))

; These routines are from the February 11 revision called expsim.lsp

(defvar *data-for-categories-net* 
  '(("__" age)
    ("4_" quart_m)
    ("5_" quart_m)
    ("___" age)
    ("0__" age)
    ("1__" age)
    ("2__" age)
    ("3__" age)
    ("4__" one_m)
    ("5__" one_m)
    ("6__" one_m)
    ("7__" three_k)
    ("8__" fast_2m)
    ("9__" fast_2m)
    ("04_" age)
    ("05_" quart_m)
    ("11_" age)
    ("12_" half_m)
    ("13_" half_m)
    ("14_" half_m)
    ("15_" half_m)
    ("20_" half_m)
    ("21_" half_m)
    ("22_" half_m)
    ("23_" half_m)
    ("24_" half_m)
    ("25_" half_m)
    ("30_" threequart_m)
    ("31_" threequart_m)
    ("32_" threequart_m)
    ("33_" threequart_m)
    ("34_" one_m)
    ("35_" one_m)
    ("46_" quart_m)
    ("47_" quart_m)
    ("48_" quart_m)
    ("49_" quart_m)
    ("56_" quart_m)
    ("57_" quart_m)
    ("58_" quart_m)
    ("59_" quart_m)
    ("66_" age)
    ("67_" age)
    ("68_" age)
    ("69_" age)
    ("76_" age)
    ("77_" age)
    ("78_" age)
    ("79_" age)
    ("86_" age)
    ("87_" age)
    ("88_" age)
    ("89_" age)
    ("96_" age)
    ("97_" age)
    ("98_" age)
    ("99_" age)
    ("____" age)
    ("0___" age)
    ("2___" half_m)
    ("3___" threequart_m)
    ("4___" one_m)
    ("5___" one_m)
    ("6___" one_m)
    ("7___" three_k)
    ("8___" fast_2m)
    ("9___" fast_2m)
    ("10__" date)
    ("11__" slow_2m)
    ("12__" three_m)
    ("13__" three_m)
    ("14__" three_m)
    ("15__" three_m)
    ("16__" date)
    ("17__" date)
    ("18__" date)
    ("19__" date)
    ("100_" slow_2m)
    ("101_" slow_2m)
    ("102_" slow_2m)
    ("103_" slow_2m)
    ("104_" slow_2m)
    ("105_" slow_2m)
    ("109_" slow_2m)
    ("111_" misc)
    ("116_" date)
    ("117_" date)
    ("118_" date)
    ("126_" date)
    ("127_" date)
    ("128_" date)
    ("129_" date)
    ("136_" date)
    ("137_" date)
    ("138_" date)
    ("139_" date)
    ("146_" date)
    ("147_" date)
    ("148_" date)
    ("149_" date)
    ("156_" date)
    ("157_" date)
    ("158_" date)
    ("159_" date)
    ("26__" ten_k)
    ("27__" ten_k)
    ("28__" ten_k)
    ("29__" ten_k)
    ("34__" threequart_m)
    ("35__" one_m)
    ("36__" ten_k)
    ("37__" ten_k)
    ("38__" ten_k)
    ("39__" ten_k)
    ("346_" one_m)
    ("347_" one_m)
    ("348_" one_m)
    ("349_" one_m)
    ("46__" ten_m)
    ("47__" ten_m)
    ("48__" ten_m)
    ("49__" ten_m)
    ("468_" misc)
    ("466_" dbl_age)
    ("467_" dbl_age)
    ("488_" dbl_age)
    ("56__" ten_m)
    ("57__" ten_m)
    ("58__" ten_m)
    ("59__" ten_m)
    ("576_" dbl_age)
    ("578_" dbl_age)
    ("586_" dbl_age)
    ("588_" dbl_age)
    ("596_" dbl_age)
    ("597_" dbl_age)
    ("598_" dbl_age)
    ("566_" dbl_age)
    ("567_" dbl_age)
    ("568_" dbl_age)
    ("66__" ten_m)
    ("67__" ten_m)
    ("68__" ten_m)
    ("69__" ten_m)
    ("666_" dbl_age)
    ("667_" dbl_age)
    ("668_" dbl_age)
    ("676_" dbl_age)
    ("677_" dbl_age)
    ("678_" dbl_age)
    ("686_" dbl_age)
    ("687_" dbl_age)
    ("688_" dbl_age)
    ("689_" dbl_age)
    ("696_" dbl_age)
    ("697_" dbl_age)
    ("698_" dbl_age)
    ("76__" dbl_age)
    ("77__" dbl_age)
    ("78__" dbl_age)
    ("765_" misc)
    ("777_" misc)
    ("770_" ten_m)
    ("771_" ten_m)
    ("772_" ten_m)
    ("780_" ten_m)
    ("781_" ten_m)
    ("782_" ten_m)
    ("789_" misc)
    ("86__" dbl_age)
    ("87__" dbl_age)
    ("88__" dbl_age)
    ("89__" dbl_age)
    ("888_" misc)
    ("880_" ten_m)
    ("890_" ten_m)
    ("870_" ten_m)
    ("876_" misc)
    ("860_" ten_m)
    ("96__" dbl_age)
    ("97__" dbl_age)
    ("98__" dbl_age)
    ("99__" dbl_age)
    ("987_" misc)
    ("999_" misc)
    ))

(defvar *contextual-codes* '(decimal b2b-digits b2b-codes symmetry add-em-up diff))

(defvar *parent-contextual-codes* '(symmetry progression))

(defun time-tally-to-add-detail-to-retrieval-structure ()
   "This is the time taken to in conscious time to add detail to a retrieval structure.
Please note that EPAM learning time to fill a slot is 500 msec.  Therefore, slots can
not be filled faster than 500 msec. apart." 
   (when *print-time-updates*
      (princ "     Time tally to add detail to retrieval structure = "))
   (advance-the-timer 250))

(defun time-tally-to-search-for-context-cue ()
   "This is the time taken to search an EPAM net for a match."
   (when *print-time-updates*
      (princ "     Time tally to search for context cue = "))
   (advance-the-timer 100))

(defun time-tally-to-sort-through-net ()
   (when *print-time-updates*
    (princ "     Time tally to recognize chunk = "))
  (advance-the-timer 100))

(defun time-tally-to-orient-to-probe ()
  (when *print-time-updates*
    (princ "     Time tally to orient to probe = "))
  (advance-the-timer 1500))

(defun time-tally-to-vocalize-digit ()
  (when *print-time-updates* 
    (princ "     Time tally to vocalize a digit = "))
  (advance-the-timer 200))


(defun create-categories-net ()
   "This routine creates an initial categories net for use in the
 expert memory simulations of DD's memory for digits."
   (let ((objects (loop for pair in *data-for-categories-net*
                    for string = (first pair)
                    for object = (create-object-for-categories-net string)
                    collect object))
         (categories (loop for pair in *data-for-categories-net*
                       collect (second pair)))
         (forgetting-parameter *forgetting-parameter*))
      (setf *forgetting-parameter* 0) ; At the end of the routine this will be restored
      (when (eql (get-chunk-for (first objects)) *net*)
         (discriminate (first objects))
         (add-test-to-node 'length (get-chunk-for (first objects)))
         (loop for object in objects
           do 
           (when *print-experimenter-updates* 
              (format t "Studying object = ~a~%" object))
           (loop while (study object)))
         (loop for object in objects
           for category in categories
           do 
           ;(setf *object* object)
           ;(setf *category* category)
           (when *print-experimenter-updates*
              (format t "Associating object = ~a with category = ~a~%"
                object category))
           (loop while (associate 'category object category)))
         (loop for object in objects
           for category in categories
           for result = (find-associate 'category object 2 'instant)
           do 
           (when *print-experimenter-updates*
              (format t "Category associated with object = ~a ~a~%"
                object result category))
           if (not (eql category result))
           do (break))
         (setf *forgetting-parameter* forgetting-parameter)
         )))



(defun create-object-for-categories-net (string)
   (let ((object (change-string-to-object string 'categories))
         (pairs (loop for character in *character-digits*
                  for digit in *digits*
                  collect (list character digit))))
      (loop for remainder on (cdr object)
        if (eql (car remainder) #\~) 
        do (rplaca remainder '~)
        else if (assoc (car remainder) pairs)
        do (rplaca remainder (second (assoc (car remainder) pairs))))
      object))


;;                B. INITIALIZATION ROUTINES

(defun initialize-retrieval-structure ()
  (create-chunks-for-words *spoken-digits* 'auditory)
  (dotimes (count 3)
    (mapcar #'(lambda (x y)
                (associate 'auditory-visual 
                           (change-string-to-object x 'auditory)
                           y))
            *spoken-digits* *digits*))
  (format t "Nodes in net after learning chunks for spoken digits = ~a~%"
          *nodes-in-net*)
  (study-retrieval-structure-components *rs-components*)
  (format t "Nodes in net after learning initial rs-components = ~a~%"
          *nodes-in-net*)
  (study-episodic-objects *initial-episodic-knowledge*)
  (format t "Nodes in net after learning initial digit groups = ~a~%"
          *nodes-in-net*))


(defun study-retrieval-structure-components (list-of-lists)
  "Given a list-structure of lists, each consisting of lists
like '(digit digit digit digit), this routine studies them to create a 
chunk which can be instantiated as a structure of the given length.
Each chunk is associated via the attribute
'number-to-structure with the number of digits that can be represented
by the list structure."
  (loop initially (format t "Studying components of retrieval-structures.~%")
        while (member t (mapcar 
                         #'(lambda (x) 
                             (associate 'number-to-structure 
                                        (how-many-atoms-in x)
                                        (make-rs-components x)))
                         list-of-lists))))

(defun make-rs-components (object)
  (cond 
   ((atom object) object)
   ('else
    (loop for subobject in object
          collect (make-rs-components subobject) into lis
          finally (push nil lis)
                  (put-value lis 'retrieval-structure 'is-a)
                  (return lis)))))

(defun how-many-atoms-in (list-structure)
  (cond 
   ((null list-structure) 0)
   ((atom list-structure) 1)
   ('else
    (+ (how-many-atoms-in (car list-structure))
       (how-many-atoms-in (cdr list-structure))))))

(defun how-many-digits-in (rs-chunk)
  (cond
   ((null rs-chunk) 0)
   ((eql rs-chunk 'digit) 1)
   ((listp rs-chunk)
    (+ (how-many-digits-in (car rs-chunk))
       (how-many-digits-in (cdr rs-chunk))))
   ('else (how-many-digits-in (cdr (get rs-chunk 'image))))))
    
(defun study-episodic-objects  (list-of-lists)
  "Given a list in the format of *initial-episodic-knowledge*,
this routine creates objects out of each sublist and
studies these objects in order to create the initial episodic nets."
  (loop initially (format t "Making initial entries for episodic nets.~%")
        while (member t (mapcar 
                         #'(lambda (x) 
                             (study (make-episodic-object x)))
                         list-of-lists))))

 
(defun make-episodic-object (lis)
  "The input lists are lists of digits followed by
their is-a, for example (1 9 9 0 date).  This routine converts the
list into an EPAM object.  For example (1 9 9 0 date) becomes
(((is-a date)) 1 9 9 0)." 
  (let ((object (cons nil (reverse (cdr (reverse lis))))))
    (put-value object (car (last lis)) 'is-a)
    object))


;;                  C. EXPERIMENTER ROUTINES 

(defun print-out-record-of-rs-growth ()
  (loop with total = 0
        for item in (reverse *record-of-rs-growth*)
        for position = 1 then (if (= position 5) 1 (1+ position))
        do (if (= position 1) (setf total item) (setf total (+ total item)))
        when (= position 5)
        collect (float (/ total 5)) into record
        finally (format t "Mean digit span per 5-session block: ~%")
                (princ record)
                (terpri)))
  
(defun print-out-record-of-nodes-in-net ()
  (loop for item in (reverse *record-of-nodes-in-net*)
        for position = 1 then (if (= position 5) 1 (1+ position))
        when (= position 5)
        collect item into record
        finally (format t "Nodes-in-net at end of each 5-session block: ~%")
                (princ record)
                (terpri)))
        
(defun write-latency-times ()
   (let ((pause-times-array (make-array 150 :initial-element nil)))
      (loop for i from 0 to *list-number*
        do (loop for clocks on (reverse (aref *latency-array* i))
             for n from 1
             for pause-time = (when (second clocks)
                                 (- (second clocks) (first clocks)))
             if pause-time
             do (setf (aref pause-times-array n) 
                      (cons pause-time (aref pause-times-array n)))))
      (format t "~%Comparison of predicted with actual median-pause times ~%")
      (format t "in serial recall for retrieval structure.~%")
      (format t " ------------------------------~%")
      (format t "  Digit Location   DD     EPAM~%")
      (format t " ------------------------------~%")
      (format t "       1          269    ~5,0f~%" (find-median (aref pause-times-array  1)))
      (format t "       2          228    ~5,0f~%" (find-median (aref pause-times-array  2)))
      (format t "       3          241    ~5,0f~%" (find-median (aref pause-times-array  3)))
      (format t "       4         1547    ~5,0f~%" (find-median (aref pause-times-array  4)))
      (format t "       5          179    ~5,0f~%" (find-median (aref pause-times-array  5)))
      (format t "       6          180    ~5,0f~%" (find-median (aref pause-times-array  6)))
      (format t "       7          219    ~5,0f~%" (find-median (aref pause-times-array  7)))
      (format t "       8         1508    ~5,0f~%" (find-median (aref pause-times-array  8)))
      (format t "       9          259    ~5,0f~%" (find-median (aref pause-times-array  9)))
      (format t "      10          218    ~5,0f~%" (find-median (aref pause-times-array 10)))
      (format t "      11          228    ~5,0f~%" (find-median (aref pause-times-array 11)))
      (format t "      12         1142    ~5,0f~%" (find-median (aref pause-times-array 12)))
      (format t "      13          188    ~5,0f~%" (find-median (aref pause-times-array 13)))
      (format t "      14          208    ~5,0f~%" (find-median (aref pause-times-array 14)))
      (format t "      15          224    ~5,0f~%" (find-median (aref pause-times-array 15)))
      (format t "      16         1122    ~5,0f~%" (find-median (aref pause-times-array 16)))
      (format t "      17          142    ~5,0f~%" (find-median (aref pause-times-array 17)))
      (format t "      18          172    ~5,0f~%" (find-median (aref pause-times-array 18)))
      (format t "      19          496    ~5,0f~%" (find-median (aref pause-times-array 19)))
      (format t "      20          158    ~5,0f~%" (find-median (aref pause-times-array 20)))
      (format t "      21          198    ~5,0f~%" (find-median (aref pause-times-array 21)))
      (format t "      22          409    ~5,0f~%" (find-median (aref pause-times-array 22)))
      (format t "      23          175    ~5,0f~%" (find-median (aref pause-times-array 23)))
      (format t "      24          220    ~5,0f~%" (find-median (aref pause-times-array 24)))
      (format t "      25         1235    ~5,0f~%" (find-median (aref pause-times-array 25)))
      (format t "      26          152    ~5,0f~%" (find-median (aref pause-times-array 26)))
      (format t "      27          171    ~5,0f~%" (find-median (aref pause-times-array 27)))
      (format t "      28          246    ~5,0f~%" (find-median (aref pause-times-array 28)))
      (format t "      29          744    ~5,0f~%" (find-median (aref pause-times-array 29)))
      (format t "      30          179    ~5,0f~%" (find-median (aref pause-times-array 30)))
      (format t "      31          178    ~5,0f~%" (find-median (aref pause-times-array 31)))
      (format t "      32          228    ~5,0f~%" (find-median (aref pause-times-array 32)))
      (format t "      33          567    ~5,0f~%" (find-median (aref pause-times-array 33)))
      (format t "      34          182    ~5,0f~%" (find-median (aref pause-times-array 34)))
      (format t "      35          181    ~5,0f~%" (find-median (aref pause-times-array 35)))
      (format t "      36          194    ~5,0f~%" (find-median (aref pause-times-array 36)))
      (format t "      37         2011    ~5,0f~%" (find-median (aref pause-times-array 37)))
      (format t "      38          186    ~5,0f~%" (find-median (aref pause-times-array 38)))
      (format t "      39          206    ~5,0f~%" (find-median (aref pause-times-array 39)))
      (format t "      40          611    ~5,0f~%" (find-median (aref pause-times-array 40)))
      (format t "      41          160    ~5,0f~%" (find-median (aref pause-times-array 41)))
      (format t "      42          224    ~5,0f~%" (find-median (aref pause-times-array 42)))
      (format t "      43          476    ~5,0f~%" (find-median (aref pause-times-array 43)))
      (format t "      44          151    ~5,0f~%" (find-median (aref pause-times-array 44)))
      (format t "      45          222    ~5,0f~%" (find-median (aref pause-times-array 45)))
      (format t "      46         1586    ~5,0f~%" (find-median (aref pause-times-array 46)))
      (format t "      47          160    ~5,0f~%" (find-median (aref pause-times-array 47)))
      (format t "      48          198    ~5,0f~%" (find-median (aref pause-times-array 48)))
      (format t "      49          250    ~5,0f~%" (find-median (aref pause-times-array 49)))
      (format t "      50          674    ~5,0f~%" (find-median (aref pause-times-array 50)))
      (format t "      51          194    ~5,0f~%" (find-median (aref pause-times-array 51)))
      (format t "      52          215    ~5,0f~%" (find-median (aref pause-times-array 52)))
      (format t "      53          252    ~5,0f~%" (find-median (aref pause-times-array 53)))
      (format t "      54          933    ~5,0f~%" (find-median (aref pause-times-array 54)))
      (format t "      55          181    ~5,0f~%" (find-median (aref pause-times-array 55)))
      (format t "      56          216    ~5,0f~%" (find-median (aref pause-times-array 56)))
      (format t "      57          236    ~5,0f~%" (find-median (aref pause-times-array 57)))
      (format t "      58         2286    ~5,0f~%" (find-median (aref pause-times-array 58)))
      (format t "      59          165    ~5,0f~%" (find-median (aref pause-times-array 59)))
      (format t "      60          220    ~5,0f~%" (find-median (aref pause-times-array 60)))
      (format t "      61          555    ~5,0f~%" (find-median (aref pause-times-array 61)))
      (format t "      62          165    ~5,0f~%" (find-median (aref pause-times-array 62)))
      (format t "      63          217    ~5,0f~%" (find-median (aref pause-times-array 63)))
      (format t "      64          650    ~5,0f~%" (find-median (aref pause-times-array 64)))
      (format t "      65          158    ~5,0f~%" (find-median (aref pause-times-array 65)))
      (format t "      66          225    ~5,0f~%" (find-median (aref pause-times-array 66)))
      (format t "      67          839    ~5,0f~%" (find-median (aref pause-times-array 67)))
      (format t "      68          180    ~5,0f~%" (find-median (aref pause-times-array 68)))
      (format t "      69          156    ~5,0f~%" (find-median (aref pause-times-array 69)))
      (format t "      70          230    ~5,0f~%" (find-median (aref pause-times-array 70)))
      (format t "      71          661    ~5,0f~%" (find-median (aref pause-times-array 71)))
      (format t "      72          159    ~5,0f~%" (find-median (aref pause-times-array 72)))
      (format t "      73          170    ~5,0f~%" (find-median (aref pause-times-array 73)))
      (format t "      74          204    ~5,0f~%" (find-median (aref pause-times-array 74)))
      (format t "      75          619    ~5,0f~%" (find-median (aref pause-times-array 75)))
      (format t "      76          163    ~5,0f~%" (find-median (aref pause-times-array 76)))
      (format t "      77          168    ~5,0f~%" (find-median (aref pause-times-array 77)))
      (format t "      78          212    ~5,0f~%" (find-median (aref pause-times-array 78)))
      (format t "      79          872    ~5,0f~%" (find-median (aref pause-times-array 79)))
      (format t "      80          171    ~5,0f~%" (find-median (aref pause-times-array 80)))
      (format t "      81          224    ~5,0f~%" (find-median (aref pause-times-array 81)))
      (format t "      82          415    ~5,0f~%" (find-median (aref pause-times-array 82)))
      (format t "      83          155    ~5,0f~%" (find-median (aref pause-times-array 83)))
      (format t "      84          226    ~5,0f~%" (find-median (aref pause-times-array 84)))
      (format t "      85          523    ~5,0f~%" (find-median (aref pause-times-array 85)))
      (format t "      86          169    ~5,0f~%" (find-median (aref pause-times-array 86)))
      (format t "      87          196    ~5,0f~%" (find-median (aref pause-times-array 87)))
      (format t "      88          978    ~5,0f~%" (find-median (aref pause-times-array 88)))
      (format t "      89          175    ~5,0f~%" (find-median (aref pause-times-array 89)))
      (format t "      90          187    ~5,0f~%" (find-median (aref pause-times-array 90)))
      (format t "      91          223    ~5,0f~%" (find-median (aref pause-times-array 91)))
      (format t "      92          597    ~5,0f~%" (find-median (aref pause-times-array 92)))
      (format t "      93          149    ~5,0f~%" (find-median (aref pause-times-array 93)))
      (format t "      94          184    ~5,0f~%" (find-median (aref pause-times-array 94)))
      (format t "      95          201    ~5,0f~%" (find-median (aref pause-times-array 95)))
      (format t "      96          841    ~5,0f~%" (find-median (aref pause-times-array 96)))
      (format t "      97          154    ~5,0f~%" (find-median (aref pause-times-array 97)))
      (format t "      98          191    ~5,0f~%" (find-median (aref pause-times-array 98)))
      (format t "      99          215    ~5,0f~%" (find-median (aref pause-times-array 99)))
      ))
             
             
(defun find-median (given-lis)
   "Assume that lis is a list of numbers.  This routine finds the median."
   (when given-lis
      (let ((lis (sort (copy-list given-lis) '>))
            (n (length given-lis)))
         (cond ((evenp n) (/ (+ (nth (/ n 2) lis) (nth (1- (/ n 2)) lis)) 2))
               ('else (nth (/ (1- n) 2) lis))))))
         
      

;;             D. ROUTINES THAT SEARCH RETRIEVAL STRUCTURES

(defun depth-first-search-in-image (node exit continue)
  "Inputs are:
  1.  Node from which search starts.  If the given node has any descendents it 
       searching begin by going down first before going up.
  2.  Exit-test routine which is used to exit if search arrives at a node
       with particular characteristics.
  3.  Continue-test routine which is used to determine whether search will
       continue down a particular path.
  4.  Partial image (if any) which can be used to guide search
Output is either a node (if the exit test was satisfied) or nil if it
wasn't.  This routine does not test the given node or its ancestors to see 
if they satisfy the continue or exit tests."
  (loop for descendent in (get node 'descendents)
        for success = (search-down-in-image descendent exit continue)
        when success return success
        finally (return  
                 (search-up-then-down-image node exit continue))))

(defun search-up-then-down-image (node exit continue)
  (let ((parent-node (get node 'ancestor)))
    (when parent-node 
      (decay (get node 'image))
      (time-tally-for-node-traversal)
      (loop for descendent in (cdr (member node (get parent-node 'descendents)))
            for success = (search-down-in-image descendent exit continue)
            when success return success
            finally (return 
                     (search-up-then-down-image parent-node exit continue))))))

(defun search-down-in-image (node exit continue)
  "This routine checks a given node to see if it is indeed a node (characters, 
numbers, and slots are not nodes).  If it is a node, it determines if it passes 
the continue and exit tests.  It exits with node or one of its descendents if 
one is found that passes the exit test.  If none is found it exits nil."
  (cond
   ((characterp node) nil)
   ((numberp node) nil)
   ((get node 'members) nil)
   ((apply exit (list node)) (time-tally-for-node-traversal) node)
   ((null (apply continue (list node))) nil)
   ('else (time-tally-for-node-traversal)
          (loop for descendent in (get node 'descendents)
                for success = (search-down-in-image descendent exit continue)
                if success 
                do (decay (get (get node 'ancestor) 'image))
                   (return success)))))

(defun backward-depth-first-search-in-image (node exit continue)
  (loop for descendent in (reverse (get node 'descendents))
        for success = (backward-search-down-in-image descendent exit continue)
        when success return success
        finally (return  
                 (backward-search-up-then-down-image node exit continue))))

(defun backward-search-up-then-down-image (node exit continue)
  (let ((parent-node (get node 'ancestor)))
    (when parent-node 
      (decay (get node 'image))
      (time-tally-for-node-traversal)
      (loop for descendent 
            in (cdr (member node (reverse (get parent-node 'descendents))))
            for success = (backward-search-down-in-image descendent 
                                                         exit 
                                                         continue)
            when success return success
            finally (return 
                     (backward-search-up-then-down-image parent-node 
                                                         exit 
                                                         continue))))))

(defun backward-search-down-in-image (node exit continue)
  "Checks given node to see if it passes the continue test and the exit test
it exits with node or one of its descendents if one is found that passes
the exit test.  If none is found it exits nil."
  (cond
   ((characterp node) nil)
   ((numberp node) nil)
   ((get node 'members) nil)
   ((apply exit (list node)) (time-tally-for-node-traversal) node)
   ((null (apply continue (list node))) nil)
   ('else (time-tally-for-node-traversal)
          (loop for descendent in (reverse (get node 'descendents))
                for success = (backward-search-down-in-image descendent 
                                                             exit 
                                                             continue)
                if success 
                do (decay (get (get node 'ancestor) 'image))
                   (return success)))))

(defun get-top-node-of-image (node)
  (let ((parent (get node 'ancestor)))
    (cond ((null parent) node)
          ('else (decay (get node 'image))
                 (get-top-node-of-image parent)))))


(defun get-next-retrieval-structure-node ()
  "This routine performs a depth first search
to find the next node in the retrieval structure.   This routine
puts a pointer to the node in *rsf* unless there is no next node, in which
case it exits nil."
  (when (not (is-this-node-dead-last? *rsf*))
    (depth-first-search-in-image *rsf* 'rsf-exit 'rsf-continue)))

(defun get-previous-retrieval-structure-node ()
  (when (not (is-this-the-very-first-node? *rsf*))
    (backward-depth-first-search-in-image *rsf* 'rsf-exit 'rsf-continue)))

;; This exit test exits if the first descendent of a node is a character
(defun rsf-exit (node)
  (let ((first-descendent (car (get node 'descendents))))
      (or (null first-descendent)
          (numberp first-descendent)
          (characterp first-descendent)
          (get first-descendent 'members))))

;; This continue test always returns t (so long as given node exists).
(defun rsf-continue (node)
  (identity node))


;;;         E. ROUTINES THAT CREATE RETRIEVAL STRUCTURES

(defun instantiate-chunk-as-tree (chunk &optional parent-node)
  "Given a chunk, this routine creates a tree of
property lists representing the chunk's image.  The subobjects of the
chunk's image become a list of values associated with 'descendents.  The
parent node is the 'ancestor."
   (let (node)
      (cond
       ((null chunk) nil)
       ((characterp chunk) chunk)
       ((numberp chunk) chunk)
       ((eql chunk 'digit) nil)
       ('else 
        ; here the value is a chunk having an image such as (((retrieval-structure)) digit digit digit)
        (let ((image (list nil))
              (chunk-image (get-image-of chunk)))
           (setf node (gensym))
           (when parent-node (setf (get node 'ancestor) parent-node))
           (setf (get node 'image) image)
           (put-value image 'episode 'is-a)
           (loop repeat (1- (length chunk-image))
             do (nconc image (list #\_)))
           (cond ((eql (second chunk-image) 'digit)
                  (loop for attribute in *contextual-codes*
                    do (put-value image #\_ attribute)))
                 ('else 
                  (loop for attribute in *parent-contextual-codes*
                    do (put-value image #\_ attribute))))
           (loop for subobject in (cdr chunk-image)
             for value = (instantiate-chunk-as-tree subobject node)
             if value collect value into descendents
             finally (when descendents (setf (get node 'descendents) descendents))))
        node))))


(defun instantiate-retrieval-structure (n)
  "This routine creates a retrieval structure of the length
specified, prints the structure, and puts the first node in *rsf*."
  (setf *rsf* 
        (instantiate-chunk-as-tree (get-retrieval-structure-for-length n)))
  (when *print-major-updates* (print-retrieval-structure *rsf*)))

;; Print-retrieval-structure prints the *rsf* in more-or-less readable form.
(defun print-retrieval-structure (tree &optional level)
  (when (null level) (setf level 0))
  (tab-over level)
  (princ tree)
  (when (get tree 'image)
    (format t ": ~a" (get tree 'image)))
  (if (or (characterp (car (get tree 'descendents)))
          (numberp (car (get tree 'descendents)))
          (get (car (get tree 'descendents)) 'members))
    (format t " -- ~a digits~%" (length (get tree 'descendents)))
    (loop for descendent in (get tree 'descendents)
          initially (terpri)
          do (print-retrieval-structure descendent (1+ level)))))

(defun get-retrieval-structure-for-length (n)
  "If there is already a retrieval structure associated with this length, the
system will output that chunk after checking to make sure that the length
of the chunk is correct since familiarization can cause changes in 
retrieval structures that were created earlier.  If an error occurred in
one of the retrieval structures that was initially given to the system, 
then this routine will cause the system to study its original structures
again.  If the system has not already built a retrieval structure for this
length, this routine will call create-new-retrieval-structue whichs
causes the system to build retrieval structures that closely resemble DD's
retrieval structures."
   (let (associate)
      (if (and (setf associate (find-associate 'number-to-structure n))
               (= n (how-many-digits-in associate)))
         (progn (loop while (study associate))
           (get-chunk-for associate))
         (if (member n '(3 4 5 6 7 8 9 10 11 12 13 14 16 21 22 23))
            (progn
             (study-retrieval-structure-components *rs-components*)
             (get-retrieval-structure-for-length n))
            (create-new-retrieval-structure n)))))

(defun create-new-retrieval-structure (m)
   "This routine will build retrieval structures according to the following
 algorithm:
 1.  If n is greater then 18, put d16 at the beginning of the list and subtract
    16 from n. 
 2.  If n < 21 proceed
    If n = 23 then add d23 to end of list and subtract 23 from n.
    If n = 22 then add d22 to end of list and subtract 22 from n.
    add d21 to end of list, subtract 21 from n and
    goback to 2..
 At this point, n ranges from 3 to 20
 3.  If n < 9 proceed
    If n = 11 then add d11 to end of list subtract 11 from n.
    If n = 10 then add d10 to end of list and subtract 10 from n.
    Add d9 to end of list, subtract 9 from n and goback to 3.
 At this point n ranges from 3 to 8.
 4.  If n = 0 proceed
    Add the one that matches n perfectly, and proceed
 5.  Associate the original number input with the object presented so that
    in the future it will be possible to find a chunk representing this 
    object without having to reproduce it again from scratch.
 6.  Output the chunk represented by this object."
   (let ((object (list nil)) (n m))
      (put-value object 'retrieval-structure 'is-a)
      (when (> n 18)
         (nconc object (list (get-retrieval-structure-for-length 16)))
         (setf n (- n 16)))
      (loop
       (when (< n 21) (return t))
       (when (= n 23)
          (nconc object (list (get-retrieval-structure-for-length 23)))
          (setf n (- n 23))
          (return t))
       (when (= n 22)
          (nconc object (list (get-retrieval-structure-for-length 22)))
          (setf n (- n 22))
          (return t))
       (nconc object (list (get-retrieval-structure-for-length 21)))
       (setf n (- n 21)))
      (loop
       (when (< n 9) (return t))
       (when (= n 11)
          (nconc object (list (get-retrieval-structure-for-length 11)))
          (setf n (- n 23))
          (return t))
       (when (= n 10)
          (nconc object (list (get-retrieval-structure-for-length 10)))
          (setf n (- n 10))
          (return t))
       (nconc object (list (get-retrieval-structure-for-length 9)))
       (setf n (- n 9)))
      (when (> n 0)
         (nconc object (list (get-retrieval-structure-for-length n))))
      (loop
       (if (null (associate 'number-to-structure m object))
          (return t)))
      (loop while (study object))
      (get-chunk-for object)))



;;      F. ROUTINES THAT ANSWER QUESTIONS ABOUT RETRIEVAL STRUCTURES

(defun does-this-node-have-any-descendents? (node)
  (loop for descendent in (get node 'descendents)
        thereis (and (not (characterp descendent))
                     (not (numberp descendent))
                     (not (get descendent 'members)))))

(defun is-this-node-dead-last? (node)
   (let* ((parent (get node 'ancestor))
          (grandparent (get parent 'ancestor))
          (ggrandparent (get grandparent 'ancestor)))
      (and (not (does-this-node-have-any-descendents? node))
           (is-this-a-last-node? node)
           (is-this-a-last-node? parent)
           (is-this-a-last-node? grandparent)
           (is-this-a-last-node? ggrandparent))))
   
(defun is-this-a-last-node? (node)
  (let* ((parent (get node 'ancestor)))
    (cond ((null node) t)
          ((null parent) t)
          ((eq node (car (last (get parent 'descendents)))) t))))

(defun is-this-the-very-first-node? (node)
   (let* ((parent (get node 'ancestor))
          (grandparent (get parent 'ancestor))
          (ggrandparent (get grandparent 'ancestor)))
      (and (is-this-a-first-node? node)
           (not (does-this-node-have-any-descendents? node))
           (is-this-a-first-node? parent)
           (is-this-a-first-node? grandparent)
           (is-this-a-first-node? ggrandparent))))

(defun is-this-a-first-node? (node)
   (let* ((parent (get node 'ancestor)))
    (cond ((null node) t)
          ((null parent) t)
          ((eq node (car (get parent 'descendents))) t))))
        
(defun is-this-in-the-first-four-groups-of-four? (rs-node)
  "This routine exits t if the parent's parent-branch is 1 and there is no
great-grandparent.  This routine is called by the rehearsal routine so
that it knows when it has reached the first-four-groups-of-four so that it won't
bother to search for a missing code until it gets to the beginning."
  (let* ((parent (get rs-node 'ancestor))
         (grandparent (get parent 'ancestor)))
    (and grandparent
         (null (get grandparent 'ancestor))
         (eql parent (car (get grandparent 'descendents))))))


(defun number-of-digits (rs-node)
  (1- (length (get-image-of rs-node))))

(defun is-image-complete? ()
  "This routine tests to see if none of the digits in the 
image of *rsf* have #\_ as their values."
  (null (member #\_ (cdr (get *rsf* 'image)))))
       


;;                    G. EXPERIMENTER ROUTINES

(defun check-digits-experimenter ()
  "Check-digits-experimenter compares the output in *output* with the next
digits in *digits-list*.  It adds to the *errors* and prints what the 
output should have been.  It also adds to the *list-of-errors*"
  (let ((correct-output nil)
        (output *output*)
        (errors *errors*))
    (loop
      (if (null output) (return t))
      (if (null (eq (car *digits-list*) (car output)))
          (setf *errors* (1+ *errors*)))
      (setf correct-output (append correct-output (list (car *digits-list*))))
      (setf *digits-list* (cdr *digits-list*))
      (setf output (cdr output)))
    (when *print-major-updates*
      (princ "Experimenter says digits should have been: ")
      (princ correct-output)
      (terpri))
    (when *print-experimenter-updates*
      (princ "Total errors so far on this list = ")
      (princ *errors*)
      (terpri))
    (when (> *errors* errors)
       (let ((signals (mapcar
                      #'(lambda (x)
                          (change-string-to-object (nth x *spoken-digits*) 'auditory))
                      correct-output)))
          (loop for signal in signals
            do
            (put-value signal 'new 'group)
            (put-value signal (+ (get 'auditory 'sensory-persistence) 15000) 'time)
            (setf (get 'auditory 'sensory-store)
                  (append (get 'auditory 'sensory-store) (list signal))
                  *new-stimulus-flag* t
                  (get 'auditory 'new-stimulus) t)))
       (setf *list-of-errors* (cons correct-output *list-of-errors*)))))
 

(defun create-stimulus-list-from (list-of-digits)
  "This routine is used when a 
specific list of digits is to be used by the system.  It calls that list
the *digits-list* and creates a *stimulus-list* consisting of objects that
represent spoken digits."
    (setf *digits-list* list-of-digits)
    (setf *stimulus-list* 
          (mapcar
           #'(lambda (x)
               (change-string-to-object (nth x *spoken-digits*) 'auditory))
           list-of-digits)))

      
;;                     H. CONVERSION ROUTINES


(defun turn-rs-object-into-semantic-object (rs-node &optional code)
  "This routine turns a retrieval structure image into an object that can be
sorted in the semantic net.  The object is created by taking the image and
cutting off a leading 0 if one is there."
   (when (not code) (setf code (try-to-get-code rs-node)))
   (let* ((image (get rs-node 'image))
          (object (mapcar 'specify (cdr image))))
      (when (eql (car object) 0) (pop object))
      (push nil object)
      (put-value object code 'is-a)
      object))


(defun turn-semantic-object-into-rs-object (rs-image)
  "This routine creates an object out of the image of the node that is in the 
car of *chunks*.  The rs-node is used as a template to help figure out which
part of the image to utilize.
    Algorithm:
        A. If the rs-image begins with a zero, add a zero to the beginning
           of the object since images of semantic objects do not include
           the leading zero.
        B. If object is longer than the retrieval structure image
           cut the object down in size by peeling off the final
           digits."
  (let ((object (get (car *chunks*) 'image)))
    (when (eql (specify (cadr rs-image)) 0)
               (setf object (append (list (car object)) (list 0) (cdr object))))
    (loop while (> (length object) (length rs-image))
          do (setf object (reverse (cdr (reverse object)))))
    object))
          

;;               I. DEPTH FIRST SEARCHES IN NET

(defun search-for-semantic-node (object)
  "Search-for-semantic-node searches net beginning with the semantic node which
is in (car chunks) until a chunk is found that has the current activation and
the correct retrieval structure node.  If it finds the correct semantic node
it puts it in *chunks*.  If not, it puts nil in chunks."
   (setf *search-interrupt* 'null-experimenter)
   (let ((result (search-down-in-net *net* object 'good-semantic-node-exit-test)))
      (cond ((and result 
                  (not (eql result 'blank))
                  (good-semantic-node-exit-test result))
             (setf *chunks* (cons result *chunks*)))
            ('else (setf *chunks* (cons nil *chunks*))))))



(defun free-recall-search (semantic-object)
  (setf *search-interrupt* 'null-experimenter)
  (setf *chunks* (cons (depth-first-search-in-net (car *chunks*)
                                                  semantic-object
                                                  'test-for-active-node-with-rsn)
                       (cdr *chunks*))))
        
(defun good-semantic-node-exit-test (node)
   (when (was-this-just-activated? node)
      (loop 
        for episode in (find-episodes 'episode node)
        if (eql episode *rsf*) return t
        if (not (was-this-just-activated? episode)) return nil
        do (variable-time-tally-to-recognize))))
        
(defun test-for-active-node-with-rsn (node)
   (cond ((not (was-this-just-activated? node))
          (when *print-epam-updates* 
             (format t "Rejecting node = ~a because not activated.~%" node))
          nil)
         ((let ((episodes (find-episodes 'episode node)))
             (and (not (was-this-just-activated? (car episodes)))
                  (not (was-this-just-activated? (second episodes)))))
          (when *print-epam-updates* 
             (format t "Rejecting node = ~a because episode not activated.~%" node))
          nil)
         ('else 
          (when *print-epam-updates*
             (format t "Accepting node = ~a which passes test-for-active-node-with-rsn"
               node))
          t)))
         
(defun search-categories-net-for-a-code (semantic-object last-node)
   "Changed in 2000 to recognize that the categories net was just part of the 
general net." 
   (let ((object (copy-object semantic-object)))
      (put-value object 'categories 'is-a)
      (setf *search-interrupt* 'null-experimenter)
      (depth-first-search-in-net last-node object 'good-categories-node-exit-test)))
 
(defun good-categories-node-exit-test (node)
  (find-associate 'category node 2 'instant))

;;                  J. STUDY STAGE ROUTINES

(defun study-digit-list-strategy ()
  "1. At the beginning of this routine, the root node of the retrieval structure
      is the retrieval structure focus (i.e. the *rsf*).  This routine ends
      when the system has performed the following steps with each leaf node
      of the retrieval structure and has reached the end of the retrieval 
      structure.
   2. Get the first digit from the auditory sensory store, attach it in the 
      first position of the retrieval-structure-image.
   3. Get the second digit from the auditory sensory store, attach it in the 
      second position of the retrieval-structure-image.
   4. Using the auditory sensory store, compare the first two digits of this
      group with the last two digits of the previous node.  If there is a match
      set the value of the b2b-digits attribute of the retrieval-structure image 
      to be 'single, forward, or reverse.
   4. Input digits until all but the last for this code has been input.  As 
      each digit is input, attach it to the end of the retrieval structure image
   5. Sort the retrieval-structure-image to find the semantic 
      interpretation and attach it as t3he is-a.
   7. Access the last digit and attach it to the end of the image.
   8. Add contextual codes to the retrieval structure node.  
   9. Find-decimal adds the decimal (the last digit) redundantly to the retrieval structure.  
  10. Find and familiarize the semantic node in order to attach a pointer from it to the 
      retrieval structure.
  11. Get the next node in the retrieval structure. 
  11. Go back to 1."
   (when *record-codes-found*
      (setf (aref *codes-found* *trial-position*)
            (cons nil (aref *codes-found* *trial-position*))))
   (loop for rsf = (get-next-retrieval-structure-node)
     while rsf
     do (setf *rsf* rsf)
     (when (null (access-and-attach 1)) (return t))
     (print-rsimage)
     (access-and-attach 2)
     (print-rsimage)
     (when (search-for-contextual-codes?) (check-for-back-to-back-pairs))
     (access-and-attach 3)
     (print-rsimage)
     (attach-semantic-code-to-parent (use-net-to-find-the-code))
     (when (and (not (is-this-the-first-descendent? *rsf*))
                (search-for-contextual-codes?))
        (attach-back-to-back-codes))
     (when (and (is-this-the-last-descendent? *rsf*)
                (search-for-contextual-codes?))
        (add-context-cue-to-parent))
     (print-parent-rsimage)
     (when (> (length (get-image-of *rsf*)) 4) 
        (access-and-attach 4) 
        (print-rsimage))
     (when (> (length (get-image-of *rsf*)) 5) 
        (access-and-attach 5) 
        (print-rsimage))
     (loop while (and (search-for-contextual-codes?) (add-context-cue)))
     ; The familiarization below creates a link between the semantic-node and the *rsf*
     (create-link-from-semantic-objects-node-to-episode)
     (when *record-codes-found* (record-codes-found))
     (print-rsimage)))

(defun record-codes-found ()
   "This routine records the codes that were found by putting them in the *codes-found*
 array."  
   (let* ((image (specify (get-image-of *rsf*)))
          (codes-list (first (aref *codes-found* *trial-position*)))
          (codes (list (cdr image))))
      (setf codes (append codes 
                    (loop 
                      for code in *contextual-codes*
                      for value = (get-value image code)
                      if (and (not (eql value #\_))
                              (not (eql code 'decimal)))
                      collect (list code value))))
      (setf codes-list (append codes-list (list codes)))
      (when (is-this-the-last-descendent? *rsf*)
         (let ((parent-image (specify (get-image-of (get *rsf* 'ancestor)))))
            (setf codes (append (list (cdr parent-image))
                          (loop 
                            for code in *parent-contextual-codes*
                            for value = (get-value parent-image code)
                            if (not (eql value #\_))
                            collect (list code value))))
            (setf codes-list (append codes-list (list codes)))))
      (rplaca (aref *codes-found* *trial-position*) codes-list)))

(defun create-link-from-semantic-objects-node-to-episode ()
   (create-link-to *rsf* (get-chunk-for (turn-rs-object-into-semantic-object *rsf*))))
        
(defun is-this-the-first-descendent? (rs-node)
   (let ((parent (get rs-node 'ancestor)))
      (eq rs-node (car (get parent 'descendents)))))

(defun is-this-the-last-descendent? (rs-node)
  (let ((parent (get rs-node 'ancestor)))
    (eq rs-node (car (last (get parent 'descendents))))))

(defun print-rsimage ()
  (when *print-imagery-stores*
    (format t "Retrieval structure image: ~a~%" (get *rsf* 'image))))

(defun print-parent-rsimage ()
  (when *print-imagery-stores*
    (format t 
            "Retrieval structure parent's image: ~a~%" 
            (get (get *rsf* 'ancestor) 'image))))

(defun create-retrieval-structure-image ()
  "This routine creates a blank image for a retrieval structure node."
  (let (object)
    (setf object (list nil))
    (setf (get *rsf* 'image) object)))


(defun access-and-attach (position)
  "Access-and-attach accesses a digit from the auditory sensory store
 and attaches it in the proper position of the image of the *rsf*.
 Recognize first puts the object recognized at the beginning of *chunks*
 and the rest of the routine adds that digit to the position of the image."
  (let (chunk)
    (when (not *new-stimulus-flag*) (wait-for-item))
    (cond ((access-next-object-in-sensory-store 'new 'auditory)
           (translate-chunks 'auditory-visual)
           (setf chunk (car (get-chunks-off-top-of-stack)))
           (put-value-on-rsf chunk position)
           t)
          ((memory-store-search (get 'auditory 'sensory-store) 
                                'signal-to-respond)
           nil)
          ('else (print "ERROR INCORRECT NEW STIMULUS FLAG!")
                 (break)))))

(defun were-last-two-inputs-the-same (memory-store)
  (loop for sublist on (reverse memory-store)
        when (null (get-value (car sublist) 'group))
          do (if (equal (cdr (first sublist)) (cdr (second sublist)))
               (return t)
               (return nil))))


(defun check-for-back-to-back-pairs ()
  (let ((memory-store (reverse (loop for item in (get 'auditory 'sensory-store)
                                     if (eql 'old
                                             (get-value item 'group))
                                     collect item))))
    (cond ((and (cdr (fourth memory-store))
                (equal (cdr (fourth memory-store))
                       (cdr (second memory-store)))
                (equal (cdr (third memory-store))
                       (cdr (first memory-store))))
           (when *print-major-updates*
             (format t "Back-to-back-pairs found!~%"))
           (put-value-on-rsf 'forward 'b2b-digits))      
          ((and (cdr (fourth memory-store))
                (equal (cdr (fourth memory-store))
                       (cdr (first memory-store)))
                (equal (cdr (third memory-store))
                       (cdr (second memory-store))))
           (when *print-major-updates*
             (format t "Reverse-Back-to-back-pairs found!~%"))
           (put-value-on-rsf 'reverse 'b2b-digits))
          ((and (cdr (third memory-store))
                (equal (cdr (third memory-store))
                       (cdr (second memory-store))))
           (when *print-major-updates*
             (format t "Back-to-back-digits found!~%"))
           (put-value-on-rsf 'single 'b2b-digits))
          ('else nil))))

(defun add-context-cue ()
  "This routine checks for contextual-codes and adds the first one found to
the retrieval structure node.  It exits t if one was found 
regardless of whether it is able to add it.
Contextual codes searched for include:
    1. Decimal information -- the last digit is stored redundantly
    2. Symmetry information -- back-to-backs such as 5995 or 636

    3. Difference information -- ones differences such as 4546
                              -- tens differences such as 3747
                              -- elevens differences such as 2536
    4. Add-em-ups -- such as 459  since 4+5=9"
  (let ((image (specify (get *rsf* 'image))))
    (when *print-strategy-updates*
      (format t "Searching for context cues.~%"))
    (cond
     ((find-decimal-info image))
     ((find-symmetry-info image *rsf*))
     ((find-difference-info image))
     ((find-add-em-ups-info image)))))

     
(defun find-decimal-info (image)
   (cond ((not (eql (get-value (get-image-of *rsf*) 'decimal) #\_))
          nil)
         ('else 
          (put-value-on-rsf (car (last image)) 'decimal))))


(defun add-context-cue-to-parent ()
  (let* ((rsf (get *rsf* 'ancestor))
         (image (specify (get-image-of rsf))))
    (cond
     ((find-symmetry-info image rsf)) 
     ((find-ascending-progression-info image rsf))
     ((find-descending-progression-info image rsf)))))


(defun find-ascending-progression-info (image rsf)
   (let ((progression (member (specify (cadr image)) *ascending-races*)))
      (loop for image-tail on (cdr image)
        for progression-tail = progression then (cdr progression-tail)
        do  
        (when (null progression-tail) (return nil))
        (when (not (eql (specify (car image-tail)) (car progression-tail)))
           (return nil))
        finally
        (progn
         (put-value-on-rsf 'ascending 'progression rsf)
         (when *print-major-updates*
            (princ "Ascending progression found in this supergroup.")
            (terpri))
         (return t)))))

(defun find-descending-progression-info (image rsf)
   (let ((progression
          (member (specify (cadr image)) (reverse *ascending-races*))))
      (loop for image-tail on (cdr image)
        for progression-tail = progression then (cdr progression-tail)
        do  
        (when (null progression-tail) (return nil))
        (when (not (eql (specify (car image-tail)) (car progression-tail)))
           (return nil))
        finally
        (progn
         (put-value-on-rsf 'descending 'progression rsf)
         (when *print-major-updates*
            (princ "Descending progression found in this supergroup.")
            (terpri))
         (return t)))))


(defun find-all-same-info (image rsf)
  (let ((item (second image)))
    (loop for subimage in (cddr image)
          if (not (eql item subimage))
          return nil
          finally (return (put-value-on-rsf 'all-same 'symmetry rsf)))))

(defun find-symmetry-info (image &optional rsf)
   (when (not rsf) (setf rsf *rsf*))
   (cond
    ((get-value-on image 'symmetry)
     nil)
    ((find-all-same-info image rsf))
    ((= (length image) 4) 
     (find-symmetry-info-for-three image rsf))
    ((= (length image) 5)
     (find-symmetry-info-for-four image rsf))
    ('else nil)))

(defun find-symmetry-info-for-three (image rsf)
   (when (eql (nth 1 image) (nth 3 image))
      (when *print-strategy-updates*
         (format t "Frontwards-backwards found.~%"))
      (put-value-on-rsf 'front-back 'symmetry rsf)))
 
(defun find-symmetry-info-for-four (image rsf)
  (cond
   ((and (eql (nth 1 image) (nth 4 image))
         (eql (nth 2 image) (nth 3 image)))
    (when *print-strategy-updates*
      (format t "Frontwards-backwards found.~%"))
    (put-value-on-rsf 'front-back 'symmetry rsf))
   ('else nil)))


(defun find-add-em-ups-info (image)
  (cond ((not (eql (get-value (get-image-of *rsf*) 'add-em-up) #\_))
         nil)
        ((= (length image) 5) (find-add-em-ups-info-for-four image))
        ((= (length image) 4) (find-add-em-ups-info-for-three image))
        ('else nil)))

(defun find-add-em-ups-info-for-three (image)
  (let ((digit1 (nth 1 image))
        (digit2 (nth 2 image))
        (digit3 (nth 3 image)))
      (cond
       ((or (not (numberp digit1))
            (not (numberp digit2))
            (not (numberp digit3)))
        nil)
       ((= digit1 (+ digit2 digit3))
        (put-value-on-rsf 1 'add-em-up)
        (when *print-strategy-updates*
          (princ "Add-em-up found d1=d2+d3.")
          (terpri))
        t)
       ((= digit3 (+ digit1 digit2))
        (put-value-on-rsf 3 'add-em-up)
        (when *print-strategy-updates*
          (princ "Add-em-up found d3=d1+d2.")
          (terpri))
        t)
       ((= digit2 (+ digit1 digit3))
        (put-value-on-rsf 2 'add-em-up)
        (when *print-strategy-updates*
          (princ "Add-em-up found d2=d1+d3.")
          (terpri))
        t)
       ('else nil))))

(defun find-add-em-ups-info-for-four (image)
  (let ((digit1 (nth 1 image))
        (digit2 (nth 2 image))
        (digit3 (nth 3 image))
        (digit4 (nth 4 image)))
    (cond
       ((or (not (numberp digit1))
            (not (numberp digit2))
            (not (numberp digit3))
            (not (numberp digit4)))
        nil)
     ((= digit1 (+ digit2 digit3 digit4))
      (put-value-on-rsf 1 'add-em-up)
      (when *print-strategy-updates*
        (princ "Add-em-up found d1=d2+d3+d4.")
        (terpri))
      t)
     ((= digit2 (+ digit1 digit3 digit4))
      (put-value-on-rsf 2 'add-em-up)
      (when *print-strategy-updates*
        (princ "Add-em-up found d2=d1+d3+d4.")
        (terpri))
      t)
     ((= digit3 (+ digit1 digit2 digit4))
      (put-value-on-rsf 3 'add-em-up)
      (when *print-strategy-updates*
        (princ "Add-em-up found d3=d1+d2+d4.")
        (terpri))
      t)
     ((= digit4 (+ digit1 digit2 digit3))
      (put-value-on-rsf 4 'add-em-up)
      (when *print-strategy-updates*
        (princ "Add-em-up found d4=d1+d2+d3.")
        (terpri))
      t)
     ('else nil))))

;; Find-difference-info applies only to four digit numbers.  It finds if the 
;;  first and third digits or the second and
;;  fourth digits are the same.  If the first and third digits are the same 
;;  then it notes a 'ones-diff.  If the second and fourth digits are the
;;  same then it notes a '10s-diff.  If the third is one higher than the
;;  first and the fourth is one higher than the second then it notices an
;;  '11s-diff.
(defun find-difference-info (image)
  (let ((digit1 (nth 1 image))
        (digit2 (nth 2 image))
        (digit3 (nth 3 image))
        (digit4 (nth 4 image)))
    (cond
     ((or (not (numberp digit1))
          (not (numberp digit2))
          (not (numberp digit3))
          (not (numberp digit4)))
      nil)
     ((and (eql (get-value (get-image-of *rsf*) 'diff) #\_)
           (eql digit1 digit3))
      (when *print-strategy-updates*
        (format t "Ones-difference found.~%"))
      (put-value-on-rsf (- digit2 digit4) 'diff))
     ((and (eql (get-value (get-image-of *rsf*) 'diff) #\_)
           (eql digit2 digit4))
      (when *print-strategy-updates*
        (format t "Tens-difference found.~%"))
      (put-value-on-rsf (* 10 (- digit1 digit3)) 'diff))
     ((and (eql (get-value (get-image-of *rsf*) 'diff) #\_)
           (= digit3 (1+ digit1))
           (= digit4 (1+ digit2)))
      (when *print-strategy-updates*
        (format t "Elevens-difference found.~%"))
      (put-value-on-rsf 11 'diff))
     ('else nil))))

(defun use-net-to-find-the-code (&optional digit-list)
  "This routine uses the categories-net to find the semantic interpretation 
of the digit group that has been attached to the *rsf*"
  (let* ((object (create-object-for-sorting-to-the-code digit-list))
         (code (find-associate 'category object 2 'instant)))
    (time-tally-to-access-chunk)
    (when *print-major-updates*
      (format t "Semantic code found is: ~a~%" code))
    code))


(defun create-object-for-sorting-to-the-code (&optional digit-list)
  "This routine creates an object whose is-a is categories from the *rsf* for sorting to a code.
The result of this routine is fed into the use-net-to-find-the-code routine.
Two manipulations are performed on the object.  First, if the object has more 
than 4 subobjects as sometimes occurs at the end of a retrieval structure it is 
cut back to four because the routine for turning digits into categories is unable
to deal with lists of more than four digits.  Second, if there is an initial zero
on the object, that 0 is cut because this is what DD apparently does."
   (let ((object (cond (digit-list (cons nil (copy-list digit-list)))
                       ('else (specify (get-image-of *rsf*))))))
      (put-value object 'categories 'is-a)
      (when (> (length object) 4)
         (rplacd (nthcdr 4 object) nil))
      (when (eql (second object) 0)
         (rplacd object (cddr object)))
      object))
   

 
(defun search-for-contextual-codes? ()
  "This routine determines whether the strategy is to search for context cues
or not.  The strategy is to search for context cues only when there is
enough time and *use-contextual-codes* is true.  If both are true then this
routine charges the system for the search.  In all of the simulations reported
in our paper, *use-contextual-codes* was set to be true.  Setting it to
nil allows the experimenter to see how EPAM would perform if it was unable
to use contextual codes."
  (cond ((not *use-contextual-codes*) nil)
        ((out-of-time?)
         (when *print-strategy-updates*
           (format t "Not enough time to search for context cues!~%"))
         nil)
        ('else
         (time-tally-to-search-for-context-cue)
         t)))

(defun out-of-time? ()
  "This routine determines if not enough time for learning
operations to proceed.  There is no time left if there are already two
digits waiting in the auditory sensory store."
  (> (how-many-of-this-group? 'new (get 'auditory 'sensory-store)) 1))

(defun was-this-just-activated? (node)
  "Was-this-just-activated? returns t if the node was recently familiarized
and responds nil if it wasn't.  Activation only occurs when an image is studied.
Current version calls an image activated if the subjective interpretation of the 
time tag puts the time that it was activated within the last 3 hours."
   (when node
      (let ((recency (* (find-subjective-time-elapsed node)
                     (exp (* *standard-deviation-time* 
                             (find-random-deviation))))))
      (< recency 10800000))))

; 108000000 is 3 hours


(defun attach-semantic-code-to-parent (code)
  (let* ((parent (get *rsf* 'ancestor))
         (parent-branch (1+ (position *rsf* (get parent 'descendents)))))
     (put-value-on-rsf code parent-branch parent)))

(defun attach-back-to-back-codes ()
   (let* ((parent (get *rsf* 'ancestor))
          (image (get parent 'image))
          (parent-branch (1+ (position *rsf* (get parent 'descendents))))
          (previous-branch (if (= 1 parent-branch) nil (1- parent-branch))))
      (when previous-branch
         (when *print-strategy-updates*
            (format t "Searching for back-to-back-codes.~%"))
         (when (eql 
                 (test-value-extractor image parent-branch)
                 (test-value-extractor image previous-branch))
            (put-value-on-rsf t 'b2b-codes)
            (when *print-major-updates*
               (format t "Back-to-back semantic codes found.~%"))))))

;;                   K. REHEARSAL STAGE ROUTINES

(defun rehearsal-strategy (&optional stop-at-first-four-groups times-array)
  "Rehearsal-strategy.  The subject's strategy during rehearsal is to do each
of the following:
  1. Study last retrieval-structure-image until it is completely familiar and 
     re-associate retrieval-structure node with the semantic node.  Then go
     to the previous and back to the last to begin normal rehearsal with the
     last node.
  2. Try to find the code (i.e. the semantic interpretation category).  
     If it can't be found don't search categories net.
  3. If code is found, conduct a depth first search to find the semantic node
     and fill out the image in the retrieval structure with the digits found."
   (let ((beginning-time *clock*))
      (when *print-major-updates* (format t "BEGIN REHEARSAL STAGE: ~%"))
      (loop for rsf = *rsf* then (get-previous-retrieval-structure-node)
        with code = nil
        initially (progn (study-last-group-until-familiar)
                    (setf *rsf* (get-previous-retrieval-structure-node))
                    (setf *rsf* (get-next-retrieval-structure-node)))
        while (and rsf
                   (not (and stop-at-first-four-groups
                             (is-this-in-the-first-four-groups-of-four? rsf))))
        do (setf *rsf* rsf)
        (setf code (find-semantic-category))
        (fill-out-rs-image)
        (when (not code)
           (one-search-for-the-semantic-code)))
      (when *print-major-updates*
         (format t "Rehearsal stage took ~a msec.~%" 
           (- *clock* beginning-time)))
      (when times-array
         (setf (aref times-array *trial-position*)
               (append (aref times-array *trial-position*) 
                 (list (/ (- *clock* beginning-time) 1000.0)))))))

(defun study-last-group-until-familiar ()
   "changed with version h so only studies once."
   (let (object)
      (when *print-strategy-updates*
         (format t "Studying last set of digits.~%"))
       (setf object 
            (cons nil 
              (loop for item in (cut-leading-zero-off (cdr (get *rsf* 'image)))
                collect (specify item))))
      (nconc object (list '~))
      (put-value object (try-to-get-code *rsf*) 'is-a)
      (when (not (member #\_ object))
         (when (> *clock* *epam-clock*) (setf *epam-clock* *clock*))
         (study object)
         (create-link-from-semantic-objects-node-to-episode) ; this used to be an associate-chunks in
         ; 1994 version.  
         (advance-the-timer (- *epam-clock* *clock*)))))


(defun searches-for-the-semantic-code ()
  "This routine calls search-for-the-semantic-code.  If the first digit on the
*rsf* image is nil, it tries 0 first and searches the categories net.  If
it fails, it puts nil back in the head cell and searches the net again."
  (let (code)
    (if (eql #\_ (specify (second (get *rsf* 'image))))
      (progn (put-value-on-rsf 0 1)
             (setf code (search-for-the-semantic-code))
             (when (null code)
               (put-test-value-on (get *rsf* 'image) #\_ 1)
               (setf code (search-for-the-semantic-code))))
      (setf code (search-for-the-semantic-code)))
    code))

(defun search-for-the-semantic-code ()
  (loop 
    with beginning-time = *clock*
    with semantic-object = (turn-rs-object-into-semantic-object *rsf* nil)
    with last-codes = nil 
    for last-node = *net* then categories-node
    for categories-node = (search-categories-net-for-a-code semantic-object
                           last-node)
    for code = (find-associate 'category categories-node 2 'instant)
    do (when *print-strategy-updates* 
          (format t "Code found = ~a~%" code))
    if (not code) return nil
    else if (not (member code last-codes))
    do (setf last-codes (cons code last-codes))
    (put-value semantic-object code 'is-a)
    (search-for-semantic-node semantic-object)
    (when (car *chunks*)
       (attach-semantic-code-to-parent code)
       (fill-out-parent-image)
       (combine-image-with-chunk-image)
          (when *print-major-updates*
             (format t "Search for code succeeded in ~a msec!~%" 
               (- *clock* beginning-time)))
       (pop *chunks*)
       (return code))
    (pop *chunks*)
    finally (return nil)))

(defun one-search-for-the-semantic-code ()
   (let* ((beginning-time *clock*)
          (semantic-object (turn-rs-object-into-semantic-object *rsf*))
          (code (find-associate 
                  'category 
                  (search-categories-net-for-a-code semantic-object *net*)
                  2 
                  'instant)))
      (when code
         (put-value semantic-object code 'is-a)
         (search-for-semantic-node semantic-object)
         (if (car *chunks*)
            (progn (attach-semantic-code-to-parent code)
              (fill-out-parent-image)
              (combine-image-with-chunk-image)
              (when *print-major-updates*
                 (format t "Search for code succeeded in ~a msec!~%" 
                   (- *clock* beginning-time)))
              (pop *chunks*)
              code)
            (progn (pop *chunks*)
              nil)))))
  

;;                  L. ORDERED RECALL STAGE ROUTINES

(defun ordered-recall (&optional latency-array times-array)
  "This routine recalls the items of the retrieval structure.  The work of this
routine is mostly performed by the prepare-for-ordered-recall routine."
   (setf *rsf* (get-top-node-of-image *rsf*))
   (loop with starting-time = *clock*
     initially (when *print-major-updates*
                  (format t "BEGIN ORDERED RECALL: ~%")
                  (format t "Retrieval-structure:~%")
                  (print-retrieval-structure *rsf*))
     for rsf = (get-next-retrieval-structure-node)
     while rsf
     do (setf *rsf* rsf)
     (when *print-strategy-updates*
        (format t "Digits remaining in retrieval structure were: ~a~%"
          (cdr (get *rsf* 'image))))
     (prepare-for-ordered-recall)
     (output-digits-from-retrieval-structure *rsf* latency-array)
     (when *print-major-updates*
        (format t "Subject says digits were: ~a~%" *output*))
     (check-digits-experimenter)
     (when *new-stimulus-flag*
        (loop for n from 1 upto (1- (length (get-image-of *rsf*)))
          while *new-stimulus-flag*
          do (access-and-attach n)))
     finally (progn (when *print-major-updates*
                       (format t "Ordered recall took ~a msec.~%" 
                         (- *clock* starting-time)))
               (when times-array
                  (setf (aref times-array *trial-position*)
                        (append (aref times-array *trial-position*) 
                          (list (/ (- *clock* starting-time)
                                   1000.0))))))))



(defun prepare-for-ordered-recall ()
  "This routine performs the work of the ordered recall routine with a single
retrieval structure node in *rsf*.  The following algorithm is followed:
  1. Fill out the image at the rsf by using contextual code information if
any is available using the routine fill-out-rs-image.
  2. If the semantic interpretation is not attached to the image at the
*rsf* node's parent, then first
try to use contextual code information to find the code through the routine
find-semantic-category.  If that doesn't work, conduct searches for the
semantic interpretation using the routine searches-for-the-semantic-code. 
  3. If the image is complete, which means that all of the digits are present
at the retrieval-structure-node, then exit.
  4. Execute the routine search-for-the-digit-group to search for 
a node with information that will help fill out the retrieval structure node's
image."
   (let (code)
      (fill-out-rs-image)
      (when *print-strategy-updates*
         (format t "Partial image after adding information from context: ~a~%"
           (cdr (get *rsf* 'image))))
      (when (not (setf code (find-semantic-category)))
      (setf code (searches-for-the-semantic-code)))
      (when (and (not code) (not (is-image-complete?)))
         (setf code (searches-for-the-semantic-code)))
      (when *print-major-updates* 
         (format t "Semantic category found: ~a~%" code))
      (cond
       ((is-image-complete?)
        (when *print-strategy-updates*
           (format t "Image in retrieval-structure was complete!~%")))
       ((not code)
        (when *print-major-updates*
           (format t "Couldn't find a semantic code where search could start.~%")))
       ('else
        (when *print-strategy-updates*
           (format t "Semantic category = ~a~%" code))
        (search-for-the-digit-group code)))))

(defun one-search-for-the-digit-group (code)
   (let ((semantic-object (turn-rs-object-into-semantic-object *rsf* code)))
      (search-for-semantic-node semantic-object)
      (if (car *chunks*)
         (combine-image-with-chunk-image)
         (when *print-major-updates* 
            (format t 
              "Semantic chunk could not be found in search of semantic net!~%")))
      (pop *chunks*)
      ))



(defun search-for-the-digit-group (code)
   (let ((semantic-object (turn-rs-object-into-semantic-object *rsf* code)))
      (search-for-semantic-node semantic-object)
      (when (and (null (car *chunks*))
                 (eql #\_ (specify (second (get *rsf* 'image)))))
         (pop *chunks*)
         (when *print-major-updates*
            (format t "Trying out 0 in first digit position: ~a~%" semantic-object))
         (put-value-on-rsf 0 1)
         (setf semantic-object (turn-rs-object-into-semantic-object *rsf* code))
         (search-for-semantic-node semantic-object)
         (when (not (car *chunks*))
            (put-test-value-on (get *rsf* 'image) #\_ 1)))
      (if (car *chunks*)
         (combine-image-with-chunk-image)
         (when *print-major-updates* 
            (format t 
              "Semantic chunk could not be found in search of semantic net!~%")))
      (pop *chunks*)
      ))

(defun fill-out-parent-image ()
  "Fill-out-parent-image combines information from the children and information
from context cues in order to create as full an image of the list of
codes stored at the parent as possible."
  (let ((image (get-image-of (get *rsf* 'ancestor))))
    (when (equal-value image 'symmetry 'all-same) 
      (fill-out-image-using-all-same-information image (get *rsf* 'ancestor)))
    (when (equal-value image 'symmetry 'front-back)
      (put-symmetry-info-on image (get *rsf* 'ancestor)))
    (when (equal-value image 'progression 'ascending)
      (fill-out-image-using-progression image *ascending-races* (get *rsf* 'ancestor)))
    (when (equal-value image 'progression 'descending)
      (fill-out-image-using-progression image 
       (reverse *ascending-races*) (get *rsf* 'ancestor)))))

(defun equal-value (image attribute value)
   (let ((value2 (get-value-on image attribute)))
      (eql value2 value)))
         
        

(defun fill-out-image-using-progression (image progression &optional rsf)
  "This routine is called if the subobjects on an image are supposed to match
a portion of a progression.  The progression could be an alphabet, a series
of numbers, or, for DD races in the following order (quart_m half_m threequart_m
one_m) or the reverse of that order.  Nil values on the image are filled
in with values of the progression."
   (when (not rsf) (setf rsf *rsf*))
   (loop for lis on (cdr image)
     for item in (find-match-for (cdr image) progression)
     for n from 1
     when (eql #\_ (specify (car lis)))
     do (put-value-on-rsf item n rsf)))


(defun does-list1-match-first-elements-of-list2 (list1 list2)
  "This routine only works if list2 is at least as long as list1.  If that is 
true it returns t if all members of list1 are nil or match the members in same
positions on list2."
  (loop initially (when (< (length list2) (length list1)) (return nil))
        for item1 in list1
        for item2 in list2
        always (or (eql #\_ (specify item1)) (eql (specify item1) item2))))

(defun find-match-for (list1 list2)
  "This routine matches list1 against list2 until it finds a portion of list2 
which could match every element in list1.  It outputs nil if it could not find
such a portion, or outputs the portion of list2 if one was found."
  (loop for sublist on list2
        until (does-list1-match-first-elements-of-list2 list1 sublist)
        finally (return sublist)))

(defun fill-out-image-using-all-same-information (image rsf)
  "This routine is called if all of the subobjects on an image are supposed to
have the same value.  It finds that value, if it is still present, and then
replaces every null subobject with that value."
   (when (not rsf) (setf rsf *rsf*))
   (let ((value (loop for item in (cdr image)
                  for specify-item = (specify item)
                  if (not (eql specify-item #\_))
                  return specify-item)))
      (when value
         (loop for lis on (cdr image)
           for n from 1
           when (eql #\_ (specify (car lis)))
           do (put-value-on-rsf value n rsf)))))
           
(defun fill-out-rs-image ()
  "This routine fills out the digit information of the *rsf* with 
contextual code information in order to create as full an
image of the digits in the retrieval structure node as possible. "
   (let ((image (get *rsf* 'image)))
      (when (and (not (is-this-node-dead-last? *rsf*))
                 (or (eql #\_ (specify (car (last image))))
                     (eql #\_ (specify (test-value-finder image (- (length image) 2))))))
         (let (back-to-front-digits)
            (setf *rsf* (get-next-retrieval-structure-node))
            (setf back-to-front-digits (test-value-extractor (get-image-of *rsf*) 'b2b-digits))
            (setf *rsf* (get-previous-retrieval-structure-node))
            (cond ((eql back-to-front-digits #\_) nil)
                  ((eql back-to-front-digits 'single)
                   (when *print-major-updates*
                      (format t "Back-to-front-digits~%"))
                   (when (eql #\_ (car (last image)))
                      (put-value-on-rsf (get-first-digit-of-next-node) (1- (length image)))))
                  ((eql back-to-front-digits 'forward)
                   (when *print-major-updates* 
                     (format t "Back-to-front-digit-pairs-forward~%"))
                   (put-back-to-front-pairs-forward-info-on image))
                  ((eql back-to-front-digits 'reverse)
                   (when *print-major-updates* 
                      (format t "Back-to-front-digit-pairs-reverse~%"))
                  (put-back-to-front-pairs-reverse-info-on image)))))
      (when (equal-value image 'b2b-digits 'single)
         (when *print-major-updates*
            (format t "Back-to-back-digits~%"))
         (when (eql #\_ (nth 1 image))
            (put-value-on-rsf (get-last-digit-of-previous-node) 1)))
      (when (equal-value image 'b2b-digits 'forward)
         (when *print-major-updates*
            (format t "Back-to-back-digit-pairs-forward~%"))
         (put-back-to-back-pairs-forward-info-on image))
      (when (equal-value image 'b2b-digits 'reverse)
         (when *print-major-updates*
            (format t "Back-to-back-digit-pairs-reverse~%"))
         (put-back-to-back-pairs-reverse-info-on image))
      (when (eql #\_ (specify (car (last image))))
         (put-value-on-rsf (get-value-on image 'decimal) (1- (length image))))
      (when (get-value-on image 'symmetry)
         (when *print-major-updates* (format t "Symmetry~%"))
         (put-symmetry-info-on image))
      (when (get-value-on image 'add-em-up)
         (when *print-major-updates* (format t "Add-em-up~%"))
         (put-add-em-up-info-on image))
      (let ((difference (get-value-on image 'diff)))
         (when (numberp difference)
            (cond ((= (mod difference 10) 0)
                   (when *print-major-updates* (format t "Tens-difference~%"))
                   (put-tens-difference-info-on image difference))
                  ((= difference 11)
                   (when *print-major-updates* (format t "Elevens-difference~%"))
                   (put-elevens-difference-info-on image))
                  ('else
                   (when *print-major-updates* (format t "Ones-difference~%"))
                   (put-ones-difference-info-on image difference)))))))
              
      
(defun get-last-digit-of-previous-node ()
   (let (image)
      (setf *rsf* (get-previous-retrieval-structure-node))
      (setf image (get *rsf* 'image))
      (setf *rsf* (get-next-retrieval-structure-node))
      (cond ((null image) nil)
            ((not (eql #\_ (specify (car (last image)))))
             (specify (car (last image))))
            ('else (test-value-extractor image 'decimal)))))

(defun get-second-to-last-digit-of-previous-node ()
  (let (image)
    (setf *rsf* (get-previous-retrieval-structure-node))
    (setf image (get *rsf* 'image))
    (setf *rsf* (get-next-retrieval-structure-node))
    (if (null image)
      nil 
      (specify (second (reverse image))))))

(defun get-first-digit-of-next-node ()
  (let (image)
    (setf *rsf* (get-next-retrieval-structure-node))
    (setf image (get *rsf* 'image))
    (setf *rsf* (get-previous-retrieval-structure-node))
    (if (null image)
      nil 
      (specify (second image)))))

(defun get-second-digit-of-next-node ()
  (let (image)
    (setf *rsf* (get-next-retrieval-structure-node))
    (setf image (get *rsf* 'image))
    (setf *rsf* (get-previous-retrieval-structure-node))
    (if (null image)
      nil 
      (specify (third image)))))

(defun put-back-to-back-pairs-forward-info-on (image)
  "If the value of 'b2b-digits is forward, the second to last and last digits
on the previous node are the first and second digits on this image.  If the 
value is 'reverse than the second to last and last digits on the previous
node are the second and first digits on this image."
   (when (eql #\_ (specify (second image)))
      (put-value-on-rsf (get-second-to-last-digit-of-previous-node) 1))
   (when (eql #\_ (specify (third image)))
      (put-value-on-rsf (get-last-digit-of-previous-node) 2)))
     
(defun put-back-to-back-pairs-reverse-info-on (image)
  "If the value of 'b2b-digits is forward, the second to last and last digits
on the previous node are the first and second digits on this image.  If the 
value is 'reverse than the second to last and last digits on the previous
node are the second and first digits on this image."
   (when (eql #\_ (specify (third image)))
      (put-value-on-rsf (get-second-to-last-digit-of-previous-node) 2))
   (when (eql #\_ (specify (second image)))
      (put-value-on-rsf (get-last-digit-of-previous-node) 1)))



(defun put-back-to-front-pairs-forward-info-on (image)
  "The first and second digits of the next node are the second-to-last and
last digits of this image."
   (when (eql #\_ (specify (nth (- (length image) 2) image)))
      (put-value-on-rsf (get-first-digit-of-next-node) (- (length image) 2)))
   (when (eql #\_ (specify (car (last image))))
      (put-value-on-rsf (get-second-digit-of-next-node) (1- (length image)))))

(defun put-back-to-front-pairs-reverse-info-on (image)
  "The first and second digits of the next node are the last and
second-to-last digits of this image."
   (when (eql #\_ (specify (nth (- (length image) 2) image)))
      (put-value-on-rsf (get-second-digit-of-next-node) (- (length image) 2)))
   (when (eql #\_ (specify (car (last image))))
      (put-value-on-rsf (get-first-digit-of-next-node) (1- (length image)))))

(defun get-semantic-code-from-previous-node ()
  (let (code)
    (setf *rsf* (get-previous-retrieval-structure-node))
    (setf code (try-to-get-code *rsf*))
    (setf *rsf* (get-next-retrieval-structure-node))
    code))

(defun get-semantic-code-from-next-node ()
  (let (code)
    (setf *rsf* (get-next-retrieval-structure-node))
    (setf code (try-to-get-code *rsf*))
    (setf *rsf* (get-previous-retrieval-structure-node))
    code))

(defun get-code-from-next-node-if-it-has-back-to-back-attached ()
  (let (code)
    (setf *rsf* (get-next-retrieval-structure-node))
    (when (eql t (test-value-extractor (get *rsf* 'image) 'b2b-codes))
      (setf code (try-to-get-code *rsf*)))
    (setf *rsf* (get-previous-retrieval-structure-node))
    code))

(defun find-semantic-category ()
  "This routine finds the semantic-interpretation of the digit group and.
attaches it to the *rsf* node's parent.
  1.  If the semantic category is already associated with the parent, then
exit with that category.
  2.  If 'b2b-codes is associated with this digit group, then if a 
semantic code is associated with the previous group, then associate
its code with the current group.
  3. If 'b2b-codes is associated with the next digit group, then if a
semantic code is associated with the next group, then associate its
code with the current group.
  4.  If the semantic category is part of the image of the chunk associated
as the semantic-pattern of the parent chunk, then use that category.
  5.  Fill out the parent image using context cues that may be attached to
the parent.  See if it was possible to find the semantic category through
that action.
  6.  If the category is found, attach the category to the image and exit
with the category else exit nil."
   (let ((code (try-to-get-code *rsf*))
         (image (get *rsf* 'image)))
      (when (null code)
         (when (get-value-on image 'b2b-codes)
            (setf code (get-semantic-code-from-previous-node)))
         (when (null code)
            (fill-out-parent-image)
            (setf code (try-to-get-code *rsf*)))
         (when (and (null code)
                    (not (is-this-a-last-node? *rsf*)))
            (setf code (get-code-from-next-node-if-it-has-back-to-back-attached)))
         (when code 
            (attach-semantic-code-to-parent code)
            (fill-out-parent-image)))
      code))

(defun get-value-on (object attribute)
   (let ((result (test-value-extractor object attribute)))
      (cond ((eql result #\_) nil)
            ('else result))))

(defun try-to-get-code (rs-node)
  (let* ((parent (get rs-node 'ancestor))
         (parent-image (get parent 'image))
         (parent-branch (1+ (position rs-node (get parent 'descendents))))
         (code (test-value-extractor parent-image parent-branch)))
     (cond ((eql code #\_) nil)
           ('else code))))
  
(defun combine-image-with-chunk-image ()
   (let* ((rs-image (get *rsf* 'image))
          (image (get-image-of (car *chunks*)))
          (length-rs-image (test-value-finder rs-image 'length))
          (length-image (test-value-finder image 'length)))
      ;(format t "image = ~a, rs-image = ~a," (cdr image) (cdr rs-image))
      (cond ((null length-image) nil)
            ((eql length-image #\_)
             (cond ((eql (test-value-extractor rs-image 1) 0)
                    (put-values-of-image-on-rsimage image rs-image 1))
                   ((eql (test-value-extractor rs-image 1) #\_)
                    (cond ((do-values-superimpose? image rs-image)
                           (put-values-of-image-on-rsimage image rs-image))
                          ('else
                           (put-value-on-rsf 0 1)
                           (put-values-of-image-on-rsimage image rs-image 1))))
                   ('else
                    (put-values-of-image-on-rsimage image rs-image))))
            ((= length-image length-rs-image)
             (put-values-of-image-on-rsimage image rs-image))
            ((= (1+ length-image) length-rs-image)
             (when (not (eql (test-value-extractor rs-image 1) 0))
                (put-value-on-rsf 0 1))
             (put-values-of-image-on-rsimage image rs-image 1))))
   ;(format t "result ~a~%" (cdr (get *rsf* 'image)))
   )
      
(defun do-values-superimpose? (image rs-image)
   (loop for image-value in (cdr image)
     for rs-value in (cdr (specify rs-image))
     if (eql rs-value #\_) do 'nil
     else if (or (eql image-value rs-value)
                 (eql image-value #\_)
                 (null image-value)
                 (eql image-value '~))
     do 'nil
     else if t return nil
     finally (return t)))
             
(defun put-values-of-image-on-rsimage (image rs-image &optional offset)
   "This routine assumes that the image is in the *rsf*"
   (when (not offset) (setf offset 0))
   (loop for n from 1
     for image-value in (cdr image)
     for rs-value in (nthcdr (1+ offset) rs-image)
     if (and (eql rs-value #\_)
             image-value
             (not (eql image-value #\_))
             (not (eql image-value '~)))
     do (put-value-on-rsf image-value (+ offset n))))

     
     
     
                          
(defun put-add-em-up-info-on (image)
  "This routine puts digits on the image if there are enough digits
available to specify an unknown digit.  If the pattern for an add-em-up
is  '(1 2 3)  then the first digit = the second digit + the third digit)." 
  (cond ((= (length image) 4)
         (put-add-em-up-info-on-three-digit-image image))
        ((= (length image) 5)
         (put-add-em-up-info-on-four-digit-image image))))

(defun put-add-em-up-info-on-three-digit-image (image)
  (let* ((position (specify (get-value image 'add-em-up)))
         (pattern (cond ((eql position 1) '(1 2 3))
                        ((eql position 2) '(2 1 3))
                        ((eql position 3) '(3 1 2))))
         (digit1 (specify (nth (first pattern) image)))
         (digit2 (specify (nth (second pattern) image)))
         (digit3 (specify (nth (third pattern) image))))
    (cond
     ((and (not (eql digit1 #\_))
           (not (eql digit2 #\_))
           (not (eql digit3 #\_)))
      nil)
     ((and (eql #\_ digit1) (eql #\_ digit2)) nil)
     ((and (eql #\_ digit1) (eql #\_ digit3)) nil)
     ((and (eql #\_ digit2) (eql #\_ digit3)) nil)
     ((eql #\_ digit1)
      (put-value-on-rsf (+ digit2 digit3) (first pattern)))
     ((eql #\_ digit2) 
      (put-value-on-rsf (- digit1 digit3) (second pattern)))
     ((eql #\_ digit3) 
      (put-value-on-rsf (- digit1 digit2) (third pattern)))
     ('else nil))))

(defun put-add-em-up-info-on-four-digit-image (image)
   (let* ((position (specify (get-value image 'add-em-up)))
          (pattern (cond ((eql position 1) '(1 2 3 4))
                         ((eql position 2) '(2 1 3 4))
                         ((eql position 3) '(3 1 2 4))
                         ((eql position 4) '(4 1 2 3))))
          (digit1 (specify (nth (first pattern) image)))
          (digit2 (specify (nth (second pattern) image)))
          (digit3 (specify (nth (third pattern) image)))
          (digit4 (specify (nth (fourth pattern) image))))
      (cond
       ((and (not (eql digit1 #\_))
             (not (eql digit2 #\_))
             (not (eql digit3 #\_))
             (not (eql digit4 #\_)))
        nil)
       ((and (eql #\_ digit1) (eql #\_ digit2)) nil)
       ((and (eql #\_ digit1) (eql #\_ digit3)) nil)
       ((and (eql #\_ digit1) (eql #\_ digit4)) nil)
       ((and (eql #\_ digit2) (eql #\_ digit3)) nil)
       ((and (eql #\_ digit2) (eql #\_ digit4)) nil)
       ((and (eql #\_ digit3) (eql #\_ digit4)) nil)
       ((eql #\_ digit1)
        (put-value-on-rsf (+ digit2 digit3 digit4)(first pattern)))
       ((eql #\_ digit2) 
        (put-value-on-rsf (- digit1 digit3 digit4)(second pattern)))
       ((eql #\_ digit3) 
        (put-value-on-rsf (- digit1 digit2 digit4)(third pattern)))
       ((eql #\_ digit4) 
        (put-value-on-rsf (- digit1 digit2 digit3)(fourth pattern)))
       ('else nil))))

(defun put-elevens-difference-info-on (image)
  "Third and fourth digits are one higher than first and second digits."
  (let ((digit1 (specify (nth 1 image)))
        (digit2 (specify (nth 2 image)))
        (digit3 (specify (nth 3 image)))
        (digit4 (specify (nth 4 image))))
     (when (and (numberp digit1)
                (eql #\_ digit3))
        (put-value-on-rsf (1+ digit1) 3))
     (when (and (numberp digit2)
                (eql #\_ digit4))
        (put-value-on-rsf (1+ digit2) 4))
     (when (and (numberp digit3)
                (eql #\_ digit1))
        (put-value-on-rsf (1- digit3) 1))
     (when (and (numberp digit4)
                (eql #\_ digit2))
        (put-value-on-rsf (1- digit4) 2))))

(defun put-tens-difference-info-on (image difference)
  "If there is a tens difference, the second and
fourth digit are assumed to be the same."
   (let ((diff (/ difference 10))
         (digit1 (specify (nth 1 image)))
         (digit2 (specify (nth 2 image)))
         (digit3 (specify (nth 3 image)))
         (digit4 (specify (nth 4 image))))
      (cond
       ((eql #\_ digit2) (put-value-on-rsf digit4 2))
       ((eql #\_ digit4) (put-value-on-rsf digit2 4)))
      (cond
       ((and (eql #\_ digit1)
             (numberp digit3))
        (put-value-on-rsf (+ diff digit3) 1))
       ((and (eql #\_ digit3)
             (numberp digit1))
        (put-value-on-rsf (- digit1 diff) 3)))))

(defun put-ones-difference-info-on (image diff)
   "If there is a ones difference, the first and third digit
 are assumed to be the same."
   (let ((digit1 (specify (nth 1 image)))
         (digit2 (specify (nth 2 image)))
         (digit3 (specify (nth 3 image)))
         (digit4 (specify (nth 4 image))))
      (cond
       ((eql #\_ digit1)
        (put-value-on-rsf digit3 1))
       ((eql #\_ digit3)
        (put-value-on-rsf digit1 3)))
      (cond
       ((and (eql #\_ digit2)
             (numberp digit4))
        (put-value-on-rsf (+ diff digit4) 2))
       ((and (eql #\_ digit4)
             (numberp digit2))
        (put-value-on-rsf (- digit2 diff) 4)))))
 
(defun put-symmetry-info-on (image &optional rsf)
   (when (not rsf) (setf rsf *rsf*))
   (cond ((= (length image) 5) 
          (equalize 1 4 image rsf)
          (equalize 2 3 image rsf))
         ((= (length image) 4)
          (equalize 1 3 image rsf))))

(defun equalize (pos1 pos2 image rsf)
  "This routine takes an image and two attributes as inputs.  If the value
of one of the attributes is missing it is set equal to the value of the
other attribute."
  (let ((digit1 (specify (nth pos1 image)))
        (digit2 (specify (nth pos2 image))))
    (cond
     ((and (not (eql digit1 #\_))
           (not (eql digit2 #\_))) nil)
     ((eql digit1 #\_)
      (put-value-on-rsf digit2 pos1 rsf))
     ((eql digit2 #\_)
      (put-value-on-rsf digit1 pos2 rsf)))))

(defun output-digits-from-retrieval-structure (node &optional latency-array)
  (let ((output (mapcar 'specify (cdr (get node 'image)))))
    (loop repeat (length output)
          do (when latency-array
               (setf (aref latency-array *list-number*)
                     (cons *clock* (aref latency-array *list-number*))))
          (time-tally-to-vocalize-digit))
    (setf *output* output)))

(defun cut-leading-zero-off (digits)
  (if (eql (specify (car digits)) 0)
    (cdr digits)
    digits))


;;                   M. FREE RECALL STAGE ROUTINES

;; Free recall strategy
(defun free-recall-strategy (&optional times-array)
   (setf *free-recall-found* (cons nil *free-recall-found*))
   (let ((categories '(quart_m half_m threequart_m one_m three_k fast_2m slow_2m
                        three_m ten_k ten_m date age dbl_age misc))
         (starting-time *clock*)
         seconds-taken)
      (when *print-major-updates*
         (princ "BEGIN FREE RECALL: ")
         (terpri))
      (loop for category in categories
        for semantic-object = (make-semantic-object-for category)
        do (when *print-major-updates* 
              (format t "Searching for ~a~%" category))
        (loop repeat 1
          do
          (setf *chunks* (cons *net* *chunks*))
          (free-recall-search semantic-object)
          (loop while (car *chunks*)
            do (output-associates-of-semantic-node)
            (free-recall-search semantic-object)
            finally (pop *chunks*))))
      (setf seconds-taken (/ (- *clock* starting-time) 1000.0))
      (when *print-major-updates* 
         (format t "Free recall took ~a seconds~%" seconds-taken))
      ;(break)
      (when times-array
         (setf (aref times-array 3)
            (append (aref times-array 3) (list seconds-taken)))))
   (setf *print-epam-updates* nil))

(defun make-semantic-object-for (category)
  (let (semantic-object)
    (cond ((member category '(quart_m age))
           (setf semantic-object (list nil #\_ #\_ #\_ '~)))
          ('else
           (setf semantic-object (list nil #\_ #\_ #\_ #\_ #\_ '~))))
     (put-value semantic-object category 'is-a)
     semantic-object))
          
(defun output-associates-of-semantic-node ()
  "This routine is called by the free-recall routine when the system
wants to output the digits from all of the retrieval structure nodes associated
with the semantic node that is in (car *chunks*)"
  (let* ((node (car *chunks*))
         (associates (find-episodes 'episode node)))
     (loop for associate in associates
       if (and (was-this-just-activated? associate)
               (not (eql associate *rsf*))) ; this could prevent the same *rsf* from being output from both child and parent nodes.
       do (setf *output* nil)
       (setf *rsf* associate)
       (when (not (test-value-extractor (get-image-of *rsf*) 'already-recalled))
          (combine-image-with-chunk-image)
          (output-digits-from-retrieval-structure *rsf*)
          (when *record-free-recall* (record-free-recall))
          (put-value-on-rsf t 'already-recalled)
          (when *print-major-updates*
             (format t "Subject says digits were: ~a~%" *output*))))))

(defun record-free-recall ()
   (rplaca *free-recall-found* 
     (append (car *free-recall-found*) (list *output*))))

(defun display-free-recall ()
   (format t "~%For digit-groups output by EPAM, the positions of EPAM's groups~%")
   (format t "in DD's output were the following with nil indicating that the group~%")
   (format t "was not output by DD:~%")
   (loop for recalled-items in (reverse *free-recall-found*)
     for dds-free-recall-group in *free-recall-data*
     for n from 1
     for order = (loop 
                   with order-of-items = nil
                   for ritem in recalled-items
                   for position-in-list = (loop for item in dds-free-recall-group
                                            for i from 1
                                            for digits = (car item)
                                            if (and (not (member i order-of-items))
                                                    (equal ritem digits))
                                            return i)
                   do (setf order-of-items 
                            (append order-of-items (list position-in-list)))
                   finally (return order-of-items))
     do (format t "For session ~a order was: ~a~%" n order))
   (loop for recalled-items in (reverse *free-recall-found*)
     for free-recall-group in *free-recall-groups*
     for n from 1
     with list1errors = 0
     with list2errors = 0
     with list3errors = 0
     do (loop 
          with extra-items = nil
          for omitted-items = (reorder-list-randomly free-recall-group) 
          then remaining-items
          for ritem in recalled-items
          for remaining-items = (loop with i = nil
                                  for item in omitted-items
                                  for digits = (car item)
                                  if (and (null i)
                                          (equal ritem digits))
                                  do (setf i t)
                                  else if t
                                  collect item
                                  finally (when (not i) 
                                             (setf extra-items (cons ritem extra-items)
                                                   )))
          finally (progn 
                    (when *print-major-updates* 
                       (format t "Extra-items on list ~a = ~a~%" n extra-items)
                       (format t "Omitted-items on list ~a = ~a~%" n omitted-items))
                    (loop for oitem in omitted-items
                      if (= (second oitem) 1) do (incf list1errors)
                      if (= (second oitem) 2) do (incf list2errors)
                      if (= (second oitem) 3) do (incf list3errors))))
     finally
     (progn
      (format t "~%Percentage of Free Recall Omissions by Position of list~%")
      (format t "within session for DD and EPAM.~%")
      (format t "Position   DD-omissions    EPAM-omissions~%")
      (format t "    1          20.0            ~5,1f~%" (/ list1errors 2.9))
      (format t "    2           6.6            ~5,1f~%" (/ list2errors 2.9))
      (format t "    3           3.1            ~5,1f~%" (/ list3errors 2.9))))
   )

(defun probe-task (list-of-probes)
  "The first cell in the list of probes is either 'forward or 'backward."
  (loop with direction = (car list-of-probes)
    for sublist in (cdr list-of-probes)
    for probe-group = (first sublist)
    for correct-response = (second sublist)
    for probe-location = (third sublist)
    for digits = (cut-leading-zero-off probe-group)
    for code = (use-net-to-find-the-code digits)
    for object = (cons (list (list 'is-a code)) digits)
    for sorting-clock = *sorting-clock*
    for node = (get-chunk-for object)
    for starting-time = *clock*
    for rsfs = (find-episodes 'episode node)
    for rsf = (loop for item in rsfs
                when (= (number-of-digits item) (length probe-group))
                return item)
    if (not rsf)
    do (when *print-major-updates* 
          (format t "~%Can't find the probe digits in the list.~%")
          (format t "Experimenter says response should have been: ~a~%"
            correct-response))
    else do (setf *sorting-clock* (- *sorting-clock* sorting-clock))
    (time-tally-to-sort-through-net)
    (time-tally-to-orient-to-probe)
    (when *print-major-updates*
       (format t "~%Digits presented were: ~a~%" probe-group))
    (setf *rsf* rsf)
    (if (eq direction 'forwards)
       (setf *rsf* (get-next-retrieval-structure-node))
       (setf *rsf* (get-previous-retrieval-structure-node)))
    (prepare-for-ordered-recall)
    (when *print-major-updates*
       (format t "Response found in ~a msec.~%" 
         (- *clock* starting-time)))
    (cond ((eq direction 'forwards)
           (setf (aref *forward-latencies-found* probe-location)
                 (cons (- *clock* starting-time) 
                   (aref *forward-latencies-found* probe-location))))
          ('else
            (setf (aref *backward-latencies-found* probe-location)
                 (cons (- *clock* starting-time) 
                   (aref *backward-latencies-found* probe-location)))))
    (output-digits-from-retrieval-structure *rsf*)
    (when *print-major-updates*
       (format t "Subject says response is: ~a~%" *output*)
       (format t "Experimenter says response should have been: ~a~%"
         correct-response))
    ))


;;                    N. Overall Strategies
       
(defun retrieval-structure-practice-day (n &optional digit-lists times-array no-free-recall)
  "This routine executes a typical day of learning in a
retrieval structure.  It returns the number of digits that should be
used during the next session.  If no-free-recall is true, it omits free-recall(.  If digit lists are
constructed randomly, the number of digits in each list is controlled by
n, and the number of lists per session is controlled by the variable
*how-many-lists-per-session*."
   (increment-activation)
   (when *print-major-updates*
      (format t "~%This is session number ~a~%" *activation*))
   (loop for x from 1 to (if digit-lists 
                            (length digit-lists) 
                            *how-many-lists-per-session*)
     do (setf *trial-position* (1- x))
     (setf *list-of-errors* nil)
     (zero-out-variables)
     (if digit-lists
        (instantiate-retrieval-structure (length (car digit-lists)))
        (instantiate-retrieval-structure n))
     (setf *clock* 0
           *epam-clock* 0)
     (if digit-lists
        (create-stimulus-list-from (car digit-lists))
        (create-random-list-of-digits n))
     (setf digit-lists (cdr digit-lists))
     (setf *experimenter* 'auditory-span-experimenter)
     (study-digit-list-strategy)
     (setf *experimenter* 'null-experimenter)
     (rehearsal-strategy 'stop-at-first-four-groups-of-four times-array)
     (ordered-recall nil times-array)
     (if (= *errors* 0) (incf n) (decf n))
     (when (< n 6) (incf n))
     (when *print-major-updates*
        (format t "Number of errors on list were: ~a~%" *errors*))
     (when times-array
        (setf (aref times-array (+ *trial-position* 6))
              (append (aref times-array (+ *trial-position* 6)) 
                (list *errors*)))) 
     (when *print-major-updates*
        (format t "Next list will have length: ~a~%" n))
     )
   (when (not no-free-recall) (free-recall-strategy times-array))
   n)


(defun practice-day-with-free-recall (digit-lists &optional times-array)
  "This routine does 3 lists with free recall and reporting of latencies between
digits and digit groups."
   (increment-activation)
   (format t "~%This is session number ~a~%" *activation*)
   (loop for x from 1 to 3
     do (setf *trial-position* (1- x))
     (setf *list-of-errors* nil)
     (incf *list-number*)
     (zero-out-variables)
     (instantiate-retrieval-structure (length (car digit-lists)))
     (setf *clock* 0
           *epam-clock* 0)
     (create-stimulus-list-from (pop digit-lists))
     (setf *experimenter* 'auditory-span-experimenter)
     (study-digit-list-strategy)
     (setf *experimenter* 'null-experimenter)
     (rehearsal-strategy 'stop-at-first-four-groups-of-four times-array)
     (ordered-recall *latency-array* times-array)
     (princ "Number of errors on list were: ")
     (princ *errors*)
     (terpri)
     (format t "ERRORS: ~a~%" *list-of-errors*))
   (free-recall-strategy times-array))

(defun practice-day-with-probe-task (digit-lists probe-lists)
   (increment-activation)
   (when *print-major-updates* 
      (format t "~%This is session number ~a~%" *activation*))
   (dotimes (count 8)
      (setf *list-of-errors* nil)
      (zero-out-variables)
      (instantiate-retrieval-structure (length (car digit-lists)))
      (setf *clock* 0
            *epam-clock* 0)
      (create-stimulus-list-from (pop digit-lists))
      (setf *experimenter* 'auditory-span-experimenter)
      (study-digit-list-strategy)
      (setf *experimenter* 'null-experimenter)
      (rehearsal-strategy)
      (probe-task (pop probe-lists))))

(defun rs-growth-session (sessions &optional net-file use-current-net)
   (cond (use-current-net nil)
         (net-file 
           (initialize-variables)
           (read-net-from net-file))
        ('else 
         (initialize-variables)
         (create-categories-net)
         (initialize-retrieval-structure)))
  (format t "Number of nodes in net at beginning: ~a~%" *nodes-in-net*)
  (format t "Number of lists to be presented per session: ~a~%"
          *how-many-lists-per-session*)
  (setf *presentation-rate* 1000)
  (setf *stimulus-duration-rate* 200)
  (setf *signal-to-respond-delay* 4000)
  (setf *method-for-counting-syllables* 'subobject-method)
  (loop repeat sessions
    do (setf *current-list-length* 
             (retrieval-structure-practice-day *current-list-length*))
       (when (< *current-list-length* 6) (setf *current-list-length* 6))
       (push *current-list-length* *record-of-rs-growth*)
       (push *nodes-in-net* *record-of-nodes-in-net*))
  (print-out-record-of-rs-growth)
  (print-out-record-of-nodes-in-net)
  (format t "Last session was session: ~a~%" *activation*)
  (format t "Number of digits next session: ~a~%" *current-list-length*)
  (format t "Number of nodes in net: ~a~%" *nodes-in-net*))

(defun retrieval-structure-day (n &optional digit-lists times-array no-free-recall)
  "This routine does several lists of a given length"
   (setf *presentation-rate* 1000)
   (setf *stimulus-duration-rate* 200)
   (setf *signal-to-respond-delay* 4000)
   (setf *method-for-counting-syllables* 'subobject-method)
   (retrieval-structure-practice-day n digit-lists times-array no-free-recall))

(defun display-codes-found ()
   (format t "Here are the codes found by EPAM during the study stage over the course of~%")
   (format t "the contextual codes simulation.  Depending upon the forgetting~%")
   (format t "parameter, a proportion of these codes were forgotten before~%")
   (format t "the rehearsal stage.  For example if the forgetting-parameter~%")
   (format t "is 20 then 20% of the codes reported here would have been~%")
   (format t "forgotten by EPAM.~%")
    (loop for first-list-codes in (reverse (aref *codes-found* 0))
     for second-list-codes in (reverse (aref *codes-found* 1))
     for third-list-codes in (reverse (aref *codes-found* 2))
     for fourth-list-codes in (reverse (aref *codes-found* 3))
     for fifth-list-codes in (reverse (aref *codes-found* 4))
     for sixth-list-codes in (reverse (aref *codes-found* 5))
     for session from 2
     do (format t "~% Session ~a, List 1~%" session)
     (loop for code in first-list-codes
          do (format t "~a~%" code))
     (format t "~% Session ~a, List 2~%" session)
     (loop for code in second-list-codes
          do (format t "~a~%" code))
     (format t "~% Session ~a, List 3~%" session)
     (loop for code in third-list-codes
          do (format t "~a~%" code))
     (format t "~% Session ~a, List 4~%" session)
     (loop for code in fourth-list-codes
          do (format t "~a~%" code))
     (format t "~% Session ~a, List 5~%" session)
     (loop for code in fifth-list-codes
          do (format t "~a~%" code))
     (format t "~% Session ~a, List 6~%" session)
     (loop for code in sixth-list-codes
          do (format t "~a~%" code))))
     
   



; Expert Memory Simulations

(defun retrieval-structure-growth-simulation (file-name)
  "A string must be supplied to this routine (a string is a word in quotes).
It will become the root of the name of net files that will be created by this 
routine. This routine performs a simulation of DD's learning over the course of 
173 5-session blocks.  Nets are produced through out and are turned into files 
which are save on the epamcode directory.  The file net135, which is used
in the other simulations was created by this routine when this routine was
fed the file name net (in quotes).  Nets created by this routine can be 
used in simulations."
   (setf *print-time-updates* nil) 
   (setf *print-imagery-stores* nil) 
   (setf *print-experimenter-updates* nil) 
   (setf *print-epam-updates* nil) 
   (setf *print-strategy-updates* nil)
   (setf *print-forgetting-updates* nil)
   (setf *print-major-updates* nil) 
   (setf *use-contextual-codes* t) 
   (setf *record-codes-found* nil)
   (setf *record-free-recall* nil)
   (retrieval-structure-growth file-name))


(defun retrieval-structure-growth (file-name)
  (let (net-file)
     (seed-random-generator)
     (setf *activation* 0)
     (setf *nodes-in-net* 0)
     (setf *record-of-rs-growth* nil)
     (setf *record-of-nodes-in-net* nil)
     (setf *how-many-lists-per-session* 25)
     (setf *current-list-length* 7)
     (rs-growth-session 15)
     (setf *how-many-lists-per-session* 15)
     (rs-growth-session 5 net-file 'use-current-net)
     (setf *how-many-lists-per-session* 10)
     (rs-growth-session 5 net-file 'use-current-net)
     (setf *how-many-lists-per-session* 8)
     (rs-growth-session 15 net-file 'use-current-net)
     (setf *how-many-lists-per-session* 6)
     (rs-growth-session 180 net-file 'use-current-net)
     (setf *how-many-lists-per-session* 5)
     (rs-growth-session 45 net-file 'use-current-net)
     (setf *how-many-lists-per-session* 4)
     (rs-growth-session 160 net-file 'use-current-net)
     (setf *how-many-lists-per-session* 4)
     (rs-growth-session 50 net-file 'use-current-net)
     (setf *how-many-lists-per-session* 3)
     (rs-growth-session 390 net-file 'use-current-net)
     (setf net-file (concatenate 'string file-name "173"))
     (write-net-to net-file)))


(defun ordered-recall-and-free-recall-simulation ()
  "This routine simulates DD's performance on the set of lists where he passed
100 in his memory span.  Free report order and latencies between digit groups
can be calculated from this data.  This routine creates a logfile where the
data should go.  The logfile is a string name (a word in double quotes).  The
log file shows the latencies between digits for each digit on each list."
   (setf *net-file* "net173")
   (setf *activation* 868) ; This is the activation after a weekend and 173 blocks
   ; (+ (* 173 5) 2)
   (setf *digit-lists* (list *session858*
                         *session859*
                         *session860*
                         *session861*
                         *session862*
                         *session863*
                         *session864*
                         *session865*
                         *session866*
                         *session868*))
   (setf *print-time-updates* nil) 
   (setf *print-imagery-stores* nil) 
   (setf *print-experimenter-updates* nil) 
   (setf *print-epam-updates* nil) 
   (setf *print-strategy-updates* nil) 
   (setf *print-forgetting-updates* nil)
   (setf *print-major-updates* nil) 
   (setf *use-contextual-codes* t) 
   (setf *record-codes-found* nil)
   (ordered-recall-and-free-recall))

(defun ordered-recall-and-free-recall ()
  "Inputs are a net-file such as rs100, a file where latencies will be written
such as latency.dat and a list of lists of digit lists such as a list of
*session858* *session859* and *session860*"
   (let ((times-array (make-array 4)))
      (seed-random-generator)
      (setf *record-free-recall* t)
      (setf *free-recall-found* nil)
      (initialize-variables)
      (read-net-from *net-file*)
      ;(increment-activation)
      (setf *presentation-rate* 1000)
      (setf *stimulus-duration-rate* 200)
      (setf *signal-to-respond-delay* 4000)
      (setf *method-for-counting-syllables* 'subobject-method)
      (setf *list-number* -1)
      (setf *latency-array* (make-array (* 3 (length *digit-lists*))))
      (loop for digit-lists in *digit-lists*
        do (practice-day-with-free-recall digit-lists times-array))
      (format t "~%Free recall times were:~%~a~%" (aref times-array 3))
      (display-free-recall)
      (write-latency-times)
      (setf *times-array* times-array)
      (format t "~%DD's and EPAM's Mean Performance Times~%")
      (format t "(in Seconds) on 100+ Digit Lists~%")
      (format t "-------------------------------------~%")
      (format t "   Measure              DD     EPAM  ~%")
      (format t "-------------------------------------~%")
      (format t "Rehearsal time         190    ~5,0f  ~%"
        (/ (loop for a from 0 upto 2
             sum (loop for lis on (aref times-array a) by #'cddr
                   sum (car lis)))
           30))
      (format t "Ordered-recall time    200    ~5,0f  ~%"
        (/ (loop for a from 0 upto 2
             sum (loop for lis on (aref times-array a) by #'cddr
                   sum (second lis)))
           30))
      (format t "Free-recall time       504    ~5,0f  ~%"
        (/ (loop for item in (aref times-array 3)
             sum item)
           10))))


(defun contextual-codes-simulation ()
  "This is a simulation of DD's performance over 8 sessions each with 3 
lists that have been enriched with context cues and three lists which have
been depleted of context cues.  Enriched lists are the following:
     *session2*: list2, list3, list6
     *session3*: list1, list2, list5
     *session4*: list1, list4, list5
     *session5*: list3, list4, list6
     *session6*: listl, list4, list5
     *session7*: list2, list3, list6
     *session8*: list3, list4, list6
     *session9*: list1, list2, list5
The other lists are depleted lists."
   (setf *net-file* "net173")
   (setf *number-of-sessions* 12)
   (setf *activation* 868) ; This is the activation after a weekend and 173 blocks
                           ; (+ (* 173 5) 2)
   (setf *digit-lists* (list *session2*
                         *session3*
                         *session4*
                         *session5*
                         *session6*
                         *session7*
                         *session8*
                         *session9*))
   (setf *print-time-updates* nil) 
   (setf *print-imagery-stores* nil) 
   (setf *print-experimenter-updates* nil) 
   (setf *print-epam-updates* nil) 
   (setf *print-strategy-updates* nil) 
   (setf *print-forgetting-updates* nil)
   (setf *print-major-updates* nil) 
   (setf *use-contextual-codes* t) 
   (setf *record-free-recall* nil)
   (setf *number-of-sessions* 12)
   (setf *times-array* (make-array 12))
   (loop repeat *number-of-sessions*
     do (contextual-codes))
   (report-contextual-codes-results))

(defun contextual-codes ()
   (setf *record-codes-found* t)
   (setf *codes-found* (make-array 6))
   (initialize-variables)
   (read-net-from *net-file*)
   (loop for digit-lists in *digit-lists*
     do (retrieval-structure-day 50 digit-lists *times-array* 'no-free-recall))
   (when *print-major-updates* (display-codes-found))
   )

(defun report-contextual-codes-results ()
   (let ((enhanced-rehearsal 0)
         (depleted-rehearsal 0)
         (enhanced-recall 0)
         (depleted-recall 0)
         (enhanced-errors 0)
         (depleted-errors 0)
         (both-recall-array (make-array 6))
         (depleted-recall-array (make-array 6))
         (enhanced-recall-array (make-array 6)))
      (loop 
        for x from 0 to 5
        for enhanced-template in '((nil t t nil t nil nil t)
                                   (t t nil nil nil t nil t)
                                   (t nil nil t nil t t nil)
                                   (nil nil t t t nil t nil)
                                   (nil t t nil t nil nil t)
                                   (t nil nil t nil t t nil))
        for enhanced-list = (loop repeat *number-of-sessions*
                              append enhanced-template)
        for times-array = (aref *times-array* x)
        for errors-array = (aref *times-array* (+ x 6))
        do 
        (loop 
          for enhanced? in enhanced-list
          for times-list on times-array by #'cddr
          for errors in errors-array
          for rehearsal-time = (first times-list)
          for recall-time = (second times-list)
          for total-time = (+ rehearsal-time recall-time)
          if enhanced?
          do (setf enhanced-rehearsal (+ enhanced-rehearsal rehearsal-time)
                   (aref both-recall-array x) 
                   (cons total-time (aref both-recall-array x))
                  (aref enhanced-recall-array x) 
                   (cons total-time (aref enhanced-recall-array x))
                   enhanced-recall (+ enhanced-recall recall-time)
                   enhanced-errors (+ enhanced-errors errors))
          else if t
          do (setf depleted-rehearsal (+ depleted-rehearsal rehearsal-time)
                   (aref both-recall-array x) 
                   (cons total-time (aref both-recall-array x))
                   (aref depleted-recall-array x) 
                   (cons total-time (aref depleted-recall-array x))
                   depleted-recall (+ depleted-recall recall-time)
                   depleted-errors (+ depleted-errors errors))
          ))
      (format t "~%DD's and EPAM's Mean Serial Recall Performances Scores~%")
      (format t "(in Seconds) as a Function of List is-a~%")
      (format t "-----------------------------------------------------------~%")
      (format t "                      Enriched Lists     Depleted Lists~%")
      (format t "                      --------------     --------------~%")
      (format t "   Measure              DD     EPAM        DD     EPAM ~%")
      (format t "-----------------------------------------------------------~%")
      (format t "Percentage correct    99.8    ~5,1f      98.8    ~5,1f~%"
        (/ (- (* 24 50 *number-of-sessions*) enhanced-errors) (* 12 *number-of-sessions*))
        (/ (- (* 24 50 *number-of-sessions*) depleted-errors) (* 12 *number-of-sessions*)))
      (format t "Rehearsal time        29.1    ~5,1f      48.9    ~5,1f~%"
        (/ enhanced-rehearsal (* 24 *number-of-sessions*))
        (/ depleted-rehearsal (* 24 *number-of-sessions*)))
      (format t "Recall time           43.3    ~5,1f      66.7    ~5,1f~%"
        (/ enhanced-recall (* 24 *number-of-sessions*))
        (/ depleted-recall (* 24 *number-of-sessions*)))
      (format t "Total time            72.4    ~5,1f     115.6    ~5,1f~%"
        (/ (+ enhanced-recall enhanced-rehearsal) (* 24 *number-of-sessions*))
        (/ (+ depleted-rehearsal depleted-recall) (* 24 *number-of-sessions*)))
      (format t "~%DD's and EPAM's Median Total Recall Times (in Seconds) by~%")
      (format t "Trial Order for Enriched and Depleted Lists~%")
      (format t "------------------------------------------------------~%")
      (format t "       Enriched Lists   Depleted Lists   Both Combined~%")
      (format t "Trial  --------------   --------------   -------------~%")
      (format t "Order   DD     EPAM      DD     EPAM      DD     EPAM ~%")
      (format t "------------------------------------------------------~%")
      (format t "  1     53.5 ~5,1f       67.5 ~5,1f       61.0 ~5,1f ~%"
        (find-median (aref enhanced-recall-array 0))
        (find-median (aref depleted-recall-array 0))
        (find-median (aref both-recall-array 0)))
      (format t "  2     55.5 ~5,1f      102.5 ~5,1f       61.5 ~5,1f ~%"
        (find-median (aref enhanced-recall-array 1))
        (find-median (aref depleted-recall-array 1))
        (find-median (aref both-recall-array 1)))
      (format t "  3     55.0 ~5,1f      113.0 ~5,1f       96.5 ~5,1f ~%"
        (find-median (aref enhanced-recall-array 2))
        (find-median (aref depleted-recall-array 2))
        (find-median (aref both-recall-array 2)))
      (format t "  4     54.5 ~5,1f      102.0 ~5,1f       78.0 ~5,1f ~%"
        (find-median (aref enhanced-recall-array 3))
        (find-median (aref depleted-recall-array 3))
        (find-median (aref both-recall-array 3)))
      (format t "  5     73.5 ~5,1f      109.0 ~5,1f       77.5 ~5,1f ~%"
        (find-median (aref enhanced-recall-array 4))
        (find-median (aref depleted-recall-array 4))
        (find-median (aref both-recall-array 4)))
      (format t "  6     77.0 ~5,1f      112.0 ~5,1f      103.5 ~5,1f ~%"
        (find-median (aref enhanced-recall-array 5))
        (find-median (aref depleted-recall-array 5))
        (find-median (aref both-recall-array 5)))
    ))


(defun probes-simulation ()
   "This is the retrieval-structure simulation that was used in order to 
 predict the latency times of DD in the memory scanning task."
   (setf *net-file* "net173")
   (setf *activation* 868) ; This is the activation after a weekend and 173 blocks
   ; (+ (* 173 5) 2)
   (setf *record-codes-found* nil)
   (setf *digit-lists*  (list *probelist1a*
                          *probelist1b*
                          *probelist2a*
                          *probelist2b*
                          *probelist3a*
                          *probelist3b*
                          *probelist4a*
                          *probelist4b*
                          *probelist5a*
                          *probelist5b*
                          *probelist6a*
                          *probelist6b*))
   (setf *probes* (list *probes1a*
                    *probes1b*
                    *probes2a*
                    *probes2b*
                    *probes3a*
                    *probes3b*
                    *probes4a*
                    *probes4b*
                    *probes5a*
                    *probes5b*
                    *probes6a*
                    *probes6b*))
   (setf *print-time-updates* nil) 
   (setf *print-imagery-stores* nil) 
   (setf *print-experimenter-updates* nil) 
   (setf *print-epam-updates* nil) 
   (setf *print-strategy-updates* nil) 
   (setf *print-major-updates* nil) 
   (setf *use-contextual-codes* t) 
   (setf *record-free-recall* nil)
   (probes))

(defun probes ()
   (seed-random-generator)
   (initialize-variables)
   (read-net-from *net-file*)
   (setf *presentation-rate* 1000)
   (setf *stimulus-duration-rate* 200)
   (setf *signal-to-respond-delay* 4000)
   (setf *method-for-counting-syllables* 'subobject-method)
   (setf *forward-latencies-found* (make-array 15 :initial-element nil))
   (setf *backward-latencies-found* (make-array 15 :initial-element nil))
   (loop for digit-lists in *digit-lists*
     for probe-lists in *probes*
     do (practice-day-with-probe-task digit-lists probe-lists))
   (format t "~%Median response times in memory scanning task for forward~%")
   (format t "and backward directions.~%")
   (format t "----------------------------------------~%")
   (format t "Boundary Crossing   DD             EPAM~%")
   (format t "----------------------------------------~%")
   (format t "     Forward~%")     
   (format t "         1         2304           ~5,0f~%" (find-median (aref *forward-latencies-found* 1)))
   (format t "         2         2516           ~5,0f~%" (find-median (aref *forward-latencies-found* 2)))
   (format t "         3         1757           ~5,0f~%" (find-median (aref *forward-latencies-found* 3)))
   (format t "         4         2902           ~5,0f~%" (find-median (aref *forward-latencies-found* 4)))
   (format t "         5         2039           ~5,0f~%" (find-median (aref *forward-latencies-found* 5)))
   (format t "         6         2154           ~5,0f~%" (find-median (aref *forward-latencies-found* 6)))
   (format t "         7         2608           ~5,0f~%" (find-median (aref *forward-latencies-found* 7)))
   (format t "         8         2835           ~5,0f~%" (find-median (aref *forward-latencies-found* 8)))
   (format t "         9         2521           ~5,0f~%" (find-median (aref *forward-latencies-found* 9)))
   (format t "        10         3191           ~5,0f~%" (find-median (aref *forward-latencies-found* 10)))
   (format t "        11         2630           ~5,0f~%" (find-median (aref *forward-latencies-found* 11)))
   (format t "        12         1926           ~5,0f~%" (find-median (aref *forward-latencies-found* 12)))
   (format t "        13         2661           ~5,0f~%" (find-median (aref *forward-latencies-found* 13)))
   (format t "     Backward~%")                                                                                 
   (format t "         1         2532           ~5,0f~%" (find-median (aref *backward-latencies-found* 2)))
   (format t "         2         2128           ~5,0f~%" (find-median (aref *backward-latencies-found* 3)))
   (format t "         3         1981           ~5,0f~%" (find-median (aref *backward-latencies-found* 4)))
   (format t "         4         2710           ~5,0f~%" (find-median (aref *backward-latencies-found* 5)))
   (format t "         5         1808           ~5,0f~%" (find-median (aref *backward-latencies-found* 6)))
   (format t "         6         1639           ~5,0f~%" (find-median (aref *backward-latencies-found* 7)))
   (format t "         7         2938           ~5,0f~%" (find-median (aref *backward-latencies-found* 8)))
   (format t "         8         2279           ~5,0f~%" (find-median (aref *backward-latencies-found* 9)))
   (format t "         9         2832           ~5,0f~%" (find-median (aref *backward-latencies-found* 10)))
   (format t "        10         3787           ~5,0f~%" (find-median (aref *backward-latencies-found* 11)))
   (format t "        11         2245           ~5,0f~%" (find-median (aref *backward-latencies-found* 12)))
   (format t "        12         1502           ~5,0f~%" (find-median (aref *backward-latencies-found* 13)))
   (format t "        13         2757           ~5,0f~%" (find-median (aref *backward-latencies-found* 14)))
   )

(defun test-categorization-accuracy-simulation ()
   (initialize-variables)
   (create-categories-net)
   (loop for item in (loop for lis in *free-recall-data* append lis)
     for digit-list = (first item)
     for correct-category = (second item)
     for category = (use-net-to-find-the-code digit-list)
     for n from 1
     count (eql category correct-category) into result
     if (not (eql category correct-category))
     do (format t "For item = ~a, EPAM responded ~a~%" item category)
     finally (format t "Percentage-correct = ~5,1f for ~a categorizations" 
               (* 100 (/ result n)) n)))
          
 (defun calculate-dds-free-recall-errors ()
    (loop for recalled-items in *free-recall-data*
      for free-recall-group in *free-recall-groups*
      for n from 1
      with list1errors = 0
      with list2errors = 0
      with list3errors = 0
      do (loop 
           with extra-items = nil
           for omitted-items = (reorder-list-randomly free-recall-group) 
           then remaining-items
           for remitem in recalled-items
           for ritem = (first remitem)
           for remaining-items = (loop with i = nil
                                   for item in omitted-items
                                   for digits = (car item)
                                   if (and (null i)
                                           (equal ritem digits))
                                   do (setf i t)
                                   else if t
                                   collect item
                                   finally (when (not i) 
                                              (setf extra-items (cons ritem extra-items)
                                                    )))
           finally (progn 
                    (when *print-major-updates* 
                       (format t "Extra-items on list ~a = ~a~%" n extra-items)
                       (format t "Omitted-items on list ~a = ~a~%" n omitted-items))
                    (loop for oitem in omitted-items
                      if (= (second oitem) 1) do (incf list1errors)
                      if (= (second oitem) 2) do (incf list2errors)
                      if (= (second oitem) 3) do (incf list3errors))))
      finally
      (progn
       (format t "~%Percentage of Free Recall Omissions by Position of list~%")
      (format t "within session for DD and EPAM.~%")
       (format t "Position   DD-omissions    DD-omissions~%")
       (format t "    1          20.0            ~5,1f~%" (/ list1errors 2.9))
       (format t "    2           6.6            ~5,1f~%" (/ list2errors 2.9))
       (format t "    3           3.1            ~5,1f~%" (/ list3errors 2.9)))))
