# EPAM

EPAM (Elementary Perceiver And Memoriser) is a cognitive architecture, first developed by Herbert Simon and Edward Feigenbaum in the late 1950s. The final version of EPAM was published in 2002, and is known as EPAM VI.

EPAM VI is described in two important technical reports, still available from: http://web.archive.org/web/20131126061458/http://www.pahomeschoolers.com/epam/

This repository contains the implementation from that site, revised to work with SBCL.

To run all the simulations, open a terminal within the source code:

$ sbcl --script run-experiments.lisp


## MIT License

Copyright (c) 2002-2019, Howard Richman, Peter Lane

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

